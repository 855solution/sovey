<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	.list_thumbimg li{display: inline !important;}
	.list_thumbimg {padding-left:0 !important; margin-top: 10px;}
	.list_thumbimg li img{width:17% !important;}
	.list_thumbimg li img:hover{cursor: pointer;}
	.cur{border:2px solid #009E23 !important;}
	#top-bar img{width: 20px; margin-top: 5px;}
	
</style>
<div class="col-sm-9 p_wrap" id='featured'  >
	<div class="my_title col-sm-12">
		<span class=' col-sm-6 p_title'>Items Detail</span>
		
	</div>
	<p style='clear:both'></p>
	<div>
		<table class='table table-bordered'>
			<tr>
				<td style="font-weight:bold">Part Name</td>
				<td><?php echo $row->name ?></td>
			</tr>
			<tr>
				<td style="font-weight:bold">Make</td>
				<td><?php echo $row->make_name ?></td>
			</tr>
			<tr>
				<td style="font-weight:bold">Model</td>
				<td><?php echo $row->model_name ?></td>
			</tr>
			<tr>
				<td style="font-weight:bold">Year</td>
				<td><?php echo $row->year ?></td>
			</tr>
			<tr>
				<td style="font-weight:bold">Description</td>
				<td><?php echo $row->description ?></td>
			</tr>
		</table>
		<div style='text-align:center; width:100%'>
			<?php 
				$bigimg=base_url('assets/site/image/no_img.png');
				$imgdata=$this->sit->getarrimage($row->item_id);
				$imgdata=$imgdata['img'];
				if(count($imgdata)>0)
					if(file_exists(FCPATH."/uploads/".$imgdata[0]))
						$bigimg=site_url("../uploads/".$imgdata[0]);
			?>
			<!-- <div class='col-sm-12'>
				<img src="<?php echo $bigimg ?>" class='my-foto' data-large="<?php echo $bigimg ?>" id="bigpreview" style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
			</div> -->
			<p></p>
			<div class='col-sm-12' style="text-align:left">
				<ul id="gallery">
					<?php 
						
						for ($i=0; $i < count($imgdata); $i++) {
							$class='';
							if($i==0)
								$class='cur'; 
							if(file_exists(FCPATH."/uploads/thumb/".$imgdata[$i])){ ?>
								
								<li style="width:33%;"><a href="<?php echo site_url("../uploads/".$imgdata[$i]) ?>">
							        <img style="width:100%;" src="<?php echo site_url("../uploads/thumb/".$imgdata[$i]) ?>"
							            title="photo1 title">
							    </a></li>
							<?php }
						}
					?>
					
				</ul>
			</div>
			<div style="text-align:right; padding:10px;">
				<a href="<?php echo site_url('site/viewimage/'.$row->item_id) ?>" target='_blank'>View All Images</a>
			</div>
			
		</div>
	</div>
</div>

<script>
    // applying photobox on a `gallery` element which has lots of thumbnails links.
    // Passing options object as well:
    //-----------------------------------------------
   
	$('#gallery').photobox('a');
	// or with a fancier selector and some settings, and a callback:
	$('#gallery').photobox('a:first', { thumbs:false, time:0 }, imageLoaded);
	function imageLoaded(){
		console.log('image has been loaded...');
	}
	function preview(event){
			// aler('ok');
			$('.list_thumbimg li img').removeClass('cur');
			$(event.target).addClass('cur');
			var img=$(event.target).attr('rel');
			$('#bigpreview').attr('src',"<?php echo base_url('/uploads/"+img+"')?>");
			$('#bigpreview').attr('data-large',"<?php echo base_url('/uploads/"+img+"')?>");
			
		}
		
		$(function(){
			 $(".my-foto").imagezoomsl({
			  	descarea: ".big-caption", 				

				zoomrange: [1.68, 20],

				zoomstart: 1.68,

				cursorshadeborder: "10px solid black",

				magnifiereffectanimate: "fadeIn",
			  });
		});

</script>