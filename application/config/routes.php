<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/*
#
#Api
#
*/
$route['api/text_search'] = "api/api_search/searchText";
$route['api/search'] = "api/api_search";
$route['api/get_image'] = "api/get_image";
$route['api/scan_item'] = "api/scan_item";
$route['api/counting_item'] = "api/counting_item";
$route['api/get_itemnumber'] = "api/get_item_number";
$route['api/get_category'] = "api/get_category";
$route['api/list_location'] = "api/item_location/location";
$route['api/post'] = "api/item_entry/post";
$route['api/item_entry'] = "api/item_entry";
$route['api/get_make'] = "api/api_make";
$route['api/get_year'] = "api/api_year";
$route['api/get_model'] = "api/api_model";
$route['api/get_color'] = "api/api_color";
$route['api/get_partplacement'] = "api/api_partplacement";
$route['api/get_branch'] = "api/api_branch";
$route['api/get_khmername'] = "api/api_khmername";
$route['api/get_condition'] = "api/api_condition";
$route['api/add_item'] = "api/add_item";
$route['api/filter_part_number'] = "api/filter_partnumber";
$route['api/part_number'] = "api/add_item/get_part_number";
$route['api/item_location'] = "api/item_location";
$route['api/sign_in']      = "api/sign_in";
$route['api/get_location'] = "api/get_location/list_location";

$route['reports/(detailed_sales_vinnumber)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/detailed_sales_vinnumber'] = "reports/date_input";


$route['default_controller'] = "site/site";
$route['no_access/(:any)'] = "no_access/index/$1";
$route['reports/(summary_:any)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/summary_:any'] = "reports/date_input_excel_export";
$route['reports/(graphical_:any)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/graphical_:any'] = "reports/date_input";
$route['reports/(inventory_:any)/(:any)'] = "reports/$1/$2";
$route['reports/inventory_:any'] = "reports/excel_export";

$route['reports/(detailed_sales)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/detailed_sales'] = "reports/date_input";
$route['reports/(detailed_receivings)/(:any)/(:any)'] = "reports/$1/$2/$3";
$route['reports/detailed_receivings'] = "reports/date_input";
$route['reports/(specific_:any)/(:any)/(:any)/(:any)'] = "reports/$1/$2/$3/$4";
$route['reports/specific_customer'] = "reports/specific_customer_input";
$route['reports/specific_employee'] = "reports/specific_employee_input";
$route['product'] = "site/search";
$route['qr_code'] = "site/qr_code";

$route['scaffolding_trigger'] = "";

$route['404_override'] = 'errors/page_missing';

/* End of file routes.php */
/* Location: ./application/config/routes.php */