<?php
$lang['locations_location_name']='Location Name';
$lang['locations_card_description']='Description';
$lang['locations_basic_information']='Location Information';
$lang['locations_name_required']='Location Name is a required field';
$lang['locations_description_required']='Location Description is a required field';
$lang['locations_name']='Location Name is a required field';
$lang['locations_description']='Location Description is a required field';

$lang['locations_retrive_location_info']='Retrive Location Info';
$lang['locations_description']='Description';
$lang['locations_amazon']='Amazon';
$lang['locations_upc_database']='UPC Database';
$lang['locations_cannot_find_location']='Cannot find any information about location';
$lang['locations_info_provided_by']='Info provided by';
$lang['locations_name_information']='Location Name';
$lang['locations_new']='New Location';
$lang['locations_update']='Update Location';
$lang['locations_location']='Location';
$lang['locations_edit_multiple_locations']='Editing Multiple Locations';
$lang['locations_category']='Category';
$lang['locations_cost_price']='Cost Price';
$lang['locations_unit_price']='Unit Price';
$lang['locations_tax_1']='Tax 1';
$lang['locations_tax_2']='Tax 2';
$lang['locations_sales_tax_1'] = 'Sales Tax';
$lang['locations_sales_tax_2'] = 'Sales Tax 2';
$lang['locations_tax_percent']='Tax Percent';
$lang['locations_tax_percents']='Tax Percent(s)';
$lang['locations_reorder_level']='Reorder Level';
$lang['locations_quantity']='Quantity';
$lang['locations_no_locations_to_display']='No Locations to display';
$lang['locations_bulk_edit']='Bulk Edit';
$lang['locations_confirm_delete']='Are you sure you want to delete the selected locations?';
$lang['locations_none_selected']='You have not selected any locations to edit';
$lang['locations_confirm_bulk_edit']='Are you sure you want to edit all the locations selected?';
$lang['locations_successful_bulk_edit']='You have successfully updated the selected locations';
$lang['locations_error_updating_multiple']='Error updating locations';
$lang['locations_edit_fields_you_want_to_update']='Edit the fields you want to edit for ALL selected locations';
$lang['locations_error_adding_updating'] = 'Error adding/updating location';
$lang['locations_successful_adding']='You have successfully added location';
$lang['locations_successful_updating']='You have successfully updated location';
$lang['locations_successful_deleted']='You have successfully deleted';
$lang['locations_one_or_multiple']='location(s)';
$lang['locations_cannot_be_deleted']='Could not deleted selected locations, one or more of the selected locations has sales.';
$lang['locations_none'] = 'None';
$lang['locations_supplier'] = 'Supplier';
$lang['locations_generate_barcodes'] = 'Generate Barcodes';
$lang['locations_must_select_location_for_barcode'] = 'You must select at least 1 location to generate barcodes';
$lang['locations_excel_import_failed'] = 'Excel import failed';
$lang['locations_allow_alt_desciption'] = 'Allow Alt Description';
$lang['locations_is_serialized'] = 'Location has Serial Name';
$lang['locations_low_inventory_locations'] = 'Low Inventory Locations';
$lang['locations_serialized_locations'] = 'Serialized Locations';
$lang['locations_no_description_locations'] = 'No Description Locations';
$lang['locations_inventory_comments']='Comments';
$lang['locations_count']='Update Inventory';
$lang['locations_details_count']='Inventory Count Details';
$lang['locations_add_minus']='Inventory to add/subtract';
$lang['locations_current_quantity']='Current Quantity';
$lang['locations_quantity_required']='Quantity is a required field. Please Close ( X ) to cancel';
$lang['locations_do_nothing'] = 'Do Nothing';
$lang['locations_change_all_to_serialized'] = 'Change All To Serialized';
$lang['locations_change_all_to_unserialized'] = 'Change All To Unserialized';
$lang['locations_change_all_to_allow_alt_desc'] = 'Allow Alt Desc For All';
$lang['locations_change_all_to_not_allow_allow_desc'] = 'Not Allow Alt Desc For All';
$lang['locations_use_inventory_menu'] = 'Use Inv. Menu';
$lang['locations_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
?>
