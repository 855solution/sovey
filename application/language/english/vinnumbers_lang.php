<?php
$lang['vinnumbers_vinnumber_name']='Vinnumber';
$lang['vinnumbers_card_description']='Description';
$lang['vinnumbers_basic_information']='Vinnumber Information';
$lang['vinnumbers_name_required']='Vinnumber is a required field';
$lang['vinnumbers_description_required']='Vinnumber Description is a required field';
$lang['vinnumbers_name']='Vinnumber Name is a required field';
$lang['vinnumbers_description']='Vinnumber Description is a required field';

$lang['vinnumbers_retrive_vinnumber_info']='Retrive Vinnumber Info';
$lang['vinnumbers_description']='Description';
$lang['vinnumbers_amazon']='Amazon';
$lang['vinnumbers_upc_database']='UPC Database';
$lang['vinnumbers_cannot_find_vinnumber']='Cannot find any information about vinnumber';
$lang['vinnumbers_info_provided_by']='Info provided by';
$lang['vinnumbers_name_information']='Vinnumber Name';
$lang['vinnumbers_new']='New Vinnumber';
$lang['vinnumbers_update']='Update Vinnumber';
$lang['vinnumbers_vinnumber']='Vinnumber';
$lang['vinnumbers_edit_multiple_vinnumbers']='Editing Multiple Vinnumbers';
$lang['vinnumbers_category']='Category';
$lang['vinnumbers_cost_price']='Cost Price';
$lang['vinnumbers_unit_price']='Unit Price';
$lang['vinnumbers_tax_1']='Tax 1';
$lang['vinnumbers_tax_2']='Tax 2';
$lang['vinnumbers_sales_tax_1'] = 'Sales Tax';
$lang['vinnumbers_sales_tax_2'] = 'Sales Tax 2';
$lang['vinnumbers_tax_percent']='Tax Percent';
$lang['vinnumbers_tax_percents']='Tax Percent(s)';
$lang['vinnumbers_reorder_level']='Reorder Level';
$lang['vinnumbers_quantity']='Quantity';
$lang['vinnumbers_no_vinnumbers_to_display']='No Vinnumbers to display';
$lang['vinnumbers_bulk_edit']='Bulk Edit';
$lang['vinnumbers_confirm_delete']='Are you sure you want to delete the selected vinnumbers?';
$lang['vinnumbers_none_selected']='You have not selected any vinnumbers to edit';
$lang['vinnumbers_confirm_bulk_edit']='Are you sure you want to edit all the vinnumbers selected?';
$lang['vinnumbers_successful_bulk_edit']='You have successfully updated the selected vinnumbers';
$lang['vinnumbers_error_updating_multiple']='Error updating vinnumbers';
$lang['vinnumbers_edit_fields_you_want_to_update']='Edit the fields you want to edit for ALL selected vinnumbers';
$lang['vinnumbers_error_adding_updating'] = 'Error adding/updating vinnumber';
$lang['vinnumbers_successful_adding']='You have successfully added vinnumber';
$lang['vinnumbers_successful_updating']='You have successfully updated vinnumber';
$lang['vinnumbers_successful_deleted']='You have successfully deleted';
$lang['vinnumbers_one_or_multiple']='vinnumber(s)';
$lang['vinnumbers_cannot_be_deleted']='Could not deleted selected vinnumbers, one or more of the selected vinnumbers has sales.';
$lang['vinnumbers_none'] = 'None';
$lang['vinnumbers_supplier'] = 'Supplier';
$lang['vinnumbers_generate_barcodes'] = 'Generate Barcodes';
$lang['vinnumbers_must_select_vinnumber_for_barcode'] = 'You must select at least 1 vinnumber to generate barcodes';
$lang['vinnumbers_excel_import_failed'] = 'Excel import failed';
$lang['vinnumbers_allow_alt_desciption'] = 'Allow Alt Description';
$lang['vinnumbers_is_serialized'] = 'Vinnumber has Serial Name';
$lang['vinnumbers_low_inventory_vinnumbers'] = 'Low Inventory Vinnumbers';
$lang['vinnumbers_serialized_vinnumbers'] = 'Serialized Vinnumbers';
$lang['vinnumbers_no_description_vinnumbers'] = 'No Description Vinnumbers';
$lang['vinnumbers_inventory_comments']='Comments';
$lang['vinnumbers_count']='Update Inventory';
$lang['vinnumbers_details_count']='Inventory Count Details';
$lang['vinnumbers_add_minus']='Inventory to add/subtract';
$lang['vinnumbers_current_quantity']='Current Quantity';
$lang['vinnumbers_quantity_required']='Quantity is a required field. Please Close ( X ) to cancel';
$lang['vinnumbers_do_nothing'] = 'Do Nothing';
$lang['vinnumbers_change_all_to_serialized'] = 'Change All To Serialized';
$lang['vinnumbers_change_all_to_unserialized'] = 'Change All To Unserialized';
$lang['vinnumbers_change_all_to_allow_alt_desc'] = 'Allow Alt Desc For All';
$lang['vinnumbers_change_all_to_not_allow_allow_desc'] = 'Not Allow Alt Desc For All';
$lang['vinnumbers_use_inventory_menu'] = 'Use Inv. Menu';
$lang['vinnumbers_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
$lang['vinnumbers_model'] = 'Model';
$lang['vinnumbers_interior_color'] = 'Interior Color';
$lang['vinnumbers_exterior_color'] = 'Exterior Color';
$lang['vinnumbers_year'] = 'Year';
$lang['vinnumbers_make'] = 'Make';

?>
