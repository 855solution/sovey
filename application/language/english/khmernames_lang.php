<?php
$lang['khmernames_khmername_name']='Khmer Name';
$lang['khmernames_english_name']='English Name';

$lang['khmernames_card_description']='Description';
$lang['khmernames_basic_information']='Khmer Information';
$lang['khmernames_name_required']='Khmer Name is a required field';
$lang['khmernames_description_required']='Khmer Description is a required field';
$lang['khmernames_name']='Khmer Name is a required field';
$lang['khmernames_description']='Khmer Description is a required field';

$lang['khmernames_retrive_khmername_info']='Retrive Khmer Info';
$lang['khmernames_description']='Description';
$lang['khmernames_amazon']='Amazon';
$lang['khmernames_upc_database']='UPC Database';
$lang['khmernames_cannot_find_khmername']='Cannot find any information about Khmer Name';
$lang['khmernames_info_provided_by']='Info provided by';
$lang['khmernames_name_information']='Khmer Name';
$lang['khmernames_new']='New Khmer Name';
$lang['khmernames_update']='Update Khmer Name';
$lang['khmernames_khmername']='Khmer Name';
$lang['khmernames_edit_multiple_khmernames']='Editing Multiple Khmer Name';
$lang['khmernames_category']='Category';
$lang['khmernames_cost_price']='Cost Price';
$lang['khmernames_unit_price']='Unit Price';
$lang['khmernames_tax_1']='Tax 1';
$lang['khmernames_tax_2']='Tax 2';
$lang['khmernames_sales_tax_1'] = 'Sales Tax';
$lang['khmernames_sales_tax_2'] = 'Sales Tax 2';
$lang['khmernames_tax_percent']='Tax Percent';
$lang['khmernames_tax_percents']='Tax Percent(s)';
$lang['khmernames_reorder_level']='Reorder Level';
$lang['khmernames_quantity']='Quantity';
$lang['khmernames_no_khmernames_to_display']='No Khmer Name to display';
$lang['khmernames_bulk_edit']='Bulk Edit';
$lang['khmernames_confirm_delete']='Are you sure you want to delete the selected khmernames?';
$lang['khmernames_none_selected']='You have not selected any khmernames to edit';
$lang['khmernames_confirm_bulk_edit']='Are you sure you want to edit all the khmernames selected?';
$lang['khmernames_successful_bulk_edit']='You have successfully updated the selected khmernames';
$lang['khmernames_error_updating_multiple']='Error updating khmernames';
$lang['khmernames_edit_fields_you_want_to_update']='Edit the fields you want to edit for ALL selected khmernames';
$lang['khmernames_error_adding_updating'] = 'Error adding/updating khmername';
$lang['khmernames_successful_adding']='You have successfully added khmername';
$lang['khmernames_successful_updating']='You have successfully updated khmername';
$lang['khmernames_successful_deleted']='You have successfully deleted';
$lang['khmernames_one_or_multiple']='khmername(s)';
$lang['khmernames_cannot_be_deleted']='Could not deleted selected khmernames, one or more of the selected khmernames has sales.';
$lang['khmernames_none'] = 'None';
$lang['khmernames_supplier'] = 'Supplier';
$lang['khmernames_generate_barcodes'] = 'Generate Barcodes';
$lang['khmernames_must_select_khmername_for_barcode'] = 'You must select at least 1 khmername to generate barcodes';
$lang['khmernames_excel_import_failed'] = 'Excel import failed';
$lang['khmernames_allow_alt_desciption'] = 'Allow Alt Description';
$lang['khmernames_is_serialized'] = 'Khmer Name has Serial Name';
$lang['khmernames_low_inventory_khmernames'] = 'Low Inventory Khmer Name';
$lang['khmernames_serialized_khmernames'] = 'Serialized Khmer Name';
$lang['khmernames_no_description_khmernames'] = 'No Description Khmer Name';
$lang['khmernames_inventory_comments']='Comments';
$lang['khmernames_count']='Update Inventory';
$lang['khmernames_details_count']='Inventory Count Details';
$lang['khmernames_add_minus']='Inventory to add/subtract';
$lang['khmernames_current_quantity']='Current Quantity';
$lang['khmernames_quantity_required']='Quantity is a required field. Please Close ( X ) to cancel';
$lang['khmernames_do_nothing'] = 'Do Nothing';
$lang['khmernames_change_all_to_serialized'] = 'Change All To Serialized';
$lang['khmernames_change_all_to_unserialized'] = 'Change All To Unserialized';
$lang['khmernames_change_all_to_allow_alt_desc'] = 'Allow Alt Desc For All';
$lang['khmernames_change_all_to_not_allow_allow_desc'] = 'Not Allow Alt Desc For All';
$lang['khmernames_use_inventory_menu'] = 'Use Inv. Menu';
$lang['khmernames_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
?>
