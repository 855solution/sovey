<?php
$lang['due_new']='New Due';
$lang['due_Due']='Due';
$lang['due_update']='Update Due';
$lang['due_confirm_delete']='Are you sure you want to delete the selected due?';
$lang['due_none_selected']='You have not selected any due to delete';
$lang['due_error_adding_updating'] = 'Error adding/updating Due';
$lang['due_successful_adding']='You have successfully added Due';
$lang['due_successful_updating']='You have successfully updated Due';
$lang['due_successful_deleted']='You have successfully deleted';
$lang['due_one_or_multiple']='Due(s)';
$lang['due_cannot_be_deleted']='Could not deleted selected due, one or more of the selected due has sales.';
$lang['due_basic_information']='Due Information';
$lang['due_account_number']='Account #';
$lang['due_taxable']='Taxable';
?>
