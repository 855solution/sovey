<?php
$lang['engine_engine_name']='Engine Name';
$lang['engine_deleted']='Deleted';
$lang['engine_new']='New Engine';
$lang['engine_name_required']='Engine Name is a required field';
$lang['engine_basic_information']='Engine Information';

$lang['engine_successful_deleted']='You have successfully deleted';
$lang['engine_one_or_multiple']='Engine(s)';
$lang['engine_cannot_be_deleted']='Could not deleted selected makes, one or more of the selected makes has sales.';

$lang['body_description_required']='Make Description is a required field';
$lang['body_name']='Make Name is a required field';
$lang['body_description']='Make Description is a required field';
$lang['body_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
?>
