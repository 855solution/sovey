<?php
$lang['body_body_name']='Body Name';
$lang['body_id']='ID';
$lang['body_new']='New Body';
$lang['body_basic_information']='Body Information';
$lang['body_name_required']='Body Name is a required field';
$lang['body_description_required']='Make Description is a required field';
$lang['body_name']='Body Name is a required field';
$lang['body_successful_deleted']='You have successfully deleted';
$lang['body_one_or_multiple']='Body(s)';
$lang['body_cannot_be_deleted']='Could not deleted selected makes, one or more of the selected makes has sales.';
$lang['body_successful_adding']='You have successfully added body';
$lang['body_successful_updating']='You have successfully updated body';
$lang['body_error_adding_updating'] = 'Error adding/updating make';
$lang['body_description']='Make Description is a required field';
$lang['body_manually_editing_of_quantity'] = 'Manual Edit of Quantity';
?>
