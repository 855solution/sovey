<?php
$lang['module_home']='Home';

$lang['module_customers']='Customers';
$lang['module_customers_desc']='Add, Update, Delete, and Search customers';

$lang['module_body']='Body';
$lang['module_body_desc']='Add, Update, Delete, and Search customers';

$lang['module_engine']='Engine';
$lang['module_engine_desc']='Add, Update, Delete, and Search customers';

$lang['module_trim']='Trim';
$lang['module_trim_desc']='Add, Update, Delete, and Search customers';

$lang['module_suppliers']='Suppliers';
$lang['module_suppliers_desc']='Add, Update, Delete, and Search suppliers';

$lang['module_employees']='Employees';
$lang['module_employees_desc']='Add, Update, Delete, and Search employees';

$lang['module_sales']='Sales';
$lang['module_sales_desc']='Process sales and returns';

$lang['module_reports']='Reports';
$lang['module_reports_desc']='View and generate reports';

$lang['module_items']='Items';
$lang['module_items_desc']='Add, Update, Delete, and Search items';

$lang['module_config']='Store Config';
$lang['module_config_desc']='Change the store\'s configuration';

$lang['module_receivings']='Receivings';
$lang['module_receivings_desc']='Process Purchase orders';

$lang['module_giftcards']='Gift Cards';
$lang['module_giftcards_desc']='Add, Update, Delete and Search gift cards';

$lang['module_item_kits']='Item Kits';
$lang['module_item_kits_desc']='Add, Update, Delete and Search Item Kits';

$lang['module_locations']='Locations';
$lang['module_locations_desc']='Add, Update, Delete and Search location';

$lang['module_makes']='Makes';
$lang['module_makes_desc']='Add, Update, Delete and Search make';

$lang['module_models']='Models';
$lang['module_models_desc']='Add, Update, Delete and Search model';

$lang['module_colors']='Colors';
$lang['module_colors_desc']='Add, Update, Delete and Search color';

$lang['module_conditions']='Conditions';
$lang['module_conditions_desc']='Add, Update, Delete and Search condition';

$lang['module_partplacements']='Partplacements';
$lang['module_partplacements_desc']='Add, Update, Delete and Search partplacement';

$lang['module_branchs']='Brand';
$lang['module_branchs_desc']='Add, Update, Delete and Search brand';

$lang['module_categorys']='Categorys';
$lang['module_categorys_desc']='Add, Update, Delete and Search category';

$lang['module_vinnumbers']='Vinnumbers';
$lang['module_vinnumbers_desc']='Add, Update, Delete and Search vinnumber';

//Customer Due
$lang['module_past_dues']='Due History';
$lang['module_dues_desc']='Add, Update, Delete and Search due';

$lang['module_khmernames']='Khmer Name';
$lang['module_khmernames_desc']='Add, Update, Delete and Search Khmer Name';

$lang['module_slider']='Slider';
$lang['module_slider_desc']='Add, Update, Delete and Search slider';

$lang['module_page']='Page';
$lang['module_page_desc']='Add, Update, Delete and Search pages';

$lang['module_location_not']='Location Available';
$lang['module_location_notfill_desc']='List available location space';

$lang['module_partbarcode']='Part barcode';
$lang['module_partbarcode_desc']='List available barcode of part space';

$lang['module_subpart']='Sub Part';
$lang['module_subpart_desc']='List available barcode of part space';

$lang['module_member']='Member';
$lang['module_member_desc']='List member';

$lang['module_template']='Template';
$lang['module_template_desc']='List template';

$lang['module_returns']='Returns';
$lang['module_returns_desc']='List Returns';

$lang['module_permission']='Permission';
$lang['module_permission_desc']='Add, Update, Delete, and Search Permission';

$lang['module_transmission']='Transmission';
$lang['module_transmission_desc']='List Transmission';

$lang['module_fuel']='Fuel';
$lang['module_fuel_desc']='List Fuel';

$lang['module_check_price']='Check Price';
$lang['module_check_price_desc']='List Template';
?>