<?php
$lang['sales_giftcard_number']='Gift Card Number';
$lang['sales_giftcard']='Gift Card';
$lang['sales_register']='Sales Register';
$lang['sales_mode']='Register Mode';
$lang['sales_new_item'] = 'New Item';
$lang['sales_item_name'] = 'Item Name';
$lang['sales_item_number'] = 'Item #';
$lang['sales_new_customer'] = 'New Customer';
$lang['sales_customer'] = 'Customer';
$lang['sales_no_items_in_cart']='There are no items in the cart';
$lang['sales_total']='Total';
$lang['sales_tax_percent']='Tax %';
$lang['sales_price']='Price';
$lang['sales_quantity']='Qty.';
$lang['sales_discount']='Disc $';
$lang['sales_edit']='Edit';
$lang['sales_action']='Action';
$lang['sales_payment']='Payment Type';
$lang['sales_edit_item']='Edit Item';
$lang['sales_find_or_scan_item']='Find/Scan Item';
$lang['sales_find_or_scan_item_or_receipt']='Find/Scan Item OR Receipt';
$lang['sales_select_customer']='Select Customer (Optional)';
$lang['sales_start_typing_item_name']='Start Typing barcode or scan barcode...';
$lang['sales_start_typing_customer_name']='Start Typing customer\'s name...';
$lang['sales_sub_total']='Sub Total';
$lang['sales_tax']='Tax';
$lang['sales_comment']='Comment';
$lang['sales_unable_to_add_item']='Unable to add item to sale';
$lang['sales_sale_for_customer']='Customer:';
$lang['sales_remove_customer']='Remove Customer';
$lang['sales_error_editing_item']='Error editing item';
$lang['sales_complete_sale']='Complete Sale';
$lang['sales_cancel_sale']='Cancel Sale';
$lang['sales_add_payment']='Add Payment';
$lang['sales_receipt']='Sales Receipt';
$lang['sales_id']='Sale ID';
$lang['sales_sale']='Sale';
$lang['sales_return']='Return';
$lang['sales_confirm_finish_sale'] = 'Are you sure you want to submit this sale? This cannot be undone.';
$lang['sales_confirm_cancel_sale'] = 'Are you sure you want to clear this sale? All items will cleared.';
$lang['sales_cash'] = 'Cash';
$lang['sales_check'] = 'Check';
$lang['sales_debit'] = 'Debit Card';
$lang['sales_credit'] = 'Credit Card';
$lang['sales_giftcard'] = 'Gift Card';
$lang['sales_amount_tendered'] = 'Amount Tendered';
$lang['sales_change_due'] = 'Amount Due';
$lang['sales_payment_not_cover_total'] = 'Payment Amount does not cover Total';
$lang['sales_transaction_failed'] = 'Sales Transaction Failed';
$lang['sales_must_enter_numeric'] = 'Must enter numeric value for amount tendered';
$lang['sales_must_enter_numeric_giftcard'] = 'Must enter numeric value for giftcard number';
$lang['sales_serial'] = 'Serial';
$lang['sales_description_abbrv'] = 'Desc';
$lang['sales_item_out_of_stock'] = 'Item is Out of Stock';
$lang['sales_item_insufficient_of_stock'] = 'Item is Insufficient of Stock';
$lang['sales_quantity_less_than_zero'] = 'Warning, Desired Quantity is Insufficient. You can still process the sale, but check your inventory';
$lang['sales_item_no_quantity'] = "Warning, No Stock Quantity for this Item";
$lang['sales_quantity_less_than_zero_cant_sell'] = 'Warning, Desired Quantity is Insufficient.';

$lang['sales_successfully_updated'] = 'Sale successfully updated';
$lang['sales_unsuccessfully_updated'] = 'Sale unsuccessfully updated';
$lang['sales_edit_sale'] = 'Edit Sale';
$lang['sales_employee'] = 'Employee';
$lang['sales_successfully_deleted'] = 'Sale successfully deleted';
$lang['sales_unsuccessfully_deleted'] = 'Sale unsuccessfully deleted';
$lang['sales_delete_entire_sale'] = 'Delete entire sale';
$lang['sales_delete_confirmation'] = 'Are you sure you want to delete this sale, this action cannot be undone';
$lang['sales_date'] = 'Sale Date';
$lang['sales_delete_successful'] = 'You have successfully deleted a sale';
$lang['sales_delete_unsuccessful'] = 'You have unsuccessfully deleted a sale';
$lang['sales_suspend_sale'] = 'Deposit Sale';
$lang['sales_confirm_suspend_sale'] = 'Are you sure you want to Deposit this sale?';
$lang['sales_suspended_sales'] = 'Deposit Sales';
$lang['sales_suspended_sale_id'] = 'Deposit Sale ID';
$lang['sales_date'] = 'Date';
$lang['sales_customer'] = 'Customer';
$lang['sales_customer_name'] = 'Customer Name';
$lang['sales_customer_phone'] = 'Customer Phone';
$lang['sales_customer_address'] = 'Customer Address';

$lang['sales_comments'] = 'Comments';
$lang['sales_unsuspend_and_delete'] = 'View Deposit and Delete';
$lang['sales_unsuspend'] = 'View deposit';
$lang['sales_successfully_suspended_sale'] = 'Your sale has been successfully deposited';
$lang['sales_successfully_quoted_sale'] = 'Your sale has been successfully quoted';
$lang['sales_email_receipt'] = 'E-Mail Receipt';
$lang['sales_return_item_problem'] = 'Problem';

$lang['sales_item_vin'] = 'Vin #';
$lang['sales_item_khmername'] = 'Khmername';
$lang['sales_item_name'] = 'Name';
$lang['sales_item_make'] = 'Make';
$lang['sales_item_model'] = 'Model';
$lang['sales_item_year'] = 'Year';
$lang['sales_item_color'] = 'Color';
$lang['sales_item_part_placement'] = 'Part Placment';
$lang['sales_item_cost'] = 'Cost';
$lang['sales_item_price'] = 'Price';
$lang['sales_item_quantity'] = 'Quantity';
$lang['sales_item_note'] = 'Note';
$lang['sales_paid'] = 'Paid';









?>