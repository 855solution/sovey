<?php
/*
Gets the html table to manage people.
*/
function get_people_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','ID', 
	'Photo',
	$CI->lang->line('cus_title'),
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_company'),
	$CI->lang->line('common_email'),'Permission',
	$CI->lang->line('common_phone_number'),'Phone Number 2',
	'&nbsp');
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_people_manage_table_data_rows($people,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the people.
*/
function get_people_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_person_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_person_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$img_path=base_url('assets/site/image/no_img.png');
	$img=$CI->Customer->getimage($person->person_id);
	    if ($img->customer_id==$person->person_id) {
            if(file_exists(FCPATH."/uploads/thumb/".$img->imagename)){
                $img_path=base_url("/uploads/thumb/".$img->imagename);
            }
        }

	$table_data_row='<tr>';
	$table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
	$table_data_row.="<td>$person->person_id</td>";
	$table_data_row.='<td><img style="width:80px;" src="'.$img_path.'"></td>';
	$table_data_row.="<td>$person->title</td>";
	$table_data_row.='<td width="20%">'.character_limiter($person->last_name,13).'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->first_name,13).'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->nick_name,30).'</td>';
	$table_data_row.='<td width="20%">'.mailto($person->email,character_limiter($person->email,22)).'</td>';
	$table_data_row.='<td width="30%">'.$person->perm_name.'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->phone_number,13).'</td>';	
	$table_data_row.='<td width="20%">'.character_limiter($person->phone_number_2,13).'</td>';	
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$person->person_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	$table_data_row.='</tr>';
	
	return $table_data_row;
}



function get_customer_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('ID','Photo', 'Title',
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_company'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'&nbsp');
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_customer_manage_table_data_rows($people,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the people.
*/
function get_customer_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_customer_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='6'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_customer_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$img_path=base_url('assets/site/image/no_img.png');
	$img=$CI->Customer->getimage($person->person_id);
	    if ($img->customer_id==$person->person_id) {
            if(file_exists(FCPATH."/uploads/thumb/".$img->imagename)){
                $img_path=base_url("/uploads/thumb/".$img->imagename);
            }
        }
	// var_dump($person);
	$table_data_row='<tr>';
	// $table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
	$table_data_row.='<td>'.$person->person_id.'</td>';
	$table_data_row.='<td><img id="pic" src="'.$img_path.'"></td>';
	$table_data_row.='<td>'.$person->title.'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->last_name,13).'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->first_name,13).'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->nick_name,30).'</td>';
	$table_data_row.='<td width="30%">'.mailto($person->email,character_limiter($person->email,22)).'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($person->phone_number,13).'</td>';	
	// $table_data_row.='<td width="5%">'.anchor($controller_name."/view/$person->person_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	$table_data_row.='<td width="5%">
						<a href="'.site_url("sales/select_customer/$person->person_id").'">
						<div class="small_button">
							<span name="btnselectcus" id="btnselectcus" style="font-size:14px;">Select</span>
						</div>
						</a></td>';		
	
	$table_data_row.='</tr>';
	
	return $table_data_row;
}


/*
Gets the html table to manage suppliers.
*/
function get_supplier_manage_table($suppliers,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />',
	$CI->lang->line('suppliers_company_name'),
	$CI->lang->line('common_last_name'),
	$CI->lang->line('common_first_name'),
	$CI->lang->line('common_email'),
	$CI->lang->line('common_phone_number'),
	'&nbsp');
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_supplier_manage_table_data_rows($suppliers,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the supplier.
*/
function get_supplier_manage_table_data_rows($suppliers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($suppliers->result() as $supplier)
	{
		$table_data_rows.=get_supplier_data_row($supplier,$controller);
	}
	
	if($suppliers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_supplier_data_row($supplier,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='5%'><input type='checkbox' id='person_$supplier->person_id' value='".$supplier->person_id."'/></td>";
	$table_data_row.='<td width="17%">'.character_limiter($supplier->company_name,13).'</td>';
	$table_data_row.='<td width="17%">'.character_limiter($supplier->last_name,13).'</td>';
	$table_data_row.='<td width="17%">'.character_limiter($supplier->first_name,13).'</td>';
	$table_data_row.='<td width="22%">'.mailto($supplier->email,character_limiter($supplier->email,22)).'</td>';
	$table_data_row.='<td width="17%">'.character_limiter($supplier->phone_number,13).'</td>';		
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$supplier->person_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	$table_data_row.='</tr>';
	
	return $table_data_row;
}

/*
Gets the html table to manage items.
*/
function get_items_manage_table($items,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	'Images','Barcode',
	$CI->lang->line('items_item_number'),
	$CI->lang->line('items_name'),
	//$CI->lang->line('items_vinnumber'),
	$CI->lang->line('items_khmer_name'),
	
	$CI->lang->line('items_make'),
	$CI->lang->line('items_model'),
	$CI->lang->line('items_year'),
	$CI->lang->line('items_color'),
	$CI->lang->line('items_part_placement'),
	$CI->lang->line('items_branch'),
	
	$CI->lang->line('items_cost_price'),
	$CI->lang->line('items_unit_price'),
	//$CI->lang->line('items_tax_percents'),
	//$CI->lang->line('items_location'),
	$CI->lang->line('items_quantity'),
	$CI->lang->line('items_inventory')
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_items_manage_table_data_rows($items,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the items.
*/
function get_items_manage_table_data_rows($items,$controller,$image)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_item_data_row($item,$controller);
	}
	
	if($items->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='12'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_item_data_row($item,$controller)
{
	$CI =& get_instance();
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	//$item_khmer_name=$controller->get_item_khmer($item->khmername_id);
	
	
	
	$image=$controller->Item->getdefaultimg($item->item_id);
	$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
	if($image!=''){
		if(file_exists(FCPATH."uploads/thumb/".$image)){
			$im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
	            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
	      }
	}
    $img_path=img($im);
	
	$pend = '';
	if ($item->is_pending == 1) {
		$pend = 'ispending';
	}
	$table_data_row='<tr id="'.$pend.'">';
	$table_data_row.="<td style='vertical-align:middle;' width='2%'>


	<input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></td>";
	$table_data_row.='<td>'.$img_path.'<div class="v_photo"><a target="_blank" href='.site_url("$controller_name/view_all_v_photo/$item->item_id").'>View all photos</a></div></td>';

	// $table_data_row.='<td width="15%">'.$img_path.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.anchor($controller_name."/view/$item->item_id/width:$width", $item->barcode,array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';
	
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$item->item_number.'</td>';
	
	$table_data_row.='<td style="vertical-align:middle;" width="20%">'.$item->name.'</td>';
	
	//$table_data_row.='<td width="14%">'.$item->category.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="20%">'.$controller->get_item_khmer($item->khmername_id).'</td>';
	
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$CI->Item->get_make_model_year($item->item_id)->make_name.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$CI->Item->get_make_model_year($item->item_id)->model_name.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$item->year.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$CI->Item->get_make_model_year($item->item_id)->color_name.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$CI->Item->get_make_model_year($item->item_id)->partplacement_name.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$CI->Item->get_make_model_year($item->item_id)->branch_name.'</td>';
	
	
	$table_data_row.='<td style="vertical-align:middle;" width="14%">'.to_currency($item->cost_price).'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="14%">'.to_currency($item->unit_price).'</td>';
	//$table_data_row.='<td width="14%">'.$tax_percents.'</td>';	
	//$table_data_row.='<td width="14%">'.$location.'</td>';	
	
	$table_data_row.='<td style="vertical-align:middle;" width="14%">'.anchor($controller_name."/inventory/$item->item_id/width:$width", $item->quantity,array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_count'))).'</td>';
	
	// $table_data_row.='<td style="vertical-align:middle;" width="0%">'./*anchor($controller_name."/view/$item->item_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).*/'</td>';		
	
	//Ramel Inventory Tracking
	$table_data_row.='<td style="vertical-align:middle;" width="5%">'./*anchor($controller_name."/inventory/$item->item_id/width:$width", $CI->lang->line('common_inv'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_count')))./'</td>';//inventory count	
	
	$table_data_row.='<td width="5%">'*/'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/width:$width", $CI->lang->line('common_det'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_details_count'))).'</td>';//inventory details	
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage giftcards.
*/
function get_giftcards_manage_table( $giftcards, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('giftcards_giftcard_number'),
	$CI->lang->line('giftcards_card_value'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_giftcards_manage_table_data_rows( $giftcards, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_giftcards_manage_table_data_rows( $giftcards, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($giftcards->result() as $giftcard)
	{
		$table_data_rows.=get_giftcard_data_row( $giftcard, $controller );
	}
	
	if($giftcards->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('giftcards_no_giftcards_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_giftcard_data_row($giftcard,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='giftcard_$giftcard->giftcard_id' value='".$giftcard->giftcard_id."'/></td>";
	$table_data_row.='<td width="15%">'.$giftcard->giftcard_number.'</td>';
	$table_data_row.='<td width="20%">'.to_currency($giftcard->value).'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$giftcard->giftcard_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage item kits.
*/
function get_item_kits_manage_table( $item_kits, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('item_kits_name'),
	$CI->lang->line('item_kits_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_item_kits_manage_table_data_rows( $item_kits, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the item kits.
*/
function get_item_kits_manage_table_data_rows( $item_kits, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($item_kits->result() as $item_kit)
	{
		$table_data_rows.=get_item_kit_data_row( $item_kit, $controller );
	}
	
	if($item_kits->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('item_kits_no_item_kits_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_item_kit_data_row($item_kit,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='item_kit_$item_kit->item_kit_id' value='".$item_kit->item_kit_id."'/></td>";
	$table_data_row.='<td width="15%">'.$item_kit->name.'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($item_kit->description, 25).'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$item_kit->item_kit_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}



/*
Gets the html table to manage locations.
*/
function get_locations_manage_table( $locations, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','Image', 
	$CI->lang->line('locations_location_name'),
	$CI->lang->line('locations_card_description'),'Max Quantity',
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_locations_manage_table_data_rows( $locations, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the location.
*/
function get_locations_manage_table_data_rows( $locations, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($locations->result() as $location)
	{
		$table_data_rows.=get_location_data_row( $location, $controller );
	}
	
	if($locations->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('locations_no_locations_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_location_data_row($location,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
	
	if(file_exists(FCPATH."assets/upload/location/".$location->location_id.'.jpg')){
		$im=array('src'=>'assets/upload/location/'.$location->location_id.'.jpg','width'=>"120");
            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
      }
     $img_path=img($im);

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='location_$location->location_id' value='".$location->location_id."'/></td>";
	$table_data_row.='<td width="15%" >'.$img_path.'</td>';
	$table_data_row.='<td width="15%" style="vertical-align:middle">'.$location->location_name.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$location->description.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$location->max_qty.'</td>';
	$table_data_row.='<td width="5%" style="vertical-align:middle">'.anchor($controller_name."/view/$location->location_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage locations.
*/
function get_locations_not_manage_table( $locations, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','Image', 
	$CI->lang->line('locations_location_name'),
	$CI->lang->line('locations_card_description'),'Max Quantity',
	'#items', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_locations_not_manage_table_data_rows( $locations, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the location.
*/
function get_locations_not_manage_table_data_rows( $locations, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($locations->result() as $location)
	{
		$table_data_rows.=get_location_not_data_row( $location, $controller );
	}
	
	if($locations->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('locations_no_locations_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_location_not_data_row($location,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$count=$location->count;
	if($count==null)
		$count=0;
	$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
	
	if(file_exists(FCPATH."assets/upload/location/".$location->location_id.'.jpg')){
		$im=array('src'=>'assets/upload/location/'.$location->location_id.'.jpg','width'=>"120");
            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
      }
     $img_path=img($im);

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='location_$location->location_id' value='".$location->location_id."'/></td>";
	$table_data_row.='<td width="15%" >'.$img_path.'</td>';
	$table_data_row.='<td width="15%" style="vertical-align:middle">'.$location->location_name.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$location->description.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$location->max_qty.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$count.'</td>';
	// $table_data_row.='<td width="5%" style="vertical-align:middle">'.anchor($controller_name."/view/$location->location_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}



/*
Gets the html table to manage locations.
*/
function get_page_manage_table( $page, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','Page Name', 
	'Order',
	'Is active',
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_page_manage_table_data_rows( $page, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the location.
*/
function get_page_manage_table_data_rows( $pages, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($pages->result() as $page)
	{
		$table_data_rows.=get_page_data_row( $page, $controller );
	}
	
	if($pages->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>No Page found</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_page_data_row($page,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='location_$page->pageid' value='".$page->pageid."'/></td>";
	$table_data_row.='<td width="15%" >'.$page->page_name.'</td>';
	$table_data_row.='<td width="15%" style="vertical-align:middle">'.$page->orders.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$page->is_active.'</td>';
	$table_data_row.='<td width="5%" style="vertical-align:middle">'.anchor($controller_name."/view/$page->pageid/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}



/*
Gets the html table to manage locations.
*/
function get_member_manage_table( $page, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','Last name', 
	'First name',
	'Gender',
	'Address',
	'Email',
	'Spacial',
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_member_manage_table_data_rows( $page, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the location.
*/
function get_member_manage_table_data_rows( $pages, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($pages->result() as $page)
	{
		$table_data_rows.=get_member_data_row( $page, $controller );
	}
	
	if($pages->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>No Page found</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_member_data_row($page,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$sp='No';
	if($page->is_sp==1)
		$sp="Yes";
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='location_$page->member_id' value='".$page->member_id."'/></td>";
	$table_data_row.='<td width="15%" >'.$page->last_name.'</td>';
	$table_data_row.='<td width="15%" style="vertical-align:middle">'.$page->first_name.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$page->gender.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$page->address.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$page->email.'</td>';
	$table_data_row.='<td width="20%" style="vertical-align:middle">'.$sp.'</td>';
	$table_data_row.='<td width="5%" style="vertical-align:middle">'.anchor($controller_name."/view/$page->member_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}


/*
Gets the html table to manage makes.
*/
function get_makes_manage_table( $makes, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('makes_make_name'),
	$CI->lang->line('makes_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_makes_manage_table_data_rows( $makes, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the make.
*/
function get_makes_manage_table_data_rows( $makes, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($makes->result() as $make)
	{
		$table_data_rows.=get_make_data_row( $make, $controller );
	}
	
	if($makes->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('makes_no_makes_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_make_data_row($make,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='make_$make->make_id' value='".$make->make_id."'/></td>";
	$table_data_row.='<td width="15%">'.$make->make_name.'</td>';
	$table_data_row.='<td width="20%">'.$make->description.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$make->make_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
function get_template_manage_table( $makes, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	"Images",
	'Part number',
	"Part name",
	// "Fit",
	"Make",
	"Model",
	"Year",
	"Color",
	"Condition",
	"Approved",
	'&nbsp',
	'&nbsp',
	'&nbsp',

	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_template_manage_table_data_rows( $makes, $controller );
	$table.='</tbody></table>';
	return $table;
} 


/*
Gets the html data rows for the make.
*/
function get_template_manage_table_data_rows( $makes, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($makes->result() as $make)
	{
		$table_data_rows.=get_template_data_row( $make, $controller );
	}
	
	if($makes->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('makes_no_makes_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_template_data_row($make,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
		$images=$make->partid;
	$im=array('src'=>'assets/site/image/no_img.png','width'=>"70");
	if(file_exists(FCPATH."uploads/thumb/".$images.'.png')){
		$im=array('src'=>'uploads/thumb/'.$images.'.png','width'=>"70");
        //$img_path=img("uploads/thumb/".$make->tem_image_id.'.jpg');
    }
 
     $img_path=img($im);
     $class='';
    $approve='Approved';
    if($make->approve==0){
    	$approve='Pending';
    	$class='mark';
    }
     // var_dump($make);
	$table_data_row='<tr class="'.$class.'">';
	$table_data_row.="<td width='3%'><input type='checkbox' id='make_$make->partid' value='".$make->partid."'/></td>";
	$table_data_row.='<td width="15%">'.$img_path.'</td>';

	$table_data_row.='<td width="15%">'.$make->part_number.'<input id="part_id" type="hidden" value='.$make->partid.'></td>';
	
	$table_data_row.='<td width="20%">'.$make->part_name.'</td>';
	// $table_data_row.='<td width="20%">'.$make->fit.'</td>';
	$table_data_row.='<td width="20%">'.$make->make_name.'</td>';
	$table_data_row.='<td width="20%">'.$make->model_name.'</td>';
	$table_data_row.='<td width="20%">'.$make->year.'</td>';
	$table_data_row.='<td width="20%">'.$make->color_name.'</td>';
	$table_data_row.='<td width="20%">'.$make->condition_name.'</td>';
	$table_data_row.='<td width="20%">'.$approve.'</td>';

	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$make->partid/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox disable_select','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';				
	$table_data_row.='<td width="5%"><a onclick="doOnClick(event);">'.$CI->lang->line('common_add_to_item').'</a></td>';				
	$table_data_row.='<td>'.anchor("$controller_name/view_tem_history/$make->part_number",
		"History",
		array('class'=>'thickbox none','title'=>'Sale History')).'</td>';			
	

	$table_data_row.='</tr>';
	return $table_data_row;
}
//============= Body ================

function get_body_manage_table( $body, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 

	$CI->lang->line('body_body_name'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_body_manage_table_data_rows( $body, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the make.
*/
function get_body_manage_table_data_rows( $body, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($body->result() as $body)
	{
		$table_data_rows.=get_body_data_row( $body, $controller );
	}
	
	// if($body->num_rows()==0)
	// {
	// 	$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('body_no_body_to_display')."</div></tr></tr>";
	// }
	
	return $table_data_rows;
}

function get_body_data_row($body,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='body_$body->body_id' value='".$body->body_id."'/></td>";
	$table_data_row.='<td width="15%">'.$body->body_name.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$body->body_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
//============ End Body =================

//============= Engine ================

function get_engine_manage_table( $engine, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('engine_engine_name'),
	$CI->lang->line('engine_deleted'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_engine_manage_table_data_rows( $engine, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the make.
*/
function get_engine_manage_table_data_rows( $engine, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($engine->result() as $engine)
	{
		$table_data_rows.=get_engine_data_row( $engine, $controller );
	}
	
	// if($body->num_rows()==0)
	// {
	// 	$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('body_no_body_to_display')."</div></tr></tr>";
	// }
	
	return $table_data_rows;
}

function get_engine_data_row($engine,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='engine_$engine->engine_id' value='".$engine->engine_id."'/></td>";
	$table_data_row.='<td width="15%">'.$engine->engine_name.'</td>';
	$table_data_row.='<td width="20%">'.$engine->deleted.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$engine->engine_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
//============ End Engine =================

//============= Trim ================

function get_trim_manage_table( $trim, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('trim_trim_name'),
	$CI->lang->line('trim_deleted'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_trim_manage_table_data_rows( $trim, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the make.
*/
function get_trim_manage_table_data_rows( $trim, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($trim->result() as $trim)
	{
		$table_data_rows.=get_trim_data_row( $trim, $controller );
	}
	
	// if($body->num_rows()==0)
	// {
	// 	$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('body_no_body_to_display')."</div></tr></tr>";
	// }
	
	return $table_data_rows;
}

function get_trim_data_row($trim,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='trim_$trim->trim_id' value='".$trim->trim_id."'/></td>";
	$table_data_row.='<td width="15%">'.$trim->trim_name.'</td>';
	$table_data_row.='<td width="20%">'.$trim->deleted.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$trim->trim_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
//============ End Trim =================

/*
Gets the html table to manage model.
*/
function get_models_manage_table( $models, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('models_model_name'),
	$CI->lang->line('models_card_model_description'),
	$CI->lang->line('makes_make_name'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_models_manage_table_data_rows( $models, $controller );
	$table.='</tbody></table>';
	return $table;
}
/*
Gets the html table to manage slide.
*/
function get_slide_manage_table( $models, $controller ) 
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	"Images",
	"Slide Name",
	"Type",
	"Orders",
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_slide_manage_table_data_rows( $models, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the model.
*/
function get_models_manage_table_data_rows( $models, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($models->result() as $model)
	{
		$table_data_rows.=get_model_data_row( $model, $controller );
	}
	
	if($models->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('models_no_models_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}
function get_slide_manage_table_data_rows( $models, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($models->result() as $model)
	{
		$table_data_rows.=get_slide_data_row( $model, $controller );
	}
	
	if($models->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('models_no_models_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_slide_data_row($model,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$im=array('src'=>'assets/site/image/no_img.png','width'=>"80");
	
	if(file_exists(FCPATH."assets/upload/slide/".$model->slide_id.'.jpg')){
		$im=array('src'=>'assets/upload/slide/'.$model->slide_id.'.jpg','width'=>"80");
            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
      }
     $img_path=img($im);
	$table_data_row='<tr>';
	$table_data_row.="<td style='vertical-align:middle;' width='3%'><input type='checkbox' id='model_$model->slide_id' value='".$model->slide_id."'/></td>";
	
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$img_path.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="15%">'.$model->name.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="20%">'.$model->type.'</td>';
	$table_data_row.='<td style="vertical-align:middle;" width="20%">'.$model->orders.'</td>';
	
	$table_data_row.='<td style="vertical-align:middle;" width="5%">'.anchor($controller_name."/view/$model->slide_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
function get_model_data_row($model,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='model_$model->model_id' value='".$model->model_id."'/></td>";
	$table_data_row.='<td width="15%">'.$model->model_name.'</td>';
	$table_data_row.='<td width="20%">'.$model->model_description.'</td>';
	$table_data_row.='<td width="20%">'.$model->make_name.'</td>';
	
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$model->model_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}



/*
Gets the html table to manage color.
*/
function get_colors_manage_table( $colors, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('colors_color_name'),
	$CI->lang->line('colors_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_colors_manage_table_data_rows( $colors, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the color.
*/
function get_colors_manage_table_data_rows( $colors, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($colors->result() as $color)
	{
		$table_data_rows.=get_color_data_row( $color, $controller );
	}
	
	if($colors->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('colors_no_colors_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_color_data_row($color,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='color_$color->color_id' value='".$color->color_id."'/></td>";
	$table_data_row.='<td width="15%">'.$color->color_name.'</td>';
	$table_data_row.='<td width="20%">'.$color->description.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$color->color_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}





/*
Gets the html table to manage color.
*/
function get_conditions_manage_table( $conditions, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('conditions_condition_name'),
	$CI->lang->line('conditions_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_conditions_manage_table_data_rows( $conditions, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the condition.
*/
function get_conditions_manage_table_data_rows( $conditions, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($conditions->result() as $condition)
	{
		$table_data_rows.=get_condition_data_row( $condition, $controller );
	}
	
	if($conditions->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('conditions_no_conditions_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_condition_data_row($condition,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='condition_$condition->condition_id' value='".$condition->condition_id."'/></td>";
	$table_data_row.='<td width="15%">'.$condition->condition_name.'</td>';
	$table_data_row.='<td width="20%">'.$condition->description.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$condition->condition_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}




/*
Gets the html table to manage partplacement.
*/
function get_partplacements_manage_table( $partplacements, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('partplacements_partplacement_name'),
	$CI->lang->line('partplacements_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_partplacements_manage_table_data_rows( $partplacements, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the partplacement.
*/
function get_partplacements_manage_table_data_rows( $partplacements, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($partplacements->result() as $partplacement)
	{
		$table_data_rows.=get_partplacement_data_row( $partplacement, $controller );
	}
	
	if($partplacements->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('partplacements_no_partplacements_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_partplacement_data_row($partplacement,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='partplacement_$partplacement->partplacement_id' value='".$partplacement->partplacement_id."'/></td>";
	$table_data_row.='<td width="15%">'.$partplacement->partplacement_name.'</td>';
	$table_data_row.='<td width="20%">'.$partplacement->description.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$partplacement->partplacement_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}




/*
Gets the html table to manage branch.
*/
function get_branchs_manage_table( $branchs, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('branchs_branch_name'),
	$CI->lang->line('branchs_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_branchs_manage_table_data_rows( $branchs, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the branch.
*/
function get_branchs_manage_table_data_rows( $branchs, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($branchs->result() as $branch)
	{
		$table_data_rows.=get_branch_data_row( $branch, $controller );
	}
	
	if($branchs->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('branchs_no_branchs_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_branch_data_row($branch,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='branch_$branch->branch_id' value='".$branch->branch_id."'/></td>";
	$table_data_row.='<td width="15%">'.$branch->branch_name.'</td>';
	$table_data_row.='<td width="20%">'.$branch->description.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$branch->branch_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage category.
*/
function get_categorys_manage_table( $categorys, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('categorys_category_name'),
	$CI->lang->line('categorys_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_categorys_manage_table_data_rows( $categorys, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the category.
*/
function get_categorys_manage_table_data_rows( $categorys, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($categorys->result() as $category)
	{
		$table_data_rows.=get_category_data_row( $category, $controller );
	}
	
	if($categorys->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('categorys_no_categorys_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_category_data_row($category,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='category_$category->category_id' value='".$category->category_id."'/></td>";
	$table_data_row.='<td width="15%">'.$category->category_name.'</td>';
	$table_data_row.='<td width="20%">'.$category->description.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$category->category_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}


/*
Gets the html table to manage vinnumber.
*/
function get_vinnumbers_manage_table( $vinnumbers, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','Images', 
	$CI->lang->line('vinnumbers_vinnumber_name'),
	$CI->lang->line('vinnumbers_year'),
	$CI->lang->line('vinnumbers_make'),
	$CI->lang->line('vinnumbers_model'),
	$CI->lang->line('vinnumbers_interior_color'),
	$CI->lang->line('vinnumbers_exterior_color'),
	$CI->lang->line('vinnumbers_cost_price'),
	'Actions',
	// $CI->lang->line(''), 


	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_vinnumbers_manage_table_data_rows( $vinnumbers, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the vinnumber.
*/
function get_vinnumbers_manage_table_data_rows( $vinnumbers, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	foreach($vinnumbers->result() as $vinnumber)
	{
		$table_data_rows.=get_vinnumber_data_row( $vinnumber, $controller );
	}
	
	if($vinnumbers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='13'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('vinnumbers_no_vinnumbers_to_display')."</div></td></tr>";
	}
	
	return $table_data_rows;
}

	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}


function get_vinnumber_data_row($vinnumber,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	
	$y = get2lastString($vinnumber->year);
	$image=$controller->vinnm->getdefaultimg($vinnumber->vinnumber_id);
	$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
	if($image!=''){
		if(file_exists(FCPATH."uploads/thumb/".$image)){
			$im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
	            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
	      }
	}
	
     $img_path=img($im);
	// var_dump($im);//die();
	$color_vin = anchor($controller_name."/view/$vinnumber->vinnumber_id/width:$width",'<strong>'.'H'."<span style='color:#7a0606'>".$vinnumber->vinnumber_id."</span><span style='color:#067a34'>".$vinnumber->model_id."</span><span style='color:#7a0668'>".$y.'</span></strong>',array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update')));
	$table_data_row='<tr>';
	$table_data_row.="<td><input type='checkbox' id='vinnumber_$vinnumber->vinnumber_id' value='".$vinnumber->vinnumber_id."'/></td>";
	$table_data_row.='<td>'.$img_path.'<div class="v_photo"><a target="_blank" href="'.site_url("$controller_name/view_all_v_photo/$vinnumber->vinnumber_id").'">View all photos</a></div></td>';
	$table_data_row.='<td>'.$color_vin.'</td>';
	$table_data_row.='<td>'.$vinnumber->year.'</td>';
	$table_data_row.='<td>'.$vinnumber->make_name.'</td>';
	$table_data_row.='<td>'.$vinnumber->model_name.'</td>';
	$table_data_row.='<td>'.$vinnumber->in_co.'</td>';
	$table_data_row.='<td>'.$vinnumber->ex_co.'</td>';
	$table_data_row.='<td>'.to_currency($vinnumber->cost_price).'</td>';

	// $table_data_row.='<td>'.anchor($controller_name."/view/$vinnumber->vinnumber_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';
	// $table_data_row.='<td>'/*.anchor($controller_name."/view/$vinnumber->vinnumber_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update')))*/.'</td>';
	// $table_data_row.='<td>'.anchor("$controller_name/view_vin_sale/$vinnumber->vinnumber_id",
	// 						"View Sale Items",
	// 						array('class'=>'thickbox none','title'=>'Vinnumber Sale Item')).'</td>';
	$table_data_row.='<td><a target="_blank" href="'.site_url("$controller_name/view_vin_sale/$vinnumber->vinnumber_id").'">Part Sold</a></td>';
	$table_data_row.='</tr>';


	return $table_data_row;
}



function get_partbar_manage_table( $vinnumbers, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	'Name','Make','Model','Year','Description',
	$CI->lang->line(''),'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_partbar_manage_table_data_rows( $vinnumbers, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the vinnumber.
*/
function get_partbar_manage_table_data_rows( $vinnumbers, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($vinnumbers->result() as $vinnumber)
	{
		$table_data_rows.=get_partbar_data_row( $vinnumber, $controller );
	}
	
	if($vinnumbers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='13'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('vinnumbers_no_vinnumbers_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}


function get_partbar_data_row($vinnumber,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	// // $image=$controller->vinnm->getdefaultimg($vinnumber->vinnumber_id);
	// $im=array('src'=>'assets/site/image/no_img.png','width'=>"70");
	// if(file_exists(FCPATH."uploads/partbar/".$vinnumber->partbar_id.'.jpg')){
	// 	$im=array('src'=>'uploads/partbar/thumb/'.$vinnumber->partbar_id.'.jpg','width'=>"70");
 //            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
 //      }
	
 //     $img_path=img($im);
	
	// // $color_vin = '<strong>'.'H'."<span style='color:#7a0606'>".$vinnumber->partbar_id."</span><span style='color:#067a34'>".$vinnumber->model_id."</span><span style='color:#7a0668'>".$y.'</span></strong>';
	
	
	
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='vinnumber_$vinnumber->partbar_id' class='partbarid' value='".$vinnumber->partbar_id."'/></td>";
	
	// $table_data_row.='<td width="15%">'.$img_path.'</td>';
	// $table_data_row.='<td width="15%">'.$vinnumber->item_number.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->name.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->make_name.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->model_name.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->year.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->descr.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$vinnumber->partbar_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';
	$table_data_row.='<td width="5%">'/*.anchor($controller_name."/view/$vinnumber->vinnumber_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update')))*/.'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}




function get_subpartbar_manage_table( $vinnumbers, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','Images', 
	'Item Number','Mian part',
	$CI->lang->line(''),'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_subpartbar_manage_table_data_rows( $vinnumbers, $controller );
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the vinnumber.
*/
function get_subpartbar_manage_table_data_rows( $vinnumbers, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($vinnumbers->result() as $vinnumber)
	{
		$table_data_rows.=get_subpartbar_data_row( $vinnumber, $controller );
	}
	
	if($vinnumbers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='13'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('vinnumbers_no_vinnumbers_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}


function get_subpartbar_data_row($vinnumber,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	// $image=$controller->vinnm->getdefaultimg($vinnumber->vinnumber_id);
	$im=array('src'=>'assets/site/image/no_img.png','width'=>"70");
	if(file_exists(FCPATH."uploads/partbar/".$vinnumber->subpartbar_id.'.jpg')){
		$im=array('src'=>'uploads/partbar/thumb/'.$vinnumber->subpartbar_id.'.jpg','width'=>"70");
            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
      }
	
     $img_path=img($im);
	
	// $color_vin = '<strong>'.'H'."<span style='color:#7a0606'>".$vinnumber->partbar_id."</span><span style='color:#067a34'>".$vinnumber->model_id."</span><span style='color:#7a0668'>".$y.'</span></strong>';
	
	
	
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='vinnumber_$vinnumber->subpartbar_id' class='partbarid' value='".$vinnumber->subpartbar_id."'/></td>";
	
	$table_data_row.='<td width="15%">'.$img_path.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->item_number.'</td>';
	$table_data_row.='<td width="15%">'.$vinnumber->name.'</td>';
	// $table_data_row.='<td width="15%">'.$vinnumber->make_name.'</td>';
	// $table_data_row.='<td width="15%">'.$vinnumber->model_name.'</td>';
	// $table_data_row.='<td width="15%">'.$vinnumber->year.'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$vinnumber->subpartbar_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';
	$table_data_row.='<td width="5%">'/*.anchor($controller_name."/view/$vinnumber->vinnumber_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update')))*/.'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}


//======= past due ==============

function get_past_due_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array(
	'<input type="checkbox" id="select_all" />', 
	'Invoice ID','Sale Date',
	'Customer',	
	'Grand Total',
	'Paid',
	'Balance',
	'&nbsp');
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_past_due_manage_table_data_rows($people,$controller);
	$table.='</tbody></table>';
	return $table;
}

function get_past_due_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_past_due_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='8'><div class='warning_message' style='padding:7px;'>No Data to Display</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_past_due_data_row($person,$controller)
{
$CI =& get_instance();
$controller_name=strtolower(get_class($CI));
$width = $controller->get_form_width();

$com = '';
if ($person->nick_name!='') {
	$com = "($person->nick_name)";
}
$CI->load->model('sale');
// GET total_due
// $paid = $CI->sale->get_sale_due_paid($person->sale_id) + $person->first_payment;
// $total_due = $CI->sale->get_sale_due($person->sale_id)->total_due;
// $grand_total = $CI->past_due->get_sale_total($person->sale_id);
// $total_due = $grand_total - $paid;
// if ($total_due>0) {
	
	$table_data_row='<tr class="pay_row">';
	$table_data_row.="<td width='5%'><input class='sel_pay' d='".$person->total_due."' t='".$person->paid."' s='".$person->sale_id."' type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
	$table_data_row.='<td width="10%">'.$person->invoiceid.'</td>';
	$table_data_row.='<td width="10%">'.date('d-m-Y',strtotime($person->sale_time)).'</td>';
	$table_data_row.='<td width="20%">'.$person->last_name." ".$person->first_name.$com.'</td>';
	$table_data_row.='<td width="20%">'.number_format($person->sale_total,2).'</td>';
	$table_data_row.='<td width="20%">'.number_format($person->paid,2).'</td>';
	$table_data_row.='<td width="30%">'.number_format($person->total_due,2).'</td>';
	// $table_data_row.='<td width="5%">'.anchor($controller_name."/view/$person->sale_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';	
	// $table_data_row.='<td width="5%">'.anchor($controller_name."/detail/$person->sale_id/width:$width",$CI->lang->line('common_detail'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_detail'))).'</td>';
	$table_data_row.='<td width="5%"><a href="'.site_url('past_dues/view_invoice/'.$person->sale_id).'" target="_blank">Invoice</a></td>';
	$table_data_row.='</tr>';
// }

return $table_data_row;
}


/*
Gets the html table to manage khmername.
*/
function get_khmernames_manage_table( $khmernames, $controller,$from )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />','ID','Photo',
	$CI->lang->line('khmernames_khmername_name'),$CI->lang->line('khmernames_english_name'),
	$CI->lang->line('khmernames_card_description'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	foreach($headers as $header)
	{
		$table.="<th>$header</th>";
	}
	$table.='</tr></thead><tbody>';
	$table.=get_khmernames_manage_table_data_rows( $khmernames, $controller ,$from);
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the khmername.
*/
function get_khmernames_manage_table_data_rows( $khmernames, $controller ,$from)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($khmernames->result() as $khmername)
	{
		$table_data_rows.=get_khmername_data_row( $khmername, $controller ,$from);
	}
	
	if($khmernames->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".$CI->lang->line('khmernames_no_khmernames_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_khmername_data_row($khmername,$controller,$from)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$img_path=base_url('assets/site/image/no_img.png');
	$img=$CI->Khmername->getimage($khmername->khmername_id);
	    if ($img->khmername_id==$khmername->khmername_id) {
            if(file_exists(FCPATH."/uploads/khmername/thumb/".$img->imagename)){
                $img_path=base_url("/uploads/khmername/thumb/".$img->imagename);
            }
        }

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='khmername_$khmername->khmername_id' value='".$khmername->khmername_id."'/></td>";
	$table_data_row.='<td width="3%">'.$khmername->khmername_id.'</td>';
	$table_data_row.='<td width="3%"><img style="width:80px;" src="'.$img_path.'"></td>';
	$table_data_row.='<td width="15%">'.$khmername->khmername_name.'</td>';
	$table_data_row.='<td width="15%">'.$khmername->english_name.'</td>';
	$table_data_row.='<td width="20%">'.$khmername->description.'</td>';
	if ($from!='') {
		
		if ($from=='item') {
			$table_data_row.='<td width="5%">
									<div style="cursor:pointer;" class="btn_select_name" rel='.$khmername->khmername_id.'>
										<div class="small_button">
											<span name="btnselectname" id="btnselectname" style="font-size:14px;">Select</span>
										</div>
									</div>
								</td>';	
			$table_data_row.='</tr>';
		}else{
			$url_link = site_url("sales/select_name/$khmername->khmername_id");
			$table_data_row.='<td width="5%">
								<a href="'.$url_link.'">
								<div class="small_button">
									<span name="btnselectname" id="btnselectname" style="font-size:14px;">Select</span>
								</div>
								</a></td>';	
			$table_data_row.='</tr>';
		}
		
	}else{
		$table_data_row.='<td width="5%">'.anchor($controller_name."/view/$khmername->khmername_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).'</td>';		

	}
	
	return $table_data_row;
}




?>