
<script type="text/javascript">
// +++++++++++++++++++++ Image upload++++++++++++++++++
   function PreviewImage(event) {
        var uppreview=$(event.target).parent().parent().find('#imgpreview');
        var oFReader = new FileReader();
        oFReader.readAsDataURL($(event.target)[0].files[0]);

        oFReader.onload = function (oFREvent) {
            uppreview.attr('src',oFREvent.target.result);
            // uppreview.style.backgroundImage = "none";
        };
        $(event.target).parent().parent().find('.remove_img').removeClass('hide');
        $(event.target).removeClass('bl');
        var slideid=$(event.target).parent().parent().find('.remove_img').attr('rel');
        var del=$('#deleteimg').val()
        $('#deleteimg').val(del+','+slideid);
        addnewimg();
    };
    function addnewimg(){
        var exst=false;
        var ma=0;
        $('.uploadimg').each(function(){
            var ma_o=$(this).parent().parent().find('#order').val();
            var f = $(this)[0].files[0];
            // alert(f);
            if(ma<ma_o)
                ma=ma_o;
            if(f==undefined && $(this).hasClass('bl')){
                exst=true;
            }
        })
        // if(exst==true)
        //     $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
        //                                 "<div class='img' style='width:130px; float:left'> "+ 
        //                                     "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
        //                                     "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
        //                                     "<input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
        //                                     "<input type='text' value='' class='removeimgid hide' />"+
        //                                 "</div>"+
        //                             "</div>"+
        //                             "<p style='clear:both;'></p>");
    }
       
    function choosimg(event){
        $(event.target).parent().parent().find('#uploadImage').click();
    }
	// +++++++++++++++++++++++++++++remove upload pic+++++++++++++++++++ 
    function remove_img(event){
        var picid=$(event.target).attr('rel');
        $(event.target).parent().parent().remove();
        var delete_img=$('#deleteimg').val();
        if(picid!='')
            $('#deleteimg').val(delete_img+','+picid);

             $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
                                        "<div class='img' style='width:130px; float:left'> "+ 
                                            "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
                                            "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
                                            "<input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
                                            "<input type='text' value='' class='removeimgid hide' />"+
                                        "</div>"+
                                    "</div>"+
                                    "<p style='clear:both;'></p>");
    }


   // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
</script>
<style type="text/css">
	#img_row{padding: 3px 0px !important}
    .hide{display: none !important;}
    .img_row>div{
        width: 45%;
        float: left;
    }
    .remove_img{
        margin-bottom:-23px;position: relative; margin-left:-6px !important;
        width: 24px;
    }
    .top_action_button img{width: 28px; margin-top: 5px;}
    .saveloading{width:35px;}
    .remove_img:hover{
        cursor: pointer;
    }
    a{
        cursor: pointer;
    }
    .previewupl:hover{
        cursor: pointer;
    }
</style>
<?php
echo form_open_multipart('customers/save/'.$person_info->person_id,array('id'=>'customer_form','Type'=>'POST'));
// echo form_open('customers/save/'.$person_info->person_id,array('id'=>'customer_form'));
?>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo $this->lang->line("customers_basic_information"); ?></legend>
<?php $this->load->view("people/form_basic_info"); ?>
<!--<div class="field_row clearfix">	-->
<?php form_label($this->lang->line('customers_account_number').':', 'account_number'); ?>
	<!--<div class='form_field'>-->
	<?php form_input(array(
		'name'=>'account_number',
		'id'=>'account_number',
		'value'=>$person_info->account_number)
	);?>
<!--	</div>
</div>-->

<!--<div class="field_row clearfix">	-->
<?php form_label($this->lang->line('customers_taxable').':', 'taxable'); ?>
	<!--<div class='form_field'>-->
	<?php form_checkbox('taxable', '1', $person_info->taxable == '' ? TRUE : (boolean)$person_info->taxable);?>
<!--	</div>
</div>-->

<!-- Upload Image -->
<div id='blog_img'> 
                <input type='text' name='deleteimg' style='display:none' id='deleteimg'/>
                <?PHP

                $ma=1;
                $img_path=base_url('assets/site/image/no_img.png');
                if($person_info->person_id!=''){
                   $img=$this->Customer->getimage($person_info->person_id);
                    if ($img->customer_id==$person_info->person_id) {
                            if($ma<$img->first_image)
                                $ma=$img->first_image;
                            if(file_exists(FCPATH."/uploads/thumb/".$img->imagename)){
                                $img_paths=base_url("/uploads/thumb/".$img->imagename);
                        ?>
                                <div style='width:400px;' id='img_row'>
                                    <div class="img" style="width:130px; float:left"> 
                                        <img onclick='remove_img(event);' rel='<?PHP echo $img->image_id ?>' class='remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                                        <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_paths ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                                        <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg ext' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                                        <input type='text' class='hide'  name='updimg[]' id='updimg'/>
                                    </div>
                                    
                                </div>
                                <p style='clear:both;'></p>  
                        <?PHP
                            }else{
                                $this->Customer->deleteimg($img->image_id);
                            }
                    }else{
                    ?>
                        <div style='width:400px;' id='img_row'>
                        <div class="img" style="width:130px; float:left"> 
                            <img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                            <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                            <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                        </div>
                       
                        </div> 
                        <p style='clear:both;'></p>
                    <?php
                    }
                }else{

                ?>

                    <div style='width:400px;' id='img_row'>
                    <div class="img" style="width:130px; float:left"> 
                        <img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                        <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                        <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                    </div>
                   
                    </div> 
                    <p style='clear:both;'></p>
                <?php
            }
                ?>
                
            </div>
<!--  close Image-->

<?php
echo form_submit(array(
    'name'=>'submit_real',
    'id'=>'submit_real',
    'value'=>$this->lang->line('common_submit'),
    'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

// validation and submit handling
$(document).ready(function()
{
	$('#customer_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
    		email: "email"
   		},
		messages: 
		{
     		first_name: "<?php echo $this->lang->line('common_first_name_required'); ?>",
     		last_name: "<?php echo $this->lang->line('common_last_name_required'); ?>",
     		email: "<?php echo $this->lang->line('common_email_invalid_format'); ?>"
		}
	});
});
</script>