<style type="text/css">
	#pic{
		width: 80px;
	}
	.search_left{
		width: 45%;
		float: left;
	}
	.search_right{
		width: 45%;
		float: left;
	}
</style>

<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	
</div>

<div id="table_action_header">
	<div style="margin-top:-3px;margin-left:25px;">
		<div class="search_left">
			<label>Search</label>	

			<input tabindex="0" type="text" name ='isearch' id='isearch'/>
		</div>
		<div class="search_right">
			<label>Customer ID</label>
			<input tabindex="0" type="text" name="cus_id" class="cus_id" onkeypress='return isNumberKey(event)'>
			<button id="btn_select_id">Select ID</button>
		</div>
		
		
	</div>
	
</div>
<div id="table_holder">
	<!-- TOP PAGINATION -->
	
	<!-- END TOP PAGINATION -->

	<?php echo $manage_table; ?> 

	<!-- BOTTOM PAGINATION -->
	
	<!-- END BOTTOM PAGINATION -->
</div>
<script type="text/javascript">
$('#isearch').keyup(function(){
	var	s = $('#isearch').val();

	$.post("<?php echo site_url('customers/search_cus_sale') ?>",
		{
			search:s
		},
		function(data){
			$('#table_holder').html(data.manage_table);
		},
		'Json'
		);
});




$(document).ready(function() 
{ 



    init_table_sorting();

    
	setTimeout(function() { $('#isearch').focus() }, 200);

}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{ 
			sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
				5: { sorter: false} 
			} 

		}); 
	}
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$('.cus_id').keypress(function(e){
	var code = (e.keyCode ? e.keyCode : e.which);
	if(code == 13) { //Enter keycode
		// alert(e.val());
		// console.log($(e.target).val());

		do_select_cus();

	    
	}
});

$('#btn_select_id').click(function(){
	do_select_cus();
});
function do_select_cus(){
	var cid = $('.cus_id').val();
	if (cid!='' && cid!=0) {
    	location.href = "<?php echo site_url('sales/select_customer').'/' ?>"+cid;
    }else{
    	alert("Invalid ID");
    }
}

</script>