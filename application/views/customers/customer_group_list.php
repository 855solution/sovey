<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">

<div class="row" style="margin-top:15px;">
	<div class="col-md-12">
		<div class="alert alert-success al_suc" style="display: none;">Save Success</div>

		<div class="col-md-3">
			<label>Group Name :</label>
		</div>
		<div class="col-md-5">
			<input type="hidden" name="" class="group_id" value="">
			<input type="text" name="group_name" class="form-control group_name">
			<div class="alert alert-danger al_gname" style="display: none;">Group name already exist</div>
			<input type="hidden" name="is_dup" class="is_dup" value="0">
		</div>
		<div class="col-md-4">
			<button class="btn btn-primary btn_save">Save</button>
			<button class="btn btn-danger btn_cancel" style="display: none;">Cancel</button>
		</div>

	</div>
</div>
<div class="row" style="margin-top: 15px">
	<div class="col-md-12">
		<label>List Customer Group</label>
	</div>
	<div class="col-md-12">
		<table class="table table-bordered cg_table">
			<thead>
				<th>No</th>
				<th>Group Name</th>
				<th>Actions</th>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootbox/bootbox.min.js') ?>"></script>


<script type="text/javascript">
	$(document).ready(function(){
		get_all_data();
	});

	$('.group_name').keyup(function(){
		check_name();
	})

	function check_name(){
		var name = $('.group_name').val();
		if (name!='' && name.replace(/\s/g, "")!='') {
			$.ajax({
				url:"<?php echo site_url('customers/check_group_name') ?>",
				type:'POST',
				dataType:'Json',
				data:{
					name:name
				},
				success:function(d){
						// console.log(d);

					if (d==1) {

						$('.is_dup').val(1);
						$('.al_gname').slideDown('fast');
					}else{
						$('.is_dup').val(0);
						$('.al_gname').slideUp('fast');

					}
				}
			});
		}else{
			$('.al_gname').slideUp('fast');

		}
		
	}

	$('.btn_save').click(function(){
		do_save();
	})

	function do_save(){
		var cg_name = $('.group_name').val();
		var is_dup = $('.is_dup').val();
		var cg_id = $('.group_id').val();

		if (is_dup!=1 && cg_name!='') {
			$.ajax({
				url:"<?php echo site_url('customers/save_cg') ?>",
				type:'POST',
				dataType:'Json',
				data:{
					id:cg_id,
					name:cg_name
				},
				success:function(d){

					if (d==1) {

						// $('.is_dup').val(1);
						$('.al_suc').slideDown('fast');
						$('.group_name').val('');
						// check_name();
						cancel_edit();
						get_all_data();
					}else{
						// $('.is_dup').val(0);
						$('.al_suc').slideUp('fast');

					}
				}
			});
		}
	}

	function get_all_data(){
		$.ajax({
			url:"<?php echo site_url('customers/get_all_cg') ?>",
			type:'POST',
			dataType:'Json',
			data:{
				name:'q'
			},
			success:function(d){

				$('.cg_table tbody').html(d.tb);

				// $('.cg_table').DataTable(); //usable
			}
		});
	}


	function action_del(id){
		bootbox.confirm("Confirm Delete?",function(result){
			if (result===true) {
				$.ajax({
					url:"<?php echo site_url('customers/del_cg') ?>",
					type:'POST',
					dataType:'Json',
					data:{
						id:id
					},
					success:function(d){

						// $('.cg_table tbody').html(d.tb);

						// $('.cg_table').DataTable(); //usable

						if (d==1) {

							// $('.is_dup').val(1);
							// $('.al_suc').slideDown('fast');
							$('.group_name').val('');
							// check_name();
							get_all_data();
						}else{
							// $('.is_dup').val(0);
							// $('.al_suc').slideUp('fast');

						}
					}
				});
			}
		})
	}

	function action_edit(id){
		$.ajax({
			url:"<?php echo site_url('customers/get_cg') ?>",
			type:'POST',
			dataType:'Json',
			data:{
				id:id
			},
			success:function(d){

				// $('.cg_table tbody').html(d.tb);

				// $('.cg_table').DataTable(); //usable

				if (d) {

					// $('.is_dup').val(1);
					// $('.al_suc').slideDown('fast');
					$('.btn_cancel').slideDown('fast');
					$('.group_id').val(d.cg_id);
					$('.group_name').val(d.cg_name);
					// check_name();
					// get_all_data();
				}
			}
		});
	}
	$('.btn_cancel').click(function(){
		cancel_edit();
	})
	function cancel_edit(){
		$('.btn_cancel').slideUp('fast');
		$('.group_id').val('');
		$('.group_name').val('');
	}

</script>




