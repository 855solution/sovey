<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
$img_path=base_url('assets/site/image/no_img.png');
  
    if(file_exists(FCPATH."assets/upload/location/$location_info->location_id.jpg")){
      $img_path=base_url("assets/upload/location/$location_info->location_id.jpg");
    }
echo form_open('locations/save/'.$location_info->location_id,array('id'=>'location_form'));
?>
<fieldset id="location_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("locations_basic_information"); ?></legend>
<!--
<table cellpadding="6" cellspacing="6">
	<tr>
    	<td align="right"><?php //echo form_label($this->lang->line('locations_location_name').':', 'name',array('class'=>'required wide')); ?></td>
        <td align="left">	
            <div class='form_field'>
                <?php //echo form_input(array(
                        //'name'=>'location_name',
                        //'id'=>'location_name',
                        //'value'=>$location_info->location_name)
                //);?>
            </div>
		</td>
    </tr>
    <tr>
    	<td align="right"><?php //echo form_label($this->lang->line('locations_card_description').':', 'name'); ?></td>
        <td align="left">
            <div class='form_field'>
            <?php //echo form_input(array(
                //'name'=>'description',
               // 'id'=>'description',
               // 'value'=>$location_info->description)
           // );?>
            </div>
        </td>
    </tr>
    <tr>
    	<td></td>
    	<td align="left">
			<?php
               // echo form_submit(array(
                   // 'name'=>'submit',
                   // 'id'=>'submit',
                   // 'value'=>$this->lang->line('common_submit'),
                   // 'class'=>'submit_button float_left')
               // );
            ?>
        </td>
    </tr>
</table>-->
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('locations_location_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'location_name',
		'id'=>'location_name',
		'value'=>$location_info->location_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('locations_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$location_info->description)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Max Quantity:', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'max_qty',
		'id'=>'max_qty',
		'value'=>$location_info->max_qty)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label("image".':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	 	<img  onclick="$('#uploadImage').click();" src="<?php echo $img_path?>" id="uploadPreview" style='width:350px;border:solid 1px #CCCCCC; padding:3px;'>
        <input id="uploadImage" rel='uploadPreview' type="file" name="userfile" onchange="PreviewImage(event);" style="visibility:hidden; display:none" />
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function PreviewImage(event) {
    var uppreview=$(event.target).attr('rel');
    //alert(uppreview);
    var upimage=$(event.target).attr('id');
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById(upimage).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById(uppreview).src = oFREvent.target.result;
         document.getElementById(uppreview).style.backgroundImage = "none";
    };
}

//validation and submit handling
$(document).ready(function()
{
	$('#location_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_location_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			location_name:"required"
			//,	
			//location_name:
			//{
				//required:true,
				//name:true
			//}
   		},
		messages:
		{
			location_name:"<?php echo $this->lang->line('locations_name_required'); ?>",	
			//location_name:
			//{
				//required:"<?php //echo $this->lang->line('locations_name_required'); ?>",
				//name:"<?php //echo $this->lang->line('locations_name'); ?>"
			//}
		}
	});
});
</script>