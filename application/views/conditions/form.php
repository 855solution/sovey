<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('conditions/save/'.$condition_info->condition_id,array('id'=>'condition_form'));
?>
<fieldset id="condition_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("conditions_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('conditions_condition_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'condition_name',
		'id'=>'condition_name',
		'value'=>$condition_info->condition_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('conditions_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$condition_info->description)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#condition_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_condition_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			condition_name:"required"
   		},
		messages:
		{
			condition_name:"<?php echo $this->lang->line('conditions_name_required'); ?>",	
		}
	});
});
</script>