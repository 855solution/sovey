
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}

	.name_er{
		color: red;
	}
</style>
<?php $er = $this->session->flashdata('er') ?>
<?php if ($er): ?>
	<div class="alert alert-danger">
		<p><?php echo $er ?></p>
	</div>
<?php endif ?>

<div class="panel panel-default">
  <!-- Default panel contents -->
  	<div class="panel-heading">
  		<div class="row">
  			<div class="col-md-6 pull-left">
  				<h4>List Conditions</h4>
	  		</div>
	  		<?php if ($can_add==1): ?>
	  			<div class="col-md-6 pull-right">
		  			<button class="btn btn-primary pull-right btn_add" data-toggle='modal' data-target='#myModal'>New</button>
		  		</div>
	  		<?php endif ?>
	  		
	  		

  		</div>
  		
  	</div>
  	<div class="panel-body">
    	<div class="col-md-12">
			<table class="table table-bordered table-striped tdata">
				<thead>
					<th>Nº</th>
					<th>Condition Name</th>
					<th>Description</th>
					<th>Actions</th>
					<th></th>
				</thead>
				<tbody>
					<?php echo $table ?>
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>

  
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Condition</h4>
      </div>
      <form action="<?php echo site_url('conditions/save') ?>" method='post'>
      	
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12 form-group">
	        		<div class="col-md-6">
	        			<label>Condition Name :</label>
	        		</div>
	        		<div class="col-md-6">
	        			<input type="text" name="condition_name" class="form-control condition_name" value="" required="">
	        			<p class="name_er hide">Name Exist!</p>

	        		</div>
	        	</div>
	        	<div class="col-md-12 form-group">
	        		<div class="col-md-6">
	        			<label>Description :</label>
	        		</div>
	        		<div class="col-md-6">
	        			<textarea name="description" class="form-control description"></textarea>
	        		</div>
	        	</div>
	        </div>
      	</div>

      	<div class="modal-footer">
      		<input type="hidden" value="" class="condition_id" name="condition_id">
	        <input type="submit" class="btn btn-primary" value="Save">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>
      </form>

    </div>

  </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootbox/bootbox.min.js') ?>"></script>


<script type="text/javascript">
	$('.tdata').DataTable();

	$('.condition_name').keyup(function(){
		var n = $('.condition_name').val();
		var id = $('.condition_id').val();
		$.ajax({
			url:"<?php echo site_url('conditions/check_name') ?>",
			type:'post',
			dataType:'Json',
			data:{
				name:n,
				id:id
			},success:function(re){
				if (re==1) {
					$('.name_er').removeClass('hide');
					$('.condition_name').css('color','red');
				}else{
					$('.name_er').addClass('hide');
					$('.condition_name').css('color','black');
				}
			}
		});
	})

	$('.tdata tbody').on('click','.btn_del',function(){
	
		var id = $(this).attr('f');
		var text = $(this).closest('tr').find('td:eq(1)').text();

		bootbox.confirm('សម្រេចលុប '+text+' ?',function(result){
			if (result===true) {
				location.href="<?php echo site_url('conditions/delete') ?>"+"/"+id;
			};
		});
	})
	$('.tdata tbody').on('click','.btn_edit',function(){
	
		var d = $(this).attr('d');
		var n = $(this).attr('n');
		var id = $(this).attr('f');
		$('.condition_name').css('color','black');
		$('.name_er').addClass('hide');

		$('.condition_id').val(id);
		$('.condition_name').val(n);
		$('.description').text(d);
		$('.modal-title').text('Edit Partplacement');
	});
	$('.btn_add').click(function(){
		$('.description').text('');
		$('.condition_name').val('');
		$('.condition_id').val('');
		$('.name_er').addClass('hide');
		$('.modal-title').text('New Partplacement');
	});
</script>