   <script type="text/javascript">
   $(document).ready(function(){	
   
	 $("div .value_selected").hide();   
	 
	   
	 var con=$(".condition_id option:selected").text();
	 var condition=new String(con.toLowerCase()); 
	if(condition.replace(/\s/g, "")=="used"){
		$("div .value_selected").show();   
		}

	
   $('.condition_id').change(function (){
	   var str = "";
	   var compare="used";
       $(".condition_id option:selected").each(function () {
             str += $(this).text() + " ";
           });
       var firstString = new String(str.toLowerCase());
      
       
   		if(firstString.replace(/\s/g, "")=="used"){
   		 //$("div .value_selected").text(str);
   			$("div .value_selected").show();
   	   		}else{
			 $("div .value_selected").hide();
   	   	   		}
  		
	   });

   
	$('.imageDelete').click(function(event){
		var id= $(this).attr('title');
		var msg=false;
		msg=confirm("Are you sure, delete this picture?");
		//alert(msg);
		if(msg==true){
			$(this).prev().andSelf().remove();	
			 $.post('index.php/items/deleteImage', {'imageid':id},function(result)
				        {
			        
				        });	
			}
		});

   
   
	$('.grade_id').change(function (){
		var unitpriceA=$('.unitpriceA').val();	
		var costpriceA=$('.costpriceA').val();

		var unitpriceB=$('.unitpriceB').val();	
		var costpriceB=$('.costpriceB').val();

		var unitpriceC=$('.unitpriceC').val();	
		var costpriceC=$('.costpriceC').val();


		var str = "";
		  $(".grade_id option:selected").each(function () {
	             str += $(this).text() + " ";
	           });
          
		var strValue = new String(str.toLowerCase());  
		

		if(strValue.replace(/\s/g, "")=="a"){		
			$("#cost_price").val(costpriceA);
			$("#unit_price").val(unitpriceA);
			}
		
		if(strValue.replace(/\s/g, "")=="b"){		
			$("#cost_price").val(costpriceB);
			$("#unit_price").val(unitpriceB);
			}
		
		if(strValue.replace(/\s/g, "")=="c"){		
			$("#cost_price").val(costpriceC);
			$("#unit_price").val(unitpriceC);
			}
		
		});
		//$("#quantity").val(1);
	   
		   });
   </script>
  <input type="hidden" name="unitpriceA" class="unitpriceA" value="<?php echo $unitpriceA;?>"/>
  <input type="hidden" name="costpriceA" class="costpriceA" value="<?php echo $costpriceA;?>"/>
  <br>
  <input type="hidden" name="unitpriceB" class="unitpriceB" value="<?php echo $unitpriceB;?>"/>
  <input type="hidden" name="costpriceB" class="costpriceB" value="<?php echo $costpriceB;?>"/>
  <br>
  <input type="hidden" name="unitpriceC" class="unitpriceC" value="<?php echo $unitpriceC;?>"/>
  <input type="hidden" name="costpriceC" class="costpriceC" value="<?php echo $costpriceC;?>"/>
	

	<tr>
		<td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_name').':', 'name',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'name',
                    'id'=>'name',
                    'size'=>'25',
                    'value'=>$item_info->name)
                );?>
                </div>
            </div>
		</td>
	</tr>
	
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_category').':', 'category',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('category_id', $categorys, $selected_category,'id="category_id"');?>
                </div>
            </div>
		</td>
	</tr>    
	
    <tr>
	    <td colspan="2">
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_fit').':', 'fit',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'fit',
                    'id'=>'fit',
                    'size'=>'25',
                    'value'=>$item_info->fit)
                );?>
                </div>
            </div>
	    </td>
	</tr>
	
    <tr>
        <td>
            <!--<div class="field_row clearfix">
            <?php //echo form_label($this->lang->line('items_model').':', 'model',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php //echo form_dropdown('model_id', $models, $selected_model);?>
                </div>
            </div>-->
            <script language="JavaScript" type="text/javascript">
				$(function()
				{
					$('#make_id').chainSelect('#model_id','<?php echo site_url('items/combobox') ?>',
					{ 
						before:function (target) //before request hide the target combobox and display the loading message
						{ 
							$("#loading").css("display","block");
							$(target).css("display","none");
						},
						after:function (target) //after request show the target combobox and hide the loading message
						{ 
							$("#loading").css("display","none");
							$(target).css("display","inline");
						}
					});
				});
            </script>
	
			<div class="field_row clearfix">
				<?php echo form_label($this->lang->line('items_make').':', 'make',array('class'=>'wide')); ?>
				<div class='form_field'>
				<?php 
					$js = 'id="make_id"';
					echo form_dropdown('make_id', $makes, $selected_make,$js);
				?>
					
				</div>
			</div>
        </td>
        <td>
			<?php echo form_label($this->lang->line('items_model').':', 'model',array('class'=>'wide')); ?>
            <?php 
                $model_js = 'id="model_id"';
                echo form_dropdown('model_id', $models, $selected_model,$model_js);
            ?>
		</td>
	</tr>
	
	<tr>        
        <td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_year').':', 'year',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php 
                    define (MINY,1980);
                    $year_temp = $item_info->year;
                ?>
                <select name="year" id="year">
						<option value="<?php echo $year_temp; ?>"><?php echo $year_temp; ?></option>
                    <?php 
						for($j=MINY;$j<=date('Y');$j++){
							echo '<option value="'.$j.'">'.$j.'</option>';
						}
                    ?>                    
                </select>
                </div>
            </div>
        </td>
        <td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_color').':', 'color',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('color_id', $colors, $selected_color,'class="color_id"');?>
                </div>
            </div>
		</td>

	</tr>
	
	
    <tr>        
        <td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_condition').':', 'condition',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('condition_id', $conditions, $selected_condition,'class="condition_id"');?>
                </div>
                 
            </div>
            <br>
            <div class='value_selected'>
            <div class="field_row clearfix">
            <?php echo form_label('Grade'); ?>
                <div class='form_field'>

                 <?php echo form_dropdown('grade_id', $grade, $selected_grade,'class="grade_id"');?>
                 
                 </div>
                </div>
                 
            </div>
            
            
		</td>
		
        <td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_part_placement').':', 'partplacement',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('partplacement_id', $partplacements, $selected_partplacement,'class="partplacement_id"');?>
                </div>
            </div>
		</td>
	</tr>
	
	
	<tr>
		<td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_branch').':', 'branch',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('branch_id', $branchs, $selected_branch,'class="branch_id"');?>
                </div>
            </div>
        </td>
        <td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_location').':', 'location',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('location_id', $locations, $selected_location,'class="location_id"');?>
                </div>
            </div>
            <div class="field_row clearfix">
            <?php form_label($this->lang->line('items_supplier').':', 'supplier',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php form_dropdown('supplier_id', $suppliers, $selected_supplier,'class="supplier_id"');?>
                </div>
            </div>
		</td>
	</tr>  
	
	           
    <tr>
		<td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_cost_price').':', 'cost_price',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'cost_price',
                    'size'=>'12',
                    'id'=>'cost_price',
                    'value'=>$item_info->cost_price)
                );?>
                </div>
            </div>
		</td>
        <td>			       
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_unit_price').':', 'unit_price',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'unit_price',
                    'size'=>'12',
                    'id'=>'unit_price',
                    'value'=>$item_info->unit_price)
                );?>
                </div>
            </div>
		</td>
	</tr>
	
	
    <tr>
    
    <?php 
    
    $quantity='';
    if($item_info->quantity=='' || $item_info->quantity==Null){
    	$quantity=1;	
    }else{
    	$quantity=$item_info->quantity;
    }
    
    ?>
    	<td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_quantity').':', 'quantity',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'quantity',
                    'id'=>'quantity',
					'size'=>'12',
                    'value'=>$quantity
                )
                );?>
                </div>
            </div>
        </td>
    </tr>
    
    
	<!--<tr>
		<td colspan="3">
            <div class="field_row clearfix">-->
            <?php form_label($this->lang->line('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
                <!--<div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_names[]',
                    'id'=>'tax_name_1',
                    'size'=>'12',
                    'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'))
                );?>
                <!--</div>
                
                <div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_percents[]',
                    'id'=>'tax_percent_name_1',
                    'size'=>'3',
                    'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate)
                );?><!--
                %
                </div>
            </div>
		</td>
	</tr>
	<tr>
    	<td colspan="3">       
            <div class="field_row clearfix">-->
            <?php form_label($this->lang->line('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
                <!--<div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_names[]',
                    'id'=>'tax_name_2',
                    'size'=>'12',
                    'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'))
                );?>
                <!--</div>
                <div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_percents[]',
                    'id'=>'tax_percent_name_2',
                    'size'=>'3',
                    'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate)
                );?>
                <!--%
                </div>
            </div>
		</td>
	</tr>      
	
	<tr>
    	<td colspan="2">
            <div class="field_row clearfix">
            <?php form_label($this->lang->line('items_reorder_level').':', 'reorder_level',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php form_input(array(
                    'name'=>'reorder_level',
                    'id'=>'reorder_level',
					'size'=>'12',
                    'value'=>$item_info->reorder_level)
                );?>
                </div>
            </div>
		</td>
	</tr>
	<tr>
    	<td colspan="3">
            <div class="field_row clearfix">	
            <?php form_label($this->lang->line('items_location').':', 'location',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php form_input(array(
                    'name'=>'location',
                    'id'=>'location',
					'size'=>'12',
                    'value'=>$item_info->location)
                );?>
                </div>
            </div>
		</td>
    </tr>-->
    
	<tr>
    	<td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_description').':', 'description',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_textarea(array(
                    'name'=>'description',
                    'id'=>'description',
                    'value'=>$item_info->description,
                    'rows'=>'3',
                    'cols'=>'25')
                );?>
                </div>
            </div>
		</td>
    </tr>
    
    <!-- upload image -->

    <tr>
    	<td colspan="2">
     
            <div class="field_row clearfix">
            <?php echo form_label("Upload Image".':'); ?>
                <div class='form_field'>
                <?php foreach ($image_name as $key){?>
                	<img width="200" height="200" src="<?php echo base_url()?>uploads/<?php echo $key['imagename'];?>" class="imageContain">
                	<?php $imageid=$key['image_id'];?>
                	<a style="cursor:pointer" class="imageDelete" title="<?php echo $imageid;?>">Delete</a>
                	<br>
               <?php }?>
            		<input type="file" name="path[]" size="30" id="path[]"> 
            		 <div id="pop"></div>  
                </div>
            </div>
        <p>
                    	<a href="javascript:addElement();" >Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
                     	<a href="javascript:removeElement();" >Remove</a>
                    </p><br />    
		</td>
    </tr>
    

    <!-- Upload images -->
       
    
    
	<tr>
    	<td>
            <div class="field_row clearfix">
                <div class='form_field'>
                <?php echo form_checkbox(array(
                    'name'=>'allow_alt_description',
                    'id'=>'allow_alt_description',
                    'value'=>1,
                    'checked'=>($item_info->allow_alt_description)? 1  :0)
                );?>
                </div>
                <?php echo form_label($this->lang->line('items_allow_alt_desciption').'', 'allow_alt_description',array('class'=>'wide')); ?>
            </div>
		</td>
        <td>            
            <div class="field_row clearfix">
                <div class='form_field'>
                <?php echo form_checkbox(array(
                    'name'=>'is_serialized',
                    'id'=>'is_serialized',
                    'value'=>1,
                    'checked'=>($item_info->is_serialized)? 1 : 0)
                );?>
                </div>
                <?php echo form_label($this->lang->line('items_is_serialized').'', 'is_serialized',array('class'=>'wide')); ?>
            </div>
            
            <!-- Barcode generation Checkbox -->
            <div class="field_row clearfix">
                <div class='form_field'>
                <?php echo form_checkbox(array(
                    'name'=>'generate_barcode',
                    'id'=>'generate_barcode',
                    'value'=>1,
                    //'checked'=>($item_info->is_serialized)? 1 : 0
                )
                );?>
                </div>
                <?php echo form_label('Generate Barcode'); ?>
            </div>
            
            
		</td>
	</tr>
	
	
    <tr>
    	<td colspan="2">
			<?php
            echo form_submit(array(
                'name'=>'submit',
                'id'=>'submit',
                'value'=>$this->lang->line('common_submit'),
                'class'=>'submit_button float_left',      
            )
            );
            ?>
	</td>
    </tr>	
  