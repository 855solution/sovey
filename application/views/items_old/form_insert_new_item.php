

<?php $this->load->view("partial/header"); ?>

<script type="text/javascript">
$(document).ready(function()
{
	
	$("#subform_container").hide();
	$('#item_number').change(function(){
		var itemvalue=$('#item_number').val();
		$("#subform_container").show();

		
var itemvalue;

$.getJSON('index.php/items/go', {itemvalue:itemvalue}, function(data) {
	
    /* data will hold the php array as a javascript object */
    $.each(data, function(key, val) {
    	
    	 //alert(key+' : '+ val.imagename);
    	 $('#category').val(val.category);
    	 $('#name').val(val.name);
    	 $('#fit').val(val.fit);
    	 $('#category_id').val(val.category_id);
		 $('#year').val(val.year);
    	 
    	 $('#make_id').val(val.make_id);
    	 $('#model_id').val(val.model_id);
    	 $('.color_id').val(val.color_id);
    	 $('.condition_id').val(val.condition_id);
    	 
    	 $('.grade_id').val(val.grad_id);
    	 $('.partplacement_id').val(val.partplacement_id);
    	 $('.branch_id').val(val.branch_id);

    	 $('.location_id').val(val.location_id);
    	 $('#cost_price').val(val.cost_price);
    	 $('#unit_price').val(val.unit_price);
		
    	 if(val.quantity=='' || val.quantity==null){
    		 val.quantity=1;
        	 }
    	 $('#quantity').val(val.quantity);
    	 
    	 $('#description').val(val.description);
    	 //$('.imageTemp').val(val.imagename);

    	 var con=$(".condition_id option:selected").text();
    	 var condition=new String(con.toLowerCase()); 
    	if(condition.replace(/\s/g, "")=="used"){
    		$("div .value_selected").show();   
    		}

    	
       $('.condition_id').change(function (){
    	   var str = "";
    	   var compare="used";
           $(".condition_id option:selected").each(function () {
                 str += $(this).text() + " ";
               });
           var firstString = new String(str.toLowerCase());
          
           
       		if(firstString.replace(/\s/g, "")=="used"){
       		 //$("div .value_selected").text(str);
       			$("div .value_selected").show();
       	   		}else{
    			 $("div .value_selected").hide();
       	   	   		}
      		
    	   });
    	 
    	
    	 
          //$('#subform_container').append('<ul><li id="' + key + '">' + val.barcode +'</li></ul>');
         
    });
});
		});
	
		
    //init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo $this->lang->line('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','index.php/items/generate_barcodes/'+selected.join(':'));
    });

    $("#low_inventory").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#is_serialized").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#no_description").click(function()
    {
    	$('#items_filter_form').submit();
    });





});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		var selected_item_ids=get_selected_values();
		for(k=0;k<selected_item_ids.length;k++)
		{
			update_row(selected_item_ids[k],'<?php echo site_url("$controller_name/get_row")?>');
		}
		set_feedback(response.message,'success_message',false);
	}
}

function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="block";
        }
}


/*---------------Upload Image-----------------*/


var intTextBox=0;
function addElement(){
	intTextBox = intTextBox + 1;
	var contentID = document.getElementById('pop');
	var newTBDiv = document.createElement('div');
	newTBDiv.setAttribute('id','strText'+intTextBox);
	newTBDiv.innerHTML ="<input type='file' id='" + intTextBox + "' size='25' name='path[]' style='margin-top:15px;'/>";
	contentID.appendChild(newTBDiv);
}
function removeElement(){
	if(intTextBox != 0){
		var contentID = document.getElementById('pop');
		contentID.removeChild(document.getElementById('strText'+intTextBox));
		intTextBox = intTextBox-1;
	}
}

/*----------------------------------------------------*/



</script>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box"></ul>
<?php

echo form_open_multipart('items/save',array('id'=>'item_form'));
?>

<fieldset id="item_basic_info">
<legend><?php echo $this->lang->line("items_basic_information"); ?></legend>


<table cellpadding="5" cellspacing="5">
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_vinnumber').':', 'category',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'category',
                    'id'=>'category',
                    'size'=>'25',
                    'value'=>$item_info->category)
                );?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_item_number').':', 'name',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'item_number',
                    'id'=>'item_number',
                    'size'=>'25',
                    'value'=>$item_info->item_number)
                );?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
    		<table id="subform_container">
    		
    				<?php $this->load->view('items/sub_form');?>
    		
    		</table>
    		
    	</td>
	</tr>
 	


</table>    
</fieldset>
<?php
echo form_close();
?>

<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();

});
</script>

<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>