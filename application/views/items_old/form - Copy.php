<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/save/'.$item_info->item_id,array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo $this->lang->line("items_basic_information"); ?></legend>
<table>
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_item_number').':', 'name',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'item_number',
                    'id'=>'item_number',
                    'size'=>'30',
                    'value'=>$item_info->item_number)
                );?>
                </div>
            </div>
        </td>
    </tr>
	<tr>
		<td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_name').':', 'name',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'name',
                    'id'=>'name',
                    'size'=>'30',
                    'value'=>$item_info->name)
                );?>
                </div>
            </div>
		</td>
        <td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_category').':', 'category',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'category',
                    'id'=>'category',
                    'size'=>'30',
                    'value'=>$item_info->category)
                );?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
	    <td colspan="2">
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_fit').':', 'fit',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'fit',
                    'id'=>'fit',
                    'size'=>'30',
                    'value'=>$item_info->fit)
                );?>
                </div>
            </div>
	    </td>
	</tr>
    <tr>        
	    <td>
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_model').':', 'model',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'model',
                    'id'=>'model',
                    'size'=>'12',
                    'value'=>$item_info->model)
                );?>
                </div>
            </div>
		</td>
	    <td>
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_year').':', 'year',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'year',
                    'id'=>'year',
					'size'=>'12',
                    'value'=>$item_info->year)
                );?>
                </div>
            </div>
	    </td>
		<td>
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_color').':', 'color',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'color',
                    'id'=>'color',
					'size'=>'12',
                    'value'=>$item_info->color)
                );?>
                </div>
            </div>
		</td>
	</tr>
    <tr>        
        <td>
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_condition').':', 'condition',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'condition',
                    'id'=>'condition',
					'size'=>'12',
                    'value'=>$item_info->condition)
                );?>
                </div>
            </div>
        </td>
		<td>
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_part_placement').':', 'part_placement',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'part_placement',
                    'id'=>'part_placement',
					'size'=>'12',
                    'value'=>$item_info->part_placement)
                );?>
                </div>
            </div>
        </td>
        <td>
            <div class="field_row clearfix">	
            <?php echo form_label($this->lang->line('items_branch').':', 'branch',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'branch',
                    'id'=>'branch',
					'size'=>'12',
                    'value'=>$item_info->branch)
                );?>
                </div>
            </div>
		</td>
	</tr>
	<tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php form_label($this->lang->line('items_supplier').':', 'supplier',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php form_dropdown('supplier_id', $suppliers, $selected_supplier);?>
                </div>
            </div>
		</td>
	</tr>             
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_location').':', 'location',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('location_id', $locations, $selected_location);?>
                </div>
            </div>
		</td>
	</tr>             
    <tr>
		<td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_cost_price').':', 'cost_price',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'cost_price',
                    'size'=>'12',
                    'id'=>'cost_price',
                    'value'=>$item_info->cost_price)
                );?>
                </div>
            </div>
		</td>
		<td>       
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_unit_price').':', 'unit_price',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'unit_price',
                    'size'=>'12',
                    'id'=>'unit_price',
                    'value'=>$item_info->unit_price)
                );?>
                </div>
            </div>
		</td>
    	<td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_quantity').':', 'quantity',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'quantity',
                    'id'=>'quantity',
					'size'=>'12',
                    'value'=>$item_info->quantity)
                );?>
                </div>
            </div>
		</td>
	</tr>
	<!--<tr>
		<td colspan="3">
            <div class="field_row clearfix">-->
            <?php form_label($this->lang->line('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
                <!--<div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_names[]',
                    'id'=>'tax_name_1',
                    'size'=>'12',
                    'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'))
                );?>
                <!--</div>
                
                <div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_percents[]',
                    'id'=>'tax_percent_name_1',
                    'size'=>'3',
                    'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate)
                );?><!--
                %
                </div>
            </div>
		</td>
	</tr> -->
   <!-- <tr>
    	<td colspan="3">       
            <div class="field_row clearfix">-->
            <?php form_label($this->lang->line('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
                <!--<div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_names[]',
                    'id'=>'tax_name_2',
                    'size'=>'12',
                    'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'))
                );?>
                <!--</div>
                <div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'tax_percents[]',
                    'id'=>'tax_percent_name_2',
                    'size'=>'3',
                    'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate)
                );?>
                <!--%
                </div>
            </div>
		</td>
	</tr>       --> 
	
	<tr>
    	<td colspan="2">
            <div class="field_row clearfix">
            <?php form_label($this->lang->line('items_reorder_level').':', 'reorder_level',array('class'=>'required wide')); ?>
                <div class='form_field'>
                <?php form_input(array(
                    'name'=>'reorder_level',
                    'id'=>'reorder_level',
					'size'=>'12',
                    'value'=>$item_info->reorder_level)
                );?>
                </div>
            </div>
		</td>
	</tr>
	<!--<tr>
    	<td colspan="3">
            <div class="field_row clearfix">	-->
            <?php form_label($this->lang->line('items_location').':', 'location',array('class'=>'wide')); ?>
                <!--<div class='form_field'>-->
                <?php form_input(array(
                    'name'=>'location',
                    'id'=>'location',
					'size'=>'12',
                    'value'=>$item_info->location)
                );?>
                <!--</div>
            </div>
		</td>
    </tr>-->
	<tr>
    	<td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_description').':', 'description',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_textarea(array(
                    'name'=>'description',
                    'id'=>'description',
                    'value'=>$item_info->description,
                    'rows'=>'3',
                    'cols'=>'25')
                );?>
                </div>
            </div>
		</td>
    </tr>
	<tr>
    	<td>
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_allow_alt_desciption').':', 'allow_alt_description',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_checkbox(array(
                    'name'=>'allow_alt_description',
                    'id'=>'allow_alt_description',
                    'value'=>1,
                    'checked'=>($item_info->allow_alt_description)? 1  :0)
                );?>
                </div>
            </div>
		</td>
        <td>            
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_is_serialized').':', 'is_serialized',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_checkbox(array(
                    'name'=>'is_serialized',
                    'id'=>'is_serialized',
                    'value'=>1,
                    'checked'=>($item_info->is_serialized)? 1 : 0)
                );?>
                </div>
            </div>
		</td>
        <td></td>
	</tr>
    <tr>
    	<td colspan="2">
			<?php
            echo form_submit(array(
                'name'=>'submit',
                'id'=>'submit',
                'value'=>$this->lang->line('common_submit'),
                'class'=>'submit_button float_left')
            );
            ?>
	</td>
	<td>
	</td>
    </tr>		

</table>    
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();


	$('#item_form').validate({
		submitHandler:function(form)
		{
			/*
			make sure the hidden field #item_number gets set
			to the visible scan_item_number value
			*/
			$('#item_number').val($('#scan_item_number').val());
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			category:"required",
			cost_price:
			{
				required:true,
				number:true
			},

			unit_price:
			{
				required:true,
				number:true
			},
			tax_percent:
			{
				required:true,
				number:true
			},
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			name:"<?php echo $this->lang->line('items_name_required'); ?>",
			category:"<?php echo $this->lang->line('items_category_required'); ?>",
			cost_price:
			{
				required:"<?php echo $this->lang->line('items_cost_price_required'); ?>",
				number:"<?php echo $this->lang->line('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo $this->lang->line('items_unit_price_required'); ?>",
				number:"<?php echo $this->lang->line('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				required:"<?php echo $this->lang->line('items_tax_percent_required'); ?>",
				number:"<?php echo $this->lang->line('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				required:"<?php echo $this->lang->line('items_quantity_required'); ?>",
				number:"<?php echo $this->lang->line('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required:"<?php echo $this->lang->line('items_reorder_level_required'); ?>",
				number:"<?php echo $this->lang->line('items_reorder_level_number'); ?>"
			}

		}
	});
});
</script>