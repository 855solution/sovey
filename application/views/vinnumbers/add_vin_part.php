<?php $this->load->view("partial/header"); ?>

<style type="text/css">
	.hide{
		display: none;
	}
	.disabled *{
		cursor: not-allowed;
		background: lightgray !important;
		color: gray !important;
	}
</style>
<div>
	<div id="title_bar">
		<div id="title">Available Part For <?php echo $vin_name ?></div>
	</div>
	<div id="table_holder">

		<table id="sortable_table" class="tablesorter">
			<thead>
				<th>No</th>
				<th>Part Number</th>
				<th>Name</th>
				<th>Have</th>
				<th>Grade</th>
			</thead>
			<tbody>
				<form action="<?php echo site_url('vinnumbers/submit_part/'.$vin_id) ?>" method='post' id='part_form'>
				<input type="hidden" value="<?php echo $vin_id ?>" id="vin_id">
				<?php $i=0;foreach ($part as $p): $i++;?>
					<?php
						$have = $this->db->query("SELECT * FROM ospos_vinnumber_part WHERE part_number='$p->part_number' AND vin_id=$vin_id")->row();
						$chk = '';
						$hide='hide';
						if (!empty($have)) {
							$chk='checked';
							$hide='';
						}
						$dis='';
						if ($p->status==0 AND $p->status!= null) {
							$dis = 'disabled';
						}
					?>
					<tr class='<?php echo $dis ?>'>
						<td ><?php echo $i ?></td>
						<td><input type="hidden" name="khmer_name[]" value="<?php echo $p->khmernames_id ?>"><?php echo $p->part_number ?></td>
						<td ><?php echo $p->english_name ?> / <?php echo $p->khmername_name ?></td>
						<td ><input id="part_have" <?php echo $chk ?> <?php echo $dis ?> type="checkbox" name="part_number[]" class="part_have" value="<?php echo $p->part_number ?>"></td>
						<td style="width:200px;" class='<?php echo $dis ?>'><span class="<?php echo $hide ?>" d="<?php echo $p->partid ?>"><input <?php echo $dis ?> type="text" name="part_grade[]" value="<?php echo $have->grade ?>"></span></td>
					</tr>
				<?php endforeach ?>
				</form>

			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
		

	</div>
	<div style="float:right" class="big_button" id="btn_submit"><span>Save</span></div>
	<div style="clear:both;"></div>
</div>
<script type="text/javascript">
	$('.part_have').click(function(event) {
    	if ($(event.target).is(':checked')) {
    		// $(event.target)
    		$(event.target).parent().next().find('span').removeClass('hide')
    		// console.log()
    	}else{
    		$(event.target).parent().next().find('span').addClass('hide')
			// $(event.target).parent().next().find('span')
    	};
    	// console.log($(event.target).parent().next().find('div'))
	});

	$('#btn_submit').click(function(){
		// submit_data();
		$('#part_form').submit();
	});
	

	// function submit_data(){
	// 	// $('#part_form').submit();
	// 	var selected = [];
	// 	var grade = [];
	// 	var not_check = [];
	// 	var vin_id = $('#vin_id').val();

	// 	$('#sortable_table input:checked').each(function() {
	// 		if ($(this).prop('disabled')===false) {
	// 	    	selected.push($(this).val());
	// 	    	grade.push($(this).parent().next().find('input').val());
	// 		};
			
	// 	});
	// 	$('#sortable_table input:checkbox:not(:checked)').each(function() {
	// 		if ($(this).prop('disabled')===false) {
	// 			not_check.push($(this).val());
	// 		}
	// 	});
	// 	$.ajax({
	// 		url:"<?php echo site_url('vinnumbers/submit_part/') ?>"+'/'+vin_id,
	// 		type:"POST",
	// 		async:false,
	// 		dataType:"Json",
	// 		data:{
	// 			khmername_id:selected,
	// 			grade:grade,
	// 			not_check:not_check
	// 		},
	// 		success:function(data){
	// 			console.log(data);
	// 			// location.href = "<?php echo site_url('vinnumbers/add_vin_part/') ?>"+'/'+vin_id;
	// 		}
	// 	});
	// }

</script>
<?php $this->load->view("partial/footer"); ?>


