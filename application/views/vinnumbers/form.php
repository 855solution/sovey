   <style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    ul,ol{
        margin-bottom: 0px !important;
    }
    #img_row{padding: 3px 0px !important}
    .hide{display: none !important;}
    .img_row>div{
        width: 45%;
        float: left;
    }
    .remove_img{
        margin-bottom:-23px;position: relative; margin-left:-6px !important;
        width: 24px;
    }
    .top_action_button img{width: 28px; margin-top: 5px;}
    .saveloading{width:35px;}
    .remove_img:hover{
        cursor: pointer;
    }
    a{
        cursor: pointer;
    }
    .previewupl:hover{
        cursor: pointer;
    }
    .datepicker {z-index: 9999;}
</style>


<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<script language="JavaScript" type="text/javascript">
			  // +++++++++++++++++++++ Image upload++++++++++++++++++
   function PreviewImage(event) {
        var uppreview=$(event.target).parent().parent().find('#imgpreview');
        var oFReader = new FileReader();
        oFReader.readAsDataURL($(event.target)[0].files[0]);

        oFReader.onload = function (oFREvent) {
            uppreview.attr('src',oFREvent.target.result);
            // uppreview.style.backgroundImage = "none";
        };
        $(event.target).parent().parent().find('.remove_img').removeClass('hide');
        $(event.target).removeClass('bl');
        var slideid=$(event.target).parent().parent().find('.remove_img').attr('rel');
        var del=$('#deleteimg').val()
        $('#deleteimg').val(del+','+slideid);
        addnewimg();
    };
    function addnewimg(){
        var exst=false;
        var ma=0;
        $('.uploadimg').each(function(){
            var ma_o=$(this).parent().parent().find('#order').val();
            var f = $(this)[0].files[0];
            // alert(f);
            if(ma<ma_o)
                ma=ma_o;
            if(f==undefined && $(this).hasClass('bl')){
                exst=true;
            }
        })
        if(exst==false)
            $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
                                        "<div class='img' style='width:130px; float:left'> "+ 
                                            "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
                                            "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
                                            "<input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
                                            "<input type='text' value='' class='removeimgid hide' />"+
                                        "</div>"+
                                        "<div class='txt_order' style='width:130px; float:left; margin-left:30px; margin-top:18px;'>"+
                                            "<label class='col-lg-1 control-label' style='margin-top:15px !important;' >Order</label>"+
                                            "<div class='col-lg-4' style='margin-top:15px !important;''>"+ 
                                                "<div class='col-md-12'>"+
                                                    "<input type='text'  class='form-control input-sm order'  name='order[]' id='order' value='"+(Number(ma)+1)+"'> "+
                                                "</div>"+ 
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                    "<p style='clear:both;'></p>");
    }
       
    function choosimg(event){
        $(event.target).parent().parent().find('#uploadImage').click();
    }
    // +++++++++++++++++++++++++++++remove upload pic+++++++++++++++++++ 
    function remove_img(event){
        var picid=$(event.target).attr('rel');
        $(event.target).parent().parent().remove();
        var delete_img=$('#deleteimg').val();
        if(picid!='')
            $('#deleteimg').val(delete_img+','+picid);
    }


   // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	$(function()
	{
		$('#make_id').chainSelect('#model_id','<?php echo site_url('items/combobox') ?>',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
	});


/*---------------Upload Image-----------------*/


				// var intTextBox=0;
				// function addElement(){
				// 	intTextBox = intTextBox + 1;
				// 	var contentID = document.getElementById('pop');
				// 	var newTBDiv = document.createElement('div');
				// 	newTBDiv.setAttribute('id','strText'+intTextBox);
				// 	newTBDiv.innerHTML ="<input type='file' id='" + intTextBox + "' size='25' name='path[]' style='margin-top:15px;'/>";
				// 	contentID.appendChild(newTBDiv);
				// }
				// function removeElement(){
				// 	if(intTextBox != 0){
				// 		var contentID = document.getElementById('pop');
				// 		contentID.removeChild(document.getElementById('strText'+intTextBox));
				// 		intTextBox = intTextBox-1;
				// 	}
				// }

				// $('.imageDelete').click(function(event){
				// 	var id= $(this).attr('title');
				// 	var msg=false;
				// 	msg=confirm("Are you sure, delete this picture?");
				// 	//alert(msg);
				// 	if(msg==true){
				// 		$(this).prev().andSelf().remove();	
				// 		 $.post('index.php/vinnumbers/deleteHeadImage', {'imageid':id},function(result)
				// 			        {
						        
				// 			        });	
				// 		}
				// 	});
/*----------------------------------------------------*/

				
            </script>
<?php
echo form_open_multipart('vinnumbers/save/'.$vinnumber_info->vinnumber_id,array('id'=>'vinnumber_form','type'=>'POST'));
?>
<fieldset id="vinnumber_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("vinnumbers_basic_information"); ?></legend>

<!-- <div class="field_row clearfix">
<?php form_label($this->lang->line('vinnumbers_vinnumber_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php form_input(array(
		'name'=>'vinnumber_code',
		'id'=>'vinnumber_code',
		'value'=>$vinnumber_info->vainnumber)
	);?>
	</div>
</div> -->
<!-- <?php var_dump($vinnumber_info) ?> -->
<div class="field_row clearfix">
    <label class="wide">Vinnumber:</label>
    <div class='form_field'>
        <input type="text" name="vinnumber_code" id="vinnumber_code" value="<?php echo $vinnumber_info->vainnumber ?>">
    </div>
</div>
<div class="field_row clearfix">
    <label class="wide">Date Arrived:</label>
    <div class='form_field'>
        <input type="text" name="date_arrived" id="date_arrived" class="date_arrived" value="<?php echo $vinnumber_info->date_arrived ?>">
    </div>
</div>
<div class="field_row clearfix">
    <label class="wide">Engine:</label>
    <div class='form_field'>
        <select name="vin_engine">
            <option value="">Please Select</option>
            <?php
                $sele='';
                $engine = $this->db->query("SELECT * FROM ospos_engine WHERE deleted=0 ORDER BY engine_name ASC")->result();
                foreach ($engine as $en) {
                    if(isset($vinnumber_info->engine)){
                        if ($vinnumber_info->engine==$en->engine_id) {
                           $sele="selected";
                        }else{
                            $sele="";
                        }
                        
                    }
                ?>

                <option value="<?php echo $en->engine_id ?>" <?php echo $sele ?>><?php echo $en->engine_name ?></option>
            <?php
                }

            ?>
        </select>
    </div>
</div>
<div class="field_row clearfix">
    <label class="wide">Transmission:</label>
    <div class='form_field'>
        <select name="vin_trans">
            <option value="">Please Select</option>
            <?php
                $sele='';
                $trans = $this->db->query("SELECT * FROM ospos_transmission WHERE deleted=0 ORDER BY transmission_name ASC")->result();
                foreach ($trans as $tra) {
                    if($vinnumber_info->transmission==$tra->trans_id){
                        $sele="selected";
                    }else{
                        $sele="";
                    }
                ?>

                <option value="<?php echo $tra->trans_id ?>" <?php echo $sele ?>><?php echo $tra->transmission_name ?></option>
            <?php
                }

            ?>
        </select>
    </div>
</div>
<div class="field_row clearfix">
    <label class="wide">Trim:</label>
    <div class='form_field'>
        <select name="vin_trim">
            <option value="">Please Select</option>
            <?php
                $sele='';
                $trim = $this->db->query("SELECT * FROM ospos_trim WHERE deleted=0 ORDER BY trim_name ASC")->result();
                foreach ($trim as $tri) {
                    if($vinnumber_info->trim==$tri->trim_id){
                        $sele="selected";
                    }else{
                        $sele='';
                    }
                ?>

                <option value="<?php echo $tri->trim_id ?>" <?php echo $sele ?>><?php echo $tri->trim_name ?></option>
            <?php
                }

            ?>
        </select>
    </div>
</div>
<div class="field_row clearfix">
    <label class="wide">Body:</label>
    <div class='form_field'>
        <select name="vin_body">
            <option value="">Please Select</option>
            <?php
                $sele='';
                $body = $this->db->query("SELECT * FROM ospos_body WHERE deleted=0 ORDER BY body_name ASC")->result();
                foreach ($body as $bo) {
                    if($vinnumber_info->body==$bo->body_id){
                        $sele="selected";
                    }else{
                        $sele="";
                    }
                ?>

                <option value="<?php echo $bo->body_id ?>" <?php echo $sele ?>><?php echo $bo->body_name ?></option>
            <?php
                }
            ?>
        </select>
    </div>
</div>
<div class="field_row clearfix">
    <label class="wide">Fuel:</label>
    <div class='form_field'>
        <select name="vin_fuel">
            <option value="">Please Select</option>
            <?php
                $sele='';
                $fuel = $this->db->query("SELECT * FROM ospos_fuel WHERE deleted=0 ORDER BY fule_name ASC")->result();
                foreach ($fuel as $fu) {
                    if($vinnumber_info->fuel==$fu->fuel_id){
                        $sele="selected";
                    }else{
                        $sele="";
                    }
                ?>

                <option value="<?php echo $fu->fuel_id ?>" <?php echo $sele ?>><?php echo $fu->fule_name ?></option>
            <?php
                }

            ?>
        </select>
    </div>
</div>
<div class="field_row clearfix">
	<?php echo form_label($this->lang->line('items_make').':', 'make',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php 
        $js = 'id="make_id"';
        echo form_dropdown('make_id', $makes, $selected_make,$js);
    ?>
        
    </div>
</div>

<div class="field_row clearfix">
	<?php echo form_label($this->lang->line('items_model').':', 'model',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php 
        $model_js = 'id="model_id"';
        echo form_dropdown('model_id', $models, $selected_model,$model_js);
    ?>
    </div>
</div>


<div class="field_row clearfix">
<?php echo form_label($this->lang->line('items_year').':', 'year',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php 
        define (MINY,1980);
        $year_temp = $vinnumber_info->year;
    ?>
    <select name="year" id="year">
            <option value="<?php echo $year_temp; ?>"><?php echo $year_temp; ?></option>
        <?php 
            for($j=MINY;$j<=date('Y');$j++){
                echo '<option value="'.$j.'">'.$j.'</option>';
            }
        ?>                    
    </select>
    </div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('items_cost_price').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'vin_cost_price',
		'id'=>'vin_cost_price',
		'value'=>$vinnumber_info->cost_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_label($this->lang->line('interior_color').':', 'make',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php 
        $colorid = 'id="color_id"';
        echo form_dropdown('internal_color', $colors, $selected_color,$colorid);
    ?>
        
    </div>
</div>


<div class="field_row clearfix">
	<?php echo form_label($this->lang->line('external_color').':', 'make',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php 
        $colorid = 'id="color_id"';
        echo form_dropdown('external_color', $enternal_colors, $enternal_selected_color,$colorid);
    ?>
        
    </div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('vinnumbers_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$vinnumber_info->description)
	);?>
	</div>
</div>

<!-- Upload Image -->
<div id='blog_img'> 
                <input type='text' name='deleteimg' style='display:none' id='deleteimg'/>
                <?PHP

                $ma=1;
                $img_path=base_url('assets/site/image/no_img.png');
                if(isset($vinnumber_info->vinnumber_id)){
                    foreach ($this->Vinnumber->getimage($vinnumber_info->vinnumber_id) as $img) { 
                        if($ma<$img->first_image)
                            $ma=$img->first_image;
                        if(file_exists(FCPATH."/uploads/thumb/".$img->url_image)){
                            $img_paths=base_url("/uploads/thumb/".$img->url_image);
                         
                    ?>
                            <div style='width:400px;' id='img_row'>
                                <div class="img" style="width:130px; float:left"> 
                                    <img onclick='remove_img(event);' rel='<?PHP echo $img->vin_image_id ?>' class='remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                                    <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_paths ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                                    <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg ext' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                                    <input type='text' class='hide'  name='updimg[]' id='updimg'/>
                                </div>
                                <div class='txt_order' style="width:130px; float:left; margin-left:30px; margin-top:18px;">
                                    <label class='col-lg-1 control-label'style="margin-top:15px !important;" >Order</label>
                                    <div class=" col-lg-4" style="margin-top:15px !important;"> 
                                        <div class="col-md-12">
                                            <input type="text" onkeypress='return isNumberKey(event);' class="form-control input-sm order"  name="order[]" id='order' rel='<?php echo $img->item_id ?>' value='<?php echo $img->first_image ?>'>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <p style='clear:both;'></p>  
                    <?PHP
                        }else{
                            $this->Vinnumber->deleteimg($img->vin_image_id);
                        }  
                    }
                }
                ?>
                 <div style='width:400px;' id='img_row'>
                    <div class="img" style="width:130px; float:left"> 
                        <img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                        <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                        <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                    </div>
                    <div class='txt_order' style="width:130px; float:left; margin-left:30px; margin-top:18px;">
                        <label class='col-lg-1 control-label' >Order</label>
                        <div class=" col-lg-4"> 
                            <div class="col-md-12">
                                <input type="text"  class="form-control input-sm" id='order'  name="order[]" value="<?php echo $ma+1 ?>">
                            </div> 
                        </div>
                    </div>
                </div> 
                <p style='clear:both;'></p>    
            </div>
<!--  close Image-->





<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>

<script type='text/javascript'>
// $("#make_id").select2();
// $("#model_id").select2();



</script>

<script src="<?php echo base_url();?>assets/js/jquery-ui.custom.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
    $("#ui-datepicker-div").remove();
    $(".date_arrived").datepicker({
        dateFormat: 'yy-mm-dd'
    });
</script>