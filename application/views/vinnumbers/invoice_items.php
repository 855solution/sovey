<?php $this->load->view("partial/header"); ?>
<?php

if (isset($error_message))
{
    echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
    exit;
}
?>
<style type="text/css">
    #receipt_items th{
        text-align: center;
    }
    #receipt_items td{
        text-align: center;
    }
    #receipt_wrapper{
        width: 45%;
        float: left;
        padding: 1% 2%;
    }
</style>
<!-- <?php var_dump($cart) ?> -->
<?php
    $sale_time = date_create($items[0]->sale_time);
?>
<div id="receipt_wrapper">
    <div id="receipt_header">
        <div id="company_name"><?php echo $this->config->item('company'); ?></div>
        <div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
        <div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
        <div id="sale_receipt"><?php echo $this->lang->line('sales_receipt'); ?></div>
        <div id="sale_time"><?php echo date_format($sale_time,'m/d/Y h:i:s a') ?></div>
    </div>
    <div id="receipt_general_info">
        <?php 
        $customer= $items[0]->cusname; 
        $employee=$items[0]->empname;
        $sale_id=$items[0]->sale_id;
        ?>
        <?php if(isset($customer))
        {
        ?>
            <div id="customer"><?php echo $this->lang->line('customers_customer').": ".$customer; ?></div>
        <?php
        }
        ?>
        <div id="sale_id"><?php echo $this->lang->line('sales_id').": ".$sale_id; ?></div>
        <div id="employee"><?php echo $this->lang->line('employees_employee').": ".$employee; ?></div>
    </div>

    <table id="receipt_items">
    <tr>
    <!-- <th></th> -->
    <th><?php echo "No"; ?></th>
    <th><?php echo "Barcode"; ?></th>
    <!-- <th><?php echo $this->lang->line('items_item'); ?></th> -->
    <th>Year</th>
    <th><?php echo "Model"; ?></th>
    <th><?php echo $this->lang->line('sales_quantity'); ?></th>
    <th><?php echo "PPL"; ?></th>
    <th><?php echo $this->lang->line('common_price'); ?></th>
    <th><?php echo "Dsc"; ?></th>
    <th><?php echo $this->lang->line('sales_total'); ?></th>
    </tr>
    <?php
    // var_dump($items);
    $i=0;
    $subtotal=0;
    $payment_total='';

    foreach($items as $sd)
    {
        $i++;
        $part='';
        if ($sd->is_new==1) {
            $part = $sd->pnamenew;
        }else{
            $part = $sd->pname;
        }
    ?>
        <tr>
        <td><?php echo $i ?></td>
        <!-- <td><?php echo $item['item_id']; ?></td> -->
        
        <td><?php echo $sd->barcode ?></td>
        <!-- <td><?php echo $item['vin_num'] ?></td> -->
        <!-- <td><span class='long_name'><?php echo $item['name']; ?></span><span class='short_name'><?php echo character_limiter($item['name'],10); ?></span></td> -->
        <td><?php echo $sd->year ?></td>
        <td><?php echo $sd->model_name;?></td>
        <td style='text-align:center;'><?php echo $sd->quantity_purchased; ?></td>
        <td><?php echo $part;?></td>
        <td><?php echo to_currency($sd->item_unit_price); ?></td>
        <td style='text-align:center;'><?php echo $sd->discount_percent; ?></td>
        <!-- <td style='text-align:right;'><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td> -->
        <td style='text-align:right;'><?php echo to_currency($sd->item_unit_price*$sd->quantity_purchased-$sd->discount_percent); ?></td>
        </tr>

        <tr>
        <td colspan="5" align="center"><?php //echo $item['description']; ?></td>
        <td colspan="4" ><?php echo $sd->serialnumber; ?></td>
        <td colspan="1"><?php echo '&nbsp;'; ?></td>
        </tr>

    <?php
    $subtotal+=($sd->item_unit_price*$sd->quantity_purchased-$sd->discount_percent);
    $total+=($sd->item_unit_price*$sd->quantity_purchased-$sd->discount_percent);
    $payment_total = explode(':',$sd->payment_type);
    }
    ?>
    <tr>
    <td colspan="7" style='text-align:right;border-top:2px solid #000000;'><?php echo $this->lang->line('sales_sub_total'); ?></td>
    <td colspan="3" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($subtotal); ?></td>
    </tr>

    <tr>
    <td colspan="7" style='text-align:right;'><?php echo $this->lang->line('sales_total'); ?></td>
    <td colspan="3" style='text-align:right'><?php echo to_currency($total); ?></td>
    </tr>
    <tr><td colspan="10">&nbsp;</td></tr>

        <tr>
        <td colspan="7" style="text-align:right;"><?php echo $this->lang->line('sales_payment'); ?></td>
        <td colspan="2" style="text-align:right;"><?php echo $payment_total[0]; ?> </td>
        <td colspan="1" style="text-align:right"><?php echo $payment_total[1]; ?>  </td>
        </tr>

    <tr><td colspan="10">&nbsp;</td></tr>
    <?php
        $payt = explode('$',$payment_total[1]);
        $amount_due=$total-$payt[1];
       
    ?>
    <tr>
        <td colspan="7" style='text-align:right;'><?php echo $this->lang->line('sales_change_due'); ?></td>
        <td colspan="3" style='text-align:right'><?php echo to_currency($amount_due*-1); ?></td>
    </tr>

    </table>

    <!-- <div id="sale_return_policy">
    <?php echo nl2br($this->config->item('return_policy')); ?>
    </div> -->
    <div id='barcode'>
    <?php echo "<img src='index.php/barcode?barcode=$sd->invoiceid&text=$sd->invoiceid&width=250&height=50' />"; ?>
    </div>
</div>

<div id="receipt_wrapper">
    <div id="receipt_header">
        <div id="company_name"><?php echo $this->config->item('company'); ?></div>
        <div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
        <div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
        <div id="sale_receipt"><?php echo $this->lang->line('sales_receipt'); ?></div>
        <div id="sale_time"><?php echo date_format($sale_time,'m/d/Y h:i:s a') ?></div>
    </div>
    <div id="receipt_general_info">
        <?php if(isset($customer))
        {
        ?>
            <div id="customer"><?php echo $this->lang->line('customers_customer').": ".$customer; ?></div>
        <?php
        }
        ?>
        <div id="sale_id"><?php echo $this->lang->line('sales_id').": ".$sale_id; ?></div>
        <div id="employee"><?php echo $this->lang->line('employees_employee').": ".$employee; ?></div>
    </div>

    <table id="receipt_items">
    <tr>
    <!-- <th></th> -->
    <th><?php echo "No"; ?></th>
    <th><?php echo "Barcode"; ?></th>
    <!-- <th><?php echo $this->lang->line('items_item'); ?></th> -->
    <th>Year</th>
    <th><?php echo "Model"; ?></th>
    <th><?php echo $this->lang->line('sales_quantity'); ?></th>
    <th><?php echo "PPL"; ?></th>
    <th><?php echo $this->lang->line('common_price'); ?></th>
    <th><?php echo "Dsc"; ?></th>
    <th><?php echo $this->lang->line('sales_total'); ?></th>
    </tr>
     <?php
    // var_dump($items);
    $i=0;
    // $subtotal=0;
    // $payment_total='';

    foreach($items as $sd)
    {
        $i++;
        $part='';
        if ($sd->is_new==1) {
            $part = $sd->pnamenew;
        }else{
            $part = $sd->pname;
        }
    ?>
        <tr>
        <td><?php echo $i ?></td>
        <!-- <td><?php echo $item['item_id']; ?></td> -->
        
        <td><?php echo $sd->barcode ?></td>
        <!-- <td><?php echo $item['vin_num'] ?></td> -->
        <!-- <td><span class='long_name'><?php echo $item['name']; ?></span><span class='short_name'><?php echo character_limiter($item['name'],10); ?></span></td> -->
        <td><?php echo $sd->year ?></td>
        <td><?php echo $sd->model_name;?></td>
        <td style='text-align:center;'><?php echo $sd->quantity_purchased; ?></td>
        <td><?php echo $part;?></td>
        <td><?php echo to_currency($sd->item_unit_price); ?></td>
        <td style='text-align:center;'><?php echo $sd->discount_percent; ?></td>
        <!-- <td style='text-align:right;'><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td> -->
        <td style='text-align:right;'><?php echo to_currency($sd->item_unit_price*$sd->quantity_purchased-$sd->discount_percent); ?></td>
        </tr>

        <tr>
        <td colspan="5" align="center"><?php //echo $item['description']; ?></td>
        <td colspan="4" ><?php echo $sd->serialnumber; ?></td>
        <td colspan="1"><?php echo '&nbsp;'; ?></td>
        </tr>

    <?php
    // $subtotal+=($sd->item_unit_price*$sd->quantity_purchased-$sd->discount_percent);
    // $total+=($sd->item_unit_price*$sd->quantity_purchased-$sd->discount_percent);
   
    }
    ?>
    <tr>
    <td colspan="7" style='text-align:right;border-top:2px solid #000000;'><?php echo $this->lang->line('sales_sub_total'); ?></td>
    <td colspan="3" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($subtotal); ?></td>
    </tr>

    <tr>
    <td colspan="7" style='text-align:right;'><?php echo $this->lang->line('sales_total'); ?></td>
    <td colspan="3" style='text-align:right'><?php echo to_currency($total); ?></td>
    </tr>
    <tr><td colspan="10">&nbsp;</td></tr>

        <tr>
        <td colspan="7" style="text-align:right;"><?php echo $this->lang->line('sales_payment'); ?></td>
        <td colspan="2" style="text-align:right;"><?php echo $payment_total[0]; ?> </td>
        <td colspan="1" style="text-align:right"><?php echo $payment_total[1]; ?>  </td>
        </tr>

    <tr><td colspan="10">&nbsp;</td></tr>
    
    <tr>
        <td colspan="7" style='text-align:right;'><?php echo $this->lang->line('sales_change_due'); ?></td>
        <td colspan="3" style='text-align:right'><?php echo to_currency($amount_due*-1); ?></td>
    </tr>

    </table>

    <!-- <div id="sale_return_policy">
    <?php echo nl2br($this->config->item('return_policy')); ?>
    </div> -->
    <div id='barcode'>
    <?php echo "<img src='index.php/barcode?barcode=$sd->invoiceid&text=$sd->invoiceid&width=250&height=50' />"; ?>
    </div>
</div>
<div style="clear:both"></div>
<?php $this->load->view("partial/footer"); ?>

<?php if ($this->Appconfig->get('print_after_sale'))
{
?>
<script type="text/javascript">
$(window).load(function()
{
    window.print();
});
</script>
<?php
}
?>