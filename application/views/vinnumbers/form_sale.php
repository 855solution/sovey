
<?php $this->load->view('partial/header') ?>

<?php if (!$sale_data){ ?>
<div align="center" class="alert alert-warning"> No Items </div>

<?php }else{ 
    $vid = $this->uri->segment(3);

?>
<style type="text/css">
    /*tbl{
        padding-top: 2%;
    }*/
   /* #TB_window{
        width: 85% !important;
        left: 7%;
        margin-left: 0 !important
    }
    #TB_ajaxContent{
        width: 97% !important;
    }*/
</style>
<script type="text/javascript">
    $(document).ready(function()
    {
        init_table_sorting();
        // enable_select_all();
        // enable_checkboxes();
        // enable_row_selection();
        // enable_search('<?php echo site_url("$controller_name/suggest_sold_item/$vid")?>');
        // enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');

    });

    function init_table_sorting()
    {
        //Only init if there is more than one row
        if($('.tablesorter tbody tr').length >1)
        {
            $("#sortable").tablesorter(
            {
                // cssChildRow: "tablesorter-childRow",
                sortList: [[1,0]]
                // headers:
                // {
                //     0: { sorter: false},
                //     0: { sorter: false}
                // }
            });
            
        }
    }
    

</script>
<div style="padding:1% 0">
    <h2>Part Sold</h2>
</div>
<div id="table_action_header">
    <ul>
        <!-- <li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li> -->
        <li class="float_right">
        <img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
        <?php echo form_open("$controller_name/search_sold_item/$vid",array('id'=>'search_form')); ?>
        <input type="text" name ='search' id='search'/>
        </form>
        </li>
    </ul>
</div>
<div class="tbl" id="table_holder">
    <table id="sortable" class="tablesorter">
        <thead>
            <tr>
                <th>Image</th>
                <th>Barcode</th>
                <th>Part Name</th>
                <th>Part Name in Khmer</th>
                <th>Quantity</th>
                <th>Part Placement</th>
                <th>Price</th>
                <th>Invoice#</th>
                <th>Date Sale</th>
                <th>Customer Name</th>

            </tr>
        </thead>
        <tbody>
            
            <?php
            
                foreach ($sale_data as $sd) {
                    if (!empty($sd)) {
                    $image=$this->Item->getdefaultimg($sd->item_id);
                    $im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
                    if($image!=''){
                        if(file_exists(FCPATH."uploads/thumb/".$image)){
                            $im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
                          }
                    }
                    $img_path=img($im);
                ?>
                <tr>
                    <td><?php echo $img_path ?></td>
                    <td><?php echo $sd->barcode ?></td>
                    <td><?php echo $sd->name ?></td>
                    <td><?php echo $sd->khmername_name ?></td>
                    <td><?php echo $sd->quantity_purchased ?></td>
                    <?php
                        if ($sd->is_new!=1) {
                    ?>
                        <td><?php echo $sd->partplacement_name ?></td>
                    <?php
                        }else{
                    ?>
                        <td><?php echo $sd->new_p ?></td>
                    <?php
                        }
                    ?>
                    <td><?php echo $sd->item_unit_price ?></td>
                    <td>
                        <a target='_blank' href='<?php echo site_url("$controller_name/view_invoice_items/$sd->invoiceid") ?>'><?php echo $sd->invoiceid ?></a>
                    </td>
                    <td><?php echo $sd->sale_time ?></td>
                    <td><?php echo "$sd->first_name $sd->last_name" ?></td>
                    
                </tr>
                <?php
                } 

            } ?>
        </tbody>
    </table>
</div>
<div id="feedback_bar"></div>
<?php } ?>

<?php $this->load->view('partial/footer') ?>