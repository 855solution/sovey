<?php
	$s='';
	$part_name='';
	$make_id='';
	$model_id='';
	$s_year='';
	$color_id='';
	$is_new='';
	$is_feat='';
	$is_pro='';
	$in_color="";
	$ex_color="";

	$where='';
	if(isset($_GET['n'])){
		$part_name=$_GET['n'];
	}
	if(isset($_GET['m']) && $_GET['m']!='' && $_GET['m']!='undefined' && $_GET['m']!='0'){
		$where.=" AND v.make_id='".$_GET['m']."'";
		$make_id=$_GET['m'];
	}
	if(isset($_GET['mo']) && $_GET['mo']!=''&& $_GET['mo']!='undefined' && $_GET['mo']!='0'){
		$where.=" AND v.model_id='".$_GET['mo']."'";
		$model_id=$_GET['mo'];

	}
	if(isset($_GET['y']) && $_GET['y']!='' && $_GET['y']!='undefined' && $_GET['y']!='0'){
		$where.=" AND v.year='".$_GET['y']."'";
		$s_year=$_GET['y'];

	}
	if(isset($_GET['in_c']) && $_GET['in_c']!='' && $_GET['in_c']!='undefined' && $_GET['in_c']!='0'){
		$where.=" AND v.color_id='".$_GET['in_c']."'";
		$in_color=$_GET['in_c'];
	}
	if(isset($_GET['ex_c']) && $_GET['ex_c']!='' && $_GET['ex_c']!='undefined' && $_GET['ex_c']!='0'){
		$where.=" AND v.color_id='".$_GET['ex_c']."'";
		$ex_color=$_GET['ex_c'];

	}
	if(isset($_GET['ne'])){
		$is_new=1;

	}
	elseif (isset($_GET['f'])){
		$is_feat=1;

	}
	elseif (isset($_GET['pr'])){
		$is_pro=1;
	}
 ?>
<script type="text/javascript">
$(document).ready(function()
{
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			// cssChildRow: "tablesorter-childRow",
			sortList: [[2,0]],
			headers:
			{
				9: { sorter: false},
				0: { sorter: false}
			}
		});
		
	}
}


function post_vinnumber_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.vinnumber_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.vinnumber_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.vinnumber_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}
</script>
<style type="text/css">
	.v_photo{
		margin-top: -23px;
		width: 120px;
		text-align: center;
		position: absolute;
		background: rgba(0, 0, 0, 0.21) none repeat scroll 0% 0%;
	}
	.v_photo a{
		color: white;
	}
	#pagination{
		padding: 1% 0;
		float: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
	#table_holder{
		width: 84%;
		float: right;
		margin-top: 0;
	}
	#table_action_header{
		width:84%;
		float: right;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
</div>

<!-- <div>
	<select id="per_page" class="form-control">
		<?php 
			$val=20;
			if (isset($_GET['p_num'])) {
				$val = $_GET['p_num'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
		?>
		<option value="20" <?php if($val==20)echo "selected"?>>20</option>
		<option value="50" <?php if($val==50)echo "selected"?>>50</option>
		<option value="100" <?php if($val==100)echo "selected"?>>100</option>
	</select>
</div>
<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	<div style="float:right">
		<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
</div> -->
<div id="table_action_header">
	<ul>
		<li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li class="float_right">
		<!-- <img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form> -->
		<form>
			<input type="text" name="isearch" id="isearch" class="form-control">
		</form>
		</li>
	</ul>
</div>
<div>
	<?php $this->load->view('vinnumbers/filter'); ?>
</div>
<div id="table_holder">
<!-- TOP PAGINATION -->

	<div style="float:left;width:100%;height:25px;">
		<?php 
				$val=10;
				if (isset($_GET['p_num'])) {
					$val = $_GET['p_num'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
		?>
		<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div style="float:left;width:50%;">
		Show
		<select id="per_page" class="per_page" name="per_page">
			<?php 
				$val=10;
				if (isset($_GET['p_num'])) {
					$val = $_GET['p_num'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div>
	<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	</div>
	<!-- END TOP PAGINATION -->
	<table id="sortable_table" class="tablesorter">
		<thead>
			<th><input type="checkbox" id="select_all" /></th>
			<th>Image</th>
			<th>Vinnumber</th>
			<th>Year</th>
			<th>Make</th>
			<th>Model</th>
			<th>Interior Color</th>
			<th>Exterior Color</th>
			<th>Cost Price</th>
			<th>Actions</th>
		</thead>
		<tbody>
			<?php
				if (!$data) {
					?>
					<tr>
						<td colspan="10">
							No Vinnumbers to Display
						</td>
					</tr>
				<?php
				}else{
				foreach ($data as $vin) {
                    $image=$this->vinnm->getdefaultimg($vin->vinnumber_id);
                    $im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
                    if($image!=''){
                        if(file_exists(FCPATH."uploads/thumb/".$image)){
                            $im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
                          }
                    }
                    $img_path=img($im);
					$y = get2lastString($vin->year);

					$color_vin = anchor($controller_name."/view/$vin->vinnumber_id/width:$width",'<strong>'.'H'."<span style='color:#7a0606'>".$vin->vinnumber_id."</span><span style='color:#067a34'>".$vin->model_id."</span><span style='color:#7a0668'>".$y.'</span></strong>',array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_update')));

			?>

				<tr>
					<td><input type='checkbox' id='vinnumber_<?php echo $vin->vinnumber_id?>' value='<?php echo $vin->vinnumber_id?>'/></td>
					<td><?php echo $img_path ?><div class="v_photo"><a target="_blank" href='<?php echo site_url("$controller_name/view_all_v_photo/$vin->vinnumber_id")?>'>View all photos</a></div></td>
					<td><?php echo $color_vin ?></td>
					<td><?php echo $vin->year ?></td>
					<td><?php echo $vin->make_name ?></td>
					<td><?php echo $vin->model_name ?></td>
					<td><?php echo $vin->in_co ?></td>
					<td><?php echo $vin->ex_co ?></td>
					<td><?php echo to_currency($vin->cost_price) ?></td>
					<td><a target="_blank" href='<?php echo site_url("$controller_name/view_vin_sale/$vin->vinnumber_id")?>'>Part Sold</a></td>
				</tr>
			<?php
				}
			}
			?>
		</tbody>
	</table>
	<!-- BOTTOM PAGINATION -->
	<div style="float:left;width:100%;height:25px;">
	<?php 
			$val=10;
			if (isset($_GET['p_num'])) {
				$val = $_GET['p_num'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
	<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div style="float:left;width:50%;">
		Show
		<select id="per_page" class="per_page" name="per_page">
			<?php 
				$val=10;
				if (isset($_GET['p_num'])) {
					$val = $_GET['p_num'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div>
	<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	</div>
	<!-- END BOTTOM PAGINATION -->
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
	$('.per_page').change(function(){
		var per = $(this).val();
		var make=$('input[name="ckmake"]:checked').val();
		var model=$('input[name="ckmodel"]:checked').val();
		// var color=$('input[name="ckcolor"]:checked').val();
		var year=''
		$('input[name="ckyear"]:checked').each(function(){
			year+=$(this).val()+'_';
		})
		var in_color=''
		$('input[name="ck-incolor"]:checked').each(function(){
			in_color+=$(this).val()+'_';
		})
		var ex_color=''
		$('input[name="ck-excolor"]:checked').each(function(){
			ex_color+=$(this).val()+'_';
		})
		location.href="<?php echo site_url('vinnumbers/search_vin');?>?n="+name+"&m="+make+"&mo="+model+"&y="+year+"&in_c="+in_color+'&ex_c='+ex_color+'&p_num='+per;
		
	})
</script>