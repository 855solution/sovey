<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('makes/save/'.$make_info->make_id,array('id'=>'make_form'));
?>
<fieldset id="make_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("makes_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('makes_make_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'make_name',
		'id'=>'make_name',
		'value'=>$make_info->make_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('makes_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$make_info->description)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#make_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_make_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			make_name:"required"
   		},
		messages:
		{
			make_name:"<?php echo $this->lang->line('makes_name_required'); ?>",	
		}
	});
});
</script>