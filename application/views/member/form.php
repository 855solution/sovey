<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	ul,ol{
		margin-bottom: 0px !important;
	}
    .saveloading{width:35px;}
	a{
		cursor: pointer;
	}
	.datepicker {z-index: 9999;}
</style>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php

echo form_open('member/save/'.$page_info->member_id,array('id'=>'model_form'));
?>
<fieldset id="model_basic_info" style="padding: 5px;">
<legend>Page Information</legend>

<div class="field_row clearfix">
<?php echo form_label('Last Name:', 'url',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'class'=>'required',
		'id'=>'first_name',
		'value'=>$page_info->last_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('First Name:', 'url',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'class'=>'required',

		'id'=>'first_name',
		'value'=>$page_info->first_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Gender:', 'url',array('class'=>'wide')); ?>
	<div class='form_field'>
		<select id="gender" name="gender" value="$page_info->first_name">
			<option value="Male">Male</option>
			<option value="FeMale">FeMale</option>
		</select>
	
	</div>
</div>




<div class="field_row clearfix">
<label class='wide'>Address</label>
	<div class='form_field'>
	<textarea name='address' id='address'><?php if(isset($page_info)) echo $page_info->address ?></textarea>
	</div>
</div>
<div class="field_row clearfix">


<div class="field_row clearfix">
<?php echo form_label('Email:', 'url',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'class'=>'required',
		'id'=>'email',
		'value'=>$page_info->email)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<label class='wide'>is Spacial</label>
	<div class='form_field'>
		<label><input type='checkbox' name='is_active' id='is_active' <?php if(isset($page_info)) if($page_info->is_sp==1) echo "checked"; ?>/> is_sp </label>
	</div>
</div>

<?php if($page_info->member_id==''){ ?>
	<div class="field_row clearfix">
	<?php echo form_label('Password:', 'url',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'password',
			'class'=>'required',

			'type'=>'password',
			'id'=>'password')
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Confirm-Password:', 'url',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'c_password',
			'type'=>'password',
			'class'=>'required',

			'id'=>'c_password',)
		);?>
		</div>
	</div>
<!-- <div class="field_row clearfix"> -->

<?php } else{ ?>
	<div class="field_row clearfix">
	<?php echo form_label('New Password:', 'url',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'new_password',
			'type'=>'password',
			'id'=>'new_password')
		);?>
		</div>
	</div>

<?php }
// echo form_submit(array(
// 	'name'=>'submit',
// 	'id'=>'submit',
// 	'value'=>$this->lang->line('common_submit'),
// 	'class'=>'submit_button float_left')
// );
?>
<input type="button" class="submit_button float_left" id='submit_page' name='submit' value='Submit'>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function PreviewImage(event) {
        var uppreview=$(event.target).attr('rel');
        //alert(uppreview);
        var upimage=$(event.target).attr('id');
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(upimage).files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById(uppreview).src = oFREvent.target.result;
             document.getElementById(uppreview).style.backgroundImage = "none";
        };
    }
$(document).ready(function(){

	// loadEditor('descr');
	
	$('#submit_page').click(function(){
		// var data = CKEDITOR.instances.descr.getData();

		// $('#descr').text(data);
		var pwd=$("#password").val();
		var fpwd=$("#c_password").val();
		if(pwd==fpwd)
			$('#model_form').submit();
		else
			alert("Password Not match...");
		
	})
	
	$('#model_form').validate({
		// var data = CKEDITOR.instances.descr.getData();
		
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
				success:function(response)
				{
					tb_remove();
					post_model_form_submit(response);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			required:"required",
			
   		},
		messages:
		{
			required:"Input to continue",	
			
		}
	});
});
// CKEDITOR.replace( 'descr',
//     {
//         filebrowserBrowseUrl :"<?php echo base_url();?>ckeditor/ckfinder/ckfinder.html?Connector=ckeditor/ckfinder/core/connectors/php/connector.php",
//         filebrowserImageBrowseUrl : "<?php echo base_url();?>ckeditor/ckfinder/ckfinder.html?Type=Images&Connector=ckeditor/ckfinder/core/connectors/php/connector.php",
//         filebrowserFlashBrowseUrl :"<?php echo base_url();?>ckeditor/ckfinder/ckfinder.html?Type=Flash&Connector=ckeditor/ckfinder/core/connector/php/connector.php",
//         filebrowserUploadUrl  :"<?php echo base_url();?>ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=File",
//         filebrowserImageUploadUrl : "<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Image",
//         filebrowserFlashUploadUrl : "<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash"
            
//       }); 
// CKEDITOR.replace('descr',{
// 	filebrowserBrowseUrl: "<?php echo base_url(); ?>ckfinder/ckfinder.html?resourceType=Files"
// });
// CKFinder.setupCKEditor();

// function loadEditor(id)
// {
//     var instance = CKEDITOR.instances[id];
//     if(instance)
//     {
//         CKEDITOR.remove(instance);
//     }
   
// }


</script>