<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo $this->lang->line('items_generate_barcodes'); ?></title>

</head>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/print.css" media="print"/>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos.css" />
<body>
<center>
<div style="width:260px; height:auto;">
<?php 
$count = 0;
for($i=0;$i<$barcodetime;$i++){
foreach($items as $item)
{
	$name 	= $item['name'];
	$barcode = $item['barcode'];
	$text = $item['barcode'];
	$item_id = $item['item_id'];
	$data=$this->db->query("SELECT * FROM ospos_subpartbarcode WHERE partbars_id='$item_id'")->result();
	//if ($count % 1 ==0 and $count!=0)
	//{
		//echo '</tr><tr>';
	//}
	//$image=$this->Item->getdefaultimg($item_id);
	foreach ($data as $s) {
			$name 	= $s->name;//$item['name'];
			$barcode = $s->item_number;//$item['barcode'];
			$text = $s->item_number;//$item['barcode'];
			$item_id = $s->subpartbar_id;//$item['item_id'];
			$path=site_url('../assets/site/image/no_img.png');
			if(file_exists(FCPATH."uploads/partbar/thumb/".$item_id.'.jpg')){
				$path=site_url("../uploads/partbar/thumb/".$item_id.'.jpg');
			}
			echo "<div style='margin-left:8px; width:360px; height:140px; color:#000;'>
					<div style='text-align:left;margin-left:8px;'>[ $item_id ] $name</div>
					<div style='width:100px; float:left;'><img src='".$path."' style='width:100%;height:100px;'/></div>
					<div style='width:260px; float:left;'><img src='".site_url()."/barcode?barcode=$barcode&text=$text&width=256' /></div>
				</div>";
			$count++;# code...
	}

}
}
?>
<div class="hideforprint" style=" width:356px; height:auto; margin-top:25px; float:left;">
    <div style="font-size:14px;">
    	<a href="javascript:window.print()" style="color:#0033CC; padding-right:5px;">
        	Print
        </a>
        &nbsp;&nbsp;|&nbsp;&nbsp; 
        <a href="<?php echo site_url("items/view/-1/width:360");?>">Add New Items</a> 
        &nbsp;&nbsp;|&nbsp;&nbsp; 
        <a href="<?php echo site_url("items");?>">Items Listing</a> 
        &nbsp;&nbsp;|&nbsp;&nbsp; 
        <a href="<?php echo site_url("home");?>">Home</a>
	</div>        
</div>
</div>

</center>
</body>
</html>