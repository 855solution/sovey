   <style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    ul,ol{
        margin-bottom: 0px !important;
    }
    #img_row{padding: 3px 0px !important}
    .hide{display: none !important;}
    .img_row>div{
        width: 45%;
        float: left;
    }
    .remove_img{
        margin-bottom:-23px;position: relative; margin-left:-6px !important;
        width: 24px;
    }
    .top_action_button img{width: 28px; margin-top: 5px;}
    .saveloading{width:35px;}
    .remove_img:hover{
        cursor: pointer;
    }
    a{
        cursor: pointer;
    }
    .previewupl:hover{
        cursor: pointer;
    }
    .datepicker {z-index: 9999;}
</style>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<script language="JavaScript" type="text/javascript">
			function PreviewImage(event) {
        var uppreview=$(event.target).attr('rel');
        //alert(uppreview);
        var upimage=$(event.target).attr('id');
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(upimage).files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById(uppreview).src = oFREvent.target.result;
             document.getElementById(uppreview).style.backgroundImage = "none";
        };
    }


   // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

				$(function()
				{
					$('#make_id').chainSelect('#model_id','<?php echo site_url('items/combobox') ?>',
					{ 
						before:function (target) //before request hide the target combobox and display the loading message
						{ 
							$("#loading").css("display","block");
							$(target).css("display","none");
						},
						after:function (target) //after request show the target combobox and hide the loading message
						{ 
							$("#loading").css("display","none");
							$(target).css("display","inline");
						}
					});
				});


            </script>
<?php

echo form_open_multipart('subpart/save/'.$partbar_info->subpartbar_id,array('id'=>'vinnumber_form','type'=>'POST'));

$img_path=base_url('assets/site/image/no_img.png');
  
    if(file_exists(FCPATH."/uploads/partbar/thumb/$partbar_info->subpartbar_id.jpg")){
      $img_path=base_url("/uploads/partbar/thumb/$partbar_info->subpartbar_id.jpg");
    }
?>
<fieldset id="vinnumber_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("vinnumbers_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label('Part Number:', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'part_number',
		'id'=>'part_number',
		'value'=>$partbar_info->item_number)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Main Part:', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
        <select name="partbar_id" id="partbar_id" required>
            <option value="">Please Select</option>
            <?php
            $mainpart=$this->db->query("SELECT * FROM ospos_partbarcode WHERE deleted=0 order by name asc")->result();
            foreach ($mainpart as $ma) {
                $sel='';
                if(isset($partbar_info->partbars_id))
                    if($partbar_info->partbars_id==$ma->partbar_id)
                        $sel='selected';
                echo "<option value='$ma->partbar_id' $sel>$ma->name </option>";
            }
             ?>
        </select>
    </div>
</div>
<!-- <div class="field_row clearfix">
<?php echo form_label('Part Name:', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_input(array(
        'name'=>'part_name',
        'id'=>'part_name',
        'value'=>$partbar_info->name)
    );?>
    </div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Make:', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
        <select name="make" id="make" onchange="getmodels(event);">
            <option value="">Please Select</option>
            <?php
            $make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0")->result();
            foreach ($make as $m) {
                $sel='';
                if(isset($partbar_info->make_id))
                    if($partbar_info->make_id==$m->make_id)
                        $sel='selected';
                echo "<option value='$m->make_id' $sel>$m->make_name</option>";
            }
             ?>
        </select>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Model:', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
        <select name="model_id" id="model_id">
            <option value="">Please Select</option>
            <?php
             if(isset($partbar_info->make_id)){
                    $model=$this->db->query("SELECT * FROM ospos_models WHERE make_id='$partbar_info->make_id' AND deleted=0")->result();
                    foreach ($model as $mo) {
                        $sel='';
                        if($partbar_info->model_id==$mo->model_id)
                                $sel='selected';
                        echo "<option value='$mo->model_id' $sel>$mo->model_name</option>";
                    }
             }
             ?>
        </select>
    </div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Year:', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_input(array(
        'name'=>'year',
        'id'=>'year',
        'value'=>$partbar_info->item_number)
    );?>
    </div>
</div>
 -->
<div class="field_row clearfix">
<?php echo form_label("image".':', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
        <img  onclick="$('#uploadImage').click();" src="<?php echo $img_path?>" id="uploadPreview" style='width:150px;border:solid 1px #CCCCCC; padding:3px;'>
        <input id="uploadImage" rel='uploadPreview' type="file" name="userfile" onchange="PreviewImage(event);" style="visibility:hidden; display:none" />
    </div>
</div>





<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function getmodels(event){
    var url="<?php echo site_url('site/getmodels')?>";
    var make_id=$('#make').val();
    $.ajax({
        url:url,
        type:"POST",
        datatype:"Json",
        async:false,
        data:{
                'make_id':make_id,
            },
        success:function(data) {
          $("#model_id").html(data);
         
        }
      })
}   
$(document).ready(function(){
    $('#vinnumber_form').validate({
        submitHandler:function(form)
        {
            $(form).ajaxSubmit({
            success:function(response)
            {
                tb_remove();
                post_model_form_submit(response);
            },
            dataType:'json'
        });
        },
        errorLabelContainer: "#error_message_box",
        wrapper: "li",
        rules:
        {
            slide_name:"required",
            
        },
        messages:
        {
            slide_name:"Input name to continue",    
            
        }
    });
});
</script>