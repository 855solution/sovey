<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('branchs/save/'.$branch_info->branch_id,array('id'=>'branch_form'));
?>
<fieldset id="branch_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("branchs_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('branchs_branch_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'branch_name',
		'id'=>'branch_name',
		'value'=>$branch_info->branch_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('branchs_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$branch_info->description)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#branch_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_branch_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			branch_name:"required"
   		},
		messages:
		{
			branch_name:"<?php echo $this->lang->line('branchs_name_required'); ?>",	
		}
	});
});
</script>