<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('categorys/save/'.$category_info->category_id,array('id'=>'category_form'));
?>
<fieldset id="category_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("categorys_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('categorys_category_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category_name',
		'id'=>'category_name',
		'value'=>$category_info->category_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('categorys_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$category_info->description)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#category_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_category_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			category_name:"required"
   		},
		messages:
		{
			category_name:"<?php echo $this->lang->line('categorys_name_required'); ?>",	
		}
	});
});
</script>