
<?php if ($part_info->approve==1): ?>

<form action="<?php echo site_url('template/add_to_item/'.$part_info->partid) ?>" method='post'>
    <div class="contain" style="border:1px solid #dadada;margin-top:1%;">
        <div class="field_row clearfix">
            <div class='form_field'>
                <label>Part Number</label><br>
                <input type="text" name="part_number" id="part_number" readonly="" value="<?php echo $part_number ?>">
            </div>
        </div>
        <div class="field_row clearfix">
            <div class='form_field'>
                <label>Head Number</label><br>
                <select name="head_number" id="head_number">
                    <option value="">Select</option>
                    <?php foreach ($vinnumber as $vin): ?>
                        <option value="<?php echo $vin->vinnumber_id ?>"><?php echo 'H'.$vin->vinnumber_id ?></option>
                    <?php endforeach ?>
                </select>
                
            </div>
        </div>
        <div class="field_row clearfix">
            <div class='form_field'>
                <label>Quantity</label><br>
                <input type="number" name="quantity" min="1" id="quantity" value="1">
            </div>
        </div>
        <div class="field_row clearfix">
            <div class='form_field' style="float:right;">
                <input type="submit" value="Submit" class="submit_button">
            </div>
        </div>
    </div>
    
</form> 

<?php else: ?>

<center><p style="color:red;">Part Number Not Approved!</p></center>

<?php endif ?>


<script type="text/javascript">
    $('#head_number').select2();
</script>