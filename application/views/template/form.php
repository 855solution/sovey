   <style type="text/css">
    table tbody tr td img{width: 20px; margin-right: 10px}
    ul,ol{
        margin-bottom: 0px !important;
    }
    #img_row{padding: 3px 0px !important}
    .hide{display: none !important;}
    .img_row>div{
        width: 45%;
        float: left;
    }
    .remove_img{
        margin-bottom:-23px;position: relative; margin-left:-6px !important;
        width: 24px;
    }
    .top_action_button img{width: 28px; margin-top: 5px;}
    .saveloading{width:35px;}
    .remove_img:hover{
        cursor: pointer;
    }
    a{
        cursor: pointer;
    }
    .hide{display: none;}
    .previewupl:hover{
        cursor: pointer;
    }
    .datepicker {z-index: 9999;}
    .select2-offscreen,
    .select2-offscreen:focus {
        clip: rect(0 0 0 0) !important;
    /*    The message is squashed and not seen.*/
    /*    width: 1px !important;*/
    /*    height: 1px !important;*/
        border: 0 !important;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden !important;
        position: relative !important;
        outline: 0 !important;
        left: 0px !important;
        top: 0px !important;
    }
    .left_side{
        width: 45%;
        float: left;
        border: 1px solid #dadada;
        padding: 1%;
    }
    .right_side{
        margin-left: 1%;
        width: 45%;
        float: left;
        border: 1px solid #dadada;
        padding: 1%;
    }
    .name_er{
        color: red;
    }

</style>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open_multipart('template/save/'.$make_info->partid,array('id'=>'make_form'));
?>
<input type="text" name="partid" style="display: none;" id="id" value="<?php echo $make_info->partid ;?>"></input>
<fieldset id="make_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("makes_basic_information"); ?></legend>
<div class="left_side">
    <div class="field_row clearfix">
    <?php echo form_label('Part Number:', 'name',array('class'=>'required wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'part_number',
            'id'=>'part_number',
            'required'=>'required',
            'value'=>$make_info->part_number)
        );?>
        </div>
        <p class="name_er">Part Number Exist !</p>
    </div>
    <!-- <div class="field_row clearfix hide">
    <?php echo form_label('Part Number:', 'name',array('class'=>'required wide hide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'part_number_old',
            'id'=>'part_number_old',
            'required',
            'value'=>$make_info->part_number)
        );?>
        </div>
    </div> -->
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_category').':', 'category',array('class'=>'required wide')); ?>
        <div class='form_field' style="position:relative">
        <?php //echo form_dropdown('category_id', $categorys, $selected_category,'id="category_id"');?>
            <select name="category_id" id="category_id" required>
                <option value="">Please Select</option>
                <?php
                $category=$this->db->query("SELECT * FROM ospos_categorys WHERE deleted=0 ORDER BY category_name ASC")->result();
                foreach ($category as $c) {
                    $sel='';
                    if(isset($make_info->category_id))
                        if($make_info->category_id==$c->category_id)
                            $sel='selected';
                    echo "<option value='$c->category_id' $sel>$c->category_name</option>";
                }
                 ?>
            </select>
        </div>
    </div>

     <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_khmername').':', 'khmername',array('class'=>'required wide')); ?>
        <div class='form_field' style="position:relative">
        <?php //echo form_dropdown('khmername_id', $khmernames, $selected_khmername,'id="khmername_id"');?>
            
            <?php $hide_name = 'hide' ;
                if ($selected_khmername) {
                    $hide_name ='';
                }
            ?>               
            <div style="margin-right:20px" class="name_div <?php echo $hide_name ?>">
                <select name="khmername_id" id="khmername_id" style="width:250px;" required>
                    <option value="">Please Select</option>
                    <?php
                    $khname=$this->db->query("SELECT * FROM ospos_khmernames WHERE deleted=0 ORDER BY khmername_name ASC")->result();
                    foreach ($khname as $kh) {
                        $sel='';
                        if(isset($make_info->khmernames_id))
                            if($make_info->khmernames_id==$kh->khmername_id)
                                $sel='selected';
                        echo "<option value='$kh->khmername_id' $sel>$kh->khmername_name / $kh->english_name</option>";
                    }
                     ?>
                </select>
            </div>
            <div class="select_btn">
                <a class="" title="Select Name" onclick="open_name_select()">
                    <div class="small_button">
                        <span name="btnselectname" id="btnselectname" style="font-size:10px;">Select Name</span>
                    </div>
                </a>
            </div>
        </div>

    </div>
    <div class="field_row clearfix">
    <?php echo form_label('Fitment:', 'fitment',array('class'=>'wide')); ?>
    <div class="form_field">
        <div class="fake_fitment hide">
            <a class="" title="Fitting">
                <div class="small_button_disabled">
                    <span name="btnfit" id="btnfit" style="font-size:10px;">Fitment</span>
                </div>
            </a>
        </div>
        <div class="real_fitment">
            <a class="" title="Fitting" onclick="open_fit()">
                <div class="small_button">
                    <span name="btnfit" id="btnfit" style="font-size:10px;">Fitment</span>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Make:', 'name',array('class'=>'wide')); ?>
    <div class='form_field'>
        <select name="make_id" id="make_id" class='make_id' onchange="getmodels(event);">
            <option value="">Please Select</option>
            <?php
            $make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0 ORDER BY make_name ASC")->result();
            foreach ($make as $m) {
                $sel='';
                $dis = "";
                if(isset($make_info->make_id))
                    if($make_info->make_id==$m->make_id)
                        $sel='selected';
                        $dis="disabled";
                echo "<option value='$m->make_id' $sel>$m->make_name</option>";
            }
             ?>
        </select>
    </div>
</div>

 <div class="field_row clearfix">
    <?php echo form_label('Model:', 'name',array('class'=>'wide')); ?>
        <div class='form_field'>
            <select name="model_id" id="model_id" class="model_id">
                <option value="">Please Select</option>
                <?php
                 // if(isset($item_info->make_id)){
                   
                    
                        $model=$this->db->query("SELECT * FROM ospos_models WHERE deleted=0 ORDER BY model_name ASC")->result();
                        foreach ($model as $mo) {
                            $sel='';
                            if($make_info->model_id==$mo->model_id)
                                    $sel='selected';
                            echo "<option value='$mo->model_id' rel='$mo->make_id' $sel>$mo->model_name</option>";
                        }
                 // }
                 ?>
            </select>
        </div>
    </div>
  <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_year').' From :', 'year',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php 
            define (MINY,1980);
            $year_from = $make_info->year;
        ?>
        <select name="year" id="year" class="year">
                <!-- <option value="<?php echo $year_temp; ?>"><?php echo $year_temp; ?></option> -->
            <?php 
                for($j=date('Y');$j>=MINY;$j--){
                    $sel='';
                    if($year_from==$j)
                        $sel='selected';
                    echo '<option value="'.$j.'" '.$sel.'>'.$j.'</option>';
                }
            ?>                    
        </select>
        </div>
    </div>
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_year').' to :', 'year',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php 
            define (MINY,1980);
            $year_to = $make_info->year_to;
        ?>
        <select name="year_to" id="year_to" class="year_to">
                <!-- <option value="<?php echo $year_temp; ?>"><?php echo $year_temp; ?></option> -->
            <?php 
                for($j=date('Y');$j>=MINY;$j--){
                    $sel='';
                    if($year_to==$j)
                        $sel='selected';
                    // echo $year_temp;
                    echo '<option value="'.$j.'" '.$sel.'>'.$j.'</option>';
                }
            ?>                    
        </select>
        </div>
    </div>

    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_color').':', 'color',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php //echo form_dropdown('color_id', $colors, $selected_color,'class="color_id"');?>
            <select name="color_id" id="color_id">
                <option value="">Please Select</option>
                <?php
                $color=$this->db->query("SELECT * FROM ospos_colors WHERE deleted=0 ORDER BY color_name ASC")->result();
                foreach ($color as $co) {
                    $sel='';
                    if(isset($make_info->color_id))
                        if($make_info->color_id==$co->color_id)
                            $sel='selected';
                    echo "<option value='$co->color_id' $sel>$co->color_name</option>";
                }
                 ?>
            </select>
        </div>
    </div>
    
    
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_cost_price').':', 'cost_price',array('class'=>'required wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'cost_price',
            'size'=>'12',
            'id'=>'cost_price',
            'required'=>'required',
            'value'=>$make_info->cost_price)
        );?>
        </div>
    </div>

    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_unit_price').' From:', 'unit_price',array('class'=>'required wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'unit_price',
            'size'=>'12',
            'id'=>'unit_price',
            'required'=>'required',
            'value'=>$make_info->unit_price)
        );?>
        </div>
    </div>
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_unit_price').' To:', 'unit_price_to',array('class'=>'required wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'unit_price_to',
            'size'=>'12',
            'id'=>'unit_price_to',
            'required'=>'required',
            'value'=>$make_info->unit_price_to)
        );?>
        </div>
    </div>
</div>


<!-- <div class="field_row clearfix">
<?php echo form_label('Part name:', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'part_name',
		'id'=>'part_name',
		'value'=>$make_info->part_name)
	);?>
	</div>
</div> -->

<!-- <div class="field_row clearfix">
<?php echo form_label('Fit:', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'fit',
		'id'=>'fit',
		'value'=>$make_info->fit)
	);?>
	</div>
</div> -->
<div class="right_side">
    
    <!-- BODY -->
    <div class="field_row clearfix">
        <label class="wide">Body:</label>
        <div class='form_field'>
            <select name="vin_body" class="body_sel">
                <option value="">Please Select</option>
                <?php
                    $sele='';
                    $body = $this->db->query("SELECT * FROM ospos_body WHERE deleted=0 ORDER BY body_name ASC")->result();
                    foreach ($body as $bo) {
                        if($make_info->body==$bo->body_id){
                            $sele="selected";
                        }else{
                            $sele="";
                        }
                    ?>

                    <option value="<?php echo $bo->body_id ?>" <?php echo $sele ?>><?php echo $bo->body_name ?></option>
                <?php
                    }
                ?>
            </select>
        </div>
    </div>
    <!-- ENGINE -->
    <div class="field_row clearfix">
        <label class="wide">Engine:</label>
        <div class='form_field'>
            <select name="vin_engine" class="engine_sel">
                <option value="">Please Select</option>
                <?php
                    $sele='';
                    $engine = $this->db->query("SELECT * FROM ospos_engine WHERE deleted=0 ORDER BY engine_name ASC")->result();
                    foreach ($engine as $en) {
                        if(isset($make_info->engine)){
                            if ($make_info->engine==$en->engine_id) {
                               $sele="selected";
                            }else{
                                $sele="";
                            }
                            
                        }
                    ?>
                    <option value="<?php echo $en->engine_id ?>" <?php echo $sele ?>><?php echo $en->engine_name ?></option>
                <?php
                    }

                ?>
            </select>
        </div>
    </div>
    <!-- TRANSMISSION -->
    <div class="field_row clearfix">
        <label class="wide">Transmission:</label>
        <div class='form_field'>
            <select name="vin_trans" class="trans_sel">
                <option value="">Please Select</option>
                <?php
                    $sele='';
                    $trans = $this->db->query("SELECT * FROM ospos_transmission WHERE deleted=0 ORDER BY transmission_name ASC")->result();
                    foreach ($trans as $tra) {
                        if($make_info->transmission==$tra->trans_id){
                            $sele="selected";
                        }else{
                            $sele="";
                        }
                    ?>

                    <option value="<?php echo $tra->trans_id ?>" <?php echo $sele ?>><?php echo $tra->transmission_name ?></option>
                <?php
                    }

                ?>
            </select>
        </div>
    </div>
    <!-- TRIM -->
    <div class="field_row clearfix">
        <label class="wide">Trim:</label>
        <div class='form_field'>
            <select name="vin_trim" class="trim_sel">
                <option value="">Please Select</option>
                <?php
                    $sele='';
                    $trim = $this->db->query("SELECT * FROM ospos_trim WHERE deleted=0 ORDER BY trim_name ASC")->result();
                    foreach ($trim as $tri) {
                        if($make_info->trim==$tri->trim_id){
                            $sele="selected";
                        }else{
                            $sele='';
                        }
                    ?>

                    <option value="<?php echo $tri->trim_id ?>" <?php echo $sele ?>><?php echo $tri->trim_name ?></option>
                <?php
                    }

                ?>
            </select>
        </div>
    </div>
    <!-- FUEL -->
    <div class="field_row clearfix">
        <label class="wide">Fuel:</label>
        <div class='form_field'>
            <select name="vin_fuel" class="fuel_sel">
                <option value="">Please Select</option>
                <?php
                    $sele='';
                    $fuel = $this->db->query("SELECT * FROM ospos_fuel WHERE deleted=0 ORDER BY fule_name ASC")->result();
                    foreach ($fuel as $fu) {
                        if($make_info->fuel==$fu->fuel_id){
                            $sele="selected";
                        }else{
                            $sele="";
                        }
                    ?>

                    <option value="<?php echo $fu->fuel_id ?>" <?php echo $sele ?>><?php echo $fu->fule_name ?></option>
                <?php
                    }

                ?>
            </select>
        </div>
    </div>
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_part_placement').':', 'partplacement',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php //echo form_dropdown('partplacement_id', $partplacements, $selected_partplacement,'class="partplacement_id"');?>
            <select name="partplacement_id" id="partplacement_id">
                <option value="">Please Select</option>
                <?php
                $placement=$this->db->query("SELECT * FROM ospos_partplacements WHERE deleted=0 ORDER BY partplacement_name ASC")->result();
                foreach ($placement as $place) {
                    $sel='';
                    // if(isset($make_info->part_placement))
                        if($make_info->part_placement==$place->partplacement_id)
                            $sel='selected';
                    echo "<option value='$place->partplacement_id' $sel>$place->partplacement_name</option>";
                }
                 ?>
            </select>
        </div>
    </div>
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_condition').':', 'condition',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php //echo form_dropdown('condition_id', $conditions, $selected_condition,'class="condition_id"');?>
            <select name="condition_id" id="condition_id">
                <option value="">Please Select</option>
                <?php
                $condic=$this->db->query("SELECT * FROM ospos_conditions WHERE deleted=0 ORDER BY condition_name ASC")->result();
                foreach ($condic as $con) {
                    $sel='';
                    if(isset($make_info->codition_id))
                        if($make_info->codition_id==$con->condition_id)
                            $sel='selected';
                    echo "<option value='$con->condition_id' $sel>$con->condition_name</option>";
                }
                 ?>
            </select>
        </div>
         
    </div>
    
    <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_branch').':', 'branch',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php //echo form_dropdown('branch_id', $branchs, $selected_branch,'class="branch_id"');
          $disabled = "";
            if(isset($make_info->brand_id))
            {
                $disabled = "disabled";
            }
        ?>
            <select name="branch_id" id="branch_id">
                <option value="">Please Select</option>
                <?php
                $brand=$this->db->query("SELECT * FROM ospos_branchs WHERE deleted=0 ORDER BY branch_name ASC")->result();
                foreach ($brand as $br) {
                    $sel='';
                    // if(isset($make_info->brand_id))
                        if($make_info->brand_id==$br->branch_id)
                            $sel='selected';
                    echo "<option value='$br->branch_id' $sel>$br->branch_name</option>";
                }
                 ?>
            </select>
        </div>
    </div>

    <!-- LOCATION -->

    <!-- <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_location').':', 'location',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php //echo form_dropdown('location_id', $locations, $selected_location,'class="location_id"');
            $disabled = "";
            if(isset($make_info->location_id))
            {
                $disabled = "disabled";
            }
        ?>
            <select name="location_id" id="location_id">
                <option value="">Please Select</option>
                <?php
                $location=$this->db->query("SELECT * FROM ospos_locations WHERE deleted=0")->result();
                foreach ($location as $loca) {
                    $sel='';
                    if(isset($make_info->location_id))
                        if($make_info->brand_id==$loca->location_id)
                            $sel='selected';
                    echo "<option value='$loca->location_id' $sel>$loca->location_name</option>";
                }
                 ?>
            </select>
        </div>
    </div> -->

    
    
    

    <!-- <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_quantity').':', 'quantity',array('class'=>'required wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'quantity',
            'id'=>'quantity',
            'size'=>'12',
            'required'=>'required',
            'value'=>$make_info->quantity
        )
        );?>
        </div>
    </div> -->
    <div class="field_row clearfix">
        <div class='form_field'>
        <?php echo form_checkbox(array(
            'name'=>'approve',
            'id'=>'approve',
            'checked'=>($make_info->approve)? 1  :0)
        );?>
        </div>
        Approve Template
    </div>
    <div id='blog_img'> 
        <input type='text' name='deleteimg' style='display:none' id='deleteimg'/>
               
        <?php  
            $img_path=base_url('assets/site/image/no_img.png'); 
            $imageid=0;
            if(isset($make_info->partid)){
                $row=$this->db->query("SELECT * FROM ospos_template_images WHERE template_id='".$make_info->partid."' Order by tem_image_id desc limit 1")->row();
                if(isset($row->tem_image_id)){
                    if(file_exists(FCPATH."/uploads/thumb/".$row->image_name)){
                            $img_path=base_url("/uploads/thumb/".$row->image_name.'?'.rand(0,999));
                    }
                    $imageid=isset($row->tem_image_id)? $row->tem_image_id : 0;
                }
                   
            }
        ?>
            <div style='width:400px;' id='img_row'>
                <div class="img" style="width:130px; float:left"> 
                    <img onclick='remove_img(event);' rel='<?php echo $imageid ?>' class='remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>

                    <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                    <input id='uploadImage' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                </div>
                <div class='txt_order' style="width:130px; float:left; margin-left:30px; margin-top:18px;">
                    <label class='col-lg-1 control-label' >Order</label>
                    <div class=" col-lg-4"> 
                        <div class="col-md-12">
                            <input type="text"  class="form-control input-sm" id='order'  name="order[]" value="1" readonly="">
                        </div> 
                    </div>
                </div>
               
            </div> 
            <p style='clear:both;'></p>    
    </div>
    <?php
        $img_path=base_url('assets/site/image/no_img.png'); 

        echo form_submit(array(
            'name'=>'submit',
            'id'=>'submit',
            'value'=>$this->lang->line('common_submit'),
            'class'=>'submit_button float_left')
        );
    ?>
</div>


</fieldset>
<?php
echo form_close();
?>
<style type="text/css">
    .grey_out {
     background-color: blue !important;
    }
</style>
<script type='text/javascript'>
    $('#category_id').select2();
    $('#khmername_id').select2();
    $('.make_id').select2();
    $('.model_id').select2();
    $('.year').select2();
    $('#color_id').select2();
    $('#condition_id').select2();
    $('#partplacement_id').select2();
    $('#branch_id').select2();
    $('#location_id').select2();
    $('.year_to').select2();

    $('#select2-color_id-container').parent().css('background-color','#c1c1c1');
    $('#select2-condition_id-container').parent().css('background-color','#c1c1c1');
    $('#select2-branch_id-container').parent().css('background-color','#c1c1c1');


    function getmodels(event){
        var url="<?php echo site_url('site/getmodels')?>";
        var make_id=$(event.target).val();
        // alert(make_id);
        $.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{
                    'make_id':make_id,
                },
            success:function(data) {
                // alert(data);
              $(".model_id").html(data);
              // document.getElementById('model_id').html=data;
            }
          })
    }   
     // +++++++++++++++++++++ Image upload++++++++++++++++++
   function PreviewImage(event) {
        var uppreview=$(event.target).parent().parent().find('#imgpreview');
        var oFReader = new FileReader();
        oFReader.readAsDataURL($(event.target)[0].files[0]);

        oFReader.onload = function (oFREvent) {
            uppreview.attr('src',oFREvent.target.result);
            // uppreview.style.backgroundImage = "none";
        };
        $(event.target).parent().parent().find('.remove_img').removeClass('hide');
        $(event.target).removeClass('bl');
        var slideid=$(event.target).parent().parent().find('.remove_img').attr('rel');
        var del=$('#deleteimg').val()
        $('#deleteimg').val(del+','+slideid);
        addnewimg();
    };
    function addnewimg(){
        var exst=false;
        var ma=0;
        $('.uploadimg').each(function(){
            var ma_o=$(this).parent().parent().find('#order').val();
            var f = $(this)[0].files[0];
            // alert(f);
            if(ma<ma_o)
                ma=ma_o;
            if(f==undefined && $(this).hasClass('bl')){
                exst=true;
            }
        })
        // if(exst==false)
        //     $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
        //                                 "<div class='img' style='width:130px; float:left'> "+ 
        //                                     "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
        //                                     "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
        //                                     "<input id='uploadImage' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
        //                                     "<input type='text' value='' class='removeimgid hide' />"+
        //                                 "</div>"+
        //                                 "<div class='txt_order' style='width:130px; float:left; margin-left:30px; margin-top:18px;'>"+
        //                                     "<label class='col-lg-1 control-label' style='margin-top:15px !important;' >Order</label>"+
        //                                     "<div class='col-lg-4' style='margin-top:15px !important;''>"+ 
        //                                         "<div class='col-md-12'>"+
        //                                             "<input type='text'  class='form-control input-sm order'  name='order[]' id='order' value='"+(Number(ma)+1)+"'> "+
        //                                         "</div>"+ 
        //                                     "</div>"+
        //                                 "</div>"+
        //                             "</div>"+
        //                             "<p style='clear:both;'></p>");
    }
       
    function choosimg(event){
        $(event.target).parent().parent().find('#uploadImage').click();
    }
    // +++++++++++++++++++++++++++++remove upload pic+++++++++++++++++++ 
    function remove_img(event){
        var picid=$(event.target).attr('rel');
        var no_im="<?php echo $img_path; ?>";
       
        var delete_img=$('#deleteimg').val();
        // msg=confirm("Are you sure, delete this picture?");
        // //alert(msg);
        // if(msg==true){
        //      $.post('index.php/template/deleteimage', {'imageid':picid},function(data){
                 $('#imgpreview').attr('src',no_im);
        //      }); 
        // }
        if(picid!='')
            $('#deleteimg').val(delete_img+','+picid);

    }
    $('.imageDelete').click(function(event){
        var id= $(this).attr('title');
        var msg=false;
        msg=confirm("Are you sure, delete this picture?");
        //alert(msg);
        if(msg==true){
            $(this).prev().andSelf().remove();  
             $.post('index.php/template/deleteImage', {'imageid':id}); 
            }
        });


$(document).ready(function(){
	$('#make_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_make_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			make_name:"required",
            part_number:"required"
   		},
		messages:
		{
            make_name:"<?php echo $this->lang->line('makes_name_required'); ?>",    
			part_number:"Part Number Required!",	
		}
	});
    // color_id
    // // alert($('#part_number').val());
    // if($('#id').val() ==''){
    //     $("#branch_id").attr('disabled',false);
    //     $("#color_id").attr('disabled',false);
    //     $("#location_id").attr('disabled',false);
    //     $("#partplacement_id").attr('disabled',false);
    //     $("#condition_id").attr('disabled',false);
    //     $("#cost_price").attr('disabled',false);
    //     $("#unit_price").attr('disabled',false);
    //     $("#unit_price_to").attr('disabled',false);
    //     $("#quantity").attr('disabled',false);
    // }else
    // {
    //    $('.make_id').attr('disabled',true);
    //    $('.model_id').attr('disabled',true);
    //    $('.year').attr('disabled',true);
    //    $('.year_to').attr('disabled',true);
    //    $('#color_id').attr('disabled',true);
    //    $('.body_sel').attr('disabled',true);
    //    $('#condition_id').attr('disabled',true);
    //    $('#branch_id').attr('disabled',true);
    //    $('#cost_price').attr('disabled',true);
    //    $('#unit_price').attr('disabled',true);

    // }
    
    // //$("#branch_id").attr('disabled','disabled');
    // $('.big_button').click(function(){
    //   // $('#branch_id').removeAttr("disabled");
    //     $("#branch_id").Attr('disabled','disabled');
    //     alert('Hoo');
    //     });
});

    $('.year_to').on("change",function(){
        var yf = $('#year').val();
        var yt = $(this).val();
        if (yf>yt) {
            alert("Year From can't be Smaller");
            $(this).val(yf).change();
        };
    });
    $('.year').on("change",function(){
        var yt = $('.year_to').val();
        var yf = $(this).val();
        if (yf>yt) {
            alert("Year From can't be Smaller");
            $(this).val(yt).change();
        };
    });
</script>
<!-- NAME BOX -->
<style type="text/css">
    #name_box_2{
    display: none;
    width: 100%;
    background: #0003;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 9999;
  }
  .table_wrapper_2{
    width: 50%;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background: white;
    padding: 1%;
    border: 7px solid #b2b2b2;
    box-shadow: 0px 0px 10px 3px;
    overflow-y: scroll;
    height: 80%;
    z-index: 99999999999;
  }
  .close_btn_2{
    position: fixed;
    float: right;
    z-index: 99999999;
    background: white;
    left: 95%;
  }

</style>
<div id="name_box_2"  class='name_box_2' onclick="close_name_select(event,0);">
    <div class="table_wrapper_2">
      <div class="close_btn_2" >
        <span class='btn_x2' style="cursor:pointer;float:right;font-size: 30px;" onclick="close_name_select(event,0)" >&times</span>
      </div>
      <div id="title_bar">
        <div id="title" class="float_left">Name List</div>
        
      </div>

      <div id="table_action_header" style="   float: none;margin-bottom: 2%;width: 99%;">
        <div style="margin-top:-3px;margin-left:25px;width:40%;float:left;">
              <label>Search</label> 
                <input type="text" name ='isearch_2' id='isearch_2'/>
                  
        </div>
        <div style="margin-top:-3px;width:50%;float:right;display: inline-flex;">
            <input type="text" name='iname_2' id="iname_2" placeholder="New Name">
              <a>
                <div class="small_button">
                    <span class="btn_save_name_2" name="btn_save_name_2" id="btn_save_name_2" style="font-size:12px;">Save & Select</span>
                </div>
              </a>
        </div>
      </div>
      <div id="table_holder_2">
        <!-- TOP PAGINATION -->
        
        <!-- END TOP PAGINATION -->
        <table class="tablesorter table_data_2" id="sortable_table">
          <thead>
            <th>No</th>
            <th>Image</th>
            <th>Khmer Name</th>
            <th>English Name</th>
            <th>Description</th>
            <th>Action</th>
          </thead>
          <tbody>
            
          </tbody>
        </table>



        <!-- BOTTOM PAGINATION -->
        <div align="center" class="loading_ball" style="display:none;">
          <img src="<?php echo base_url('assets/loading/') ?>/balls.gif" width="30px">
        </div>
        <!-- END BOTTOM PAGINATION -->
      </div>
    </div>
</div>
<script type='text/javascript'>

$('#isearch_2').keyup(function(){
  var s = $('#isearch_2').val();
  search_name(s);
});
function search_name(s){
    $('.loading_ball').show();
  $.post("<?php echo site_url('khmernames/search_name_item') ?>",
    {
      search:s
    },
    function(data){
      $('#table_holder_2 .table_data_2 tbody').html(data.manage_table);
      $('.loading_ball').hide();
    },
    'Json'
    );
}
function open_name_select(){
        $('#name_box_2').show();
        // GET DATA
        search_name();
    }
function close_name_select(event,name_id){
  // console.log($(event.target));
  if ($(event.target).hasClass('name_box_2') || $(event.target).hasClass('btn_x2') || $(event.target).hasClass('select_btn')) {

    $('#name_box_2').hide();
    if ($(event.target).attr('rel')) {
       var name_id = $(event.target).attr('rel');
       var kh_name  = $(event.target).attr('kh');
       var en_name = $(event.target).attr('en');
       // console.log(name);
       select_name(name_id,kh_name,en_name);
    }else{
      // $('.name_div').addClass('hide');
      // $('#khmer_name_id').html('<option value="">'+name+'</option>');
      // $('#khmer_name_id').select2();
    };

  };
}

function select_name(name_id,kh_name,en_name){
  $('.name_div').removeClass('hide');
  $('#khmername_id').html('<option value='+name_id+' selected>'+kh_name+'/'+en_name+'</option>');
  $('#khmername_id').select2();
}
$('#btn_save_name_2').click(function(){
    var name = $('#iname_2').val();
    save_name_select(name);
});
function save_name_select(name){
    if (name!='') {
        $.post("<?php echo site_url('khmernames/save_other') ?>",
        {
          s_name:name
        },
        function(data){
          // $('#table_holder_2 .table_data_2 tbody').html(data.manage_table);
          $('#name_box_2').hide();

            select_name(data.kh_id,name);
            $('#iname_2').val('');
        },
        'Json'
        );
    }else{
        $("#iname_2").attr("placeholder", "");
        $("#iname_2").attr("placeholder", "Name Required !").val("").focus().blur();
    };
    
}
</script>
<!-- END BOX -->


<script type="text/javascript">
    $('.body_sel').select2();
    $('.trim_sel').select2();
    $('.trans_sel').select2();
    $('.engine_sel').select2();
    $('.fuel_sel').select2();
</script>













<!-- FITMENT POPUP -->
<style type="text/css">
    #fitment_box{
    display: none;
    width: 100%;
    background: #0003;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 9999;
  }
  .table_wrapper_fitment{
    width: 50%;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background: white;
    padding: 1%;
    border: 7px solid #b2b2b2;
    box-shadow: 0px 0px 10px 3px;
    overflow-y: scroll;
    height: 80%;
    z-index: 99999999999;
  }
  .close_btn_fitment{
    position: fixed;
    float: right;
    z-index: 99999999;
    background: white;
    left: 95%;
  }
  .fitment_box label{
    color: #7d7d7d;
    vertical-align: bottom;
    margin: 0 5px;
  }
  .add_fit_wrap{
    margin-bottom: 10px;
    width: 100%;
  }

</style>
<div id="fitment_box"  class='fitment_box' onclick="close_fit(event,0);">
    <div class="table_wrapper_fitment">
      <div class="close_btn_fitment" >
        <span class='btn_x2' style="cursor:pointer;float:right;font-size: 30px;" onclick="close_fit(event,0)" >&times</span>
      </div>
      <div id="title_bar">
        <div id="title" class="float_left">Template Fitment List</div>
        
      </div>

      <div id="table_action_header" style="float: none;margin-bottom: 2%;width: 99%;">
        <div style="margin-top:-3px;margin-left:25px;width:40%;float:left;">
              <label>Search Year</label> 
                <input type="text" name ='isearch_fitment' id='isearch_fitment'/>
                  
        </div>
        
      </div>
      <div class="add_fit_wrap">
          <div class="float_left">
            <label>Make</label>
            <?php $make_fit = $this->db->query("SELECT * FROM ospos_makes WHERE deleted=0")->result() ?>
              <select class="fit_make" id="fit_make">
                  <option value="">Please Select</option>

                  <?php foreach ($make_fit as $make): ?>
                      <option value="<?php echo $make->make_id ?>"><?php echo $make->make_name ?></option>
                  <?php endforeach ?>
              </select>
              <input type="hidden" name="def_make" id="def_make" class="def_make" value="">
          </div>
          <div class="float_left">
            <label>Model</label>

              <select class="fit_model" id="fit_model">
                  <option value="">Please Select Make</option>
              </select>
              <input type="hidden" name="def_model" id="def_model" class="def_model" value="">
          </div>
          <div class="float_left">
            <label>Year From</label>

              <?php 
                define (MINY,1980); 
            ?>
            <select name="year_fit" id="year_fit" class="year_fit" style="width:60px;">
                <?php 
                    for($j=date('Y');$j>=MINY;$j--){
                       
                        echo '<option value="'.$j.'" '.$sel.'>'.$j.'</option>';
                    }
                ?>                    
            </select>
              <input type="hidden" name="def_year" id="def_year" class="def_year" value="">
          </div>
          <div class="float_left">
            <label>Year To</label>

              <?php 
                define (MINY,1980); 
            ?>
            <select name="year_to_fit" id="year_to_fit" class="year_to_fit" style="width:60px;">
                <?php 
                    for($j=date('Y');$j>=MINY;$j--){
                       
                        echo '<option value="'.$j.'" '.$sel.'>'.$j.'</option>';
                    }
                ?>                    
            </select>
            <input type="hidden" name="def_year_to" id="def_year_to" class="def_year_to" value="">

          </div>
          <div class="float_left div_add_fit" style="margin-left:10px;">
              <a>
                <div class="small_button">
                    <span class="btn_add_fit" name="btn_add_fit" id="btn_add_fit" style="font-size:12px;" onclick="add_fitment()">Add Fitment</span>
                </div>
              </a>
          </div>
      </div>
      
      <div id="table_holder_fitment">
        <!-- TOP PAGINATION -->
        
        <!-- END TOP PAGINATION -->
        <table class="tablesorter table_data_fitment" id="sortable_table">
          <thead>
            <th>No</th>
            <th>Make Name</th>
            <th>Model Name</th>
            <th>Year From</th>
            <th>Year To</th>
            <th>Action</th>
          </thead>
          <tbody>
            
          </tbody>
        </table>



        <!-- BOTTOM PAGINATION -->
        <div align="center" class="loading_ball" style="display:none;">
          <img src="<?php echo base_url('assets/loading/') ?>/balls.gif" width="30px">
        </div>
        <!-- END BOTTOM PAGINATION -->
      </div>
    </div>
</div>
<script type='text/javascript'>
$('.year_to_fit').on("change",function(){
        var yf = $('.year_fit').val();
        var yt = $(this).val();
        if (yf>yt) {
            alert("Year From can't be Smaller");
            $(this).val(yf).change();
        };
    });
$('.year_fit').on("change",function(){

    var yt = $('.year_to_fit').val();
    var yf = $(this).val();
    if (yf>yt) {
        alert("Year From can't be Smaller");
        $(this).val(yt).change();
    };
});

$('.fit_make').change(function(){
    get_model_fit($('.fit_make').val());
});
function get_model_fit(make_id){
    $.post("<?php echo site_url('template/get_model_fit')?>",
        {
            make_id:make_id
        },
        function(d){
            $('.fit_model').html(d.model_data);
        },'Json');
};
function open_fit(){
        $('#fitment_box').show();
        $('.fit_make').select2();
        $('.fit_model').select2();
        $('.year_fit').select2();
        $('.year_to_fit').select2();
        // SET DEFAULT VALUE TO HIDDEN INPUT
        $('#def_make').val($('#make_id').val());
        $('#def_model').val($('#model_id').val());
        $('#def_year').val($('#year').val());
        $('#def_year_to').val($('.year_to').val());

        // GET DATA
        search_fitment();
    }
function close_fit(event,name_id){
    // console.log($(event.target));
  if ($(event.target).hasClass('fitment_box') || $(event.target).hasClass('btn_x2') || $(event.target).hasClass('select_btn')) {

    $('#fitment_box').hide();
   

  };
  
}


$('#isearch_fitment').keyup(function(){
  var s = $('#isearch_fitment').val();
  search_fitment(s);
});
function search_fitment(s){
    partid = $('#id').val();
    $('.loading_ball').show();
  $.post("<?php echo site_url('template/get_part_fitment') ?>",
    {
      search:s,
      partid:partid
    },
    function(data){
      $('#table_holder_fitment .table_data_fitment tbody').html(data.fitment_data);
      $('.loading_ball').hide();
    },
    'Json'
    );
}
function delete_fitment(id){
    if (confirm('Delete ?')) {
        $.post("<?php echo site_url('template/delete_fitment')?>",
        {
            pfid:id
        },function(d){
            search_fitment();
        },'Json');
    };
    
}
function add_fitment(){
    var partid = $('#id').val();
    var make_id = $('#fit_make').val();
    var model_id =$('#fit_model').val();
    var year = $('#year_fit').val();
    var year_to = $('#year_to_fit').val();
    var def_make = $('#def_make').val();
    var def_model = $('#def_model').val();
    var def_year = $('#def_year').val();
    var def_year_to = $('#def_year_to').val();

    $.post("<?php echo site_url('template/add_fitment')?>",
    {
        partid:partid,
        fit_make:make_id,
        fit_model:model_id,
        fit_year:year,
        fit_year_to:year_to,
        def_make:def_make,
        def_model:def_model,
        def_year:def_year,
        def_year_to:def_year_to
    },function(d){  
        // console.log(d);
        search_fitment();
    },"Json")
}

</script>
<!-- END FITMENT POPUP -->

<script type="text/javascript">
        check_part_number();

    function check_part_number(){
        var p = $('#part_number').val();
        var pid = $('#id').val();
        p = p.replace(/\s+/g, '');
        $('#part_number').val(p);

        $.ajax({
            url:"<?php echo site_url('template/check_part') ?>",
            type:"POST",
            dataType:"Json",
            data:{
                pn:p,
                pid:pid
            },
            success:function(re){
                if (re==1) {
                    $('.name_er').removeClass('hide');
                }else{
                    $('.name_er').addClass('hide');
                }
            }
        })
    }

    $('#part_number').keyup(function(){
        check_part_number();
    });
    $('#part_number').change(function(){
        check_part_number();

    })
</script>