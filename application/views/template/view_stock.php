<div id="title_bar">
  <div id="title" class="float_left">
    <h5>Part Number: <?php echo $partnumber ?> (<?php echo count($stock_data)>0?count($stock_data):0 ?>)</h5>
    
  </div>
</div>

<?php if (!$stock_data){ ?>
<div align="center" class="alert alert-warning"> No Data </div>

<?php }else{ ?>
<div style="margin-bottom:30px">
   <table class="tablesorter">
       <thead>
           <th>No</th>
           <th>Name</th>
           <th>Make</th>
           <th>Model</th>
           <th>Year</th>
           <th>Unit Price</th>
           <th>Quantity</th>
       </thead>
       <tbody>
           <?php 
            $i=0;
            foreach ($stock_data as $s) {
              $i++;
                ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $s->khmer_name ?> / <?php echo $s->name ?></td>
                    <td><?php echo $s->make_name ?></td>
                    <td><?php echo $s->model_name ?></td>
                    <td><?php echo $s->year ?></td>
                    <td><?php echo $s->unit_price ?></td>
                    <td><?php echo $s->quantity ?></td>
                </tr>
                <?php
            }
            ?>
       </tbody>
   </table>
</div>
<?php } ?>