<div>
<p style="color:#038ee2;">Available in Stock [ <strong><?php echo $available_stock?$available_stock:'0' ?></strong> ]</p>
</div>
<?php if (!$history_data){ ?>
<div align="center" class="alert alert-warning"> No Sale Items </div>

<?php }else{ ?>
<div>
   <table class="tablesorter">
       <thead>
           <th>Sale Date</th>
           <th>Customer</th>
           <th>Employee</th>
           <th>Unit Price</th>
           <th>Quantity Purchased</th>
       </thead>
       <tbody>
           <?php 
            foreach ($history_data as $h) {
                $customer = $this->tem->get_person($h->customer_id);
                $emp = $this->tem->get_person($h->employee_id);
                ?>
                <tr>
                    <td><?php echo $h->sale_time ?></td>
                    <td><?php echo $customer->last_name.' '.$customer->first_name ?></td>
                    <td><?php echo $emp->last_name.' '.$emp->first_name ?></td>
                    <td>$<?php echo number_format($h->item_unit_price,2) ?></td>
                    <td><?php echo $h->quantity_purchased ?></td>
                </tr>
                <?php
            }
            ?>
       </tbody>
   </table>
</div>
<?php } ?>