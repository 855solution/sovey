<?php 
	// $url=site_url('template?p=');
	// if ($this->uri->segment(2)=='refresh') {
	// 	$url=site_url('template/refresh?p=');
	// }elseif($this->uri->segment(2)=='search'){
	// 	$s = $_GET['s'];
	// 	$url=site_url('template/search?s='.$s.'&p=');
	// }
 ?>
 
<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function()
{
    $('.vin_number').select2();
	
	search();
    $(document).on('click', '.pagenav', function(){
	    var page = $(this).attr("id");
		search(page);			
	});
	// $('#search').change(function(){
	// 	var s = $('.ac_over').text()
	// 	if (s=='') {return};
	// 	window.location='index.php/template/search?s='+s;

	// });
	// $('#table_holder').paginate();
	// var ses = "<?php echo $this->session->userdata('search') ?>";
	// if (ses) {
	// 	location.href='index.php/template/search';
	// };
	// $("#make_model_year_search").click(function(){
	// 	localStorage.clear();
	// });

    // init_table_sorting();
    // enable_select_all();
    // enable_checkboxes();
    // enable_row_selection();
    // enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    // enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');

	// localStorage.setItem("year", $('#year').val());
	// localStorage.setItem("yearto", $('#yearto').val());
	// localStorage.setItem("model_id", $('#model_id').val());
	// localStorage.setItem("make_id", $('#make_id').val());
	// localStorage.setItem("search_filter", $('#search_filter').val());

	$('#search').keypress(function (e) {

	    if (e.which == 13) {

	    	var s ;

	  //   	if ($('.ac_results').is(':visible')) {

			// 	s = $('.ac_results li').first().text();

			// }else{

			s = $('#search').val();

			// }



	        $('#search').val(s);



	        // if (s!='') {

				search(1,e,0,'sa','');




			// };

	    }

	});

});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				11: { sorter: false},
				12: { sorter: false},
				13: { sorter: false},
				0: {sorter: false}
			}
		});
	}
}

function post_make_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.make_id,get_visible_checkbox_ids()) != -1)
		{
			location.reload();
			update_row(response.make_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);
		}
		else //refresh entire table
		{
			do_search(true,function()
			{

				//highlight new row
				hightlight_row(response.make_id);
				set_feedback(response.message,'success_message',false);

			});
		}
	}
}


function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="block";
        }
}

</script>

<!-- Make javascript -->

 <script language="JavaScript" type="text/javascript">
				$(function()
				{
					$('#make_id').chainSelect('#model_id','<?php echo site_url('items/combobox') ?>',
					{ 
						before:function (target) 
						//before request hide the target combobox and display the loading message
						{ 
							$("#loading").css("display","block");
							$(target).css("display","none");
							
						},
						after:function (target) 
						//after request show the target combobox and hide the loading message
						{ 
							$("#loading").css("display","none");
							$(target).css("display","inline");
						}
						
					});
				});
            </script>
<!--  -->

</script>
<style type="text/css">
	.mark{
		background: rgb(217,174,179) !important;
	}
	.mark td{
		background: transparent !important;
	}
	#spa{
		width: 95%;
	}
	
	/*#spa span{
		margin-left: 5px;
		padding:0 !important;
	}*/
	#ispending td{

		background:rgba(255, 0, 0, 0.14);

	}

	#ispending:hover{

		background:#e9e9e9;

	}

	#spa span{

		/*padding: 0 1% !important;*/

	}

	#pagination{

		padding: 1% 0;

		float: right;

	}

	#pagination a{

		border: 1px solid #21759B;

		background: #FFF none repeat scroll 0% 0%;

		color: #21759B;

		border-radius: 5px;

		padding: 5px 10px;

	}

	#pagination .ui-state-disabled{

		border: 1px solid #21759B;

		background: #21759B none repeat scroll 0% 0%;

		color: white;

		border-radius: 5px;

		padding: 5px 10px;

		cursor:not-allowed;

	}

	#pagination span{

		border: 1px solid #21759B;

		background: #FFF none repeat scroll 0% 0%;

		color: #21759B;

		border-radius: 5px;

		padding: 5px 10px;

		cursor:not-allowed;



	}

	#pagination .dot{

		border: 1px solid #21759B;

		background: #FFF none repeat scroll 0% 0%;

		color: #21759B;

		border-radius: 5px;

		padding: 5px 10px;

	}

	#table_holder{

		width: 87%;

		float: right;

		margin-top: 0;

	}

	#table_action_header{

		width:86%;

		float: right;

	}

	.barcode_date{

		display: none;

		position: absolute;

		width: 190px;

		height: 110px;

		background: white;

		/*border: 1px solid;*/

		box-shadow: 0px 0px 5px;

		text-align: center;

		padding: 10px;

		border-radius: 5px;

		z-index: 99999;

	}

	.barcode_date_active{

		display: block;

		position: absolute;

		width: 200px;

		height: auto;

		background: white;

		/*border: 1px solid;*/

		box-shadow: 0px 0px 5px;

		text-align: center;

		padding: 10px;

		border-radius: 5px;

		z-index: 99999;

	}

	.hide{

		display: none;

	}

	.page_result b{

		color: red !important;

	}

	.v_photo{

		margin-top: -23px;

		width: 120px;

		text-align: center;

		position: absolute;

		background: rgba(0, 0, 0, 0.21) none repeat scroll 0% 0%;

	}

	.v_photo a{

		color: white;

	}
	.search_box{
		width: 25%;
		margin-left: 13%;
		padding: 7px 0;
	}
	.item_sort{
		cursor: pointer;
		white-space: nowrap;
	}
	.sort_a,.sort_d{
	    background: #cecece !important;
	}
	.filters_applied span{
	    margin-left: 5px;
	    padding: 10px;
	    background: #f1f1f1;
	    color: black;
	}
	.filters_applied span:hover{
	    background: #d4d4d4;
	    cursor: pointer;
	}
	.er_box{
		width: 100%;
        border: 1px solid #ffbebe;
        border-radius: 3px;
        padding: 1%;
        background: #ffd5d5;
        color: red;
        margin-top: 10px;
	}

</style>
<?php
	$er = $this->session->flashdata('er');
?>
<?php if ($er!=''): ?>
	<div class="er_box">
		<p><?php echo $er ?></p>
	</div>
<?php endif ?>
<input type="hidden" class="ck_price" value="1">
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	
</div>

<div class="search_box">
	<input type="text" class="search_all" name ='search' id='search' style="width:100%;" placeholder="Search all items" onkeypress="return event.keyCode!=13" value="" />
	
</div>
<div id="table_action_header">
	<ul>
		<li class="float_right">
			<input type="text" name="isearch" id="isearch" class="form-control" placeholder="Search this list">
		
		</li>
	</ul>
</div>

<div style="margin-left:-50px">
	<?php $this->load->view('template/filter') ?>
</div>
<!-- TABLE HOLDER -->
<div id="table_holder">
	<!-- TOP PAGINATION -->

	<div style="float:left;width:100%;height:25px;" id="page_result" class="page_result">
		<?php 
				$val=10;
				if (isset($_GET['page'])) {
					$val = $_GET['page'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
		?>
		<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div style="float:left;width:50%;">
		Show
		<select id="per_page_top" class="per_page" name="per_page">
			<option value="all">All</option>
			<?php 
				$val=10;
				if (isset($_GET['page'])) {
					$val = $_GET['page'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=500;$i>=100?$i+=200:$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div>
	<div style="float:left;width:100%;height:25px;margin-top:5px;" class="filters_applied">
	</div>
	<div id="pagination" class="dataTables_paginate">

	</div>
	<!-- END TOP PAGINATION -->
	<table id="sortable_table" class="tablesorter">
		<thead>
			<th>Image</th>
			<th rel="3" sort="part_number" class='item_sort sort_d' t='Part Number'>Part Number <i class='fa fa-sort-desc'></i></th>
			<th rel="4" sort="part_name" class='item_sort' t='Part Name'>Part Name <i class='fa fa-sort'></i></th>
			<th rel="5" sort="make_name" class='item_sort' t='Make'>Make <i class='fa fa-sort'></i></th>
			<th rel="6" sort="model_name" class='item_sort' t='Model'>Model <i class='fa fa-sort'></i></th>
			<th rel="7" sort="year" class='item_sort' t='Year'>Year <i class='fa fa-sort'></i></th>
			<th rel="8" sort="color_name" class='item_sort' t='Color'>Color <i class='fa fa-sort'></i></th>
			<th rel="8" sort="cost_price" class='item_sort' t='Cost'>Cost <i class='fa fa-sort'></i></th>
			<th rel="8" sort="unit_price" class='item_sort' t='Price From'>Price From <i class='fa fa-sort'></i></th>
			<th rel="8" sort="unit_price_to" class='item_sort' t='Price To'>Price To <i class='fa fa-sort'></i></th>
			<th rel="9" sort="condition_name" class='item_sort' t='Condition'>Condition <i class='fa fa-sort'></i></th>
			<th rel="9" sort="approve" class='item_sort' t='Approved'>Approved <i class='fa fa-sort'></i></th>
			<th>Action</th>
		</thead>
		<tbody>
			
		</tbody>
		<tfoot>
			
		</tfoot>
	</table>
	<!-- BOTTOM PAGINATION -->
	<div style="float:left;width:100%;height:25px;" class="page_result">
	<?php 
			$val=10;
			if (isset($_GET['page'])) {
				$val = $_GET['page'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
	<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div style="float:left;width:50%;" class="page_result">
		Show
		<select id="per_page_bot" class="per_page" name="per_page">
			<option value="all">All</option>
			<?php 
				$val=10;
				if (isset($_POST['page'])) {
					$val = $_POST['page'];
				}
				if (isset($_POST['per_page'])) {
				 	$per = $_POST['per_page'];
				 } 
				 for($i=10;$i<=500;$i>=100?$i+=200:$i+=10){
				 	
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php 
				}
			?>
		</select>
		results
	</div>
	<div id="pagination" class='dataTables_paginate'>
	
	</div>
	<!-- END BOTTOM PAGINATION -->
</div>
<div style="clear:both"></div>
</form>
<div class="loading_box"><!-- Place at bottom of page --></div>


<script type="text/javascript">

	$('.per_page').change(function(){
		var per = $(this).val();
		// location.href = '<?php echo site_url("$controller_name")?>'+'/?page='+per;
		$('#per_page_bot').val(per);
		$('#per_page_top').val(per);
		search();
	})
	// $('#isearch').change(function(event){
	// 	search(1,event,0);
	// });
	


</script>
<script>
	// $('#khmername_id').select2();
	// $('#make_id').select2();
	// $('#model_id').select2();
	// $('#year').select2();
	// $('#yearto').select2();


    function doOnClick(event){
    	var p_id = $(event.target).parent().parent().find('#part_id').val();
       if (window.confirm('Confirm Add ?')){
           window.location='<?php echo "index.php/template/add_to_item?tid='+p_id+'"; ?>';
       };
    }
   
		// $('#vin_id').select2();

    function move_template(){

    	var vin_id = $('#vin_number').val();
		var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('Please Select an Item');
    		return false;
    	}else{
    		if (confirm('Confirm ?')) {
				$('body').addClass('loading');
	    		$.post("<?php echo site_url('template/move_template') ?>",
	    		{
	    			vin_id: vin_id,
	    			tem_id:selected
	    		},function(d){
	    			console.log(d);
					$('body').removeClass('loading');

	    		},'Json');
	    	}
    	}
    }
    $('#isearch').keypress(function(e){

		 if(e.which == 13) {

			search(1,event,0,'s','');

		}

	});
	$('.item_sort').click(function(){
		// console.log($(this).attr('sort'));
		var txt = $(this).attr('t');
		$('.item_sort').each(function(){
			if ($(this).attr('t')!=txt) {
				$(this).removeClass('sort_a sort_d');
				$(this).html($(this).attr('t')+ ' <i class="fa fa-sort"></i>');	
			};
			
		});

		if ($(this).hasClass('sort_a')) {
			// console.log($(this).attr('sort'));
			$(this).removeClass('sort_a');
			$(this).addClass('sort_d');
			$(this).html(txt+ ' <i class="fa fa-sort-desc"></i>');
		}else{
			$(this).removeClass('sort_d');
			$(this).addClass('sort_a');
			$(this).html(txt+ ' <i class="fa fa-sort-asc"></i>');

		}
		search(1,event,0);
	});
</script>
<?php $this->load->view("partial/footer"); ?>
