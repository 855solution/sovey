<?php $this->load->view('partial/header') ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/report.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}
</style>

<div class="row">
	<div class="col-md-6 pull-left">
		<h1>Reports</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<a href="<?php echo site_url('reports/sale_report') ?>" class="btn btn-primary" target="<?php echo  'sale_'.rand(0,100)?>">Sales Report</a>
		<!-- <a href="#" class="btn btn-primary" target="<?php echo  'quote'.rand(0,100)?>">Quotations Report</a> -->
		<!-- <a href="#" class="btn btn-primary" target="<?php echo  'deposit_'.rand(0,100)?>">Deposits Report</a> -->
		<!-- <a href="#" class="btn btn-primary" target="<?php echo  'payment_'.rand(0,100)?>">Payments Report</a> -->
	</div>
</div>
  		
  


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>

<?php $this->load->view('partial/footer') ?>


</script>