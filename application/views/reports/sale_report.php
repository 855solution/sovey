<?php $this->load->view('partial/header') ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}
	.inc_top{
		margin-top: 15px;
	}
	@media print{
		
		.no_print{
			display: none;
		}
		.dataTables_length{
			display: none;
		}
		.dataTables_filter{
			display: none;
		}
		.dataTables_paginate{
			display: none;
		}
		.dataTables_info{
			display: none;
		}
		.dataTables_wrapper{
			width: 100% !important;
		}
		.sorting:after, .sorting_asc:after, .sorting_desc:after {
		    content : none !important;
		}
		thead th,tbody td,tfoot th{
			white-space: nowrap;
		}
		.row{
			margin: 0 !important; 
			width: 100% !important;
		}
		.table_wrap{
			width: 100% !important;
			margin: 0 !important;
			padding: 0 !important;
		}
		#content_area{
			width: 100% !important;
			margin:0 !important;
		}
	}
</style>
  <!-- Default panel contents -->
  	
<div class="row inc_top">
	<div class="col-md-2 pull-left no_print">
		<h4>Sales Report</h4>
	</div>

	<div class="col-md-10 pull-right no_print">
		<div class="col-md-3 form-group">
			<select class="form-control customer_id">
				<option value="">All Customer</option>
				<?php foreach ($customer as $cus): ?>
					<?php $com = '';
					if ($cus->nick_name) {
					 	$com = " <strong>($cus->nick_name)</strong>";
					 } ?>
					<option value="<?php echo $cus->person_id ?>"><?php echo $cus->last_name.' '.$cus->first_name.$com?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="col-md-3 form-group">
			<input type='text' class="form-control from_date" value="<?php echo date('Y-m-01') ?>">
		</div>
		<div class="col-md-3 form-group">
			<input type='text' class="form-control to_date" value="<?php echo date('Y-m-d') ?>">
		</div>
		<div class="col-md-1 form-group">
			<input type="button" class="btn btn-primary btn_search" value="Search">
		</div>
		<div class="col-md-1 form-group">
			<input type="button" class="btn btn-success btn_print" value="Print">
		</div>
	</div>


</div>
<div class="row">
	<div class="col-md-12 report_title">
		<center>
			<h2>Sales Report</h2>
			<h4 class="label_date"></h4>
			<h4 class="label_cus"></h4>
		</center>
	</div>
</div>	
<div class="row table_wrap">
	<div class="col-md-12">
		<table class="table table-bordered table-striped tdata">
			<thead>
				<th>Nº</th>
				<th>Date</th>
				<th>Invoice #</th>
				<th>Customer</th>
				<th>Grand Total</th>
				<th>Paid</th>
				<th>Due</th>
				<th>Created by</th>
				<!-- <th>Actions</th> -->
			</thead>
			<tbody>
				
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</div>



<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>

<?php $this->load->view('partial/footer') ?>

<script type="text/javascript">
	var oTable;
	$(document).ready(function(){
		get_data();
	});

	function get_data(){
		var fd = $('.from_date').val();
		var td = $('.to_date').val();
		var cus_id = $('.customer_id').val();
		$('.loading_box').fadeIn();
		
		$.ajax({
			url:"<?php echo site_url('reports/get_sale_report') ?>",
			type:'post',
			dataType:'json',
			data:{
				fd:fd,
				td:td,
				cus_id:cus_id
			},
			success:function(oh){
				// console.log(oh.tbody);
				$('.tdata tbody').html(oh.tbody);
				$('.tdata tfoot').html(oh.tfoot);
				$('.label_date').text(oh.label_date);
				$('.label_cus').text(oh.label_cus);
				oTable = $('.tdata').DataTable({
					"lengthMenu": [ [10, 30, 50, 100, -1], [10, 30, 50, 100, "All"] ],
					"footerCallback": function ( row, data, start, end, display ) {
			            var api = this.api(), data;
			 			
			            // Remove the formatting to get integer data for summation
			            var intVal = function ( i ) {
			                return typeof i === 'string' ?
			                    i.replace(/[\$,]/g, '')*1 :
			                    typeof i === 'number' ?
			                        i : 0;
			            };
			 
			            // Total over all pages
			            // total = api
			            //     .column( 4 )
			            //     .data()
			            //     .reduce( function (a, b) {
			            //         return intVal(a) + intVal(b);
			            //     }, 0 );

			 
			            // Total over this page
			           	pTotalG = api
			                .column( 4, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			            pTotalP = api
			                .column( 5, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			            pTotalD = api
			                .column( 6, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 4 ).footer() ).html('$ '+pTotalG.toFixed(2));
			            $( api.column( 5 ).footer() ).html('$ '+pTotalP.toFixed(2));
			            $( api.column( 6 ).footer() ).html('$ '+pTotalD.toFixed(2));

			        }
				});
 				




				$('.loading_box').fadeOut();

				
			}
		});
	}

	$('.btn_search').click(function(){
		oTable.destroy();
		get_data();
	})

	$('.from_date,.to_date').datepicker({
		format:'yyyy-mm-dd'
	});
	$('.btn_print').click(function(){
		window.print();
	});
</script>