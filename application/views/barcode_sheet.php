<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo $this->lang->line('items_generate_barcodes'); ?></title>
	<meta charset="utf-8">
</head>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/print.css" media="print"/>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos.css" />
<body style="background-color: white;">
<center>
<style type="text/css">
	.bar_wrap{
		width: 260px;
		color: black;
		text-align: center;
		padding-top: 10px;
	}
	.print_table tbody tr{
		margin:5px 0;
	}
</style>
<div style="width:260px; height:100%;">
<?php 
$count = 0;
// var_dump($items);
// for($i=0;$i<$barcodetime;$i++){

// foreach($items as $item)
// {
// 	$engname = $item->english_name;
// 	$khname = $item->khmername_name;
// 	$barcode = $item->barcode;
// 	$text = $item->barcode;
// 	$item_id = $item->item_id;
	
// 	if ($count % 1 ==0 and $count!=0)
// 	{
// 		echo '</tr><tr>';
// 	}
// 	$image=$this->Item->getdefaultimg($item_id);
// 	$path=site_url('../assets/site/image/no_img.png');
// 	if($image!='')
// 		if(file_exists(FCPATH."uploads/thumb/".$image)){
// 			$path=site_url("../uploads/thumb/".$image);
// 		}
// 	echo "<div style='width:260px;color:#000;margin:13px 0'>
// 			<div style='text-align:left;margin-left:8px;'>[ $item_id ] $engname / $khname $item->partplacement_name</div>
// 			<div style='width:260px;'><img src='".site_url()."/barcode?barcode=$barcode&text=$text&width=256' />
// 			        <div style='font-size:70px; margin:18px 0px;'>".substr($barcode, -6)."</div>
//                        </div>
// 		</div>";
// 	$count++;
// }
// }
?>
<table class="print_table">
	<thead>
		
	</thead>
	<tbody>
		<?php foreach ($items as $item): ?>
			<?php
				$barcode = $item->barcode;
				$text = $item->barcode;
				$item_id = $item->item_id;
			?>

			<tr>
				<td class="bar_wrap">
					<div style='text-align:left;margin-left:8px;'>[ <?php echo $item_id ?> ] <?php echo $item->name ?> / <?php echo $item->khmername_name ?> <?php echo $item->partplacement_name ?></div>
					<img src="<?php echo site_url()."/barcode?barcode=$barcode&text=$text&width=256"?>" />
					<div style='font-size:70px; margin:18px 0px;'><?php echo substr($barcode, -6) ?></div>

				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
	
</table>
<div class="hideforprint" style=" width:356px; height:auto; margin-top:25px; float:left;">
    <div style="font-size:14px;">
    	<a href="javascript:window.print()" style="color:#0033CC; padding-right:5px;">
        	Print
        </a>
        &nbsp;&nbsp;|&nbsp;&nbsp; 
        <a href="<?php echo site_url("items/view/-1/width:360");?>">Add New Items</a> 
        &nbsp;&nbsp;|&nbsp;&nbsp; 
        <a href="<?php echo site_url("items");?>">Items Listing</a> 
        &nbsp;&nbsp;|&nbsp;&nbsp; 
        <a href="<?php echo site_url("home");?>">Home</a>
	</div>        
</div>

</div>

</center>
</body>
</html>