<?php $this->load->view("partial/header"); ?>

<center>
    <div id="home_module_list" align="center">
       
        <?php
        foreach($allowed_modules->result() as $module)
        {
        ?>
        <div class="module_item">
            <div style="width:100px; float:left; padding:7px;">
                <a href="<?php echo site_url("$module->module_id");?>">
                <img src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" /></a>
            </div>
            <div style="width:100px; border-radius:0 0 3px 3px; float:left; background:#eee; padding:7px; border-top:1px solid #c6c6c6;">
                <a href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
                <?php //echo $this->lang->line('module_'.$module->module_id.'_desc');?>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
</center>


<?php $this->load->view("partial/footer"); ?>