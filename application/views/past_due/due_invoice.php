<?php $this->load->view("partial/header"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	exit;
}
?>
<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}


    @media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			/*width: 95%;*/
   			/*margin-right: 15px;*/
   			/*margin-left: 15px;*/
   			/*margin-top: 20px;*/

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}

	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 7px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		/*margin-top: 24px;*/
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}

</style>
<div id="receipt_wrapper">
	
	

	<table id="receipt_items">
	<thead>
		<!-- HEADER -->
		<tr>
			<td colspan="9">
				<div id="receipt_header">
					<div id="company_name"><?php echo $this->config->item('company'); ?></div>

					<div class="left_head">
						<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
						<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
						<?php if(isset($customer))
						{
						?>
							<div id="customer" class="l_text"><div>Customer</div><p><?php echo $customer; ?></p></div>
							<div class="l_text">
								<div>Phone Number</div><p><?php echo $cus_info->phone_number ?></p>
							</div class="l_text">
							<div class="l_text"> 
								<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
							</div>
							<div class="l_text">
								<div>Email</div><p><?php echo $cus_info->email ?></p>
							</div>
							<div class="l_text">
								<div>Contact</div><p><?php echo $customer; ?></p>
							</div>

						<?php
						}
						?>
						
					</div>
					<div class="right_head">
						<div id='barcode'>
						<?php echo "<img src='index.php/barcode?barcode=$invoice_id&text=$invoice_id&width=250&height=50' />"; ?>
						</div>
						<div class="r_text"><div>Page</div><p class="page_counter"></p></div>
						<div class="r_text"><div>Sale ID</div><p><?php echo $sale_id; ?></p></div>
						<div class="r_text"><div>Sale Date</div><p><?php echo $transaction_time ?></p></div>
						<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>
						<div class="r_text"><div>Payment Type</div><p><?php echo $pay_type ?></p></div>

					</div>
				</div>
			</td>
		</tr>
		
		<!-- END`HEADER -->
		<tr>
		<!-- <th></th> -->
	    <th><?php echo "No"; ?></th>
		<th><?php echo "Barcode"; ?></th>
		<th>Vinnumber</th>
		<th>Year-Make-Model</th>
		<th><?php echo $this->lang->line('sales_quantity'); ?></th>
		<th><?php echo "PPL"; ?></th>
		<th><?php echo $this->lang->line('common_price'); ?></th>
		<th><?php echo "Dsc"; ?></th>
		<th><?php echo $this->lang->line('sales_total'); ?></th>
		</tr>
	</thead>
	
	<tbody>
	<?php
	$i=0;
	$total_sale =0;
	foreach($items as $item)
	{$i++;
		
	?>
		
		<tr>
		<td><?php echo $i ?></td>
		<td><?php echo $item->barcode ?></td>
		<td><?php echo $item->category ?></td>
		<td style="text-align:left;"><?php echo $item->year.' - '.$item->make_name.' - '.$item->model_name ?></td>
		<td style='text-align:center;'><?php echo $item->quantity_purchased ?></td>
		<td><?php echo $item->partplacement_name!=null?$item->partplacement_name:$item->partplacement_id;?></td>
		<td><?php echo to_currency($item->item_unit_price); ?></td>
		<td style='text-align:center;'><?php echo $item->discount_percent; ?></td>
		<td style='text-align:right;'><?php echo to_currency($item->item_unit_price*$item->quantity_purchased-$item->discount_percent); ?></td>
		</tr>

	<?php
		$total_sale+=$item->item_unit_price*$item->quantity_purchased-$item->discount_percent;
	}
	?>
	</tbody>
	<tfoot>
		
	<tr>
	<td colspan="6" style='text-align:right;border-top:2px solid #000000;'><?php echo $this->lang->line('sales_sub_total'); ?></td>
	<td colspan="3" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($total_sale); ?></td>
	</tr>

	<?php foreach($taxes as $name=>$value) { ?>
		<tr>
			<td colspan="6" style='text-align:right;'><?php echo $name; ?>:</td>
			<td colspan="3" style='text-align:right;'><?php echo to_currency($value); ?></td>
		</tr>
	<?php }; ?>

	<tr>
	<td colspan="6" style='text-align:right;'><?php echo $this->lang->line('sales_total'); ?></td>
	<td colspan="3" style='text-align:right'><?php echo to_currency($total_sale); ?></td>
	</tr>

    <tr><td colspan="10">&nbsp;</td></tr>

	<?php
		foreach($payments as $payment)
	{ ?>
		<tr>
		<td colspan="6" style="text-align:right;"><?php echo $this->lang->line('sales_payment'); ?></td>
		<td colspan="2" style="text-align:right;"><?php echo $pay_type ?> </td>
		<td colspan="1" style="text-align:right"><?php echo $pay_amount; ?>  </td>

	    </tr>
	<?php
	}
	?>

    <tr><td colspan="10">&nbsp;</td></tr>

	<tr>
		<td colspan="6" style='text-align:right;'><?php echo $this->lang->line('sales_change_due'); ?></td>
		<td colspan="3" style='text-align:right'><?php echo  $total_sale-$total_pay<0?to_currency(0):to_currency($total_sale-$total_pay); ?></td>
	</tr>
	</tfoot>

	</table>

	<!-- <div id="sale_return_policy">
	<?php echo nl2br($this->config->item('return_policy')); ?>
	</div> -->
	
</div>


<div style="clear:both"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript">
	$(window).load(function()
	{
		window.print();
	});
</script>
