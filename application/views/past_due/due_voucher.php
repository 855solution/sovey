<?php $this->load->view("partial/header"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	exit;
}
?>
<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}


    @media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			width: 95%;
   			margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.left_head{
			width: 45%;
			float: left;
			left: 0;
			margin-top: 7px !important;

		}
		
		.left_signature{
			float: left;
			width: 30% !important;
		}
		.right_signature{
			float: right;
			width: 30% !important;
		}
		.footer_signature{
			width: 100% !important;
		}

	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 7px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		/*margin-top: 24px;*/
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}

</style>
<div id="content_area" style="width:100%;">
	
	<div id="receipt_wrapper">
		<table id="receipt_items">
		<thead>
			<!-- HEADER -->
			<tr>
				<td colspan="5">
					<div id="receipt_header">
						<div id="company_name"><?php echo $this->config->item('company'); ?>
							<span class="inv_type">Payment Voucher</span>
						</div>
						<div class="left_head">
							<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
							<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
							<?php if(isset($customer))
							{
							?>
								<div id="customer" class="l_text"><div>Customer</div><p><?php echo $cus_info->title.$customer;echo $cus_info->nick_name!=''?"($cus_info->nick_name)":''  ?></p></div>
								<div class="l_text">
									<div>Phone Number</div><p><?php echo $cus_info->phone_number ?></p>
								</div class="l_text">
								<div class="l_text"> 
									<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
								</div>
								
							<?php
							}
							?>
							
						</div>
						<div class="right_head">
							<div id='barcode'>
							<?php echo "<img src='index.php/barcode?barcode=$rec_id&text=$rec_id&width=250&height=50' />"; ?>
							</div>
							<div class="r_text"><div>Page</div><p class="page_counter"></p></div>
							<div class="r_text"><div>Pay Date</div><p><?php echo date("d/m/Y",strtotime($row->date)) ?></p></div>
							<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>

						</div>
					</div>
				</td>
			</tr>
			
			<!-- END`HEADER -->
			<tr>
			<!-- <th></th> -->
			    <th>No</th>
			    <th>Invoice #</th>
			    <th>Sale Date</th>
			    <th>Paid</th>
			    <th>Status</th>
			</tr>
		</thead>
		<tbody style="border-bottom:2px solid Black">
		<?php
		$i=0;
		$total_sale =0;
		foreach($payment as $p)
		{$i++;
			
		?>
			
			<tr>
				<td><?php echo $i ?></td>
				<td><?php echo $p->invoiceid ?></td>
				<td><?php echo date('d/m/Y H:i:s',strtotime($p->sale_time)) ?></td>
				<td><?php echo number_format($p->amount_usd,2) ?></td>
				<td><?php echo $p->payment_status ?></td>
			</tr>

		<?php
			$total_sale+=$p->amount_usd;
		}
		?>
		</tbody>
		<tfoot>
			
			<tr>
				<td colspan="2" style=""></td>
				<td style='text-align:right;border: 1px solid #a7a7a7;'><?php echo $this->lang->line('sales_total'); ?></td>
				<td colspan="2" style='position:static;text-align:right;border: 1px solid #a7a7a7;background: #dedede;font-weight: bold;'><?php echo to_currency($total_sale); ?></td>
				
			</tr>
		</tfoot>
		</table>

		<div class="footer_signature_wrapper" align="center">
			<div class="footer_signature">
				<div class="left_signature">
					<label>Customer's Signature</label>
					<div class="underline">
						
					</div>
				</div>
				<div class="right_signature">
					<label>Seller's Signature</label>
					<div class="underline">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(window).load(function()
	{
		window.print();
	});
</script>
<?php $this->load->view("partial/footer"); ?>
