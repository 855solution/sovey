<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend>Past due detials</legend>
<?php //$this->load->view("past_due/form_basic_info"); ?>


<table class="tablesorter">
	<tr>
		<th class="header">Customer Name: </th><th class="header"><?php echo $customer;?></th> <th class="header">Seller: <?php echo $employee; ?></th><th></th>
	</tr>
	
	<tr>
		<td colspan="2">
			<table border='1'>
				<tr>
					<td>No</td><td>Charged</td><td>Date</td>
				</tr>
				<?php $i=1;?>
				<?php foreach($person_info as $info){
					
					//echo $info->sale_id.' - '.$info->total_due.'<br>';
				?>	
					
				<tr>
					<td><?php echo $i;?></td><td><?php echo $info->total_due;?></td><td><?php echo $info->updated_date;?></td>
				</tr>
				<?php 
				$i++;
				}?>
				
			</table>
		</td>
	</tr>
	
</table>




</fieldset>
<?php 
echo form_close();
?>

<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#past_due_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				//post_person_form_submit(response);
				location.reload();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			charg: "required"
			//last_name: "required",
    		//email: "email"
   		},
		messages: 
		{
			charg: "<?php echo 'The adding charge is a required field.'; ?>"
     		
		}
	});
});
</script>