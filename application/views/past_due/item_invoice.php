<?php $this->load->view("partial/header"); ?>

<!-- <style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}


    @media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			width: 95%;
   			margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;
			color: black !important;
   			
   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head{
			/*width: 45%;
			float: left;
			left: 0;*/
			margin-top: 37px !important;
			color: black !important;

		}
		.right_head{
			margin-top: 89px !important;
			color: black !important;
			
		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}


	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 30px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		margin-top: 81px;
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
		white-space: nowrap;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
		white-space: nowrap;

	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}

</style>
 -->

<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		/*margin-bottom: 30px !important;*/
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		/*margin-top: 30px;*/

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		/*margin-top: 30px;*/
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:4px;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:4px;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	.r_text,.l_text{
		white-space: nowrap;
	}
	#company_phone{
		margin-bottom: 5px;
	}

	#barcode{
		height: 48px;
	}


	
@media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			font-size: 10px;
   			width:100%;
   			margin:0 auto !important;
   			padding: 0 !important;
   			float: none !important; 
   			/*margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;*/

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   			min-height: 0 !important;
   			padding: 0 !important;
   			margin: 0 !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head,.right_head{
			
			color: black !important;

		}
		.left_head{
			margin-top: 5px;
		}
		

		#receipt_header{
			color: black !important;

		}
		#receipt_items{
			color: black !important;
		    margin-top: 1px;
		}
		#receipt_items th,#receipt_items td{
			padding: 0 !important;
		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}
		.r_text div,
		.l_text div,
		.r_text p,
		.l_text p{
			padding: 4px !important;
			line-height: 10px !important;

		}
		#barcode{
			margin-top: 8px !important;

		}
		.underline{
			height: 30px !important;
		}

		textarea{
			border: none;
			resize: none;
			font-size: 8px;
			color: black;
			width: 98% !important;
			background: transparent;
		}
		thead {
	        display: table-header-group;
	    }
	    tbody{
		  display:table-row-group;
		}

		/*@page {
		   @bottom-right {
		    content: counter(page) " of " counter(pages);
		   }
		}*/
	}
</style>
<div id="receipt_wrapper">
	<div class="top_btn">
		<!-- <div class="small_button float_left" id="btn_new_sale"><span>New Sale</span></div> -->
		<div class="small_button float_right" id="btn_print"><span>Print</span></div>
	</div>
	<div style="clear:both;"></div>
	
	<div id="receipt_header">
		

		<div id="company_name"><?php echo $this->config->item('company'); ?>
		<span class="inv_type">Payment Dues</span>
		</div>
		<div class="left_head">
			<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
			<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
			<?php if(isset($customer))
			{
			?>
				<div id="customer" class="l_text"><div>Customer</div><p><?php echo $cus_info->title.$customer;echo $cus_info->person_id!='72'?$cus_info->nick_name!=''?"($cus_info->nick_name)":'':'' ?></p></div>
				<div class="l_text">
					<div>Phone Number</div><p><?php echo $cus_info->phone_number.' '.$cus_info->email ?></p>
				</div class="l_text">
				<div class="l_text"> 
					<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
				</div>
				<!-- <div class="l_text">
					<div>Email</div><p><?php echo $cus_info->email ?></p>
				</div> -->
				<!-- <div class="l_text">
					<div>Contact</div><p><?php echo $customer; ?></p>
				</div> -->

			<?php
			}
			?>
			
		</div>
		
		<div class="right_head">
			<div id='barcode'>
			<?php //echo "<img src='index.php/barcode?barcode=$qdata->qt_number&text=$qdata->qt_number&width=250&height=50' />"; ?>
			</div>
			<div class="r_text"><div>Page</div><p class="page_counter"></p><p> / <?php echo ceil(count($sales_data)/17) ?></div>
			<!-- <div class="r_text"><div>Date From</div><p><?php echo $qdata->qt_number; ?></p></div> -->
			<div class="r_text"><div>Date</div><p><?php echo date("d/m/Y H:i:s") ?></p></div>
			<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>
			<!-- <div class="r_text"><div>Payment Type</div><p><?php echo $payments['Cash']['payment_type'] ?></p></div> -->
		</div>
	</div>

	<div style="clear:both"></div>

	<table id="receipt_items">
	<thead>
		<tr>
			<th>No</th>
			<th>Date</th>
			<th >Description</th>
			<!-- <th>Code</th> -->
			<th>Invoice #</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Due</th>
			<!-- <th>Note</th> -->
		</tr>
	</thead>
	
	<tbody style="border-bottom:2px solid black;">
	<?php
	$i=0;
	$total = 0;
	$paid_left = 0;
	$last_sale_id = 0;
	foreach($sales_data as $sp)
	{

	$first_paid = $this->db->query("SELECT first_payment FROM ospos_sales WHERE sale_id = '$sp->sale_id'")->row()->first_payment;
	$sale_total_paid = $this->db->query("SELECT SUM(amount_usd) as paid FROM ospos_payment_record_detail WHERE sale_id = '$sp->sale_id' AND payment_on <>'return'")->row()->paid + $first_paid;

	if ($sp->sale_id == $last_sale_id) {
		$sale_total_paid = $paid_left;
	}else{
		$paid_left = 0;
	}

	$name = $sp->name;
	if ($sp->khmername_name) {
		$name = $sp->khmername_name;
	}
	$head = '';
	if ($sp->vinnumber_id!='') {
		$head = 'H'.$sp->vinnumber_id;
	}

	$ppl = $sp->partplacement_id;
		if ($sp->is_new!=1) {
			$ppl = $sp->partplacement_name;
		}

	$item_desc = '';
		if ($sp->description) {
			$item_desc = "($sp->description)";
		}

		$sub_total = 0;

		$item_return_desc = '';
		if ($sp->is_return==1) {
			$return_no = "<a target=".rand(0,99)." href='".site_url('returns/view_reciept/'.$sp->return_id)."'>".$sp->return_no."</a>";
			$item_return_desc = " (Restock: ".to_currency($sp->return_restock).", $sp->barcode, $sp->return_no)";
			$sub_total = $sp->return_restock;
			$item_balance = $sale_total_paid - $sp->return_restock;
			
			// if ($sale_total_paid>=$sp->return_restock) {
			// $paid_left = $sale_total_paid - $sp->return_restock;
				
			// }

			// $item_balance = $paid_left - $sp->item_unit_price;


			

			
			// $paid_left = $item_balance;

		}else{
			// $sub_total = ($item->item_unit_price*$item->quantity_purchased)-$item->discount_percent;
			$sub_total = $sp->item_unit_price - $sp->discount_percent;
			$paid_left = $sale_total_paid - ($sp->item_unit_price - $sp->discount_percent);

			$item_balance = $paid_left;
		}
		
		// if ($paid_left<0) {
		// 	//show is balanced


		// }

		// echo $sale_total_paid.' '.$last_sale_id.' '.$sp->sale_id.' '.$item_balance.'<br>';

		$last_sale_id = $sp->sale_id;
		if ($item_balance>=0) {
				continue;
			// echo 'yes-';
		}

	$i++;

	?>
		
		
		<?php if ($sp->is_return==1): ?>
				<tr>
					<td><?php echo $i ?></td>
					<td><?php echo date('d/m/Y',strtotime($sp->sale_time)) ?></td>
					<td style="text-align:left;"><?php echo $sp->year.' '.$sp->make_name.' '.$sp->model_name.' '.$name.' '.$ppl.' '.$item_desc.$item_return_desc; ?></td>
					<td><?php echo $sp->invoiceid ?></td>
					<td><?php echo $sp->quantity_purchased ?></td>
					<td>$<?php echo number_format($sp->return_restock,2) ?></td>
					<td>$<?php echo number_format($item_balance*-1,2)?></td>
				</tr>
			
		<?php else: ?>
			<tr>
				<td><?php echo $i ?></td>
				<td><?php echo date('d/m/Y',strtotime($sp->sale_time)) ?></td>
				<td style="text-align:left;"><?php echo $sp->year.' '.$sp->make_name.' '.$sp->model_name.' '.$name.' '.$ppl.' '.$item_desc; ?></td>
				<td><?php echo $sp->invoiceid ?></td>
				<td><?php echo $sp->quantity_purchased ?></td>
				<td>$<?php echo number_format($sp->item_unit_price,2) ?></td>
				<td>$<?php echo number_format($item_balance*-1,2)?></td>
			</tr>
		<?php endif ?>

	<?php
		$total+=$item_balance;
	}
	?>
	</tbody>
	<tfoot>
		
	<!-- <tr>
		<td  style="vertical-align: top;">
			Note :
		</td>
		<td colspan="4"  style="text-align: left;border: none;">
			<textarea style="width: 50%;resize: none;" ><?php echo $this->config->item('due_note') ?></textarea>
		</td>	
		
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;border-top: 1px solid black;'><?php echo "Total"; ?></td>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;background: #ececec;border-top: 1px solid black;'><?php echo to_currency($sales_total); ?></td>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;border-top: 1px solid black;'><?php echo "Total Due"; ?></td>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;background: #ececec;border-top: 1px solid black;'><?php echo to_currency($sales_due); ?></td>
	</tr> -->

	<tr>
		<td colspan="5">
			<!-- LEFT COLUMN -->
			<table style="width: 100%;border-collapse: collapse;">
				<tr>
					<td  style="vertical-align: top;">
						Note :
					</td>
					<td  style="text-align: left;border: none;">
						<textarea rows="3" style="width: 50%;resize: none;" ><?php echo $this->config->item('due_note') ?></textarea>
					</td>	
				</tr>
			</table>
		</td>
		<td colspan="2" style="padding: 0;vertical-align: top;">
			<!-- RIGHT COLUMN -->
			<table style="width: 100%;border-collapse: collapse;">
				<tr>
					<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;border-top: none;'><?php echo "Total Due"; ?></td>
					<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;background: #ececec;border-top: none;'><?php echo to_currency($sales_due); ?></td>
				</tr>
			</table>
		</td>
	</tr>
	


	
	<!-- <tr>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;border-top: 1px solid black;'><?php echo "Paid"; ?></td>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;background: #ececec;border-top: 1px solid black;'><?php echo to_currency($sales_paid); ?></td>
	</tr>
	<tr>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;border-top: 1px solid black;'><?php echo "Due"; ?></td>
		<td style='text-align: right;border: 1px solid #e4e4e4;font-weight: bold;background: #ececec;border-top: 1px solid black;'><?php echo to_currency($sales_due); ?></td>
	</tr> -->

	
	</tfoot>

	</table>
	<div class="footer_signature_wrapper" align="center">
		<div class="footer_signature">
			<div class="left_signature">
				<label>Customer's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<div class="right_signature">
				<label>Seller's Signature</label>
				<div class="underline">
					
				</div>
			</div>
		</div>
	</div>
	
</div>


<div style="clear:both"></div>


<script type="text/javascript">
$(window).load(function()
{
	window.print();
	
});
$('#btn_print').click(function(){
	window.print();
});

</script>
<?php $this->load->view("partial/footer"); ?>
