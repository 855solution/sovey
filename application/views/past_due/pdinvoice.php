<?php $this->load->view("partial/header"); ?>
<!-- <style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}


    @media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			width: 95%;
   			margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;
			color: black !important;

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head{
			/*width: 45%;
			float: left;
			left: 0;*/
			margin-top: 37px !important;
			color: black !important;

		}
		.right_head{
			margin-top: 89px !important;
			color: black !important;
			
		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}


	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 30px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		margin-top: 81px;
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}

</style>
 -->
 <style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		/*margin-bottom: 30px !important;*/
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		/*margin-top: 30px;*/

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		/*margin-top: 30px;*/
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:4px;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:4px;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	.r_text,.l_text{
		white-space: nowrap;
	}
	#company_phone{
		margin-bottom: 5px;
	}

	#barcode{
		height: 48px;
	}


	
@media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			font-size: 10px;
   			width:100%;
   			margin:0 auto !important;
   			padding: 0 !important;
   			float: none !important; 
   			/*margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;*/

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   			min-height: 0 !important;
   			padding: 0 !important;
   			margin: 0 !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head,.right_head{
			
			color: black !important;

		}
		.left_head{
			margin-top: 5px;
		}
		

		#receipt_header{
			color: black !important;

		}
		#receipt_items{
			color: black !important;
		    margin-top: 1px;
		}
		#receipt_items th,#receipt_items td{
			padding: 0 !important;
		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}
		.r_text div,
		.l_text div,
		.r_text p,
		.l_text p{
			padding: 4px !important;
			line-height: 10px !important;

		}
		#barcode{
			margin-top: 8px !important;

		}
		.underline{
			height: 30px !important;
		}

		textarea{
			border: none;
			resize: none;
			font-size: 8px;
			color: black;
			width: 98% !important;
			background: transparent;
		}
		thead {
	        display: table-header-group;
	    }
	    tbody{
		  display:table-row-group;
		}

		/*@page {
		   @bottom-right {
		    content: counter(page) " of " counter(pages);
		   }
		}*/
	}
</style>
<div id="receipt_wrapper">
	<div class="top_btn">
		<!-- <div class="small_button float_left" id="btn_new_sale"><span>New Sale</span></div> -->
		<div class="small_button float_right" id="btn_print"><span>Print</span></div>
	</div>
	<div style="clear:both;"></div>
	
	<div id="receipt_header">
		

		<div id="company_name"><?php echo $this->config->item('company'); ?>
		<span class="inv_type">Past Dues</span>
		</div>
		<div class="left_head">
			<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
			<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
			<?php if(isset($customer))
			{
			?>
				<div id="customer" class="l_text"><div>Customer</div><p><?php echo $cus_info->title.$customer;echo $cus_info->nick_name!=''?"($cus_info->nick_name)":'' ?></p></div>
				<div class="l_text">
					<div>Phone Number</div><p><?php echo $cus_info->phone_number ?></p>
				</div class="l_text">
				<div class="l_text"> 
					<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
				</div>
				<!-- <div class="l_text">
					<div>Email</div><p><?php echo $cus_info->email ?></p>
				</div> -->
				<!-- <div class="l_text">
					<div>Contact</div><p><?php echo $customer; ?></p>
				</div> -->

			<?php
			}
			?>
			
		</div>
		
		<div class="right_head">
			<div id='barcode'>
			<?php //echo "<img src='index.php/barcode?barcode=$qdata->qt_number&text=$qdata->qt_number&width=250&height=50' />"; ?>
			</div>
			<div class="r_text"><div>Page</div><p class="page_counter"></p></div>
			<!-- <div class="r_text"><div>Date From</div><p><?php echo $qdata->qt_number; ?></p></div> -->
			<div class="r_text"><div>Date</div><p><?php echo date("d/m/Y") ?></p></div>
			<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>
			<!-- <div class="r_text"><div>Payment Type</div><p><?php echo $payments['Cash']['payment_type'] ?></p></div> -->
		</div>
	</div>

	<div style="clear:both"></div>

	<table id="receipt_items">
	<thead>
		<tr>
		<!-- <th></th> -->
	    <th><?php echo "No"; ?></th>
	    <th><?php echo "Sale ID" ?></th>
		<th><?php echo "Invoice Number"; ?></th>
		<th>Sale Date</th>
		<th>Balance</th>
		</tr>
	</thead>
	
	<tbody style="border-bottom:2px solid black;">
	<?php
	$i=0;
	$total = 0;
	
	// var_dump($sales_pay);
	foreach($sales_pay as $sp)
	{
	$i++;

	// $paid = $this->sale->get_sale_due_paid($sp->sale_id) + $sp->first_payment;
	// $grand_total = $this->sale->get_sale_due($sp->sale_id)->payment_amount;
	// $total_due = $grand_total - $paid;
	$paid = $this->past_due->get_sale_paid($sp->sale_id);
	$total = $this->past_due->get_sale_total($sp->sale_id);

	?>

		
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $sp->sale_id ?></td>
			<td><?php echo $sp->invoiceid ?></td>
			<td><?php echo date('d-m-Y',strtotime($sp->sale_time)) ?></td>
			<td><?php echo number_format($total-$paid,2) ?></td>
		</tr>

	<?php
		$total+=$total_due;
	}
	?>
	</tbody>
	<tfoot>
		
	<tr>
		<td ></td>
		<td colspan="2"></td>
		<td style='text-align:right;'><?php echo "Total"; ?></td>
		<td style='text-align:right;background:#ddd;font-weight:bold;position:static;'><?php echo to_currency($sales_due); ?></td>
	</tr>
	<tr>
		<td rowspan="2" style="vertical-align: top;">
			Note :
		</td>
		<td colspan="2" rowspan="2" style="text-align: left;">
			<textarea rows="4" style="width: 50%;resize: none;" ><?php echo $this->config->item('due_note') ?></textarea>
		</td>	
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	
	</tfoot>

	</table>
	<div class="footer_signature_wrapper" align="center">
		<div class="footer_signature">
			<div class="left_signature">
				<label>Accountant's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<!-- <div class="right_signature">
				<label>Seller's Signature</label>
				<div class="underline">
					
				</div>
			</div> -->
		</div>
	</div>
	
</div>


<div style="clear:both"></div>


<script type="text/javascript">
$(window).load(function()
{
	window.print();
	
});
$('#btn_print').click(function(){
	window.print();
});

</script>
<?php $this->load->view("partial/footer"); ?>
