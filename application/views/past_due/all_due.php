<?php $this->load->view("partial/header"); ?>
<?php 'Total row= '.$model;?>

<script type="text/javascript">
$(document).ready(function() 
{ 
    init_table_sorting();
    enable_select_all();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{ 
			sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
				5: { sorter: false} 
			} 

		}); 
	}
}

function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.sale_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.sale_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.sale_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>
<style type="text/css">
	#pagination{
		padding: 1% 0;
		float: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' Past Due'; ?></div>
	<div id="title" class="float_right" style="right:0 !important;left:unset;"><a style="text-decoration:underline;" href="<?php echo site_url('past_dues/payment_history') ?>">Payment History</a></div>
	<?php /*?>
	<div id="new_button">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>

	</div>
	<?php */?>
</div>

<div id="table_action_header">
	<div style="padding-left:3%;float:left">

		<label>Customers</label>
		<select id="sel_cus">
			<option value="">All</option>
			<?php foreach ($cus_data as $cu): ?>
				<?php
					$sel = '';
					if ($cus_id==$cu->person_id) {
						$sel = 'selected';
					}
				?>
				<option value="<?php echo $cu->person_id ?>" <?php echo $sel ?>><?php echo $cu->last_name.' '.$cu->first_name ?></option>
			<?php endforeach ?>
		</select>
	</div>
	<!-- <div style="padding-left:1%;float:left;margin:-2px;">
		<label>Date From</label>
		<input type="text" class="date_from" id="date_from" value="<?php echo $_GET['df'] ?>">
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<label>Date To</label>
		<input type="text" class="date_to" id="date_to" value="<?php echo $_GET['dt'] ?>">
	</div> -->
	<div style="padding-left:1%;float:right;margin:-2px;">
		<label>Search</label>
		<input type="text" class="keyword" id="keyword">
	</div>
	<!-- <div style="padding-left:1%;float:left;margin:-2px;">
		<input type="button" class="btn btn-primary" id="btn_filter" value="Filter">
	</div> -->
	<!-- <div style="padding-left:1%;float:right;margin:-2px;">
		<input type="button" class="btn btn-primary" id="btn_print_selected" value="Print Selected">
	</div> -->
	
</div>

<div id="table_holder">
	<!-- TOP PAGINATION -->
	
	<!-- END TOP PAGINATION -->

	<!-- DATATABLE -->
	<table id="due_table" class="tablesorter">
		<thead>
			<th>No</th>
			<th>Customer Name</th>
			<th>Phone Number</th>
			<th>Grand Total</th>
			<th>Paid</th>
			<!-- <th>Returned Amount</th> -->
			<th>Balance</th>
			<th>Actions</th>
		</thead>
		<tbody>
			
		</tbody>
	</table>
	<!-- END DATATABLE -->

	<!-- BOTTOM PAGINATION -->
	
	<!-- END BOTTOM PAGINATION -->
</div>

<div id="feedback_bar"></div>

<script type="text/javascript">
	$('#sel_cus').select2();

	$('#sel_cus').change(function(){
		get_data();
	})

	$('#keyword').keyup(function(){
		get_data();
	})

	function get_data(){
		var cus_id = $('#sel_cus').val();
		var keyword = $('#keyword').val();

		$.post("<?php echo site_url('past_dues/search_past_due') ?>",
		{
			cus_id:cus_id,
			keyword:keyword
		},
		function(data){
			// console.log(data);
			// location.href="<?php echo site_url('past_dues/view_voucher/') ?>"+'/'+data.rec_id;
			$('#due_table tbody').html(data.table)
		},'Json');
	}

	$(document).ready(function(){
		get_data();
	})
</script>

<?php $this->load->view("partial/footer"); ?>