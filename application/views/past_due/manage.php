<?php $this->load->view("partial/header"); ?>
<?php 'Total row= '.$model;?>
<script type="text/javascript">
$(document).ready(function() 
{ 
    init_table_sorting();
    enable_select_all();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{ 
			// sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
				6: { sorter: false} 
			} 

		}); 
	}
}

function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.sale_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.sale_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.sale_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>
<style type="text/css">
	#pagination{
		padding: 1% 0;
		float: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}

	/* Dropdown Button */
.dropbtn {
    background-color: #d6d6d6;
    color: #1b1b1b;
    padding: 5px;
    /* font-size: 16px; */
    border: none;
    cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
    background-color: #ddd;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: black;
    padding: 5px 10px;
    text-decoration: none;
    display: block;
    font-size: 11px !important;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}
</style>
<div id="title_bar">
	<?php
		$cus_name = 'All Past Dues';

		if ($customer && $customer!=' ') {
			$cus_name = $customer;
		}

	?>
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$cus_name; ?></div>
	<?php /*?>
	<div id="new_button">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>

	</div>
	<?php */?>
</div>

<?php /*?>
<div id="table_action_header">
	<ul>
		<li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		
		<li class="float_right">
		<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form>
		</li>
	</ul>
</div>
<?php */?>
<div id="table_action_header">
	<div style="padding-left:3%;float:left">

		<label>Customers</label>
		<select id="sel_cus">
			<?php foreach ($cus_data as $cu): ?>
				<?php
					$sel = '';
					if ($cus_id==$cu->person_id) {
						$sel = 'selected';
					}
				?>
				<option value="<?php echo $cu->person_id ?>" <?php echo $sel ?>><?php echo $cu->last_name.' '.$cu->first_name ?></option>
			<?php endforeach ?>
		</select>
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<label>Date From</label>
		<input type="text" class="date_from" id="date_from" value="<?php echo $_GET['df'] ?>">
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<label>Date To</label>
		<input type="text" class="date_to" id="date_to" value="<?php echo $_GET['dt'] ?>">
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<input type="button" class="btn btn-primary" id="btn_filter" value="Filter">
	</div>
	<div class="dropdown" style="padding-left:1%;float:right;margin:-2px;">
	  <button onclick="myFunction()" class="dropbtn">Print Selected</button>
	  <div id="myDropdown" class="dropdown-content">
	    <a href="javascript:void(0)" id="print_inv">List Invoice</a>
	    <a href="javajscript:void(0)" id="print_item">List Item</a>
	  </div>
	</div>

	
</div>
<div id="table_holder">
	<!-- TOP PAGINATION -->
	<div style="float:left;width:100%;height:25px;">
		<?php 
				$val=20;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
		?>
		<!-- <p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p> -->
		<p>Showing <?php echo $total_rows ?> of <?php echo $total_rows ?> results</p>
	</div>
	<!-- <div style="float:left;">
		Show
		<select id="numpage" class="numpage" name="numpage">
			<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div> -->
	<!-- <div id="pagination">
	<?php echo $this->pagination->create_links();?>

	</div> -->
	<!-- END TOP PAGINATION -->

	<!-- DATATABLE -->
	<?php echo $manage_table; ?>
	<!-- END DATATABLE -->
	<div>
		<div style="margin-top:3%;float:right;">
			<input type="button" value="Pay" id="pay_btn">
		</div>
		<div style="margin-top:3%;float:right;">
			<input type="text" name="pay_amount" onkeypress="return isNumberKey(event);" class="pay_amount" id="amount_usd" value="0">
		</div>
	</div>
	
	<input type="hidden" value="<?php echo $cus_id ?>" id="cus">

	<!-- BOTTOM PAGINATION -->
	<div style="float:left;width:50%;height:25px;">
	<?php 
			$val=20;
			if (isset($_GET['p'])) {
				$val = $_GET['p'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
	<!-- <p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p> -->
	<p>Showing <?php echo $total_rows ?> of <?php echo $total_rows ?> results</p>
	
	</div>
	<!-- <div style="float:left;width:51%;">
		Show
		<select id="numpage" class="numpage" name="numpage">
			<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div> -->
	<div id="pagination">
	<!-- <?php echo $this->pagination->create_links();?> -->
	</div>
	<!-- END BOTTOM PAGINATION -->
</div>
<div id="feedback_bar"></div>
<script src="<?php echo base_url();?>assets/js/jquery-ui.custom.js" type="text/javascript"></script>


<script type="text/javascript">
	function isNumberKey(evt){
	    var charCode = (evt.which) ? evt.which : evt.keyCode
	    if (charCode == 190 || charCode == 46) 
			return true;
	    return !(charCode > 31 && (charCode < 48 || charCode > 57));
	}
	$(document).on('click','.pay_row',function(){
		get_total();
	});
	$(document).on('change','.sel_pay',function(){
		get_total();
	});
	$(document).on('change','#select_all',function(){
		get_total();
	});

	function get_total(){
		var total_usd = 0;
		var chk = $('.sel_pay');
		chk.each(function(){
			var v = $(this);
			var usd = v.attr('d');
			// console.log(v.attr('checked'));
			if (v.attr('checked')) {
				total_usd += parseFloat(usd);
			};
		});
		$('#amount_usd').val(parseFloat(total_usd));
		// $('#amount_b').val(formatDecimal(total_b));
		
		
	}

	$('#pay_btn').click(function(){
		

		var sale_ids = $('.sel_pay:checkbox:checked').map(function () {
					        return $(this).attr('s');
					    }).get();

		var cus_id = $('#cus').val()
		var msg ='';
		var amount_usd = $('#amount_usd').val();
		
		// console.log(sale_ids);
		// console.log(cus_id);
		// console.log(amount_usd);
		if (amount_usd == '' || amount_usd <=0) {
			msg= 'Please Add Amount To Pay';
			alert(msg);
			
			return;
		};
		if (cus_id=='') {
			msg= 'No Customer Selected';
			alert(msg);
			return;
		};
		if (sale_ids.length<1) {
			alert('No Sale Selected');
			return;
		};


		if(confirm("Confirm ?")){
			
				$.post("<?php echo site_url('past_dues/save_pay') ?>",
				{
					cus_id:cus_id,
					amount_usd:amount_usd,
					sale_ids:sale_ids
				},
				function(data){
					console.log(data);
					location.href="<?php echo site_url('past_dues/view_voucher/') ?>"+'/'+data.rec_id;

				},'Json');
		}
	});

	$('#sel_cus').select2();
	$('.date_from,.date_to').datepicker({
		dateFormat: 'dd-mm-yy'
	});

	$('#btn_filter').click(function(){
		var cus_id = $('#sel_cus :selected').val();
		var df = $('#date_from').val();
		var dt = $('#date_to').val();

		location.href = "<?php echo site_url('past_dues/due_detail') ?>"+'/'+cus_id+"?df="+df+"&dt="+dt+"";
	});

	$('#print_inv').click(function(){
		var sale_ids = $('.sel_pay:checkbox:checked').map(function () {
					        return $(this).attr('s');
					    }).get();
		if (sale_ids.length<1) {
			alert('No Sale Selected');
			return;
		};
		var ids = sale_ids.join(':');

		window.open(
		  "<?php echo site_url('past_dues/print_dues') ?>"+"/"+ids,
		  '_blank<?php echo rand(0,999)?>' // <- This is what makes it open in a new window.
		);


		console.log(ids);

	});
	$('#print_item').click(function(){
		var sale_ids = $('.sel_pay:checkbox:checked').map(function () {
					        return $(this).attr('s');
					    }).get();
		if (sale_ids.length<1) {
			alert('No Sale Selected');
			return;
		};
		var ids = sale_ids.join(':');

		window.open(
		  "<?php echo site_url('past_dues/print_list_item') ?>"+"/"+ids,
		  '_blank_<?php echo rand(0,999)?>'
		);


		console.log(ids);

	});
	/* When the user clicks on the button, 
	toggle between hiding and showing the dropdown content */
	function myFunction() {
	    document.getElementById("myDropdown").classList.toggle("show");
	}

	// Close the dropdown menu if the user clicks outside of it
	window.onclick = function(event) {
	  if (!event.target.matches('.dropbtn')) {

	    var dropdowns = document.getElementsByClassName("dropdown-content");
	    var i;
	    for (i = 0; i < dropdowns.length; i++) {
	      var openDropdown = dropdowns[i];
	      if (openDropdown.classList.contains('show')) {
	        openDropdown.classList.remove('show');
	      }
	    }
	  }
	}
</script>

<?php $this->load->view("partial/footer"); ?>