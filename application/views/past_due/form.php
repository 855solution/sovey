<?php
echo form_open('past_dues/save/',array('id'=>'past_due_form'));
?>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend>Past due information</legend>
<?php //$this->load->view("past_due/form_basic_info"); ?>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_sale_id')); ?>
<div class='form_field'>
<?php echo form_input(array(
'readonly'=> 'readonly',
'name'=>'sale_id',
'id'=>'sale_id',
'value'=>$person_info->sale_id)
);?>
</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_customer_id')); ?>
<div class='form_field'>
<?php echo form_input(array(
'readonly'=> 'readonly',
'name'=>'customer_id',
'id'=>'customer_id',
'value'=>$person_info->customer_id)
);?>
</div>
</div>



<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_payment_amount')); ?>
<div class='form_field'>
<?php echo form_input(array(
'readonly'=> 'readonly',
'name'=>'payment_amount',
'id'=>'payment_amount',
'value'=>$person_info->payment_amount)
);?>
</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_total_due')); ?>
<div class='form_field'>
<?php echo form_input(array(
'readonly'=> 'readonly',
'name'=>'dept',
'id'=>'dept',
'value'=>$person_info->dept)
);?>
</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Adding Charge:','charg',array('class'=>'required')); ?>
<div class='form_field'>
<?php echo form_input(array(
'name'=>'adding_charge',
'id'=>'adding_charge',
)
);?>
<div id="exceed_error"></div>
</div>
</div>



<?php
echo form_submit(array(
'name'=>'submit',
'id'=>'submit',
'value'=>$this->lang->line('common_submit'),
'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
$('#submit').click(function(){
var adding_chare=document.getElementById('adding_charge').value;
var dept=document.getElementById('dept').value;

var messsage="<span style='margin-top:10px;background-color: #ffe3e3;border: 1px solid #e05c5c;border-radius: 3px;font-size:12px; color:red; padding:5px 5px 5px 5px;'>your adding chage is exceed of your dept.</span>";
if(parseFloat(adding_chare)>parseFloat(dept)){
//alert('exceed value');
$('#exceed_error').html(messsage);

}


});


$('#past_due_form').validate({
submitHandler:function(form)
{
$(form).ajaxSubmit({
success:function(response)
{
tb_remove();
//post_person_form_submit(response);
location.reload();
},
dataType:'json'
});

},
errorLabelContainer: "#error_message_box",
wrapper: "li",
rules: 
{
charg: "required"
//last_name: "required",
//email: "email"
},
messages: 
{
charg: "<?php echo 'The adding charge is a required field.'; ?>"

}
});
});
</script>