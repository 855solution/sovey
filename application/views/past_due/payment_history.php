<?php $this->load->view("partial/header"); ?>



<style type="text/css">
	#pagination{
		padding: 1% 0;
		float: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left">Payment History</div>
	
</div>
<div id="table_action_header">
	<div style="padding-left:3%;float:left">

		<label>Customers</label>
		<select id="cus_sel">
			<option value="">All</option>
			<?php foreach ($cus->result() as $c): ?>
				<option value="<?php echo $c->person_id ?>"><?php echo "$c->last_name $c->first_name"; ?></option>
			<?php endforeach ?>
		</select>
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<label>Date From</label>
		<input type="text" class="date_from" id="date_from" value="<?php echo date('d-m-Y') ?>">
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<label>Date To</label>
		<input type="text" class="date_to" id="date_to" value="<?php echo date('d-m-Y') ?>">
	</div>
	<div style="padding-left:1%;float:left;margin:-2px;">
		<input type="button" class="btn btn-primary" id="btn_filter" value="Filter">
	</div>
	
</div>
<div id="table_holder">
	<!-- TOP PAGINATION -->
	
	<!-- END TOP PAGINATION -->

	<!-- DATATABLE -->
	<table id="pay_table" class="tablesorter">
		<thead>
			<th>No</th>
			<th>Customer Name</th>
			<th>Paid</th>
			<th>Date</th>
			<th>By</th>
			<th>Actions</th>
		</thead>
		<tbody>

		</tbody>
	</table>
	<!-- END DATATABLE -->

	<!-- BOTTOM PAGINATION -->
	
	<!-- END BOTTOM PAGINATION -->
</div>

<div id="feedback_bar"></div>
<script src="<?php echo base_url();?>assets/js/jquery-ui.custom.js" type="text/javascript"></script>

<script type="text/javascript">
	// $('#cus_sel').change(function(){
	// 	get_data();
	// });
	function get_data(){
		var cus_id = $('#cus_sel').val();
		var df = $('#date_from').val();
		var dt = $('#date_to').val();
		$.post("<?php echo site_url('past_dues/get_payment_history') ?>",
			{
				cus_id:cus_id,
				date_from:df,
				date_to:dt
			},
			function(d){
				$('#pay_table tbody').html(d.pay_his);
			},'Json')
	}
	$(document).ready(function(){
		get_data();
		$('#cus_sel').select2();
		// $('#date_from').datepicker();
		// $('#date_to').datepicker();
		// $("#ui-datepicker-div").remove();
		$('#date_from,#date_to').datepicker({
			dateFormat:'dd-mm-yy'
		});
	});
	$('#btn_filter').click(function(){
		get_data();
	});
</script>
<?php $this->load->view("partial/footer"); ?>