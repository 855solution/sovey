<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('colors/save/'.$color_info->color_id,array('id'=>'color_form'));
?>
<fieldset id="color_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("colors_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('colors_color_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'color_name',
		'id'=>'color_name',
		'value'=>$color_info->color_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('colors_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$color_info->description)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#color_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_color_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			color_name:"required"
   		},
		messages:
		{
			color_name:"<?php echo $this->lang->line('colors_name_required'); ?>",	
		}
	});
});
</script>