<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('partplacements/save/'.$partplacement_info->partplacement_id,array('id'=>'partplacement_form'));
?>
<fieldset id="partplacement_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("partplacements_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('partplacements_partplacement_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'partplacement_name',
		'id'=>'partplacement_name',
		'value'=>$partplacement_info->partplacement_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('partplacements_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$partplacement_info->description)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#partplacement_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_partplacement_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			partplacement_name:"required"
   		},
		messages:
		{
			partplacement_name:"<?php echo $this->lang->line('partplacements_name_required'); ?>",	
		}
	});
});
</script>