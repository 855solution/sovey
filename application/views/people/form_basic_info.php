<?php $controller = $this->router->class ?>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('cus_title').':', 'cus_title'); ?>
	<div class='form_field'>
	<?php $option = array('Ms.'=>'Ms.',
						'Mr.'=>'Mr.'); ?>
	<?php echo form_dropdown('cus_title',$option,$select_title,"id='cus_title'");
	?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_last_name').':', 'last_name'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'value'=>$person_info->last_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_first_name').':', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'value'=>$person_info->first_name,
		'required'=>'required')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Company'.':', 'Company'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'nick_name',
		'id'=>'nick_name',
		'value'=>$person_info->nick_name)
	);?>
	</div>
</div>


<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_email').':', 'email'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'id'=>'email',
		'value'=>$person_info->email)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_phone_number').':', 'phone_number',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'required'=>'required',
		'name'=>'phone_number',
		'id'=>'phone_number',
		'class'=>'pnumber',
		'onkeypress'=>'return isNumber(event)',
		'value'=>$person_info->phone_number));?>
		<img id="phone_number_chk" class="hide" src="<?php echo base_url('assets/img/tchk.png') ?>">
		<span id="phone_number_ext" class="hide" style="color:red;">Phone Number Already Exist !</span>

	</div>
	
	
</div>
<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_phone_number').' 2:', 'phone_number2'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number2',
		'id'=>'phone_number2',
		'class'=>'pnumber',
		'onkeypress'=>'return isNumber(event)',
		'value'=>$person_info->phone_number_2));?>
		<img id="phone_number2_chk" class="hide" src="<?php echo base_url('assets/img/tchk.png') ?>">
		<span id="phone_number2_ext" class="hide" style="color:red;">Phone Number Already Exist !</span>

	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_address_1').':', 'address_1'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'address_1',
		'id'=>'address_1',
		'value'=>$person_info->address_1));?>
	</div>
</div>

<!--<div class="field_row clearfix">-->
<?php form_label($this->lang->line('common_address_2').':', 'address_2'); ?>
	<!--<div class='form_field'>-->
	<?php form_input(array(
		'name'=>'address_2',
		'id'=>'address_2',
		'value'=>$person_info->address_2));?>
<!--	</div>
</div>-->

<!--<div class="field_row clearfix">	-->
<?php form_label($this->lang->line('common_city').':', 'city'); ?>
	<!--<div class='form_field'>-->
	<?php form_input(array(
		'name'=>'city',
		'id'=>'city',
		'value'=>$person_info->city));?>
<!--	</div>
</div>-->

<!--<div class="field_row clearfix">	-->
<?php form_label($this->lang->line('common_state').':', 'state'); ?>
	<!--<div class='form_field'>-->
	<?php form_input(array(
		'name'=>'state',
		'id'=>'state',
		'value'=>$person_info->state));?>
<!--	</div>
</div>-->

<!--<div class="field_row clearfix">	-->
<?php form_label($this->lang->line('common_zip').':', 'zip'); ?>
	<!--<div class='form_field'>-->
	<?php form_input(array(
		'name'=>'zip',
		'id'=>'zip',
		'value'=>$person_info->zip));?>
<!--	</div>
</div>-->

<!--<div class="field_row clearfix">	-->
<?php form_label($this->lang->line('common_country').':', 'country'); ?>
	<!--<div class='form_field'>-->
	<?php form_input(array(
		'name'=>'country',
		'id'=>'country',
		'value'=>$person_info->country));?>
<!--	</div>
</div>-->
<?php if ($controller=='customers'): ?>
	<div class="field_row clearfix">
		<?php echo form_label($this->lang->line('payment_term').':','payment_term') ?>
		<div class="form_field">
			<?php
				$period = '';
				$monthly = '';
				if ($person_info->payment_term=='Period') {
					$period = 'checked';
				}else if($person_info->payment_term=='Monthly'){
					$monthly = 'checked';
				}
			?>
			 <input type="radio" name="pay_term" id='period' value="Period" <?php echo $period ?>> Period
			 <input type="radio" name="pay_term" id='monthly' value="Monthly" <?php echo $monthly ?>> Monthly
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label($this->lang->line('customer_group').':','customer_group') ?>
		
		<div class="form_field">
			<select class="form-control" name="cg">
				<option value="">Select</option>
				<?php foreach ($cg as $c): ?>
					<?php
						$sel = '';
						if ($c->cg_id==$person_info->customer_group) {
							$sel = 'selected';
						}
					?>
					<option value="<?php echo $c->cg_id ?>" <?php echo $sel ?>><?php echo $c->cg_name ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
<?php endif ?>
<?php if ($controller=='employees'): ?>
	<div class="field_row clearfix">
		<?php echo form_label('Is Admin :') ?>
		<?php
			$chk = '';
			if ($person_info->is_admin==1) {
				$chk = 'checked';
			}
		?>
		<div class='form_field'>
			<input type="checkbox" name="is_admin" id="is_admin" <?php echo $chk ?>>
		</div>
	</div>
<?php endif ?>

<div class="field_row clearfix">	
<?php echo form_label($this->lang->line('common_comments').':', 'comments'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'comments',
		'id'=>'comments',
		'value'=>$person_info->comments,
		'rows'=>'5',
		'cols'=>'17')		
	);?>
	</div>
</div>
<input type="hidden" value="<?php echo $person_info->person_id ?>" id="person_id">
<script type="text/javascript">
	// $('#first_name,#last_name').change(function(){
	// 	enable_submit();
	// });
	// var pexist = [];
	$('#phone_number,#phone_number2').keyup(function(){
		check_phone_number($(this).val(),$(this).attr('id'));
		// enable_submit();
	});
	$('#phone_number,#phone_number2').change(function(){
		check_phone_number($(this).val(),$(this).attr('id'));
		// enable_submit();
	});
	function check_phone_number(val,el_id){
		$.post("<?php echo site_url('customers/check_phone_number') ?>",
				{
					val:val
				},
				function(d){
					// console.log(d.t);
					// console.log(el_id);
					if (d.t==true) {
						$('#'+el_id+'_chk').addClass('hide');
						$('#'+el_id+'_ext').removeClass('hide');
						// pexist.push(d.t);
					}else{
						$('#'+el_id+'_chk').removeClass('hide');
						$('#'+el_id+'_ext').addClass('hide');
						// pexist.push(d.t);
						
					};

				},'Json');
	}
	// function submit_check(p1,p2){
	// 	var fn = $('#first_name').val();
	// 	// var ln = $('#last_name').val();
		
	// 	// if (ln == '' ) {
	// 	// 	alert('Last Name Required!');
	// 	// 	return;
	// 	// };
	// 	if (fn=='') {
	// 		alert('First Name Required!');
	// 		return;
	// 	};
	// 	if (p1!='') {
	// 		$.post("<?php echo site_url('customers/submit_check') ?>",
	// 		{
	// 			p1:p1,
	// 			p2:p2
	// 		},
	// 		function(d){
				
	// 			if (d.tp1=='y') {
	// 				alert('Phone Number Already Exist!');
	// 			}else if (d.tp2=='y') {
	// 				alert('Phone Number 2 Already Exist!')
	// 			}else if(d.tp1=='n' && d.tp2=='n'){
	// 				// $('#customer_form').submit();
	// 				$('#submit_real').click();
	// 			}
	// 		},'Json');
			
	// 	}else{
	// 		alert('Phone Number Required!');
	// 		return;
	// 	}
		
		
	// }


	// $('#submit').click(function(){
	// 	var p1 = $('#phone_number').val();
	// 	var p2 = $('#phone_number2').val();

	// 	if ($('#person_id').val()=='') {
	// 		submit_check(p1,p2);
	// 	}else{
	// 		$('#submit_real').click();
	// 	};
	// });
		

	
// $('#cus_title').select2({
// 	minimumResultsForSearch: -1
// });
		
	// })
</script>