<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function() 
{ 
    init_table_sorting();
    enable_select_all();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{ 
			sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
				2: { sorter: false}, 
				10: { sorter: false} 
			} 

		}); 
	}
}

function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>
<style type="text/css">
	#pagination{
		padding: 1% 0;
		text-align: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
	.alert_danger{
	    background: #ffd0d0;
	    border: 1px solid #ffbaba;
	    padding: 15px;
	    font-size: 15px;
	    border-radius: 5px;
	    color: #bd5353;
	    margin-bottom: 15px;
	}
	.hide{
		display: none;
	}
</style>

<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="new_button">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		"<div class='big_button' style='float: left;'><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>
		<?php echo anchor("$controller_name/view_customer_group/width:$form_width",
		"<div class='big_button' style='float: left;'><span>".$this->lang->line('customer_group')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('customer_group')));
		?>
		<?php if ($controller_name =='customers') {?>
			<?php echo anchor("$controller_name/excel_import/width:$form_width",
			"<div class='big_button' style='float: left;'><span>Excel Import</span></div>",
				array('class'=>'thickbox none','title'=>'Import Items from Excel'));
			?>	
		<?php } ?>
	</div>
</div>
<?php
	$error = '';
	if (isset($_GET['error'])) {
		$error = $_GET['error'];	
	}
	$ad = 'hide';
	if ($error!='') {
		$ad='';
		if ($error=='pn') {
			$alert = 'Phone Number Already Exist !';
		}
	}
	
?>
<div class="alert_danger <?php echo $ad ?>">
	<p><?php echo $alert ?></p>
</div>

<div id="table_action_header">
	<ul>
		<li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li class="float_left"><span><a href="#" id="email"><?php echo $this->lang->line("common_email");?></a></span></li>
		<li class="float_right">
		<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form>
		</li>
	</ul>
</div>
<div id="table_holder">
<!-- TOP PAGINATION -->
	<div style="float:left;width:50%;height:25px;">
		<?php 
				$val=20;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
		?>
		<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	<!-- <div style="float:left;">
		Show
		<select id="numpage" class="numpage" name="numpage">
			<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div> -->
	</div>
	<!-- END TOP PAGINATION -->

	<?php echo $manage_table; ?>

	<!-- BOTTOM PAGINATION -->
	<div style="float:left;width:50%;height:25px;">
	<?php 
			$val=20;
			if (isset($_GET['p'])) {
				$val = $_GET['p'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
	<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<!-- <div style="float:left;width:51%;">
		Show
		<select id="numpage" class="numpage" name="numpage">
			<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div> -->
	<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	</div>
	<!-- END BOTTOM PAGINATION -->
</div>
<div id="feedback_bar"></div>

<script type="text/javascript">
	$(".numpage").change(function(){
		var p=$(this).val();
		location.href="<?php echo $url ?>"+p;
	})
</script>
<?php $this->load->view("partial/footer"); ?>
