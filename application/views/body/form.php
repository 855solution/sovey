<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('body/save/'.$body_info->body_id,array('id'=>'body_form'));
?>
<fieldset id="make_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("body_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('body_body_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'body_name',
		'id'=>'body_name',
		'value'=>$body_info->body_name)
	);?>
	</div>
	<label class='name_er' style="color:red;display:none;">Name Exist !</label>

</div>

<input type="hidden" class="chk_id" value="<?php echo $body_info->body_id ?>">

<!-- <div class="field_row clearfix">
<?php echo form_label($this->lang->line('body_deleted').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$make_info->description)
	);?>
	</div>
</div> -->

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#body_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_make_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			body_name:"required"
   		},
		messages:
		{
			body_name:"<?php echo $this->lang->line('body_name_required'); ?>",	
		}
	});
});
$('#body_name').keyup(function(){
	var n = $('#body_name').val();
	var nid = $('.chk_id').val();
	$.ajax({
		url:"<?php echo site_url('body/check_exist') ?>",
		type:'post',
		dataType:'Json',
		data:{
			name:n,
			id:nid
		},
		success:function(re){
			if (re==1) {
				$('#body_name').css('color','red');
				$('.name_er').css('display','block');
			}else{
				$('#body_name').css('color','black');
				$('.name_er').css('display','none');

			}
		}
	});
});
</script>