<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<style type="text/css">
	#pagination{
		padding: 1% 0;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
	body{
		background: white;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?>
	</div>
</div>

<div class="well">
	<div class="row">
		<div class="col-md-12">
			<button class="btn btn-warning pull-right" id="btn_set_user" data-toggle='modal' data-target='#user-modal'>Set User Permission</button>
			<button class="btn btn-primary" id="btn_add" data-toggle='modal' data-target='#perm-form-modal'><i class="glyphicon glyphicon-plus"></i>New Permission</button>
		</div>
	</div>
	
</div>

<div id="table_holder">
	<!-- TOP PAGINATION -->


	<!-- DATATABLE -->
	
	<table class="tablesorter perm_table">
		<thead class="bg-primary">
			<th>No</th>
			<th>Name</th>
			<th>Description</th>
			<th colspan="3" width="10%">Actions</th>
		</thead>
		<tbody>
			<?php $i=0;foreach ($permission as $p): $i++;?>
				<tr>
					<td><?php echo $i ?></td>
					<td><?php echo $p->perm_name ?></td>
					<td><?php echo $p->perm_desc ?></td>
					<td><a data-id='<?php echo $p->perm_id ?>' href="javascript:void(0)" class="perm_detail label label-primary" data-toggle="modal" data-target="#myModal">Permission</a></td>
					<td><a data-id='<?php echo $p->perm_id ?>' href="javascript:void(0)" class="perm_edit label label-primary" data-toggle="modal" data-target="#perm-form-modal">Edit</a></td>
					<?php if ($p->perm_id!=4): ?>
					<td><a data-id='<?php echo $p->perm_id ?>' href="javascript:void(0)" class="perm_delete label label-danger">Delete</a></td>
						
					<?php endif ?>
				</tr>
			<?php endforeach ?>
		</tbody>

	</table>
	<!-- END DATATABLE -->
	
	
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width:90%;">

    <!-- Modal content-->
    <div class="modal-content">
    	
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <input type="hidden" id="h-is_edit" value="">
        <input type="hidden" id="h-perm_id" value="">
        <h4 class="modal-title">Fetching Data...</h4>
        <p class="modal-desc"></p>
      </div>
      <div class="modal-body">
        <table class="table table-fixed table-bordered table-striped table-condensed pd-table">
        	<thead>
        		<th>No</th>
        		<th>Menu Name</th>
        		<th>View</th>
        		<th>Add</th>
        		<th>Update</th>
        		<th>Delete</th>
        		<th>Sold Item</th>
        		<th>Create Quote</th>
        		<th>Create Deposit</th>
        		<th>Complete Sale</th>
        		<th>View Deposit</th>
        		<th>View Quote</th>
        		<th>Add Payment</th>
        		<th>List Sales</th>
        	</thead>
        	<tbody>
        		
        	</tbody>
        </table>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" id="btn_save_detail" value="Save">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>

  </div>
</div>
<!--  -->
<div id="perm-form-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    	<form method="post" action="<?php echo site_url('permission/save_perm') ?>">
    		<input type="hidden" id="h-perm-id" value="" name="perm_id"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Permission</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-12">
	      		<label>Permission Name</label>
	        	<input type="text" name="perm_name" id="perm_name" class="form-control">
	      	</div>
	      	<div class="col-md-12">
	      		<label>Description</label>
	        	<textarea name="perm_desc" id="perm_desc" class="form-control"></textarea>

	      	</div>
      	</div>
      	
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" id="btn_save_perm" value="Save">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    	</form>

    </div>

  </div>
</div>
<!--  -->
<div id="user-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Set User Permission</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-6">
        		<label>User</label>
        		<select class="form-control" id="user_user_id">
        			<option value="">Select</option>
        		</select>
        	</div>
        	<div class="col-md-6">
        		<label>Permission</label>
        		<select class="form-control" id="user_perm_id">
        			<option>Select</option>
        		</select>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
      	<button class="btn btn-primary" id="btn_save_set_user">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="feedback_bar"></div>

<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>

<script type="text/javascript">
	$('#btn_set_user').click(function(){
		get_set_user();
	});

	function get_set_user(){
		clear_perm_modal();
		$.post("<?php echo site_url('permission/get_set_user') ?>",
	    {
	        perm_id: ''
	    },
	    function(d){
	    	$('#user_user_id').html(d.all_user);
	    	$('#user_perm_id').html(d.all_perm);
	    },'Json');
	};
	$('#btn_add').click(function(){
		clear_perm_modal();
	});	
	function clear_perm_modal(){
		$('.modal-title').text('Permission');
		$('#perm_name').val('');
    	$('#perm_desc').val('');
    	$('#h-perm-id').val('');
	}
	$('.perm_detail').click(function(){
		var perm_id = $(this).attr('data-id');
		get_detail(perm_id);
	})
	function get_detail(perm_id){
		$.post("<?php echo site_url('permission/get_detail') ?>",
	    {
	        perm_id: perm_id
	    },
	    function(d){
	    	$('#h-is_edit').val(d.is_edit);
	    	$('#h-perm_id').val(d.perm_id);
	    	$('.modal-title').text('Permission: '+d.perm_title);
	    	$('.modal-desc').text(d.perm_desc);
	    	$('.pd-table tbody').html(d.perm_table);
	    },'Json');
	}
	$('#btn_save_detail').click(function(){
	    $('body').addClass('loading');

		var perm_id = $('#h-perm_id').val();
 		var tr = $('.pd-table tbody tr');
 		var pdata = [];
 		tr.each(function(){
 			var m_id = $(this).find('input').closest('.m_id').val();
 			var acc = 0,v=0,a=0,u=0,d=0,rq=0,rd=0,rs=0,vd=0,vq=0,ap=0,ls=0,st=0;
 			var chk = $(this).find('input').closest('.m-access').is(':checked');
 			var cv = $(this).find('input').closest('.m-view').is(':checked');
 			var ca = $(this).find('input').closest('.m-add').is(':checked');
 			var cu = $(this).find('input').closest('.m-update').is(':checked');
 			var cd = $(this).find('input').closest('.m-delete').is(':checked');
 			var crq = $(this).find('input').closest('.m-create-q').is(':checked');
 			var crd = $(this).find('input').closest('.m-create-d').is(':checked');
 			var crs = $(this).find('input').closest('.m-create-s').is(':checked');
 			var cvq = $(this).find('input').closest('.m-view-q').is(':checked');
 			var cvd = $(this).find('input').closest('.m-view-d').is(':checked');
 			var cap = $(this).find('input').closest('.m-add-payment').is(':checked');
 			var cls = $(this).find('input').closest('.m-list-sale').is(':checked');
 			var cst = $(this).find('input').closest('.m-sold-item').is(':checked');
 			if (cst) {
 				st = 1;
 			};
 			if (cls) {
 				ls = 1;
 			};
 			if (chk) {
 				acc = 1;
 			};
 			if (cv) {
 				v=1;
 			};
 			if (ca) {
 				a=1;
 			};
 			if (cu) {
 				u=1;
 			};
 			if (cd) {
 				d=1;
 			};
 			if (crq) {
 				rq=1;
 			};
 			if (crd) {
 				rd=1;
 			};
 			if (crs) {
 				rs=1;
 			};
 			if (cvq) {
 				vq = 1;
 			};
 			if (cvd) {
 				vd=1;
 			};
 			if (cap) {
 				ap = 1;
 			};
 			pdata.push({
 						m_id:m_id,
 						access:acc,
 						view:v,
 						add:a,
 						update:u,
 						delete:d,
 						create_q:rq,
 						create_d:rd,
 						create_s:rs,
 						view_deposit:vd,
 						view_quote:vq,
 						add_payment:ap,
 						list_sale:ls,
 						sold_item:st
 						});


 		})
 		save_detail(perm_id,pdata);
 		
	});
	function save_detail(perm_id,perm_detail){
		var is_edit = $('#h-is_edit').val();
		$.post("<?php echo site_url('permission/save_detail') ?>",
	    {
	        perm_id: perm_id,
	        perm_detail:perm_detail,
	        is_edit:is_edit
	    },
	    function(d){
	    	console.log(d);
	    	$('body').removeClass('loading');
	    	location.href = "<?php echo site_url('permission') ?>";
	    },'Json');
	}
	$('.perm_edit').click(function(){
		var id = $(this).attr('data-id');
		
		get_perm(id);
	});
	function get_perm(perm_id){
		$.post("<?php echo site_url('permission/get_perm') ?>",
	    {
	        perm_id: perm_id
	    },
	    function(d){
	    	$('#perm_name').val(d.perm_name);
	    	$('#perm_desc').val(d.perm_desc);
	    	$('#h-perm-id').val(d.perm_id);
	    },'Json');
	}
	$('.perm_delete').click(function(){
		var perm_id = $(this).attr('data-id');
		if (confirm('Confirm ?')) {
			delete_perm(perm_id);
			// alert('con');
		};	

	});
	function delete_perm(perm_id){
		location.href = "<?php echo site_url('permission/delete_perm') ?>"+"/"+perm_id;
	}

	$('#btn_save_set_user').click(function(){
		var perm_id = $('#user_perm_id :selected').val();
		var user_id = $('#user_user_id :selected').val();

		if (perm_id !='' && user_id) {
			save_set_user(perm_id,user_id);
		}else{
			alert('Please Select User & Permission');

		};

	});
	function save_set_user(perm_id,user_id){
		$.post("<?php echo site_url('permission/save_set_user') ?>",
	    {
	        user_perm_id: perm_id,
	        user_user_id:user_id
	    },
	    function(d){
	    	console.log(d);
	    	$("[data-dismiss=modal]").trigger({ type: "click" });
	    },'Json');
	}
</script>
<?php $this->load->view("partial/footer"); ?>