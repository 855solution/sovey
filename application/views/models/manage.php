
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}

	.name_er{
		color: red;
	}
</style>
<?php $er = $this->session->flashdata('er') ?>
<?php if ($er): ?>
	<div class="alert alert-danger">
		<p><?php echo $er ?></p>
	</div>
<?php endif ?>

<div class="panel panel-default">
  <!-- Default panel contents -->
  	<div class="panel-heading">
  		<div class="row">
  			<div class="col-md-6 pull-left">
  				<h4>List Models</h4>
	  		</div>
	  		<?php if ($can_add==1): ?>
	  			<div class="col-md-6 pull-right">
		  			<button class="btn btn-primary pull-right btn_add" data-toggle='modal' data-target='#myModal'>New</button>
		  		</div>
	  		<?php endif ?>
	  		
	  		

  		</div>
  		
  	</div>
  	<div class="panel-body">
    	<div class="col-md-12">
			<table class="table table-bordered table-striped tdata">
				<thead>
					<th>Nº</th>
					<th>Model Name</th>
					<th>Description</th>
					<th>Make</th>
					<th>Actions</th>
					<th></th>
				</thead>
				<tbody>
					<?php echo $table ?>
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>

  
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Make</h4>
      </div>
      <form action="<?php echo site_url('models/save') ?>" method='post'>
      	
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12 form-group">
	        		<div class="col-md-6">
	        			<label>Make :</label>
	        		</div>
	        		<div class="col-md-6">
	        			<select name="make_id" class="form-control make_id" required="">
	        				<option value="">Select</option>
	        				<?php foreach ($make_data as $mk): ?>
	        					<option value="<?php echo $mk->make_id ?>"><?php echo $mk->make_name ?></option>
	        				<?php endforeach ?>
	        			</select>
	        		</div>
	        	</div>
	        	<div class="col-md-12 form-group">
	        		<div class="col-md-6">
	        			<label>Model Name :</label>
	        		</div>
	        		<div class="col-md-6">
	        			<input type="text" name="model_name" class="form-control model_name" value="" required="">
	        			<p class="name_er hide">Name Exist!</p>

	        		</div>
	        	</div>
	        	<div class="col-md-12 form-group">
	        		<div class="col-md-6">
	        			<label>Description :</label>
	        		</div>
	        		<div class="col-md-6">
	        			<textarea name="description" class="form-control description"></textarea>
	        		</div>
	        	</div>
	        	
	        </div>
      	</div>

      	<div class="modal-footer">
      		<input type="hidden" value="" class="model_id" name="model_id">
	        <input type="submit" class="btn btn-primary" value="Save">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>
      </form>

    </div>

  </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootbox/bootbox.min.js') ?>"></script>


<script type="text/javascript">
	$('.tdata').DataTable();
	$('.make_id').select2({width:'100%'});
	check_select();
	$('.make_id').change(function(){
		check_select();
	})
	$('.model_name').keyup(function(){
		var n = $('.model_name').val();
		var id = $('.model_id').val();
		var m = $('.make_id').val();
		$.ajax({
			url:"<?php echo site_url('models/check_name') ?>",
			type:'post',
			dataType:'Json',
			data:{
				name:n,
				id:id,
				m:m
			},success:function(re){
				if (re==1) {
					$('.name_er').removeClass('hide');
					$('.model_name').css('color','red');
				}else{
					$('.name_er').addClass('hide');
					$('.model_name').css('color','black');
				}
			}
		});
	})

	$('.tdata tbody').on('click','.btn_del',function(){
	
		var id = $(this).attr('f');
		var text = $(this).closest('tr').find('td:eq(1)').text();

		bootbox.confirm('សម្រេចលុប '+text+' ?',function(result){
			if (result===true) {
				location.href="<?php echo site_url('models/delete') ?>"+"/"+id;
			};
		});
	})
	$('.tdata tbody').on('click','.btn_edit',function(){
	
		var d = $(this).attr('d');
		var n = $(this).attr('n');
		var id = $(this).attr('f');
		var m = $(this).attr('m');
		$('.model_name').css('color','black');
		$('.name_er').addClass('hide');
		$('.make_id').val(m).trigger('change.select2');
		check_select();
		$('.model_id').val(id);
		$('.model_name').val(n);
		$('.description').text(d);
		$('.modal-title').text('Edit Models');

	});

	$('.btn_add').click(function(){
		$('.description').text('');
		$('.model_name').val('');
		$('.model_id').val('');
		$('.make_id').val('').trigger('change.select2');
		$('.name_er').addClass('hide');
		$('.modal-title').text('New Models');
	});

	function check_select(){
		var make = $('.make_id').val();
		if (make!='') {
			$('.model_name').prop('disabled',false);
			$('.description').prop('disabled',false);
		}else{
			$('.model_name').prop('disabled',true);
			$('.description').prop('disabled',true);


		}
	}

</script>