<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('models/save/'.$model_info->model_id,array('id'=>'model_form'));
?>
<fieldset id="model_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("models_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('models_model_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'model_name',
		'id'=>'model_name',
		'value'=>$model_info->model_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('makes_make_name').':', 'make',array('class'=>'required wide')); ?>
    <div class='form_field'>
    <?php echo form_dropdown('make_id', $makes, $selected_make);?>
    </div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('models_card_model_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'model_description',
		'id'=>'model_description',
		'value'=>$model_info->model_description)
	);?>
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#model_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_model_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			model_name:"required",
			make_id: "required",
			
   		},
		messages:
		{
			model_name:"<?php echo $this->lang->line('models_name_required'); ?>",	
			make_id:"<?php echo $this->lang->line('makes_name'); ?>",	
		}
	});
});
</script>