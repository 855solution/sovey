<?php
$s='';
$part_name='';
$make='';
$model='';
$s_year='';
$t_year='';
$s_price='';
$t_price='';
if(isset($_GET['n'])){
	$part_name=$_GET['n'];
	$make=$_GET['m'];
	$model=$_GET['mo'];
	$s_year=$_GET['sy'];
	$t_year=$_GET['ty'];
	$s_price=$_GET['sp'];
	$t_price=$_GET['tp'];
}
if(isset($se))
	$s=$se;
?>
<html>
	<title>Auto Part</title>
	<head>
		<link href="<?php echo base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/site/product_scroll/assets/owl.carousel.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/site/product_scroll/assets/owl.theme.css') ?>" rel="stylesheet">
		<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
		<script src="<?php echo base_url('assets/site/js/zoomsl-3.0.min.js')?>"></script>
		
		<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
		<link href="<?php echo base_url('css/site/style.css') ?>" rel="stylesheet">
		<!-- ====================Slide show+++++++++++++++ -->
		<script src="<?php echo base_url('assets/site/js/slideshow/jssor.js')?>"></script>
		<script src="<?php echo base_url('assets/site/js/slideshow/jssor.slider.js')?>"></script>
		<!-- ++++++++++++++++++++++ -->
		<!-- ====================scroll show+++++++++++++++ -->
		
		

		<!-- ====================   menu  +++++++++++++++ -->
		<script src="<?php echo base_url('assets/site/js/menu/script.js')?>"></script>
		<link href="<?php echo base_url('assets/site/js/menu/styles.css') ?>" rel="stylesheet">
		<!-- ++++++++++++++++++++++ -->
		<script src="<?php echo base_url('assets/site/product_scroll/owl.carousel.js')?>"></script>
		<script src="<?php //echo base_url('assets/site/js/scroll/jquery.easing.min.js')?>"></script>
		<!-- ++++++++++++++++++++++ -->
		<!-- ++++++++++++++++++++++ -->
		<script src="<?php echo base_url('assets/site/js/scroll/jquery.easy-ticker.js')?>"></script>
		<script src="<?php echo base_url('assets/site/js/scroll/jquery.easing.min.js')?>"></script>
		<!-- ++++++++++++++++++++++ -->
		<script type="text/javascript">
			
            jQuery(document).ready(function ($) {

		            var _SlideshowTransitions = [
		            //Fade
		            { $Duration: 1200, $Opacity: 2 }
		            ];

		            var options = {
		                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
		                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
		                $AutoPlayInterval: 3000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
		                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

		                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
		                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
		                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
		                $SlideWidth: 960,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
		                //$SlideHeight: 380,
		                $SlideSpacing: 3,					                //[Optional] Space between each slide in pixels, default value is 0
		                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
		                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
		                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
		                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
		                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

		                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
		                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
		                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
		                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
		                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
		                },

		                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
		                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
		                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
		                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
		                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
		                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
		                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
		                    $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
		                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
		                },

		                $ArrowNavigatorOptions: {
		                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
		                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
		                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
		                }
		            };
		            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

		            //responsive code begin
		            //you can remove responsive code if you don't want the slider scales while window resizes
		            function ScaleSlider() {
		                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
		                if (parentWidth)
		                    jssor_slider1.$ScaleWidth(Math.min(parentWidth, 1080));
		                else
		                    window.setTimeout(ScaleSlider, 30);
		            }
		            ScaleSlider();

		            $(window).bind("load", ScaleSlider);
		            $(window).bind("resize", ScaleSlider);
		            $(window).bind("orientationchange", ScaleSlider);
		            //responsive code end
		        });
            function getmodels(event){
            	var url="<?php echo site_url('site/getmodels')?>";
            	var make_id=$('#make_id').val();
            	$.ajax({
		            url:url,
		            type:"POST",
		            datatype:"Json",
		            async:false,
		            data:{
		            		'make_id':make_id,
		            	},
		            success:function(data) {
		              $("#model_id").html(data);
		             
		            }
		          })
            }
            $(function(){
				$('.btnsearch').click(function(){
					var part_name=$('#s_partname').val();
		          	var make=$('#make_id').val();
		          	var model=$('#model_id').val();
		          	var s_year=$('#s_year').val();
		          	var t_year=$('#t_year').val();
		          	var s_price=$('#s_price').val();
		          	var t_price=$('#t_price').val();
					location.href="<?php echo site_url('site/search');?>?n="+part_name+"&m="+make+"&mo="+model+"&sy="+s_year+"&ty="+t_year+"&sp="+s_price+"&tp="+t_price;
				})
				$(document).on('click', '.navigat', function(){
					var page = $(this).attr("id");
					var cur=$(this).closest('.p_wrap');
					// alert(cur.attr('id')=='new');
					if(cur.attr('id')=='new')
						getnew(page);
					else
						getdata(page);			
				});	
				$('.cssmenu').attr('style','');
			});
			
		</script>
	</head>
	<body>
		<div id="header_wrap">
			<div id="header-inner">
				<div class='col-sm-3' id='logo'>

					<img id='imglogo' src="<?php echo base_url('assets/site/image/logo.png') ?>">

				</div>
				<div class="col-sm-9 " id='top' style='text-align:right;'>
					<div id="topbar">
						<a href="#">Login</a>
						<a href="#">Sign up</a>
					</div>
						<!-- <img id='topimg' src="<?php echo base_url('assets/site/image/top.png') ?>" style='width:80%;'> -->
					<div id="searchbox">
						<select id="searchmodel">
							<option value="">Category</option>
						</select>
						<input type="text" id="searchinp" name="search" placeholder='Search'>
						<input type="button" id="btnsearch" value="Search">
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
			
			<div id='menu'>
				<div id="menu-inner">
					<div id='cssmenu'>
					   <ul>
					      <li class=""><a href='<?php echo site_url() ?>'>HOME</a></li>
					      <li><a href="<?php echo site_url('site/product') ?>">PRODUCTS</a>
					      <!-- <li class='has-sub'><a href='#'>PRODUCTS</a> -->
					         <!-- <ul>
					            <li class='has-sub'><a href='#'>Product 1</a>
					               <ul>
					                  <li><a href='#'>Sub Product</a></li>
					                  <li><a href='#'>Sub Product</a></li>
					               </ul>
					            </li>
					            <li class='has-sub'><a href='#'>Product 2</a>
					               <ul>
					                  <li><a href='#'>Sub Product</a></li>
					                  <li><a href='#'>Sub Product</a></li>
					               </ul>
					            </li>
					         </ul> -->
					      </li>
					      <?php
					      	foreach ($this->sit->getpage() as $p) {
					      		echo "<li><a href='".site_url('site/page/').'/'.$p->pageid."'/>$p->page_name</a></li>";
					      	}
					       ?>
					      
					   </ul>
					</div>
				</div>
					
					<!-- <img id='imgmenu' src="<?php echo base_url('assets/site/image/menu.png') ?>"> -->
			</div>
		</div>
		<div style="width:1000px; margin:0px auto;">
			<!-- <div class='col-sm-1'></div> -->
			<div class='body col-sm-12' >
				
				
				<div class='col-sm-12 ' id='slide'>
				                <!-- #camera_wrap_1 -->
				                    <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 960px; height: 383.9px; overflow: hidden;">

									        <!-- Loading Screen -->
									        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
									            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
									                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
									            </div>
									            <div style="position: absolute; display: block; background: url(assets/images/loading.gif) no-repeat center center;
									                top: 0px; left: 0px;width: 100%;height:100%;">
									            </div>
									        </div>

									        <!-- Slides Container -->
									        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 960px; height: 383.9px; overflow: hidden;">
									           
									            <?php 
									                foreach ($this->sit->getslide('main') as $row) {
									                    echo '<div><img src="'.base_url("assets/upload/slide/$row->slide_id.jpg").'" style="width:99%;"></div>';
									                }
									            ?>
									        </div>
									        <style>
									            /* jssor slider bullet navigator skin 17 css */
									            /*
									            .jssorb17 div           (normal)
									            .jssorb17 div:hover     (normal mouseover)
									            .jssorb17 .av           (active)
									            .jssorb17 .av:hover     (active mouseover)
									            .jssorb17 .dn           (mousedown)
									            */
									            .jssorb17 div, .jssorb17 div:hover, .jssorb17 .av
									            {
									                background: url(<?php echo site_url('../assets/site/image/nav.png') ?>) no-repeat;
									                overflow:hidden;
									                cursor: pointer;
									            }
									            .slide_img{width: 100% !important;}
									            .jssorb17 div { background-position: -7px -7px; }
									            .jssorb17 div:hover, .jssorb17 .av:hover { background-position: -37px -7px; }
									            .jssorb17 .av { background-position: -67px -7px; }
									            .jssorb17 .dn, .jssorb17 .dn:hover { background-position: -97px -7px; }
									        </style>
									        <!-- bullet navigator container -->
									        <div u="navigator" class="jssorb17" style="position: absolute; bottom: 16px; right: 6px;">
									            <!-- bullet navigator item prototype -->
									            <div u="prototype" style="POSITION: absolute; WIDTH: 16px; HEIGHT: 16px;"></div>
									        </div>
									        <!-- Arrow Navigator Skin Begin -->
									        <style>
									            /* jssor slider arrow navigator skin 05 css */
									            /*
									            .jssora05l              (normal)
									            .jssora05r              (normal)
									            .jssora05l:hover        (normal mouseover)
									            .jssora05r:hover        (normal mouseover)
									            .jssora05ldn            (mousedown)
									            .jssora05rdn            (mousedown)
									            */
									            .jssora05l, .jssora05r, .jssora05ldn, .jssora05rdn
									            {
									                position: absolute;
									                cursor: pointer;
									                display: block;
									                background: url(assets/images/arrow.png) no-repeat;
									                overflow:hidden;
									            }
									            .jssora05l { background-position: -10px -40px; }
									            .jssora05r { background-position: -70px -40px; }
									            .jssora05l:hover { background-position: -130px -40px; }
									            .jssora05r:hover { background-position: -190px -40px; }
									            .jssora05ldn { background-position: -250px -40px; }
									            .jssora05rdn { background-position: -310px -40px; }
									        </style>
									        <!-- Arrow Left -->
									        <span u="arrowleft" class="jssora05l" style="width: 40px; height: 40px; top: 123px; left: 8px;">
									        </span>
									        <!-- Arrow Right -->
									        <span u="arrowright" class="jssora05r" style="width: 40px; height: 40px; top: 123px; right: 8px">
									        </span>
									        <!-- Arrow Navigator Skin End -->
									    </div>
				                    <!-- end #camera_wrap_1 -->
				</div>
				<div class='col-sm-3 ' id='leftside'>
				<!-- =========================search form=================== -->
					<div class="search_blog col-sm-12">
						<label class="col-sm-12 s_title">Search PART </label>
						<label class="col-sm-12">Part Name</label>
						<div class="col-sm-12">
							<input type='text' id='s_partname' value='<?php echo $part_name ?>' class='form-control'/>
							<input type='text' id='s' class='form-control hide' value='<?php echo $s ?>'/>
						</div>
						<label class="col-sm-12">Make : </label>
						<div class="col-sm-12">
							<select class='form-control' id='make_id' onchange='getmodels(event);'>
								<option value="">Any</option>
								<?php
									foreach ($this->sit->getmake() as $mr) {
										$se='';
										if($make==$mr->make_id)
											$se='selected';
										echo "<option value='$mr->make_id' $se>$mr->make_name</option>";
									}
								 ?>
							</select>
						</div>
						<label class="col-sm-12">Model : </label>
						<div class="col-sm-12">
							<select class='form-control' id='model_id'>
								<option value="">Any</option>
								<?php
									if($make_id!='')
										foreach ($this->sit->getmodel($make_id) as $mk) {
											$se='';
											if($model_id==$mk->model_id)
												$se='selected';
											echo "<option value='$m->model_id' $se>$m->model_name</option>";
										}
								 ?>
							</select>
						</div>
						<label class="col-sm-12">Year : </label>
						<div class="col-sm-12">
							<div class="col-sm-6" style='padding-left:0px;'>
								<input type='text' id='s_year' value='<?php echo $s_year ?>'  placeholder="From" class='form-control'/>
							</div>
							<div class="col-sm-6" style='padding-right:0px;'>
								<input type='text' id='t_year' value='<?php echo $t_year ?>' placeholder="To" class='form-control'/>
							</div>
							
						</div>
						<label class="col-sm-12">Price : </label>
						<div class="col-sm-12">
							<div class="col-sm-6" style='padding-left:0px;'>
								<input type='text' id='s_price'  placeholder="From" value='<?php echo $s_price ?>' class='form-control'/>
							</div>
							<div class="col-sm-6" style='padding-right:0px;'>
								<input type='text' id='t_price' placeholder="To" value='<?php echo $t_price ?>' class='form-control'/>
							</div>
						</div>
						
						<div class='col-sm-12' style='margin-top: 15px;'>
							<div class="col-sm-6"></div>
							<div class="col-sm-6" style="padding-right:0px;">
								<div class="btnsearch">SEARCH</div>
							</div>
						</div>
					</div>
					<!-- ++++++++++++++++++++++++++++++++++++++++++++ -->
					<!-- =========================Login form=================== -->
					<div class="search_blog col-sm-12" style='margin-top:15px;'>
						<label class="col-sm-12 s_title" style='text-align:center;'>LOGIN FORM  </label>
						<label class="col-sm-12">Username : </label>
						<div class="col-sm-12">
							<input type='text' class='form-control'/>
						</div>
						<label class="col-sm-12">Password : </label>
						<div class="col-sm-12">
							<input type='text' class='form-control'/>
						</div>
						
						<div class='col-sm-12' style='margin-top: 15px;'>
							<div class="col-sm-5" style="padding-left:0px;">
								<div class="btnlogin">LOG IN</div>
							</div>
							<div class="col-sm-7" style="padding-left:0px; padding-right:0px; padding-top:8px;">
								<label><input type="checkbox" /> Remember Me</label>
							</div>
						</div>
						<div class="col-sm-12" style='padding-top:15px; color:#CCCCCC !important;'>
							<label><a href='#'>Forgot your password ?</a></label>
						</div>
						<div class="col-sm-12">
							<label style='font-style:italice; color:red;'><a href='#' style='color:red;'>Create an Account </a></label>
						</div>
					</div>
					<!-- ++++++++++++++++++++++++++++++++++++++++++++ -->
					<!-- <img id='imgleft' src="<?php echo base_url('assets/site/image/left.png') ?>"> -->
				</div>
				<div class="col-sm-6 " id='content'>
					
				
				