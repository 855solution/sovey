<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	.list_thumbimg li{display: inline !important;}
	.list_thumbimg {padding-left:0 !important; margin-top: 10px;}
	.list_thumbimg li img{width:17% !important;}
	.list_thumbimg li img:hover{cursor: pointer;}
	.cur{border:2px solid #009E23 !important;}
	#top-bar img{width: 20px; margin-top: 5px;}
	
</style>
<div class="col-sm-9 p_wrap" id='featured' style='border-bottom:1px solid #CCCCCC' >
	<div class="my_title col-sm-12">
		<span class=' col-sm-6 p_title'>Items Detail</span>
		
	</div>
	<p style='clear:both'></p>
	<div>
		<table class='table table-bordered'>
			<tr>
				<td>Part Name</td>
				<td><?php echo $row->name ?></td>
			</tr>
			<tr>
				<td>Make</td>
				<td><?php echo $row->make_name ?></td>
			</tr>
			<tr>
				<td>Model</td>
				<td><?php echo $row->model_name ?></td>
			</tr>
			<tr>
				<td>Year</td>
				<td><?php echo $row->year ?></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><?php echo $row->description ?></td>
			</tr>
		</table>
		<div style='text-align:center; width:100%'>
			<?php 
				$bigimg=base_url('assets/site/image/no_img.png');
				$imgdata=$this->sit->getarrimage($row->item_id);
				$imgdata=$imgdata['img'];
				if(count($imgdata)>0)
					if(file_exists(FCPATH."/uploads/".$imgdata[0]))
						$bigimg=site_url("../uploads/".$imgdata[0]);
			?>
			<!-- <div class='col-sm-12'>
				<img src="<?php echo $bigimg ?>" class='my-foto' data-large="<?php echo $bigimg ?>" id="bigpreview" style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
			</div> -->
			<p></p>
			<!-- <div class='col-sm-12' style="text-align:left"> -->
				
					<?php 
						
						for ($i=0; $i < count($imgdata); $i++) {
							$class='';
							if($i==0)
								$class='cur'; 
							if(file_exists(FCPATH."/uploads/thumb/".$imgdata[$i]))
								echo "
										<img src='".site_url("../uploads/".$imgdata[$i])."'  style=' width:100%; margin-bottom:5px; padding:1px;'>
									";
						}
					?>
				<!-- </ul> -->
			<!-- </div> -->
			
		</div>
	</div>
</div>

<script>
	
		$(function(){
			 $(".my-foto").imagezoomsl({
			  	descarea: ".big-caption", 				

				zoomrange: [1.68, 20],

				zoomstart: 1.68,

				cursorshadeborder: "10px solid black",

				magnifiereffectanimate: "fadeIn",
			  });
		});

</script>