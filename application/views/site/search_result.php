<div class="col-sm-9">
	<div class="tempo-module-header align-left">
		<h5 class="tempo-module-heading blue-text"><?php echo $title; ?></h5>
		
	</div>
	<ul class="listproduct">
		<?php
			$i=1;
			// print_r($data);
			foreach ($data as $f_p) {
				$p_name=$f_p->name;
				if (strlen($f_p->name) > 15)
				   // $p_name = substr($f_p->name, 0, 15) . '...';

				$img='';
				$img=$this->sit->getdefimg($f_p->item_id);
				if($img=='')
					$img='no_img';
				$img_path=base_url('assets/site/image/no_img.png');
				if(file_exists(FCPATH."/uploads/thumb/$img"))
	                $img_path=base_url("/uploads/thumb/$img");
					
					echo "<li class='col-sm-12'>
							<div class='carimg'>
								<a href='".site_url('site/detail').'/'.$f_p->item_id."'>
									<img  style='width:160px;' class='p_img' src='".$img_path."'>
								</a>
							</div>
							<div class='listdetail'>
								<a href='".site_url('site/detail').'/'.$f_p->item_id."'>
									".$f_p->name."
								</a>
								<ul class='listfeat'>
									<li><label>Make : </label> $f_p->make_name</li>
									<li><label>Model :</label> $f_p->model_name </li>
									<li><label>Color :</label> $f_p->color_name</li>
									<li><label>Year :</label> $f_p->year</li>
								</ul>
							</div>
							
						</li>";
				$i++;
			}
		 ?>
	</ul>
	<div style="clear:both"></div>

	<ul id="pagination">
		<li style="float:left">
			<select class="p_num">
				<?php
				// $i=10;
				for ($i=10; $i <=200 ; $i+=10) { 
					$sel='';
					if(isset($_GET['p_num']) && $_GET['p_num']==$i)
						$sel='selected';
					echo "<option $sel>$i</option>";
				}
				 ?>
			</select>
		</li>
		<?php echo $this->pagination->create_links(); ?>
		
	</ul>
</div>
