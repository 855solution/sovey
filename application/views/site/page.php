<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	.list_thumbimg li{display: inline !important;}
	.list_thumbimg {padding-left:0 !important; margin-top: 10px;}
	.list_thumbimg li img{width:17% !important;}
	.list_thumbimg li img:hover{cursor: pointer;}
	.cur{border:2px solid #009E23 !important;}
	#top-bar img{width: 20px; margin-top: 5px;}
	
</style>
<div class="col-sm-12 p_wrap" id='featured' style='border-bottom:1px solid #CCCCCC' >
	<div class="my_title col-sm-12">
		<span class=' col-sm-6 p_title'><?php echo $page->page_name ?></span>
		
	</div>
	<p style='clear:both'></p>
	<div>
		<?php echo $page->article ?>
	</div>
</div>

<script>
	function preview(event){
			// aler('ok');
			$('.list_thumbimg li img').removeClass('cur');
			$(event.target).addClass('cur');
			var img=$(event.target).attr('rel');
			$('#bigpreview').attr('src',"<?php echo base_url('/uploads/"+img+"')?>");
			$('#bigpreview').attr('data-large',"<?php echo base_url('/uploads/"+img+"')?>");
			
		}
		$(function(){
			 $(".my-foto").imagezoomsl({
			  	descarea: ".big-caption", 				

				zoomrange: [1.68, 20],

				zoomstart: 1.68,

				cursorshadeborder: "10px solid black",

				magnifiereffectanimate: "fadeIn",
			  });
		});

</script>