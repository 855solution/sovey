<script type="text/javascript">
$(function(){

	$('.carousel-category-curated').owlCarousel({
		// navigation:true,
	    items : 5, //10 items above 1000px browser width
	    itemsDesktop :false, //5 items between 1000px and 901px
	    itemsDesktopSmall : false, // 3 items betweem 900px and 601px
	    itemsTablet: false, //2 items between 600 and 0;
	    itemsMobile : false
	})
	
    $('.carousel-category-curated').hover(function() {
        $(this).parent().find('.customNavigation .btn').show(); 
    }, function() {
        $(this).parent().find('.customNavigation .btn').hide();
    });
    $('.customNavigation .btn').hover(function() {
        $(this).show(); 
    }, function() {
        $(this).hide();
    });
})
function nextbest(){
    $('.carousel-category-curated').trigger('owl.next');
}
function prevbest(){
    $('.carousel-category-curated').trigger('owl.prev');
}
	
<?php
 $is_sp=0;
 if($this->session->userdata("u_inf")){
 	$is_sp=$this->db->query("SELECT * FROM ospos_member where member_id='".$this->session->userdata("u_inf")->member_id."'")->row()->is_sp;
 }
 ?>
</script>
<div class="zone js-product-image-zone">
	<div class="standard-carousel best-sell-products">
		<div class="tempo-module-header align-left">
			<h5 class="tempo-module-heading blue-text" style="float:left">Newly Products</h5>
			<div class="viewall"><a href="<?php echo site_url('product?ne=1') ?>"> View All</a></div>
			<div style="clear:both"></div>
		</div>
		<div class="carousel carousel-category-curated">
		    <?php 
		    	$feat=$this->db->query("SELECT * FROM ospos_items where deleted=0 and is_new='1'")->result();
			    foreach($feat as $row){
						$img=$this->sit->getdefimg($row->item_id);
						$p_name=$row->name;
						if (strlen($row->name) > 15)
						   $p_name = substr($row->name, 0, 15) . '...';
						// echo $img;
						// $img_path=base_url('assets/images/no_img.png');
						$img_path=base_url('assets/site/image/no_img.png');
						if($img!='')
							if(file_exists(FCPATH."/uploads/thumb/$img"))
				                $img_path=base_url("/uploads/thumb/$img");
			            $url=site_url('products/detail/'.$row->item_id);
			            $price='';
			            if($is_sp==1){
			            	$price='$ '.$row->unit_price;
			            }
						echo "<div class='owl-item'>
									<div class='tile'>
										<div class='promo' style='background:url(".site_url('../assets/css/site/site2/images/new_icon.gif').")'></div>

										<a rel='$row->item_id' href='".site_url('site/detail').'/'.$row->item_id."' title='$row->name' class='v_detail tile-section'>
											<img src='".$img_path."' style='width:100%'>
										</a>
											<span class='price'>$price</span>

										<a rel='$row->item_id' href='".site_url('site/detail').'/'.$row->item_id."' title='$p_name' class='v_detail tile-section category-heading'>
											<span class='discription'>".$p_name."</span>
											
										</a>
									</div>
							 </div>";	

											
												
					}
			?>
			
		</div>
		<div class="customNavigation">
				<a onclick="prevbest();" class="btn prev">Previous</a>
				<a onclick="nextbest();" class="btn next">Next</a>
			</div>
		<div style='clear:both'></div>
		
	</div>
</div>
<div style='clear:both'></div>


 