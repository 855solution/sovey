<?php 

// print_r($this->session->userdata('u_inf'));
 ?>
 <style type="text/css">
 a:hover{
 	cursor: pointer;
 }
 .bg-danger{
 	background-color: #F2DEDE !important;
 	padding: 20px;
 	width: 100%;
 }
 .bg-success{
 	background-color: #DFF0D8 !important;
 	padding: 20px;
 	width: 100%;
 }
 </style>
 	<?php
 		if (isset($mail)) {
 			echo '<p class="bg-success">Please Check your Email to reset your password.</p>';
 		}
 		if(isset($_GET['log']))
 			echo '<p class="bg-danger">Your username or password incorrect.</p>';
 		if(isset($_GET['reg']) && $_GET['reg']=='error')
 			echo '<p class="bg-danger">Your email has register already please choos other email.</p>';
 		if(isset($_GET['succ']))
 			echo '<p class="bg-success">Your password has reset.</p>';

 	 ?>

	<!-- <p class="bg-danger">Error</p> -->

<div class="col-sm-6">
	<div class="tempo-module-header align-left">
		<h5 class="tempo-module-heading blue-text">Login</h5>
	</div>
	<p></p>
	<form method="post" action="<?php echo site_url('site/chklogin'); ?>">
		<label class="col-sm-12 control-label">Email</label>
		<div class="col-sm-12">
			<input type="email" name="log_email" required class="form-control" placeholder='example@mail.com'>
		</div>
		<label class="col-sm-12 control-label">Password</label>
		<div class="col-sm-12">
			<input type="password" name="log_pwd" required class="form-control" placeholder='Password'>
		</div>
		<div class="col-sm-12">
			<label><a class="forgot">Forgot Password ?</a></label>
		</div>

		<div class="col-sm-12" style="margin-top:15px;">
			<input type="submit" class="btn btn-primary pull-right" value="Sign In">
		</div>
	</form>
		
</div>
<div class="col-sm-6">
	<div class="tempo-module-header align-left">
		<h5 class="tempo-module-heading blue-text">Register</h5>
	</div>
	<p></p>
	<form method="post" action="<?php echo site_url('site/register'); ?>">

		<label class="col-sm-12 control-label">Last Name</label>
		<div class="col-sm-12">
			<input type="text" required name="last_name" class="form-control" placeholder='Last name'>
		</div>
		<label class="col-sm-12 control-label">First Name</label>
		<div class="col-sm-12">
			<input type="text" required name="first_name" class="form-control" placeholder='First Name'>
		</div>
		<label class="col-sm-12 control-label">Gender</label>
		<div class="col-sm-12">
			<select class="form-control" name="gender">
				<option>Male</option>
				<option>FeMale</option>
			</select>
		</div>
		<label class="col-sm-12 control-label">Email</label>
		<div class="col-sm-12">
			<div class="input-group">
	        	<span class="input-group-addon">@</span>
				<input type="email" name="email" required class="form-control" placeholder='example@mail.com'>
			</div>
		</div>
		<label class="col-sm-12 control-label">Address</label>
		<div class="col-sm-12">
			<textarea class="form-control" name="address"></textarea>
		</div>
		<label class="col-sm-12  control-label">Password</label>
		<div class="col-sm-12">
			<input type="password" id="password" name="password" required class="form-control" placeholder='Password'>
		</div>
		<label class="col-sm-12 control-label">Re-Password</label>
		<div class="col-sm-12">
			<input type="password" id="confirm_password" required class="form-control" placeholder='Re-Password'>
		</div>
		<div class="col-sm-12" style="margin-top:15px;">
			<input type="submit" class="btn btn-primary pull-right" value="Submit">
		</div>
	</form>
</div>
<div class="modal fade bs-example-modal-lg" id="resetpwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog modal-lg">
				        <div class="modal-content">
				            <div class="wrapper">
								<div class="clearfix" id="main_content_outer">
								    <div id="main_content">
									    <div class="result_info">
									    	<div class="col-sm-6">
									      		<!-- <strongChoose Column To Export</strong> -->
									      	</div>
									      	<div class="col-sm-6" style="text-align: center">
									      		<strong>
									      			<center class='visit_error' style='color:red;'></center>
									      		</strong>	
									      	</div>
									    </div>
									      	<form enctype="multipart/form-data" name="frmpwd" id="frmpwd" method="POST">
										        <div class="row">
													<div class="col-sm-12">
											            	<div class="panel-body">
											            		<label class="col-sm-12 control-label">Your Email</label>
																<div class="col-sm-12">
																	<div class="input-group">
															        	<span class="input-group-addon">@</span>
																		<input type="email" name="reset_email" id="reset_email" required class="form-control" placeholder='example@mail.com'>
																	</div>
																</div>

												            </div>
												    </div> 
												</div>
									      </form>
									</div> 
							    </div>
							</div> 
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				                <!-- <button type="button" id='btnprint' class="btn btn-primary">Print</button> -->
				                <button type="button" id='btnresetepwd' class="btn btn-primary">Save</button>
				            </div>
				        </div>                       <!-- /.modal-content -->
				    </div>
				                                <!-- /.modal-dialog -->
				</div>
<script>
	$(function(){
		$(".forgot").click(function(){
			// alert("OK");
			$("#resetpwd").modal('show');
		})
		$("#btnresetepwd").click(function(){
				var url="<?php echo site_url('site/resetemail')?>";
            	var email=$('#reset_email').val();
            	$.ajax({
		            url:url,
		            type:"POST",
		            datatype:"Json",
		            async:false,
		            data:{
		            		'email':email,
		            	},
		            success:function(data) {
		              	if(data>0){
		              		sendmail(email);// location.href="<?php echo site_url('site/sendmail') ?>";
		              	}else{
		              		alert("Your email not found.");
		              	}
		            }
		          })
		})
	})
	function sendmail(email){
		var url="<?php echo site_url('site/sendmail')?>";
    	// var email=$('#reset_email').val();
    	$.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{
            		'email':email,
            	},
            success:function(data) {
              	if(data=='success'){
		            location.href="<?php echo site_url('site/mailsuccess') ?>";

              	}
            }
          })
	}
	var password = document.getElementById("password")
	  , confirm_password = document.getElementById("confirm_password");

	function validatePassword(){
	  if(password.value != confirm_password.value) {
	    confirm_password.setCustomValidity("Passwords Don't Match");
	  } else {
	    confirm_password.setCustomValidity('');
	  }
	}

	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
</script>