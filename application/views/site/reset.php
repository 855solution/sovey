<?php 

// print_r($this->session->userdata('u_inf'));
 ?>
 <style type="text/css">
 a:hover{
 	cursor: pointer;
 }
 .bg-danger{
 	background-color: #F2DEDE !important;
 	padding: 20px;
 	width: 100%;
 }
 .bg-success{
 	background-color: #DFF0D8 !important;
 	padding: 20px;
 	width: 100%;
 }
 </style>
 	<?php
 		if (isset($mail)) {
 			echo '<p class="bg-success">Please Check your Email to reset your password.</p>';
 		}
 		if(isset($_GET['log']))
 			echo '<p class="bg-danger">Your username or password incorrect.</p>';
 		if(isset($_GET['reg']) && $_GET['reg']=='error')
 			echo '<p class="bg-danger">Your email has register already please choos other email.</p>';

 	 ?>

	<!-- <p class="bg-danger">Error</p> -->

<div class="col-sm-6">
	<div class="tempo-module-header align-left">
		<h5 class="tempo-module-heading blue-text">Reset password</h5>
	</div>
	<p></p>
	<form method="post" id="frmreset" action="<?php echo site_url('site/resetpwd?m='.$_GET['m']); ?>">
		<label class="col-sm-12 control-label">New Password</label>
		<div class="col-sm-12">
			<input type="password" name="password" id="rpassword" required class="form-control" placeholder='New Password'>
		</div>
		<label class="col-sm-12 control-label">Confirm Password</label>
		<div class="col-sm-12">
			<input type="password" name="confirm_password" id="rconfirm_password" required class="form-control" placeholder='Confirm Password'>
		</div>
		
		<div class="col-sm-12" style="margin-top:15px;">
			<input type="buttom" id="changepwd" class="btn btn-primary pull-right" value="Change">
		</div>
	</form>
		
</div>


<script>
	$(function(){
		$("#changepwd").click(function(){
			var password=$("#rpassword").val();
			var cpassword=$("#rconfirm_password").val();
			if(password==cpassword){
				$("#frmreset").submit();
			}else{
				alert('Your Password is not match');
			}

		})
	})
	
</script>