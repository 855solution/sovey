<script type="text/javascript">
$(function(){

	$('.carousel-category-pro').owlCarousel({
		// navigation:true,
	    items : 5, //10 items above 1000px browser width
	    itemsDesktop :false, //5 items between 1000px and 901px
	    itemsDesktopSmall : false, // 3 items betweem 900px and 601px
	    itemsTablet: false, //2 items between 600 and 0;
	    itemsMobile : false
	})
	
    $('.carousel-category-pro').hover(function() {
        $(this).parent().find('.customNavigation .btn').show(); 
    }, function() {
        $(this).parent().find('.customNavigation .btn').hide();
    });
    $('.customNavigation .btn').hover(function() {
        $(this).show(); 
    }, function() {
        $(this).hide();
    });
})
function nextpro(){
    $('.carousel-category-pro').trigger('owl.next');
}
function prevpro(){
    $('.carousel-category-pro').trigger('owl.prev');
}
	

</script>
<div class="zone js-product-image-zone">
	<div class="standard-carousel best-sell-products">
		<div class="tempo-module-header align-left">
			<h5 class="tempo-module-heading blue-text" style="float:left">Promotion Products</h5>
			<div class="viewall"><a href="<?php echo site_url('product?pr=1') ?>"> View All</a></div>
			<div style="clear:both"></div>
		</div>
		<div class="carousel carousel-category-pro">
		    <?php 
		    	$pro=$this->db->query("SELECT * FROM ospos_items where deleted=0 and is_pro='1'")->result();
			    foreach($pro as $row){
						$img=$this->sit->getdefimg($row->item_id);
						$p_name=$row->name;
						if (strlen($row->name) > 15)
						   $p_name = substr($row->name, 0, 15) . '...';
						// echo $img;
						// $img_path=base_url('assets/images/no_img.png');
						$img_path=base_url('assets/site/image/no_img.png');
						if($img!='')
							if(file_exists(FCPATH."/uploads/thumb/$img"))
				                $img_path=base_url("/uploads/thumb/$img");
			            $url=site_url('products/detail/'.$row->item_id);
			            $price=$row->unit_price;
						echo "<div class='owl-item'>
									<div class='tile'>
										<div class='promo' style='background:url(".site_url('../assets/css/site/site2/images/discount_icon.gif').")'></div>

										<a rel='$row->item_id' href='".site_url('site/detail').'/'.$row->item_id."' title='$row->name' class='v_detail tile-section'>
											<img src='".$img_path."' style='width:100%'>
										</a>
											<span class='price'>$ $price</span>

										<a rel='$row->item_id' href='".site_url('site/detail').'/'.$row->item_id."' title='$p_name' class='v_detail tile-section category-heading'>
											<span class='discription'>".$p_name."</span>
											
										</a>
									</div>
							 </div>";	

											
												
					}
			?>
			
		</div>
		<div class="customNavigation">
			<a onclick="prevpro();" class="btn prev">Previous</a>
			<a onclick="nextpro();" class="btn next">Next</a>
		</div>
		<div style='clear:both'></div>
		
	</div>
</div>
<div style='clear:both'></div>


 