<link rel="stylesheet" type="text/css" href="<?=base_url().'css/tables.css'?>">
<link rel="stylesheet" type="text/css" href="<?=base_url().'css/register.css'?>">
<!-- <script src="http://localhost/855_project/sovey/js/manage_tables.js" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
<style type="text/css">

	#ispending td{

		background:rgba(255, 0, 0, 0.14);

	}

	#ispending:hover{

		background:#e9e9e9;

	}

	#spa span{

		/*padding: 0 1% !important;*/

	}

	#pagination{

		padding: 1% 0;

		float: right;

	}

	#pagination a{

		border: 1px solid #21759B;

		background: #FFF none repeat scroll 0% 0%;

		color: #21759B;

		border-radius: 5px;

		padding: 5px 10px;

	}

	#pagination .ui-state-disabled{

		border: 1px solid #21759B;

		background: #21759B none repeat scroll 0% 0%;

		color: white;

		border-radius: 5px;

		padding: 5px 10px;

		cursor:not-allowed;

	}

	#pagination span{

		border: 1px solid #21759B;

		background: #FFF none repeat scroll 0% 0%;

		color: #21759B;

		border-radius: 5px;

		padding: 5px 10px;

		cursor:not-allowed;



	}

	#pagination .dot{

		border: 1px solid #21759B;

		background: #FFF none repeat scroll 0% 0%;

		color: #21759B;

		border-radius: 5px;

		padding: 5px 10px;

	}

	#table_holder{

		width: 100%;

		float: right;

		margin-top: 0;

	}

	#table_action_header{

		width:86%;

		float: right;

	}

	.barcode_date{

		display: none;

		position: absolute;

		width: 190px;

		height: 110px;

		background: white;

		/*border: 1px solid;*/

		box-shadow: 0px 0px 5px;

		text-align: center;

		padding: 10px;

		border-radius: 5px;

		z-index: 99999;

	}

	.barcode_date_active{

		display: block;

		position: absolute;

		width: 200px;

		height: auto;

		background: white;

		/*border: 1px solid;*/

		box-shadow: 0px 0px 5px;

		text-align: center;

		padding: 10px;

		border-radius: 5px;

		z-index: 99999;

	}

	.hide{

		display: none;

	}

	.page_result b{

		color: red !important;

	}

	.v_photo{

		margin-top: -23px;

		width: 120px;

		text-align: center;

		position: absolute;

		background: rgba(0, 0, 0, 0.21) none repeat scroll 0% 0%;

	}
	.v_photo span{
		color: white;
	}

	.v_photo a{

		color: white;

	}
	.search_box{
		width: 25%;
		margin-left: 13%;
		padding: 7px 0;
	}
	.item_sort{
		cursor: pointer;
	}
	.sort_a,.sort_d{
	    background: #cecece !important;
	}
	.filters_applied span{
	    margin-left: 5px;
	    padding: 10px;
	    background: #f1f1f1;
	    color: black;
	}
	.filters_applied span:hover{
	    background: #d4d4d4;
	    cursor: pointer;
	}
	.tablesorter th{
		white-space: nowrap;
	}
	.er_box{
	    background: #ffd9d9;
	    padding: 1%;
	    border: 1px solid #ffb8b8;
	    border-radius: 4px;
	}
	.er_box p{
		color:red;
	}
	/*table css*/
    table.tablesorter thead tr th, table.tablesorter tfoot tr th{
    	color: #21759B;
	    text-align: left;
	    background-color: #eeeeee;
	    padding: 11px 0 9px 14px;
	    border-bottom: solid 1px #ccc;
	    height: auto;
	    font-size: 11px;
	}
	td{font-size: 11px;font-family: "Khmer OS";}

	.tablesorter th {
	    white-space: nowrap;
	}
	* {
	    padding: 0;
	    margin: 0;
	}
	th {
	    font-weight: bold;
	    text-align: -internal-center;
	}
	td, th {
	    display: table-cell;
	    vertical-align: inherit;
	}
	.search_box {
    width: 25%;
    margin-left: 0%;
    padding: 7px 0;
    margin-top: -1%;}

    #table_action_header {
    width: 100%;
    float: right;
    background-image: unset;
    height: 5%;
	}

	/*table css*/
</style>
<div class="search_box">
	<input type="text" class="search_all" name ='search' id='search' style="width:100%;" placeholder="Search all items" onkeypress="return event.keyCode!=13" value="" />
	
</div>
<div id="table_action_header">

	<ul>		

		<li class="float_right" style="margin-top:-3px;">
			<input type="text" name="isearch" id="isearch" class="form-control" placeholder="Search this list">
		</li>

		</form>

	</ul>

</div>
<div class="col-sm-12">
	<!-- TABLE HOLDER -->

	<div id="table_holder">

		<!-- TOP PAGINATION -->


		
		<div style="float:left;width:100%;height:25px;" id="page_result" class="page_result">

			<?php 

					$val=50;

					if (isset($_GET['page'])) {

						$val = $_GET['page'];

					}

					if (isset($_GET['per_page'])) {

					 	$per = $_GET['per_page'];

					 } 

			?>

			<p>Showing 0 to 0 of 0 results</p>

		</div>

		<div style="float:left;width:50%; margin-top: 20px;">

			Show

			<select id="per_page_top" class="per_page" name="per_page">

				<option value="all">All</option>

				<?php 

					$val=50;

					if (isset($_GET['page'])) {

						$val = $_GET['page'];

					}

					if (isset($_GET['per_page'])) {

					 	$per = $_GET['per_page'];

					 } 

					 for($i=10;$i<=500;$i>=100?$i+=200:$i+=10){

				?>

					<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>

				<?php }?>

			</select>

			results

		</div>
		<div style="float:left;width:100%;height:25px;margin-top:5px;" class="filters_applied">
		</div>
		<div id="pagination" class="dataTables_paginate">



		</div>

		<!-- END TOP PAGINATION -->

		<table id="sortable_table" class="tablesorter">

			<thead>

				<th ><input type="checkbox" id="select_all" /></th>

				<th >Image</th>

				<th rel="2" sort="barcode" class='item_sort sort_d' t='Barcode'>Barcode <i class='fa fa-sort-desc'></i></th>

				<th rel="3" sort="item_number" class='item_sort' t='Part Number'>Part Number <i class='fa fa-sort'></i></th>

				<th rel="4" sort="name" class='item_sort' t='Part Name'>Part Name <i class='fa fa-sort'></i></th>

				<!-- <th>Name In Khmer</th> -->

				<th rel="5" sort="make_name" class='item_sort' t='Make'>Make <i class='fa fa-sort'></i></th>

				<th rel="6" sort="model_name" class='item_sort' t='Model'>Model <i class='fa fa-sort'></i></th>

				<th rel="7" sort="year" class='item_sort' t='Year'>Year <i class='fa fa-sort'></i></th>

				<th rel="8" sort="color_name" class='item_sort' t='Color'>Color <i class='fa fa-sort'></i></th>

				<th rel="9" sort="partplacement_name" class='item_sort' t='Part Placement'>Part Placement <i class='fa fa-sort'></i></th>

				<th rel="10" sort='branch_name' class='item_sort' t='Branch'>Branch <i class='fa fa-sort'></i></th>

				<th rel="11" sort='cost_price' class='item_sort' t='Cost Price'>Cost Price <i class='fa fa-sort'></i></th>

				<?php 
					$class = "hide";
					if($this->session->userdata('u_inf'))
					{
						$class = "";
					}
				?>
				<th rel="12" sort='unit_price' class='item_sort <?=$class?>' t='Unit Price'>Unit Price <i class='fa fa-sort'></i></th>

				<th rel="13" class='item_sort' t='Quantity' sort='quantity'>Quantity <i class='fa fa-sort'></i></th>

				<th>Inventory</th>

				<th rel="15" sort='item_status' class='item_sort' t='Status'>Status <i class='fa fa-sort'></i></th>



			</thead>

			<tbody>

				

			</tbody>

			<tfoot>

				

			</tfoot>

		</table>

		<!-- BOTTOM PAGINATION -->

		<div style="float:left;width:100%;height:25px;" class="page_result">

		<?php 

				$val=50;

				if (isset($_GET['page'])) {

					$val = $_GET['page'];

				}

				if (isset($_GET['per_page'])) {

				 	$per = $_GET['per_page'];

				 } 

		?>

		<!-- <p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p> -->
			<p>Showing 0 to 0 of 0 results</p>
		
		</div>

		<div style="float:left;width:50%;margin-top: 20px;" class="page_result">

			Show

			<select id="per_page_bot" class="per_page" name="per_page">

				<option value="all">All</option>

				<?php 

					$val=50;

					if (isset($_POST['page'])) {

						$val = $_POST['page'];

					}

					if (isset($_POST['per_page'])) {

					 	$per = $_POST['per_page'];

					 } 

					 for($i=10;$i<=500;$i>=100?$i+=200:$i+=10){

					 	

				?>

					<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>

				<?php 

					}

				?>

			</select>

			results

		</div>

		<div id="pagination" class='dataTables_paginate'>

		

		</div>

		<!-- END BOTTOM PAGINATION -->

	</div>

	<div style="clear:both"></div>

</div>
<div class="loading_box">
	<div class="loading_img"></div>
</div>
<script type="text/javascript">
	$(document).on('click', '.pagenav', function(){
	    var page = $(this).attr("id");

		search(page);

	});
	$('#isearch').keypress(function(e){

		 if(e.which == 13) {

			search(1,event,0,'s','');
			// alert('isearch entered');

		}

	});
	$('#search').keypress(function (e) {

	    if (e.which == 13) {

	    	var s ;

	  //   	if ($('.ac_results').is(':visible')) {

			// 	s = $('.ac_results li').first().text();

			// }else{

			s = $('#search').val();

			// }



	        $('#search').val(s);

			search(1,e,0,'sa','');

	    }

	});
</script>