<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function()
{
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');

});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				3: { sorter: false}
			}
		});
	}
}

function post_model_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.model_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.model_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.model_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

</script>
<style type="text/css">
	#pagination{
		padding: 1% 0;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="new_button">
		<?php echo anchor("$controller_name/view/-1/width:$form_width",
		"<div class='big_button' style='float: left;'><span>New</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		?>
	</div>
</div>
<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	<div style="float:right">
	<?php
		$val=20;
			if (isset($_GET['p'])) {
				$val = $_GET['p'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
		<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
</div>
<div id="table_action_header">
	<ul>
		<li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li class="float_right">
		<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		<input type="text" name ='search' id='search'/>
		</form>
		</li>
	</ul>
</div>

<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>