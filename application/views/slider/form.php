<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	ul,ol{
		margin-bottom: 0px !important;
	}
    .saveloading{width:35px;}
	a{
		cursor: pointer;
	}
	.datepicker {z-index: 9999;}
</style>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
$img_path=base_url('assets/site/image/no_img.png');
  
    if(file_exists(FCPATH."assets/upload/slide/$slide_info->slide_id.jpg")){
      $img_path=base_url("assets/upload/slide/$slide_info->slide_id.jpg");
    }
echo form_open('slider/save/'.$slide_info->slide_id,array('id'=>'model_form'));
?>
<fieldset id="model_basic_info" style="padding: 5px;">
<legend>Slide Information</legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('slide_type').':', 'make',array('class'=>' wide')); ?>
    <div class='form_field'>
    <?php echo form_dropdown('slide_type', array('main'=>'Main Slide','right'=>'Right Slide'), $slide_info->type);?>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('slide_name').':', 'url',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'slide_name',
		'id'=>'slide_name',
		'value'=>$slide_info->name)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('slide_url').':', 'url',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'slide_url',
		'id'=>'slide_url',
		'value'=>$slide_info->url)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('slide_order').':', 'url',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'slide_order',
		'id'=>'slide_order',
		'value'=>$slide_info->orders)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label("image".':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	 	<img  onclick="$('#uploadImage').click();" src="<?php echo $img_path?>" id="uploadPreview" style='width:150px;border:solid 1px #CCCCCC; padding:3px;'>
        <input id="uploadImage" rel='uploadPreview' type="file" name="userfile" onchange="PreviewImage(event);" style="visibility:hidden; display:none" />
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function PreviewImage(event) {
        var uppreview=$(event.target).attr('rel');
        //alert(uppreview);
        var upimage=$(event.target).attr('id');
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(upimage).files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById(uppreview).src = oFREvent.target.result;
             document.getElementById(uppreview).style.backgroundImage = "none";
        };
    }
$(document).ready(function(){
	$('#model_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_model_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			slide_name:"required",
			
   		},
		messages:
		{
			slide_name:"Input name to continue",	
			
		}
	});
});
</script>