
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}

	.name_er{
		color: red;
	}
</style>
<?php $er = $this->session->flashdata('er') ?>
<?php if ($er): ?>
	<div class="alert alert-danger">
		<p><?php echo $er ?></p>
	</div>
<?php endif ?>

<div class="panel panel-default">
  <!-- Default panel contents -->
  	<div class="panel-heading">
  		<div class="row">
  			<div class="col-md-6 pull-left">
  				<h4>List Engine</h4>
	  		</div>
	  		<?php if ($can_add==1): ?>
	  			<div class="col-md-6 pull-right">
		  			<button class="btn btn-primary pull-right btn_add" data-toggle='modal' data-target='#myModal'>New</button>
		  		</div>
	  		<?php endif ?>
	  		
	  		

  		</div>
  		
  	</div>
  	<div class="panel-body">
    	<div class="col-md-12">
			<table class="table table-bordered table-striped tdata">
				<thead>
					<th>Nº</th>
					<th>Engine Name</th>
					<th>Actions</th>
					<th></th>
				</thead>
				<tbody>
					<?php echo $table ?>
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>

  
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Engine</h4>
      </div>
      <form action="<?php echo site_url('engine/save') ?>" method='post'>
      	
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="col-md-6">
	        			<label>Engine Name :</label>
	        		</div>
	        		<div class="col-md-6">
	        			<input type="text" name="engine_name" class="form-control engine_name" value="" required="">
	        			<p class="name_er hide">Name Exist!</p>

	        		</div>
	        	</div>
	        </div>
      	</div>

      	<div class="modal-footer">
      		<input type="hidden" value="" class="engine_id" name="engine_id">
	        <input type="submit" class="btn btn-primary" value="Save">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>
      </form>

    </div>

  </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootbox/bootbox.min.js') ?>"></script>


<script type="text/javascript">
	$('.tdata').DataTable();

	$('.engine_name').keyup(function(){
		var n = $('.engine_name').val();
		var id = $('.engine_id').val();
		$.ajax({
			url:"<?php echo site_url('engine/check_name') ?>",
			type:'post',
			dataType:'Json',
			data:{
				name:n,
				id:id
			},success:function(re){
				if (re==1) {
					$('.name_er').removeClass('hide');
					$('.engine_name').css('color','red');
				}else{
					$('.name_er').addClass('hide');
					$('.engine_name').css('color','black');
				}
			}
		});
	})

	$('.tdata tbody').on('click','.btn_del',function(){

		var id = $(this).attr('f');
		var text = $(this).closest('tr').find('td:eq(1)').text();

		bootbox.confirm('សម្រេចលុប '+text+' ?',function(result){
			if (result===true) {
				location.href="<?php echo site_url('engine/delete') ?>"+"/"+id;
			};
		});
	})
	$('.tdata tbody').on('click','.btn_edit',function(){
		var n = $(this).attr('n');
		var id = $(this).attr('f');
		$('.engine_name').css('color','black');
		$('.name_er').addClass('hide');

		$('.engine_id').val(id);
		$('.engine_name').val(n);
		$('.modal-title').text('Edit Engine');
	});
	$('.btn_add').click(function(){
		$('.engine_name').val('');
		$('.engine_id').val('');
		$('.name_er').addClass('hide');

		$('.modal-title').text('New Engine');
	});
</script>