<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('engine/save/'.$engine_info->engine_id,array('id'=>'engine_form'));
?>
<fieldset id="make_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("engine_basic_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label($this->lang->line('engine_engine_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'engine_name',
		'id'=>'engine_name',
		'value'=>$engine_info->engine_name)
	);?>
	</div>
	<label class='name_er' style="color:red;display:none;">Name Exist !</label>
</div>
<input type="hidden" class="chk_id" value="<?php echo $engine_info->engine_id ?>">

<!-- <div class="field_row clearfix">
<?php echo form_label($this->lang->line('makes_card_description').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$make_info->description)
	);?>
	</div>
</div> -->

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
$(document).ready(function(){
	$('#engine_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_make_form_submit(response);
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			engine_name:"required"
   		},
		messages:
		{
			engine_name:"<?php echo $this->lang->line('engine_name_required'); ?>",	
		}
	});
});

$('#engine_name').keyup(function(){
	var n = $('#engine_name').val();
	var nid = $('.chk_id').val();
	$.ajax({
		url:"<?php echo site_url('engine/check_exist') ?>",
		type:'post',
		dataType:'Json',
		data:{
			name:n,
			id:nid
		},
		success:function(re){
			if (re==1) {
				$('#engine_name').css('color','red');
				$('.name_er').css('display','block');
			}else{
				$('#engine_name').css('color','black');
				$('.name_er').css('display','none');

			}
		}
	});
});
</script>