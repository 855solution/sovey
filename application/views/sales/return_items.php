<?php $this->load->view('partial/header') ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}

	.return_no_div{
		cursor: pointer;
		transition:all 0.3s;
		border-bottom: 1px solid #ccc;
	}
	.return_no_div:hover{
		background: #0a6184;
		color: white;
	}
	.bg-secondary{
		background: #eee;
	}
	.text_f{
		padding: 3px;
	}
	.return_item_div{
		display: none;
	}
	.return_item_div table th{
		text-align: center;
	    background: #4d8da6;
	    padding: 3px !important;
	    border-left: 1px solid white;
	    color: white;
	}
	.return_item_div table td{
		padding: 3px !important;
		white-space: nowrap;
	}
</style>





<div class="row">
	<div class="col-md-6">
		<h3>List Return</h3>
	</div>
	
</div>
<div class="panel panel-default">
  <!-- Default panel contents -->
  	<div class="panel-heading">
  		<div class="row">
  			<div class="col-md-12">
  				<h5>Filter:</h5>
	  		</div>
  		</div>
  		<div class="row">
  			
	  		<div class="col-md-12">
	  			<div class="col-md-6">
	  				
	  				<div class="col-md-4 form-group">
	  					<label>Employee</label>
	  					<select class="form-control filter_user">
		  					<option value="">Select</option>
		  					<?php foreach ($employee as $emp): ?>
		  						<option value="<?php echo $emp->person_id ?>"><?php echo $emp->title.$emp->last_name.' '.$emp->first_name ?></option>
		  					<?php endforeach ?>
		  				</select>
	  				</div>
	  				<div class="col-md-4 form-group">
	  					<label>From Date</label>
	  					<input type='text' class="form-control from_date" value="<?php echo date('d-m-Y') ?>">
		  			</div>
		  			<div class="col-md-4 form-group">
	  					<label>To Date</label>
		  				<input type='text' class="form-control to_date" value="<?php echo date('d-m-Y') ?>">
		  			</div>
	  				
	  			</div>
	  			
	  			<div class="col-md-1 form-group" style="margin-top: 25px;">
	  				<input type="button" class="btn btn-primary btn_search" value="Search">
	  			</div>
	  			
	  		</div>

  		</div>
  		
  	</div>
  	<div class="panel-body return_data">
    	
	</div>

  
</div>





<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<?php $this->load->view('partial/footer') ?>

<script type="text/javascript">
	$('.from_date,.to_date').datepicker({
		format:"dd-mm-yyyy"
	});

	$('.filter_user').select2();


	$(document).ready(function(){
		get_data();
	});

	$('.btn_search').click(function(){
		get_data();
	})

	function get_data(){
		var emp_id = $('.filter_user').val();
		var fd = $('.from_date').val();
		var td = $('.to_date').val();
		$('.loading_box').fadeIn();

		$.ajax({
			url:"<?php echo site_url('sales/get_list_return') ?>",
			type:'post',
			dataType:'json',
			data:{
				emp_id:emp_id,
				fd:fd,
				td:td
				
			},
			success:function(oh){

				$('.return_data').html(oh.return_data);

				$('.loading_box').fadeOut();

			}
			


		});
	}


	

	function toggleItems(row_id){
		// var ele = $(e.target);
		// var row_id = ele.attr('row-id');

		$('#row_'+row_id).slideToggle();
	}
</script>