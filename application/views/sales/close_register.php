<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<style type="text/css">
	body{
		background: white;
	}
	.danger-item td{
		padding: 2px !important;
	}
	.danger-item th{
		padding: 2px !important;
	}
	@media print{
		.btn{
			display: none;
		}
	}
</style>


<div id="title_bar" class="col-md-12">
	<div class="col-md-6">
	 	<h4><?php echo $controller_name."(".date('d/m/Y H:i:s',strtotime($reg_info->open_time))." - ".date('d/m/Y H:i:s').")";?></h4>
	</div>
	<div class="col-md-6">
		<!-- <a href="<?php echo site_url('sales/print_close_register/'.$reg_info->register_id) ?>" target='_pcr' class="btn btn-success pull-right">Print</a> -->
	</div>
</div>
<div class="clearfix"></div>
<div class="well">
	<div class="row">
		<div class="col-md-12">
			<form id="close_form" method="post" action="<?php echo site_url('sales/do_close_register') ?>">
			<div class="col-md-3">
				<div class="col-md-12 form-group">
					<label>Cash in Hand</label>
					<input type="text" class="form-control" readonly="true" value="<?php echo number_format($reg_info->cash_in_hand,2) ?>">

				</div>
				
				<div class="col-md-12 form-group">
					<label>Total Sale</label> 
					<input type="text" onkeypress="return isNumber(event)" name="open_cash" class="form-control" value="<?php echo number_format($total_sale,2); ?>" readonly="true"> 
					
				</div>
				<!-- <div class="col-md-12 form-group">
					<label>Total Sale Due</label>
					<input type="text" readonly="true" class="form-control" value="<?php echo number_format($sale_due,2) ?>">

				</div> -->
				
				<div class="col-md-12 form-group">
					<label>Total Deposit</label>
					<input type="text" readonly="true" class="form-control" value="<?php echo number_format($total_deposit,2) ?>">

				</div>
				<div class="col-md-12 form-group">
					<label>Total Return</label>
					<input type="text" readonly="true" class="form-control" value="<?php echo number_format($total_return,2) ?>">

				</div>
				<div class="col-md-12 form-group">
					<label>Total Due Payment</label>
					<input type="text" readonly="true" class="form-control" value="<?php echo number_format($total_due_payment,2) ?>">

				</div>
				<div class="col-md-12 form-group">
					<label>Total Cash</label>
					<input type="text" readonly="true" class="form-control" value="<?php echo number_format($total_cash,2) ?>">
				</div>
				<!-- <div class="col-md-12 form-group">
					<label>Total Deposit Due</label>
					<input type="text" readonly="true" class="form-control" value="<?php echo number_format($deposit_due) ?>">

				</div> -->
				<!-- <div class="col-md-12 form-group">
					<label>Total Cash</label>
					<input type="text" class="form-control" value="<?php echo number_format($reg_info->sale_submit+$reg_info->cash_in_hand) ?>" readonly="true">
				</div> -->
				
			</div>
			<div class="col-md-9" style="overflow-y: scroll;max-height: 505px;">
				<h2><span class="label label-primary">Item Sold Without Vinnumber</span></h2>
				<table class="table table-bordered">
					<thead>
						<th>No</th>
						<th>Vin</th>
						<th>Barcode</th>
						<th>Part Name</th>
						<th>Make</th>
						<th>Model</th>
						<th>Year</th>
						<th>Invoice #</th>
					</thead>
					<tbody>
						<?php echo $tbody ?>
					</tbody>
				</table>
			</div>
			<div class="col-md-12 form-group">
				<label>Note</label>
				<textarea name="register_note" class="form-control"></textarea>
			</div>
			
			<div class="col-md-12 form-group">
				<input type="button" class="btn btn-primary pull-right" id="btn_submit" value="Close Register">
				
			</div>
			</form>

		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootbox/bootbox.min.js') ?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.vin_sel').select2({ width: '100%' });

	});

	$('#btn_submit').click(function(e){

		$('#close_form').attr('target','_pcr');

		bootbox.confirm("សម្រេច Close Register ?", function(result){

			if (result==true) {
				$('#close_form').submit();
				setTimeout(function(){
					location.href="<?php echo site_url('home') ?>";
				},1000);
			};
		}
		
		);
		
		
	});

</script>

<?php $this->load->view("partial/footer"); ?>