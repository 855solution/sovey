<?php 
if (!$suspended_sales) {
	?>
	<div align="center" class="warning_message">
		No Suspended Sale.
	</div>
	<?php
}else{

 ?>
<style type="text/css">
	#suspended_sales_table td{
		text-align: center;
		padding: 1% 1px !important;
		vertical-align: middle;
	}
	#action_bar{
		background: rgb(238, 238, 238) none repeat scroll 0% 0%;
		vertical-align: middle;
		border: 1px solid rgb(204, 204, 204);
		border-radius: 5px;
		margin: 1% 0;
		padding: 1%;
		padding-right: 0.3%;
		height: 25px;

	}
</style>
<div id="action_bar">
	<input type="text" name='isearch' id="isearch" class="isearch" placeholder="Search list" style="margin-top:-2px;">
</div>
<table id="suspended_sales_table" class="tablesorter">
	<thead>
		<tr>
			<th>Deposit No</th>
			<th><?php echo $this->lang->line('sales_date'); ?></th>
			<th><?php echo $this->lang->line('sales_customer'); ?></th>
			<th><?php echo $this->lang->line('sales_customer_phone') ?></th>
			<th><?php echo $this->lang->line('sales_comments'); ?></th>
			<th colspan="3"><?php echo $this->lang->line('sales_unsuspend_and_delete'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach ($suspended_sales as $suspended_sale)
	{
	?>
		<tr>
			<td><?php echo $suspended_sale['deposit_id'];?></td>
			<td><?php echo date('d/m/Y',strtotime($suspended_sale['sale_time']));?></td>
			<td>
				<?php
				if (isset($suspended_sale['customer_id']))
				{
					$customer = $this->Customer->get_info($suspended_sale['customer_id']);
					$company = '';
					if ($customer->nick_name!='') {
						$company = "($customer->nick_name)";
					}
					echo $customer->last_name. ' '. $customer->first_name.$company;
				}
				else
				{
				?>
					&nbsp;
				<?php
				}
				$cm = '';
				if ($suspended_sale['comment']!='0') {
					$cm = $suspended_sale['comment'];
				}
				?>
			</td>
			<td><?php echo $suspended_sale['phone_number'] ?></td>
			<td><?php echo $cm;?></td>
			
				<td>
					<?php 
					echo form_open('sales/view_deposit',array('class'=>'sus_form'));
					echo form_hidden('suspended_sale_id', $suspended_sale['sale_id']);
					?>
					<input type="submit" name="submit" value="<?php echo $this->lang->line('sales_unsuspend'); ?>" id="submit" class="deposit_btn submit_button float_right" style="width:100%;">
				</td>
				</form>
				<td>
					<!-- <?php 
						echo form_open('sales/delete_suspend');
						echo form_hidden('suspended_sale_id_del', $suspended_sale['sale_id']);
					 ?>
					<input onclick="return confirm('Are you sure ?')" type="submit" name="subdelete" value="Delete" class="submit_button float_right"> -->
				</td>
				</form>
				<td>
					<a target="_target" href="<?php echo site_url('sales/dprint/'.$suspended_sale['sale_id']) ?>" id="dprint" name="dprint" class="submit_button float_right">
						Print
					</a>
				</td>
		</tr>
	<?php
	}
	
	?>
	</tbody>
</table>
<?php } ?>
<script type="text/javascript">
	$('#isearch').keyup(function(){
		var isearch = $('#isearch').val();
		$.post("<?php echo site_url('sales/search_suspend')?>",
		{
			search:isearch
		},
		function(data){
			if (data!='') {
				$('.tablesorter tbody').html(data.data);
			}else{
				console.log(data);
			}
		},'Json')
	});

	$('.sus_form').submit(function(){
		var c = $('#cart_count').val();
		if (c>0) {
			if (confirm("You Will Lose Current Data !! Are you sure ?")) {
				return true;
			};
			return false;
		}else{
			return true;

		};
	});
</script>