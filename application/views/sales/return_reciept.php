<?php $this->load->view("partial/header"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	exit;
}
?>
<!-- <style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.box_signature{
		float: left;
		width: 20%;
		margin-left: 10%;

	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}


    @media print {
    	#receipt_header,#receipt_items{
    		color: black;
    	}
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			width: 95%;
   			margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head{
			/*width: 45%;
			float: left;
			left: 0;*/
			margin-top: 38px !important;
			white-space: nowrap;
		}
		.box_signature{
			float: left;
			width: 20%;
    		color: black;
			margin-left: 10%;


		}
		.right_signature{
			float: right;
			width: 30%;
    		color: black;

		}
		.footer_signature{
			width: 90%;
		}

		
	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 30px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		margin-top: 30px;
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	

</style> -->
<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		/*margin-bottom: 30px !important;*/
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		/*margin-top: 30px;*/

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		/*margin-top: 30px;*/
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:4px;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:4px;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	.r_text,.l_text{
		white-space: nowrap;
	}
	#company_phone{
		margin-bottom: 20px;
	}

	.box_signature{
		float: left;
		width: 20%;
		color: black;
		margin-left: 10%;


	}
	.right_signature{
		float: right;
		width: 30%;
		color: black;

	}
	.footer_signature{
		width: 90%;
	}


	
@media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			font-size: 10px;
   			width:100%;
   			margin:0 auto !important;
   			padding: 0 !important;
   			float: none !important; 
   			/*margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;*/

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   			min-height: 0 !important;
   			padding: 0 !important;
   			margin: 0 !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head,.right_head{
			
			color: black !important;

		}
		.left_head{
			margin-top: 5px;
		}
		

		#receipt_header{
			color: black !important;

		}
		#receipt_items{
			color: black !important;
		    margin-top: 1px;
		}
		#receipt_items th,#receipt_items td{
			padding: 0 !important;
		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}
		.r_text div,
		.l_text div,
		.r_text p,
		.l_text p{
			padding: 4px !important;
			line-height: 10px !important;

		}
		#barcode{
			margin-top: 8px !important;

		}
		.underline{
			height: 30px !important;
		}

		textarea{
			border: none;
			resize: none;
			font-size: 8px;
			color: black;
			width: 98% !important;
			background: transparent;
		}
		#company_phone{
			margin-bottom: 15px;
		}
		/*@page {
		   @bottom-right {
		    content: counter(page) " of " counter(pages);
		   }
		}*/
	}
</style>
<div id="receipt_wrapper">
	<div class="top_btn">
		<div class="small_button float_left" id="btn_new_sale"><span>New Sale</span></div>
		<!-- <div class="small_button float_left" id="btn_edit_sale"><span>Edit Sale</span></div> -->
		<a href="<?php echo site_url('sales/view_returned_items/'.$return_id) ?>" class="thickbox" title="Returned Items">
			<div class="small_button float_left" id="btn_returned_items"><span style="font-size: 9px;">Returned Items (<?php echo $count_return_items ?>)</span></div>
		</a>
		<div class="small_button float_right" id="btn_print"><span>Print</span></div>
	</div>
	<div style="clear:both">
		
	</div>
	<div id="receipt_header">
		

		<div id="company_name"><?php echo $this->config->item('company'); ?>
		<span class="inv_type">Return</span>
		</div>

		<div class="left_head">
			<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
			<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>

			<?php if(isset($customer))
			{
			?>
				<div id="customer" class="l_text"><div>Customer</div><p><?php echo $customer ?></p></div>
				<div class="l_text">
					<div>Phone Number</div><p><?php echo $cus_info->phone_number.' '.$cus_info->email ?></p>
				</div class="l_text">
				<!-- <div class="l_text"> 
					<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
				</div> -->
				<!-- <div class="l_text">
					<div>Email</div><p><?php echo $cus_info->email ?></p>
				</div> -->
				<!-- <div class="l_text">
					<div>Contact</div><p><?php echo $customer; ?></p>
				</div> -->

			<?php
			}
			?>
			
		</div>
		<div class="right_head">
			<div id='barcode'>
			<?php echo "<img src='index.php/barcode?barcode=".str_replace('-', '', $return_id)."&text=$return->return_no&width=250&height=50' />"; ?>
			</div>
			<!-- <div class="r_text"><div>Page</div><p class="page_counter"></p></div> -->
			<!-- <div class="r_text"><div>Return ID</div><p><?php echo $return_id; ?></p></div> -->
			<div class="r_text"><div>Return Date</div><p><?php echo $transaction_time ?></p></div>
			<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>
			<!-- <div class="r_text"><div>Payment Type</div><p><?php echo $payments['Cash']['payment_type'] ?></p></div> -->
		</div>
	</div>
	<div style="clear:both"></div>

	<table id="receipt_items">
	<thead>
		<tr>
		<!-- <th></th> -->
	    <th>No</th>
		<th>Barcode</th>
		<!-- <th>Vinnumber</th> -->
		<th>Description</th>
		<th><?php echo $this->lang->line('sales_quantity'); ?></th>
		<!-- <th><?php echo "PPL"; ?></th> -->
		<th><?php echo $this->lang->line('common_price'); ?></th>
		<th><?php echo "Restock"; ?></th>
		<th>Subtotal</th>
		</tr>
	</thead>
	
	<tbody style="border-bottom:2px solid black">
	<?php
	$i=0;
	
	// var_dump($items);
	foreach($items as $item)
	{
		$i++;
		$name = $item->name;
		if ($item->khmername_name) {
			$name = $item->khmername_name;
		}
		$ppl = $item->partplacement_id;
		if ($item->is_new==0) {
			$ppl = $item->partplacement_name;
		}
		$sub_total = 0;
		$sub_total = ($item->price*1)-$item->disc;

		$item_desc = '';
		if ($item->description) {
			$item_desc = "($item->description)";
		}
	?>
		
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $item->barcode ?></td>
			<!-- <td><?php echo $item->category ?></td> -->
			<!-- <td style="text-align:left;"><?php echo $item->year.' '.$item->make_name.' '.$item->model_name.' '.$name ?></td> -->
			<td style="text-align:left;"><?php echo $item->year.' '.$item->make_name.' '.$item->model_name.' '.$name.' '.$ppl.' '.$item_desc; ?></td>

			<td style='text-align:center;'><?php echo 1; ?></td>
			<!-- <td><?php echo $item->ppl_id;?></td> -->
			<td>-<?php echo to_currency($item->price); ?></td>
			<td style='text-align:center;'><?php echo $item->disc; ?></td>
			<td style='text-align:right;'>-<?php echo to_currency($sub_total); ?></td>
		</tr>

	<?php
	$total+=$sub_total;
	}
	?>
	</tbody>
	<tfoot>
		
	<!-- <tr>
	<td colspan="4"></td>
	<td colspan="3"style='text-align:right;font-weight:bold;border:1px solid #ddd;'><?php echo $this->lang->line('sales_sub_total'); ?></td>
	<td colspan="3" style='text-align:right;border:1px solid #ddd;'>-<?php echo to_currency($total); ?></td>
	</tr> -->

	<?php foreach($taxes as $name=>$value) { ?>
		<!-- <tr>
			<td colspan="7" style='text-align:right;'><?php echo $name; ?>:</td>
			<td colspan="3" style='text-align:right;'><?php echo to_currency($value); ?></td>
		</tr> -->
	<?php }; ?>

	<tr>
		<td rowspan="3" style="vertical-align: top;">
			Note :
		</td>
		<td colspan="3" rowspan="3" style="text-align: left;">
			<textarea rows="3" style="width: 50%;resize: none;"><?php echo $this->config->item('return_note') ?></textarea>
		</td>
	<td colspan="2" style='text-align:right;font-weight:bold;border:1px solid #ddd;'><?php echo $this->lang->line('sales_total'); ?></td>
	<td colspan="2" style='text-align:right;border:1px solid #ddd;background: #e2e2e2;font-weight: bold;'>-<?php echo to_currency($total); ?></td>
	</tr>
	<!-- PAID -->
	<tr>
	<!-- <td colspan="4"></td> -->
	<td colspan="2" style='text-align:right;font-weight:bold;border:1px solid #ddd;'>Return Amount</td>
	<td colspan="2" style='text-align:right;border:1px solid #ddd;background: #e2e2e2;font-weight: bold;'><?php echo to_currency($paid); ?></td>
	</tr>
	<?php if ($total-$paid>0): ?>
		<!-- RETURN DUE -->
		<!-- <tr> -->
		<!-- <td colspan="4"></td> -->
		<!-- <td colspan="2" style='text-align:right;font-weight:bold;border:1px solid #ddd;'>Return Due</td>
		<td colspan="2" style='text-align:right;border:1px solid #ddd;background: #e2e2e2;font-weight: bold;'><?php echo to_currency((-$total)-$paid); ?></td>
		</tr> -->
	<?php endif ?>
	

	<?php
		// $first_payment= (float)filter_var($sale_pay_amount, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION); 
		// $paid=$payment->payment_amount-$payment->dept;
		// $change = $payment->dept;
		// $due_status = 'Amount Due';
		// if ($first_payment>$payment->payment_amount) {
		// 	$due_status = 'Change';
		// 	$change = $payment->payment_amount-$first_payment;
		// }
	?>
	<!-- <tr>
		<td colspan="4"></td>
		<td colspan="3" style="text-align:right;font-weight:bold;border:1px solid #ddd;"><?php echo $this->lang->line('sales_paid'); ?></td>
		<td colspan="3" style="text-align:right;border:1px solid #ddd;"><?php echo to_currency($paid); ?>  </td>
    </tr> -->
	

    <!-- <tr><td colspan="10">&nbsp;</td></tr>  -->
	<!-- <tr>
		<td colspan="4"></td>
		<td colspan="3" style='text-align:right;font-weight:bold;border:1px solid #ddd;'><?php echo $due_status ?></td>
		<td colspan="3" style='text-align:right;border:1px solid #ddd;'><?php echo  to_currency(abs($change)); ?></td>
	</tr> -->
	
	</tfoot>

	</table>

	<!-- <div id="sale_return_policy">
	<?php echo nl2br($this->config->item('return_policy')); ?>
	</div> -->
	<div class="footer_signature_wrapper" align="center">
		<div class="footer_signature">
			<div class="box_signature">
				<label>Customer's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<div class="box_signature">
				<label>Stock Controller's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<div class="box_signature">
				<label>Seller's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			
		</div>
	</div>
	
	
</div>


<div style="clear:both"></div>
<?php $this->load->view("partial/footer"); ?>

<?php if ($this->Appconfig->get('print_after_sale'))
{
?>
<script type="text/javascript">
$(window).load(function()
{
	window.print();
	localStorage.setItem('selling',1);

	// alert(s);
});

$('#btn_new_sale').click(function(){
	location.href = "<?php echo site_url('sales/new_sale') ?>";
});
$('#btn_edit_sale').click(function(){
	// alert('Under Construction...');
	location.href = "<?php echo site_url('sales/edit_sale/'.$sale_id) ?>";
});

$('#btn_print').click(function(){
	window.print();
});
</script>
<?php
}
?>