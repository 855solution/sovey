<?php $this->load->view('partial/header') ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}
</style>
<div class="row">

	<div class="col-md-6">
		<h3>List Detail Sales</h3>
	</div>
	<div class="col-md-6">
		<h3><a href="<?php echo site_url('sales/list_sales') ?>" class="btn btn-primary pull-right">List Summary Sales >></a></h3>
	</div>
</div>
<div class="panel panel-default">
  <!-- Default panel contents -->
  	<div class="panel-heading">
  		<div class="row">
  			<div class="col-md-12">
  				<h5>Filter:</h5>
	  		</div>
  		</div>
  		<div class="row">
  			
	  		<div class="col-md-12">
	  			<div class="col-md-6">
	  				
	  				<div class="col-md-4 form-group">
	  					<label>Employee</label>
	  					<select class="form-control filter_user">
		  					<option value="">Select</option>
		  					<?php foreach ($employee as $emp): ?>
		  						<option value="<?php echo $emp->person_id ?>"><?php echo $emp->title.$emp->last_name.' '.$emp->first_name ?></option>
		  					<?php endforeach ?>
		  				</select>
	  				</div>

	  				<div class="col-md-4 form-group">
	  					<label>Customer</label>
	  					<select class="form-control filter_customer">
		  					<option value="">Select</option>
		  					<?php foreach ($customer as $cus): ?>
		  						<option value="<?php echo $cus->person_id ?>"><?php echo $cus->title.$cus->last_name.' '.$cus->first_name ?></option>
		  					<?php endforeach ?>
		  				</select>
	  				</div>

	  				<!-- <div class="col-md-4 form-group">
	  					<?php
	  						$period = array('Daily','Monthly','Yearly');
	  					?>
	  					<label>Period</label>

	  					<select class="form-control filter_period">
		  					<?php foreach ($period as $key => $val): ?>
		  						<option value="<?php echo $val ?>"><?php echo $val ?></option>
		  					<?php endforeach ?>
		  				</select>
	  				</div> -->
	  				
	  			</div>
	  			


	  			<!-- DAILY -->
	  			<div class="col-md-5 daily_div hide">
	  				<div class="col-md-6 form-group">
	  					<label>From Date</label>
	  					<input type='text' class="form-control from_date" value="">
		  			</div>
		  			<div class="col-md-6 form-group">
	  					<label>To Date</label>
		  				<input type='text' class="form-control to_date" value="">
		  			</div>
	  			</div>

	  			<!-- MONTHLY -->
	  			<div class="col-md-5 monthly_div hide">
	  				<div class="col-md-6 form-group">
	  					<label>From Month</label>
	  					<input type='text' class="form-control from_month" value="">
		  			</div>
		  			<div class="col-md-6 form-group">
	  					<label>To Month</label>
		  				<input type='text' class="form-control to_month" value="">
		  			</div>
	  			</div>

	  			<!-- Yearly -->
	  			<div class="col-md-5 yearly_div hide">
	  				<div class="col-md-6 form-group">
	  					<label>From Year</label>
	  					<input type='text' class="form-control from_year" value="">
		  			</div>
		  			<div class="col-md-6 form-group">
	  					<label>To Year</label>
		  				<input type='text' class="form-control to_year" value="">
		  			</div>
	  			</div>
	  			<div class="col-md-1 form-group" style="margin-top: 25px;">
	  				<input type="button" class="btn btn-primary btn_search" value="Search">
	  			</div>
	  			
	  		</div>

  		</div>
  		
  	</div>
  	<div class="panel-body">
    	<div class="col-md-12">
			<table class="table table-bordered table-striped tdata">
				<thead>
					<th>Nº</th>
					<th>Date</th>
					<th>Invocie #</th>
					<th>Customer</th>
					<th>Grand Total</th>
					<th>Paid</th>
					<th>Due</th>
					<th>Created by</th>
					<th>Status</th>
					<th>Actions</th>
				</thead>
				<tbody>
					
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>

  
</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>

<?php $this->load->view('partial/footer') ?>

<script type="text/javascript">
	var oTable;
	$(document).ready(function(){
		get_data();
	});
	function get_data(){
		var user = $('.filter_user').val();
		var cus = $('.filter_customer').val();
		var per = $('.filter_period').val();
		var fd = $('.from_date').val();
		var td = $('.to_date').val();
		// var fm = $('.from_month').val();
		// var tm = $('.to_month').val();
		// var fy = $('.from_year').val();
		// var ty = $('.to_year').val();
		

		$('.loading_box').fadeIn();
		
		$.ajax({
			url:"<?php echo site_url('sales/get_list_sale_detail') ?>",
			type:'post',
			dataType:'json',
			data:{
				user:user,
				cus:cus,
				per:per,
				fd:fd,
				td:td
				
				
			},
			success:function(oh){
				// console.log(oh.tbody);
				if (oh.tbody) {

				$('.tdata tbody').html(oh.tbody);
				$('.tdata tfoot').html(oh.tfoot);	
					oTable = $('.tdata').DataTable({
						"lengthMenu": [ [10, 30, 50, 100, -1], [10, 30, 50, 100, "All"] ],
						"iDisplayLength": 50,
						"footerCallback": function ( row, data, start, end, display ) {
				            var api = this.api(), data;
				 			
				            // Remove the formatting to get integer data for summation
				            var intVal = function ( i ) {
				                return typeof i === 'string' ?
				                    i.replace(/[\$,]/g, '')*1 :
				                    typeof i === 'number' ?
				                        i : 0;
				            };
				 
				            // Total over all pages
				            // total = api
				            //     .column( 4 )
				            //     .data()
				            //     .reduce( function (a, b) {
				            //         return intVal(a) + intVal(b);
				            //     }, 0 );

				 
				            // Total over this page
				           	pTotalG = api
				                .column( 4, { page: 'current'} )
				                .data()
				                .reduce( function (a, b) {
				                    return intVal(a) + intVal(b);
				                }, 0 );
				            pTotalP = api
				                .column( 5, { page: 'current'} )
				                .data()
				                .reduce( function (a, b) {
				                    return intVal(a) + intVal(b);
				                }, 0 );
				            pTotalD = api
				                .column( 6, { page: 'current'} )
				                .data()
				                .reduce( function (a, b) {
				                    return intVal(a) + intVal(b);
				                }, 0 );
				 
				            // Update footer
				            $( api.column( 4 ).footer() ).html('$ '+numberWithCommas(pTotalG.toFixed(2)));
				            $( api.column( 5 ).footer() ).html('$ '+numberWithCommas(pTotalP.toFixed(2)));
				            $( api.column( 6 ).footer() ).html('$ '+numberWithCommas(pTotalD.toFixed(2)));

				        }
					});
				}

				$('.loading_box').fadeOut();

				
			}
		});
	}

	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	$('.btn_search').click(function(){
		oTable.destroy();
		get_data();
	})

	$('.from_date,.to_date').datepicker({
		format:'dd-mm-yyyy'
	});
	$('.from_month,.to_month').datepicker({
		format: "mm-yyyy",
	    viewMode: "months", 
	    minViewMode: "months"
	});
	$('.from_year,.to_year').datepicker({
		format: "yyyy",
	    viewMode: "years", 
	    minViewMode: "years"
	});
	$('.filter_user,.filter_period,.filter_customer').select2();

	$('.filter_period').change(function(){
		var per = $('.filter_period').val();
		show_detail_filter(per);
	});
	show_detail_filter('Daily');
	function show_detail_filter(period){
		var d = $('.daily_div');
		var m = $('.monthly_div');
		var y = $('.yearly_div');
		d.addClass('hide');
		m.addClass('hide');
		y.addClass('hide');
		d.val('');
		m.val('');
		y.val('');
		if (period=='Daily') {
			d.removeClass('hide');
		}
		if (period=='Monthly') {
			m.removeClass('hide');
		}
		if (period=='Yearly') {
			y.removeClass('hide');
		}
	}
</script>