<?php 
if (!$quoted_sales) {
	?>
	<div align="center" class="warning_message">
		No Quotations.
	</div>
	<?php
}else{

 ?>
<style type="text/css">
	#suspended_sales_table td{
		text-align: center;
		padding: 1% !important;
		vertical-align: middle;
	}
	
</style>
<div id="action_bar">
	<input type="text" name='isearch' id="isearch" class="isearch" placeholder="Search list" style="margin-top:-2px;">
</div>
<table id="suspended_sales_table" class="tablesorter">
	<thead>
		<tr>
			<th>Quote Sale ID</th>
			<th>Date</th>
			<th>Customer</th>
			<th>Phone</th>
			<th>Comment</th>
			<th colspan="3">Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php

	foreach ($quoted_sales as $q)
	{
	?>
		<tr>
			<td><?php echo $q['qt_number']?></td>
			<td><?php echo date('d/m/Y',strtotime($q['quote_time']));?></td>
			<td>
				<?php
				if (isset($q['customer_id']))
				{
					$customer = $this->Customer->get_info($q['customer_id']);
					$company = '';
					if ($customer->nick_name!='') {
						$company = "($customer->nick_name)";
					}
					echo $customer->last_name. ' '. $customer->first_name.$company;
				}
				else
				{
				?>
					&nbsp;
				<?php

				}
				$cm = '';
				if ($q['comment']!='0') {
					$cm = $q['comment'];
				}
				?>
			</td>
			<td><?php echo $q['phone_number'] ?></td>
			<td><?php echo $cm?></td>
			<td>
				<?php 
				echo form_open('sales/view_quoted',array('class'=>'quo_form'));
				echo form_hidden('quoted_sale_id', $q['sale_id']);
				?>
				<input type="submit" name="submit" value="<?php echo 'View' ?>" id="submit" class="submit_button float_right" style="width:100%;">
				</form>
				
			</td>
			<td>
				<?php 
					echo form_open('sales/delete_quote');
					echo form_hidden('quoted_sale_id_del', $q['sale_id']);
				 ?>
				<input onclick="return confirm('Are you sure ?')" type="submit" name="subdelete" value="Delete" class="submit_button float_right">
				</form>
				
			</td>
			<td>
				<a target="_target" href="<?php echo site_url('sales/qprint/'.$q['sale_id']) ?>" id="qprint" name="qprint" class="submit_button float_right">
					Print
				</a>
			</td>
		</tr>
	<?php
	}
	
	?>
	</tbody>
</table>
<?php } ?>
<script type="text/javascript">
	$('#isearch').keyup(function(){
		var isearch = $('#isearch').val();
		$.post("<?php echo site_url('sales/search_quote')?>",
		{
			search:isearch
		},
		function(data){
			if (data!='') {
				$('.tablesorter tbody').html(data.data);
			}else{
				console.log(data);
			}
		},'Json')
	});
	$('.quo_form').submit(function(){
		var c = $('#cart_count').val();
		if (c>0) {
			if (confirm("You Will Lose Current Data !! Are you sure ?")) {
				return true;
			};
			return false;
		}else{
			return true;

		};
	});

</script>