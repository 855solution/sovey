<?php $this->load->view("partial/header"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	exit;
}
?>
<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		/*margin-bottom: 30px !important;*/
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		/*margin-top: 30px;*/

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		/*margin-top: 30px;*/
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:4px;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:4px;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:4px;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	.r_text,.l_text{
		white-space: nowrap;
	}
	#company_phone{
		margin-bottom: 5px;
	}


	
@media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			font-size: 10px;
   			width:100%;
   			margin:0 auto !important;
   			padding: 0 !important;
   			float: none !important; 
   			/*margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;*/

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   			min-height: 0 !important;
   			padding: 0 !important;
   			margin: 0 !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head,.right_head{
			
			color: black !important;

		}
		.left_head{
			margin-top: 5px;
		}
		

		#receipt_header{
			color: black !important;

		}
		#receipt_items{
			color: black !important;
		    margin-top: 1px;
		}
		#receipt_items th,#receipt_items td{
			padding: 0 !important;
		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}
		.r_text div,
		.l_text div,
		.r_text p,
		.l_text p{
			padding: 4px !important;
			line-height: 10px !important;

		}
		#barcode{
			margin-top: 8px !important;

		}
		.underline{
			height: 30px !important;
		}

		textarea{
			border: none;
			resize: none;
			font-size: 8px;
			color: black;
			width: 98% !important;
			background: transparent;
		}

		/*@page {
		   @bottom-right {
		    content: counter(page) " of " counter(pages);
		   }
		}*/
	}
</style>
<div id="receipt_wrapper">
	<div class="top_btn">
		<div class="small_button float_left" id="btn_new_sale"><span>New Sale</span></div>
		<div class="small_button float_left" id="btn_edit_sale"><span>Edit Sale</span></div>
		<div class="small_button float_right" id="btn_print"><span>Print</span></div>
	</div>
	<div style="clear:both">
		
	</div>
	<div id="receipt_header">
		

		<div id="company_name"><?php echo $this->config->item('company'); ?>
		<span class="inv_type">Sale</span>
		</div>

		<div class="left_head">
			<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
			<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
			<?php if(isset($customer))
			{
			?>
				<div id="customer" class="l_text"><div>Customer</div><p><?php echo $cus_info->title.$customer;echo $cus_info->person_id!='72'?$cus_info->nick_name!=''?"($cus_info->nick_name) ID: $cus_info->person_id":' ID: '.$cus_info->person_id:'' ?></p></div>
				<div class="l_text">
					<div>Phone Number</div><p><?php echo $cus_info->phone_number ?></p>
				</div class="l_text">
				<div class="l_text"> 
					<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
				</div>
				<!-- <div class="l_text">
					<div>Email</div><p><?php echo $cus_info->email ?></p>
				</div> -->
				<!-- <div class="l_text">
					<div>Contact</div><p><?php echo $customer; ?></p>
				</div> -->

			<?php
			}
			?>
			
		</div>
		<div class="right_head">
			<div id='barcode'>
			<!-- <?php echo "<img src='index.php/barcode?barcode=".$invoice_id."&text=$invoice_id&width=400&height=50' />"; ?> -->
			</div>
			<div class="r_text"><div>Page</div><p class="page_counter"></p><p>/ <?php echo ceil(count($items)/13) ?></p></div>
			<!-- <div class="r_text"><div>Invoice #</div><p><?php echo $invoice_id; ?></p></div> -->
			<div class="r_text"><div>Sale Date</div><p><?php echo date("d/m/Y H:i:s",strtotime($transaction_time)) ?></p></div>
			<div class="r_text"><div>Seller</div><p><?php echo $employee; ?></p></div>
			<!-- <div class="r_text"><div>Payment Type</div><p><?php echo $payments['Cash']['payment_type'] ?></p></div> -->
		</div>
	</div>
	<div style="clear:both"></div>

	<table id="receipt_items">
	<thead>
		<tr>
		<!-- <th></th> -->
	    <th>No</th>
		<th>Barcode</th>
		<th>Head #</th>
		<th>Description</th>
		<th><?php echo $this->lang->line('sales_quantity'); ?></th>
		<!-- <th><?php echo "PPL"; ?></th> -->
		<th><?php echo $this->lang->line('common_price'); ?></th>
		<th><?php echo "Disc"; ?></th>
		<th>Subtotal</th>
		</tr>
	</thead>
	
	<tbody style="border-bottom:2px solid black">
	<?php
	$i=0;


	foreach($items as $item)
	{
		$i++;
		$name = $item->khmername_name;
		if (!$item->khmername_name) {
			$name = $item->name;
		}
		$ppl = $item->ppl_id;

		$barcode = $item->barcode;

		if ($item->is_new==0) {
			$ppl = $item->partplacement_name;
			$barcode = str_pad($item->item_id,6,"0",STR_PAD_LEFT);
		}
		

		$item_vinnumber_id = $this->db->query("SELECT * FROM ospos_vinnumbers WHERE vinnumber_name = '$item->category'")->row()->vinnumber_id;

		$item_desc = '';
		if ($item->description) {
			$item_desc = "($item->description)";
		}
		$sub_total = 0;

		$item_return_desc = '';
		if ($item->is_return==1) {
			$return_no = "<a target=".rand(0,99)." href='".site_url('returns/view_reciept/'.$item->return_id)."'>".$item->return_no."</a>";
			$item_return_desc = " (Restock: ".to_currency($item->return_restock).", $item->barcode)";
			$sub_total = $item->return_restock;


		}else{
			$sub_total = ($item->item_unit_price*$item->quantity_purchased)-$item->discount_percent;
		}



	?>
	
	<?php if ($item->is_return==1): ?>
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $return_no ?></td>
			<td><?php echo $item_vinnumber_id?'H'.$item_vinnumber_id:'' ?></td>
			<td style="text-align:left;"><?php echo $item->year.' '.$item->make_name.' '.$item->model_name.' '.$name.' '.$ppl.' '.$item_desc.$item_return_desc; ?></td>
			<td style='text-align:center;'><?php echo $item->quantity_purchased; ?></td>
			<!-- <td><?php echo $item->ppl_id;?></td> -->
			<td><?php echo to_currency($item->return_restock); ?></td>
			<td style='text-align:center;'><?php echo to_currency($item->discount_percent); ?></td>
			<td style='text-align:right;'><?php echo to_currency($sub_total); ?></td>
		</tr>
	<?php else: ?>
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $barcode ?></td>
			<td><?php echo $item_vinnumber_id?'H'.$item_vinnumber_id:'' ?></td>
			<td style="text-align:left;"><?php echo $item->year.' '.$item->make_name.' '.$item->model_name.' '.$name.' '.$ppl.' '.$item_desc; ?></td>
			<td style='text-align:center;'><?php echo $item->quantity_purchased; ?></td>
			<!-- <td><?php echo $item->ppl_id;?></td> -->
			<td><?php echo to_currency($item->item_unit_price); ?></td>
			<td style='text-align:center;'><?php echo to_currency($item->discount_percent); ?></td>
			<td style='text-align:right;'><?php echo to_currency($sub_total); ?></td>
		</tr>
	<?php endif ?>	
		

	<?php
	$total+=$sub_total;
	}
	?>
	</tbody>
	<tfoot>
		<?php
			$rowspan = 4;
			// if ($return_amount>0) {
			// 	$rowspan = 5;
			// }
		?>
	<tr>
		
	<td rowspan="<?php echo $rowspan ?>" style="vertical-align: top;">
		Note :
	</td>
	<td colspan="3" rowspan="<?php echo $rowspan ?>" style="text-align: left;">
		<textarea rows="4" style="width: 50%;resize: none;border: none;" maxlength="150" readonly=""><?php echo $this->config->item('sale_note') ?></textarea>
	</td>
	<td colspan="3"style='text-align:right;font-weight:bold;border:1px solid #ddd;'><?php echo $this->lang->line('sales_sub_total'); ?></td>
	<td colspan="3" style='text-align:right;border:1px solid #ddd;'><?php echo to_currency($total); ?></td>
	</tr>

	<?php foreach($taxes as $name=>$value) { ?>
		<!-- <tr>
			<td colspan="7" style='text-align:right;'><?php echo $name; ?>:</td>
			<td colspan="3" style='text-align:right;'><?php echo to_currency($value); ?></td>
		</tr> -->
	<?php }; ?>

	<tr>
	<!-- <td colspan="4"></td> -->
	<td colspan="3" style='text-align:right;font-weight:bold;border:1px solid #ddd;'><?php echo $this->lang->line('sales_total'); ?></td>
	<td colspan="3" style='text-align:right;border:1px solid #ddd;background: #ddd;font-weight: bold;'><?php echo to_currency($total); ?></td>
	</tr>


	<?php
		// var_dump($payment);
		$first_payment= (float)filter_var($sale_pay_amount, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION); 
		$paid=$first_payment+$payment->payment_amount; //luy pay hy
		
		$due = 0; //Luy nov jpeak
		$change = 0;	//luy ab

		

		

		
		

	?>
	<?php if ($paid>0): ?>
		<tr>
			<!-- <td colspan="4"></td> -->
			<td colspan="3" style="text-align:right;font-weight:bold;border:1px solid #ddd;"><?php echo $this->lang->line('sales_paid'); ?></td>
			<td colspan="3" style="text-align:right;border:1px solid #ddd;"><?php echo to_currency($paid); ?>  </td>
	    </tr>
	<?php endif ?>
	
	

    <!-- <tr><td colspan="10">&nbsp;</td></tr>  -->
    <?php
    	if ($paid<0) {
    		$paid=0;
    	}
    	if ($paid>$total) {
			$change = $paid - $total;
		}
		if ($paid<$total) {
			$due = $total - $paid;
		}
    ?>
    <?php if ($due>0): ?>
    	<tr>
			<!-- <td colspan="4"></td> -->
			<td colspan="3" style='text-align:right;font-weight:bold;border:1px solid #ddd;'>Amount Due</td>
			<td colspan="3" style='text-align:right;border:1px solid #ddd;'><?php echo  to_currency(abs($due)); ?></td>
		</tr>
    <?php endif ?>
    <?php if ($change>0): ?>
    	<tr>
			<!-- <td colspan="4"></td> -->
			<td colspan="3" style='text-align:right;font-weight:bold;border:1px solid #ddd;'>Change</td>
			<td colspan="3" style='text-align:right;border:1px solid #ddd;'><?php echo  to_currency(abs($change)); ?></td>
		</tr>
    <?php endif ?>
	
	
	
	</tfoot>

	</table>

	<!-- <div id="sale_return_policy">
	<?php echo nl2br($this->config->item('return_policy')); ?>
	</div> -->
	<div class="footer_signature_wrapper" align="center">
		<div class="footer_signature">
			<div class="left_signature">
				<label>Customer's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<div class="right_signature">
				<label>Seller's Signature</label>
				<div class="underline">
					
				</div>
			</div>
		</div>
	</div>
	
	
</div>


<div style="clear:both"></div>
<?php $this->load->view("partial/footer"); ?>

<?php if ($this->Appconfig->get('print_after_sale'))
{
?>
<script type="text/javascript">
$(window).load(function()
{
	window.print();
	localStorage.setItem('selling',1);

	// alert(s);
});

$('#btn_new_sale').click(function(){
	location.href = "<?php echo site_url('sales/new_sale') ?>";
});
$('#btn_edit_sale').click(function(){
	// alert('Under Construction...');
	location.href = "<?php echo site_url('sales/edit_sale/'.$sale_id) ?>";
});

$('#btn_print').click(function(){
	window.print();
});
</script>
<?php
}
?>

<script type="text/javascript">
	generateBarcode();
	function generateBarcode(){
        var value = "<?php echo $invoice_id ?>";
        var btype = 'code39';

        var settings = {
          output:'css',
          barWidth:1,
          barHeight: 25,
         
        };
         
         $("#barcode").html("").show().barcode(value, btype, settings);
        
      }
</script>