<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">

<div class="panel panel-default">
  <!-- Default panel contents -->
  	<!-- <div class="panel-head">
  		<div class="row">
  			<div class="col-md-12">
  				<label for="">Deposit No:</label><?php echo $dep_info->deposit_id ?>
  			</div>
  		</div>
  	</div> -->
  	<div class="panel-body">
    	<div class="row">
    		<form action="<?php echo site_url('sales/do_cancel_deposit') ?>" onsubmit="return confirm('Confirm Cancel Deposit ?')" method="post">
	    		<div class="col-md-12 form-group">
	    			<label for="restock_amount">Restock Amount</label>
	    			<input 
	    				type="number" 
	    				name="restock_amount" 
	    				class="form-control restock_amount" 
	    				value="0" 
	    				required="" 
	    				onkeypress='return isNumberKey(event)'
	    				onclick="select()"
	    				max = <?php echo $total_amount ?>
	    				>
	    		</div>
	    		<div class="col-md-12 form-group">
	    			<label for="cancel_deposit_note">Note</label>
	    			<textarea name="cancel_deposit_note" id="" cols="30" rows="10" class="form-control" required=""></textarea>
	    		</div>
	    		<div class="col-md-12 form-group">
	    			
	    			<input type="submit" class="btn btn-primary pull-right" value="Submit" >
	    		</div>
    		</form>
    	</div>
	</div>


<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script>
	function isNumberKey(evt){
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57))
	        return false;
	    return true;
	}

</script>