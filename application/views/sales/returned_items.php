<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>"> -->

<div class="panel panel-default">
  <!-- Default panel contents -->
  	<!-- <div class="panel-heading">
  		Returned Items
  	</div> -->
  	<div class="panel-body">
    	<div class="col-md-12">
			<table class="table table-bordered table-striped tdata">
				<thead>
					<th>Nº</th>
					<th>Barcode</th>
					<th>Head #</th>
					<th>Description</th>
					<th>Price</th>
					<th>Restock</th>
					<!-- <th>Subtotal</th> -->
				</thead>
				<tbody>
                    <td colspan="6" class="dataTables_empty">No Data</td>
					
				</tbody>
				<tfoot>
					<!-- <tr>
						<td colspan="5" align="right"><b>Total</b></td>
						<td></td>
					</tr> -->
				</tfoot>
			</table>
		</div>
	</div>

  
</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>


<script type="text/javascript">
	var oTable;
	$(document).ready(function(){
		get_data();
	});

	$('.btn_search').click(function(){
		// $('.tdata tbody').html('');
		// $('.tdata tfoot').html('');

		oTable.fnDestroy();
		// console.log(oTable);
		get_data();
	})

	function get_data(){
		

		

		$('.loading_box').fadeIn();
		
		$.ajax({
			url:"<?php echo site_url('sales/get_return_items') ?>",
			type:'post',
			dataType:'json',
			data:{
				return_id:"<?php echo $return_id ?>"
			},
			success:function(oh){
				// oh = JSON.parse(oh);
				// console.log(oh.tbody);
				// oTable.DataTable().destroy();

				// if (oh.tbody) {

				$('.tdata tbody').html(oh.tbody);
				// $('.tdata tfoot').html(oh.tfoot);	

					oTable = $('.tdata').dataTable({
						destroy: true,
						"lengthMenu": [ [10, 30, 50, 100, -1], [10, 30, 50, 100, "All"] ],
						"iDisplayLength": 50,
						"bFilter":true,
						// aoData:oh.tbody
						"footerCallback": function ( row, data, start, end, display ) {
			            var api = this.api(), data;
			 			
			            // Remove the formatting to get integer data for summation
			            var intVal = function ( i ) {
			                return typeof i === 'string' ?
			                    i.replace(/[\$,]/g, '')*1 :
			                    typeof i === 'number' ?
			                        i : 0;
			            };

		
			            
			            pTotalD = api
			                .column( 5, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 5 ).footer() ).html('$ '+numberWithCommas(pTotalD.toFixed(2)));

			        }
					});

				// }else{
				// 	$('.tdata tbody').html('');
				// 	$('.tdata tfoot').html('');
				// }

				$('.loading_box').fadeOut();

				
			}
		});
	}

	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}


	$('.from_date,.to_date').datepicker({
		format:'dd-mm-yyyy',
		todayHighlight:true
	});
	$('.from_month,.to_month').datepicker({
		format: "mm-yyyy",
	    viewMode: "months", 
	    minViewMode: "months"
	});
	$('.from_year,.to_year').datepicker({
		format: "yyyy",
	    viewMode: "years", 
	    minViewMode: "years"
	});
	// $('.filter_user,.filter_period,.filter_customer').select2();

	$('.filter_period').change(function(){
		var per = $('.filter_period').val();
		show_detail_filter(per);
	});
	show_detail_filter('Daily');
	
	function show_detail_filter(period){
		var d = $('.daily_div');
		var m = $('.monthly_div');
		var y = $('.yearly_div');

		d.addClass('hide');
		m.addClass('hide');
		y.addClass('hide');
		d.find(':input').val('');
		m.find(':input').val('');
		y.find(':input').val('');
		if (period=='Daily') {
			d.removeClass('hide');
		}
		if (period=='Monthly') {
			m.removeClass('hide');
		}
		if (period=='Yearly') {
			y.removeClass('hide');
		}
	}
</script>