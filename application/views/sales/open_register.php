<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<style type="text/css">
	body{
		background: white;
	}
</style>

<div class="col-md-12 alert alert-danger">
		<?php echo $this->session->flashdata('msg') ?>
</div>
<div class="col-md-12">
	<div id="title_bar">
		<div id="title">
		 	<?php echo $controller_name; ?>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="well">
	<div class="row">
		<div class="col-md-12">
			<form method="post" action="<?php echo site_url('sales/do_open_register') ?>">
			<div class="col-md-3 form-group">
				<label>Cash in hand</label>
				<input type="text" onkeypress="return isNumber(event)" name="open_cash" class="form-control"> 
			</div>
			<div class="col-md-12">
				<input type="submit" class="btn btn-primary" value="Open Register">
			</div>
			</form>

		</div>
	</div>
	
</div>


<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>

<?php $this->load->view("partial/footer"); ?>