
<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">

<?php 
// echo $this->session->userdata('old_return_payment');
// var_dump('E = '.$this->session->userdata('edit_sale_id')); 
// var_dump('Q = '.$this->session->userdata('q_id')); 
// var_dump('D = '.$this->session->userdata('d_id')); 
// var_dump($cart);

// echo date('d/m/Y H:i:s');
// $str = '170530-100057';

// echo substr($str,11)
// var_dump($payments);

// var_dump($cart);
// var_dump($this->sale_lib->get_all_payment());

// $is_admin  = $this->session->userdata('user_info')->is_admin;
// var_dump($this->session->userdata('user_info'));
$old_payments  = $this->session->userdata('old_payment');

// $new_payment = $this->session->userdata('payments');
// $all_payment = array_merge($old_payments,$new_payment);
// var_dump($new_payment);

// var_dump($is_admin);

if($this->uri->segment(2) && $this->uri->segment(2) != 'select_name' && $this->uri->segment(1)=='sales'){
	redirect(site_url('sales'));
}
if ($this->uri->segment(2) && $this->uri->segment(1)=='returns') {
	redirect(site_url('returns'));
	
}
?>

<style type="text/css">
	.problem td{
		background: #ffcccc !important;
	}
	.prot td{
		background:rgba(224, 0, 0, 0.64) !important;
	}
	.td_p2{
		visibility: visible; !important;
	}
	.hidden{
		display: none !important;
	}
	.select_cus{
		background: rgb(238, 238, 238) none repeat scroll 0% 0%;
		vertical-align: middle;
		border: 1px solid rgb(204, 204, 204);
		border-radius: 5px;
		margin-top: 1%;
		padding: 1%;
		padding-right: 0.3%;
		height: 25px;
	}
	.btn_select_cus{
		height: 30px;
		margin-left: 30px;
		margin-top: -1px;
	}
	.sale_detail_box{
		width: 47%;
	    float: right;
	    border: 1px solid rgb(204, 204, 204);
	    padding: 1%;
	    max-height: 400px;
	}
	.sale_payment_box{
		float: left;
		width: 47%;
		border: 1px solid rgb(204, 204, 204);
		padding: 1%;
		max-height: 400px;
	}
	#new_i_input{
		width: 100px;
		border: 1px solid rgb(170, 170, 170);
		border-radius: 5px;
		height: 20px;
		color: rgb(68, 68, 68);
	}
	#new_i_input_ta{
		/*width: 50px;*/
		border: 1px solid rgb(170, 170, 170);
		border-radius: 5px;
		height: 50px;
		color: rgb(68, 68, 68);
	}
	#savediv{
		cursor: pointer;
		float: right;
	}
	table.fixed
	{
	  table-layout:fixed;
	}
	table.fixed td,th
	{ 
	  word-wrap:break-word;
	}
	#price{
		width: 85%;
	}

	.datepicker{
		position: absolute;
		background: white;
	}
</style>
<?php 
$qid = $this->session->userdata('q_id');
$did = $this->session->userdata('d_id');
$edit_sale_id = $this->session->userdata('edit_sale_id');

$page_title = $this->lang->line('sales_register');
if ($qid!='') {
	$page_title = "Quote No. ".$qt_number;
}
if ($did!='') {
	$dno = $this->db->query("SELECT deposit_id FROM ospos_sales_suspended WHERE sale_id = $did")->row()->deposit_id;
	$page_title = "Deposit No. ".$dno;
}


if ($mode=='sale') {
	?>
		<?php if ($qid OR $did OR $edit_sale_id): ?>
			<div id="page_title" style="margin-bottom:8px;">
				<a href="<?php echo site_url('sales/new_sale') ?>" onclick="return confirm('You will lose all current Data!!! Continue ?')">
					<div class='small_button'>
						<span style='font-size:73%;'>New Sale</span>
					</div>
				</a>
				
			</div>

			
		<?php endif ?>
		<div class="top_option_left">
			
		</div>
		<div class="top_option_right">
			<div id="close_register" class="sale_menu float_right">
				<a href="<?php echo site_url('sales/close_register') ?>">
					<div class='small_button'>
						<span>Close Register</span>
					</div>
				</a>
			</div>
			<div id="show_suspended_sales_button" class="sale_menu float_right">
				<?php echo anchor("sales/suspended/width:425",
				"<div class='small_button'><span>View Deposit</span></div>",
				array('class'=>'thickbox none','title'=>'Deposit'));
				?>
			</div>
			<div id="show_quoted_sales_button" class="sale_menu float_right">
				<?php echo anchor("sales/quoted_sale/width:425",
				"<div class='small_button'><span>View Quotations</span></div>",
				array('class'=>'thickbox none','title'=>'Quoted Sale'));
				?>
			</div>

			<?php if ($is_admin==1): ?>
				<div id="list_sale_button" class="sale_menu float_right">
					<?php echo anchor("sales/list_sales",
					"<div class='small_button'><span>List Sales</span></div>",
					array('title'=>'View All Sales'));
					?>
				</div>
			<?php endif ?>

			<div id="show_suspended_sales_button" class="sale_menu float_right">
				<?php echo anchor("sales/view_transaction/width:700",
				"<div class='small_button'><span>View Transaction</span></div>",
				array('class'=>'thickbox none','title'=>'Transaction'));
				?>
			</div>
			
			
			
			
		</div>
		

		<div id="page_title" style="margin-bottom:8px;"><?php echo $page_title; ?></div>


	<?php
}else{
	?>
	<div id="page_title" style="margin-bottom:8px;">Return Register</div>
	<?php
}
 ?>
<?php
$error = $this->session->flashdata('err_message');
$warning = $this->session->flashdata('warn_message');
$success = $this->session->flashdata('suc_message');
if($error)
{
	echo "<div class='error_message'>".$error."</div>";
}

if ($warning)
{
	echo "<div class='warning_mesage'>".$warning."</div>";
}

if ($success)
{
	echo "<div class='success_message'>".$success."</div>";
}
?>


<!-- <?php var_dump($cart) ?> -->
<div id="register_wrapper">
<?php if ($mode=='sale'): ?>
	<?php echo form_open("sales/change_mode",array('id'=>'mode_form')); ?>
	<span><?php echo $this->lang->line('sales_mode') ?></span>
<!-- <?php echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"'); ?> -->

</form>
<?php endif ?>
<div style="float:left;margin-left:25px;display:none;" class="select_btn">
	<a href="<?php echo site_url('khmernames/view_all_khmername') ?>" class="thickbox" title="Select Name">
		<div class="small_button">
			<span name="btnselectname" id="btnselectname" style="font-size:10px;">Select Name</span>
		</div>
	</a>
</div>
<div class="select_cus">
	<?php $cus_id = $this->session->userdata('customer') ?>

	<input type="hidden" value="<?php echo $cus_id!=-1?$cus_id:72 ?>" id="customer_id" >

	<?php
	if(isset($customer))
	{?>
	<?php
		echo $this->lang->line("sales_customer_name").': <b>'.$customer. '</b><br/>';
		echo $this->lang->line("sales_customer_phone").': <b>'.$customer_phone. '</b><br/>';
		echo $this->lang->line("sales_customer_address").': <b>'.$customer_address. '</b><br />';
		if (!$did && $mode!='return') {
			echo anchor("sales/remove_customer",'['.$this->lang->line('common_remove').' '.$this->lang->line('customers_customer').']');
			
		}
	?>
	<?php
	}
	else
	{
		if ($mode=='sale') {
			echo form_open("sales/select_customer",array('id'=>'select_customer_form')); ?>
			<!-- <label id="customer_label" for="customer"><?php echo $this->lang->line('sales_select_customer'); ?></label>
			<?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'23','value'=>$this->lang->line('sales_start_typing_customer_name')));?> -->
			<div style="float:left">
				<label id="customer_label">Customer Type</label>
				<select name="cus_type" id="cus_type" width="150px">
					<option value="walk">Walk-in Customer</option>
				</select>
			</div>
			
			<div style="float:left;margin-left:25px;" class="select_btn">
				<a href="<?php echo site_url('customers/view_all_customer') ?>" class="thickbox" title="Select Customer">
					<div class="small_button">
						<span name="btnselectcus" id="btnselectcus" style="font-size:10px;">Select Customer</span>
					</div>
				</a>
			</div>
			<!-- <div style="float:left;margin-left:25px;width:282px" class="walk_cus hidden">
				<input type="text" name="customer_name" id="customer_name" placeholder="Name" required>
				<div id="name_msg" style="width:180px;color:red;" class="hidden">
					* Name Required
				</div>
				<input type="text" name="customer_phone1" id="customer_phone1" placeholder="Phone 1" required>
				<div id="phone_msg" style="width:180px;color:red;" class="hidden">
					* Phone Required
				</div>
				<input type="text" name="customer_phone2" id="customer_phone2" placeholder="Phone 2">
				<div id="savediv">
					<div class="small_button" style="float:right">
						<span name="btnsavecus" id="btnsavecus" style="font-size:10px;">Save</span>
					</div>
				</div>
			</div> -->
		<!-- <div style="clear:both"></div> -->
			

			
		<!-- <label style="margin: 5px 0 5px 0"><?php echo $this->lang->line('common_or'); ?></label> -->
		<?php echo anchor("customers/view/-1/width:350",
		"<div class='small_button' style='margin:0 auto;float:right;'><span>".$this->lang->line('sales_new_customer')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_customer')));
		?>
		</form>
		<!-- <div style="margin-top:5px;text-align:center;"> -->
		
		<!-- </div> -->
		<!-- <div class="clearfix">&nbsp;</div> -->
		<?php
		}else{
			echo "No Customer";
		}
		?>
	
		<?php
	}
	
	?>
	<?php if ($mode=='sale'): ?>

		<?php if ($this->config->item('sale_select_date')==1): ?>
			<div class="sale_date_holder">.
				<label style="font-weight: bold;">Sale Date</label>
				<input type="text" onochange="equantity(event);" name="sale_date" class="sale_date_input datetime" id="sale_date_input" value="<?php echo $sale_date ?>">
			</div>
		<?php endif ?>
		
	<?php endif ?>

		
	
	
	
</div>
<?php
if ($mode=='return') {
		?>
		<div style="width: 100%;background: #EEEEEE;border: 1px solid #ccc;margin-top: 5px;border-radius: 5px;height: 40px;padding: 0px 0;">
			
			<div style="float:right;margin-left:25px;margin-top: 5px;margin-right: 5px;">
				<a target="_blank" href="<?php echo site_url('sales/view_all_return_items') ?>" title="Return Items">
					<div class="small_button" style="background-size: 125px 30px;width: 125px;">
						<span name="btnviewreturn" id="btnviewreturn" style="font-size:10px;">View Returned Itmes</span>
					</div>
				</a>
			</div>
		</div>

		<?php
	}
?>
<?php 
if ($mode=='sale') {
	echo form_open("sales/add",array('id'=>'add_item_form'));
}else{
	echo form_open("returns/add_return_item/",array('id'=>'add_item_form'));
}
?>
<label id="item_label" for="item">

<?php
$problem='';
$chkpro="";

if($mode=='sale')
{
	echo $this->lang->line('sales_find_or_scan_item');
}
else
{
	echo $this->lang->line('sales_find_or_scan_item_or_receipt');
	$problem = "<th style='width:11%;'>".$this->lang->line('sales_return_item_problem')."</th>";

}
?>
</label>


<?php
if ($mode=='sale') {
echo form_input(array('name'=>'item','id'=>'item','size'=>'40'));

}else{
echo form_input(array('name'=>'item_returns','id'=>'item_returns','size'=>'40'));

}
?>

<!-- <div id="new_item_button_register" >
		<?php 
		//New 02-08-2012
		echo anchor("items/view_sale_item/-1/width:360",
		"<div class='small_button'><span>".$this->lang->line('sales_new_item')."</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line('sales_new_item')));
		?>
	</div> -->

</form>
<style type="text/css">
	#content_area{
		width: 95%;
		/*padding:1%;*/
	}
	/*#disc{
		width: 80px;
	}
	#quantity{
		width: 80px;
	}*/

</style>

<?php 
$disable = '';
if (count($cart)==0) {
	$disable = 'disabled';
}
 ?>

<!-- <?php var_dump($this->session->userdata) ?>; -->
<table id="register" cellpadding="5" style="width:100%;;">
<thead>
<tr>
<!-- <th><?php echo $this->lang->line('common_delete'); ?></th> -->
<?php 
	if ($mode=='sale'){
?>
	<th id="remove_all_item"><input type="button" name="remove_all_item" value="Remove All" onclick="removeall()" <?php echo $disable ?>></th>
	<th id="remove_sel_item"><input type="button" name="remove_sel_item" value="Remove Selected" onclick="removesel()"></th>
	<th>No</th>
	<th style="text-align:center"><?php echo $this->lang->line('sales_item_vin'); ?></th>
<?php }elseif ($mode=='return') {
?>
	<th>No</th>

<?php
} ?>

<th><?php echo $this->lang->line('sales_item_number'); ?></th>
<th><?php echo $this->lang->line('sales_item_year'); ?></th>
<th><?php echo $this->lang->line('sales_item_make'); ?></th>
<th><?php echo $this->lang->line('sales_item_model'); ?></th>
<th><?php echo "Part Name"; ?></th>
<th><?php echo $this->lang->line('sales_item_color'); ?></th>

<!-- <th><?php echo $this->lang->line('sales_quantity'); ?></th> -->
<th><?php echo $this->lang->line('sales_item_part_placement'); ?></th>
<th><?php echo $this->lang->line('sales_price'); ?></th>
<th><?php echo $mode=='return'? 'Restock': $this->lang->line('sales_discount'); ?></th>
<th><?php echo $this->lang->line('sales_total'); ?></th>
<th>Note</th>



<!-- <th style="width:25%;"><?php echo $this->lang->line('sales_item_name'); ?></th> -->
<!-- <th style="width:25%;"><?php //echo "Make"; ?></th> -->
<!-- <th style="width:25%;"><?php echo "Model"; ?></th> -->
<!-- <th style="width:25%;"><?php echo "Part Placement"; ?></th> -->
<?php echo $problem ?>
<th><?php echo $this->lang->line('sales_action'); ?></th>

</tr>
<?php  
if ($mode=='sale') {
echo form_open("sales/add_new_item",array('id'=>'add_new_item_form'));
$new_item_info = $this->db->query("SELECT * FROM ospos_items_from_sale WHERE item_id=(SELECT MAX(item_id) FROM ospos_items_from_sale)")->row();
// $i_name = $new_item_info->name;
$i_name = '';
$i_make = $new_item_info->make_id;
$i_model = $new_item_info->model_id;
$i_part = $new_item_info->partplacement_id;
$i_color = $new_item_info->color_id;
$i_qty = $new_item_info->quantity;
$i_price = $new_item_info->unit_price;
$i_desc = $new_item_info->description;
$i_vin = $new_item_info->category;
$i_khmername = $new_item_info->khmername_id;
?>
	<tr style="border-bottom: 4px solid white;">
	<td><h6>New Item</h6></td>

	<td></td>

	<td>
		<select name="s_vin_number" id="s_vin_number" tabindex="0">
			<option value="">Please Select</option>
			<?php 
				
				$vin=$this->db->query("SELECT * FROM ospos_vinnumbers WHERE deleted=0")->result();
				foreach ($vin as $v) {
					$sel='';
					// if($i_vin==$v->vinnumber_name)
     //                    $sel='selected';
					echo "<option value='$v->vinnumber_name' $sel>H$v->vinnumber_id</option>";

				}
			 ?>
		</select>
	</td>
	<td></td>
	<td>
		<?php 
                    define (MINY,1980);
                    $year_temp = $new_item_info->year;
                    // var_dump($year_temp);
                ?>
                <select name="s_year" id="year" style="width:100px;" tabindex="0">
						<!-- <option value="<?php echo $year_temp; ?>"><?php echo $year_temp; ?></option> -->
                    <?php 
						for($j=date('Y');$j>=MINY;$j--){
                            $sel='';
                            if($year_temp==$j)
                                $sel='selected';
                            // echo $year_temp;
							echo '<option value="'.$j.'" '.$sel.'>'.$j.'</option>';
						}
                    ?>                    
                </select>
	</td>
	<td>
        <select name="s_make_id" id="s_make_id" class="clmake" onchange="getmodels();" tabindex="0">
                        <option value="">Please Select</option>
                        <?php
                        $make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0")->result();
                        foreach ($make as $m) {
                            $sel='';
                            if(isset($i_make))
                                if($i_make==$m->make_id)
                                    $sel='selected';
                            echo "<option value='$m->make_id' $sel>$m->make_name</option>";
                        }
                         ?>
                         <!-- <option>Hello</option> -->
                    </select>
	</td>
	<td>
	<input type="hidden" id="last_model_id" value="<?php echo $new_item_info->model_id ?>">
			<select name="s_model_id" id="s_model_id" class="s_model_id" style="width:110px" tabindex="0">

                        <!-- <option value="">Please Select</option>
                        <?php
                         if(isset($new_item_info->make_id)){
                            $where="";
                            if($new_item_info->make_id!='')
                                $where=" AND make_id='$new_item_info->make_id'";
                                $model=$this->db->query("SELECT * FROM ospos_models WHERE deleted=0 {$where}")->result();
                                foreach ($model as $mo) {
                                    $sel='';
                                    if($new_item_info->model_id==$mo->model_id)
                                            $sel='selected';
                                    echo "<option value='$mo->model_id' rel='$mo->make_id' $sel>$mo->model_name</option>";
                                }
                         }
                         ?> -->
                    </select>
	</td>
	
	
	<!-- <td>
        <?php echo form_dropdown('s_partplacement_id', $partplacements, $selected_partplacement,'class="partplacement_id"');?>
	</td> -->
	<td>
		<div id="select_name_div">
			<select id="name_select" name="s_name_select" tabindex="0">
				<option value="">Please Select</option>
				<?php
					if ($sale_select_name->khmername_id!='') {
						echo "<option value='$sale_select_name->khmername_id' selected>$sale_select_name->khmername_name</option>";
					}
				?>
				<option value="select">Select Name</option>
				<option value="other">Other</option>
			</select>
		</div>
		<span id="close_name" class='hidden' style='position:absolute;cursor:pointer;margin-left: 105px;top: 63px;'>&times</span>
		<input type='text' class='hidden' id='new_i_input' name="s_name" value="<?php echo isset($i_name)? $i_name:'' ?>">
	</td>
	<td>
	<select name="s_color_id" id="s_color_id" class="color_id" tabindex="0">
                        <option value="">Please Select</option>
                        <?php
                        $color=$this->db->query("SELECT * FROM ospos_colors WHERE deleted=0")->result();
                        foreach ($color as $c) {
                            $sel='';
                            if(isset($i_color))
                                // if($i_color==$c->color_id)
                                //     $sel='selected';
                            echo "<option value='$c->color_id' $sel>$c->color_name</option>";
                        }
                         ?>
                         <!-- <option>Hello</option> -->
                    </select>
	</td>
	<!-- <td>
		<?php echo form_dropdown('s_color_id', $colors, $selected_color,'class="color_id"');?>
	</td> -->
	<input tabindex="0" onclick="select();" type='hidden' style="width:80px;" id='new_i_input' name="s_quantity" readonly value="<?php echo 1;//echo isset($i_qty)?$i_qty:'' ?>">
	<td>
		<input tabindex="0" type='text' style="width:60px;" id='pplacement' class="pplacement" name="s_partplacement_id" value="<?php //echo isset($i_part)?$i_part:'' ?>" >
	</td>
	<td><input tabindex="0" size="2" onclick="select();" type='text' id='new_i_input_price' name="s_price" value="<?php //echo isset($i_price)?$i_price:'' ?>"></td>
	
	<td></td>
	<td></td>
	<td><textarea tabindex="0" style="width:60px;" id='new_i_input_ta' name="s_desc"><?php //echo isset($i_desc)?$i_desc:'' ?></textarea></td>
	
	
	<?php 
		if ($problem) {
			echo "<td></td>";
		}
	 ?>
	<td><input tabindex="0" type='button' value="Add" id="add_new_item_btn"></td>

</tr>
</form>
<?php } ?>
</thead>

<!-- CART ITEMS -->
<tbody id="cart_contents">
<?php
// var_dump($cart);
if(count($cart)==0)
{

?>
<tr><td colspan='17'>
<div class='warning_message' style='padding:7px;'><?php echo $this->lang->line('sales_no_items_in_cart'); ?></div>
</tr>
<?php
}
else
{
	$i=0;
	foreach($cart as $line=>$item)
	{
		$i++;
		
		$cl='';
		if ($item['is_pro']!=0) {
			$cl = "problem";
		}
		echo form_open("sales/edit_item/$line");
	?>
		<tr id="pro_td" class="<?php echo $cl?>">
		<?php if ($mode=='sale'): ?>
		<td><input type="checkbox" value="<?php echo $line ?>" name="del_check" id="del_check" onclick="dval()"></td>
			
		<?php endif ?>

		<td><?php echo $i ?></td>
		<!-- <td><?php echo anchor("sales/delete_item/$line",'['.$this->lang->line('common_delete').']');?></td> -->
		<?php if ($mode=='sale'): ?>
		<td><?php echo $item['vin_num'] ?></td>
			
		<?php endif ?>
		<td><?php echo $item['barcode'];//echo $cur_item_info->barcode; ?></td>
		<td><?php echo $item['year']?></td>
		<!-- <br /> [<?php echo $item['cur_quantity']; ?> in stock] -->
		<td><?php echo $item['makename'];?></td>
		<td><?php echo $item['modelname'];?></td>
		<?php 
			// $khmername = $this->db->where('khmername_id',$item['khmername_id'])->from('khmernames')->get()->row()->khmername_name;
		 ?>
		<td style="align:center;"><?php echo $item['name']; ?></td>
		
		<td><?php echo $item['color_name'] ?></td>

		<!-- <td></td> -->
		<!-- <td></td> -->
		<?php
			echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'1','id'=>'quantity','onclick'=>'edit_qty(event);','onchange'=>'equantity(event);','type'=>'hidden'));
		?>
		<!-- <td>
		<?php
        	if($item['is_serialized']==1)
        	{
        		echo $item['quantity'];
        		echo form_hidden('quantity',$item['quantity']);
        	}
        	else
        	{
        		echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'1','id'=>'quantity','onclick'=>'edit_qty(event);','onchange'=>'equantity(event);'));
        	}
		?>
		</td> -->
		<td><?php echo $item['partplacement_name'];?></td>
		<?php if ($items_module_allowed)
		{
		?>
			<?php if ($mode=='return'): ?>
				<td><?php echo form_input(array('name'=>'price','readonly'=>'readonly','value'=>$item['price'],'size'=>'2','id'=>'price','onclick'=>'edit_price(event);','onchange'=>'equantity(event);'));?></td>
				
			<?php else: ?>
				<td><?php echo form_input(array('name'=>'price','value'=>$item['price'],'size'=>'2','id'=>'price','onclick'=>'edit_price(event);','onchange'=>'equantity(event);'));?></td>

			<?php endif ?>
		<?php
		}
		else
		{
		?>
			<td><?php echo $item['price']; ?></td>
			<?php echo form_hidden('price',$item['price']); ?>
		<?php
		}
		?>

		

		<td style="padding-left: 12px;">
			
			<?php if ($mode=='return'): ?>
				<?php echo form_input(array('name'=>'discount',
										'value'=>$item['discount'],
										'size'=>'2',
										'id'=>'disc',
										'onchange'=>'ereturn(event)',
										'onclick'=>'$(this).select()',
										'onkeypress'=>'return isNumberKey(event)',
										'max'=>$item['price']


										)
								);?>
			<?php else: ?>
				<?php echo form_input(array('name'=>'discount',
										'value'=>$item['discount'],
										'size'=>'2',
										'id'=>'disc',
										'onclick'=>'edit_disc(event);',
										'onchange'=>'equantity(event);'
										)
								);?>
			<?php endif ?>
				
		</td>
		
		<!-- <td><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td> -->
		<!-- <td><?php echo to_currency($item['price']*$item['quantity']-$item['discount']) ?></td> -->

		<?php if ($mode=='return'): ?>
		<td id="total_row"><?php echo to_currency($item['price']*$item['quantity']-$item['discount']) ?></td>

		<td><textarea readonly name="desc" id="desc" style="width:60px" onchange="equantity(event);"><?php echo $item['description'] ;?></textarea></td>
		<?php else: ?>
		<td id="total_row"><?php echo to_currency($item['price']*$item['quantity']-$item['discount']) ?></td>

		<td><textarea name="desc" id="desc" style="width:60px" onchange="equantity(event);"><?php echo $item['description'] ;?></textarea></td>

		<?php endif ?>
		<!-- <td><textarea name="desc" id="desc" style="width:60px" onchange="equantity(event);"><?php echo $item['description'] ;?></textarea></td> -->
		<?php if ($mode=='sale'): ?>
			<td></td>
			
		<?php endif ?>
		<td style="display:none;"><input name="is_new" id="is_new" type="hidden" value='<?php echo $item['is_new']?>'></td>

		<?php  
			if($mode=='return')
			{
			// 	$chk="";
			// 	$chk2='';
			// 	$cla2='hidden';
			// 	$cla='';
			// 	$check= '';
			// 	$pro2 = '';
			// 	$is_re = '';
			// 	// $val='';
			// 	if ($item['is_pro']!=0) {
			// 		$chk="checked";
					
			// 		if($item['is_pro']==1 || $item['is_pro']==2){
			// 			$cla = 'hidden';
			// 			if ($item['is_pro']==2) {
			// 				$check='checked';
			// 				$cl='prot';
			// 			}
			// 			$cla2="td_p2";
			// 		}
			// 	}
				$is_re = '';

				if ($item['is_return']!=0) {
					$is_re = 'checked';
				}
			
			// 	echo "<td id='td_p' class='".$cla."'><input name='pchk' id='pchk' type='checkbox' $chk onclick='chkVal(event);'>";
   //      		echo "<input type='hidden' name='is_pro' value='".$item['is_pro']."' id='pchkval'></td>";
   //      		echo "<td id='td_p2' class='".$cla2."'><input name='pchk2' id='pchk2' type='checkbox' $check onclick='chkVal2(event);'>
			// 		<input type='hidden' name='is_pro2' value='".$item['is_pro']."' id='pchkval2'></td>";
			// 	echo "<td><input type='checkbox' value='".$line."' name='return_check' id='return_check' onclick='rval()' onchange='ereturn(event)' ".$is_re.">Return</td>";
			}
			
			// $i++;
		?>

		<!-- <td><input name="is_pro" id='pchkval' type="hidden" value=""></td> -->
		<?php if ($mode=='return'): ?>
			<?php
				$rpo0 = '';
				$rpo1 = '';
				$rpo2 = '';
				if ($item['is_pro']==0) {
					$rpo0 = 'checked';
				}
				if ($item['is_pro']==1) {
					$rpo1 = 'checked';
				}
				if ($item['is_pro']==2) {
					$rpo2 = 'checked';
				}
			?>
			<td style="text-align: left;padding-left: 40px;">
				<input type="radio" name="problem" id="rapro" value="0" <?php echo $rpo0 ?> onchange="ereturn(event)" > None <br>
				<input type="radio" name="problem" id="rapro" value="1" <?php echo $rpo1 ?> onchange="ereturn(event)"> Broken <br>
				<input type="radio" name="problem" id="rapro" value="2" <?php echo $rpo2 ?> onchange="ereturn(event)"> Not Working

			</td>
			<td>
				<input type='checkbox' value='<?php echo $line ?>' name='return_check' id='return_check' onclick='rval()' onchange='ereturn(event)' <?php echo $is_re ?>>Return
			</td>

		<?php endif ?>

		<!-- <td><?php echo form_submit("edit_item", $this->lang->line('sales_edit_item'));?></td> -->
		<!-- <td></td> -->
		</tr>
		<!-- <tr id="prob_td" class="<?php echo $cl?>">
		<td style="color:#2F4F4F";><?php //echo $this->lang->line('sales_description_abbrv').':';?></td>
		<td colspan=2 style="text-align:left;">

		<?php
        	if($item['allow_alt_description']==1)
        	{
        		//echo form_input(array('name'=>'description','value'=>$item['description'],'size'=>'20'));
        	}
        	else
        	{
				if ($item['description']!='')
				{
					//echo $item['description'];
        			echo form_hidden('description',$item['description']);
        		}
        		else
        		{
        			//echo 'None';
        			echo form_hidden('description','');
        		}
        	}
		?>
		</td>
		<td>&nbsp;</td>
		<td></td>
		<td style="color:#2F4F4F";>
		<?php
        	if($item['is_serialized']==1)
        	{
				echo $this->lang->line('sales_serial').':';
			}
		?>
		</td>
		<td colspan=11 style="text-align:left;">
		<?php
        	if($item['is_serialized']==1)
        	{
        		echo form_input(array('name'=>'serialnumber','value'=>$item['serialnumber'],'size'=>'20'));
			}
			else
			{
				echo form_hidden('serialnumber', '');
			}
		?>
		</td>
		

		</tr> -->
		<tr style="height:3px">
			<td colspan=14 style="background-color:white"></td>
		</tr>	
	</form>
	<?php
	}
}
?>
</tbody>
</table>
</div>
<div class="clearfix" style="margin-bottom:1px;">&nbsp;</div>
<div class="sale_detail_box">
	<div id='sale_details'>
		


		<?php foreach($taxes as $name=>$value) { ?>
		<div class="float_left" style='width:55%;'><?php echo $name; ?>:</div>
		<div class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($value); ?></div>
		<?php }; ?>
		
		<?php if ($mode=='return'): ?>
			<div class="float_left" style="width:55%;">Total Sale:</div>
			<div id="return_total_sale" class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($total_sale); ?></div>
			<!-- <div class="float_left" style="width:55%;"><?php echo 'Amount Payable:' ?></div>
			<div id="return_payable" class="float_left" style="width:45%;font-weight:bold;"><?php echo $amount_payable; ?></div> -->
			<div class="float_left" style='width:55%;' id="total_restock_label">Total Paid:</div>
			<div id="sd_total_old_payment" class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($old_payment_total); ?></div>
			<div class="float_left" style='width:55%;' id="total_restock_label">Total Restock:</div>
			<div id="sd_total_restock" class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($total_restock); ?></div>

			<div class="float_left" style='width:55%;' id="total_return_label">
			<?php if ($total<=0): ?>
				Total Return:
			<?php else: ?>
				Total Due:

			<?php endif ?>
			</div>
			<div id="sd_total" class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($total); ?></div>
			

			

		<?php else: ?>
			<div class="float_left" style="width:55%;"><?php echo $this->lang->line('sales_sub_total'); ?>:</div>
			<div id="subtotal" class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($subtotal); ?></div>
			<div class="float_left" style='width:55%;'><?php echo $this->lang->line('sales_total'); ?>:</div>
			<div id="sd_total" class="float_left" style="width:45%;font-weight:bold;"><?php echo to_currency($total); ?></div>
		<?php endif ?>
		
		
		
	</div>

	<?php
	// Only show this part if there are Items already in the sale.
	if(count($cart) > 0)
	{
	?>	
			
    	
		<div class="clearfix" style="margin-bottom:1px;">&nbsp;</div>
		<?php
		// Only show this part if there is at least one payment entered.
		if(count($payments) > 0 OR $old_payments OR $mode=='return')
		{
		?>
			<div id="finish_sale">

				<?php
				if ($mode=='sale') {
					echo form_open("sales/complete",array('id'=>'finish_sale_form'));
				}else{
					echo form_open("return/complete_return",array('id'=>'finish_return_form'));

				}
				?>
				<label id="comment_label" for="comment"><?php echo $this->lang->line('common_comments'); ?>:</label>
				<?php echo form_textarea(array('name'=>'comment', 'id' => 'comment', 'value'=>$comment,'rows'=>'4','cols'=>'23'));?>
				<br /><br />
				
				<?php
				
				if(!empty($customer_email))
				{
					echo $this->lang->line('sales_email_receipt'). ': '. form_checkbox(array(
					    'name'        => 'email_receipt',
					    'id'          => 'email_receipt',
					    'value'       => '1',
					    'checked'     => (boolean)$email_receipt,
					    )).'<br />('.$customer_email.')<br />';
				}
				 
				//if ($payments_cover_total)//if Amount Tendered<Amount Due will not display complete sale button
				//{
				?>
				
				
				
				<?php

				if ($mode=='sale') {
					if ($edit_sale_id) {
						echo "<div class='small_button' id='finish_sale_button' style='float:right;margin-top:5px;'><span>Update Sale</span></div>";
						
					}else{
						
						if ($amount_due<=0) {
						
							echo "<div class='small_button' id='finish_sale_button' style='float:right;margin-top:5px;'><span>".$this->lang->line('sales_complete_sale')."</span></div>";
						}else{
							if ($customer_info) {
								if ($payments OR $old_payments) {
									echo "<div class='small_button' id='finish_sale_button' style='float:right;margin-top:5px;'><span>".$this->lang->line('sales_complete_sale')."</span></div>";
								}
								
							}
						}
						
						
					}
					if (count($payments)<0) {
						
						echo "<div class='small_button' id='create_quote_button' style='float:right;margin-top:5px;'><span>Create Quote</span></div>";

					}	
					
				
				//}

					if ($did) {
						$sty = "display:none;";
						if ($is_updatable) {
							$sty = "";
							
						}
					?>
						
						<div class='small_button update_deposit_button' id='suspend_sale_button' style='float:right;margin-top:5px;<?php echo $sty ?>'><span>Update Deposit</span></div>
							
						
					<?php
						
					}else{
						if ($customer_info) {
							if ($payments) {
								if ($payments_total>0) {
									echo "<div class='small_button' id='suspend_sale_button' style='float:right;margin-top:5px;'><span>Deposit</span></div>";
								}
								
							}
							
						}
					

					}
				}else{
					$c_margin = '30px';
					$crsty = "display:none;";

					if ($total!=0 OR $has_return_item==1) {
						$c_margin = '5px';
						$crsty = "";

					}


					?>
						<div class='small_button' id='finish_return_button' style='float:right;margin-top:5px;<?php echo $crsty ?>'><span>Complete Return</span></div>
						
					<?php
				}
				?>
			</div>
			</form>
			<?php if ($did): ?>
						<div id="Cancel_sale">
							<a href="<?php echo site_url('sales/cancel_deposit/width:300') ?>" class="thickbox" title="Cancel Deposit: <?php echo $dno ?>">
								<div class="small_button" id="cancel_deposit_button" style="margin-top: 5px;">
									<span>Cancel Deposit</span>
								</div>
							</a>

				    	</div>
				    	
					

				<?php else: ?>

						<?php if ($edit_sale_id): ?>
							<?php echo form_open("returns/add_return_item",array('id'=>'cancel_sale_form')); ?>
							
						<?php else: ?>
							<?php echo form_open("sales/cancel_sale",array('id'=>'cancel_sale_form')); ?>
							
						<?php endif ?>
	
						<div id="Cancel_sale">
							<div class='small_button' id='cancel_sale_button' style='margin-top:<?php echo $c_margin ?>;'>
								<input type="hidden" name="item_returns" value="<?php echo $sale_info->invoiceid ?>">
								<span>
									<?php 
										if($mode=='sale'){
											if ($edit_sale_id) {
												echo "Return Sale";
											}else{
												echo "Cancel";
											}
										}else{
											echo 'Cancel Return';
										} ?></span>
							</div>
				    	</div>
			    		<?php echo form_close() ?>

				<?php endif ?>

		<?php
		}else{
			if ($mode=='sale') {
				// echo $qid;
    			echo form_open("sales/complete",array('id'=>'finish_sale_form'));
    			if ($qid) {
					echo "<div class='small_button' id='update_quote_button' style='float:right;margin-top:5px;'><span>Update Quote</span></div>";

    			}else{
					echo "<div class='small_button' id='create_quote_button' style='float:right;margin-top:5px;'><span>Create Quote</span></div>";

    			}
    			echo form_close();
    		}
		}
		?>



</div>

<div class="sale_payment_box">
	<input type="hidden" name="hadue" id="hadue" value="<?php echo $amount_due ?>">
    <table width="100%">
    <?php if ($mode=='sale'): ?>
	    <tr>

	    	<td style="width:55%; "><div class="float_left"><?php echo 'Payments Total:' ?></div></td>
	    	<td style="width:45%; text-align:right;"><div id="payment_total" class="float_left" style="text-align:right;font-weight:bold;"><?php echo to_currency($payments_total); ?></div></td>
		</tr>
		<tr>
			<?php
				$due_label = "Amount Due";
				$due_amount_text  = $amount_due;
				if ($amount_due<0) {
					$due_label = "Change";
					$due_amount_text *= -1;
				}
			?>
			<td style="width:55%; "><div class="float_left" ><?php echo $due_label ?></div></td>
			<td style="width:45%; text-align:right; ">
			<div id="amount_due" class="float_left" style="text-align:right;font-weight:bold;"><?php echo to_currency($due_amount_text); ?></div></td>
		</tr>
	<?php else: ?>
		
		<tr>

	    	<td style="width:55%; "><div class="float_left"><?php echo 'Sale Paid Amount:' ?></div></td>
	    	<td style="width:45%; text-align:right;"><div id="return_sale_paid" class="float_left" style="text-align:right;font-weight:bold;"><?php echo to_currency($old_payment_total); ?></div></td>
		</tr>
    <?php endif ?>
		
	</table>

	<div id="Payment_Types" >

		<div style="height:100px;">

			<?php
			if ($mode=='sale') {
				echo form_open("sales/add_payment",array('id'=>'add_payment_form'));
			}else{

				echo form_open("returns/add_payment_return",array('id'=>'add_payment_form'));

			}
			?>
			<table width="100%">
				<tr>
					<td>
						<?php echo $this->lang->line('sales_payment').':   ';?>
					</td>
					<td>
						<?php echo form_dropdown('payment_type',$payment_options,array(), 'id="payment_types"');?>
					</td>
				</tr>
				<tr>
					<td>
						<span id="amount_tendered_label"><?php echo $this->lang->line('sales_amount_tendered').': ';?></span>
					</td>
					<td>
						<?php if ($mode=='return'): ?>

							<?php echo form_input(array('name'=>'amount_tendered','readonly'=>'readonly','onchange'=>'ereturn(event)','id'=>'amount_tendered','value'=>to_currency_no_money($amount_tendered),'size'=>'10','onclick'=>'select();'));	?>
							
						<?php else: ?>
							<?php echo form_input(array('name'=>'amount_tendered','id'=>'amount_tendered','value'=>to_currency_no_money($amount_due),'size'=>'10','onclick'=>'select();'));	?>

						<?php endif ?>
					</td>
				</tr>
				<!-- <?php if ($mode=='return'): ?>
				<tr>
					<td>
						<span id="amount_tendered_label">Surcharge Amount:</span>
					</td>
					<td>
						<?php echo form_input(array('name'=>'amount_charge','id'=>'amount_charge','value'=>to_currency_no_money($amount_due),'size'=>'10','onclick'=>'select();'));	?>
					</td>
				</tr>
				<?php endif ?> -->
        	</table>

        	<?php if ($did): ?>
        		<?php if($amount_due>0){
        			$asty = "";
        		}else{
        			$asty = "display:none;";

        		} ?>
        		<div class='small_button' id='add_payment_button' style='float:left;margin-top:5px;<?php echo $asty ?>'>
					<span><?php echo $this->lang->line('sales_add_payment'); ?></span>
				</div>
        	<?php else: ?>
        		<?php if ($mode=='return'): ?>
        			<?php if($payments_total!=$total || ($total==0 && $has_return_item==1) ){
        				
	        		}else{
	        			$rsty = "display:none;";

	        		} 
	        		// var_dump(count($payments));
	        		if (count($payments)>0) {
	        			$rsty = "display:none;";
        			}

	        		?>
	        		<!-- <div class='small_button' id='add_payment_button' style='float:left;margin-top:5px;<?php echo $rsty ?>'>
						<span><?php echo $this->lang->line('sales_add_payment'); ?></span>
					</div> -->
        		<?php else: ?>
        			<div class='small_button' id='add_payment_button' style='float:left;margin-top:5px;'>
						<span><?php echo $this->lang->line('sales_add_payment'); ?></span>
					</div>
        		<?php endif ?>
        		
        	<?php endif ?>
			
		</div>
		</form>

		<?php
		// Only show this part if there is at least one payment entered.

		?>
		


		<?php

		if((count($payments) > 0 OR $old_payments) AND $mode!='return')
		{
		?>
	    	<table id="register">
	    	<thead>
			<tr>
			<th style="width:10%;"><?php echo $this->lang->line('common_delete'); ?></th>
			<th><?php echo "Date" ?></th>
			<th style="width:15%;">By</th>
			
			<th style="width:60%;"><?php echo 'Type'; ?></th>
			<th style="width:15%;"><?php echo 'Amount'; ?></th>

			</tr>
			</thead>
			<tbody id="payment_contents">
				<?php if ($edit_sale_id OR $did): ?>
					<?php if ($old_payments): ?>
						<?php foreach ($old_payments as $old_payment=>$old_pay): ?>
							<tr>
					            <td>

					            	<?php 
					            		if ($did || $qid || $edit_sale_id) {
					            			echo '';
					            		}else{
					            			// echo anchor("sales/delete_payment/$payment_id",'['.$this->lang->line('common_delete').']');

					            		}
					            	?>

					            	

					            </td>
					            <td style="white-space:nowrap;"><?php echo $old_pay['date'] ?></td>
								<td><?php echo  $old_pay['payment_type'] ?> </td>
								<td><?php echo  to_currency($old_pay['payment_amount'])  ?>  </td>
								<td style="white-space:nowrap;"><?php echo  $old_pay['emp_name']  ?>  </td>


							</tr>
						<?php endforeach ?>
					<?php endif ?>
				<?php endif ?>
			<?php

				foreach($payments as $payment_id=>$payment)
				{
				echo form_open("sales/edit_payment/$payment_id",array('id'=>'edit_payment_form'.$payment_id));
				?>
	            <tr>
	            <td>

	            	<?php 
	            		// if ($did || $qid || $edit_sale_id) {
	            		// 	echo '';
	            		// }else{
	            			if ($mode=='return') {
	            				echo anchor("returns/delete_payment_return/$payment_id",'['.$this->lang->line('common_delete').']');
	            				
	            			}else{
	            				echo anchor("sales/delete_payment/$payment_id",'['.$this->lang->line('common_delete').']');

	            			}

	            		// }
	            	?>

	            	

	            </td>
	            <td style="white-space:nowrap;"><?php echo $payment['date'] ?></td>
				<td style="white-space:nowrap;"><?php echo  $payment['emp_name']  ?>  </td>
				<td><?php echo  $payment['payment_type'] ?> </td>
				<td><?php echo  to_currency($payment['payment_amount'])  ?>  </td>


				</tr>
				</form>
				<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr style="border-top: 1px solid #cccccc;">
					<td colspan="4" style="text-align: right;"><b>Total Payment:</b></td>
					<td style="text-align: right;"><b><?php echo $payments_total ?></b></td>
				</tr>
			</tfoot>
			</table>
		    <br />
		<?php
		}
		?>



	</div>

	<?php
	}
	?>
	</div>
<!-- <button class='www'>CLICK</button> -->
<input type="hidden" name="cart_cus" id="cart_cus" value="<?php echo $this->sale_lib->get_customer() ?>">
<div class="clearfix" style="margin-bottom:30px;">&nbsp;</div>
<!-- <div class="loading_box">Place at bottom of page</div> -->



<script type="text/javascript" language="javascript">
// $('.www').click(function(){
// 	$('body').addClass('loading');
// });


$(document).ready(function()
{
    $("#item").autocomplete('<?php echo site_url("sales/item_search"); ?>',
    {
        
    	minChars:0,
    	max:100,
    	selectFirst: true,
       	delay:10,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#item").result(function(event, data, formatted)
    {
		$("#add_item_form").submit();
    });

	$('#item').focus();

	$('#item').blur(function()
    {
    	$(this).attr('value',"<?php echo $this->lang->line('sales_start_typing_item_name'); ?>");
    });
  //   $("#item_returns").autocomplete('<?php echo site_url("returns/item_return_search"); ?>',
  //   {
        
  //   	minChars:0,
  //   	max:100,
  //   	selectFirst: true,
  //      	delay:2,
  //   	formatItem: function(row) {
  //   		re = row[0]+' | '+row[1];
		// 	return re;
		// }
  //   });

    $("#item_returns").result(function(event, data, formatted)
    {
		$("#add_item_form").submit();
    });

	$('#item_returns').focus();

	$('#item_returns').blur(function()
    {
    	$(this).attr('value',"Invoice # OR Item Barcode");
    });

	$('#item,#customer,#item_returns').click(function()
    {
		
    	$(this).attr('value','');
    	
    });

    $("#customer").autocomplete('<?php echo site_url("sales/customer_search"); ?>',
    {
    	minChars:0,
    	delay:10,
    	max:100,
    	formatItem: function(row) {
			return row[1];
		}
    });

    $("#customer").result(function(event, data, formatted)
    {
		$("#select_customer_form").submit();
    });

    $('#customer').blur(function()
    {
    	$(this).attr('value',"<?php echo $this->lang->line('sales_start_typing_customer_name'); ?>");
    });
	
	$('#comment').change(function() 
	{
		$.post('<?php echo site_url("sales/set_comment");?>', {comment: $('#comment').val()});
	});
	
	$('#email_receipt').change(function() 
	{
		$.post('<?php echo site_url("sales/set_email_receipt");?>', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
	});
	
	
    $("#finish_sale_button").click(function()
    {
    	var cui = $('#customer_id').val();
    	var am_due = $('#hadue').val(); //amount due
    	var random = Math.floor((Math.random() * 100) + 1);


    	if (confirm('<?php echo $this->lang->line("sales_confirm_finish_sale"); ?>'))
    	{
    		if (am_due==0 || (cui!='' && cui !=-1 && cui!=72)) {
				$('body').addClass('loading');
    			
				$('#finish_sale_form').attr('target', '_sale_'+random);
    			// $('#finish_sale_form').submit();
    			$('#finish_sale_button').css('pointer-events','none');
    			$('#suspend_sale_button').css('pointer-events','none');
    			$('#create_quote_button').css('pointer-events','none');
    			$('#update_quote_button').css('pointer-events','none');
    			$('#cancel_sale_button').css('pointer-events','none');

    			var frm = $('#finish_sale_form');
    			var print_link = "<?php echo site_url('sales/sale_invoice') ?>";
	    		submit_data(frm,print_link);
    		}else{
    			alert('Please Select Customer');
    		};
    		
    	}
    });

    $("#finish_return_button").click(function()
    {
    	if (confirm('Are you sure ?'))
    	{
    		// location.href="<?php echo site_url('returns/get_return_items')?>"
    		// console.log(ret);
    		// $('#finish_return_form').submit();
    		if (retsel=='') {
    			alert('No Item Selected');
    		}else{
    			$.post("<?php echo site_url('returns/complete_return') ?>",
				{
					returns: ret
				},
				 function(result){
		        	location.href="<?php echo site_url('returns/view_reciept')?>"+'/'+result;
		        	// console.log(result);
		    	},'json');

    		};
    		
    	}
    });

	$("#suspend_sale_button").click(function()
	{
    	var random = Math.floor((Math.random() * 100) + 1);

		var cui = $('#customer_id').val();
		if (cui != '' && cui !='-1' && cui!=72) {
			if (confirm('<?php echo $this->lang->line("sales_confirm_suspend_sale"); ?>'))
	    	{
				$('body').addClass('loading');
				
				$('#finish_sale_form').attr('action', '<?php echo site_url("sales/suspend"); ?>');
				$('#finish_sale_form').attr('target', '_deposit_'+random);
				$('#finish_sale_button').css('pointer-events','none');
    			$('#suspend_sale_button').css('pointer-events','none');
    			$('#create_quote_button').css('pointer-events','none');
    			$('#update_quote_button').css('pointer-events','none');
    			$('#cancel_sale_button').css('pointer-events','none');
	    		
	    			var frm = $('#finish_sale_form');
	    			var print_link = "<?php echo site_url('sales/dprint/') ?>";
				    submit_data(frm,print_link);
			        
				       
	    	}
    	}else{
			alert('Please Select a Customer!');
		}
	});
	$("#create_quote_button").click(function()
    {	
		var cui = $('#customer_id').val();
    	var random = Math.floor((Math.random() * 100) + 1);

    	if (confirm('Confirm ?'))
    	{
    		if (cui != '' && cui !='-1' && cui !=72) {
				$('body').addClass('loading');
				
				$('#finish_sale_form').attr('action', '<?php echo site_url("sales/add_quotation"); ?>');
				$('#finish_sale_form').attr('target', '_quote_'+random);
	    		// $('#finish_sale_form').submit();
				$('#finish_sale_button').css('pointer-events','none');
    			$('#suspend_sale_button').css('pointer-events','none');
    			$('#create_quote_button').css('pointer-events','none');
    			$('#update_quote_button').css('pointer-events','none');
    			$('#cancel_sale_button').css('pointer-events','none');

    			var frm = $('#finish_sale_form');
    			var print_link = "<?php echo site_url('sales/qprint/') ?>";
			    submit_data(frm,print_link);
	    		// setTimeout(function(){
	    		// 	location.href= "<?php echo site_url('sales/new_sale') ?>";
	    		// },2000)
	    		// location.href= "<?php echo site_url('sales') ?>";
			}else{
				alert('Please Select a Customer!');
			}
			
    	}
    });
    $('#update_quote_button').click(function(){
    	var cui = $('#customer_id').val();
    	var random = Math.floor((Math.random() * 100) + 1);

    	if (confirm('Confirm ?'))
    	{
    		if (cui != '' && cui !='-1' && cui !=72) {
				$('body').addClass('loading');

				$('#finish_sale_form').attr('action', '<?php echo site_url("sales/add_quotation"); ?>');
				$('#finish_sale_form').attr('target', '_quote_'+random);
	    		// $('#finish_sale_form').submit();
	    		$('#finish_sale_button').css('pointer-events','none');
    			$('#suspend_sale_button').css('pointer-events','none');
    			$('#create_quote_button').css('pointer-events','none');
    			$('#update_quote_button').css('pointer-events','none');
    			$('#cancel_sale_button').css('pointer-events','none');
    			var frm = $('#finish_sale_form');
    			var print_link = "<?php echo site_url('sales/qprint') ?>";
	    		submit_data(frm,print_link);
	    		// location.href= "<?php echo site_url('sales') ?>";
			}else{
				alert('Please Select a Customer!');
			}
			
    	}
    });

    $("#cancel_sale_button").click(function()
    {
    	if (confirm('<?php echo $this->lang->line("sales_confirm_cancel_sale"); ?>'))
    	{

    		$('#cancel_sale_form').submit();
    		$('#finish_sale_button').css('pointer-events','none');
			$('#suspend_sale_button').css('pointer-events','none');
			$('#create_quote_button').css('pointer-events','none');
			$('#update_quote_button').css('pointer-events','none');
			$('#cancel_sale_button').css('pointer-events','none');
    	}
    });
    $('#add_payment_button').click(function(){
    	$("#add_payment_form").submit();
    })
	$("#add_payment_form").submit(function()
	{	
		var mode = '<?php echo $mode ?>';

		if (confirm('សម្រេចបញ្ចូល Payment ?')) {
			if (mode=='return') {
				var return_item = $('input[name="return_check"]:checked').length;
				if (return_item>0) {
					return true;
				}else{

					alert('Please Select Item to Return.')
					return false;
				}
			}else{
	   			return true;

			}
		}else{
			return false;
		}
		
    });

	$("#payment_types").change(checkPaymentTypeGiftcard).ready(checkPaymentTypeGiftcard);

	
//	$('#item').keyup(function(){
//		//select query where barcode=$('#item').val();
//		
//		var barcode=$('#item').val();
//		var getbarcode;
//		$.getJSON('index.php/sales/convertBarcodeToItemNumber', {getbarcode:barcode}, function(data) {
//			
//		    /* data will hold the php array as a javascript object */
//		    $.each(data, function(key, itemID) {
//
//		    	 $('#item').val(itemID);
//
//		    });
//		});
//			
//		});

/*$('#item').keypress(function(e) {
	var $this = $(this);
		setTimeout(function() {
			var text = $this.val();
			//console.log(text.length);
			if(text.length==12){
				var barcode=$('#item').val();
				var getbarcode;
				$.getJSON('index.php/sales/convertBarcodeToItemNumber', {getbarcode:barcode}, function(data) {
					// data will hold the php array as a javascript object 
					$.each(data, function(key, itemID) {
						$('#item').val(itemID);
					});
				});
			}
	}, 0);
});
*/

/*$(function() {
   window.charCount = 0;
   setInterval(function() {
      var c = $("#item").val().length;
      if(c != window.charCount) {
        window.charCount = c;
        if(window.charCount==12){
				var barcode=$('#item').val();
				var getbarcode;
				$.getJSON('index.php/sales/convertBarcodeToItemNumber', {getbarcode:barcode}, function(data) {
					// data will hold the php array as a javascript object 
					$.each(data, function(key, itemID) {
						$('#item').val(itemID);
					});
				});
			} 
      }
    }, 500);
})*/
//////////////


});
function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value",response.item_id);
		$("#add_item_form").submit();
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#customer").attr("value",response.person_id);
		$("#select_customer_form").submit();
	}
}

function checkPaymentTypeGiftcard()
{
	if ($("#payment_types").val() == "<?php echo $this->lang->line('sales_giftcard'); ?>")
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_giftcard_number'); ?>");
		$("#amount_tendered").val('');
		$("#amount_tendered").focus();
	}
	else
	{
		$("#amount_tendered_label").html("<?php echo $this->lang->line('sales_amount_tendered'); ?>");		
	}
}

function chkVal(event){
			
	var pchkval = $(event.target).parent().parent().find('#pchkval');
	if ($(event.target).is(':checked')){
		pchkval.val(1);
	}else{
		pchkval.val(0);

	};
	if (confirm("Set Level 1 Problem ?")==true) {
		equantity(event);
	};
	
}	
function chkVal2(event){
	var pchkval2 = $(event.target).parent().parent().find('#pchkval2');
	if ($(event.target).is(':checked')){
		pchkval2.val(2);
	}else{
		pchkval2.val(1);
	};
	if (confirm("Set Level 2 Problem ?")==true) {
		equantity(event);
	};
	
}
function edit_price(event){
	var p = $(event.target).parent().parent().find('#price');
	p.select();
	// alert('hi');/
}
function edit_disc(event){
	var p = $(event.target).parent().parent().find('#disc');
	p.select();
	// alert('hi');/
}

chkVal2();
chkVal();

</script>
<script type="text/javascript">
	getmodels();
    function getmodels(){
        var url="<?php echo site_url('site/getmodels')?>";
        var make_id=$('#s_make_id').val();
        var lmodel = $('#last_model_id').val();
        // alert(make_id);
        $.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{
                    'make_id':make_id,
                    'last_model_id':lmodel
                },
            success:function(data) {
                // alert(data);
              $("#s_model_id").html(data);
              // document.getElementById('model_id').html=data;
             
            }
          })
    }   
</script>
<script type="text/javascript">
	$('#s_make_id').select2();
	$('#s_model_id').select2();
	$('#year').select2();
	$('.partplacement_id').select2();
	$('.color_id').select2();
	$('#cus_type').select2({
		 minimumResultsForSearch: -1,
	});
	$('#savediv').click(function(){
		var q = confirm("Confirm ?");
		var name = $('#customer_name').val();
		var phone_1 = $('#customer_phone1').val();
		var phone_2 = $('#customer_phone2').val();

		if (q) {
			if (name =='' || phone_1=='') {
				$('#name_msg').removeClass('hidden');
				$('#phone_msg').removeClass('hidden');
				$(".select_cus").css('height','125px');
			}else{
				$.post("<?php echo site_url('customers/save_from_sale') ?>",
				{
					name:name,
					phone_1,phone_1,
					phone_2,phone_2
				}
				,
				function(data){
					location.href="<?php echo site_url('sales/select_customer') ?>"+"/"+data.customer_id;
				}
				,
				'Json')
			}
			
		}
	});

	$(document).ready(function(){
		// var s = localStorage.getItem('selling');
		// localStorage
		// alert(s);
		check_cus_type();
	});
	$(window).on("storage", this.handleLocalStorageChange);
	$("#cus_type").change(function(){
			// check_cus_type();
	});
	function check_cus_type(){
		if ($("#cus_type").val()!='walk') {
			$('.select_btn').addClass("hidden");
			$('.walk_cus').removeClass("hidden");
			$(".select_cus").css('height','88px');
			$('#name_msg').addClass('hidden');
			$('#phone_msg').addClass('hidden');
		}else{
			$('.select_btn').removeClass("hidden");
			$('.walk_cus').addClass("hidden");
			$(".select_cus").css('height','25px')

		}
	}
	

$('#s_vin_number').change(function(){
	$('#year').select2('open');

});
$('#year').change(function(){
	$('#s_make_id').select2('open');
})

$('#s_make_id').change(function(){
	$('#s_model_id').select2('open');
});
$('#s_model_id').change(function(){
	$('#name_select').select2();

	$('#name_select').select2('open');
});

$(document).ready(function(){
	current_method = "<?php echo $this->router->method ?>";
	if (current_method=='select_name') {
		$('#s_color_id').select2('open');
	}
});

$('#new_i_input').change(function(){
	$('#s_color_id').select2('open');

});
// $('#s_color_id').change(function(){
	
// 		console.log($('.pplacement'));


// })



$('#s_color_id').on('change', function (e) {
  // Do something
  setTimeout(function(){
  	$('#pplacement').focus();

  },0)
});

$('.pplacement').change(function(){
	$('#new_i_input_price').focus();
});

$('#new_i_input_price').change(function(){
	$('#new_i_input_ta').focus();
})

	$('#s_vin_number').select2();

function matchStart (term, text) {
  if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
    return true;
  }
 
  return false;
}
 
$.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
  $("#s_vin_number").select2({
    matcher: oldMatcher(matchStart)
  })
});

var lines = [];
var ret = [];
var retsel = [];

function rval(){
	ret = $('input[name="return_check"]:not(:checked)').map(function() {
    return this.value;
	}).get();
	retsel = $('input[name="return_check"]:checked').map(function() {
    return this.value;
	}).get();

	// console.log(ret);
	// console.log(retsel);
}
function dval(){
	lines = $('input[name="del_check"]:checked').map(function() {
    	return this.value;
	}).get();
	if (lines.length) {
		$('#remove_all_item').hide();
		$('#remove_sel_item').show();
	}else{
		$('#remove_all_item').show();
		$('#remove_sel_item').hide();
	};
}
dval();
rval();
// $('.td_p2').hide();

function removeall(){
	if (confirm("Are you sure ?")==true) {
		location.href="<?php echo site_url('sales/remove_all_item') ?>"
    };
}
function removesel(){
	if (confirm("Are you sure ?")==true) {
		// console.log(lines);
		$.post("<?php echo site_url('sales/remove_item') ?>",
			{
				lines: lines
			},
			 function(d){
			 		// console.log(d);
			 		location.href = "<?php echo site_url('sales') ?>"

	   	 	},'json');
    };
}
function edit_qty(event){
	var p = $(event.target).parent().parent().find('#quantity');
	p.select();
	// alert('hi');/
}
function ereturn(event){
	$('body').addClass('loading');
	// $('#subtotal').html('500');

	var line = $(event.target).parent().parent().find('#return_check').val();

	var pric = $(event.target).parent().parent().find('#price').val();

	var disc = $(event.target).parent().parent().find('#disc').val();
	// if (disc>pric) {
	// 	disc = pric;
	// 	$(event.target).parent().parent().find('#disc').val(disc);
	// }
	var quan = $(event.target).parent().parent().find('#quantity').val();
	var problem = $(event.target).parent().parent().find('#rapro:checked').val();

	var input_id = $(event.target).attr('id');

	// if (input_id=='amount_tendered') {
	// 	var amt_tender = $('#amount_tendered').val();
	// }else{
	// 	unset_return_payment();
	// }
	
	var is_return ;
	
	if ($(event.target).parent().parent().find('#return_check').is(':checked')){
		is_return = 1;
	}else{
		is_return = 0;
	};

	
	$.post("<?php echo site_url('returns/edit_item_return') ?>"+"/"+line,
		{
			disc:disc,
			is_return:is_return,
			price:pric,
			quantity:quan,
			problem:problem

		},
		function(data){
				if (problem) {
					if (problem!=0) {
						$(event.target).parent().parent().addClass('problem');
					}else{
						$(event.target).parent().parent().removeClass('problem');

					}
				}
				if (data) {
					$('#subtotal').html(data.subtotal);

					real_payment = parseFloat(data.real_total);
					if (real_payment!=0) {
						show_complete_return();
						// $('#add_payment_button').hide();
					}else{

						if (data.has_item==1) {
						show_complete_return();

						}else{
							hide_complete_return();

						}
						// $('#add_payment_button').show();
					}
					
					if (data.real_total<=0) {
						$('#total_return_label').text('Total Return:');
					}else{
						$('#total_return_label').text('Total Due:');
					}

					$('#sd_total').html(data.total);
					$('#sd_total_restock').html(data.total_restock);
					$('#return_payable').html(data.amount_payable);
					// $('#max_return').html(data.payments_total);
					// $('#amount_due').html(data.amount_due);
					$('#amount_tendered').val(data.amount_tendered);
					$(event.target).parent().parent().find('#total_row').html(data.total_row);
				}
				
				
				$('body').removeClass('loading');
			},'json'
		);
}

function unset_return_payment(){
	$.ajax({
		url:"<?php echo site_url('returns/unset_return_payment') ?>",
		type:'get',
		success:function(re){
			console.log(re);
		}
	});
}

function hide_complete_return(){
	$('#finish_return_button').hide();
	$('#cancel_sale_button').css('margin-top','40px');
}

function show_complete_return(){
	$('#finish_return_button').show();
	$('#cancel_sale_button').css('margin-top','5px');
}

function equantity(event){
	$('body').addClass('loading');
	var quan = $(event.target).parent().parent().find('#quantity').val();
	var pric = $(event.target).parent().parent().find('#price').val();
	var disc = $(event.target).parent().parent().find('#disc').val();
	var desc = $(event.target).parent().parent().find('#desc').val();
	var line = $(event.target).parent().parent().find('#del_check').val();
	var is_new = $(event.target).parent().parent().find('#is_new').val();
	var is_pro = $(event.target).parent().parent().find('#pchkval').val();
	var is_pro2 = $(event.target).parent().parent().find('#pchkval2').val();
	var sale_date = $('#sale_date_input').val();

	console.log(sale_date);
	
	// alert(is_new);
	$.post("<?php echo site_url('sales/edit_item') ?>"+"/"+line,
		{
			price:pric,
			quantity:quan,
			discount:disc,
			desc:desc,
			is_new:is_new,
			is_pro:is_pro,
			is_pro2:is_pro2,
			sale_date:sale_date
		},
		function(data){
				$('body').removeClass('loading');

				if (data.no_qty!='') {
					alert(data.no_qty);
					$(event.target).parent().parent().find('#quantity').val(data.max_qty);
				}else{
					$(event.target).parent().parent().find('#total_row').html(data.total_row);
					$('#subtotal').html(data.subtotal);
					$('#sd_total').html(data.total);
					$('#payment_total').html(data.payments_total);
					$('#amount_due').html(data.amount_due);
					$('#amount_tendered').val(data.amount_tendered);
					// $('#sale_date_input').val(data.sale_date)

					var amu_due = data.amount_due_no_currency;
					if (amu_due>0) {
						$('#add_payment_button').show();
					}else{
						$('#add_payment_button').hide();
					}

					if (data.updatable) {
						$('.update_deposit_button').show();
					}else{
						$('.update_deposit_button').hide();

					}
					// $(event.target).parent().parent().find('#pro_td').css('background-color','#333');
					if (data.problem==1) {
						$(event.target).closest('#pro_td').children('td').css('background-color','#ffcccc');
						$(event.target).closest('#pro_td').next().children('td').css('background-color','#ffcccc');
						$(event.target).closest('#td_p').hide();
						$(event.target).parent().parent().find('#td_p2').show();
					}else if(data.problem==2){
						$(event.target).closest('#pro_td').children('td').css('background-color','#ffcccc');
						$(event.target).closest('#pro_td').next().children('td').css('background-color','rgba(224, 0, 0, 0.64)');
					};
				};
			},'json'
		);
}

// $('.td_p2').hide();
$('#name_select').select2({ width: '100%',minimumResultsForSearch: -1 });

$('#name_select').change(function(){
	if ($('#name_select').val()=='other') {
		$('#new_i_input').removeClass('hidden');
		$('#new_i_input').focus();
		$('#close_name').removeClass('hidden');
		$('#select_name_div').addClass('hidden');
	}else if($('#name_select').val()=='select'){
		$('#name_select').val('').change();
		$('#btnselectname').click();
	};
	
});
$('#close_name').click(function(){
	$('#name_select').val('').change();
	$('input[name="s_name"]').val('');

	$('#new_i_input').addClass('hidden');
	$('#close_name').addClass('hidden');
	$('#select_name_div').removeClass('hidden');
})
$('#payment_types').change(function(){
	var cui = $('#customer_id').val();
	if (cui=='' || cui=='-1') {
		alert('Please Select a Customer!');
		$('#payment_types').val('Cash');
	}
	// alert(cui);
});

$('#add_new_item_btn').click(function(){
	var new_item_sel = $('#name_select').val();
	var new_item_text  = $('#new_i_input').val();
	var new_item_price = $('#new_i_input_price').val();

	// alert(new_item_price);return;
	if (new_item_sel=='') {
		// alert(new_item_sel);
		alert('Part Name Required !');
	}else if(new_item_sel=='other' && new_item_text==''){
		alert('Part Name Required !');
	}else if(new_item_price =='' || new_item_price == 0){
		alert('Price Required !');
	}else{
		if (new_item_text!='' && new_item_sel=='other') {
			$.post("<?php echo site_url('sales/chk_new_name') ?>",
				{
					name:new_item_text
				},
				function(d){
					console.log(d);
					if (d.t=='y') {
						alert('Name Already Exist!');
					}else if(d.t=='n'){
						// alert('Submit');
						$('#add_new_item_form').submit();

					}
				},'Json');
		}else{
						$('#add_new_item_form').submit();
			
		}
	};
	
})


 $("#s_vin_number,#year,#s_make_id,#s_model_id,#name_select,#s_color_id").on("select2:select", function (evt) {
   	// alert('Hello');
   	$(this).first().focus();
 });
// $('#s_vin_number').click(function(){
// 	alert('hhh');
// 	// $('#s_vin_number').first().focus();
// })
$('#s_vin_number').change(function(){
	set_remember("vinnumber",$(this).val());
});
$('#year').change(function(){
	set_remember("year",$(this).val());
})
$('#s_make_id').change(function(){
	set_remember("make_id",$(this).val());
})
$('#s_model_id').change(function(){
	set_remember("model_id",$(this).val());
})
$('#s_color_id').change(function(){
	set_remember("color_id",$(this).val());
})
$('.pplacement').change(function(){
	set_remember("pplacement",$(this).val());
})
$('#new_i_input_price').change(function(){
	set_remember("price",$(this).val());
})


function set_remember(name,val){
	localStorage.setItem(name, val);
}





$(document).ready(function(){
	if (localStorage.getItem('vinnumber')) {
		$('#s_vin_number').val(localStorage.getItem('vinnumber')).trigger('change.select2');
	};
	if (localStorage.getItem('year')) {
		$('#year').val(localStorage.getItem('year')).trigger('change.select2');
	};
	if (localStorage.getItem('make_id')) {
		$('#s_make_id').val(localStorage.getItem('make_id')).trigger('change.select2');
	};
	if (localStorage.getItem('model_id')) {
		$('#s_model_id').val(localStorage.getItem('model_id')).trigger('change.select2');
	};
	if (localStorage.getItem('color_id')) {
		// $('#s_color_id').val(localStorage.getItem('color_id')).trigger('change.select2');
	};
	if (localStorage.getItem('pplacement')) {
		// $('.pplacement').val(localStorage.getItem('pplacement'));
	};
	if (localStorage.getItem('price')) {
		// $('#new_i_input_price').val(localStorage.getItem('price'));
	};
});


function view_storage(){
	var i;
	console.log("local storage");
	for (i = 0; i < localStorage.length; i++)   {
	    console.log(localStorage.key(i) + "=[" + localStorage.getItem(localStorage.key(i)) + "]");
	}
}
function submit_data(frm,print_link){
    $('body').addClass('loading');
	$.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        async: false,
        data: frm.serialize(),
        success: function (data) {
        	if (data!='-1') {
                window.open(print_link+"/"+data,frm.attr('target'));
                setTimeout(function(){
	    			location.href= "<?php echo site_url('sales/new_sale') ?>";
	    		},2000);
        	}else{
	    			location.href= "<?php echo site_url('sales/new_sale') ?>";
        		
        	}
        	$('body').removeClass('loading');

        }
    });
}


	
	// $('#item').keypress(function(e){

	// 	if(e.which == 13) {
	// 		e.preventDefault();
	// 		var li_result = $('.ac_results ul li');
	// 		var selected_val;

	// 		li_result.each(function(index){
	// 			var this_li = $(this);
	// 			if (this_li.hasClass('ac_over')==true) {
	// 				selected_val = this_li;
				
	// 			}else{
	// 				if (index==0) {
	// 					selected_val = this_li;
	// 				}
	// 			}
	// 		});
	// 		var val = selected_val[0].innerText;

	// 		console.log(selected_val);

	// 		$('.ac_input').val(val);
	// 		$('#add_item_form').submit(); 

	// 		// console.log(ul_result);
	// 		// $('#item').val(first_val);

	//     }
	// });

</script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>

<script type="text/javascript">
	// location.href = "<?php echo site_url('sales') ?>";
	// $(document).ready(function(){
		// if (localStorage.getItem('sale_date')!='') {
			// $('#sale_date_input').val(localStorage.getItem('sale_date'));
		// }
		$('#sale_date_input').datepicker({
			format:"dd-mm-yyyy"

		});


		
	// });

	$('#sale_date_input').change(function(event){
		// if ($(this).val()!='') {
		// 	// localStorage.setItem('sale_date',$(this).val());
		// 	// location.href = "<?php echo site_url('sales/set_sale_date?d=') ?>"+$(this).val();
		// 	set_date($(this).val());
		// }else{
		// 	// location.href = "<?php echo site_url('sales/set_sale_date?d=') ?>";
		// 	set_date();


		// }
		equantity(event);
	});

	function set_date(date){
		$.ajax({
			url:"<?php echo site_url('sales/set_sale_date') ?>",
			type:'post',
			data:{sale_date:date},
			success:function(res){
				console.log(res);
			}

		})
	}

	function isNumberKey(evt){
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57))
	        return false;
	    return true;
	}


	
</script>
<?php $this->load->view("partial/footer"); ?>

