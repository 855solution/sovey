<?php $this->load->view("partial/header"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	exit;
}
?>
<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}


    @media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			width: 95%;
   			margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head{
			/*width: 45%;
			float: left;
			left: 0;*/
			margin-top: 38px !important;

		}
		.left_signature{
			float: left;
			width: 30%;
		}
		.right_signature{
			float: right;
			width: 30%;
		}
		.footer_signature{
			width: 90%;
		}
		
	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 30px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		margin-top: 30px;
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	

</style>
<div id="receipt_wrapper">
	<div class="top_btn">
		<div class="small_button float_right" id="btn_print"><span>Print</span></div>
	</div>
	<div style="clear:both">
		
	</div>
	<div id="receipt_header">
		

		<div id="company_name"><?php echo $this->config->item('company'); ?>
		<span class="inv_type">Close Register</span>
		</div>

		<div class="left_head">
			<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
			<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>

				<?php
					$total_cash = ($reg_info->cash_in_hand + $reg_info->total_sale + $deposit) - $sale_due;
				?>
				<div id="customer" class="l_text"><div>Cash in Hand</div><p>$<?php echo number_format($reg_info->cash_in_hand) ?></p></div>
				
				<div class="l_text">
					<div>Total Sale</div><p>$<?php echo number_format($reg_info->total_sale) ?></p>
				</div class="l_text">
				<div class="l_text">
					<div>Sale Due</div><p>$<?php echo number_format($sale_due) ?></p>
				</div>
				<div class="l_text"> 
					<div>Total Deposit</div><p>$<?php echo number_format($deposit) ?></p>
				</div>
				
				<!-- <div class="l_text">
					<div>Contact</div><p><?php echo $customer; ?></p>
				</div> -->

			
			
		</div>
		<div class="right_head">
			<div id='barcode'>
			<?php echo "<img src='index.php/barcode?barcode=".str_replace('-', '', $reg_info->register_id)."&text=$reg_info->register_id&width=400&height=50' />"; ?>
			</div>
			<div class="r_text"><div>Page</div><p class="page_counter"></p></div>
			<div class="r_text"><div>Open Date</div><p><?php echo date('d/m/Y H:i:s',strtotime($reg_info->open_time)); ?></p></div>
			<div class="r_text"><div>Close Date</div><p><?php echo date("d/m/Y H:i:s",strtotime($reg_info->close_time)) ?></p></div>
			<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>
			<!-- <div class="r_text"><div>Payment Type</div><p><?php echo $payments['Cash']['payment_type'] ?></p></div> -->
		</div>
	</div>
	<div style="clear:both"></div>

	<table id="receipt_items">
	<thead>
		<tr>
		<!-- <th></th> -->
	    <th>No</th>
		<th>Invoice #</th>
		<th>Sale Time</th>
		<th>Customer</th>
		<th>Total</th>
		<th>Paid</th>
		<th>Due</th>
		</tr>
	</thead>
	
	<tbody style="border-bottom:2px solid black">
	<?php echo $tbody ?>
	</tbody>
	<tfoot>
		
	<tr>
	<td colspan="3"></td>
	<td style='text-align:right;font-weight:bold;border:1px solid #ddd;'>Total</td>
	<td style='text-align:right;border:1px solid #ddd;'><?php echo to_currency($ts); ?></td>
	<td style='text-align:right;border:1px solid #ddd;'><?php echo to_currency($tp); ?></td>
	<td style='text-align:right;border:1px solid #ddd;'><?php echo to_currency($td); ?></td>
	</tr>

	<?php foreach($taxes as $name=>$value) { ?>
		<!-- <tr>
			<td colspan="7" style='text-align:right;'><?php echo $name; ?>:</td>
			<td colspan="3" style='text-align:right;'><?php echo to_currency($value); ?></td>
		</tr> -->
	<?php }; ?>

	<tr>
	<td colspan="3"></td>
	<td style='text-align:right;border:1px solid #ddd;font-weight: bold;'><?php echo 'Total Cash'; ?></td>
	<td colspan="3" style='text-align:right;border:1px solid #ddd;background: #ddd;font-weight: bold;'><?php echo to_currency($total_cash); ?></td>
	</tr>


	<?php
		$first_payment= (float)filter_var($sale_pay_amount, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION); 
		$paid=$payment->payment_amount-$payment->dept;
		$change = $payment->dept;
		$due_status = 'Amount Due';
		if ($first_payment>$payment->payment_amount) {
			$due_status = 'Change';
			$change = $payment->payment_amount-$first_payment;
		}

	?>
	<!-- <tr>
		<td colspan="3"></td>
		<td colspan="2" style="text-align:right;font-weight:bold;border:1px solid #ddd;"><?php echo $this->lang->line('sales_paid'); ?></td>
		<td colspan="2" style="text-align:right;border:1px solid #ddd;"><?php echo to_currency($paid); ?>  </td>
    </tr> -->
	

    <!-- <tr><td colspan="10">&nbsp;</td></tr>  -->
	<!-- <tr>
		<td colspan="3"></td>
		<td colspan="2" style='text-align:right;font-weight:bold;border:1px solid #ddd;'><?php echo $due_status ?></td>
		<td colspan="2" style='text-align:right;border:1px solid #ddd;'><?php echo  to_currency(abs($change)); ?></td>
	</tr> -->
	
	</tfoot>

	</table>

	<!-- <div id="sale_return_policy">
	<?php echo nl2br($this->config->item('return_policy')); ?>
	</div> -->
	<div class="footer_signature_wrapper">
		<div class="footer_signature">
			<h4 class="title">Note</h4>
			<p><?php echo $reg_info->note ?></p>
			<!-- <div class="left_signature">
				<label>Customer's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<div class="right_signature">
				<label>Seller's Signature</label>
				<div class="underline">
					
				</div>
			</div> -->
		</div>
	</div>
	
	
</div>


<div style="clear:both"></div>
<?php $this->load->view("partial/footer"); ?>

<?php if ($this->Appconfig->get('print_after_sale'))
{
?>
<script type="text/javascript">
$(window).load(function()
{
	window.print();
	localStorage.setItem('selling',1);

	// alert(s);
});

$('#btn_new_sale').click(function(){
	location.href = "<?php echo site_url('sales/new_sale') ?>";
});
$('#btn_edit_sale').click(function(){
	// alert('Under Construction...');
	location.href = "<?php echo site_url('sales/edit_sale/'.$sale_id) ?>";
});

$('#btn_print').click(function(){
	window.print();
});
</script>
<?php
}
?>