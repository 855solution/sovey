<?php $this->load->view("partial/header"); ?>
<style type="text/css">
	#receipt_items th{
		text-align: center;
		background: #4d8da6;
		padding: 5px;
		border-left: 1px solid white;
		color: white;
	}
	#receipt_items td{
		text-align: center;
	}
	#receipt_items tbody tr:nth-child(even) {
		background: rgba(78, 142, 167, 0.34);
		border: none;
	}
	#receipt_wrapper{
		width: 100%;
		float: left;
		padding: 1% 2%;
	}
	#receipt_header{
		margin-bottom: 30px !important;
	}
	.underline{
		border-bottom: 1px solid #6F6F6F;
		height: 50px;
	}
	.left_signature{
		float: left;
		width: 20%;
	}
	.right_signature{
		float: right;
		width: 20%;
	}
	.footer_signature_wrapper{
		width: 100%;
	}
	.footer_signature{
		width: 50%;
	}


    @media print {
   		#receipt_wrapper{
   			/*padding-left:2%;*/
   			/*padding-right: 0;*/
   			padding: 0;
   			font-size: 10px;
   			width: 95%;
   			margin-right: 15px;
   			margin-left: 15px;
   			margin-top: 20px;

   		}
   		tbody td{
   			border: none;
   		}
   		#content_area{
   			width: 100% !important;
   		}
   		.page_counter:after{
		    counter-increment: p;
		   	content: counter(p);
		}
		.top_btn{
			display: none;
		}
		.left_head{
			/*width: 45%;
			float: left;
			left: 0;*/
			margin-top: 37px !important;
			color: black !important;

		}
		.right_head{
			color: black !important;

		}
		#receipt_header{
			color: black !important;

		}
		#receipt_items{
			color: black !important;

		}
		.left_signature{
			float: left;
			width: 30%;
			color: black !important;

		}
		.right_signature{
			float: right;
			width: 30%;
			color: black !important;

		}
		.footer_signature{
			width: 90%;
			color: black !important;
			
		}


	}
	
	.left_head{
		width: 45%;
		float: left;
		left: 0;
		margin-top: 30px;

	}
	.right_head{
		width: 45%;
		float: right;
		right: 0;
		margin-top: 30px;
	}
	.left_head .l_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;
	}
	.left_head .l_text p{
		padding:1% 2%;
	}
	.left_head .l_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	.right_head .r_text div{
		background: #4d8da6;
		padding:1% 2%;
		width: 100px;
		color:white;

	}
	.right_head .r_text p{
		padding:1% 2%;
	}
	.right_head .r_text{
		display: flex;
		border-bottom: 1px solid #ddd;
	}
	#company_name{
		background: linear-gradient(to right, rgba(255,0,0,0), rgb(77, 141, 166));
		background: -webkit-linear-gradient(left,#ffffff, #4d8da6);
		width: 96%;
		font-size: 20px;
		padding: 1% 2%;
	}
	.inv_type{
		float: right;
		color: white;
		font-family: Arial;
		font-weight: initial;
	}
	

</style>

<div id="receipt_wrapper">
	<div class="top_btn">
		<div class="small_button float_left" id="btn_new_sale"><span>New Sale</span></div>
		<div class="small_button float_right" id="btn_print"><span>Print</span></div>
	</div>
	<div style="clear:both;"></div>

	<div id="receipt_header">
		

		<div id="company_name"><?php echo $this->config->item('company'); ?>
		<span class="inv_type">Quotation</span>
		</div>
		<div class="left_head">
			<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
			<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
			<?php if(isset($customer))
			{
			?>
				<div id="customer" class="l_text"><div>Customer</div><p><?php echo $cus_info->title.$customer;echo $cus_info->nick_name!=''?"($cus_info->nick_name)":'' ?></p></div>
				<div class="l_text">
					<div>Phone Number</div><p><?php echo $cus_info->phone_number ?></p>
				</div class="l_text">
				<div class="l_text"> 
					<div>Address</div><p><?php echo $cus_info->address_1 ?></p>
				</div>
				<div class="l_text">
					<div>Email</div><p><?php echo $cus_info->email ?></p>
				</div>
				<!-- <div class="l_text">
					<div>Contact</div><p><?php echo $customer; ?></p>
				</div> -->

			<?php
			}
			?>
			
		</div>

		<div class="right_head">
			<div id='barcode'>
			<?php echo "<img src='index.php/barcode?barcode=$qdata->qt_number&text=$qdata->qt_number&width=250&height=50' />"; ?>
			</div>
			<div class="r_text"><div>Page</div><p class="page_counter"></p></div>
			<div class="r_text"><div>Quote ID</div><p><?php echo $qdata->qt_number; ?></p></div>
			<div class="r_text"><div>Quote Date</div><p><?php echo date("d/m/Y",strtotime($qdata->quote_time)) ?></p></div>
			<div class="r_text"><div>Employee</div><p><?php echo $employee; ?></p></div>
			<!-- <div class="r_text"><div>Payment Type</div><p><?php echo $payments['Cash']['payment_type'] ?></p></div> -->
		</div>
	</div>

	<div style="clear:both"></div>

	<table id="receipt_items">
	<thead>
		<tr>
		<!-- <th></th> -->
	    <th><?php echo "No"; ?></th>
		<th><?php echo "Barcode"; ?></th>
		<th>Head #</th>
		<th>Description</th>
		<th><?php echo $this->lang->line('sales_quantity'); ?></th>
		<th><?php echo "PPL"; ?></th>
		<th><?php echo $this->lang->line('common_price'); ?></th>
		<th><?php echo "Dsc"; ?></th>
		<th><?php echo $this->lang->line('sales_total'); ?></th>
		</tr>
	</thead>
	
	<tbody>
	<?php
	$i=0;
	$total = 0;
	// var_dump($items);

	foreach($items as $item)
	{
		$i++;
		$name = $item->name;
		if (!$item->name) {
			$name = $item->khmername_name;
		}
		$ppl = $item->ppl_id;
		if ($item->is_new==0) {
			$ppl = $item->partplacement_name;
		}
		$sub_total = 0;
		$sub_total = ($item->item_unit_price*$item->quantity_purchased)-$item->discount_percent;
	?>
		
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $item->barcode ?></td>
			<td><?php echo $item->category ?></td>
			<td style="text-align:left;"><?php echo $item->year.' '.$item->make_name.' '.$item->model_name.' '.$name.' '.$item->description ?></td>
			<td style='text-align:center;'><?php echo $item->quantity_purchased; ?></td>
			<td><?php echo $item->ppl_id;?></td>
			<td><?php echo to_currency($item->item_unit_price); ?></td>
			<td style='text-align:center;'><?php echo $item->discount_percent; ?></td>
			<td style='text-align:right;'><?php echo to_currency($sub_total); ?></td>
		</tr>

	<?php
		$total+=$sub_total;
	}
	?>
	</tbody>
	<tfoot>
		
	<tr>
		<td colspan="7" style='text-align:right;border-top:2px solid #000000;'><?php echo "Total"; ?></td>
		<td colspan="3" style='text-align:right;border-top:2px solid #000000;'><?php echo to_currency($total); ?></td>
	</tr>

	
	</tfoot>

	</table>
	<div class="footer_signature_wrapper" align="center">
		<div class="footer_signature">
			<div class="left_signature">
				<label>Customer's Signature</label>
				<div class="underline">
					
				</div>
			</div>
			<div class="right_signature">
				<label>Seller's Signature</label>
				<div class="underline">
					
				</div>
			</div>
		</div>
	</div>
	
</div>


<div style="clear:both"></div>
<?php $this->load->view("partial/footer"); ?>


<script type="text/javascript">
$(window).load(function()
{
	window.print();
	localStorage.setItem('selling',1);
	
});

$('#btn_new_sale').click(function(){
	location.href = "<?php echo site_url('sales/new_sale') ?>";
});
$('#btn_print').click(function(){
	window.print();
});
</script>