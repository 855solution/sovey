<?php

	$s='';
	$part_name='';
	$make_id='';
	$model_id='';
	$s_year='';
	$color_id='';
	$is_new='';
	$is_feat='';
	$is_pro='';
	// if(isset($_GET['n'])){
	// 	$part_name=$_GET['n'];
	// 	$make_id=$_GET['m'];
	// 	$model_id=$_GET['mo'];
	// 	$s_year=$_GET['y'];
	// 	$color_id=$_GET['c'];
	// }
	$where='';
	if(isset($_GET['n'])){
		$part_name=$_GET['n'];
	}
	if(isset($_GET['m']) && $_GET['m']!='' && $_GET['m']!='undefined' && $_GET['m']!='0'){
		$where.=" AND i.make_id='".$_GET['m']."'";
		$make_id=$_GET['m'];
	}
	if(isset($_GET['mo']) && $_GET['mo']!=''&& $_GET['mo']!='undefined' && $_GET['mo']!='0'){
		$where.=" AND i.model_id='".$_GET['mo']."'";
		$model_id=$_GET['mo'];

	}
	if(isset($_GET['y']) && $_GET['y']!='' && $_GET['y']!='undefined' && $_GET['y']!='0'){
		$where.=" AND i.year='".$_GET['y']."'";
		$s_year=$_GET['y'];

	}
	if(isset($_GET['c']) && $_GET['c']!='' && $_GET['c']!='undefined' && $_GET['c']!='0'){
		$where.=" AND i.color_id='".$_GET['c']."'";
		$color_id=$_GET['c'];

	}
	if(isset($_GET['ne'])){
		$is_new=1;

	}
	elseif (isset($_GET['f'])){
		$is_feat=1;

	}
	elseif (isset($_GET['pr'])){
		$is_pro=1;
	}
 ?>
 <style type="text/css">
 .delete{
 	cursor: pointer;
 	color: red;
 	margin-top: 5px;
 }
 #filter{
 	float: left;
 	padding:1%;
 	width: 13%;
 }
 .hide{
 	display: none !important;
 }
 </style>
<div id='filter' class="col-sm-3" style="border:1px solid #CCCCCC;">
	<h2>ADVANCE SEARCH</h2>
	<?php if($this->session->userdata('u_inf')){ ?>
		<div class="s_title">Recent Search</div>
	<?php

			$member_id=$this->session->userdata('u_inf')->member_id;
			$savesearch=$this->db->query("SELECT * FROM ospos_savesearch WHERE member_id='$member_id'")->result();
			echo "<ul style='padding-left:10px;'>";
				foreach ($savesearch as $ss) {
					echo "<li style='list-style:none;'><a href='".site_url("site/search?n=$ss->name&m=$ss->make_id&mo=$ss->model_id&y=$ss->year&c=$ss->color_id")."'>$ss->search_name</a><span class='delete pull-right fa fa-trash-o' rel='$ss->search_id'></span></li>";
				}
			echo "</ul>";
	 
	} ?>
	<div class="s_title">Make</div>
	<ul class="listleft">
		<li><label><input onclick='getmodel(event,1);' type="radio" <?php if($make_id==''||$make_id=='undefined') echo 'checked' ?> name="ckmake" class='ckmake' value=""> All</label></li>
		<?php
			$make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0")->result();
			foreach ($make as $m) {

				$count=$this->vinnm->getnumsearch('model_id',$part_name,$m->make_id,$model_id,$s_year,$color_id,$is_new,$is_feat,$is_pro);
				$sele='';
				if($make_id==$m->make_id)
					$sele='checked';
				// if($count>0)
					echo "<li><label><input type='radio' onclick='getmodel(event,1);' name='ckmake' $sele class='ckmake' value='$m->make_id'/> $m->make_name</label></li>";# code...
			}
		 ?>
	</ul>
	<div class="s_title">Model</div>
	<!-- <img class="loading hide" src="<?php echo site_url('../images/loading.gif') ?>" style='width:100%'> -->

	<ul class="listleft lists" id="model_list">
		<li><label><input onclick='getmodel(event,0);' type="radio" <?php if($model_id==''||$model_id=='undefined') echo 'checked' ?> name="ckmodel" class='ckmodel' value=""> All</label></li>
		<?php
			$model_where="";
			if($make_id!='')
				$model_where=" AND make_id='$make_id'";
			$model=$this->db->query("SELECT * FROM ospos_models WHERE deleted=0 {$model_where}")->result();
			foreach ($model as $mo) {
				$sele='';
				$count=$this->vinnm->getnumsearch('model_id',$part_name,$make_id,$mo->model_id,$s_year,$color_id,$is_new,$is_feat,$is_pro);

				if($model_id==$mo->model_id)
					$sele='checked';
				// $class='hide';
				
				if($count>0)
					echo "<li  rel='$mo->make_id'><label><input onclick='getmodel(event,0);' type='radio' $sele name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name</label></li>";# code...
			}
		 ?>
	</ul>
	<div class="loading hide" style='width:15%; margin:auto;'>
		<img src="<?php echo site_url('../images/loading.gif') ?>" style='width:100%'>
	</div>
	<div class="s_title">Year</div>
	<!-- <img class="loading hide" src="<?php echo site_url('../images/loading.gif') ?>" style='width:100%'> -->

	<ul class="listleft lists" id="year_list">
		<li><label><input onclick='getmodel(event,0);' type="checkbox" <?php if($s_year=='' ||$s_year=='undefined'  || $s_year=='_') echo 'checked' ?> name="ckyear" class='ckyear' value=""> All</label></li>
		<?php
			$arrofyear=array();
			if($s_year!='undefined'){
				
				$yeararr=explode('_', $s_year);
				for ($i=0; $i <count($yeararr) ; $i++) { 
					$arrofyear[$yeararr[$i]]=$yeararr[$i];
				}
			}
			for($j=date('Y');$j>=1980;$j--){
				$sele='';
				$count=$this->vinnm->getnumsearch('year',$part_name,$make_id,$model_id,$j,$color_id,$is_new,$is_feat,$is_pro);

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count>0)
					echo "<li><label><input onclick='getmodel(event,0);' type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j</label></li>";# code...
				
			}
		 ?>
	</ul>
	<div class="loading hide" style='width:15%; margin:auto;'>
		<img src="<?php echo site_url('../images/loading.gif') ?>" style='width:100%'>
	</div>
	<div class="s_title">Color</div>
	<ul class="listleft lists" id="color_list">
		<li><label><input type="checkbox" onclick='getmodel(event,0);' <?php if($color_id==''||$color_id=='undefined'  || $color_id=='_') echo 'checked' ?> name="ckcolor" class='ckcolor' value=""> All</label></li>
		<?php
			$color=$this->db->query("SELECT * FROM ospos_colors WHERE deleted=0")->result();
			$arrofcolor=array();
			if($color_id!='undefined'){
				
				$colorarr=explode('_', $color_id);
				for ($i=0; $i <count($colorarr) ; $i++) { 
					$arrofcolor[$colorarr[$i]]=$colorarr[$i];
				}
			}
			foreach ($color as $col) {
				$sele='';
				$count=$this->vinnm->getnumsearch('color_id',$part_name,$make_id,$model_id,$s_year,$col->color_id,$is_new,$is_feat,$is_pro);

				if(isset($arrofcolor[$col->color_id]))
					$sele='checked';
				if($count>0)
					echo "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ckcolor' class='ckcolor' value='$col->color_id'/> $col->color_name</label></li>";# code...
			}
		 ?>
	</ul>
	<div class="loading hide" style='width:15%; margin:auto;'>
		<img src="<?php echo site_url('../images/loading.gif') ?>" style='width:100%'>
	</div>

	<?php if($this->session->userdata('u_inf')){ ?>
		<a href="#" id="save_search">Save Search</a>
		
	<?php } ?>
	<input type="button" class="btn btn-primary pull-right" id="btnadvsearch" style="margin-bottom:10px;float:right" value="Search">
</div>
                 <!-- /.modal-dialog -->
<script type="text/javascript">
	$("#btnadvsearch").click(function(){
		search();

	})
	$("#save_search").click(function(){
		$("#savesearch").modal('show');
	})
	$(".delete").click(function(){
		var s_id=$(this).attr('rel');
		var conf=confirm("Are you sure to delete this search ?");
		if(conf==true){
			var url="<?php echo site_url('site/deletesearch')?>";
			$.ajax({
	            url:url,
	            type:"POST",
	            datatype:"Json",
	            async:false,
	            data:{
	            		'search_id':s_id,
	            		
	            	},
	            success:function(data) {
	            	location.reload();//$(this).closest('li').remove();
	            }
	          })
		}
		
	})
	// $(document).on("change","#per_page",function(){
	// 	var num=$(this).val();
	// 	// var url="<?php echo site_url('site/savenum')?>";
 //    	$.ajax({
 //            url:url,
 //            type:"POST",
 //            datatype:"Json",
 //            async:false,
 //            data:{
 //            		'num':num,
 //            	},
 //            success:function(data) {
 //             	search();
 //            }
 //          })
		

	// })
	// $("#btnsavesearch").click(function(){
	// 	var make=$('input[name="ckmake"]:checked').val();
	// 	var model=$('input[name="ckmodel"]:checked').val();
	// 	var year=''
	// 	$('input[name="ckyear"]:checked').each(function(){
	// 		year+=$(this).val()+'_';
	// 	})
	// 	var color=''
	// 	$('input[name="ckcolor"]:checked').each(function(){
	// 		color+=$(this).val()+'_';
	// 	})
	// 	var name=$('.s_partname').val();
	// 	var p_num=$('.p_num').val();
	// 	var savename=$('#save_name').val();

	// 	var url="<?php echo site_url('site/savesearch')?>";
 //    	$.ajax({
 //            url:url,
 //            type:"POST",
 //            datatype:"Json",
 //            async:false,
 //            data:{
 //            		'make':make,
 //            		'model':model,
 //            		'color':color,
 //            		'year':year,
 //            		'savename':savename,
 //            		'name':name
 //            	},
 //            success:function(data) {
 //            	alert('This Seacher Has Been Save.');
 //             	search();
 //            }
 //          })
	// })
	getmodel();
	function search(){
		var make=$('input[name="ckmake"]:checked').val();
		var model=$('input[name="ckmodel"]:checked').val();
		// var color=$('input[name="ckcolor"]:checked').val();
		var year=''
		$('input[name="ckyear"]:checked').each(function(){
			year+=$(this).val()+'_';
		})
		var color=''
		$('input[name="ckcolor"]:checked').each(function(){
			color+=$(this).val()+'_';
		})
		var name=$('.s_partname').val();
		var p_num=$('#per_page').val();
		location.href="<?php echo site_url('vinnumbers/search_vin');?>?n="+name+"&m="+make+"&mo="+model+"&y="+year+"&c="+color+'&p_num='+p_num;
	}
	function getmodel(event,is_make){
		// $('.lists').addClass('hide');
		$('.loading').removeClass('hide');

		var make_id=$('input[name="ckmake"]:checked').val();
		// var make_id=$(event.target).val();
		var model='';
		if(is_make==0)
			model=$('input[name="ckmodel"]:checked').val();
		// var color=$('input[name="ckcolor"]:checked').val();
		var year=''
		$('input[name="ckyear"]:checked').each(function(){
			year+=$(this).val()+'_';
		})
		var color=''
		$('input[name="ckcolor"]:checked').each(function(){
			color+=$(this).val()+'_';
		})
		var name=$('.s_partname').val();
		var url="<?php echo site_url('site/gemodel')?>";
		var savename=$('#save_name').val();

		$.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{
            		'make':make_id,
            		'model':model,
            		'color':color,
            		'year':year,
            		'savename':savename,
            		'name':name
            	},
            success:function(data) {
            	$("#model_list").html(data.model);
            	$("#year_list").html(data.year);
            	$("#color_list").html(data.color);
				// $('.lists').removeClass('hide');
				$('.loading').addClass('hide');

             	// search();
            }
          })
	}
</script>
