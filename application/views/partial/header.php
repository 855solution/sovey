<?php //session_start();?>
<?php if ($this->uri->segment(1)=='sales' OR $this->uri->segment(1)=='returns') {
	$this->load->library('sale_lib');
} ?>

<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" href="<?php echo base_url('assets') ?>/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url('assets') ?>/favicon.ico" type="image/x-icon">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
	<title><?php echo $this->config->item('company')?></title>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos.css?<?php echo rand(0,1111) ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ospos_print.css"  media="print"/>
		<!-- <link href="<?php echo base_url('css/bootstrap.min.css') ?>" rel="stylesheet"> -->

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/fontawesome/css/font-awesome.min.css" type="text/css" />

	
	<link href="<?php echo base_url('css/site/style.css?'.rand(0,1111)) ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('css/register.css?'.rand(0,99999)) ?>">

	<script type="text/javascript" src="<?php echo base_url()?>assets/vendor/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/site/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>						

	<script src="<?php echo base_url();?>js/jquery.color.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.metadata.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.tablesorter.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.ajax_queue.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.bgiframe.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.autocomplete.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.jkey-1.1.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/thickbox.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/common.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/manage_tables.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/swfobject.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/date.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<!-- <script src="<?php echo base_url();?>js/datepicker.js" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
    <script src="<?php echo base_url();?>js/jquery.chainedSelects.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>ckeditor/ckeditor.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
   	<!--<script src="<?php echo base_url();?>js/jquery.tablesorter.pager.js" type="text/javascript"></script>-->
    <!--<script src="<?php echo base_url();?>ckfinder/ckfinder.js" type="text/javascript" language="javascript" charset="UTF-8"></script>-->
  	

    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/tablesorter.pager.css"> -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/header.css?<?php echo rand(0,10000) ?>">

  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/select2/css/select2.css">
 <!-- 	<script type="text/javascript" src="<?php echo base_url();?>vselect/jquery-1.6.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>

<!-- 1. Add latest jQuery and fancyBox files -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>

<!-- END FANCYBOX -->

<script type="text/javascript" src="<?php echo base_url() ?>jquery-barcode/jquery-barcode.js"></script>
</head>
<body>

<!-- <div class="site_loading" align="center">
	<img src="<?php echo base_url('assets/loading/load.gif') ?>" width="5%">
</div> -->
<div id="menubar">
	<div id="menubar_container">
		<div id="menubar_company_info">
			<div class="company_title_div">
				<span id="company_title"><a href="index.php/home"><?php echo $this->config->item('company'); ?></a></span>
			</div>
		</div>

	    <div id="menubar_navigation">
	        <?php
	        foreach($allowed_modules->result() as $module)
	        {
	        ?>
	        <div class="menu_item" rel="<?php echo $module->module_id ?>">
	            <a href="<?php echo site_url("$module->module_id");?>">
	            <img width="32" height="32" src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" /></a><br />
	            <a href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
	        </div>
	        <?php
	        }
	        ?>
	    </div>
	</div>

    <div id="menubar_footer">
    	<div id="menubar_welcome">
    		<?php echo $this->lang->line('common_welcome')." $user_info->last_name $user_info->first_name ! &nbsp;&nbsp;|&nbsp;&nbsp; "; ?>
    		<?php echo anchor("home/logout",$this->lang->line("common_logout")); ?>
    	</div>
	    <div id="menubar_date">
	    	<?php echo date('d F, Y h:i a') ?>
	    </div>
	    
	</div>
</div>
<div style="clear:both;"></div>

<div id="content_area_wrapper">
		<div id="content_area">

