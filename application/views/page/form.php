<style type="text/css">
	table tbody tr td img{width: 20px; margin-right: 10px}
	ul,ol{
		margin-bottom: 0px !important;
	}
    .saveloading{width:35px;}
	a{
		cursor: pointer;
	}
	.datepicker {z-index: 9999;}
</style>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php

echo form_open('page/save/'.$page_info->pageid,array('id'=>'model_form'));
?>
<fieldset id="model_basic_info" style="padding: 5px;">
<legend>Page Information</legend>

<div class="field_row clearfix">
<?php echo form_label('Page name:', 'url',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'Page_name',
		'id'=>'Page_name',
		'value'=>$page_info->page_name)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Page Order:', 'url',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'Page_order',
		'id'=>'Page_order',
		'value'=>$page_info->orders)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<label class='wide'>Keyword</label>
	<div class='form_field'>
	<textarea name='keyword' id='keyword'><?php if(isset($page_info)) echo $page_info->keyword ?></textarea>
	</div>
</div>
<div class="field_row clearfix">
<label class='wide'>is Active</label>
	<div class='form_field'>
		<label><input type='checkbox' name='is_active' id='is-active' <?php if(isset($page_info)) if($page_info->is_active==1) echo "checked"; ?>/> is_active </label>
	</div>
</div>
<div class="field_row clearfix">
<label class='wide'>Article Content</label>
	<div class='form_field'>
	<textarea name='descr' id='descr'><?php if(isset($page_info)) echo $page_info->article ?></textarea>
	</div>
</div>
<?php
// echo form_submit(array(
// 	'name'=>'submit',
// 	'id'=>'submit',
// 	'value'=>$this->lang->line('common_submit'),
// 	'class'=>'submit_button float_left')
// );
?>
<input type="button" class="submit_button float_left" id='submit_page' name='submit' value='Submit'>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function PreviewImage(event) {
        var uppreview=$(event.target).attr('rel');
        //alert(uppreview);
        var upimage=$(event.target).attr('id');
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(upimage).files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById(uppreview).src = oFREvent.target.result;
             document.getElementById(uppreview).style.backgroundImage = "none";
        };
    }
$(document).ready(function(){

	// loadEditor('descr');
	
	$('#submit_page').click(function(){
		var data = CKEDITOR.instances.descr.getData();

		$('#descr').text(data);
		setTimeout(function(){$('#model_form').submit(); }, 500);
		
	})
	
	$('#model_form').validate({
		// var data = CKEDITOR.instances.descr.getData();
		
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
				success:function(response)
				{
					tb_remove();
					post_model_form_submit(response);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			page_name:"required",
			
   		},
		messages:
		{
			page_name:"Input name to continue",	
			
		}
	});
});
CKEDITOR.replace( 'descr',
    {
        filebrowserBrowseUrl :"<?php echo base_url();?>ckeditor/ckfinder/ckfinder.html?Connector=ckeditor/ckfinder/core/connectors/php/connector.php",
        filebrowserImageBrowseUrl : "<?php echo base_url();?>ckeditor/ckfinder/ckfinder.html?Type=Images&Connector=ckeditor/ckfinder/core/connectors/php/connector.php",
        filebrowserFlashBrowseUrl :"<?php echo base_url();?>ckeditor/ckfinder/ckfinder.html?Type=Flash&Connector=ckeditor/ckfinder/core/connector/php/connector.php",
        filebrowserUploadUrl  :"<?php echo base_url();?>ckeditor/js/ckeditor/filemanager/connectors/php/upload.php?Type=File",
        filebrowserImageUploadUrl : "<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Image",
        filebrowserFlashUploadUrl : "<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash"
            
      }); 
// CKEDITOR.replace('descr',{
// 	filebrowserBrowseUrl: "<?php echo base_url(); ?>ckfinder/ckfinder.html?resourceType=Files"
// });
// CKFinder.setupCKEditor();

// function loadEditor(id)
// {
//     var instance = CKEDITOR.instances[id];
//     if(instance)
//     {
//         CKEDITOR.remove(instance);
//     }
   
// }


</script>