


<?php $this->load->view("partial/header"); ?>

<script type="text/javascript">
$(document).ready(function()
{
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo $this->lang->line('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','index.php/items/generate_barcodes/'+selected.join(':'));
    });

    $("#low_inventory").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#is_serialized").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#no_description").click(function()
    {
    	$('#items_filter_form').submit();
    });





});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		var selected_item_ids=get_selected_values();
		for(k=0;k<selected_item_ids.length;k++)
		{
			update_row(selected_item_ids[k],'<?php echo site_url("$controller_name/get_row")?>');
		}
		set_feedback(response.message,'success_message',false);
	}
}

function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="block";
        }
}


/*---------------Upload Image-----------------*/


var intTextBox=0;
function addElement(){
	intTextBox = intTextBox + 1;
	var contentID = document.getElementById('pop');
	var newTBDiv = document.createElement('div');
	newTBDiv.setAttribute('id','strText'+intTextBox);
	newTBDiv.innerHTML ="<input type='file' id='" + intTextBox + "' size='25' name='path[]' style='margin-top:15px;'/>";
	contentID.appendChild(newTBDiv);
}
function removeElement(){
	if(intTextBox != 0){
		var contentID = document.getElementById('pop');
		contentID.removeChild(document.getElementById('strText'+intTextBox));
		intTextBox = intTextBox-1;
	}
}

/*----------------------------------------------------*/



</script>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box"></ul>
<?php
echo form_open('items/go');
?>

<fieldset id="item_basic_info">
<legend><?php echo $this->lang->line("items_basic_information"); ?></legend>






<table cellpadding="5" cellspacing="5">
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_vinnumber').':', 'category',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'category',
                    'id'=>'category',
                    'size'=>'25',
                    'value'=>$item_info->category)
                );?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
            <?php echo form_label($this->lang->line('items_item_number').':', 'name',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_input(array(
                    'name'=>'item_number',
                    'id'=>'item_number',
                    'size'=>'25',
                    'value'=>$item_info->item_number)
                );?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
    
    <tr>
        <td colspan="2">
            <div class="field_row clearfix">
           
                <div class='form_field'>
					   <br>
					  			<?php
            echo form_submit(array(
                'name'=>'submit',
                'id'=>'submit',
                'value'=>'Go',
                'class'=>'submit_button float_left',
            	
            
            )
            );
            ?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
    
  
</table>    
</fieldset>
<?php
echo form_close();
?>


<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});
    $("#category").result(function(event, data, formatted){});
	$("#category").search();


	$('#item_form').validate({
		submitHandler:function(form)
		{
			/*
			make sure the hidden field #item_number gets set
			to the visible scan_item_number value
			*/
			//$('#item_number').val($('#scan_item_number').val());
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_item_form_submit(response);
				window.location.href = "<?php echo site_url('items')?>";
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			//category:"required",
			cost_price:
			{
				required:true,
				number:true
			},

			unit_price:
			{
				required:true,
				number:true
			},
			tax_percent:
			{
				required:true,
				number:true
			},
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			name:"<?php echo $this->lang->line('items_name_required'); ?>",
			//category:"<?php //echo $this->lang->line('items_category_required'); ?>",
			cost_price:
			{
				required:"<?php echo $this->lang->line('items_cost_price_required'); ?>",
				number:"<?php echo $this->lang->line('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo $this->lang->line('items_unit_price_required'); ?>",
				number:"<?php echo $this->lang->line('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				required:"<?php echo $this->lang->line('items_tax_percent_required'); ?>",
				number:"<?php echo $this->lang->line('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				required:"<?php echo $this->lang->line('items_quantity_required'); ?>",
				number:"<?php echo $this->lang->line('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required:"<?php echo $this->lang->line('items_reorder_level_required'); ?>",
				number:"<?php echo $this->lang->line('items_reorder_level_number'); ?>"
			}

		}
	});

	$("#item_number").autocomplete("<?php //echo site_url('items/suggest_partNumber');?>",{max:100,minChars:0,delay:10});
    //$("#item_number").result(function(event, data, formatted){});
	//$("#item_number").search();
	

	
	
});
</script>

<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>