<?php $this->load->view("partial/header"); ?>

<?php 
	$url=site_url('items?p=');
	if ($this->uri->segment(2)=='refresh') {
		$url=site_url('items/refresh?p=');
	}elseif($this->uri->segment(2)=='search'){
		$s = $_GET['s'];
		$url=site_url('items/search?s='.$s.'&p=');
	}

 ?>

<script type="text/javascript">
$(document).ready(function()
{
	$('#search').change(function(){
		var s = $('.ac_over').text()
		if (s=='') {return};
		window.location='index.php/items/search?s='+s;

	});

	$("#numpage").change(function(){
		var p=$(this).val();
		location.href="<?php echo $url ?>"+p;
	})
    //init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo $this->lang->line('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','index.php/items/generate_barcodes/'+selected.join(':'));
    });
    
    $("#low_inventory").click(function()
    {
    	if ($('#low_inventory').attr('checked')) {
    		$('#items_filter_form').submit();
    	}else{
    		location.href='<?php echo site_url('items') ?>';
    	
    	};
    	
    });

    $("#is_serialized").click(function()
    {
    	if ($('#is_serialized').attr('checked')) {
    		$('#items_filter_form').submit();

    	}else{
    		location.href='<?php echo site_url('items') ?>';

    	};
    });

    $("#no_description").click(function()
    {
    	if ($('#no_description').attr('checked')){
    		$('#items_filter_form').submit();

    	}else{
    		location.href='<?php echo site_url('items') ?>';
    		
    	};
    });


    
    $("#make_model_year_search").click(function()
  	 {
   	 $('#items_filter_form').submit();

   	});
    /*
    $("#model_id").change(function()
    	{
    	$('#items_filter_form').submit();

    });
    $("#year").change(function()
 	{
 		$('#items_filter_form').submit();

 	});
    */	    

});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		var selected_item_ids=get_selected_values();
		for(k=0;k<selected_item_ids.length;k++)
		{
			update_row(selected_item_ids[k],'<?php echo site_url("$controller_name/get_row")?>');
		}
		set_feedback(response.message,'success_message',false);
	}
}
$(document).ready(function(){
	$("#clearitem").click(function(){
		var conf=confirm("Are you sure to clear empty item.");
		if(conf==true){
            var url="<?php echo site_url('items/clearitem')?>";
			$.ajax({
	            url:url,
	            type:"POST",
	            datatype:"Json",
	            async:false,
	            data:{},
	            success:function(data) {
	            	alert(data);
	             
	            }
	          })
		}
	})
})

function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="block";
        }
}

</script>

<!-- Make javascript -->

 <script language="JavaScript" type="text/javascript">
				$(function()
				{
					$('#make_id').chainSelect('#model_id','<?php echo site_url('items/combobox') ?>',
					{ 
						before:function (target) 
						//before request hide the target combobox and display the loading message
						{ 
							$("#loading").css("display","block");
							$(target).css("display","none");
							
						},
						after:function (target) 
						//after request show the target combobox and hide the loading message
						{ 
							$("#loading").css("display","none");
							$(target).css("display","inline");
						}
						
					});
				});
            </script>
<!--  -->
<style type="text/css">
	#ispending td{
		background:rgba(255, 0, 0, 0.14);
	}
	#ispending:hover{
		background:#e9e9e9;
	}
	#spa span{
		/*padding: 0 1% !important;*/
	}
	.barcode_date{
		display: none;
		position: absolute;
		width: 190px;
		height: 110px;
		background: white;
		/*border: 1px solid;*/
		box-shadow: 0px 0px 5px;
		text-align: center;
		padding: 10px;
		border-radius: 5px;
		z-index: 99999;
	}
	.barcode_date_active{
		display: block;
		position: absolute;
		width: 200px;
		height: auto;
		background: white;
		/*border: 1px solid;*/
		box-shadow: 0px 0px 5px;
		text-align: center;
		padding: 10px;
		border-radius: 5px;
		z-index: 99999;
	}
	.hide{
		display: none;
	}
	.page_result b{
		color: red !important;
	}
	.v_photo{
		margin-top: -23px;
		width: 120px;
		text-align: center;
		position: absolute;
		background: rgba(0, 0, 0, 0.21) none repeat scroll 0% 0%;
	}
	.v_photo a{
		color: white;
	}
	#pagination{
		padding: 1% 0;
		float: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		color: #21759B;
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination .ui-state-disabled{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
		cursor:not-allowed;
	}
	#pagination span{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		color: #21759B;
		border-radius: 5px;
		padding: 5px 10px;
		cursor:not-allowed;

	}
	#pagination .dot{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		color: #21759B;
		border-radius: 5px;
		padding: 5px 10px;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="new_button">
		<?php 
		$js = 'id="make_id"';
		
		//echo anchor("$controller_name/view/-1/width:$form_width",
		// "<div class='big_button' style='float: left;'><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		//array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new'));
		//);
		?>
		
		
		<a href="<?php echo site_url("$controller_name/view/-1/width:$form_width");?>">
		<?php echo "<div class='big_button' style='float: left;'>
		<span>".$this->lang->line($controller_name.'_new')."</span>
		</div>";?>
		</a>
		
		
		<?php echo anchor("$controller_name/excel_import/width:$form_width",
		"<div class='big_button' style='float: left;'><span>Excel Import</span></div>",
		array('class'=>'thickbox none','title'=>'Import Items from Excel'));
		?>
	</div>
</div>

<!-- <div style="background-color: #EEEEEE; color:#7d7d7d; border: 1px solid #CCCCCC; border-radius: 5px 5px 5px 5px;
    height: auto; margin-bottom: 15px; padding-left: 15px; padding-top: 9px; position: relative;">
	<div id="titleTextImg" style="height: 25px;">
		<div style="float:left;vertical-align:text-top;">Search Options :</div>
		<a id="imageDivLink" href="javascript:show_hide_search_filter('search_filter_section', 'imageDivLink');" style="outline:none;">
		<img src="
		<?php echo isset($search_section_state)?  ( ($search_section_state)? base_url().'images/minus.png' : base_url().'images/plus.png') : base_url().'images/plus.png';?>" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;"></a>
	</div>
</div> -->
<?php echo $this->pagination->create_links();?>
<select name="numpage" id="numpage" style="margin-left:20px;">
	<?php
		

		for ($i=10; $i <= 300; $i+=10) { 
			$sel='';
			if(isset($_GET['p'])){
				 if($_GET['p']==$i)
					$sel='selected';
			}else{
				if($i==30)
					$sel='selected';
			}
			echo "<option $sel>$i</option>";
		}
	 ?>
</select>
<div id="table_action_header">
	<ul>
		<li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li class="float_left"><span><?php echo anchor("$controller_name/bulk_edit/width:$form_width",$this->lang->line("items_bulk_edit"),array('id'=>'bulk_edit','title'=>$this->lang->line('items_edit_multiple_items'))); ?></span></li>
		<li class="float_left"><span><?php echo anchor("$controller_name/generate_barcodes",$this->lang->line("items_generate_barcodes"),array('id'=>'generate_barcodes', 'target' =>'_blank','title'=>$this->lang->line('items_generate_barcodes'))); ?></span></li>
		<li class="float_left" id="clearitem"><span>Clear Empty Quantity(2 weeks)</span></li>
		

		<li class="float_right">
		<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		
		<input type="text" name ='search' id='search'/>
		</li>
		<!-- <div class="float_right" style="margin-top:-1px">
			<select id="vin_select">
				<option>Please Select</option>
				<?php 
					foreach ($vinnumber as $vin) {
				?>
					<option value="<?php echo $vin->vinnumber_name?>"><?php echo $vin->vinnumber_name ?></option>
				<?php
					}
				?>
			</select>
		</div> -->
		</form>
		<li class="float_left">
			<span id='barcode_date_btn' onclick='bdate();'>Generate Barcode By Added Date</span>
			<div class='barcode_date'>
				<div class="g_barcode_date">
					<input maxlength="2" style="width:20px" type="text" name="db_from_day" placeholder='DD' class="db_from_day" id="db_from_day" onkeypress="return isNumberKey(event);">
					<input maxlength="2" style="width:22px" type="text" name="db_from_month" placeholder='MM' class="db_from_month" id="db_from_month" onkeypress="return isNumberKey(event);">
					<input maxlength="2" style="width:20px" type="text" name="db_from_year" placeholder='YY' class="db_from_year" id="db_from_year" onkeypress="return isNumberKey(event);">
					<br>To<br>
					<input maxlength="2" style="width:20px" type="text" name="db_to_day" placeholder='DD' class="db_to_day" style="margin-bottom: 10px;" id="db_to_day" onkeypress="return isNumberKey(event);">
					<input maxlength="2" style="width:22px" type="text" name="db_to_month" placeholder='MM' class="db_to_month" style="margin-bottom: 10px;" id="db_to_month" onkeypress="return isNumberKey(event);">
					<input maxlength="2" style="width:20px" type="text" name="db_to_year" placeholder='YY' class="db_to_year" style="margin-bottom: 10px;" id="db_to_year" onkeypress="return isNumberKey(event);">
				</div>
				<div class="g_barcode_digit">
					<input size="5" type="text" name="db_from_digit" class="db_from_digit" id="db_from_digit" placeholder="digits" onkeypress="return isNumberKey(event);">
					To
					<input size="5" type="text" name="db_to_digit" class="db_to_digit" id="db_to_digit" placeholder="digits" onkeypress="return isNumberKey(event);">
				</div>
				<div style="margin-top:10px;">
					<span id='switch_div' onclick='switch_div();' style='padding:6px 5px;'>By digits</span>
					<span onclick='gb_date();'>Genrate</span>
				</div>
					
			</div>
		</li>
	</ul>
</div>

<div id="table_holder">
<?php echo $manage_table; ?>
</div>
<div id="feedback_bar"></div>

<script type="text/javascript">
	$('#khmername_id').select2();
	$('#make_id').select2();
	$('#model_id').select2();
	$('#year').select2();
	$('#yearto').select2();
	$('#vin_select').select2();

	$('#vin_select').change(function(){
		var vin = $(this).val()
		// alert(vin);
	});
</script>
<?php $this->load->view("partial/footer"); ?>

<!-- GENERATE BARCODE -->
<script type="text/javascript">
	function bldate(){
		$('.barcode_date').toggleClass('barcode_date_active');
	}
	function bdate(){
		$('.barcode_date').toggleClass('barcode_date_active');
	}
	$('.g_barcode_digit').hide();
	$('#switch_div').text('By Digits');
	

	function switch_div(){
		$('.g_barcode_digit,.g_barcode_date').toggle();
		// $('.g_barcode_date').addClass('hide');

		if ($('#switch_div').text()=='By Digits') {
			$('#switch_div').text('By Date');
		}else{
			$('#switch_div').text('By Digits');
		}
		$('#db_from_day').val('');
		$('#db_from_month').val('');
		$('#db_from_year').val('');
		$('#db_to_day').val('');
		$('#db_to_month').val('');
		$('#db_to_year').val('');

		$('#db_from_digit').val('');
		$('#db_to_digit').val('');

	}
	
	$("#ui-datepicker-div").remove();
	// $('.db_from,.db_to').datepicker({
	// 	dateFormat:'yy-mm-dd'
	// });
    
    var temp='';
    // var temp_val=0;
    var i=0;
	function chkInput(id,val){
		
		var str = id.split('_');
		var con = str[1];
		var dmy = str[2];
		// var new_val = val+temp_val;
		var opid='';
		if (dmy=='day' && val>31) {
			$('#'+id).val(val.slice(0,-1));
			return;
		};
		if (dmy=='month' && val>12) {
			$('#'+id).val(val.slice(0,-1));
			return;
		};
	

		if (con=='to') {
			opid = id.replace("to","from");
		};
		if (con=='from') {
			opid = id.replace("from","to");
		};

		if (temp!=con && i!=0) {
			return;
		};
		$('#'+opid).val(val);

		temp = con;
		// temp_val+=val;
		i++;
		// console.log(temp_val);

	}
	$('#db_from_day,#db_from_month,#db_from_year,#db_to_day,#db_to_month,#db_to_year,#db_from_digit,#db_to_digit').keyup(function(event){
	
		chkInput(event.target.id,event.target.value);
	});
	function gb_date(){
		var str = new Date().getFullYear().toString();
		var res = str.substr(0, 1);
    	var current_year = parseInt(res*1000);
		// alert(current_year);return;
		var f_d = $('#db_from_day').val();
		var f_m = $('#db_from_month').val();
		var f_y = parseInt($('#db_from_year').val())+current_year;
		var t_d = $('#db_to_day').val();
		var t_m = $('#db_to_month').val();
		var t_y = parseInt($('#db_to_year').val())+current_year;
		var df = f_y+'-'+f_m+'-'+f_d;
		var dt = t_y+'-'+t_m+'-'+t_d;

		var dfd = $('#db_from_digit').val();
		var dtd = $('#db_to_digit').val();

		// if (dfd!='' || dtd!='') {
		// 	if (dfd.length<6 || dtd.length<6) {
		// 		alert('Please Input 6 Digits');
		// 		return;
		// 	};
		// };
		if ((f_y != '' && f_y>t_y) || (dfd!='' && dfd>dtd)) {
			alert('"From" value is bigger than "To" value');
			return;
		};
		

		$.post("<?php echo site_url('items/get_item_ft_date')?>",
		{
			db_from:df,
			db_to:dt,
			db_from_digit:dfd,
			db_to_digit:dtd
		},
		function(d){
			// console.log(d);
			if (d!='' && d!='Array') {
				window.open("<?php echo site_url('items/generate_barcodes')?>"+"/"+d,'_blank');

			}else{
				alert('No item to generate');
			}
		});
	}
	function isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode
	    return !(charCode > 31 && (charCode < 48 || charCode > 57));
	}



</script>