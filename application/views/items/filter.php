<style type="text/css">

 .delete{

 	cursor: pointer;

 	color: red;

 	margin-top: 5px;

 }

 #filter{

 	float: left;
 	padding:1%;
 	width: 11%;
 	position: relative;
 }
 .filter_loading{
 	position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.15);
    z-index: 2;
 }
 .filter_loading_img{
    position: absolute;
	top: 50%;
    left: 30%;
    background: white;
    padding: 5% 10%;
    border: 3px solid #4d8da6;
    border-radius: 15px 15px;
 }
 .filter_loading_img img{
 	vertical-align: middle;
    width: 40px;
 }

 .hide{

 	display: none !important;

 }

 .f_foot{

 	text-align: center;

 	padding: 5px;

 }

 .f_foot span{

 	font-size: 12px;

 }

 .year_ft{

 	padding: 5px 5px !important;

 }

 .lists li label{

 	text-transform: uppercase;

 }

 .select2-container{

 	width: 175px !important;

 }
 .i_exp{
 	cursor: pointer;
 }
 .i_exp_all{
 	cursor: pointer;
 }

 </style>

 

<div id='filter' class="col-sm-3" style="border:1px solid #CCCCCC;">
<div class="filter_loading hide">
	<div class="filter_loading_img">
		<img src="<?php echo base_url('assets/loading/balls.gif') ?>">
	</div>
</div>
	<h1><i class="fa fa-plus-square i_exp_all" > ADVANCE SEARCH</i></h1>

	<div class="f_foot" align="center">

		<span style='cursor:pointer;color:#21759B' onclick='clear_all();' class='clear_all_btn'>Clear All</span>

	</div>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_head">
		Head Number
		</i>
	</div>

	<ul class="listleft" id="head_list">

		

	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_make">
		Make
		</i>
	</div>

	<ul class="listleft" id="make_list">

		

	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_model">
		Model
		</i>
	</div>

	<ul class="listleft lists" id="model_list">

	

	</ul>

	<div class="loading hide" style='width:15%; margin:auto;'>

		<img src="<?php echo site_url('../images/loading.gif') ?>" style='width:100%'>

	</div>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_year">
		Year
		</i>
	</div>

	<ul class="listleft lists" id="year_list" style="display:none;">

		

	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_color">
		Color
		</i>
	</div>

	<ul class="listleft lists" id="color_list" style="display:none;">

		

	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_part">
		Part Placement
		</i>
	</div>

	<ul class="listleft lists" id="ppl_list" style="display:none;">

		

	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_body">
		Body
		</i>
	</div>

	<ul class="listleft lists" id="body_list" style="display:none;">



	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_engine">
		Engine
		</i>
	</div>

	<ul class="listleft lists" id="engine_list" style="display:none;">



	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_fuel">
		Fuel
		</i>
	</div>

	<ul class="listleft lists" id="fuel_list" style="display:none;">



	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_trans">
		Transmission
		</i>
	</div>

	<ul class="listleft lists" id="trans_list" style="display:none;">



	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_trim">
		Trim
		</i>
	</div>

	<ul class="listleft lists" id="trim_list" style="display:none;">



	</ul>

	<div class="s_title">
		<i class="fa fa-plus-square i_exp" id="e_loc">
		Location
		</i>
	</div>

	<ul class="listleft lists" id="location_list" style="display:none;">



	</ul>

	<?php if($this->session->userdata('u_inf')){ ?>

		<a href="#" id="save_search">Save Search</a>

	<?php } ?>

	<!-- <input type="button" class="btn btn-primary pull-right" id="btnadvsearch" style="margin-bottom:10px;float:right" value="Search"> -->

	<div class="f_foot" align="center">

		<span style='cursor:pointer;color:#21759B' onclick='clear_all();' class='clear_all_btn'>Clear All</span>

	</div>

</div>

<script src="<?php echo base_url();?>js/manage_tables.js" type="text/javascript" language="javascript" charset="UTF-8"></script>



                 <!-- /.modal-dialog -->

<script type="text/javascript">

	// $('#btnadvsearch').click(function(){

	// 	search();

	// });

	function chkYear(){

		$('.ckyear').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		});

		var yf = $('#year_from').val();

		var yt = $('#year_to').val();

		if (yt.length<4) {

			$('#year_to').val(yf);

		};

	}
	
	function clear_head(){
		$('#vin_select').val('');
		search(1,false,0,'head','');
	}
	function clear_s(){
		$('#isearch').val('');
		search(1,false,0,'s','');

	}
	function clear_make(){

		$(".ckmake[value='']").prop('checked',true);
		search(1,false,0,'make','');

	};

	function clear_part(this_part){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.ckpart').each(function(){
			if (this_part!='') {
				if (this_part==$(this).val()) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		search(1,false,0,'part',this_part);


	};
	function clear_yearrange(){
		$('#year_from').val('');
		$('#year_to').val('');
		search(1,false,0,'yearrange','');
	}
	function clear_location(this_loc){

		$('.cklocation').each(function(){
			if (this_loc!='') {
				if ($(this).val()==this_loc) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		search(1,false,0,'location',this_loc);

	}

	function clear_model(){


		$(".ckmodel[value='']").prop('checked',true);

		

		search(1,false,0,'model','');
		

	};

	function clear_year(this_year){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.ckyear').each(function(){
			if (this_year!='') {
				if ($(this).val()==this_year) {
					$(this).prop('checked',false);
				}
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		$('#year_from').val('');

		$('#year_to').val('');

		search(1,false,0,'year',this_year);

	};

	function clear_in(){

		$('input[name="ck-incolor"]:checked').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		});

		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	if ($(this).val()!='') {

		// 		$(this).prop('checked',false);

		// 	}else{

		// 		$(this).prop('checked',true);

		// 	};

		// });

		search();

	}

	function clear_co(this_co){

		$('input[name="ckcolor"]:checked').each(function(){

			if (this_co!='') {
				if ($(this).val()==this_co) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	if ($(this).val()!='') {

		// 		$(this).prop('checked',false);

		// 	}else{

		// 		$(this).prop('checked',true);

		// 	};

		// });
		search(1,false,0,'co',this_co);
	
		// search();

	}

	function clear_ex(){

		$('input[name="ck-excolor"]:checked').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		});

		search();

	}

	function clear_body(this_body){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.ckbody').each(function(){

			if (this_body!='') {
				if ($(this).val()==this_body) {
					$(this).prop('checked',false);
				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		// search();
		search(1,false,0,'body',this_body);


	};

	function clear_engine(this_engine){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.ckengine').each(function(){
			if (this_engine!='') {
				if ($(this).val()==this_engine) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		// search();
		search(1,false,0,'engine',this_engine);


	};

	function clear_fuel(this_fuel){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.ckfuel').each(function(){
			if (this_fuel!='') {
				if ($(this).val()==this_fuel) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});


		// search();
		search(1,false,0,'fuel',this_fuel);


	};

	function clear_trans(this_trans){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.cktrans').each(function(){
			if (this_trans!='') {
				if (this_trans==$(this).val()) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {

					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		search(1,false,0,'trans',this_trans);
		

	};

	function clear_trim(this_trim){

		// $(".ckmake[value='']").prop('checked',true);

		// $(".ckmodel[value='']").prop('checked',true);

		// var mo = $(".ckmodel:checked").val();

		// alert(mo);

		$('.cktrim').each(function(){
			if (this_trim!='') {
				if (this_trim==$(this).val()) {
					$(this).prop('checked',false);

				};
			}else{
				if ($(this).val()!='') {
					$(this).prop('checked',false);

				}else{

					$(this).prop('checked',true);

				};
			}
			

		});

		// $('input[name="ck-incolor"]:checked').each(function(){

		// 	if ($(this).val()!='') {

		// 		$(this).prop('checked',false);

		// 	}else{

		// 		$(this).prop('checked',true);

		// 	};

		// });

		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	if ($(this).val()!='') {

		// 		$(this).prop('checked',false);

		// 	}else{

		// 		$(this).prop('checked',true);

		// 	};

		// });
		search(1,false,0,'trim',this_trim);

	};

	function clear_all(){

		// location.href = "<?php echo site_url('items') ?>";

		$('.search_all').val('');

		localStorage.removeItem('search_all');

		$('#isearch').val('');

		$(".ckmake[value='']").prop('checked',true);

		$(".ckmodel[value='']").prop('checked',true);

		$('.ckyear').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.ckbody').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.ckfuel').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.ckengine').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.cktrans').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.cktrim').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('#year_from').val('');

		$('#year_to').val('');



		$('input[name="ckcolor"]:checked').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('input[name="ckpart"]:checked').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	if ($(this).val()!='') {

		// 		$(this).prop('checked',false);

		// 	}else{

		// 		$(this).prop('checked',true);

		// 	};

		// })
		$('.cklocation').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		});

		$('#vin_select').val('');

		search(1,false,0,'all','');

	};

	function clear_all_without_search(evn){

		// location.href = "<?php echo site_url('items') ?>";

		$('#isearch').val('');

		$(".ckmake[value='']").prop('checked',true);

		$(".ckmodel[value='']").prop('checked',true);

		$('.ckyear').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})
		$('input[name="ckcolor"]:checked').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})
		$('input[name="ckpart"]:checked').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.ckbody').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.ckengine').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.ckfuel').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.cktrans').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		$('.cktrim').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		})

		// $('#year_from').val('');

		// $('#year_to').val('');

		$('.cklocation').each(function(){

			if ($(this).val()!='') {

				$(this).prop('checked',false);

			}else{

				$(this).prop('checked',true);

			};

		});


		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	if ($(this).val()!='') {

		// 		$(this).prop('checked',false);

		// 	}else{

		// 		$(this).prop('checked',true);

		// 	};

		// })
		if (evn.hasClass('search_all')) {
			$('#vin_select').val('');
		};
		if (evn.attr('id')=='vin_select') {
			localStorage.removeItem('search_all');

			$('#search').val('');
		};

		// search();

	};
	
	// filter_count=0;
	function search(page,event,is_make,fi,fid){
		$('body').addClass('loading');
		

		var evn = '';

		if (event) {

			evn = $(event.target);

			if (evn.hasClass('search_all') || evn.attr('id')=='vin_select') {

				clear_all_without_search(evn);

			};

		};

		// console.log($(event.target).attr('class'));

		var vin = $('#vin_select').val();

		var s = $('#search').val();
		

		

		if (s!='') {

			localStorage.setItem('search_all',s);

		};

		

		var search = $('input[name="isearch"]').val();

		var make=$('input[name="ckmake"]:checked').val();

		var model=$('input[name="ckmodel"]:checked').val();




		// return search;

		// var color=$('input[name="ckcolor"]:checked').val();

		var year='';

		$('input[name="ckyear"]:checked').each(function(){

			year+=$(this).val()+'_';

		})

		var body='';

		$('input[name="ckbody"]:checked').each(function(){

			body+=$(this).val()+'_';

		})

		var engine='';

		$('input[name="ckengine"]:checked').each(function(){

			engine+=$(this).val()+'_';

		})

		var fuel='';

		$('input[name="ckfuel"]:checked').each(function(){

			fuel+=$(this).val()+'_';

		})

		var trans='';

		$('input[name="cktrans"]:checked').each(function(){

			trans+=$(this).val()+'_';

		})

		var trim='';

		$('input[name="cktrim"]:checked').each(function(){

			trim+=$(this).val()+'_';

		})

		var color=''

		$('input[name="ckcolor"]:checked').each(function(){

			color+=$(this).val()+'_';

		})

		var part=''

		$('input[name="ckpart"]:checked').each(function(){

			part+=$(this).val()+'_';

		})

		var loc=''

		$('input[name="cklocation"]:checked').each(function(){

			loc+=$(this).val()+'_';

		})

		// var ex_color=''

		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	ex_color+=$(this).val()+'_';

		// })

		var name=$('.s_partname').val();

		var p_num=$('.per_page').val();

		var yf = $('#year_from').val();

		var yt = $('#year_to').val();


		
		var sort_up = $('.sort_d');
		var sort_down = $('.sort_a');
		// var ud=0;
		// var sn=2;
		var dud = 'DESC';
		var dsn = 'barcode';

		if (sort_up.attr('sort')) {
			// sn = sort_up.attr('rel');
			// ud = 1;
			dsn = sort_up.attr('sort');
			dud = 'DESC';
		};
		if (sort_down.attr('sort')) {
			// ud = 0;
			// sn = sort_down.attr('rel');
			dsn = sort_down.attr('sort');
			dud = 'ASC';
		};
		


		// console.log(dsn);
		// console.log(dud);
		



		$.post("<?php echo site_url('items/search_vin')?>",

			{

				search:search,

				'page':page,

				m:make,

				mo:model,

				y:year,

				c:color,

				// ex_c:ex_color,

				p_num:p_num,

				year_from:yf,

				year_to:yt,

				vin:vin,

				body:body,

				engine:engine,

				fuel:fuel,

				trans:trans,

				trim:trim,

				ppl:part,

				// search_all:'<?php echo $_GET['s'] ?>'

				loc:loc,

				search_all:s,
				dud:dud,
				dsn:dsn,
				cl_f:fi,
				cl_fid:fid

			},

			function(data){

				if (data.data!='') {
					


					// if($('#sortable_table tbody tr').length >1)

					// {
						

					// 	$("#sortable_table").tablesorter(

					// 	{

					// 		// cssChildRow: "tablesorter-childRow",
							
					// 			// sortList: [[sn,ud]],

							

					// 		// "order":[[2,'desc']],

					// 		// headers:

					// 		// {

					// 		// 	0: { sorter: false},
					// 		// 	1: { sorter:false},
					// 		// 	14: {sorter: false}

					// 		// 	// 3: { sorter: false}

					// 		// },

					// 	});

						

					// }

				    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');

				    



					$('#sortable_table tbody').html(data.data);

					$('.dataTables_paginate').html(data.pagination.pagination);

					$('.page_result p').html(data.result_row);
					$('.filters_applied').html(data.filters_applied);


					// $('#isearch').removeClass('loadinggif');

					



					var tb_pathToImage = "images/loading_animation.gif";

					tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox

					imgLoader = new Image();// preload image

					imgLoader.src = tb_pathToImage;



					// update_sortable_table();



					// init_table_sorting();

				    enable_select_all();
				    enable_checkboxes();

				    enable_row_selection();

					// $("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});

				    

				    if(page==undefined || page==1){
						$('body').removeClass('loading');
				    	getmodel(event,is_make);
				    }
					// setTimeout(function(){getmodel(event,is_make);}, 200);

					// $('#vin_select').select2();

				}else{

					$('#sortable_table tbody').html(data.nodata);

					$('.page_result p').html(data.result_row);

					$('.dataTables_paginate').html(data.pagination.pagination);

					





				};

					

				    // enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');

				    // enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');

				// setTimeout(function(){}, 10);
				$('body').removeClass('loading');
	        	$('.ac_results').css('display','none');


			},'json');

		// location. href="<?php echo site_url('vinnumbers/search_vin');?>?n="+name+"&m="+make+"&mo="+model+"&y="+year+"&in_c="+in_color+'&ex_c='+ex_color+'&p_num='+p_num;

		

	}

	function getmodel(event,is_make){
		// $('body').removeClass('loading');

		// $('.lists').addClass('hide');

		// $('.loading').removeClass('hide');
		// $('.filter_loading').removeClass('hide');

		// $('body').addClass('loading');

		var search = $('input[name="isearch"]').val();

		// var s = $('input[name="isearch"]').val();

		var make_id=$('input[name="ckmake"]:checked').val();

		var vin = $('#vin_select').val();

		// var make_id=$(event.target).val();

		var model='';

		// if(is_make==0)

			model=$('input[name="ckmodel"]:checked').val();

		// var color=$('input[name="ckcolor"]:checked').val();

		var year=''

		$('input[name="ckyear"]:checked').each(function(){

			year+=$(this).val()+'_';

		})



		var body='';

		$('input[name="ckbody"]:checked').each(function(){

			body+=$(this).val()+'_';

		})

		var engine='';

		$('input[name="ckengine"]:checked').each(function(){

			engine+=$(this).val()+'_';

		})

		var fuel='';

		$('input[name="ckfuel"]:checked').each(function(){

			fuel+=$(this).val()+'_';

		})

		var trans='';

		$('input[name="cktrans"]:checked').each(function(){

			trans+=$(this).val()+'_';

		})

		var trim='';

		$('input[name="cktrim"]:checked').each(function(){

			trim+=$(this).val()+'_';

		})

		var color=''

		$('input[name="ckcolor"]:checked').each(function(){

			color+=$(this).val()+'_';

		})

		var part=''

		$('input[name="ckpart"]:checked').each(function(){

			part+=$(this).val()+'_';

		})

		var loc=''

		$('input[name="cklocation"]:checked').each(function(){

			loc+=$(this).val()+'_';

		})

		// var ex_color=''

		// $('input[name="ck-excolor"]:checked').each(function(){

		// 	ex_color+=$(this).val()+'_';

		// })

		var name=$('.s_partname').val();

		var s_all = $('.search_all').val();

		var yf = $('#year_from').val();

		var yt = $('#year_to').val();

		if (year!='_') {

			$('#year_from').val('');

			$('#year_to').val('');

		};

		// var s = $('#isearch').val();

		var url="<?php echo site_url('items/gemodel_pos')?>";

		var savename=$('#save_name').val();



		$.ajax({

            url:url,

            type:"POST",

            datatype:"Json",

            async:false,

            data:{

            		'make':make_id,

            		'model':model,

            		'color':color,

            		// 'ex_color':ex_color,

            		'year':year,

            		'savename':savename,

            		'name':name,

            		'year_from':yf,

            		'year_to':yt,

            		'search':search,

            		vin:vin,

            		'body':body,

            		'engine':engine,

            		'fuel':fuel,

            		'trans':trans,

            		'trim':trim,

            		'ppl':part,

            		'loc':loc,

            		search_all:s_all

            	},

            success:function(data) {

            	$("#model_list").html(data.model);

            	$("#year_list").html(data.year);

            	$("#color_list").html(data.color);

            	// $("#ex_color_list").html(data.ex_color);

            	$("#make_list").html(data.make);

            	$("#body_list").html(data.body);

            	$("#engine_list").html(data.engine);

            	$("#fuel_list").html(data.fuel);

            	$("#trans_list").html(data.trans);

            	$("#trim_list").html(data.trim);

            	$("#head_list").html(data.vin);

            	$("#ppl_list").html(data.ppl);

            	$("#location_list").html(data.loc);

				// $('.lists').removeClass('hide');

				$('.loading').addClass('hide');

				// $('#vin_select').select2();

             	// search();		

				// setTimeout(function(){$('.filter_loading').addClass('hide')}, 50);

	        	$('.ac_results').css('display','none');
				
				$('body').removeClass('loading');

            }

          })

	}

	function yearft(){

		

		if ($('#year_from').val()!='' && $('#year_to').val()!='') {



			search(1,false,0,'yearrange','yft');

		};

		

		// search();

		// $('#year_to').focus();

		

	}
	$('.i_exp').click(function(){
		open_close_filter($(this));
	});
	function open_close_filter(ele){
		if (ele.parent().next().closest('ul').is(':visible')) {
			ele.removeClass('fa-minus-square');
			ele.addClass('fa-plus-square');
			ele.parent().next().closest('ul').slideUp();
		}else{
			ele.removeClass('fa-plus-square');
			ele.addClass('fa-minus-square');
			ele.parent().next().closest('ul').slideDown();

		}
	}

	$('.i_exp_all').click(function(){
		var cl = $('.i_exp_all');
		if (cl.hasClass('fa-plus-square')) {
			open_close_all_filter('plus');
		}else if (cl.hasClass('fa-minus-square')) {
			open_close_all_filter('minus');
		};
	});

	function open_close_all_filter(all){
		var iexp = $('.i_exp');
		// alert(all);
		if (all=='plus') {
			iexp.each(function(){
				var ele = $(this);
				ele.parent().next().closest('ul').slideDown();
				ele.removeClass('fa-plus-square');
				ele.addClass('fa-minus-square');
			});
			$('.i_exp_all').removeClass('fa-plus-square');
			$('.i_exp_all').addClass('fa-minus-square');
		};
		if (all=='minus') {
			iexp.each(function(){
				var ele = $(this);
				ele.removeClass('fa-minus-square');
				ele.addClass('fa-plus-square');
				ele.parent().next().closest('ul').slideUp();
			});
			$('.i_exp_all').removeClass('fa-minus-square');
			$('.i_exp_all').addClass('fa-plus-square');
		};
		// if (all.hasClass('fa-minus-square')) {
		// 	// console.log(all);
		// 	// iexp.each(function(){
		// 	// 	var ele = $(this);
		// 	// 	ele.removeClass('fa-minus-square');
		// 	// 	ele.addClass('fa-plus-square');
		// 	// 	ele.parent().next().closest('ul').slideUp();
		// 	// });
		// 	all.removeClass('fa-minus-square');
		// 	all.addClass('fa-plus-square');

		// };

	}

	function search_filter(text,f){
		// $('body').addClass('loading');
			var li;
			switch(f){
				case 'make': li=$('.ckmake');break;
				case 'model': li=$('.ckmodel');break;
				case 'year': li=$('.ckyear');break;
				case 'color': li=$('.ckcolor');break;
				case 'part': li=$('.ckpart');break;
				case 'body': li=$('.ckbody');break;
				case 'engine': li=$('.ckengine');break;
				case 'fuel': li=$('.ckfuel');break;
				case 'trans': li=$('.cktrans');break;
				case 'trim': li=$('.cktrim');break;
				case 'location': li=$('.cklocation');break;
				default: console.log('no field');break;
			}

			li.each(function(){
				var label = $(this).closest('label');

				var preg = new RegExp(text,'gi');

				// console.log(label.text().match(preg));
				if (label.text().match(preg)) {
					label.closest('li').show();
				}else{
					label.closest('li').hide();
				}
			});
		
	}
	
	// function test_data(){
	// 	$.ajax({
	// 		url:"<?php echo site_url('items/test_data') ?>",
	// 		type:'POST',
	// 		dataType:'Json',
	// 		data:{
	// 			make_id:'26'
	// 		},
	// 		success:function(r){
	// 			console.log(r);
	// 		}
	// 	});
	// }
	function search_this_list(text){
			var tr = $('#sortable_table tbody tr');

			// console.log(text);
			// console.log(td);
			tr.each(function(index) {
				var label = $(this).find('td');
				var preg = new RegExp(text,'gi');

		       	if (label.text().match(preg)) {
					label.closest('tr').show();
				}else{
					label.closest('tr').hide();
				}
		    });
	}
</script>