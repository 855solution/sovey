<?php $this->load->view("partial/header"); ?>



<script type="text/javascript">
$(document).ready(function()
{
	$('#search').change(function(){
		var s = $('.ac_over').text()
		if (s=='') {return};
		window.location='index.php/items/search?s='+s;

	});

	
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo $this->lang->line('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','index.php/items/generate_barcodes/'+selected.join(':'));
    });
    
    $("#low_inventory").click(function()
    {
    	if ($('#low_inventory').attr('checked')) {
    		$('#items_filter_form').submit();
    	}else{
    		location.href='<?php echo site_url('items') ?>';
    	
    	};
    	
    });

    $("#is_serialized").click(function()
    {
    	if ($('#is_serialized').attr('checked')) {
    		$('#items_filter_form').submit();

    	}else{
    		location.href='<?php echo site_url('items') ?>';

    	};
    });

    $("#no_description").click(function()
    {
    	if ($('#no_description').attr('checked')){
    		$('#items_filter_form').submit();

    	}else{
    		location.href='<?php echo site_url('items') ?>';
    		
    	};
    });


    
    $("#make_model_year_search").click(function()
  	 {
   	 $('#items_filter_form').submit();

   	});
    /*
    $("#model_id").change(function()
    	{
    	$('#items_filter_form').submit();

    });
    $("#year").change(function()
 	{
 		$('#items_filter_form').submit();

 	});
    */	    

});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				// 8: { sorter: false},
				16: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		var selected_item_ids=get_selected_values();
		for(k=0;k<selected_item_ids.length;k++)
		{
			update_row(selected_item_ids[k],'<?php echo site_url("$controller_name/get_row")?>');
		}
		set_feedback(response.message,'success_message',false);
	}
}
$(document).ready(function(){
	$("#clearitem").click(function(){
		var conf=confirm("Are you sure to clear empty item.");
		if(conf==true){
            var url="<?php echo site_url('items/clearitem')?>";
			$.ajax({
	            url:url,
	            type:"POST",
	            datatype:"Json",
	            async:false,
	            data:{},
	            success:function(data) {
	            	alert(data);
	             
	            }
	          })
		}
	})
})

function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="block";
        }
}

</script>

<!-- Make javascript -->

 <script language="JavaScript" type="text/javascript">
				$(function()
				{
					$('#make_id').chainSelect('#model_id','<?php echo site_url('items/combobox') ?>',
					{ 
						before:function (target) 
						//before request hide the target combobox and display the loading message
						{ 
							$("#loading").css("display","block");
							$(target).css("display","none");
							
						},
						after:function (target) 
						//after request show the target combobox and hide the loading message
						{ 
							$("#loading").css("display","none");
							$(target).css("display","inline");
						}
						
					});
				});
            </script>
<!--  -->
<style type="text/css">
	#ispending td{
		background:rgba(255, 0, 0, 0.14);
	}
	#ispending:hover{
		background:#e9e9e9;
	}
	#spa span{
		/*padding: 0 1% !important;*/
	}
	#pagination{
		padding: 1% 0;
		float: right;
	}
	#pagination a{
		border: 1px solid #21759B;
		background: #FFF none repeat scroll 0% 0%;
		/*color: #00F;*/
		border-radius: 5px;
		padding: 5px 10px;
	}
	#pagination strong{
		border: 1px solid #21759B;
		background: #21759B none repeat scroll 0% 0%;
		color: white;
		border-radius: 5px;
		padding: 5px 10px;
	}
	#table_holder{
		width: 87%;
		float: right;
		margin-top: 0;
	}
	#table_action_header{
		width:86%;
		float: right;
	}
</style>
<div id="title_bar">
	<div id="title" class="float_left"><?php echo $this->lang->line('common_list_of').' '.$this->lang->line('module_'.$controller_name); ?></div>
	<div id="new_button">
		<?php 
		$js = 'id="make_id"';
		
		//echo anchor("$controller_name/view/-1/width:$form_width",
		// "<div class='big_button' style='float: left;'><span>".$this->lang->line($controller_name.'_new')."</span></div>",
		//array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new'));
		//);
		?>
		
		
		<a href="<?php echo site_url("$controller_name/view/-1/width:$form_width");?>">
		<?php echo "<div class='big_button' style='float: left;'>
		<span>".$this->lang->line($controller_name.'_new')."</span>
		</div>";?>
		</a>
		
		
		<?php echo anchor("$controller_name/excel_import/width:$form_width",
		"<div class='big_button' style='float: left;'><span>Excel Import</span></div>",
		array('class'=>'thickbox none','title'=>'Import Items from Excel'));
		?>
	</div>
</div>


<!-- <div>
	<select id="numpage" class="form-control" name="numpage">
		<?php 
			$val=20;
			if (isset($_GET['p_num'])) {
				$val = $_GET['p_num'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
		?>
		<option value="20" <?php if($val==20)echo "selected"?>>20</option>
		<option value="50" <?php if($val==50)echo "selected"?>>50</option>
		<option value="100" <?php if($val==100)echo "selected"?>>100</option>
	</select>
</div>
<div id="pagination">
<?php echo $this->pagination->create_links();?>
<div style="float:right">
	<?php 
			$val=20;
			if (isset($_GET['p_num'])) {
				$val = $_GET['p_num'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
	<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
</div>
</div> -->


<div id="table_action_header">
	<ul>
		<li class="float_left"><span><?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete')); ?></span></li>
		<li class="float_left"><span><?php echo anchor("$controller_name/bulk_edit/width:$form_width",$this->lang->line("items_bulk_edit"),array('id'=>'bulk_edit','title'=>$this->lang->line('items_edit_multiple_items'))); ?></span></li>
		<li class="float_left"><span><?php echo anchor("$controller_name/generate_barcodes",$this->lang->line("items_generate_barcodes"),array('id'=>'generate_barcodes', 'target' =>'_blank','title'=>$this->lang->line('items_generate_barcodes'))); ?></span></li>
		<li class="float_left" id="clearitem"><span>Clear Empty Quantity(2 weeks)</span></li>
		

		<li class="float_right">
		<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
		<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
		
		<input type="text" name ='search' id='search'/>
		</li>
		<div class="float_right" style="margin-top:-1px">
			<select id="vin_select" name="v">
				<option value="">Please Select</option>
				<?php 
					foreach ($vinnumber as $vin) {
				?>
					<option value="<?php echo $vin->vinnumber_name?>"><?php echo $vin->vinnumber_name ?></option>
				<?php
					}
				?>
			</select>
		</div>
		</form>
	</ul>
</div>
<div style="margin-left:-50px">
	<?php $this->load->view('items/filter'); ?>
</div>
<div id="table_holder">
	<!-- TOP PAGINATION -->
	<div style="float:left;width:100%;height:25px;">
		<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
		?>
		<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div style="float:left;">
		Show
		<select id="numpage" class="numpage" name="numpage">
			<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div>
	<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	</div>
	<!-- END TOP PAGINATION -->
	
 <table id="sortable_table" class="tablesorter">
 	<thead>
 		<th><input type="checkbox" id="select_all" /></th>
 		<th>Images</th>
 		<th>Barcode</th>
 		<th>Item Number</th>
 		<th>Name</th>
 		<th>Khmer Name</th>
 		<th>Make</th>
 		<th>Model</th>
 		<th>Year</th>
 		<th>Color</th>
 		<th>PPL</th>
 		<th>Brand</th>
 		<th>Cost Price</th>
 		<th>Unit Price</th>
 		<th>Quantiy</th>
 		<th>Inventory</th>
 	</thead>
 	<tbody>
 		<?php foreach ($data as $item) {
 			$pend = '';
			if ($item->is_pending == 1) {
				$pend = 'ispending';
			}
			$image=$this->Item->getdefaultimg($item->item_id);
			$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
			if($image!=''){
				if(file_exists(FCPATH."uploads/thumb/".$image)){
					$im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
			            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
			      }
			}
		    $img_path=img($im);
		
 		?>
 			<tr id="<?php echo $pend ?>">
 				<td><input type='checkbox' id='item_<?php echo $item->item_id?>' value='<?php echo $item->item_id?>'/></td>
 				<td><?php echo $img_path ?></td>
				<td><?php echo anchor($controller_name."/view/$item->item_id", $item->barcode,array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_update')))?></td>
				<td><?php echo $item->item_number ?></td>
				<td><?php echo $item->name ?></td>
				<td><?php echo $item->khmername_name ?></td>
				<td><?php echo $item->make_name ?></td>
				<td><?php echo $item->model_name ?></td>
				<td><?php echo $item->year ?></td>
				<td><?php echo $item->color_name ?></td>
				<td><?php echo $item->partplacement_name ?></td>
				<td><?php echo $item->branch_name ?></td>
				<td><?php echo $item->cost_price ?></td>
				<td><?php echo $item->unit_price ?></td>
				<td><?php echo $item->quantity ?></td>
				<td><?php echo anchor($controller_name."/count_details/$item->item_id", $this->lang->line('common_det'),array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_details_count')))?></td>

 			</tr>
 		<?php
 		} ?>
 	</tbody>
 </table>

 <!-- BOTTOM PAGINATION -->
	<div style="float:left;width:50%;height:25px;">
	<?php 
			$val=10;
			if (isset($_GET['p'])) {
				$val = $_GET['p'];
			}
			if (isset($_GET['per_page'])) {
			 	$per = $_GET['per_page'];
			 } 
	?>
	<p>Showing <?php if($per==''){echo 1;}else{echo $per+1;} ?> to <?php if($per+$val<$total_rows){echo $per+$val;}else{echo $total_rows;} ?> of <?php echo $total_rows ?> results</p>
	</div>
	<div style="float:left;width:51%;">
		Show
		<select id="numpage" class="numpage" name="numpage">
			<?php 
				$val=10;
				if (isset($_GET['p'])) {
					$val = $_GET['p'];
				}
				if (isset($_GET['per_page'])) {
				 	$per = $_GET['per_page'];
				 } 
				 for($i=10;$i<=100;$i+=10){
			?>
				<option value="<?php echo $i ?>" <?php if($val==$i)echo "selected"?>><?php echo $i ?></option>
			<?php }?>
		</select>
		results
	</div>
	<div id="pagination">
	<?php echo $this->pagination->create_links();?>
	</div>
	<!-- END BOTTOM PAGINATION -->
</div>
<div style="clear:both"></div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript">
	$('#numpage').change(function(){
		var per = $('#numpage').val();
		var make=$('input[name="ckmake"]:checked').val();
		var model=$('input[name="ckmodel"]:checked').val();
		// var color=$('input[name="ckcolor"]:checked').val();
		var year='';
		$('input[name="ckyear"]:checked').each(function(){
			year+=$(this).val()+'_';
		})
		var color='';
		$('input[name="ckcolor"]:checked').each(function(){
			color+=$(this).val()+'_';
		})
		location.href="<?php echo site_url('items/search_vin');?>?n="+name+"&m="+make+"&mo="+model+"&y="+year+"&c="+color+'&p_num='+per;
		
	});
	$('#khmername_id').select2();
	$('#make_id').select2();
	$('#model_id').select2();
	$('#year').select2();
	$('#yearto').select2();
	$('#vin_select').select2();

	// $('#vin_select').change(function(){
	// 	var vin = $(this).val()
	// 	$.post('<?php echo site_url("$controller_name/get_vin")?>',
	// 		{v:vin},
	// 		function(data){
	// 			if (data) {
	// 				console.log('Session success');
	// 			};
	// 		}
	// 	)
	// });
	// $('#search').keyup(function(){
	// 	var vin = $('#vin_select').val();
	// 	var search = $('#search').val();
	// 	// alert(vin);
	// 	$.post('<?php echo site_url("$controller_name/suggest")?>',
	// 		{
	// 			v:vin,
	// 			q:search,
	// 			limit:100
	// 			},
	// 		function(data){
	// 			console.log(data);
	// 		},'json'
	// 	)
	// });
</script>
