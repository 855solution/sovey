    <script type="text/javascript" src="<?=base_url().'assets/js/dropzone.js'?>"></script>
   <style type="text/css">
    button.btn-delete-pic
    {
      position: relative;
      cursor: pointer;
      float: right;
      color:#f94f4f;
    }
    table tbody tr td img{width: 20px; margin-right: 10px}
    ul,ol{
        margin-bottom: 0px !important;
    }
    #img_row{padding: 3px 0px !important}
    .hide{display: none !important;}
    .img_row>div{
        width: 45%;
        float: left;
    }
    /*dz customer style*/
    g
    {
      display: none;
    }
    .dz-preview
    {
      height: 150px;
      width: 10%;
      display: inline-block;
      float: left;
      margin: 2%;
    }
    .dz-remove
    {
      top: -110px;
      position: relative;
    }
    .dz-remove:hover
    {
      color: red !important;
    }
    /*dz customer style*/
    #img-previewer
    {
      height: 210px;
      width: 60%;
      border: 1px solid gray;
      overflow: auto;
      cursor: pointer;
    }
    .remove_img{
        margin-bottom:-23px;position: relative; margin-left:-6px !important;
        width: 24px;
    }
    .top_action_button img{width: 28px; margin-top: 5px;}
    .saveloading{width:35px;}
    .remove_img:hover{
        cursor: pointer;
    }
    a{
        cursor: pointer;
    }
    .previewupl:hover{
        cursor: pointer;
    }
    .datepicker {z-index: 9999;}

    .name_div .select2-container{
        width: 200px !important;
    }

    /*Upload Section*/

    .file-loading {
      top: 0;
      right: 0;
      width: 25px;
      height: 25px;
      font-size: 999px;
      text-align: right;
      color: #fff;
      background: transparent url('../img/loading.gif') top left no-repeat;
      border: none;
  }

  .file-object {
      margin: 0 0 -5px 0;
      padding: 0;
  }

  .btn-file {
      position: relative;
      overflow: hidden;
  }

  .btn-file input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      min-width: 100%;
      min-height: 100%;
      text-align: right;
      opacity: 0;
      background: none repeat scroll 0 0 transparent;
      cursor: inherit;
      display: block;
  }

  .file-caption-name {
      display: inline-block;
      overflow: hidden;
      height: 20px;
      word-break: break-all;
  }

  .input-group-lg .file-caption-name {
      height: 25px;
  }

  .file-preview-detail-modal {
      text-align: left;
  }

  .file-error-message {
      background-color: #f2dede;
      color: #a94442;
      text-align: center;
      border-radius: 5px;
      padding: 5px 10px 5px 5px;
  }

  .file-error-message pre, .file-error-message ul {
      margin: 5px 0;
      text-align: left;
  }

  .file-caption-disabled {
      background-color: #EEEEEE;
      cursor: not-allowed;
      opacity: 1;
  }

  .file-preview {
      border-radius: 5px;
      border: 1px solid #ddd;
      padding: 5px;
      width: 100%;
      margin-bottom: 5px;
  }

  .file-preview-frame {
      display: table;
      margin: 8px;
      height: 160px;
      border: 1px solid #ddd;
      box-shadow: 1px 1px 5px 0 #a2958a;
      padding: 6px;
      float: left;
      text-align: center;
      vertical-align: middle;
  }

  .file-preview-frame:not(.file-preview-error):hover {
      box-shadow: 3px 3px 5px 0 #333;
  }

  .file-preview-image {
      height: 160px;
      vertical-align: middle;
  }

  .file-preview-text {
      text-align: left;
      width: 160px;
      margin-bottom: 2px;
      color: #428bca;
      background: #fff;
      overflow-x: hidden;
  }

  .file-preview-other {
      display: table-cell;
      text-align: center;
      vertical-align: middle;
      width: 160px;
      height: 160px;
      border: 2px solid #999;
      border-radius: 30px;
      opacity: 0.8;
  }

  .file-actions, .file-other-error {
      text-align: left;
  }

  .file-icon-lg {
      font-size: 1.2em;
  }

  .file-icon-2x {
      font-size: 2.4em;
  }

  .file-icon-4x {
      font-size: 4.8em;
  }

  .file-input-new .file-preview, .file-input-new .close, .file-input-new .glyphicon-file,
  .file-input-new .fileinput-remove-button, .file-input-new .fileinput-upload-button,
  .file-input-ajax-new .fileinput-remove-button, .file-input-ajax-new .fileinput-upload-button {
      display: none;
  }

  .loading {
      background: transparent url('../img/loading.gif') no-repeat scroll center center content-box !important;
  }

  .file-actions {
      margin-top: 15px;
  }

  .file-footer-buttons {
      float: right;
  }

  .file-upload-indicator {
      padding-top: 2px;
      cursor: default;
      opacity: 0.8;
      width: 60%;
  }

  .file-upload-indicator:hover {
      font-weight: bold;
      opacity: 1;
  }

  .file-footer-caption {
      display: block;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      width: 160px;
      text-align: center;
      padding-top: 4px;
      font-size: 11px;
      color: #777;
      margin: 5px auto 10px auto;
  }

  .file-preview-error {
      opacity: 0.65;
      box-shadow: none;
  }

  .file-preview-frame:not(.file-preview-error) .file-footer-caption:hover {
      color: #000;
  }

  .file-drop-zone {
      border: 1px dashed #aaa;
      border-radius: 4px;
      height: 100%;
      text-align: center;
      vertical-align: middle;
      margin: 12px 15px 12px 12px;
      padding: 5px;
  }

  .file-drop-zone-title {
      color: #aaa;
      font-size: 40px;
      padding: 85px 10px;
  }

  .highlighted {
      border: 2px dashed #999 !important;
      background-color: #f0f0f0;
  }

  .file-uploading {
      background: url('../img/loading-sm.gif') no-repeat center bottom 10px;
      opacity: 0.65;
  }

  .file-error-message .close {
      margin-top: 5px;
  }

  .file-thumb-progress .progress, .file-thumb-progress .progress-bar {
      height: 10px;
      font-size: 9px;
      line-height: 10px;
  }

  .file-thumbnail-footer {
      position: relative;
  }

  .file-thumb-progress {
      position: absolute;
      top: 22px;
      left: 0;
      right: 0;
  }
  /*upload section*/
  /*style when update item*/
  <?php 
    if(isset($loader) AND $loader = 'form')
    {
  ?>
    #img-previewer {
        height: 210px;
        width: 96%;
        border: 1px solid gray;
    }
    .dz-preview {
        width: 25%;
        margin: 4%;
    }
    .dz-image > img
    {
      width: 100%;
    }
  <?php
    }
  ?>
  /*style when update item*/
</style>
  <script src="<?=base_url()?>/assets/js/fileinput.js" type="text/javascript" language="javascript" charset="UTF-8"></script>

   <script type="text/javascript">

   
   // +++++++++++++++++++++ Image upload++++++++++++++++++
   function PreviewImage(event) {
        // console.log($(event.target));
        $(event.target).parent().parent().find('#tem_img').remove();
        var uppreview=$(event.target).parent().parent().find('#imgpreview');
        var oFReader = new FileReader();
        oFReader.readAsDataURL($(event.target)[0].files[0]);



        oFReader.onload = function (oFREvent) {
            // console.log(oFREvent.target);
            uppreview.attr('src',oFREvent.target.result);
            // uppreview.style.backgroundImage = "none";
        };
        $(event.target).parent().parent().find('.remove_img').removeClass('hide');
        $(event.target).removeClass('bl');
        var slideid=$(event.target).parent().parent().find('.remove_img').attr('rel');
        var del=$('#deleteimg').val()
        $('#deleteimg').val(del+','+slideid);
        addnewimg();
    };
    function addnewimg(){
        var exst=false;
        var ma=0;
        $('.uploadimg').each(function(){
            var ma_o=$(this).parent().parent().find('#order').val();
            var f = $(this)[0].files[0];
            // console.log($(this));
            // alert(f);
            if(ma<ma_o)
                ma=ma_o;
            if(f==undefined && $(this).hasClass('bl')){
                exst=true;
            }
        })
        if(exst==false)
            $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
                                        "<div class='img' style='width:130px; float:left'> "+ 
                                            "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
                                            "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
                                            "<input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
                                            "<input type='text' value='' class='removeimgid hide' />"+
                                        "</div>"+
                                        "<div class='txt_order' style='width:130px; float:left; margin-left:30px; margin-top:18px;'>"+
                                            "<label class='col-lg-1 control-label' style='margin-top:15px !important;' >Order</label>"+
                                            "<div class='col-lg-4' style='margin-top:15px !important;''>"+ 
                                                "<div class='col-md-12'>"+
                                                    "<input type='text' readonly class='form-control input-sm order'  name='order[]' id='order' value='"+(Number(ma)+1)+"'> "+
                                                "</div>"+ 
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                    "<p style='clear:both;'></p>");
    }
       
    function choosimg(event){
        $(event.target).parent().parent().find('#uploadImage').click();
    }
    // +++++++++++++++++++++++++++++remove upload pic+++++++++++++++++++ 
    function remove_img(event){
        var picid=$(event.target).attr('rel');
        $(event.target).parent().parent().remove();
        var delete_img=$('#deleteimg').val();
        if(picid!='')
            $('#deleteimg').val(delete_img+','+picid);
    }

    
   // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   $(document).ready(function(){	

    // $("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});
    // $("#category").result(function(event, data, formatted){});
    // $("#category").search();
    // $('.vinnumber').select2();
       
	 $("div .value_selected").hide(); 
	
	   
	 var con=$(".condition_id option:selected").text();
	 var condition=new String(con.toLowerCase()); 
	if(condition.replace(/\s/g, "")=="used"){
		$("div .value_selected").show();   
		}

	
   $('.condition_id').change(function (){
	   var str = "";
	   var compare="used";
       $(".condition_id option:selected").each(function () {
             str += $(this).text() + " ";
           });
       var firstString = new String(str.toLowerCase());
      
       
   		if(firstString.replace(/\s/g, "")=="used"){
   		 //$("div .value_selected").text(str);
   			$("div .value_selected").show();
   	   		}else{
			 $("div .value_selected").hide();
   	   	   		}
  		
	   });

   
	$('.imageDelete').click(function(event){
		var id= $(this).attr('title');
		var msg=false;
		msg=confirm("Are you sure, delete this picture?");
		//alert(msg);
		if(msg==true){
			$(this).prev().andSelf().remove();	
			 $.post('index.php/items/deleteImage', {'imageid':id},function(result)
				        {
			        
				        });	
			}
		});

   
   
	$('.grade_id').change(function (){
		var unitpriceA=$('.unitpriceA').val();	
		var costpriceA=$('.costpriceA').val();

		var unitpriceB=$('.unitpriceB').val();	
		var costpriceB=$('.costpriceB').val();

		var unitpriceC=$('.unitpriceC').val();	
		var costpriceC=$('.costpriceC').val();


		var str = "";
		  $(".grade_id option:selected").each(function () {
	             str += $(this).text() + " ";
	           });
          
		var strValue = new String(str.toLowerCase());  
		

		if(strValue.replace(/\s/g, "")=="a"){		
			$("#cost_price").val(costpriceA);
			$("#unit_price").val(unitpriceA);
			}
		
		if(strValue.replace(/\s/g, "")=="b"){		
			$("#cost_price").val(costpriceB);
			$("#unit_price").val(unitpriceB);
			}
		
		if(strValue.replace(/\s/g, "")=="c"){		
			$("#cost_price").val(costpriceC);
			$("#unit_price").val(unitpriceC);
			}
		
		});
		//$("#quantity").val(1);
	 
		   });
   </script>
   
     <script type="text/javascript">
      $(".clmake").select2();
       $(".khname").select2();
       // $('.name_id').select2();
    </script> 
<style type="text/css">
    .clmake{
        width: 220px;
    }
     .khname{
        width: 210px;
    }
    .pre_image{
        width: 100%;
        cursor: pointer;
    }
    .img_wrap{
        border: 1px solid #dadada;
        width: 130px;
        display: inline-block;
    }
    .removeimg{
        cursor: pointer;
        position: relative;
        float: right;

    }
</style>
<?php if ($item_info->item_id==''): ?>
    <div class="left_side">
<?php endif ?>
  <input type="hidden" name="unitpriceA" class="unitpriceA" value="<?php echo $unitpriceA;?>"/>
  <input type="hidden" name="costpriceA" class="costpriceA" value="<?php echo $costpriceA;?>"/>
  
  <input type="hidden" name="unitpriceB" class="unitpriceB" value="<?php echo $unitpriceB;?>"/>
  <input type="hidden" name="costpriceB" class="costpriceB" value="<?php echo $costpriceB;?>"/>
  
  <input type="hidden" name="unitpriceC" class="unitpriceC" value="<?php echo $unitpriceC;?>"/>
  <input type="hidden" name="costpriceC" class="costpriceC" value="<?php echo $costpriceC;?>"/>

  <input type="hidden" name="item_id_spare" class="item_id" value="<?php echo $item_info->item_id ?>">

    <input type="hidden" value="" id="is_new_template" name="is_new_template">

	
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_condition').':', 'condition',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('condition_id', $conditions, $selected_condition,'class="condition_id"');?>
            </div>
             
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_color').':', 'color',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('color_id', $colors, $selected_color,'class="color_id"');?>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_branch').':', 'branch',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('branch_id', $branchs, $selected_branch,'class="branch_id"');?>
            </div>
        </div>
            
    </div>
    <div class="right_side">
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_khmername').':', 'khmername',array('class'=>'required wide')); ?>
            <div class='form_field' style="display:inline-flex">
            <?php //echo form_dropdown('khmername_id', $khmernames, $selected_khmername,'class="khname"','id="khmername_id"');?>
            <?php $hide_name = 'hide' ;
                if ($selected_khmername) {
                    $hide_name ='';
                }
            ?>               
            <div style="margin-right:20px" class="name_div <?php echo $hide_name ?>">
                <select class="khmer_name_id" name="khmername_id" id="khmer_name_id" required>
                    <?php if ($selected_khmername): ?>
                        <option value="<?php echo $selected_khmername ?>" selected><?php echo $selected_khmername_name ?></option>
                    <?php endif ?>
                </select>
            </div>
            <div class="select_btn">
                <a class="" title="Select Name" onclick="open_name_select()">
                    <div class="small_button">
                        <span name="btnselectname" id="btnselectname" style="font-size:10px;">Select Name</span>
                    </div>
                </a>
            </div>
            </div>
        </div> 
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_category').':', 'category',array('class'=>'required wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('category_id', $categorys, $selected_category,'id="category_id"');?>
            </div>
        </div>
        <div class="field_row clearfix">    
        <?php echo form_label($this->lang->line('items_fit').':', 'fit',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_input(array(
                'name'=>'fit',
                'id'=>'fit',
                'size'=>'25',
                'value'=>$item_info->fit)
            );?>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label('Make:', 'name',array('class'=>'wide')); ?>
            <div class='form_field'>
                <select style="width: 200px" name="make_id" id="make_id" class="clmake">
                    <option value="">Please Select</option>
                    <?php
                    $make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0 ORDER BY make_name ASC")->result();
                    foreach ($make as $m) {
                        $sel='';
                        if(isset($item_info->make_id))
                            if($item_info->make_id==$m->make_id)
                                $sel='selected';
                        echo "<option value='$m->make_id' $sel>$m->make_name</option>";
                    }
                     ?>
                     <!-- <option>Hello</option> -->
                </select>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label('Model:', 'name',array('class'=>'wide')); ?>
            <div class='form_field'>
                <select name="model_id" id="model_id" class="model_id">
                    <option value="">Please Select</option>
                    <?php
                     // if(isset($item_info->make_id)){
                     //    $where="";
                        // if($item_info->make_id!='')
                        //     $where=" AND make_id='$item_info->make_id'";
                        
                            $model=$this->db->query("SELECT * FROM ospos_models WHERE deleted=0 ORDER BY model_name ASC")->result();
                            foreach ($model as $mo) {
                                $sel='';
                                if($item_info->model_id==$mo->model_id)
                                        $sel='selected';
                                echo "<option value='$mo->model_id' rel='$mo->make_id' $sel>$mo->model_name</option>";
                            }
                     // }
                     ?>
                </select>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_year').':', 'year',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php 
                define (MINY,1980);
                $year_temp = $item_info->year;
            ?>
            <select name="year" id="year" class="year" style="width:100px;"> 
                <option value="">None</option>
                    <!-- <option value="<?php echo $year_temp; ?>"><?php echo $year_temp; ?></option> -->
                <?php 
                    for($j=date('Y');$j>=MINY;$j--){
                        $sel='';
                        if($year_temp==$j)
                            $sel='selected';
                        // echo $year_temp;
                        echo '<option value="'.$j.'" '.$sel.'>'.$j.'</option>';
                    }
                ?>                    
            </select>
            </div>
        </div>
        <div class="field_row clearfix">
            <label class="wide">Body:</label>
            <div class='form_field'>
                <select name="vin_body" class="body_sel">
                    <option value="">Please Select</option>
                    <?php
                        $sele='';
                        $body = $this->db->query("SELECT * FROM ospos_body WHERE deleted=0 ORDER BY body_name ASC")->result();
                        foreach ($body as $bo) {
                            if($item_info->body==$bo->body_id){
                                $sele="selected";
                            }else{
                                $sele="";
                            }
                        ?>

                        <option value="<?php echo $bo->body_id ?>" <?php echo $sele ?>><?php echo $bo->body_name ?></option>
                    <?php
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="field_row clearfix">
            <label class="wide">Engine:</label>
            <div class='form_field'>
                <select name="vin_engine" class="engine_sel">
                    <option value="">Please Select</option>
                    <?php
                        $sele='';
                        $engine = $this->db->query("SELECT * FROM ospos_engine WHERE deleted=0 ORDER BY engine_name ASC")->result();
                        foreach ($engine as $en) {
                            if(isset($item_info->engine)){
                                if ($item_info->engine==$en->engine_id) {
                                   $sele="selected";
                                }else{
                                    $sele="";
                                }
                                
                            }
                        ?>
                        <option value="<?php echo $en->engine_id ?>" <?php echo $sele ?>><?php echo $en->engine_name ?></option>
                    <?php
                        }

                    ?>
                </select>
            </div>
        </div>
        <div class="field_row clearfix">
            <label class="wide">Transmission:</label>
            <div class='form_field'>
                <select name="vin_trans" class="trans_sel">
                    <option value="">Please Select</option>
                    <?php
                        $sele='';
                        $trans = $this->db->query("SELECT * FROM ospos_transmission WHERE deleted=0 ORDER BY transmission_name ASC")->result();
                        foreach ($trans as $tra) {
                            if($item_info->transmission==$tra->trans_id){
                                $sele="selected";
                            }else{
                                $sele="";
                            }
                        ?>

                        <option value="<?php echo $tra->trans_id ?>" <?php echo $sele ?>><?php echo $tra->transmission_name ?></option>
                    <?php
                        }

                    ?>
                </select>
            </div>
        </div>
        <div class="field_row clearfix">
            <label class="wide">Trim:</label>
            <div class='form_field'>
                <select name="vin_trim" class="trim_sel">
                    <option value="">Please Select</option>
                    <?php
                        $sele='';
                        $trim = $this->db->query("SELECT * FROM ospos_trim WHERE deleted=0 ORDER BY trim_name ASC")->result();
                        foreach ($trim as $tri) {
                            if($item_info->trim==$tri->trim_id){
                                $sele="selected";
                            }else{
                                $sele='';
                            }
                        ?>

                        <option value="<?php echo $tri->trim_id ?>" <?php echo $sele ?>><?php echo $tri->trim_name ?></option>
                    <?php
                        }

                    ?>
                </select>
            </div>
        </div>
        <div class="field_row clearfix">
            <label class="wide">Fuel:</label>
            <div class='form_field'>
                <select name="vin_fuel" class="fuel_sel">
                    <option value="">Please Select</option>
                    <?php
                        $sele='';
                        $fuel = $this->db->query("SELECT * FROM ospos_fuel WHERE deleted=0 ORDER BY fule_name ASC")->result();
                        foreach ($fuel as $fu) {
                            if($item_info->fuel==$fu->fuel_id){
                                $sele="selected";
                            }else{
                                $sele="";
                            }
                        ?>

                        <option value="<?php echo $fu->fuel_id ?>" <?php echo $sele ?>><?php echo $fu->fule_name ?></option>
                    <?php
                        }

                    ?>
                </select>
            </div>
        </div>
        <div class='value_selected'>
        <div class="field_row clearfix">
            <label style="width:150px;">Grade :</label>
            <div class='form_field'>

             <?php echo form_dropdown('grade_id', $grade, $selected_grade,'class="grade_id"');?>
             
             </div>
            </div>
             
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_part_placement').':', 'partplacement',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('partplacement_id', $partplacements, $selected_partplacement,'class="partplacement_id"');?>
            </div>
        </div>
        
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_location').':', 'location',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_dropdown('location_id', $locations, $selected_location,'class="location_id"');?>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php form_label($this->lang->line('items_supplier').':', 'supplier',array('class'=>'required wide')); ?>
            <div class='form_field'>
            <?php form_dropdown('supplier_id', $suppliers, $selected_supplier,'class="supplier_id"');?>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_cost_price').':', 'cost_price',array('class'=>'required wide')); ?>
            <div class='form_field'>
            <?php echo form_input(array(
                'name'=>'cost_price',
                'size'=>'12',
                'id'=>'cost_price',
                'value'=>$item_info->cost_price)
            );?>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_unit_price').':', 'unit_price',array('class'=>'required wide')); ?>
            <div class='form_field'>
            <?php echo form_input(array(
                'name'=>'unit_price',
                'size'=>'12',
                'id'=>'unit_price',
                'value'=>$item_info->unit_price)
            );?>
            </div>
        </div>
        <?php 
   
        $quantity='';
        if($item_info->quantity=='' || $item_info->quantity==null){
            $quantity=1;    
        }else{
            $quantity=$item_info->quantity;
        }
            
        ?>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_quantity').':', 'quantity',array('class'=>'required wide')); ?>
            <div class='form_field'>
            <?php echo form_input(array(
                'name'=>'quantity',
                'id'=>'quantity',
                'size'=>'12',
                'value'=>$quantity
            )
            );?>
            </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label($this->lang->line('items_description').':', 'description',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_textarea(array(
                'name'=>'description',
                'id'=>'description',
                'value'=>$item_info->desc_item,
                'rows'=>'3',
                'cols'=>'25')
            );?>
            </div>
        </div>
        <!-- <div id='blog_img'> 
            <input type='text' name='deleteimg' style='display:none' id='deleteimg'/>
            <div class="image_new_blog">
                    <?PHP

                    $ma=1;
                    $img_path=base_url('assets/site/image/no_img.png');
                    if(isset($item_info->desc_item)){
                        foreach ($this->Item->getimage($item_info->item_id) as $img) { 
                            if($ma<$img->first_image)
                                $ma=$img->first_image;
                            if(file_exists(FCPATH."/uploads/thumb/".$img->imagename)){
                                $img_paths=base_url("/uploads/thumb/".$img->imagename);
                             
                        ?>
                                <div style='width:400px;' id='img_row'>
                                    <div class="img" style="width:130px; float:left"> 
                                    <?php if($img->tem_image==0){ ?>
                                        <img onclick='remove_img(event);' rel='<?PHP echo $img->image_id ?>' class='remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                                    <?php } ?>
                                        
                                        <img  <?php if($img->tem_image==0){ echo 'onclick="choosimg(event);"'; }?> id='imgpreview' src='<?php echo $img_paths ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                                        <input id='uploadImage' multiple accept='image/*' type='file' class='uploadimg ext' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                                        <input multiple type='text' class='hide'  name='updimg[]' id='updimg'/>
                                    </div>
                                    <div class='txt_order' style="width:130px; float:left; margin-left:30px; margin-top:18px;">
                                        <label class='col-lg-1 control-label'style="margin-top:15px !important;" >Order</label>
                                        <div class=" col-lg-4" style="margin-top:15px !important;"> 
                                            <div class="col-md-12">
                                                <input type="text"  <?php if($img->tem_image!=0){ echo 'disabled'; }?> readonly onkeypress='return isNumberKey(event);' class="form-control input-sm order"  name="order[]" id='order' rel='<?php echo $img->item_id ?>' value='<?php echo $img->first_image ?>'>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <p style='clear:both;'></p>  
                        <?PHP
                            }else{
                                $this->Item->deleteimg($img->image_id);
                            }  
                        }
                    }
                    ?>
                     <div style='width:400px;' id='img_row'>
                        <div class="img" style="width:130px; float:left"> 
                            <img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                            <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                            <input multiple id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                        </div>
                        <div class='txt_order' style="width:130px; float:left; margin-left:30px; margin-top:18px;">
                            <label class='col-lg-1 control-label' >Order</label>
                            <div class=" col-lg-4"> 
                                <div class="col-md-12">
                                    <input type="text"  readonly class="form-control input-sm" id='order'  name="order[]" value="<?php echo $ma+1 ?>">
                                </div> 
                            </div>
                        </div>
                    </div> 
                    <p style='clear:both;'></p>    
            </div>
        </div> -->
        <!-- MULTIPLE UPLOAD IMAGE -->

        <div class="img_blog">

            <?PHP
                $ma=1;
                $img_paths=base_url('assets/site/image/no_img.png');
                
                $item_image = $this->Item->getimage($item_info->item_id);
                if($item_image){
                    ?>
                    <!-- <input type='button' onclick='clearimg()' value='Clear Image'><br><br> -->

                    <?php
                    foreach ($item_image as $img) { 
                        if($ma<$img->first_image)
                            $ma=$img->first_image;
                        if(file_exists(FCPATH."/uploads/thumb/".$img->imagename)){
                            $img_paths=base_url("/uploads/thumb/".$img->imagename);
                    ?>

                           <!-- <div class="img_wrap">
                              <button product_id = "<?=$img->item_id?>" img_name = "<?=str_replace($img->item_id.'_', '',$img->imagename)?>" type="button" class="text-danger btn-delete-pic">X</button>
                              <img src="<?php echo $img_paths ?>?<?php echo rand(0,999) ?>" class="pre_image" imgid = <?php echo $img->image_id ?>>
                            </div> -->
                            <!-- <input type="file" accept="*/image" name="userfile[]" disabled="" class="updfile hide" multiple=""> -->
                            <!-- <p style='clear:both;'></p>   -->
                    <?PHP
                        }else{
                            $this->Item->deleteimg($img->image_id);
                        }
                    }
                }
                    ?>
                    <label>Drop Image Here</label>
                    <div id="img-previewer">
                        <!-- <input class="img_browser" type="file" accept="*/image" name="my_img[]" /> -->
                    </div>
                    <?php
                
                ?>
            <!-- <input type="file" accept="*/image" name="userfile[]" class="updfile" multiple /> -->
            
            <!-- upload img -->
            
            <!-- upload img -->

        </div>
        <input type='text' name='deleteimg' style='display:none' id='deleteimg'/>
        <!-- MULTIPLE UPLOAD IMAGE -->
        <div class="field_row clearfix">
            <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'is_new',
                'id'=>'is_new',
                'value'=>1,
                'checked'=>($item_info->is_new)? 1  :0)
            );?>
            </div>
            <?php echo form_label("Is new"); ?>
        </div>
         <div class="field_row clearfix">
            <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'is_pro',
                'id'=>'is_pro',
                'value'=>1,
                'checked'=>($item_info->is_pro)? 1  :0)
            );?>
            </div>
            <?php echo form_label('Promotion'); ?>
        </div>
         <div class="field_row clearfix">
            <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'is_feat',
                'id'=>'is_feat',
                'value'=>1,
                'checked'=>($item_info->is_feat)? 1  :0)
            );?>
            </div>
            <?php echo form_label('Featured'); ?>
        </div>
         <div class="field_row clearfix">
            <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'allow_alt_description',
                'id'=>'allow_alt_description',
                'value'=>1,
                'checked'=>($item_info->allow_alt_description)? 1  :0)
            );?>
            </div>
            <?php echo form_label($this->lang->line('items_allow_alt_desciption').'', 'allow_alt_description',array('class'=>'wide')); ?>
        </div>

        <div class="field_row clearfix">
            <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'is_serialized',
                'id'=>'is_serialized',
                'value'=>1,
                'checked'=>($item_info->is_serialized)? 1 : 0)
            );?>
            </div>
            <?php echo form_label($this->lang->line('items_is_serialized').'', 'is_serialized',array('class'=>'wide')); ?>
        </div>
        
        <!-- Barcode generation Checkbox -->
        <div class="field_row clearfix">
            <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'generate_barcode',
                'id'=>'generate_barcode',
                'value'=>1,
                //'checked'=>($item_info->is_serialized)? 1 : 0
            )
            );?>
            </div>
            <?php echo form_label('Generate Barcode'); ?>
        </div>

        <?php
        echo form_submit(array(
            'name'=>'submit',
            'id'=>'submit',
            'value'=>$this->lang->line('common_submit'),
            'class'=>'submit_button float_left',      
        )
        );
        ?>
    </div>
<script type="text/javascript">
    function remove_img(i)
    {
        $('#img_wrap_'+i).remove();
    }
    $('input[class="img_browser"]').change(function(){
        // alert('change');
        // var src = URL.createObjectURL($(this));
        var img_div = "";
        img_div += '<div class="img_wrap" id="img_wrap_'+i+'">';
        img_div += '<i onclick="remove_img('+i+')" style="cursor: pointer; color: red" class="fa fa-times pull-right"></i>';
        img_div += '<img width="50px" style="display: inline;" src="'+src+'" class="pre_image">';
        img_div += "<span style='font-size:10px;'>"+($(this).name)+"</span>";
        img_div += '</div>';
        $('#img-previewer').html(img_div);
    });

    //dropzone
    var myDropzone = $('#img-previewer');
    myDropzone.dropzone(
      { url: "<?=base_url().'index.php/items/save_photo'?>",
      addRemoveLinks : true,
      parallelUploads : 1,
      params:{
        product_id:"<?=$next_item_id?>"
      },
      removedfile : function(file)
        {
          // alert(file.name);
          var name = file.name;
            request_remove("<?=$next_item_id?>", name);
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        }
        ,
        init: function(){
                thisDropzone = this;
                $.get("<?=base_url().'index.php/items/get_photos?id='.$item_info->item_id?>", function(data) {
                    data = JSON.parse(data);
                    if(data!=null){
                        $.each(data, function(key,value){
                         
                            var mockFile = { name: value.name, size: value.size,uploaded_name:value.name,accepted:true };
                             
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            // thisDropzone.options.complete.call(thisDropzone, mockFile);
                            thisDropzone.files.push(mockFile);
             
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "<?php echo base_url('uploads/thumb') ?>/<?php echo $next_item_id.'_' ?>"+value.name);
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                        });
                    }
                    
                     
                });
            }
      }
      
      );
    //dropzone
    function request_remove(product_id, file_name)
    {
      $.ajax({
         type: 'post',
         dataType:"JSON",
         url: '<?=base_url().'index.php/items/delete_photo'?>',
         data: {name: file_name, product_id:product_id},
         sucess: function(data){
          console.log('success: ' + data);
          // alert(file.name);
         }
      });
    }
    $(document).on('click', '.btn-delete-pic', function(){
      var c = confirm("Are You Sure to Delete ?");
      if(c){
        // alert('confirmed delete.');
        var product_id = $(this).attr('product_id');
        var img_name = $(this).attr('img_name');
        // alert('you deleted '+product_id+"_"+img_name);
        request_remove(product_id, img_name);
        $(this).closest('div[class="img_wrap"]').remove();
      }
    });
    // $('#img_browser').change(function()
    //     {
    //         var total_img = $(this).get(0).files.length;
    //         var img_div = "";
    //         if(total_img > 0)
    //         {
    //             var items = $(this).get(0).files;
    //             for(var i = 0; i < total_img; i++)
    //             {
    //                 // var src = URL.createObjectURL(event.target.files[i]);
    //                 var src = URL.createObjectURL(items[i]);
    //                 img_div += '<div class="img_wrap" id="img_wrap_'+i+'">';
    //                 img_div += '<i onclick="remove_img('+i+')" style="cursor: pointer; color: red" class="fa fa-times pull-right"></i>';
    //                 img_div += '<img width="50px" style="display: inline;" src="'+src+'" class="pre_image">';
    //                 img_div += "<span style='font-size:10px;'>"+items[i].name+"</span>";
    //                 img_div += '</div>';
    //             }
    //         }
            
    //         $('#img-previewer').html(img_div);
    //     });
    $('#make_id').change(function(event){
        getmodels(event);
    })
    function getmodels(event){
        var url="<?php echo site_url('site/getmodels')?>";
        var make_id=$(event.target).val();
        // alert(make_id);
        $.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{
                    'make_id':make_id,
                },
            success:function(data) {
                // alert(data);
              $(".model_id").html(data);
              // console.log(data.data);
              // document.getElementById('model_id').html=data;
             
            }
          })
    }   
    $('#category_id').select2();
    // $('.khname').select2();
    $('.clmake').select2();
    $('.model_id').select2();
    $('.year').select2();
    $('.color_id').select2();
    $('.condition_id').select2();
    $('.partplacement_id').select2();
    $('.branch_id').select2();
    $('.location_id').select2();
    $('.body_sel').select2();
    $('.trim_sel').select2();
    $('.trans_sel').select2();
    $('.engine_sel').select2();
    $('.fuel_sel').select2();
    $('.name_id').select2({
        templateSelection:template
    });

    $('#khmer_name_id').select2();

    function template(data) {
          var img = data.element.getAttribute('img');
          // if (!data.id) { return data.text; }
              var data_img = $('<span><img src="'+img+'" width="400px">'+data.text+'</span>');
            
          //   return $data;
        return data_img;
      }
    

    // function formatState (state) {
    //   if (!state.id) { return state.text; }
    //   var $state = $(
    //     '<span><img src="vendor/images/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
    //   );
    //   return $state;
    // };
    // $('.name_id').select2({
    //     minimumResultsForSearch: -1
    // });

</script>
<!-- NAME BOX -->
<style type="text/css">
    #name_box_2{
    display: none;
    width: 100%;
    background: #0003;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 9999;
  }
  .table_wrapper_2{
    width: 50%;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background: white;
    padding: 1%;
    border: 7px solid #b2b2b2;
    box-shadow: 0px 0px 10px 3px;
    overflow-y: scroll;
    height: 80%;
    z-index: 99999999999;
  }
  .close_btn_2{
    position: fixed;
    float: right;
    z-index: 99999999;
    background: white;
    left: 95%;
  }

</style>
<div id="name_box_2"  class='name_box_2' onclick="close_name_select(event,0);">
    <div class="table_wrapper_2">
      <div class="close_btn_2" >
        <span class='btn_x2' style="cursor:pointer;float:right;font-size: 30px;" onclick="close_name_select(event,0)" >&times</span>
      </div>
      <div id="title_bar">
        <div id="title" class="float_left">Name List</div>
      </div>

      <div id="table_action_header" style="float: none;margin-bottom: 2%;width: 99%;">
        <div style="margin-top:-3px;margin-left:25px;width:40%;float:left;">
              <label>Search</label> 
                <input type="text" name ='isearch_2' id='isearch_2'/>
                  
        </div>
        <div style="margin-top:-3px;width:50%;float:right;display: inline-flex;">
            <input type="text" name='iname_2' id="iname_2" placeholder="New Name">
              <a>
                <div class="small_button">
                    <span class="btn_save_name_2" name="btn_save_name_2" id="btn_save_name_2" style="font-size:12px;">Save & Select</span>
                </div>
              </a>
        </div>
      </div>
      <div id="table_holder_2">
        <!-- TOP PAGINATION -->
        
        <!-- END TOP PAGINATION -->
        <table class="tablesorter table_data_2" id="sortable_table">
          <thead>
            <th>No</th>
            <th>Image</th>
            <th>Khmer Name</th>
            <th>English Name</th>
            <th>Description</th>
            <th>Action</th>
          </thead>
          <tbody>
            
          </tbody>
        </table>



        <!-- BOTTOM PAGINATION -->
        <div align="center" class="loading_ball" style="display:none;">
          <img src="<?php echo base_url('assets/loading/') ?>/balls.gif" width="30px">
        </div>
        <!-- END BOTTOM PAGINATION -->
      </div>
    </div>

</div>
<!-- END BOX -->
<script type='text/javascript'>

$('#isearch_2').keyup(function(){
  var s = $('#isearch_2').val();
  search_name(s);
});
function search_name(s){
    $('.loading_ball').show();
  $.post("<?php echo site_url('khmernames/search_name_item') ?>",
    {
      search:s
    },
    function(data){
      $('#table_holder_2 .table_data_2 tbody').html(data.manage_table);
      $('.loading_ball').hide();
    },
    'Json'
    );
}
function open_name_select(){
        $('#name_box_2').show();
        // GET DATA
        search_name();
    }
function close_name_select(event,name_id){
  // console.log($(event.target));
  if ($(event.target).hasClass('name_box_2') || $(event.target).hasClass('btn_x2') || $(event.target).hasClass('select_btn')) {

    $('#name_box_2').hide();
    if ($(event.target).attr('rel')) {
       var name_id = $(event.target).attr('rel');
       var kh  = $(event.target).attr('kh');
       var en = $(event.target).attr('en');
       var name = en+'/'+kh;
       // console.log(name);
       select_name(name_id,name);
    }else{
      // $('.name_div').addClass('hide');
      // $('#khmer_name_id').html('<option value="">'+name+'</option>');
      // $('#khmer_name_id').select2();
    };

  };
}

function select_name(name_id,name){
    if (name_id!='' && name_id!=0) {
        $('.name_div').removeClass('hide');
          $('#khmer_name_id').html('<option value='+name_id+' selected>'+name+'</option>');
          $('#khmer_name_id').select2();
    }
  
}
$('#btn_save_name_2').click(function(){
    var name = $('#iname_2').val();
    save_name_select(name);
});
function save_name_select(name){
    $.post("<?php echo site_url('khmernames/save_other') ?>",
    {
      s_name:name
    },
    function(data){
      // $('#table_holder_2 .table_data_2 tbody').html(data.manage_table);
      $('#name_box_2').hide();

        select_name(data.kh_id,name);
        $('#iname_2').val('');
    },
    'Json'
    );
}
if ($('.item_id').val()!='') {
    $('#quantity').attr('readonly', true);
}else{
    $('#quantity').attr('readonly', false);

}

enable_all_input();
function check_input(){
   // var item_id =  $('.item_id').val();
   var can_edit = $('#can_edit').val();

   // console.log(can_edit);
   if (can_edit!=1) {
        disable_all_input();
   }else{
        enable_all_input();
   }
   
}
check_input();

// PREVIEW IMAGE

$('.pre_image').click(function(){
    selectimg();
});
$('.updfile').change(function(){
    readURL(this);

});

function selectimg(){
    $('.updfile').click();
}

function clearimg(){
    var img = $('.pre_image');
    var del_id;
    img.each(function(){
        del_id = del_id +',' + $(this)[0].getAttribute('imgid');
    });
    
    // console.log(del_id);
    var new_input = "<div class='img_wrap'>"+
                        "<img src='<?php echo base_url('assets/site/image/no_img.png') ?>' class='pre_image' onclick='selectimg();'>"+
                    "</div>"+
                    "<input type='file' class='updfile hide' multiple name='userfile[]' onchange='readURL(this)'>";
    $('.updfile').remove();
    $('.img_blog').html(new_input);
    $('#deleteimg').val(del_id);

}

function readURL(input) {

    if (input.files && input.files.length>0) {
        $('.img_wrap').remove();
        $('.img_blog').append("<input type='button' onclick='clearimg()' value='Clear Image'><br><br>");
        $(input.files).each(function(){
            var reader = new FileReader();

            reader.readAsDataURL(this);

            reader.onload = function (e) {
                $('.img_blog').append("<div class='img_wrap'><img class='pre_image' src='"+e.target.result+"'></div><br>");
            }
        });
        
    }
}
// 

function disable_all_input(){
    $('#category').prop('disabled',true);
    $('#item_number').prop('disabled',true);
    $('.condition_id').prop('disabled',true);
    $('.color_id').prop('disabled',true);
    $('.branch_id').prop('disabled',true);
    $('#khmer_name_id').prop('disabled',true);
    $('#khmer_name_id').prop('disabled',true);
    $('.select_btn').addClass('hide');
    $('#category_id').prop('disabled',true);
    $('#fit').prop('disabled',true);
    $('#make_id').prop('disabled',true);
    $('#model_id').prop('disabled',true);
    $('#year').prop('disabled',true);
    $('.body_sel').prop('disabled',true);
    $('.engine_sel').prop('disabled',true);
    $('.trans_sel').prop('disabled',true);
    $('.trim_sel').prop('disabled',true);
    $('.fuel_sel').prop('disabled',true);
    $('.partplacement_id').prop('disabled',true);
    $('.location_id').prop('disabled',true);
    $('#cost_price').prop('disabled',true);
    $('#unit_price').prop('disabled',true);
    $('#quantity').prop('disabled',true);
    $('#description').prop('disabled',true);
    $('.updfile').prop('disabled',true);
    $('#is_new').prop('disabled',true);
    $('#is_pro').prop('disabled',true);
    $('#is_feat').prop('disabled',true);
    $('#allow_alt_description').prop('disabled',true);
    $('#is_serialized').prop('disabled',true);
    $('#generate_barcode').prop('disabled',true);
    $('.submit_button').prop('disabled',true);  
}


function enable_all_input(){
    $('#category').prop('disabled',false);
    $('#item_number').prop('disabled',false);
    $('.condition_id').prop('disabled',false);
    $('.color_id').prop('disabled',false);
    $('.branch_id').prop('disabled',false);
    $('#khmer_name_id').prop('disabled',false);
    $('#khmer_name_id').prop('disabled',false);
    $('.select_btn').removeClass('hide');
    $('#category_id').prop('disabled',false);
    $('#fit').prop('disabled',false);
    $('#make_id').prop('disabled',false);
    $('#model_id').prop('disabled',false);
    $('#year').prop('disabled',false);
    $('.body_sel').prop('disabled',false);
    $('.engine_sel').prop('disabled',false);
    $('.trans_sel').prop('disabled',false);
    $('.trim_sel').prop('disabled',false);
    $('.fuel_sel').prop('disabled',false);
    $('.partplacement_id').prop('disabled',false);
    $('.location_id').prop('disabled',false);
    $('#cost_price').prop('disabled',false);
    $('#unit_price').prop('disabled',false);
    $('#quantity').prop('disabled',false);
    $('#description').prop('disabled',false);
    $('.updfile').prop('disabled',false);
    $('#is_new').prop('disabled',false);
    $('#is_pro').prop('disabled',false);
    $('#is_feat').prop('disabled',false);
    $('#allow_alt_description').prop('disabled',false);
    $('#is_serialized').prop('disabled',false);
    $('#generate_barcode').prop('disabled',false);
    $('.submit_button').prop('disabled',false);  
}

function disable_when_have_template(){
    $('#khmer_name_id').prop('disabled',true);
    $('.select_btn').addClass('hide');
    $('#category_id').prop('disabled',true);
    $('#fit').prop('disabled',true);
    $('#make_id').prop('disabled',true);
    $('#model_id').prop('disabled',true);
    $('#year').prop('disabled',true);
    $('.body_sel').prop('disabled',true);
    $('.engine_sel').prop('disabled',true);
    $('.trans_sel').prop('disabled',true);
    $('.trim_sel').prop('disabled',true);
    $('.fuel_sel').prop('disabled',true);
    $('.partplacement_id').prop('disabled',true);
    $('.location_id').prop('disabled',true);
    $('#cost_price').prop('disabled',true);
    $('#unit_price').prop('disabled',true);
    $('#quantity').prop('disabled',true);
}
function enable_when_have_template(){
     $('#khmer_name_id').prop('disabled',false);
    $('.select_btn').removeClass('hide');
    $('#category_id').prop('disabled',false);
    $('#fit').prop('disabled',false);
    $('#make_id').prop('disabled',false);
    $('#model_id').prop('disabled',false);
    $('#year').prop('disabled',false);
    $('.body_sel').prop('disabled',false);
    $('.engine_sel').prop('disabled',false);
    $('.trans_sel').prop('disabled',false);
    $('.trim_sel').prop('disabled',false);
    $('.fuel_sel').prop('disabled',false);
    $('.partplacement_id').prop('disabled',false);
    $('.location_id').prop('disabled',false);
    $('#cost_price').prop('disabled',false);
    $('#unit_price').prop('disabled',false);
    $('#quantity').prop('disabled',false);
}

$('#item_form').submit(function(){
    enable_when_have_template();
});
</script>
<script type="text/javascript">
    $('#TB_overlay,#TB_closeWindowButton').click(function(){
        enable_search('<?php echo site_url("items/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    })
</script>
