<?php $this->load->view('partial/header') ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">

<style type="text/css">
	body{
		background: white;
	}
</style>
<div id="title_bar" class="col-md-12">
	<div class="col-md-6">
	 	<h4>Sold Items</h4>
	</div>
	
</div>
<div class="clearfix"></div>

<div class="row">
		<div class="col-md-12">
			<form id="form">
			<div class="col-md-12">
				<table class="table table-bordered table-striped tdata">
					<thead>
						<th>Nº</th>
						<th>Vinnumber</th>
						<th>Part Number</th>
						<th>Part Name</th>
						<th>Make</th>
						<th>Model</th>
						<th>Year</th>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
			
			
			<div class="col-md-12 form-group">
				<input type="submit" class="btn btn-primary pull-left" value="Submit" id="btn_submit">
			</div>
			</form>

		</div>
	</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		get_data();
	});

	function get_data(){
		$('.loading_box').fadeIn();
		var v = '';
		$.ajax({
			url:"<?php echo site_url('items/get_sold_item') ?>",
			type:'post',
			dataType:'json',
			data:{
				con:v
			},
			success:function(oh){
				// console.log(oh.tbody);
				$('.tdata tbody').html(oh.tbody);
				$('.vin_sel').select2({ width: '100%' });
				var oTable = $('.tdata').DataTable();
 
			    $('#form').submit( function() {
			    	// console.log(oTable.$('.vin_sel').serialize());
			        var data = oTable.$('.vin_sel,.item_id,.is_new,.price').serialize();
			        save_data(data);
			        return false;
			    } );



				$('.loading_box').fadeOut();

				
			}
		});
	}

	function save_data(data){
		$.ajax({
			url:"<?php echo site_url('items/update_item_vin') ?>",
			type:'post',
			dateType:'json',
			data:data,
			success:function(r){
				console.log(r);
				location.reload();
			},
			error:function(d){
				console.log(d);
			}
		})
	}

	




</script>
<?php $this->load->view('partial/footer') ?>