<style type="text/css">
  .part_num_er{
    color: red;
  }
  .left_side{
    width: 45%;
    float: left;
    border: 1px solid #dadada;
    padding: 10px;
    margin-right: 5px;
  }
  .right_side{
    width: 45%;
    float: left;
    border: 1px solid #dadada;
    padding: 10px;
  }
</style>
<script type="text/javascript">
$(document).ready(function()
{
    //init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo $this->lang->line("common_confirm_search")?>');
    enable_delete('<?php echo $this->lang->line($controller_name."_confirm_delete")?>','<?php echo $this->lang->line($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo $this->lang->line($controller_name."_none_selected")?>');

    $('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo $this->lang->line('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','index.php/items/generate_barcodes/'+selected.join(':'));
    });

    $("#low_inventory").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#is_serialized").click(function()
    {
    	$('#items_filter_form').submit();
    });

    $("#no_description").click(function()
    {
    	$('#items_filter_form').submit();
    });







});



function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				8: { sorter: false},
				9: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				hightlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		var selected_item_ids=get_selected_values();
		for(k=0;k<selected_item_ids.length;k++)
		{
			update_row(selected_item_ids[k],'<?php echo site_url("$controller_name/get_row")?>');
		}
		set_feedback(response.message,'success_message',false);
	}
}

function show_hide_search_filter(search_filter_section, switchImgTag) {
        var ele = document.getElementById(search_filter_section);
        var imageEle = document.getElementById(switchImgTag);
        var elesearchstate = document.getElementById('search_section_state');
        if(ele.style.display == "block")
        {
                ele.style.display = "none";
				imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/plus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="none";
        }
        else
        {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src=" <?php echo base_url()?>images/minus.png" style="border:0;outline:none;padding:0px;margin:0px;position:relative;top:-9px;" >';
                elesearchstate.value="block";
        }
}


/*---------------Upload Image-----------------*/


var intTextBox=0;
function addElement(){
	intTextBox = intTextBox + 1;
	var contentID = document.getElementById('pop');
	var newTBDiv = document.createElement('div');
	newTBDiv.setAttribute('id','strText'+intTextBox);
	newTBDiv.innerHTML ="<input type='file' id='" + intTextBox + "' size='25' name='path[]' style='margin-top:15px;'/>";
	contentID.appendChild(newTBDiv);
}
function removeElement(){
	if(intTextBox != 0){
		var contentID = document.getElementById('pop');
		contentID.removeChild(document.getElementById('strText'+intTextBox));
		intTextBox = intTextBox-1;
	}
}

/*----------------------------------------------------*/

$(document).ready(function(){
  $('#category').select2();
  $('#item_number').change(function(){
    var value = $('#item_number').val();
    // if ($('#category').val()=='') {
      do_change_data(value,'p');
    // }
  })


  $('#category').change(function(){
    var value = $('#category').val();
    do_change_data(value,'v');
  });

  function do_change_data(value,vtype){
    value = value.replace(/\s+/g, '');
    if (vtype=='p') {
    $('#item_number').val(value);
    }
    if (value.indexOf(' ')<0 && value!='') {
        $('#subform_container').removeClass('hide');
       $('#category_id').select2();
       $('#year').select2();
       $('#make_id').select2();
       $('#model_id').select2();
       $('.color_id').select2();
       $('.condition_id').select2();
       $('.grade_id').select2();
       $('.partplacement_id').select2();
       $('.branch_id').select2();
       // $('#khmername_id').select2();
       $('.location_id').select2();
       $('.body_sel').select2();
       $('.engine_sel').select2();
       $('.trans_sel').select2();
       $('.trim_sel').select2();
       $('.fuel_sel').select2();

      get_change_data(value,vtype);
      $('#category').prop('disabled',false);

    }else{
      $('#subform_container').addClass('hide');
    }
    

  }

  function get_change_data(value,vtype){
    $('body').addClass('loading');
    var url = "<?php echo site_url('items/get_change_data') ?>";
    $.ajax({
            url:url,
            type:"POST",
            datatype:"Json",
            async:false,
            data:{
                    'value':value,
                    'vtype':vtype
                },
            success:function(re) {
              var d = JSON.parse(re);
              var type = d.res.type;
              if (type=='v') {
                $('#make_id').val(d.res.make_id).trigger('change.select2');
                $('#model_id').val(d.res.model_id).trigger('change.select2');
                $('#year').val(d.res.year).trigger('change.select2');
                $('.body_sel').val(d.res.body_id).trigger('change.select2');
                $('.engine_sel').val(d.res.engine_id).trigger('change.select2');
                $('.trans_sel').val(d.res.transmission_id).trigger('change.select2');
                $('.trim_sel').val(d.res.trim_id).trigger('change.select2');
                $('.fuel_sel').val(d.res.fuel_id).trigger('change.select2');
              }else if(type=='p'){
                // if (d.res.is_approve=='1') {
                  $('#category').prop('disabled',false);
                  disable_when_have_template();
                  $('#category_id').val(d.res.category_id).trigger('change.select2');
                  $('#make_id').val(d.res.make_id).trigger('change.select2');
                  $('#model_id').val(d.res.model_id).trigger('change.select2');
                  $('#year').val(d.res.year).trigger('change.select2');
                  $('.color_id').val(d.res.color_id).trigger('change.select2');
                  $('.body_sel').val(d.res.body_id).trigger('change.select2');
                  $('.engine_sel').val(d.res.engine_id).trigger('change.select2');
                  $('.trans_sel').val(d.res.transmission_id).trigger('change.select2');
                  $('.trim_sel').val(d.res.trim_id).trigger('change.select2');
                  $('.fuel_sel').val(d.res.fuel_id).trigger('change.select2');
                  $('.condition_id').val(d.res.condition_id).trigger('change.select2');
                  $('.partplacement_id').val(d.res.part_placement).trigger('change.select2');
                  $('.branch_id').val(d.res.brand_id).trigger('change.select2');
                  // $('.location_id').val(d.res.location_id).trigger('change.select2');
                  $('#cost_price').val(d.res.cost_price).trigger('change.select2');
                  $('#unit_price').val(d.res.unit_price_to).trigger('change.select2');
                  select_name(d.res.name_id,d.res.name_text);

                  $('.part_num_er').addClass('hide');
                  
                  if (d.res.is_approve!=1) {
                    $('.part_num_er').removeClass('hide');
                    enable_when_have_template();
                    
                  }
                  
                  if (d.res.is_new_template==1) {
                    $("#is_new_template").val(1);
                  }else{
                    $("#is_new_template").val(0);

                  }
                // }else{
                //   // $('#subform_container').addClass('hide');
                //   $('.part_num_er').removeClass('hide');
                // }
                
              }
              
              
              $('body').removeClass('loading');
            }
          });
  }



})//END READY

</script>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box"></ul>
<?php

echo form_open_multipart('items/save/'.$item_info->item_id,array('id'=>'item_form','class'=>'dropzone'));
?>
<input type="hidden" name="item_id" id="item_id" value="<?php echo $item_info->item_id ?>">
<input type="hidden" name="can_edit" id="can_edit" value="<?php echo $can_edit ?>">

<fieldset id="item_basic_info">
<legend><?php echo $this->lang->line("items_basic_information"); ?></legend>



<div class="left_side">
  <div class="field_row clearfix">
    <?php echo form_label($this->lang->line('items_vinnumber').':', 'category',array('class'=>'wide vinnumber')); ?>
        <div class='form_field'>
        <?php 
            // echo form_input(array(
            // 'name'=>'category',
            // 'id'=>'category',
            // 'size'=>'25',
            // 'value'=>$item_info->category)
            // );
        ?>
        <select class="category" name="category" id="category">
            <option value="">Select</option>
            <?php foreach ($vin as $v): ?>
                <?php $sel='';
                    if ($v->vinnumber_name==$item_info->category) {
                        $sel = 'selected';
                    }
                 ?>
                <option value='<?php echo $v->vinnumber_name ?>' <?php echo $sel ?>><?php echo 'H'.$v->vinnumber_id?></option>
            <?php endforeach ?>
        </select>
        </div>
    </div>
    <div class="field_row clearfix">
      <?php echo form_label($this->lang->line('items_item_number').':', 'name',array('class'=>'wide')); ?>
          <div class='form_field'>
          <?php echo form_input(array(
              'name'=>'item_number',
              'id'=>'item_number',
              'size'=>'25',
              'value'=>$item_info->item_number)
          );?>
          <div class='part_num_er hide'>Partnumber Not Approved!</div>

          </div>
      </div>
 <?php
    $data['loader'] = "form"; 
    $this->load->view('items/sub_form', $data);
 ;?> 



 

</table>    
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	// $("#category").autocomplete("<?php echo site_url('items/suggest_category');?>",{max:100,minChars:0,delay:10});
 //    $("#category").result(function(event, data, formatted){
 //        console.log(data);
 //    });
	$(".category").select2();



	
});
</script>

<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>