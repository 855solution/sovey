

<script type="text/javascript">
// +++++++++++++++++++++ Image upload++++++++++++++++++
   function PreviewImage(event) {
        var uppreview=$(event.target).parent().parent().find('#imgpreview');
        var oFReader = new FileReader();
        oFReader.readAsDataURL($(event.target)[0].files[0]);

        oFReader.onload = function (oFREvent) {
            uppreview.attr('src',oFREvent.target.result);
            // uppreview.style.backgroundImage = "none";
        };
        $(event.target).parent().parent().find('.remove_img').removeClass('hide');
        $(event.target).removeClass('bl');
        var slideid=$(event.target).parent().parent().find('.remove_img').attr('rel');
        var del=$('#deleteimg').val()
        $('#deleteimg').val(del+','+slideid);
        addnewimg();
    };
    function addnewimg(){
        var exst=false;
        var ma=0;
        $('.uploadimg').each(function(){
            var ma_o=$(this).parent().parent().find('#order').val();
            var f = $(this)[0].files[0];
            // alert(f);
            if(ma<ma_o)
                ma=ma_o;
            if(f==undefined && $(this).hasClass('bl')){
                exst=true;
            }
        })
        // if(exst==true)
        //     $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
        //                                 "<div class='img' style='width:130px; float:left'> "+ 
        //                                     "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
        //                                     "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
        //                                     "<input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
        //                                     "<input type='text' value='' class='removeimgid hide' />"+
        //                                 "</div>"+
        //                             "</div>"+
        //                             "<p style='clear:both;'></p>");
    }
       
    function choosimg(event){
        $(event.target).parent().parent().find('#uploadImage').click();
    }
	// +++++++++++++++++++++++++++++remove upload pic+++++++++++++++++++ 
    function remove_img(event){
        var picid=$(event.target).attr('rel');
        $(event.target).parent().parent().remove();
        var delete_img=$('#deleteimg').val();
        if(picid!='')
            $('#deleteimg').val(delete_img+','+picid);

             $('#blog_img').append(" <div style='width:400px;' id='img_row'>"+
                                        "<div class='img' style='width:130px; float:left'> "+ 
                                            "<img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>"+
                                            "<img  onclick='choosimg(event);' id='imgpreview' src='<?PHP echo base_url('assets/site/image/no_img.png'); ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>"+
                                            "<input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />"+
                                            "<input type='text' value='' class='removeimgid hide' />"+
                                        "</div>"+
                                    "</div>"+
                                    "<p style='clear:both;'></p>");
    }


   // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
</script>
<style type="text/css">
	#img_row{padding: 3px 0px !important}
    .hide{display: none !important;}
    .img_row>div{
        width: 45%;
        float: left;
    }
    .remove_img{
        margin-bottom:-23px;position: relative; margin-left:-6px !important;
        width: 24px;
    }
    .top_action_button img{width: 28px; margin-top: 5px;}
    .saveloading{width:35px;}
    .remove_img:hover{
        cursor: pointer;
    }
    a{
        cursor: pointer;
    }
    .previewupl:hover{
        cursor: pointer;
    }
</style>
<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open_multipart('khmernames/save/'.$khmername_info->khmername_id,array('id'=>'khmername_form'));
?>
<fieldset id="khmername_basic_info" style="padding: 5px;">
<legend><?php echo $this->lang->line("khmernames_basic_information"); ?></legend>
<div class="field_row clearfix">
	<?php echo form_label($this->lang->line('khmernames_khmername_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'khmername_name',
		'id'=>'khmername_name',
        'required'=>'required',
		'value'=>$khmername_info->khmername_name)
	);?>
	</div>
	
</div>
<div class="field_row clearfix">
	<?php echo form_label($this->lang->line('khmernames_english_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'english_name',
		'id'=>'english_name',
        'required'=>'required',
		'value'=>$khmername_info->english_name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label($this->lang->line('khmernames_card_description').':', 'name',array('class'=>'wide')); ?>

	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$khmername_info->description)
	);?>
	</div>
</div>
<!-- Upload Image -->
<div id='blog_img'> 
                <input type='text' name='deleteimg' style='display:none' id='deleteimg'/>
                <?PHP

                $ma=1;
                $img_path=base_url('assets/site/image/no_img.png');
                if($khmername_info->khmername_id!=''){
                   $img=$this->Khmername->getimage($khmername_info->khmername_id);
                    if ($img->khmername_id==$khmername_info->khmername_id) {
                            if($ma<$img->first_image)
                                $ma=$img->first_image;
                            if(file_exists(FCPATH."/uploads/khmername/thumb/".$img->imagename)){
                                $img_paths=base_url("/uploads/khmername/thumb/".$img->imagename);
                        ?>
                                <div style='width:400px;' id='img_row'>
                                    <div class="img" style="width:130px; float:left"> 
                                        <img onclick='remove_img(event);' rel='<?PHP echo $img->image_id ?>' class='remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                                        <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_paths ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                                        <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg ext' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                                        <input type='text' class='hide'  name='updimg[]' id='updimg'/>
                                    </div>
                                    
                                </div>
                                <p style='clear:both;'></p>  
                        <?PHP
                            }else{
                                $this->Khmername->deleteimg($img->image_id);
                            }
                    }else{
                    ?>
                        <div style='width:400px;' id='img_row'>
                        <div class="img" style="width:130px; float:left"> 
                            <img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                            <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                            <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                        </div>
                       
                        </div> 
                        <p style='clear:both;'></p>
                    <?php
                    }
                }else{

                ?>

                    <div style='width:400px;' id='img_row'>
                    <div class="img" style="width:130px; float:left"> 
                        <img onclick='remove_img(event);' class='hide remove_img' src='<?PHP echo base_url('assets/site/image/remove.png'); ?>'/>
                        <img  onclick="choosimg(event);" id='imgpreview' src='<?php echo $img_path ?>' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                        <input id='uploadImage' accept='image/png,image/jpg,image/jpeg,image/gif' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                    </div>
                   
                    </div> 
                    <p style='clear:both;'></p>
                <?php
            }
                ?>
                
            </div>
<!--  close Image-->
<input type="hidden" value="<?php echo $khmername_info->khmername_id?>" id='kh_id'>
<?php
echo form_input(array(
    'type'=>'submit',
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->lang->line('common_submit'),
	'class'=>'submit_button float_left')
);
?>

</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
// $(document).ready(function(){
// 	$('#khmername_form').validate({
// 		submitHandler:function(form)
// 		{
// 			$(form).ajaxSubmit({
// 			success:function(response)
// 			{
// 				tb_remove();
// 				post_khmername_form_submit(response);
// 			},
// 			dataType:'json'
// 		});
// 		},
// 		errorLabelContainer: "#error_message_box",
//  		wrapper: "li",
// 		rules:
// 		{
// 			khmername_name:"required"
//    		},
// 		messages:
// 		{
// 			khmername_name:"<?php echo $this->lang->line('khmernames_name_required'); ?>",	
// 		}
// 	});
// });
// $('#khmername_form').submit(function(){
//     var kh = $('#khmername_name').val();
//     var en = $('#english_name').val();
//     chk_name(kh,en);
    
// });

function chk_name(kh,en){
    var kh_id = $('#kh_id').val();
    if (kh=='') {
        alert('Khmername Required!');
    };
    if (en=='') {
        alert('English Name Required!');
    };
    $.post("<?php echo site_url('khmernames/check_names') ?>",
        {
            kh:kh,
            en:en,
            kh_id:kh_id
        },
        function(d){
            if (d.tkh=='y') {
                alert('Khmername Already Exist!');
            }else if (d.ten=='y') {
                alert('English Name Already Exist !');
            }else if(d.tkh=='n' && d.ten=='n'){

                $('#khmername_form').submit();
                // $('#submit_real').click();
                // alert('submit');
            }

            
            
        },'Json')
}
// $('#khmername_form').submit(function(){
//     var kh = $('#khmername_name').val();
//     var en = $('#english_name').val();

    
//     console.log(skh);
//     console.log(sen);
    
//     alert('false');
//     return false;
    
// });


</script>