
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/list_sales.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.css') ?>">
<style type="text/css">
	body{
		background: white;
	}

	.name_khmer_er{
		color: red;
	}
	.name_english_er{
		color: red;
	}
	.img_wrap{
	    border: 1px solid #dadada;
	    text-align: center;
	    width: 125px;
	}
	.pre_img{
		width: 120px;
		cursor: pointer;
	}
</style>
<?php $er = $this->session->flashdata('er') ?>
<?php if ($er): ?>
	<div class="alert alert-danger">
		<p><?php echo $er ?></p>
	</div>
<?php endif ?>

<div class="panel panel-default">
  <!-- Default panel contents -->
  	<div class="panel-heading">
  		<div class="row">
  			<div class="col-md-6 pull-left">
  				<h4>List Khmernames</h4>
	  		</div>
	  		<?php if ($can_add==1): ?>
	  			<div class="col-md-6 pull-right">
		  			<button class="btn btn-primary pull-right btn_add" data-toggle='modal' data-target='#myModal'>New</button>
		  		</div>
	  		<?php endif ?>
	  		
	  		

  		</div>
  		
  	</div>
  	<div class="panel-body">
    	<div class="col-md-12">
			<table class="table table-bordered table-striped tdata">
				<thead>
					<th>Nº</th>
					<th>Image</th>
					<th>Khmer Name</th>
					<th>English Name</th>
					<th>Actions</th>
					<th></th>
				</thead>
				<tbody>
					<?php echo $table ?>
				</tbody>
				<tfoot>
					
				</tfoot>
			</table>
		</div>
	</div>

  
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Khmername</h4>
      </div>
      <form action="<?php echo site_url('khmernames/save_new') ?>" enctype="multipart/form-data" method='post'>
      	
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="col-md-6">
	        			<label>Khmer Name :</label>
	        		</div>
	        		<div class="col-md-6 form-group">
	        			<input type="text" name="khmername_name" class="form-control khmer_name" value="" required="">
	        			<p class="name_khmer_er hide">Name Exist!</p>

	        		</div>
	        		<div class="col-md-6">
	        			<label>English Name :</label>
	        		</div>
	        		<div class="col-md-6 form-group">
	        			<input type="text" name="english_name" class="form-control english_name" value="" required="">
	        			<p class="name_english_er hide">Name Exist!</p>

	        		</div>
	        		<div class="col-md-6">
	        			<label>Photo :</label>
	        		</div>
	        		<div class="col-md-6 form-group">
	        			<input type="hidden" name="deleteimg" id="deleteimg" value="">
	        			<input type="button" name="clear_img" class="clear_img btn btn-default" value="Clear Image" onclick="clearimg()">
	        			<div class="img_wrap">
	        				<img src="uploads/no_img.png" width="120px" class="pre_img">
	        				<input type="file" name="userfile[]" class="updfile hide">
	        			</div>
	        			
	        		</div>
	        	</div>
	        </div>
      	</div>

      	<div class="modal-footer">
      		<input type="hidden" value="" class="khmer_id" name="khmer_id">
	        <input type="submit" class="btn btn-primary" value="Save">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>
      </form>

    </div>

  </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootbox/bootbox.min.js') ?>"></script>


<script type="text/javascript">
	$('.tdata').DataTable({
		"lengthMenu": [ [10, 30, 50, 100, -1], [10, 30, 50, 100, "All"] ],
		"iDisplayLength": 50
	});

	$('.khmer_name').keyup(function(){
		var n = $('.khmer_name').val();
		var id = $('.khmer_id').val();
		var t = 'k';
		$.ajax({
			url:"<?php echo site_url('khmernames/check_name') ?>",
			type:'post',
			dataType:'Json',
			data:{
				name:n,
				id:id,
				t:t
			},success:function(re){

				if (re==1) {
					$('.name_khmer_er').removeClass('hide');
					$('.khmer_name').css('color','red');
				}else{
					$('.name_khmer_er').addClass('hide');
					$('.khmer_name').css('color','black');
				}
			}
		});
	})
	$('.english_name').keyup(function(){
		var n = $('.english_name').val();
		var id = $('.khmer_id').val();
		var t = 'e';
		$.ajax({
			url:"<?php echo site_url('khmernames/check_name') ?>",
			type:'post',
			dataType:'Json',
			data:{
				name:n,
				id:id,
				t:t
			},success:function(re){
				if (re==1) {
					$('.name_english_er').removeClass('hide');
					$('.english_name').css('color','red');
				}else{
					$('.name_english_er').addClass('hide');
					$('.english_name').css('color','black');
				}
			}
		});
	})

	$('.tdata tbody').on('click','.btn_del',function(){
	
		var id = $(this).attr('f');
		var text = $(this).closest('tr').find('td:eq(2)').text();
		var text2 = $(this).closest('tr').find('td:eq(3)').text();

		bootbox.confirm('សម្រេចលុប '+text+'/'+text2+' ?',function(result){
			if (result===true) {
				location.href="<?php echo site_url('khmernames/delete_new') ?>"+"/"+id;
			};
		});
	})
	$('.tdata tbody').on('click','.btn_edit',function(){
	
		var k = $(this).attr('k');
		var e = $(this).attr('e');
		var id = $(this).attr('f');
		var timg = $(this).attr('timg');
		var imgid = $(this).attr('imgid');
		var rimg = $(this).attr('rimg');

		$('.khmer_name').css('color','black');
		$('.english_name').css('color','black');
		$('.name_khmer_er').addClass('hide');
		$('.name_english_er').addClass('hide');

		$('.khmer_id').val(id);
		$('.khmer_name').val(k);
		$('.english_name').val(e);
		$('.pre_img').attr('src',timg);
		$('.pre_img').attr('imgid',imgid);
		$('.modal-title').text('Edit Khmername');
		$('.updfile').val('uploads/'+rimg);
	});
	$('.btn_add').click(function(){
		$('.khmer_name').val('');
		$('.english_name').val('');
		$('.khmer_id').val('');
		$('.pre_img').attr('src','uploads/no_img.png');

		$('.name_khmer_er').addClass('hide');
		$('.name_english_er').addClass('hide');
		$('.modal-title').text('New Khmername');
		$('.updfile').val('');

	});



	// PREVIEW IMAGE

$('.pre_img').click(function(){
    selectimg();
});
$('.updfile').change(function(){
    readURL(this);

});

function selectimg(){
    $('.updfile').click();
}

function clearimg(){
    var img = $('.pre_img');
    var del_id;
    img.each(function(){
        del_id = del_id +',' + $(this)[0].getAttribute('imgid');
    });
    
    // console.log(del_id);
    var new_input = "<img src='<?php echo base_url('assets/site/image/no_img.png') ?>' class='pre_img' onclick='selectimg();'>"+
                    "<input type='file' class='updfile hide' name='userfile[]' onchange='readURL(this)' value=''>";
    $('.updfile').remove();
    $('.img_wrap').html(new_input);
    $('#deleteimg').val(del_id);

}

function readURL(input) {
    if (input.files && input.files.length>0) {
        // $('.img_wrap').remove();
        // $('.img_wrap').append("<input type='button' onclick='clearimg()' value='Clear Image'><br><br>");
        $(input.files).each(function(){
            var reader = new FileReader();

            reader.readAsDataURL(this);

            reader.onload = function (e) {
                // $('.img_wrap').append("<div class='img_wrap'><img class='pre_img' width='120px' src='"+e.target.result+"'></div><br>");
                $('.pre_img').attr('src',e.target.result);
            }
        });
        
    }
}
// 
</script>