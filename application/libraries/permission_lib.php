<?php
class permission_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function checkPermission($is_edit=false){
		$user_id = $this->CI->session->userdata('person_id');
		$module_id = $this->CI->uri->segment(1);
		$func = $this->CI->uri->segment(2);
		$id = $this->CI->uri->segment(3);
		$field = 'view';
		
		if ($func=='view') {
			$field='add';
			if ($id!='' && $id>0) {
				$field = 'edit';
			}
		}
		if ($func=='delete') {
			$field = 'delete';
		}
		if ($func=='suspend') {
			$field = 'create_deposit';
		}
		if ($func=='suspended') {
			$field = 'view_deposit';
		}
		if ($func == 'complete') {
			$field = 'create_sale';
		}
		if ($func=='add_quotation') {
			$field = 'create_quote';
		}
		if ($func == 'quoted_sale') {
			$field = 'view_quote';
		}
		if ($func == 'add_payment') {
			$field = 'add_payment';
		}
		if ($func == 'list_sales') {
			$field = 'list_sale';
		}
		// if ($func == 'save_perm' || $func=='save_detail' || $func == 'save_set_user') {

		// 	$field = 'add';
		// }
		// if ($func == 'delete_perm') {
		// 	$field = 'delete';
		// }
		// FOR ITEMS
		if ($module_id=='items') {
			if ($func=='save') {
				if ($id!='' && $id>0) {
					$field = 'edit';
				}
			}
		}

		$condition = $this->CI->modpermission->check_permission($user_id,$module_id,$field);
		if (!$condition) {
			redirect('no_access/'.$module_id);
		}
	}
	function user_f($f){
		$user_id = $this->CI->session->userdata('person_id');
		$module_id = $this->CI->router->class;
		$condition = $this->CI->modpermission->check_permission($user_id,$module_id,$f);
		if ($condition) {
			return 1;
		}else{
			return 0;
		}
	}
}
?>