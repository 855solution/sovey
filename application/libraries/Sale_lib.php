<?php
class Sale_lib
{
	var $CI;
	var $return_payment=null;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_cart()
	{
		if(!$this->CI->session->userdata('cart'))
			$this->set_cart(array());

		return $this->CI->session->userdata('cart');
	}

	function set_cart($cart_data)
	{
		$this->CI->session->set_userdata('cart',$cart_data);
	}

	//Alain Multiple Payments
	function get_payments()
	{
		if(!$this->CI->session->userdata('payments'))
			$this->set_payments(array());

		return $this->CI->session->userdata('payments');
	}
	
	//Alain Multiple Payments
	function set_payments($payments_data)
	{
		$this->CI->session->set_userdata('payments',$payments_data);
	}
	
	function get_comment() 
	{
		return $this->CI->session->userdata('comment');
	}
	function set_comment($comment) 
	{
		$this->CI->session->set_userdata('comment', $comment);
	}

	function clear_comment() 	
	{
		$this->CI->session->unset_userdata('comment');
	}
	
	function get_email_receipt() 
	{
		return $this->CI->session->userdata('email_receipt');
	}

	function set_email_receipt($email_receipt) 
	{
		$this->CI->session->set_userdata('email_receipt', $email_receipt);
	}

	function clear_email_receipt() 	
	{
		$this->CI->session->unset_userdata('email_receipt');
	}

	function add_payment($payment_id,$payment_amount,$payment_date,$emp_name,$emp_id)
	{
		$this->set_updatable();

		// if ($payment_amount<=0) {
		// 	return false;
		// }
		$payments=$this->get_payments();

		$key = 1;
		if (count($payments)>0) {
			$key += count($payments);
		}

		for ($i=0; $i < count(array_keys($payments)); $i++) { 
			if (array_keys($payments)[$i]==$key) {
				$key++;
			}
		}

		$payment = array($key=>
							array(
								'payment_type'=>$payment_id,
								'payment_amount'=>$payment_amount,
								'date'=> $payment_date,
								'emp_name'=>$emp_name,
								'emp_id'=>$emp_id
								)
		);


		//payment_method already exists, add to payment_amount
		// if(isset($payments[$payment_id]))
		// {
			// $payments[$payment_id]['payment_amount']+=$payment_amount;
		// }
		// else
		// {
			//add to existing array
			$payments+=$payment;
		// }
		// var_dump($payments);


		$this->set_payments($payments);
		return true;

	}

	//Alain Multiple Payments
	function edit_payment($payment_id,$payment_amount)
	{
		$payments = $this->get_payments();
		if(isset($payments[$payment_id]))
		{
			$payments[$payment_id]['payment_type'] = $payment_id;
			$payments[$payment_id]['payment_amount'] = $payment_amount;
			$this->set_payments($payment_id);
		}

		return false;
	}

	//Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$payments=$this->get_payments();
		unset($payments[$payment_id]);
		$this->set_payments($payments);
	}

	//Alain Multiple Payments
	function empty_payments()
	{
		$this->CI->session->unset_userdata('payments');
	}

	//Alain Multiple Payments
	function get_payments_total()
	{

		$edit_sale_id = $this->CI->session->userdata('edit_sale_id');
		$did = $this->CI->session->userdata('d_id');

		$subtotal = 0;
		if ($edit_sale_id OR $did) {
			foreach ($this->get_old_sale_payment() as $old_payment) {
			    $subtotal+=$old_payment['payment_amount'];
				
			}
		}
		
		foreach($this->get_payments() as $payments)
		{
		    $subtotal+=$payments['payment_amount'];
		}
		return to_currency_no_money($subtotal);
	}

	//Alain Multiple Payments
	function get_amount_due()
	{	
		$mode = $this->get_mode();
		$amount_due=0;
		$payment_total = $this->get_payments_total();
		$old_payment = $this->total_old_payment();
		$sales_total=$this->get_total();
		$amount_due=to_currency_no_money($sales_total - $payment_total);

		if ($mode=='return') {
			$sales_total = ($old_payment*-1) + $this->get_subtotal();
			$amount_due = $sales_total - $payment_total;

		// var_dump($sales_total.' '.$payment_total);
		// var_dump($amount_due);die();

		}
		
		return $amount_due;
	}

	

	function get_customer()
	{
		if(!$this->CI->session->userdata('customer'))
			$this->set_customer(-1);

		return $this->CI->session->userdata('customer');
	}

	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer',$customer_id);
	}
	function set_sale_date($sale_date){
		$this->set_updatable();
		$this->CI->session->set_userdata('sale_date',$sale_date);
	}
	public function get_sale_date()
	{
		$sale_date = $this->CI->session->userdata('sale_date');
		
		return $sale_date;
		
	}

	function get_mode()
	{
		if(!$this->CI->session->userdata('sale_mode'))
			$this->set_mode('sale');

		return $this->CI->session->userdata('sale_mode');
	}

	function set_mode($mode)
	{
		$this->CI->session->set_userdata('sale_mode',$mode);
	}
	
	function add_item($item_id=0,$is_new=false,$quantity=1,$barcode=0,$discount=0,$price=null,$description=null,$serialnumber=null,$return=0,$sale_item=false)
	{

		$this->set_updatable();
		// //make sure item exists
		// if(!$this->CI->Item->exists($item_id))
		// {
		// 	//try to get item id given an item_number
			
		// 		$item_id = $this->CI->Item->get_item_id($item_id);
			

		// 	if(!$item_id)
		// 		return false;
		// }


		//Alain Serialization and Description
		$edit_sale_id = $this->CI->session->userdata('edit_sale_id');
		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;         			 //Key to use to update(quantity)
		$stock_quantity='';

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}

			if($item['item_id']==$item_id)
			{
				$itemalreadyinsale=TRUE;
				$updatekey=$item['line'];
			}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		// echo $item_id; die();
		// print_r($this->CI->Item->get_info($item_id)); die();
		///new item added from sale
		// var_dump($item_id);
		// var_dump($this->CI->Item->get_vin_info($this->CI->Item->get_info($item_id)->category));die();
		$mode = $this->get_mode();
		if ($is_new==true) {
				// var_dump($this->get_mode());die();
				$i++;
				$stock_quantity = $this->CI->Item->get_new_item_info($item_id)->quantity;
				// var_dump($stock_quantity);
				// die();
				$sale_id = $this->CI->Sale->generate_sale_id();
				if ($mode=='return') {
					$item_number = $this->CI->Item->get_new_item_info($item_id)->barcode;
					// var_dump($this->CI->Item->get_new_item_info($item_id));die();
				}else{
					$item_number = substr($barcode,7);

				}
				$item = array(($insertkey)=>
					array(
							'item_id'=>$item_id,
							'line'=>$insertkey,
							'name'=>$this->CI->Item->get_new_item_info($item_id)->name!=null?$this->CI->Item->get_new_item_info($item_id)->name:$this->CI->Item->get_new_item_khmername($item_id),
							'barcode'=>$item_number,
							'serialnumber'=>0,
							'makename'=>$this->CI->Item->get_new_item_make_model_partplacement($item_id)->make_name,
							'modelname'=>$this->CI->Item->get_new_item_make_model_partplacement($item_id)->model_name,
							'partplacement_name'=>$this->CI->Item->get_new_item_partplacement($item_id)->partplacement_id,
							'year'=>$this->CI->Item->get_new_item_info($item_id)->year,
							// 'item_number'=>$this->CI->Item->get_new_item_info($item_id)->item_number,
							'khmername_id'=>$this->CI->Item->get_new_item_info($item_id)->khmername_id,
							'item_number'=>$barcode,
							'desc'=>$this->CI->Item->get_new_item_info($item_id)->description,
							'description'=>$description!=null ? $description: $this->CI->Item->get_new_item_info($item_id)->description,

							'quantity'=>$quantity,
							'cur_quantity'=>$this->CI->Item->get_new_item_info($item_id)->quantity,
							'color_name'=>$this->CI->Item->get_color_name($this->CI->Item->get_new_item_info($item_id)->color_id),
							// 'discount'=>$discount,
							'discount'=>$discount,
							'price'=>$price!=null ? $price:$this->CI->Item->get_new_item_info($item_id)->unit_price,
							'is_pro'=>$this->CI->Item->get_new_item_info($item_id)->is_pro,
							'is_new'=>1,
							'vin_id'=>$this->CI->Item->get_vin_info($this->CI->Item->get_new_item_info($item_id)->category)->vinnumber_id,
							'vin_num'=>$this->CI->Item->get_new_item_info($item_id)->category,
							'is_return'=>$return,
							'mode'=>$mode
						)
					);
				
			}else{

				$stock_quantity = $this->CI->Item->get_info($item_id)->quantity;


				$item = array(($insertkey)=>
				array(
					'item_id'=>$item_id,
					'line'=>$insertkey,
					'name'=>$this->CI->Item->get_infokhmername($item_id)->khmername_name!=null?$this->CI->Item->get_infokhmername($item_id)->khmername_name:$this->CI->Item->get_info($item_id)->name,
					'barcode'=>$this->CI->Item->get_info($item_id)->barcode,
					'makename'=>$this->CI->Item->get_make_model_year($item_id)->make_name,
					'modelname'=>$this->CI->Item->get_make_model_year($item_id)->model_name,
					'partplacement_name'=>$this->CI->Item->get_info($item_id)->partplacement_name,
					'year'=>$this->CI->Item->get_info($item_id)->year,
					'item_number'=>$this->CI->Item->get_info($item_id)->barcode,
					// 'description'=>$this->CI->Item->get_info($item_id)->description,
					'description'=>$description!=null ? $description: $this->CI->Item->get_info($item_id)->description,
					'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
					'allow_alt_description'=>$this->CI->Item->get_info($item_id)->allow_alt_description,
					'is_serialized'=>$this->CI->Item->get_info($item_id)->is_serialized,
					'cur_quantity'=>$this->CI->Item->get_info($item_id)->quantity,
					'quantity'=>$quantity,
		            'discount'=>$discount,
					'color_name'=>$this->CI->Item->get_color_name($this->CI->Item->get_new_item_info($item_id)->color_id),

		            'desc'=>$this->CI->Item->get_info($item_id)->desc,
					'price'=>$price!=null ? $price: $this->CI->Item->get_info($item_id)->unit_price,
					'is_pro'=>$this->CI->Item->get_info($item_id)->is_pro,
					'is_new'=>0,
					'vin_id'=>$this->CI->Item->get_vin_info($this->CI->Item->get_info($item_id)->category)->vinnumber_id,
					'vin_num'=>$this->CI->Item->get_info($item_id)->category,
					'is_return'=>$return,
					'mode'=>$mode

					)
				);
	}	
		// var_dump($this->CI->Item->get_vin_info($this->CI->Item->get_info($item_id)->category)->vinnumber_id);die();
		
		// echo $this->CI->Item->get_info($item_id)->year;die();
		// var_dump($stock_quantity);die();
		$did = $this->CI->session->userdata('d_id');
		//Item already exists and is not serialized, add to quantity
		
		if($itemalreadyinsale && ($this->CI->Item->get_info($item_id)->is_serialized ==0) )
		{
			if (!$sale_item) {

				$items[$updatekey]['quantity']+=$quantity;
				$insert_quantity = $items[$updatekey]['quantity'];

				// echo $insert_quantity;die();
				if ($mode=='sale') {
					# code...
					if ($stock_quantity < $insert_quantity) {
						$items[$updatekey]['quantity']=$stock_quantity;
						$this->CI->session->set_flashdata('warn_message',$this->CI->lang->line('sales_quantity_less_than_zero_cant_sell'));
						redirect('/sales');
					}
				}
			}


		}
		else
		{
			//add to existing array
			$items+=$item;
			if ($mode=='sale') {
				if (!$did) {
					$st = $this->CI->Item->check_item_status($item_id);
					if ($st=='Deposited') {
						$items = $this->delete_item($insertkey);
						$emp_name = $this->CI->Item->get_sus_emp_by_item($item_id);
						$this->CI->session->set_flashdata('warn_message',"ទំនិញបាន Deposit ដោយ ".$emp_name);
						redirect('/sales');
					}
				}
				
				$insert_quantity = $item[$insertkey]['quantity'];

				// if (!$edit_sale_id) {
				if (!$sale_item) {
					if ($stock_quantity < $insert_quantity) {
						$items = $this->delete_item($insertkey);
						$this->CI->session->set_flashdata('warn_message',$this->CI->lang->line('sales_item_no_quantity'));
						redirect('/sales');
					}	
				}

					
				// }
			}

		}


		
		$this->set_cart($items);
		// var_dump($this->get_cart()); die();
		return true;

	}
	
	function out_of_stock($item_id)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);
			if(!$item_id)
				return false;
		}
		
		$item = $this->CI->Item->get_info($item_id);
		$quanity_added = $this->get_quantity_already_added($item_id);
		
		// echo $item->quantity." - $quanity_added";
		// die();

		if ($item->quantity - $quanity_added < 0)
		{
			return true;
		}
		
		return false;
	}
	
	function get_quantity_already_added($item_id)
	{
		$items = $this->get_cart();
		$quanity_already_added = 0;
		foreach ($items as $item)
		{
			if($item['item_id']==$item_id)
			{
				$quanity_already_added+=$item['quantity'];
			}
		}
		
		return $quanity_already_added;
	}
	
	function get_item_id($line_to_get)
	{
		$items = $this->get_cart();

		foreach ($items as $line=>$item)
		{
			if($line==$line_to_get)
			{
				return $item['item_id'];
			}
		}
		
		return -1;
	}
	

	function edit_item($line,$description,$serialnumber,$quantity,$discount,$price,$desc,$problem)
	{
		$items = $this->get_cart();
		if(isset($items[$line]))
		{
			// $stock_quantity = $this->CI->Item->get_info($items[$line]['item_id'])->quantity;
			// $insert_quantity = $items[$line]['quantity'];
			// if ($stock_quantity < $insert_quantity) {
			// 	$quantity = $stock_quantity;
			// 	// echo $quantity;die();
			// 	$this->CI->session->set_flashdata('no_qty',$this->CI->lang->line('sales_quantity_less_than_zero_cant_sell'));
				
			// }	

			$items[$line]['description'] = $description;
			$items[$line]['serialnumber'] = $serialnumber;
			$items[$line]['quantity'] = $quantity;
			$items[$line]['discount'] = $discount;
			$items[$line]['price'] = $price;
			$items[$line]['desc'] = $desc;
			$items[$line]['is_pro'] = $problem;
			// $items[$line]['is_return'] = $is_return;
			$items[$line]['is_pro2'] = $problem2;
			

			
			$this->set_cart($items);
			// redirect('/sales');
		}

		return false;
	}
	function edit_item_return($line,$is_return,$discount,$problem)
	{
		$items = $this->get_cart();
		// var_dump($items);
		if(isset($items[$line]))
		{
			// $stock_quantity = $this->CI->Item->get_info($items[$line]['item_id'])->quantity;
			// $insert_quantity = $items[$line]['quantity'];
			// if ($stock_quantity < $insert_quantity) {
			// 	$quantity = $stock_quantity;
			// 	// echo $quantity;die();
			// 	$this->CI->session->set_flashdata('no_qty',$this->CI->lang->line('sales_quantity_less_than_zero_cant_sell'));
				
			// }	

			// $items[$line]['description'] = $description;
			// $items[$line]['serialnumber'] = $serialnumber;
			// $items[$line]['quantity'] = $quantity;
			$items[$line]['discount'] = $discount;
			// $items[$line]['price'] = $price;
			// $items[$line]['desc'] = $desc;
			$items[$line]['is_pro'] = $problem;
			$items[$line]['is_return'] = $is_return;
			// $items[$line]['is_pro2'] = $problem2;
			

			
			$this->set_cart($items);
			// redirect('/sales');
		}

		return false;
	}


	function is_valid_receipt($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);

		if(count($pieces)==2)
		{
			return $this->CI->Sale->exists($pieces[1]);
		}

		return false;
	}
	
	function is_valid_item_kit($item_kit_id)
	{
		//KIT #
		$pieces = explode(' ',$item_kit_id);

		if(count($pieces)==2)
		{
			return $this->CI->Item_kit->exists($pieces[1]);
		}

		return false;
	}

	function return_entire_sale($receipt_sale_id)
	{
		// //POS #
		// $pieces = explode(' ',$receipt_sale_id);
		// $sale_id = $pieces[1];
		$sale_id = $receipt_sale_id;
		// $is_new = false;
		$barcode='';
		$this->empty_cart();
		$this->remove_customer();
		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			if ($row->is_return!=1) {
				$this->add_item($row->item_id,$row->is_new,$row->quantity_purchased,$barcode,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
			}
		}

		foreach($this->CI->Sale->get_sale_payments($sale_id)->result() as $row)
		{
			$emp_id = $row->emp_id;
			$emp = $this->CI->Employee->get_info($row->emp_id);
			$emp_name = $emp->last_name.' '.$emp->first_name;
			$this->add_old_payment($row->payment_type,$row->payment_amount,date('d/m/Y H:i:s',strtotime($row->date)),$emp_name,$emp_id);	
		}

		
			$this->set_old_return_payment($this->CI->Sale->get_old_return_payment($sale_id));
		
		
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);

		
		
	}
	
	function add_item_kit($external_item_kit_id)
	{
		//KIT #
		$pieces = explode(' ',$external_item_kit_id);
		$item_kit_id = $pieces[1];
		
		foreach ($this->CI->Item_kit_items->get_info($item_kit_id) as $item_kit_item)
		{
			$this->add_item($item_kit_item['item_id'], $item_kit_item['quantity']);
		}
	}

	function copy_entire_sale($sale_id)
	{
		$this->empty_cart();
		$this->remove_customer();

		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->is_new,$row->quantity_purchased,'barcode',$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
				   // add_item($item_id,$is_new=false,$quantity=1,$barcode,$discount=0,$price=null,$description=null,$serialnumber=null,$return=0)
		}
		foreach($this->CI->Sale->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);
		$this->set_sale_date($this->CI->Sale->get_sale_date($sale_id));

	}
	function edit_entire_sale($sale_id)
	{
		$this->empty_cart();
		$this->remove_customer();
		$this->empty_old_payment();
		// var_dump($this->CI->Sale->get_edit_sale_items($sale_id)->result());die();
		foreach($this->CI->Sale->get_edit_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->is_new,$row->quantity_purchased,$row->barcode,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber,0,true);
				   // add_item($item_id,$is_new=false,$quantity=1,$barcode,$discount=0,$price=null,$description=null,$serialnumber=null,$return=0)
		}
		foreach($this->CI->Sale->get_sale_payments($sale_id)->result() as $row)
		{
			$emp_id = $row->emp_id;
			$emp = $this->CI->Employee->get_info($row->emp_id);
			$emp_name = $emp->last_name.' '.$emp->first_name;
			$this->add_old_payment($row->payment_type,$row->payment_amount,date('d/m/Y H:i:s',strtotime($row->date)),$emp_name,$emp_id);	
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);
		

	}
	function empty_old_payment(){
		$this->CI->session->unset_userdata('old_payment');
	}
	function add_old_payment($payment_id,$payment_amount,$payment_date,$emp_name,$emp_id)
	{
		// if ($payment_amount<=0) {
		// 	return false;
		// }
		$payments=$this->get_old_sale_payment();

		$key = 1;
		if (count($payments)>0) {
			$key += count($payments);
		}

		for ($i=0; $i < count(array_keys($payments)); $i++) { 
			if (array_keys($payments)[$i]==$key) {
				$key++;
			}
		}

		$payment = array($key=>
							array(
								'payment_type'=>$payment_id,
								'payment_amount'=>$payment_amount,
								'date'=> $payment_date,
								'emp_name'=>$emp_name,
								'emp_id'=>$emp_id
								)
		);


		//payment_method already exists, add to payment_amount
		// if(isset($payments[$payment_id]))
		// {
			// $payments[$payment_id]['payment_amount']+=$payment_amount;
		// }
		// else
		// {
			//add to existing array
			$payments+=$payment;
		// }
		// var_dump($payments);


		$this->set_old_payments($payments);
		return true;
	}
	function get_old_sale_payment(){
		if(!$this->CI->session->userdata('old_payment'))
			$this->set_old_payments(array());

		return $this->CI->session->userdata('old_payment');
	}

	function set_old_payments($old_payments){
		$this->CI->session->set_userdata('old_payment',$old_payments);
	}
	function newitem_barcode_sale($new_item_id){
	
		$sale_id = $this->CI->Sale->generate_sale_id();
		$items = $this->get_cart();
		$item_number = '';

		// GET THE LAST BARCODE
		foreach ($items as $key => $value) {
			if ($value['is_new']==1) {
				$item_number = $value['item_number'];
			}
		}
		$i = substr($item_number,strlen($sale_id));
		$cur_sale_id = substr($sale_id,6);
		$last_sale_id = substr($item_number,6,-strlen($i));
		
		if ($last_sale_id==$cur_sale_id) {
			// $i = substr($this->item->get_new_item_last_barcode(),strlen($sale_id));
			$i++;
		}else{
			$i=1;
		}
		$barcode = $sale_id.$i;
		return $barcode;
	}
	function copy_entire_suspended_sale($sale_id)
	{
		$this->empty_cart();
		$this->remove_customer();
		// var_dump($this->CI->Sale_suspended->get_sale_items($sale_id)->result());die();
		foreach($this->CI->Sale_suspended->get_sale_items($sale_id)->result() as $row)
		{
												
			$barcode = $this->newitem_barcode_sale($row->item_id);


			$this->add_item($row->item_id,$row->is_new,$row->quantity_purchased,$barcode,$row->discount_percent,$row->item_unit_price,$row->desc,$row->serialnumber);
		}
		$this->unset_updatable();
		// foreach($this->CI->Sale_suspended->get_sale_payments($sale_id)->result() as $row)
		// {
		// 	$emp_id = $row->emp_id;
		// 	$emp = $this->CI->Employee->get_info($row->emp_id);
		// 	$emp_name = $emp->last_name.' '.$emp->first_name;
		// 	$this->add_payment($row->payment_type,$row->payment_amount,date('d/m/Y H:i:s',strtotime($row->date)),$emp_name,$emp_id);
		// }
		foreach($this->CI->Sale_suspended->get_sale_payments($sale_id)->result() as $row)
		{

			$emp_id = $row->emp_id;
			$emp = $this->CI->Employee->get_info($row->emp_id);
			$emp_name = $emp->last_name.' '.$emp->first_name;
			$this->add_old_payment($row->payment_type,$row->payment_amount,date('d/m/Y H:i:s',strtotime($row->date)),$emp_name,$emp_id);	
		}
		$this->set_customer($this->CI->Sale_suspended->get_customer($sale_id)->person_id);
		$this->set_comment($this->CI->Sale_suspended->get_comment($sale_id));
	}
	function copy_entire_quoted_sale($sale_id)
	{
		$this->empty_cart();
		$this->remove_customer();
		// var_dump($this->CI->Sale_suspended->get_sale_items($sale_id)->result());die();
		foreach($this->CI->Sale_quotation->get_sale_items($sale_id)->result() as $row)
		{
												
			$barcode = $this->newitem_barcode_sale($row->item_id);


			$this->add_item($row->item_id,$row->is_new,$row->quantity_purchased,$barcode,$row->discount_percent,$row->item_unit_price,$row->desc,$row->serialnumber);
		}
		foreach($this->CI->Sale_quotation->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		$this->set_customer($this->CI->Sale_quotation->get_customer($sale_id)->person_id);
		$this->set_comment($this->CI->Sale_quotation->get_comment($sale_id));
	}

	function delete_item($line)
	{
		$q_id = $this->CI->session->userdata('q_id');
		$d_id = $this->CI->session->userdata('d_id');
		$edit_sale_id = $this->CI->session->userdata('edit_sale_id');
		$items=$this->get_cart();
		$mode=$this->get_mode();


		if (!$q_id && !$d_id && !$edit_sale_id) {
			if ($mode=='sale') {
				if ($items[$line]['is_new']==1) {
					$this->CI->Sale->delete_new_item($items[$line]['item_id']);
				}
			}
		}
		
		// var_dump($items[$line]);die();
		unset($items[$line]);
		$this->set_cart($items);
	}
	function remove_item($line){
		$items=$this->get_cart();
		unset($items[$line]);
		$this->set_cart($items);
	}

	function empty_cart()
	{
		$this->CI->session->unset_userdata('cart');
	}

	function remove_customer()
	{
		$this->CI->session->unset_userdata('customer');
	}

	function clear_mode()
	{
		$this->CI->session->unset_userdata('sale_mode');
	}

	function clear_all()
	{
		$this->clear_mode();
		$this->empty_cart();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->empty_payments();
		$this->remove_customer();
		$this->empty_old_payment();
		$this->unset_updatable();
		$this->CI->session->unset_userdata('sale_date');

	}

	
	function get_taxes()
	{
		$customer_id = $this->get_customer();
		$customer = $this->CI->Customer->get_info($customer_id);

		//Do not charge sales tax if we have a customer that is not taxable
		if (!$customer->taxable and $customer_id!=-1)
		{
		   return array();
		}

		$taxes = array();
		foreach($this->get_cart() as $line=>$item)
		{
			$tax_info = $this->CI->Item_taxes->get_info($item['item_id']);

			foreach($tax_info as $tax)
			{
				$name = $tax['percent'].'% ' . $tax['name'];
				$tax_amount=($item['price']*$item['quantity']-$item['discount'])*(($tax['percent'])/100);


				if (!isset($taxes[$name]))
				{
					$taxes[$name] = 0;
				}
				$taxes[$name] += $tax_amount;
			}
		}

		return $taxes;
	}

	function get_subtotal()
	{
		$subtotal = 0;
		$mode = $this->get_mode();
		if ($mode == 'return') {
			// $subtotal = $this->total_old_payment();
		
		}
		foreach($this->get_cart() as $item)
		{
		    // $subtotal+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		    if ($this->get_mode()=='return') {
		    	if ($item['is_return']==1) {
		    		// $sub_per_item = 0

		    		$subtotal += ($item['price']-$item['discount']) *(-1);

		    		// $subtotal*= -1;
		    	}
		    }else{
		    	$subtotal+=($item['price']*$item['quantity']-$item['discount']);
		    }
		}

		// if ($mode=='return') {
		// 	return to_currency_no_money($subtotal);
			
		// }
		return to_currency_no_money($subtotal);
	}

	function get_total()
	{
		$total = 0;
		foreach($this->get_cart() as $item)
		{
            // $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
            if ($this->get_mode()=='return') {
            	if ($item['is_return']==1) {
            		$total+=($item['price']*$item['quantity']-$item['discount']);
            	}
            }else{
            	$total+=($item['price']*$item['quantity']-$item['discount']);

            }
		}

		foreach($this->get_taxes() as $tax)
		{
			$total+=$tax;
		}

		return to_currency_no_money($total);
	}
	function set_return_sale_id($sale_id){
		$this->CI->session->set_userdata('return_sale_id',$sale_id);
	}
	function unset_return_sale_id(){
		$this->CI->session->unset_userdata('return_sale_id');
	}
	function get_return_sale_id(){
		return $this->CI->session->userdata('return_sale_id');
	}
	function set_filter($filters){
		$this->CI->session->set_userdata('filter_applied',$filters);
	}
	function add_filter($id,$name,$f){
		if ($id=='other') {
			$name='OTHER';
		}
		$counter=1;
		$last_key = 0;
		$current_filter = $this->get_filter();
		$filter = array();
		$is=0;
		if ($current_filter!=false) {
			$counter += count($current_filter);
			
			foreach ($current_filter as $line => $fi) {
				
				if ($fi['fi']==$f && $fi['fid']==$id) {
					$is = 1;
				}
				if ($f=='make' && $fi['fi']=='make') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='model' && $fi['fi']=='model') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='head' && $fi['fi']=='head') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='sa' && $fi['fi']=='sa') {
					$this->unset_filter();
					
				}
			}
			if ($is!=1) {
				$filter = array(
							'fi'=>$f,
							'fid'=>$id,
							'fname'=>$name
							);
				array_push($current_filter, $filter);
			}
			

		}else{
			$filter = array(array(
							'fi'=>$f,
							'fid'=>$id,
							'fname'=>$name
							)		
						);
			$current_filter = $filter;
		}
		
		// $this->CI->session->set_userdata('filter_applied',array($counter=>array('fid'=>$id,'fname'=>$name)));
		// +=;
		
		// array_push($current_filter, $filter);
		
		$this->set_filter($current_filter);
	}
	function get_filter(){
		return $this->CI->session->userdata('filter_applied');
	}
	function unset_filter($f,$id){
		if (!$f || $f=='all') {
			$this->CI->session->unset_userdata('filter_applied');
		}
		$counter = count($current_filter);
		$current_filter = $this->get_filter();
		
		if ($current_filter!=false) {
			// $counter += count($current_filter);
			foreach ($current_filter as $line => $fi) {
				if ($id=='') {
					unset($current_filter[$line]);
				}else{
				 	if($fi['fi']==$f && $fi['fid']==$id) {
					// $is = 1;
						unset($current_filter[$line]);
					}
				// $current_filter+= $filter;
				}
			}
			// if ($is==1) {
			// 	$filter = array(
			// 				'fi'=>$f,
			// 				'fid'=>$id,
			// 				'fname'=>$name
			// 				);
			// 	array_push($current_filter, $filter);
			// }
			

		}
		// if (!$id) {
			// $this->CI->session->unset_userdata('filter_applied');
		$this->set_filter($current_filter);
		// }
	}

	// //////////////////
	function set_filter_vin($filters){
		$this->CI->session->set_userdata('filter_applied_vin',$filters);
	}
	function add_filter_vin($id,$name,$f){
		if ($id=='other') {
			$name='OTHER';
		}
		$counter=1;
		$last_key = 0;
		$current_filter = $this->get_filter_vin();
		$filter = array();
		$is=0;
		if ($current_filter!=false) {
			$counter += count($current_filter);
			
			foreach ($current_filter as $line => $fi) {
				
				if ($fi['fi']==$f && $fi['fid']==$id) {
					$is = 1;
				}
				if ($f=='make' && $fi['fi']=='make') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='model' && $fi['fi']=='model') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='head' && $fi['fi']=='head') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='sa' && $fi['fi']=='sa') {
					$this->unset_filter();
					
				}
			}
			if ($is!=1) {
				$filter = array(
							'fi'=>$f,
							'fid'=>$id,
							'fname'=>$name
							);
				array_push($current_filter, $filter);
			}
			

		}else{
			$filter = array(array(
							'fi'=>$f,
							'fid'=>$id,
							'fname'=>$name
							)		
						);
			$current_filter = $filter;
		}
		
		// $this->CI->session->set_userdata('filter_applied',array($counter=>array('fid'=>$id,'fname'=>$name)));
		// +=;
		
		// array_push($current_filter, $filter);
		
		$this->set_filter_vin($current_filter);
	}
	function get_filter_vin(){
		return $this->CI->session->userdata('filter_applied_vin');
	}
	function unset_filter_vin($f,$id){
		if (!$f || $f=='all') {
			$this->CI->session->unset_userdata('filter_applied_vin');
		}
		$counter = count($current_filter);
		$current_filter = $this->get_filter_vin();
		
		if ($current_filter!=false) {
			// $counter += count($current_filter);
			foreach ($current_filter as $line => $fi) {
				if ($id=='') {
					unset($current_filter[$line]);
				}else{
				 	if($fi['fi']==$f && $fi['fid']==$id) {
					// $is = 1;
						unset($current_filter[$line]);
					}
				// $current_filter+= $filter;
				}
			}
			// if ($is==1) {
			// 	$filter = array(
			// 				'fi'=>$f,
			// 				'fid'=>$id,
			// 				'fname'=>$name
			// 				);
			// 	array_push($current_filter, $filter);
			// }
			

		}
		// if (!$id) {
			// $this->CI->session->unset_userdata('filter_applied');
		$this->set_filter_vin($current_filter);
		// }
	}

	// //////////////////////////////////
	function set_filter_tem($filters){
		$this->CI->session->set_userdata('filter_applied_tem',$filters);
	}
	function add_filter_tem($id,$name,$f){
		if ($id=='other') {
			$name='OTHER';
		}
		$counter=1;
		$last_key = 0;
		$current_filter = $this->get_filter_tem();
		$filter = array();
		$is=0;
		if ($current_filter!=false) {
			$counter += count($current_filter);
			
			foreach ($current_filter as $line => $fi) {
				
				if ($fi['fi']==$f && $fi['fid']==$id) {
					$is = 1;
				}
				if ($f=='make' && $fi['fi']=='make') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='model' && $fi['fi']=='model') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='head' && $fi['fi']=='head') {
					if ($id!=$fi['fid']) {
						unset($current_filter[$line]);
					}
				}
				if ($f=='sa' && $fi['fi']=='sa') {
					$this->unset_filter();
					
				}
			}
			if ($is!=1) {
				$filter = array(
							'fi'=>$f,
							'fid'=>$id,
							'fname'=>$name
							);
				array_push($current_filter, $filter);
			}
			

		}else{
			$filter = array(array(
							'fi'=>$f,
							'fid'=>$id,
							'fname'=>$name
							)		
						);
			$current_filter = $filter;
		}
		
		// $this->CI->session->set_userdata('filter_applied',array($counter=>array('fid'=>$id,'fname'=>$name)));
		// +=;
		
		// array_push($current_filter, $filter);
		
		$this->set_filter_tem($current_filter);
	}
	function get_filter_tem(){
		return $this->CI->session->userdata('filter_applied_tem');
	}
	function unset_filter_tem($f,$id){
		if (!$f || $f=='all') {
			$this->CI->session->unset_userdata('filter_applied_tem');
		}
		$counter = count($current_filter);
		$current_filter = $this->get_filter_tem();
		
		if ($current_filter!=false) {
			// $counter += count($current_filter);
			foreach ($current_filter as $line => $fi) {
				if ($id=='') {
					unset($current_filter[$line]);
				}else{
				 	if($fi['fi']==$f && $fi['fid']==$id) {
					// $is = 1;
						unset($current_filter[$line]);
					}
				// $current_filter+= $filter;
				}
			}
			// if ($is==1) {
			// 	$filter = array(
			// 				'fi'=>$f,
			// 				'fid'=>$id,
			// 				'fname'=>$name
			// 				);
			// 	array_push($current_filter, $filter);
			// }
			

		}
		// if (!$id) {
			// $this->CI->session->unset_userdata('filter_applied');
		$this->set_filter_tem($current_filter);
		// }
	}

	function get_all_payemnt(){
		$old_payment = $this->get_old_sale_payment();
		$new_payment = $this->get_payment();
		$payment = array_merge($old_payment,$new_payment);
		return $payment;
	}

	function set_updatable(){
		$this->CI->session->set_userdata('sale_updatable',true);
	}
	function get_updatable(){
		return $this->CI->session->userdata('sale_updatable');
	}
	function unset_updatable(){
		$this->CI->session->unset_userdata('sale_updatable');
	}

	function clear_edit_sale(){
		$this->CI->session->unset_userdata('edit_sale_id');
	}

	function total_old_payment(){
		$old_payment = $this->get_old_sale_payment();
		$total_sale_returned = $this->get_old_return_payment(); 

		$subtotal = 0;

		foreach($old_payment as $payments)
		{
		    $subtotal+=$payments['payment_amount'];
		}

		return $subtotal+$total_sale_returned;

	}

	function has_item_return(){

		$items = $this->get_cart();
		
		foreach ($items as $item) {
			if ($item['is_return'] == 1) {
				return true;
			}
		}

		return false;

	}

	function total_return_item(){
		$items = $this->get_cart();

		$item_total=0;
		foreach ($items as $item) {
			if ($item['is_return'] == 1) {
				$item_total += $item['price'] - $item['discount'];
			}
		}

		return $item_total;
	}

	function get_return_total(){
		// $total = $this->total_return_item();
		$total_old_payment = $this->total_old_payment();
		$total_item_b4 = $this->get_total_item_before_restock();
		$subtotal = $this->get_subtotal();
		$amount_payable = $this->get_amount_payable();
		$total_restock = $this->get_total_restock();

		// // if ($total<$total_old_payment) {
		// // 	return -$total;
		// // }else{
		// // 	return -$total_old_payment;
		// // }


		//WHEN OLD PAYMENT IS GREATER THAN MAX PAYABLE
		//WHEN SOME ITEM RETURN SO MAX PAYMENT = SELECTED ITEM PRICE
		// if ($total_old_payment>=$total_item_b4*(-1)) {
		// 	$total_old_payment = $total_item_b4*(-1);
		// }
		// if ($total_old_payment>$subtotal*(-1)) {
		// 	$total = $subtotal;
			
		// }else{
		// 	$total = $subtotal*(-1) - $total_old_payment;

		// }

		$total = 0;
		if ($total_old_payment>0) {	//HAVE SALE PAYMENT

			if ($total_old_payment>$total_item_b4 && $total_restock<=0) {
				$total_old_payment = $total_item_b4;
				
				
				
				
			}

			$total = $total_restock - $total_old_payment;





			//IF PAYMENT SET
			$return_payment = $this->get_return_payment();
			if ($return_payment!=0) {
				if ($return_payment<$total) {
					
				}else{
					$total = $return_payment;
				}
			}
			

		}else{					//NO PAYMENT
			$total = 0;
		}


		// if ($this->get_payments_total()!=0) {
		// 	$total = $this->get_payments_total();
		// }


		$this->set_new_payment($total);

		return $total;
	}

	public function set_new_payment($payment_amount)
	{
		if ($payment_amount!=0 OR $this->has_item_return()) {
			$emp_id = $this->CI->session->userdata('person_id');
			$emp = $this->CI->Employee->get_info($emp_id);
			$emp_name = $emp->last_name.' '.$emp->first_name;
			$now = date('Y-m-d H:i:s');
			
			$payment[0] = [
							'payment_type'=>'Cash',
							'payment_amount'=>$payment_amount,
							'date'=> $now,
							'emp_name'=>$emp_name,
							'emp_id'=>$emp_id
						];
			$this->set_payments($payment);
		}else{
			$this->unset_payments();
		}
		


	}
	public function unset_payments()
	{
		$this->CI->session->unset_userdata('payments');
	}
	function get_total_restock(){
		$items = $this->get_cart();

		$total_restock=0;
		foreach ($items as $item) {
			if ($item['is_return'] == 1) {
				$total_restock += $item['discount'];
			}
		}

		return $total_restock;
	}

	function get_amount_tender(){ //FOR RETURN
		$total_payment = $this->get_payments_total() + $this->total_old_payment(); //TOTAL PAID
		$total_return = $this->total_return_item(); //TOTAL TO PAY - OR +
		$total_item_b4 = $this->get_total_item_before_restock(); //OTAL ITEM BEFORE -RESTOCK
		$total_restock = $this->get_total_restock();
		$total_old_payment = $this->total_old_payment();



		$amount_tender = $total_item_b4 + $total_restock + $total_old_payment;

		// if ($total_old_payment<($amount_tender*(-1))) {
		// 	$amount_tender = $amount_tender + $total_old_payment;
		// }
		return $this->get_return_total();
	}

	function get_total_item_before_restock(){
		$items = $this->get_cart();

		$total_item_b4=0;
		foreach ($items as $item) {
			if ($item['is_return'] == 1) {
				$total_item_b4 += $item['price'];
			}
		}

		return $total_item_b4;
	}

	function get_amount_payable(){
		$amount = $this->get_total_item_before_restock();
		if ($this->total_old_payment()<$this->get_subtotal()) {
			$amount = $total_old_payment;
		}

		return $amount;
	}

	function get_cart_total()
	{
		$items = $this->get_cart();

		$total=0;
		foreach ($items as $item) {
			$total += $item['price'];
		}

		return $total;
	}

	public function set_return_payment($amount)
	{
		// $this->CI->session->set_userdata('return_payment',$amount);
		$this->return_payment = $amount;
	}

	public function get_return_payment()
	{
		return $this->return_payment;
	}

	public function unset_return_payment()
	{
		// $this->CI->session->unset_userdata('return_payment');
		$this->return_payment = 0;
		return $this->get_return_payment();
	}



	public function set_old_return_payment($total_payment)
	{
		$this->CI->session->set_userdata('old_return_payment',$total_payment);
	}

	public function get_old_return_payment()
	{
		return $this->CI->session->userdata('old_return_payment');
	}

	public function unset_old_return_payment()
	{
		$this->CI->session->unset_userdata('old_return_payment',$total_payment);
		
	}
}
?>