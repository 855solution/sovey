<?php
class Model extends CI_Model
{
	/*
	Determines if a given model_id is an model
	*/
	function exists( $model_id )
	{
		//$this->db->from('models');
		//$this->db->where('model_id',$model_id);
		//$this->db->where('deleted',0);
		
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where('model_id',$model_id);
		$this->db->where('models.deleted',0);

		
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the models
	*/
	function get_all($limit=10000, $offset=0)
	{
//		$this->db->from('models');
//		$this->db->where('deleted',0);
//		$this->db->order_by("model_name", "asc");
//		$this->db->limit($limit);
//		$this->db->offset($offset);
//		return $this->db->get();

		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where('models.deleted',0);
		$this->db->order_by("model_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	
	}
	
	function count_all()
	{
		//$this->db->from('models');
		//$this->db->where('deleted',0);
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where('models.deleted',0);
		
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular model
	*/
	function get_info($model_id)
	{
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where('model_id',$model_id);
		$this->db->where('models.deleted',0);
		
		//$this->db->from('models');
		//$this->db->where('model_id',$model_id);
		//$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $model_id is NOT an model
			$model_obj=new stdClass();

			//Get all the fields from models table
			$fields = $this->db->list_fields('models');

			foreach ($fields as $field)
			{
				$model_obj->$field='';
			}

			return $model_obj;
		}
	}

	/*
	Get an model id given an model number
	*/
	function get_model_id($model_name)
	{
		//$this->db->from('models');
		//$this->db->where('model_name',$model_name);
		//$this->db->where('deleted',0);
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where('model_name',$model_name);
		$this->db->where('models.deleted',0);

		if($query->num_rows()==1)
		{
			return $query->row()->model_id;
		}

		return false;
	}

	/*
	Gets information about multiple models
	*/
	function get_multiple_info($model_ids)
	{
//		$this->db->from('models');
//		$this->db->where_in('model_id',$model_ids);
//		$this->db->where('deleted',0);
//		$this->db->order_by("model_name", "asc");
//		return $this->db->get();
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where_in('model_id',$model_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("model_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a model
	*/
	function save_old(&$model_data,$model_id=false)
	{
		if (!$model_id or !$this->exists($model_id))
		{
			if($this->db->insert('models',$model_data))
			{
				$model_data['model_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('model_id', $model_id);
		return $this->db->update('models',$model_data);
	}

	/*
	Updates multiple models at once
	*/
	function update_multiple($model_data,$model_ids)
	{
		$this->db->where_in('model_id',$model_ids);
		return $this->db->update('models',$model_data);
	}

	/*
	Deletes one model
	*/
	function delete($model_id)
	{
		$this->db->where('model_id', $model_id);
		return $this->db->update('models', array('deleted' => 1));
	}

	/*
	Deletes a list of models
	*/
	function delete_list($model_ids)
	{
		$this->db->where_in('model_id',$model_ids);
		return $this->db->update('models', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find models
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		//$this->db->from('models');
//		$this->db->like('model_name', $search);
//		$this->db->where('deleted',0);
//		$this->db->order_by("model_name", "asc");
//		$by_name = $this->db->get();
//		foreach($by_name->result() as $row)
//		{
//			$suggestions[]=$row->model_name;
//		}
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where("(ospos_models.deleted=0) and (model_name LIKE '%".$this->db->escape_like_str($search)."%' or make_name LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("model_name", "asc");
		$by_name = $this->db->get();
		
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->model_name;
			$suggestions[]=$row->make_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on models
	*/
	function search($search)
	{
//		$this->db->from('models');
//		$this->db->where("model_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
//		$this->db->order_by("model_name", "asc");
//		return $this->db->get();
		
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where("(ospos_models.deleted=0) and (model_name LIKE '%".$this->db->escape_like_str($search)."%' or make_name LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("model_name", "asc");
		return $this->db->get();	
	}
	
	public function get_model_model_description( $model_name )
	{
		if ( !$this->exists( $this->get_model_id($model_name)))
			return 0;
		
		$this->db->from('models');
		$this->db->where('model_name',$model_name);
		return $this->db->get()->row()->model_description;
	}
	
	function update_model_model_description( $model_name, $model_description )
	{
		$this->db->where('model_name', $model_name);
		$this->db->update('models', array('model_description' => $model_description));
	}
	
function get_model_depend_make($make_id)
	{
		$limit=10000;
		$offset=0;
		$this->db->from('models');
		$this->db->join('makes','models.make_id=makes.make_id');
		$this->db->where('models.make_id',$make_id);
		$this->db->where('models.deleted',0);
		$this->db->order_by("model_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	
	}
	function get_all_model(){
		return $this->db->query("SELECT *,mo.model_description as description FROM ospos_models mo 
								INNER JOIN ospos_makes m ON mo.make_id=m.make_id
								WHERE mo.deleted=0")->result();
	}
	function check_name($n,$id,$m){
		$w = '';
		if ($id!='') {
			$w = " AND model_id <> $id";
		}
		$w.= " AND make_id = '$m'";
		return $this->db->query("SELECT * FROM ospos_models WHERE model_name = '$n' {$w}  AND deleted=0 LIMIT 1")->row();
	}
	function save($id,$data){
		if ($id!='') {
			$this->db->where('model_id',$id)->update('ospos_models',$data);
		}else{
			$this->db->insert('ospos_models',$data);
		}
	}
}
?>
