<?php
class Color extends CI_Model
{
	/*
	Determines if a given color_id is an color
	*/
	function exists( $color_id )
	{
		$this->db->from('colors');
		$this->db->where('color_id',$color_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the colors
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('colors');
		$this->db->where('deleted',0);
		$this->db->order_by("color_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('colors');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular color
	*/
	function get_info($color_id)
	{
		$this->db->from('colors');
		$this->db->where('color_id',$color_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $color_id is NOT an color
			$color_obj=new stdClass();

			//Get all the fields from colors table
			$fields = $this->db->list_fields('colors');

			foreach ($fields as $field)
			{
				$color_obj->$field='';
			}

			return $color_obj;
		}
	}

	/*
	Get an color id given an color number
	*/
	function get_color_id($color_name)
	{
		$this->db->from('colors');
		$this->db->where('color_name',$color_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->color_id;
		}

		return false;
	}

	/*
	Gets information about multiple colors
	*/
	function get_multiple_info($color_ids)
	{
		$this->db->from('colors');
		$this->db->where_in('color_id',$color_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("color_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a color
	*/
	function save_old(&$color_data,$color_id=false)
	{
		if (!$color_id or !$this->exists($color_id))
		{
			if($this->db->insert('colors',$color_data))
			{
				$color_data['color_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('color_id', $color_id);
		return $this->db->update('colors',$color_data);
	}

	/*
	Updates multiple colors at once
	*/
	function update_multiple($color_data,$color_ids)
	{
		$this->db->where_in('color_id',$color_ids);
		return $this->db->update('colors',$color_data);
	}

	/*
	Deletes one color
	*/
	function delete($color_id)
	{
		$this->db->where('color_id', $color_id);
		return $this->db->update('colors', array('deleted' => 1));
	}

	/*
	Deletes a list of colors
	*/
	function delete_list($color_ids)
	{
		$this->db->where_in('color_id',$color_ids);
		return $this->db->update('colors', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find colors
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('colors');
		$this->db->like('color_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("color_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->color_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on colors
	*/
	function search($search)
	{
		$this->db->from('colors');
		$this->db->where("color_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("color_name", "asc");
		return $this->db->get();	
	}
	
	public function get_color_description( $color_name )
	{
		if ( !$this->exists( $this->get_color_id($color_name)))
			return 0;
		
		$this->db->from('colors');
		$this->db->where('color_name',$color_name);
		return $this->db->get()->row()->description;
	}
	
	function update_color_description( $color_name, $description )
	{
		$this->db->where('color_name', $color_name);
		$this->db->update('colors', array('description' => $description));
	}
	function get_all_color(){
		return $this->db->query("SELECT * FROM ospos_colors WHERE deleted=0 ")->result();
	}
	function check_name($n,$id){
		$w = '';
		if ($id!='') {
			$w = " AND color_id <> $id";
		}
		return $this->db->query("SELECT * FROM ospos_colors WHERE color_name = '$n' {$w}  AND deleted=0 LIMIT 1")->row();
	}
	function save($id,$data){
		if ($id!='') {
			$this->db->where('color_id',$id)->update('ospos_colors',$data);
		}else{
			$this->db->insert('ospos_colors',$data);
		}
	}

}
?>
