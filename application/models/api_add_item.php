<?php
class Api_add_item extends CI_Model
{
	function getdefaultimg($item_id){
		$api = $this->load->database('api', TRUE);
		$api->select('imagename');
		$api->from('image_items');
		$api->where('item_id', $item_id);
		$api->order_by("first_image", "asc");
		$get_image = $api->get();
		$get_image = $get_image->result_array();
		return  $get_image;
            
        }
	
	function khmernames($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('khmernames');
		$api->where('deleted',0);
		$api->order_by("khmername_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		$khmer_names = $api->get();
		$khmer_names = $khmer_names->result_array();
		return  $khmer_names;
	}

	function categories($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('categorys');
		$api->where('deleted',0);
		$api->order_by("category_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}

	function makes($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('makes');
		$api->where('deleted',0);
		$api->order_by("make_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}

	function models($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('models');
		$api->join('makes', 'makes.make_id = models.make_id');
		$api->where('models.deleted',0);
		$api->order_by("model_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	
	}

	function year()
	{
                $api = $this->load->database('api', TRUE);
		$api->from('items');
		$api->select('
				items.year,
				items.item_id,
				make_id,
				model_id
				');
		$api->where('items.year >', 1990);
		$api->group_by('items.item_id');
		$arr_year = $api->get();
		
		return $arr_year;
	}

	function colors($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('colors');
		$api->where('deleted',0);
		$api->order_by("color_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}

	function conditions($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('conditions');
		$api->where('deleted',0);
		$api->order_by("condition_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}

	function partplacements($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('partplacements');
		$api->where('deleted',0);
		$api->order_by("partplacement_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}

	function branchs($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('branchs');
		$api->where('deleted',0);
		$api->order_by("branch_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}

	function locations($limit=10000, $offset=0)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('locations');
		$api->where('deleted',0);
		$api->order_by("location_name", "asc");
		$api->limit($limit);
		$api->offset($offset);
		return $api->get();
	}
	function part_number($part_number)
	{
		$api = $this->load->database('api', TRUE);
		$api->from('part');
		$api->where('part_number', $part_number);
		$data = $api->get();
		$data = $data->row_array();
		return $data;
		
	}
	function search_part_number()
	{
		$api = $this->load->database('api', TRUE);
		$api->select('partid, part_number, part_name');
		$api->from('part');
		$api->group_by('part_number');
		return $api->get();
		
	}
	function update_part($part_number, $part_name, $khmername_id, $category_id, $fitment, $make_id, $model_id, $year, $color_id, $condition_id, $part_placement, $brand_id, $location_id, $cost_price, $unit_price, $quantity)
	{
		$api = $this->load->database('api', TRUE);
		$data = array(
               'part_name' => $title,
               'khmernames_id' => $khmername_id,
               'category_id' => $category_id,
               'fit' => $fitment,
               'model_id' => $model_id,
               'make_id' => $make_id,
               'year' => $year,
               'color_id' => $color_id,
               'codition_id' => $condition_id,
               'part_placement' => $part_placement,
               'brand_id' => $brand_id,
               'location_id' => $location_id,
               'cost_price' => $cost_price,
               'unit_price' => $unit_price,
               'quantity' => $quantity,
            );

		$api->where('part_number', $part_number);
		$api->update('part', $data); 
	}
	function add_item($json_send)
	{
		$api = $this->load->database('api', TRUE);

		$data = json_decode($json_send);
		$data = (array)$data;
		$item_id = $this->last_item_id() + 1;
		$year = $data['year'];
		$make_name  = $this->get2FirstString($data['make_name']);
		$model_name = $this->get2FirstString($data['model_name']);
			
		$digit_padding = str_pad($item_id, 6, "0", STR_PAD_LEFT);
		$get_year = $this->get2lastString($year);
		$barcode = $get_year.$make_name.$model_name.$digit_padding;
		
		$item_data = array(
			 //'name'=>$this->input->post('name'),
			'name'=>$data['part_name'],
			'item_number'=>$data['part_number'],
			'description'=>$data['description'],
			'category_id'=>$data['category_id'],
			//'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
			//'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
			'cost_price'=>$data['cost_price'],
			'unit_price'=>$data['unit_price'],
			'quantity'=>$data['quantity'],
			//'reorder_level'=>$this->input->post('reorder_level'),
			//'location'=>$this->input->post('location'),
			'fit'=>$data['fit'],
			'model_id'=>$data['model_id'],
			'year'=>$data['year'],
			'color_id'=>$data['color_id'],
			'condition_id'=>$data['condition_id'],
			'partplacement_id'=>$data['partplacement_id'],
			'branch_id'=>$data['branch_id'],
			//'allow_alt_description'=>$this->input->post('allow_alt_description'),
			//'is_serialized'=>$this->input->post('is_serialized'),
			'location_id'=>$data['location_id'] == '' ? null:$data['location_id'],
			'make_id' => $data['make_id'],
			'is_new' => $data['is_new'],
			'is_feat' => $data['is_feat'],
			'is_pro' => $data['is_pro'],
			//'barcode' => $data['barcode'] == '' ? null:$data['barcode'],
			'allow_alt_description' => $data['allow_alt_description'] == '' ? 0:$data['allow_alt_description'],
			'is_serialized' => $data['is_serialized'] == '' ? 0:$data['is_serialized'],
			'reorder_level' => $data['reorder_level'],
			//'category'=>$this->input->post('category'),
			//'is_new'=>$this->input->post('is_new'),
			//'is_pro'=>$this->input->post('is_pro'),
			//'is_feat'=>$this->input->post('is_feat'),
			//'grad_id'=>$grad_id,
			'barcode' => $barcode,
			'khmername_id'=>$data['khmername_id']

			);
		$insert = $api->insert('items', $item_data);
		if($insert)
		{
			$part = $this->add_part($data['part_number'], $data['part_name'], $data['khmername_id'], $data['category_id'], $data['fit'], $data['make_id'], $data['model_id'], $data['year'], $data['color_id'], $data['condition_id'], $data['partplacement_id'], $data['branch_id'], $data['location_id']);
			$item_id = $this->last_item_id();
			$this->add_image_item($item_id, $json_send);
			return true;
		}else
		{
			return false;
		}
	}

	function add_part($part_number, $part_name, $khmer_id, $category_id, $fit, $make_id, $model_id, $year, $color_id, $codition_id, $part_placement, $brand_id, $location_id) {
		$api = $this->load->database('api', TRUE);
		$api->from('part');
		$api->where('part_number', $part_number);
		$row = $api->get();
		$row = $row->num_rows();
		if ($row == 0) {
			$item_data = array(
						'part_number' => $part_number, 
						'part_name' => $part_name, 
						'khmernames_id' => $khmer_id, 
						'category_id' => $category_id, 
						'fit' => $fit, 
						'make_id' => $make_id, 
						'model_id' => $model_id, 
						'year' => $year, 
						'color_id' => $color_id, 
						'codition_id' => $codition_id, 
						'color_id' => $color_id, 
						'part_placement' => $part_placement, 
						'brand_id' => $brand_id, 
						'location_id' => $location_id,
						'cost_price' => 0,
						'unit_price' => 0,
						'quantity' =>0
					);
					
		$insert = $api->insert('part', $item_data);
		}
		
	}
	function update_item($barcode, $item_id) {
		$api = $this->load->database('api', TRUE);
		$data = array(
               'barcode' => $barcode,
            );

		$api->where('item_id', $item_id);
		$api->update('items', $data); 
	}
	function add_image_item($item_id, $json_send)
	{
		$api = $this->load->database('api', TRUE);
		$data = json_decode($json_send);
		$data = (array)$data;
		
		foreach (json_decode($data['image_list']) as  $value) {
			$timestamp = time();
			$data = base64_decode($value->image);
			file_put_contents('uploads/'.$timestamp.'.jpg', $data);
			$data_insert = array(
			   'item_id'     => $item_id,
			   'imagename'   => $timestamp.'.jpg',
			   'first_image' => 0
			);
		
			$insert = $api->insert('image_items', $data_insert);
		}
			if($insert)
				return true;
			else
				return false;
	}

	function last_item_id(){
		$api = $this->load->database('api', TRUE);
		$api->from('items');
		$api->order_by('item_id', 'desc');
		$last_id = $api->get();
		$last_id = $last_id->row();
		return $last_id->item_id;
	}
	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
	
	function get2FirstString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$end = $characters-$length;
		
		$firstString = substr($string ,0 ,$end);
		return $firstString;
		}
	}
}