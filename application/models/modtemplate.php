<?php
class Modtemplate extends CI_Model
{
	/*
	Determines if a given make_id is an make
	*/
	function exists( $make_id )
	{
		$this->db->from('ospos_part');
		$this->db->where('partid',$make_id);
		// $this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	function getimage($stockid){
        $data=$this->db->query("SELECT * FROM ospos_template_images WHERE template_id='$stockid' ORDER BY first_image ASC")->row();
       
        return $data;
    }
	/*
	Returns all the makes
	*/
	function delete_image_item($imageid){
			$query = $this->db->query("DELETE FROM ospos_template_images WHERE tem_image_id = '$imageid'");
			$row = $query->row();
	
	}	
	function updateBarcodeAfterSave($item_id,$item_data){
		$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	
	}		
	function getImageName($imageid){
		$query = $this->db->query("SELECT image_name FROM ospos_template_images WHERE tem_image_id = '$imageid'");
		$row = $query->row();
		return $row->image_name;
	}	
	 function deleteimg($imgid){
            $this->db->where('tem_image_id',$imgid)->delete('ospos_template_images');
        }
	function get_all($page, $offset=0)
	{
		
		// return $this->db->query("SELECT * 
		// 						FROM ospos_part p 
		// 						LEFT JOIN ospos_makes m 
		// 						ON(p.make_id=m.make_id) 
		// 						LEFT JOIN ospos_models mo
		// 						ON(p.model_id=mo.model_id)
		// 						LEFT JOIN ospos_colors c 
		// 						ON(p.color_id=c.color_id)
		// 						LEFT JOIN ospos_conditions con 
		// 						ON(p.condition_id=con.condition_id) LIMIT $offset,$limit")->result();
		// $this->db->select("*")
		// ->from('ospos_part p')
		// ->join('ospos_makes m','p.make_id=m.make_id','LEFT')
		// ->join('ospos_models mo','p.model_id=mo.model_id','LEFT')
		// ->join('ospos_colors c','p.color_id=c.color_id','LEFT')
		// ->join('ospos_conditions co','p.codition_id=co.condition_id','LEFT')
		// ->join('ospos_template_images ci','p.partid=ci.template_id','LEFT')
		// ->group_by('p.partid')
		// ->order_by('p.partid','desc','ci.first_image','ASC')
		// ->limit($limit)->offset($offset);
		// return $this->db->get();
		$limit=" limit $offset";
		if($page!='')
			$limit=" LIMIT $page,$offset";
		$sql="SELECT
					*
				FROM
					(
						SELECT
							p.*,
							images.image_name
						FROM
							`ospos_part` p
						LEFT JOIN `ospos_template_images` images ON(p.partid = images.template_id)
						GROUP BY
							`p`.`partid`
					)AS T1
				ORDER BY
					approve asc,partid desc
				{$limit} 
				";
		// print_r($this->db->query($sql)->result()); die();
		return $this->db->query($sql);
		// return $this->db->query("SELECT * 
		// 						FROM ospos_part p 
		// 						LEFT JOIN ospos_makes m 
		// 						ON(p.make_id=m.make_id) 
		// 						LEFT JOIN ospos_models mo
		//  						ON(p.model_id=mo.model_id)
		// 						LEFT JOIN ospos_colors c 
		//  						ON(p.color_id=c.color_id)
		// 						LEFT JOIN ospos_template_images ci 
		//  						ON(p.partid=ci.template_id)
		// 						LEFT JOIN ospos_conditions ct 
		//  						ON(p.codition_id=ct.condition_id)
		// 						GROUP BY p.partid 
		// 						ORDER BY p.partid DESC ,ci.first_image ASC
		//  					 LIMIT $limit,$offset ")->result();
	}
	
	function count_all()
	{
		$this->db->from('ospos_part');
		// $this->db->where('deleted',0);
		return $this->db->count_all_results();
	}


	/*
	Gets information about a particular make
	*/
	function get_info($make_id)
	{
		$this->db->from('ospos_part');
		$this->db->where('partid',$make_id);
		// $this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $make_id is NOT an make
			$make_obj=new stdClass();

			//Get all the fields from makes table
			$fields = $this->db->list_fields('ospos_part');

			foreach ($fields as $field)
			{
				$make_obj->$field='';
			}

			return $make_obj;
		}
	}

	/*
	Get an make id given an make number
	*/
	function get_make_id($make_name)
	{
		$this->db->from('makes');
		$this->db->where('make_name',$make_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->make_id;
		}

		return false;
	}

	/*
	Gets information about multiple makes
	*/
	function get_multiple_info($make_ids)
	{
		$this->db->from('makes');
		$this->db->where_in('make_id',$make_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("make_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a make
	*/
	function save(&$make_data,$make_id=false)
	{	
		// var_dump($make_id);
		// var_dump($make_data);die();
		if (!$make_id or !$this->exists($make_id))
		{
			if($this->db->insert('ospos_part',$make_data))
			{
				$make_data['tem_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('partid', $make_id);
		return $this->db->update('ospos_part',$make_data);
	}

	/*
	Updates multiple makes at once
	*/
	function update_multiple($make_data,$make_ids)
	{
		$this->db->where_in('partid',$make_ids);
		return $this->db->update('ospos_part',$make_data);
	}

	/*
	Deletes one make
	*/
	function delete($make_id)
	{
		$this->db->where('partid', $make_id);
		return $this->db->delete('ospos_part');
	}

	/*
	Deletes a list of makes
	*/
	function delete_list($make_ids)
	{
		$this->db->where_in('partid',$make_ids);
		return $this->db->delete('ospos_part');
 	}

 	/*
	Get search suggestions to find makes
	*/
	function get_search_suggestions($search,$limit=20)
	{

		
		$suggestions = array();

		$this->db->distinct();
		$this->db->from('makes');
		$this->db->like('make_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("make_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{

			$suggestions[]=$row->make_name;
		}

		$this->db->distinct();
		$this->db->from('part');
		$this->db->like('year', $search);
		$this->db->group_by('year');
		$this->db->order_by("partid", "asc");
		$by_year = $this->db->get();
		foreach($by_year->result() as $row)
		{
			$suggestions[]=$row->year;
		}
		$this->db->distinct();
		$this->db->from('part');
		$this->db->like('part_name', $search);
		$this->db->group_by('part_name');
		$this->db->order_by("partid", "asc");
		$by_partname = $this->db->get();
		foreach($by_partname->result() as $row)
		{
			$suggestions[]=$row->part_name;
		}
		$this->db->distinct();
		$this->db->from('part');
		$this->db->like('part_number', $search);
		$this->db->group_by('part_number');
		$this->db->order_by("partid", "asc");
		$by_partnum = $this->db->get();
		foreach($by_partnum->result() as $row)
		{
			$suggestions[]=$row->part_number;
		}

		// only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	function get_search_suggestions_filter($search,$limit=20)
	{

		
		$suggestions = array();
		$sql=$this->db->query("SELECT part_name,khmername_name FROM ospos_part p
							LEFT JOIN ospos_khmernames k ON p.khmernames_id=k.khmername_id
							WHERE part_name OR khmername_name LIKE '%".$search."%'
							GROUP BY part_name");
		// $this->db->from('part');
		// $this->db->join('khmernames','khmernames_id=khmernames_id');
		// $this->db->like("concat(part_name,'-',khmername_name)", $search);
		// $this->db->where('deleted',0);
		// $this->db->order_by("sname", "asc");
		// $by_name = $this->db->get();
		$by_name = $sql;
		foreach($by_name->result() as $row)
		{
			$part_name = $row->part_name;
			$khmername = $row->khmername_name;
			$concat = "$part_name-$khmername";
			$suggestions[]=$concat;
		}
		// $this->db->distinct();
		// $this->db->from('part');
		// $this->db->like('year', $search);
		// $this->db->group_by('year');
		// $this->db->order_by("partid", "asc");
		// $by_year = $this->db->get();
		// foreach($by_year->result() as $row)
		// {
		// 	$suggestions[]=$row->year;
		// }
		// $this->db->distinct();
		// $this->db->from('part');
		// $this->db->like('part_name', $search);
		// $this->db->group_by('part_name');
		// $this->db->order_by("partid", "asc");
		// $by_partname = $this->db->get();
		// foreach($by_partname->result() as $row)
		// {
		// 	$suggestions[]=$row->part_name;
		// }
		// $this->db->distinct();
		// $this->db->from('part');
		// $this->db->like('part_number', $search);
		// $this->db->group_by('part_number');
		// $this->db->order_by("partid", "asc");
		// $by_partnum = $this->db->get();
		// foreach($by_partnum->result() as $row)
		// {
		// 	$suggestions[]=$row->part_number;
		// }

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on makes
	*/
	function search($search)
	{	

		
		$this->db->select("*")
		->from('ospos_part p')
		->join('ospos_makes m','p.make_id=m.make_id','LEFT')
		->join('ospos_models mo','p.model_id=mo.model_id','LEFT')
		->join('ospos_colors c','p.color_id=c.color_id','LEFT')
		->join('ospos_conditions co','p.codition_id=co.condition_id','LEFT')
		->where("p.part_name LIKE '%".$this->db->escape_like_str($search)."%' OR p.part_number LIKE '%".$this->db->escape_like_str($search)."%' ")

		->order_by('p.partid','desc')
		->limit($limit)->offset($offset);

		return $this->db->get();	
	}
	
	public function get_make_description( $make_name )
	{
		if ( !$this->exists( $this->get_make_id($make_name)))
			return 0;
		
		$this->db->from('makes');
		$this->db->where('make_name',$make_name);
		return $this->db->get()->row()->description;
	}
	
	function update_make_description( $make_name, $description )
	{
		$this->db->where('make_name', $make_name);
		$this->db->update('makes', array('description' => $description));
	}


	function seachWithoutFulltext($search,$page,$offset){
	
		$this->db->from('part');
		$this->db->JOIN('makes','makes.make_id=part.make_id'); 
		$this->db->where("(part_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		part_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		make_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		year LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->limit($offset);

		if ($page!='') {
			$this->db->limit($page,$offset);
		}
		$this->db->order_by("partid", "desc");
		// var_dump($this->db);
		$query=$this->db->get();
		return $query;
		
	}
	function count_all_search($search){
		$this->db->from('part');
		$this->db->JOIN('makes','makes.make_id=part.make_id'); 
		$this->db->where("(part_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		part_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		make_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		year LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("partid", "desc");
		// var_dump($this->db);
		$query=$this->db->get();
		return $query->num_rows;
	}
	function fullTextSearch($searchfield,$page,$offset){


	$numrows=$this->seachWithoutFulltext($searchfield,$page,$offset)->num_rows();
	
	$sql="
	 
		SELECT 
		
		ospos_part.*,
		MATCH(part_name,year)AGAINST('$searchfield')as yearrank,
		MATCH(model_name,model_description)AGAINST ('$searchfield') as modelrank,
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield') as makerank
		FROM
		ospos_items
		INNER JOIN 
		ospos_models ON ospos_part.model_id=ospos_models.model_id
		INNER JOIN
		ospos_makes ON ospos_makes.make_id=ospos_part.make_id
		AND 
		(
		MATCH (part_name,year)AGAINST ('$searchfield') OR 
		MATCH (model_name,model_description)AGAINST ('$searchfield')OR
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield')
		) 
		WHERE
		order by modelrank desc, makerank desc,yearrank desc limit 0,30";

		
		
		if($numrows!=0){
			return $this->seachWithoutFulltext($searchfield,$page,$offset);
		}else{
			$query = $this->db->query($sql);
			return $query;
		}
		
	}
	function get_all_filtered_name_make_model_year($name,$make_filter=0,$model_fitler=0,$year_filter=1980,$yearto_filter=1980,$page,$offset=0)
	{

		$this->db->select('*');
		$this->db->from('part');
		$this->db->join('colors','part.color_id=colors.color_id','left');
		$this->db->join('conditions','conditions.condition_id=part.codition_id','left');
		$this->db->join('makes','makes.make_id=part.make_id','left');
		$this->db->join('models','models.model_id=part.model_id','left');
		$this->db->join('khmernames','part.khmernames_id=khmernames.khmername_id','left');

		//////////////////////////////////////1
		if ($name) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
		}
		if ($make_filter) {
			$this->db->where('part.make_id',$make_filter);
		}
		if ($model_fitler) {
			$this->db->where('part.model_id',$model_fitler);
		}
		if ($year_filter) {
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($yearto_filter) {
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		/////////////////////////////////////2
		if ($name && $make_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
		}
		if ($name && $model_fitler) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
		}
		if ($name && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('year >= "'.$year_filter.'"');
		}
		if ($name && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
		}
		if ($make_filter && $year_filter) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($make_filter && $yearto_filter) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($model_fitler && $year_filter) {
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($model_fitler && $yearto_filter) {
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($year_filter && $yearto_filter) {
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		//////////////////////////////////////3
		if ($name && $make_filter && $model_fitler) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
		}
		if ($name && $make_filter && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($name && $make_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($name && $model_fitler && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($name && $model_fitler && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year <= "'.$year_filter.'"');
		}
		if ($name && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////4
		if ($name && $make_filter && $model_fitler && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($name && $make_filter && $model_fitler && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		if ($name && $make_filter && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		if ($name && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////5
		if ($name && $make_filter && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}

		$this->db->order_by("part.part_name", "desc");
		$this->db->order_by("year", "desc");
		$this->db->limit($offset);

		if ($page!='') {
			$this->db->limit($page,$offset);
		}
	
		return $this->db->get();

	}	
	function get_info_by_partid($part_id){
		$this->db->select('*');
		$this->db->from('part');
		$this->db->where('partid',$part_id);
		return $this->db->get();
	}
	function get_last_item_id($part_id){
		$this->db->select('item_id');
		$this->db->from('items');
		$this->db->join('part','items.item_number=part.part_number','left');
		$this->db->where('partid',$part_id);
		$this->db->order_by("item_id",'desc');
		$this->db->limit(1);
		return $this->db->get()->row();
	}
	function count_all_filtered($name=0,$make_filter=0,$model_fitler=0,$year_filter=1980,$yearto_filter=1980)
	{
		$this->db->select('*');
		$this->db->from('part');
		$this->db->join('colors','part.color_id=colors.color_id','left');
		$this->db->join('conditions','conditions.condition_id=part.codition_id','left');
		$this->db->join('makes','makes.make_id=part.make_id');
		$this->db->join('models','models.model_id=part.model_id');
		$this->db->join('khmernames','part.khmernames_id=khmernames.khmername_id','left');


		//////////////////////////////////////1
		if ($name) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
		}
		if ($make_filter) {
			$this->db->where('part.make_id',$make_filter);
		}
		if ($model_fitler) {
			$this->db->where('part.model_id',$model_fitler);
		}
		if ($year_filter) {
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($yearto_filter) {
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		/////////////////////////////////////2
		if ($name && $make_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
		}
		if ($name && $model_fitler) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
		}
		if ($name && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('year >= "'.$year_filter.'"');
		}
		if ($name && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
		}
		if ($make_filter && $year_filter) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($make_filter && $yearto_filter) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($model_fitler && $year_filter) {
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($model_fitler && $yearto_filter) {
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($year_filter && $yearto_filter) {
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		//////////////////////////////////////3
		if ($name && $make_filter && $model_fitler) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
		}
		if ($name && $make_filter && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($name && $make_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($name && $model_fitler && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($name && $model_fitler && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year <= "'.$year_filter.'"');
		}
		if ($name && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////4
		if ($name && $make_filter && $model_fitler && $year_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year >= "'.$year_filter.'"');
		}
		if ($name && $make_filter && $model_fitler && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		if ($name && $make_filter && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		if ($name && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		//////////////////////////////////5
		if ($name && $make_filter && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('(part_name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('part.make_id',$make_filter);
			$this->db->where('part.model_id',$model_fitler);
			$this->db->where('part.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		$this->db->order_by("part.part_name", "desc");
		$this->db->order_by("year", "desc");

		return $this->db->get()->num_rows();

	}	
	function get_history($part_number){
		$this->db->select('*');
		$this->db->from('v_select_all_items va');
		$this->db->join('sales_items si','va.item_id=si.item_id','inner');
		$this->db->join('sales sa','si.sale_id=sa.sale_id','inner');
		$this->db->where('item_number',$part_number);
		$this->db->order_by('sale_time','desc');
		return $this->db->get()->result();
	}
	function get_template_stock($part_number){
		return $this->db->query("SELECT COUNT(*) as stock FROM ospos_items WHERE item_number='".$part_number."' AND deleted=0 AND quantity>0")->row();
	}

	function getnumsearch_other($field,$where){
        $where=str_replace("AND()","",$where);
        
        return $this->db->query("SELECT count(*) as count FROM ospos_part i 
    								WHERE 1=1
    								-- i.deleted=0 
    								-- AND i.quantity>0 
    								AND (i.$field IS NULL OR i.$field = '' OR i.$field=0) 
    								{$where}")->row()->count;
        
    }

    function getnumsearch($field,$where){
        $where=str_replace("AND()","",$where);

        $group=" GROUP BY $field";
        $data = $this->db->query("SELECT count(i.$field) as count,i.$field FROM ospos_part as i 
        	WHERE 1=1
        	-- i.deleted=0 
        	-- AND i.quantity>0 
        	AND i.$field<>'' 
        	AND i.$field IS NOT NULL 
        	{$where} 
        	{$group}")->result();
        $arr=array();
       
        foreach ($data as $row) {
        	$arr[$row->$field]=$row->count;# code...
        }
        return $arr;
    }
    function get_part_item($partnumber){
    	return $this->db->query("SELECT * FROM ospos_items WHERE item_number = '$partnumber' AND deleted=0 AND quantity>0")
    			->result();
    }
    function check_part($n,$id){
		$w = '';
		if ($id!='' && $id!=-1) {
			$w = " AND partid <> $id";
		}
		$n = str_replace(' ', '', $n);
		return $this->db->query("SELECT * FROM ospos_part WHERE part_number = '$n' {$w} LIMIT 1")->row();
	}
	function get_person($person_id){
		return $this->db->query("SELECT * FROM ospos_people WHERE person_id = '$person_id'")->row();
	}

	function add_new_from_item($data){
		$this->db->insert('ospos_part',$data);
	}
    

}
?>
