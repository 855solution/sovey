<?php
class Item extends CI_Model
{
	function getremoveitem(){
		$date=date('Y-m-d',strtotime("-14 day",strtotime(date('Y-m-d'))));
		return $this->db->query("SELECT DISTINCT trans_items 
								FROM ospos_inventory i
								INNER JOIN ospos_items t
								ON(i.trans_items=t.item_id)
								WHERE i.trans_date<'$date' AND t.quantity<=0 AND t.deleted=0")->result();
	}
	function get_dropdown($name, $value)
    {
        $arr = array();
        switch($name)
        {
            case 'make_id' :
                $query = $this->db->where('make_id', $value)->get('models');
                
                if($query->num_rows() > 0)
                {
                    foreach($query->result() as $row)
                    {
                        $arr[] = array($row->model_id => $row->model_name);
                    }
                }
                else
                {
                    $arr[] = array('0' => 'No model');
                }
            break;
            default :
                $arr[] = array(
                    array('1' => 'Data 1'),
                    array('2' => 'Data 2'),    
                    array('3' => 'Data 3')
                );
            break;
        }
        return $arr;
    }

	/*
	Determines if a given item_id is an item
	*/
	function exists($item_id)
	{
		$this->db->from('items');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($limit=10000, $offset=0)
	{
		
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function getdefaultimg($item_id){
        return $this->green->getValue("SELECT imagename FROM ospos_image_items where item_id='$item_id' ORDER BY image_id ASC limit 1");
    }

    function getItemTempImage($item_number){
    	$partid = $this->db->query("SELECT partid FROM ospos_part WHERE part_number = '$item_number'")->row()->partid;

    	return $this->db->query("SELECT * FROM ospos_template_images WHERE template_id = '$partid'")->row()->image_name;
    }
    function getVinImage($vin_id){
    	return $this->db->query("SELECT * FROM ospos_head_images WHERE vin_number = '$vin_id'
									ORDER BY first_image ASC LIMIT 1")->row()->url_image;
    }
	function count_all()
	{
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		return $this->db->count_all_results();
	}
	function count_all_filtered($make_filter=0,$model_fitler=0,$year_filter=1980,$yearto_filter=1980,$khmername_filter){
		
		$this->db->select('*');
		$this->db->from('items');
		$this->db->join('colors','items.color_id=items.color_id','left');
		$this->db->join('conditions','conditions.condition_id=items.condition_id','left');
		$this->db->join('makes','makes.make_id=items.make_id','left');
		$this->db->join('models','models.model_id=items.model_id','left');
		$this->db->join('khmernames','items.khmername_id=khmernames.khmername_id','left');

		//////////////////////////////////////1
		if ($khmername_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

		}
		if ($make_filter) {
			$this->db->where('items.make_id',$make_filter);
		}
		if ($model_fitler) {
			$this->db->where('items.model_id',$model_fitler);
		}
		if ($year_filter) {
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($yearto_filter) {
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		/////////////////////////////////////2
		if ($khmername_filter && $make_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
		}
		if ($khmername_filter && $model_fitler) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.model_id',$model_fitler);
		}
		if ($khmername_filter && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
		}
		if ($make_filter && $year_filter) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($make_filter && $yearto_filter) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($model_fitler && $year_filter) {
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($model_fitler && $yearto_filter) {
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($year_filter && $yearto_filter) {
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		//////////////////////////////////////3
		if ($khmername_filter && $make_filter && $model_fitler) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

		}
		if ($khmername_filter && $make_filter && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $make_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($khmername_filter && $model_fitler && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $model_fitler && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year <= "'.$year_filter.'"');
		}
		if ($khmername_filter && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////4
		if ($khmername_filter && $make_filter && $model_fitler && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $make_filter && $model_fitler && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		if ($khmername_filter && $make_filter && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		if ($khmername_filter && $model_fitler && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////5
		if ($khmername_filter && $make_filter && $model_fitler && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}

		$this->db->order_by("items.name", "desc");
		return $this->db->get()->num_rows();
	}
	function getimage($stockid){
        $data=$this->db->query("SELECT * FROM ospos_image_items WHERE item_id='$stockid' ORDER BY first_image ASC")->result();
       
        return $data;
    }
    function deleteimg($imgid){
            $this->db->where('image_id',$imgid)->delete('ospos_image_items');
        }
    function count_filtered($low_inventory=0,$is_serialized=0,$no_description){
		$this->db->select('*');
    	$this->db->from('items');
		if ($low_inventory !=0 )
		{
			$this->db->where('quantity <=',' reorder_level', false);
		}
		if ($is_serialized !=0 )
		{
			$this->db->where('is_serialized',1);
		}
		if ($no_description!=0 )
		{
			$this->db->where('description','');
		}
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		return $this->db->get()->num_rows();
    }
	function get_all_filtered($low_inventory=0,$is_serialized=0,$no_description,$page,$offset)
	{
		$this->db->select('*');
		$this->db->from('items');
		if ($low_inventory !=0 )
		{
			$this->db->where('quantity <=','reorder_level', false);
		}
		if ($is_serialized !=0 )
		{
			$this->db->where('is_serialized',1);
		}
		if ($no_description!=0 )
		{
			$this->db->where('description','');
		}
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit($offset);

		if ($page!='') {
			$this->db->limit($page,$offset);
		}
		// var_dump($tis->db->get());
		return $this->db->get();
	}

	/*
	Gets information about a particular item
	*/
	function get_infokhmername($item_id)
	{
		$this->db->from('items');
		$this->db->join('khmernames', 'khmernames.khmername_id = items.khmername_id');
		$this->db->where('item_id',$item_id);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	
	function get_info($item_id)
	{
		//$this->db->from('items');
		// $this->db->select('*','items.description as item_desc');

		$this->db->select('*');
		$this->db->select('items.description as desc_item');
		$this->db->from('items');
		$this->db->join('khmernames', 'items.khmername_id = khmernames.khmername_id','left');
		$this->db->join('partplacements', 'items.partplacement_id = partplacements.partplacement_id','left');
		$this->db->where('item_id',$item_id);

		$query = $this->db->get();
		// var_dump(gettype($item_id));die();

		// if (gettype($item_id)=='string') {
		// 	$where = "items.barcode='$item_id'";
		// }else{
		// 	$where = "items.item_id= $item_id";
		// }

		// $query = $this->db->query("SELECT *,items.description as desc_item FROM ospos_itemss items
		// 							LEFT JOIN ospos_khmernames khmernames ON items.khmername_id = khmernames.khmername_id
		// 							LEFT JOIN ospos_partplacements partplacements ON items.partplacement_id = partplacements.partplacement_id
		// 							WHERE {$where}");

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	/*
	Get an item id given an item number
	*/
	function get_item_id($item_number)
	{
		$this->db->from('items');
		$this->db->where('item_number',$item_number);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->item_id;
		}

		return false;
	}
	function get_vin_info($vin_number){
		$query = $this->db->query("SELECT * FROM ospos_vinnumbers
									WHERE vinnumber_name='$vin_number'");
		// var_dump($query->row()->vinnumber_id);die();
		if ($query->num_rows()==1) {
			return $query->row();
		}else{
			return false;
		}
	}
	function get_new_item_id($new_item_number){
		$this->db->from('items_from_sale');
		$this->db->where('category',$new_item_number);
		$this->db->order_by('item_id','desc');
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows()==1) {
			return $query->row()->item_id;
		}
		return false;
	}
	// function get_new_item_last_barcode(){
	// 	$query = $this->db->query("select barcode from ospos_items_from_sale 
	// 		where item_id=(select MAX(item_id) from ospos_items_from_sale)")->row()->barcode;
	// 	var_dump($this->db);
	// 	return $query;
	// 	// $this->db->select('*');
	// 	// $this->db->from('items_from_sale');
	// 	// $this->db->where('item_id',$new_item_id);
	// 	// $query = $this->db->get();
	// 	// return $query->row()->barcode;
	// }
	function get_new_item_info($item_id){
		$this->db->select('*');
		$this->db->from('items_from_sale');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();
		
		if ($query->num_rows()==1) {
			return $query->row();
		}
		return false;
	}
	function get_new_item_make_model_partplacement($item_id){
		$this->db->select('model_name,make_name,partplacement_name');
		$this->db->from('items_from_sale it');
		$this->db->join('models mo','it.model_id = mo.model_id','left');
		$this->db->join('makes ma','it.make_id = ma.make_id','left');
		$this->db->join('partplacements pa','it.partplacement_id = pa.partplacement_id','left');
		$this->db->where('item_id',$item_id);

		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		}else{
			return false;
		}
	}
	function get_new_item_partplacement($item_id){
		$this->db->select('partplacement_id');
		$this->db->from('items_from_sale');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();
		if ($query->num_rows()==1) {
			return $query->row();
		}else{
			return false;
		}
	}
	/*
	Gets information about multiple items
	*/
	function get_multiple_info($item_ids)
	{
		$this->db->from('items');
		$this->db->where_in('item_id',$item_ids);
		$this->db->order_by("item_id", "desc");
		return $this->db->get();
	}
	function save_vin_sale($data){
		if ($this->db->insert('vinnumber_sale',$data)) {
			return true;
		}else{
			return false;
		}
	}
	function get_sale_id($item_id){
		// var_dump($item_id);die();
		if (strpos($item_id, '-')) {
			$this->db->from('sales');
			$this->db->where('invoiceid',$item_id);
			$sale_row = $this->db->get()->row();
		}else if(strpos($item_id,'000')){
			$sale_row= $this->db->query("SELECT * FROM ospos_sales WHERE invoiceid LIKE '%$item_id'")->row();
		}else{
			$sale_row = $this->db->query("SELECT * FROM ospos_sales_items si
			INNER JOIN ospos_v_select_all_items i ON i.item_id = si.item_id AND i.is_new = si.is_new
			WHERE i.barcode = '$item_id'")->row();

		}
		// var_dump(strpos($item_id,'000'));
		// var_dump($sale_row);die();
		
		return $sale_row->sale_id;
	}
	/*
	Inserts or updates a item
	*/
	function save(&$item_data,$item_id=false)
	{

		if (!$item_id or !$this->exists($item_id))
		{
			if($this->db->insert('items',$item_data))
			{
				// var_dump('insert');die();
				$item_data['item_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}
		// var_dump('update');die();
		$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	}
	function update_item_sales($item_data,$item_id){
		$this->db->where('item_id',$item_id);
		return $this->db->update('items_from_sale',$item_data);
	}

	function save_item_sale($item_data){
		$this->db->insert('items_from_sale',$item_data);

		return $this->db->insert_id();
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($item_data,$item_ids)
	{
		$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items',$item_data);
	}

	/*
	Deletes one item
	*/
	function delete($item_id)
	{
		$this->db->where('item_id', $item_id);
		return $this->db->update('items', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find items
	*/
	function get_search_suggestions($vin,$search,$limit=15)
	{
		// $vin = $this->session->userdata('vin');
		$suggestions = array($search);

		$this->db->from('items');
		$this->db->like('name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->where('quantity >',0);
		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->order_by("item_id", "desc");
		$this->db->group_by("name");
		$by_name = $this->db->get();
		// var_dump($by_name->result());
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->name;
		}
		// $this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->like('item_number', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->where('quantity >',0);

		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->order_by("item_id", "desc");
		$this->db->group_by("item_number");

		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_number;
		}

		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->where('quantity >',0);

		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		// $this->db->distinct();
		$this->db->like('category', $search);
		$this->db->order_by("item_id", "desc");
		$this->db->group_by("category");

		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		$this->db->from('items');
		$this->db->like('barcode', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->where('quantity >',0);

		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->order_by("item_id", "desc");
		$this->db->group_by("barcode");

		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->barcode;
		}

		// var_dump($suggestions);die();
		//only return $limit suggestions
		if(count($suggestions) > $limit)
		{
			$suggestions = array_slice($suggestions,0,$limit);
		}
		
		// var_dump($suggestions);die();
		
		return $suggestions;

	}

	function get_item_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('name', $search);
		$this->db->order_by("item_id", "desc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->name;
		}
		$this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('item_number', $search);
		$this->db->order_by("item_id", "desc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->item_number;
		}

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('barcode', $search);
		$this->db->order_by("item_id", "desc");
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->barcode;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_item_search_suggestions_sale($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('item_id', $search);
		$this->db->order_by("item_id", "desc");
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->item_id;
		}
		$this->db->distinct();
		
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('barcode', $search);
		$this->db->order_by("item_id", "desc");
		$by_bar = $this->db->get();
		foreach($by_bar->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->barcode;
		}
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	function get_item_search_suggestions_return($search,$limit=25)
	{
		$suggestions = array();

		// $this->db->from('sales_items');
		// // $this->db->where('deleted',0);
		// // $this->db->where('ishead',0);
		// // $this->db->where('is_new',0);
		// $this->db->where('is_return',0);
		// $this->db->like('item_id', $search,'after');
		// $this->db->order_by("item_id", "desc");
		// $by_item_number = $this->db->get();
		// foreach($by_item_number->result() as $row)
		// {
		// 	$suggestions[]=$row->item_id.'|'.$row->item_id;
		// }
		// $this->db->distinct();
		
		// $this->db->from('v_select_all_items');
		// $this->db->where('deleted',0);
		// $this->db->where('ishead',0);
		// $this->db->like('barcode', $search,'after');
		// $this->db->or_like('item_id',$search,'after');
		// $this->db->order_by("item_id", "desc");
		// // $this->db->group_by('item_id');
		// // $this->db->group_by('is_new');
		// $by_bar = $this->db->get();
		// foreach($by_bar->result() as $row)
		// {
		// 	$this->db->from('sales_items');
		// 	// $this->db->where('is_new',0);
		// 	$this->db->where('is_return',0);
		// 	$this->db->where('item_id',$row->item_id);
		// 	$sales_items = $this->db->get();
		// 	foreach ($sales_items->result() as $item) {
		// 		if ($row->item_id==$item->item_id && $row->is_new==$item->is_new) {
		// 			$suggestions[]=$row->item_id.'|'.$row->barcode;
		// 		}
		// 	}
		// }

		if (strpos($search, '-')) {
			$where = "s.invoiceid LIKE '%".$search."%'";
		}else{
			$where = "i.barcode LIKE '%".$search."%'";
		}
		$query = $this->db->query("SELECT s.invoiceid,i.item_id,i.barcode FROM ospos_sales_items si
									INNER JOIN ospos_v_select_all_items i ON si.item_id = i.item_id AND si.is_new=i.is_new
									INNER JOIN ospos_sales s ON si.sale_id = s.sale_id
									WHERE {$where}
									LIMIT {$limit}")->result();
		foreach ($query as $item) {
			
			$suggestions[]=$item->invoiceid.'|'.$item->barcode;
			
		}
		//only return $limit suggestions
		// if(count($suggestions > $limit))
		// {
		// 	$suggestions = array_slice($suggestions, 0,$limit);
		// }
		return $suggestions;

	}


	function get_category_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('items');
		$this->db->like('category', $search);
		$this->db->where('deleted', 0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	/*
	Preform a search on items
	*/
	function search($search)
	{
		$this->db->from('items');
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
		barcode LIKE '%".$this->db->escape_like_str($search)."%' or
		item_number LIKE '%".$this->db->escape_like_str($search)."%' or
		category LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("item_id", "desc");
		return $this->db->get();	
	}

	function get_categories()
	{
		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->distinct();
		$this->db->order_by("item_id", "desc");

		return $this->db->get();
	}
	
	function get_partNumber_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->like('item_number', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("item_id", "desc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->item_number;
		}

		return $suggestions;
	}

	function make_name($makeId){
		$suggestions = array();
		
		$this->db->select('make_name');
		$this->db->from('ospos_makes');
		$this->db->where('make_id', $makeId);
		$get_result=$this->db->get();
		return $get_result;
		
	}
	
	function model_name($modelId){
		$suggestions = array();
		
		$this->db->select('model_name');
		$this->db->from('models');
		$this->db->where('model_id', $modelId);
		$get_result=$this->db->get();
		return $get_result;
		
	}
	function color_name($color_id){
		$this->db->select('color_name');
		$this->db->from('colors');
		$this->db->where('color_id',$color_id);
		return $this->db->get();
	}
	function location_name($location_id){
		$this->db->select('location_name');
		$this->db->from('locations');
		$this->db->where('location_id',$location_id);
		return $this->db->get();
	}
	function body_name($body_id){
		return $this->db->select('body_name')
						->from('body')
						->where('body_id',$body_id)
						->get();
	}

	function branch_name($branch_id){
		return $this->db->select('branch_name')
						->from('branchs')
						->where('branch_id',$branch_id)
						->get();
	}
	function part_placement_name($partp_id){
		return $this->db->select('partplacement_name')
						->from('partplacements')
						->where('partplacement_id',$partp_id)
						->get();
	}
	function engine_name($engine_id){
		return $this->db->select('engine_name')
						->from('engine')
						->where('engine_id',$engine_id)
						->get();
	}
	function fuel_name($fuel_id){
		return $this->db->select('fule_name')
						->from('fuel')
						->where('fuel_id',$fuel_id)
						->get();
	}
	function trans_name($trans_id){
		return $this->db->select('transmission_name')
						->from('transmission')
						->where('trans_id',$trans_id)
						->get();
	}
	function trim_name($trim_id){
		return $this->db->select('trim_name')
						->from('trim')
						->where('trim_id',$trim_id)
						->get();
	}
	function condition_name($con_id){
		return $this->db->select('condition_name')
						->from('conditions')
						->where('deleted',0)
						->where('condition_id',$con_id)
						->get();
	}
	
	function listAllInfoItem($item_number){
		$this->db->from('items');
		$this->db->where('item_number',$item_number);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$query = $this->db->get();
		
	}
	
	/*
	Gets information about a particular item
	*/
	function get_info_where_itemnumber($itemnumber)
	{
		$this->db->from('items');
		$this->db->where('item_number',$itemnumber);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	/*
	function get_image_source($itemId){
		
		//$this->db->select('imagename');
		$this->db->from('image_items');
		$this->db->where('item_id', $itemId);
		$get_result=$this->db->get();
		return $get_result;
		
	}	
	*/
	
	function get_image_source($item_id)
	{
		$this->db->from('image_items');
		$this->db->where('item_id', $item_id);
		$get_result=$this->db->get();
		return $get_result;
	}
	
	function get_all_grade($limit=10000, $offset=0)
	{
		$this->db->from('grade');
		$this->db->order_by("grad_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function get_unitPrice_costPrice($gradid)
	{
		$this->db->from('items');
		$this->db->where('grad_id', $gradid);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		$get_result=$this->db->get();
		return $get_result;
	}
	
function getLastId(){
	
		
		$query = $this->db->query('SELECT max(item_id) as maxid FROM ospos_items');
		$row = $query->row();
		return $max_id = $row->maxid; 
		
		}
		
function updateBarcodeAfterSave($item_id,$item_data){
	$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	
	}
function updateNewItemBarcodeAfterSave($item_id,$item_data){
	$this->db->where('item_id', $item_id);
		return $this->db->update('items_from_sale',$item_data);
	
	}	
function delete_Image_item($imageid){
	$query = $this->db->query("DELETE FROM ospos_image_items WHERE image_id = '$imageid'");
	$row = $query->row();
	
}		
function getImageName($imageid){
	$query = $this->db->query("SELECT imagename FROM ospos_image_items WHERE image_id = '$imageid'");
	$row = $query->row();
	return $row->imagename;
}	
function mobile_fullTextSearch($searchfield,$hasstock){


	//$numrows=$this->seachWithoutFulltext($searchfield)->num_rows();
if($hasstock>0){
$stocksql=" AND ospos_items.quantity>0 ";

}else{
$stocksql="";
}	
	$sql="
	SELECT 
		ospos_items.item_id, ospos_colors.color_name,ospos_items.year, ospos_conditions.condition_name, ospos_makes.make_name,ospos_items.name,ospos_items.fit,ospos_items.item_number,ospos_items.description,ospos_models.model_name,ospos_branchs.branch_name,ospos_partplacements.partplacement_name,ospos_image_items.imagename,
		MATCH(name,year)AGAINST('$searchfield')as yearrank,
		MATCH(model_name,model_description)AGAINST ('$searchfield') as modelrank,
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield') as makerank
		FROM
		ospos_items
		LEFT JOIN 
		ospos_models ON ospos_items.model_id=ospos_models.model_id
		LEFT JOIN
		ospos_makes ON ospos_makes.make_id=ospos_items.make_id
		LEFT JOIN
		ospos_branchs ON ospos_items.branch_id=ospos_branchs.branch_id
		LEFT JOIN
		ospos_partplacements ON ospos_items.partplacement_id=ospos_partplacements.partplacement_id
                LEFT JOIN
		ospos_image_items ON ospos_items.item_id=ospos_image_items.item_id
   		LEFT JOIN
		ospos_conditions ON ospos_items.condition_id=ospos_conditions.condition_id
		LEFT JOIN
		ospos_colors ON ospos_items.color_id=ospos_colors.color_id
		
		WHERE
		ospos_items.deleted=0 and ishead=0
AND 
		(
		MATCH (name,year)AGAINST ('$searchfield')>0 OR 
		MATCH (model_name,model_description)AGAINST ('$searchfield')>0 OR
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield')>0
		) 
		
$stocksql
		
group by item_id
		order by modelrank desc, makerank desc,yearrank desc limit 0, 1000";
                
		
		
		//if($numrows!=0){
		//	return $this->seachWithoutFulltext($searchfield);
		//}else{
			$query = $this->db->query($sql);
			return $query;
		//}
		
}

function fullTextSearch($searchfield,$page,$offset){


	$numrows=$this->seachWithoutFulltext($searchfield,$page,$offset)->num_rows();
	
	$sql="
	 
		SELECT 
		
		ospos_items.*,
		MATCH(name,year)AGAINST('$searchfield')as yearrank,
		MATCH(model_name,model_description)AGAINST ('$searchfield') as modelrank,
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield') as makerank
		FROM
		ospos_items
		INNER JOIN 
		ospos_models ON ospos_items.model_id=ospos_models.model_id
		INNER JOIN
		ospos_makes ON ospos_makes.make_id=ospos_items.make_id
		AND 
		(
		MATCH (name,year)AGAINST ('$searchfield') OR 
		MATCH (model_name,model_description)AGAINST ('$searchfield')OR
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield')
		) 
		WHERE
		ospos_items.deleted=0 and ishead=0
		order by modelrank desc, makerank desc,yearrank desc limit 0, 30";

		
		
		if($numrows!=0){
			return $this->seachWithoutFulltext($searchfield,$page,$offset);
		}else{
			$query = $this->db->query($sql);
			return $query;
		}
		
}

function seachWithoutFulltext($search,$page,$offset){
	
		$this->db->from('items');
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
		item_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		barcode LIKE '%".$this->db->escape_like_str($search)."%') and (deleted=0 and ishead=0)");
		// if ($vin!='') {
		// 	$this->db->where('category',$vin);
		// }
		$this->db->order_by("item_id", "desc");
		$this->db->limit($offset);
		if ($page!='') {
			$this->db->limit($page,$offset);
		}
		$query=$this->db->get();
		return $query;
		
}

function check_where($where){
	return $this->db->query("SELECT * FROM ospos_items i WHERE i.deleted=0 {$where}");
}
function count_all_search($vin,$search){
	$this->db->from('items');
	$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
	item_number LIKE '%".$this->db->escape_like_str($search)."%' or 
	barcode LIKE '%".$this->db->escape_like_str($search)."%') and (deleted=0 and ishead=0)");
	if ($vin!='') {
			$this->db->where('category',$vin);
		}
	$this->db->order_by("item_id", "desc");
	$query=$this->db->get();
	return $query->num_rows();
}

/*Sale*/
function convertBarcodetoItemid($barcode){
	
		$this->db->from('items');
		$this->db->where('barcode',$barcode);
		$this->db->where('deleted',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	
}
function convert6DigitToID($digits){
	return $this->db->from('items')
					->where('deleted',0)
					->like('barcode',$digits)
					->get()
					->row()
					->item_id;
}



function khmer_name($khmer_id){
		$suggestions = array();
		
		$this->db->select('khmername_name,english_name');
		$this->db->from('khmernames');
		$this->db->where('khmername_id',$khmer_id);
		$get_result=$this->db->get();
		return $get_result;
		
	}

function get_all_filtered_make_model_year($make_filter=0,$model_fitler=0,$year_filter=1980,$yearto_filter=1980,$khmername_filter,$page,$offset)
	{
		
		$this->db->select('*');
		$this->db->from('items');
		$this->db->join('colors','items.color_id=items.color_id','left');
		$this->db->join('conditions','conditions.condition_id=items.condition_id','left');
		$this->db->join('makes','makes.make_id=items.make_id','left');
		$this->db->join('models','models.model_id=items.model_id','left');
		$this->db->join('khmernames','items.khmername_id=khmernames.khmername_id','left');

		//////////////////////////////////////1
		if ($khmername_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
		}
		if ($make_filter) {
			$this->db->where('items.make_id',$make_filter);
		}
		if ($model_fitler) {
			$this->db->where('items.model_id',$model_fitler);
		}
		if ($year_filter) {
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($yearto_filter) {
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		/////////////////////////////////////2
		if ($khmername_filter && $make_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
		}
		if ($khmername_filter && $model_fitler) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.model_id',$model_fitler);
		}
		if ($khmername_filter && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
		}
		if ($make_filter && $year_filter) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($make_filter && $yearto_filter) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($model_fitler && $year_filter) {
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($model_fitler && $yearto_filter) {
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($year_filter && $yearto_filter) {
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		//////////////////////////////////////3
		if ($khmername_filter && $make_filter && $model_fitler) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);

		}
		if ($khmername_filter && $make_filter && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $make_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($khmername_filter && $model_fitler && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $model_fitler && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year <= "'.$year_filter.'"');
		}
		if ($khmername_filter && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////4
		if ($khmername_filter && $make_filter && $model_fitler && $year_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year >= "'.$year_filter.'"');
		}
		if ($khmername_filter && $make_filter && $model_fitler && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year <= "'.$yearto_filter.'"');
		}
		if ($make_filter && $model_fitler && $year_filter && $yearto_filter) {
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');

		}
		if ($khmername_filter && $make_filter && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		if ($khmername_filter && $model_fitler && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		//////////////////////////////////5
		if ($khmername_filter && $make_filter && $model_fitler && $year_filter && $yearto_filter) {
			// $this->db->where('(name LIKE "%'.$name.'%" OR khmername_name LIKE "%'.$name.'%")');
			$this->db->where('items.khmername_id',$khmername_filter);
			$this->db->where('items.make_id',$make_filter);
			$this->db->where('items.model_id',$model_fitler);
			$this->db->where('items.year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}

		$this->db->order_by("items.name", "desc");
		$this->db->order_by("year", "desc");
		$this->db->limit($offset);

		if ($page!='') {
			$this->db->limit($page,$offset);
		}
		// var_dump($khmername_filter);die();
		// return $this->db->get();
		// var_dump($this->db);
		return $this->db->get();
	}	

	function get_info_where_makeId($makeid)
	{
		$this->db->from('items');
		$this->db->where('make_id',$makeid);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	function get_info_where_modelId($modelid)
	{
		$this->db->from('items');
		$this->db->where('model_id',$modelid);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}	
	
	function get_make_model_year($itemid)
	{
		$this->db->from('items');
		$this->db->join('makes', 'makes.make_id = items.make_id','left');
		$this->db->join('models', 'models.model_id = items.model_id','left');
		$this->db->join('colors', 'colors.color_id = items.color_id','left');
		$this->db->join('partplacements', 'partplacements.partplacement_id = items.partplacement_id','left');
		$this->db->join('branchs', 'branchs.branch_id = items.branch_id','left');
		$this->db->where('item_id',$itemid);
		$this->db->where('items.deleted',0);
		//$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	
	function mobile_fullTextSearch2($searchfield,$cat,$model,$make,$color,$brand,$condition,$hasstock){

		if($hasstock>0){
		$stocksql=" AND ospos_items.quantity>0 ";

		}else{
		$stocksql="";
		}	
			
			$sql="
			SELECT 
				ospos_items.item_id, ospos_colors.color_name,ospos_items.year, ospos_conditions.condition_name, ospos_makes.make_name,ospos_items.name,ospos_items.fit,ospos_items.item_number,ospos_items.description,ospos_models.model_name,ospos_branchs.branch_name,ospos_partplacements.partplacement_name,ospos_image_items.imagename
				
				FROM
				ospos_items
				LEFT JOIN 
				ospos_models ON ospos_items.model_id=ospos_models.model_id
				LEFT JOIN
				ospos_makes ON ospos_makes.make_id=ospos_items.make_id
				LEFT JOIN
				ospos_branchs ON ospos_items.branch_id=ospos_branchs.branch_id
				LEFT JOIN
				ospos_partplacements ON ospos_items.partplacement_id=ospos_partplacements.partplacement_id
		                LEFT JOIN
				ospos_image_items ON ospos_items.item_id=ospos_image_items.item_id
		   		LEFT JOIN
				ospos_conditions ON ospos_items.condition_id=ospos_conditions.condition_id
				LEFT JOIN
				ospos_colors ON ospos_items.color_id=ospos_colors.color_id

				WHERE
				ospos_items.deleted=0 and ishead=0
				AND 
				(";
				
				if($searchfield){
					$sql.="MATCH (name,year)AGAINST ('$searchfield')>0 OR 
					MATCH (model_name,model_description)AGAINST ('$searchfield')>0 OR
					MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield')>0
					) 
			AND 
					(";
				}
				if($model)
					$sql.="	ospos_items.model_id = $model AND ";
				if($color)
					$sql.="	ospos_items.color_id = $color AND ";
				if($brand)
					$sql.="	ospos_items.branch_id = $brand AND ";
				if($make)
					$sql.=" ospos_items.make_id = $make AND ";
				if($condition)
					$sql.=" ospos_items.condition_id = $condition ";
				else
					$sql.=" 1 = 1";
				

				$sql.=") 
		$stocksql
				group by item_id
				order by ospos_items.name desc limit 0, 1000";
		                
				//echo $sql;
				//ospos_items
				//if($numrows!=0){
				//	return $this->seachWithoutFulltext($searchfield);
				//}else{
					$query = $this->db->query($sql);
					return $query;
				//}
				
	}
	function get_color_name($color_id){
		return $this->db->select('color_name')->from('colors')->where('color_id',$color_id)->get()->row()->color_name;		
	}
	
    function getnumsearch_other($field,$where){
    	// if(trim($where)=='AND()'){
     //    	$where='';
     //    }
    	$where=str_replace("AND()","",$where);
        
        return $this->db->query("SELECT count(*) as count FROM ospos_items i 
    								WHERE i.deleted=0 
    								-- AND i.quantity>0 
    								AND (i.$field IS NULL OR i.$field = '' OR i.$field=0) 
    								{$where}")->row()->count;
    }
    function getnumsearch($field,$where){
        // $where='';
        $where=str_replace("AND()","",$where);
        
       	if ($this->config->item('show_item_quantiy_0')!=1) {
			$where.= " AND i.quantity > 0";
		}

        $group=" GROUP BY $field";
        $data = $this->db->query("SELECT count(i.$field) as count,i.$field FROM ospos_items as i 
        	WHERE i.deleted=0 
        	-- AND i.quantity>0 
        	AND i.$field<>'' 
        	AND i.$field IS NOT NULL 
        	{$where} 
        	{$group}")->result();
        $arr=array();
       
        foreach ($data as $row) {
        	$arr[$row->$field]=$row->count;# code...
        }
        return $arr;
    }
    function get_item_ft_date($df,$dt){
    	return $this->db->select('item_id')
    					->from('items')
    					->where("(add_date BETWEEN '$df' AND '$dt')")
    					->get()
    					->result();
    }
    function get_item_ft_digit($df,$dt){
    	return $this->db->select('item_id')
    					->from('items')
    					->where("(item_id BETWEEN $df AND $dt)")
    					->where('deleted',0)
    					->get()
    					->result();
    }
    function get_new_item_khmername($item_id){
    	$data = $this->db->query("SELECT * FROM ospos_items_from_sale i
    								LEFT JOIN ospos_khmernames k ON i.khmername_id=k.khmername_id
    							WHERE item_id='$item_id'")->row()->khmername_name;
    	return $data;
    }
    function getAllSaleItem(){
    	// $where = " ";
		return $this->db->query("SELECT * FROM ospos_sales_items s
								INNER JOIN ospos_v_select_all_items i ON s.item_id=i.item_id AND s.is_new=i.is_new
								LEFT JOIN ospos_khmernames k ON i.khmername_id = k.khmername_id
								LEFT JOIN ospos_makes m ON i.make_id = m.make_id
								LEFT JOIN ospos_models mo ON i.model_id=mo.model_id
								LEFT JOIN ospos_partplacements p ON i.partplacement_id=p.partplacement_id
								WHERE 1=1 AND (i.category='' OR i.category IS NULL) AND s.is_return = 0")->result();
	}
	function getAllVin(){
		return $this->db->query("SELECT * FROM ospos_vinnumbers WHERE deleted=0")->result();
	}
	function getVinSale($item_id,$is_new){
		return $this->db->query("SELECT *FROM ospos_vinnumber_sale WHERE item_id=$item_id AND is_new = $is_new")->row();
	}
	function get_item_suggest($search){
		$suggestions = array($search);

		$this->db->from('items');
		$this->db->like('name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('name');
		$by_name = $this->db->get();
		// var_dump($by_name->result());
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->name;
		}
		$this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->like('item_number', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->order_by("item_number", "desc");
		$this->db->group_by('name');

		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_number;
		}

		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->distinct();
		$this->db->like('category', $search);
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('category');

		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		$this->db->from('items');
		$this->db->like('barcode', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		if ($vin!='') {
			$this->db->where('category',$vin);
		}
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('barcode');

		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->barcode;
		}
		$this->db->from('items');
		$this->db->like('make_name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('make_name');

		$by_make_name = $this->db->get();
		foreach($by_make_name->result() as $row)
		{
			$suggestions[]=$row->make_name;
		}
		// KHMER NAME
		$this->db->from('items');
		$this->db->like('khmer_name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('khmer_name');
		
		$by_khmer_name = $this->db->get();
		foreach($by_khmer_name->result() as $row)
		{
			$suggestions[]=$row->khmer_name;
		}
		// Model NAME
		$this->db->from('items');
		$this->db->like('model_name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('model_name');
		
		$by_model_name = $this->db->get();
		foreach($by_model_name->result() as $row)
		{
			$suggestions[]=$row->model_name;
		}
		// YEAR
		$this->db->from('items');
		$this->db->like('`year`', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('`year`');
		
		$by_year_name = $this->db->get();
		foreach($by_year_name->result() as $row)
		{
			$suggestions[]=$row->year;
		}


		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions,0,$limit);
		}
		// COLOR
		$this->db->from('items');
		$this->db->like('color_name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('`color_name`');
		
		$by_color_name = $this->db->get();
		foreach($by_color_name->result() as $row)
		{
			$suggestions[]=$row->color_name;
		}
		// PPL
		$this->db->from('items');
		$this->db->like('partplacement_name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		
		$this->db->order_by("item_id", "desc");
		$this->db->group_by('partplacement_name');
		
		$by_ppl_name = $this->db->get();
		foreach($by_ppl_name->result() as $row)
		{
			$suggestions[]=$row->partplacement_name;
		}


		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions,0,$limit);
		}
		
		
		return $suggestions;
	}
	function check_item_status($item_id){
		return $this->db->query("SELECT * FROM ospos_items WHERE 1=1 AND(item_id = '$item_id' OR barcode = '$item_id') AND quantity>0")->row()->item_status;
	}
	function get_sus_emp_by_item($item_id){
		$people = $this->db->query("SELECT p.title,p.last_name,p.first_name FROM ospos_sales_suspended s
									LEFT JOIN ospos_sales_suspended_items i ON s.sale_id=i.sale_id
									INNER JOIN ospos_people p ON s.employee_id = p.person_id
									WHERE i.item_id = $item_id AND (s.status <> 'cancel' OR s.status IS NULL)")->row();

		return $people->title.$people->last_name.' '.$people->first_name;
	}

	function get_vinnumber(){
		return $this->db->query("SELECT * FROM ospos_vinnumbers WHERE deleted=0")->result();
	}
	function get_loc_name($loc_id){
		return $this->db->query("SELECT * FROM ospos_locations WHERE location_id='$loc_id'")->row()->location_name;
	}

	function get_change_data($val,$type){
		$val = preg_replace('/\s+/', '', $val);
		if ($type=='p') {
			$part = $this->db->query("SELECT * FROM ospos_part WHERE part_number = '$val' LIMIT 1")->row();
			if ($part) {
				return $part;
			}else{
				return 0;
			}
		}
		if ($type=='v') {
			$vid = $this->item->get_vin_info($val)->vinnumber_id;
			return $this->db->query("SELECT * FROM ospos_vinnumbers WHERE vinnumber_id= '$vid' AND deleted=0 LIMIT 1")->row();
		}
	}
	function checktempapprove($pnum){
		return $this->db->query("SELECT * FROM ospos_part WHERE part_number = '$pnum'")->row()->approve;
	}
	function countAllItemImage($item_id){
		$img = $this->db->query("SELECT * FROM ospos_image_items WHERE item_id = '$item_id'")->result();

		return count($img);
	}
	
}
?>