<?php
class Api_search_data extends CI_Model
{

	public function search($data)
	{
		$json = json_decode($data);
		$start = ($json->page - 1) * $json->limit;
		$api = $this->load->database('api', TRUE);
		
		$api->select('makes.make_name,
				items.year,
				items.item_id,
				items.name,
				items.unit_price,
				items.cost_price,
				items.quantity,
				items.barcode,
				items.fit,
				items.description,
				items.item_number,
				colors.color_name,
				models.model_name,
				conditions.condition_name,
				khmernames.khmername_name,
				partplacements.partplacement_name,
				branchs.branch_name,
				categorys.category_name,
				vinnumbers.vinnumber_name
				
				');
		$api->from('items');
		$api->join('locations', 'items.location_id = locations.location_id', 'left');
		$api->join('colors', 'colors.color_id = items.color_id', 'left');
		$api->join('categorys', 'categorys.category_id = items.category_id', 'left');
		$api->join('conditions', 'conditions.condition_id = items.condition_id', 'left');
		$api->join('partplacements', 'partplacements.partplacement_id = items.partplacement_id', 'left');
		$api->join('branchs', 'branchs.branch_id = items.branch_id', 'left');
		$api->join('makes', 'makes.make_id = items.make_id', 'left');
		$api->join('models', 'models.model_id = items.model_id', 'left');
		$api->join('khmernames', 'khmernames.khmername_id = items.khmername_id', 'left');
		$api->join('vinnumbers', 'vinnumbers.make_id = items.make_id','left');
		
	 	$arr_color = [];
		foreach($json->arr_color as $index => $data) {
	        	array_push($arr_color, $data->color);
	        }
	        
	       
	        $arr_location = [];
	        
	        foreach($json-> arr_location as $data){
			array_push($arr_location, $data->location);
	        	
	        }
	      	if ($arr_color[0] != "all") {
	      		$api->where_in('items.color_id', $arr_color);
	      	}
	      	
	    
	      	if (($json->make) != "all") {
	      		 $api->where('items.make_id', $json->make);
	      	}
	       
	        if (($json->model) != "all"){
	        	$api->where('items.model_id', $json->model);
		}
		if ($arr_location[0] != "all"){
	        	$api->where_in('items.location_id', $arr_location);
		
		}
		
		if (($json->condition) != "all") {
	      		 $api->where('items.condition_id', $json->condition);
	      	}
	      	
	      	if (($json->partplacement) != "all") {
	      		 $api->where('items.partplacement_id', $json->partplacement);
	      	}
	      	if (($json->branch) != "all") {
	      		 $api->where('items.branch_id', $json->branch);
	     	}
	     	 if ($json->start_year != "all" && $json->end_year != "all") {
	        	$api->where("items.year BETWEEN '$json->start_year' AND '$json->end_year'");
		        $api->where('items.year >=', (int)$json->start_year);
			//$api->where('items.year', 2000);
		}
		if ($json->text != "all") {
		 	
		 $api->like("( `ospos_makes`.`make_name` LIKE '%$json->text%' 
		 		OR `ospos_models`.`model_name` LIKE '%$json->text%' 
		 		OR `ospos_colors`.`color_name` LIKE '%$json->text%' 
		 		OR `ospos_items`.`name` LIKE '%$json->text%' 
		 		OR `ospos_items`.`year` LIKE '%$json->text%' 
		 		OR `ospos_items`.`description` LIKE '%$json->text%')
		 	");
	 	}
	 	
	 	$api->where('items.deleted', 0);
	 	$api->limit($json->limit, $start);
	
	 	$api->order_by('items.item_id', 'desc');
	        
		return $api->get();
		
		
	}
	
	public function count($data) {
		$json = json_decode($data);
		$start = ($json->page - 1) * $json->limit;
		$api = $this->load->database('api', TRUE);
		$api->select('makes.make_name,
				items.year,
				items.item_id,
				items.name,
				colors.color_name,
				models.model_name,
				conditions.condition_name,
				partplacements.partplacement_name,
				branchs.branch_name
				
				');
		$api->from('items');
		$api->join('locations', 'items.location_id = locations.location_id', 'left');
		$api->join('colors', 'colors.color_id = items.color_id', 'left');
		$api->join('categorys', 'categorys.category_id = items.category_id', 'left');
		$api->join('conditions', 'conditions.condition_id = items.condition_id', 'left');
		$api->join('partplacements', 'partplacements.partplacement_id = items.partplacement_id', 'left');
		$api->join('branchs', 'branchs.branch_id = items.branch_id', 'left');
		$api->join('makes', 'makes.make_id = items.make_id', 'left');
		$api->join('models', 'models.model_id = items.model_id', 'left');
		$api->join('khmernames', 'khmernames.khmername_id = items.khmername_id', 'left');
		$api->join('vinnumbers', 'vinnumbers.make_id = items.make_id','left');
		
	 	$arr_color = [];
		foreach($json->arr_color as $index => $data) {
	        	array_push($arr_color, $data->color);
	        }
	        
	        if ($json->start_year != "all" && $json->end_year != "all") {
	        	$api->where("items.year BETWEEN '$json->start_year' AND '$json->end_year'");
		        //$api->where('items.year >=', (int)$json->start_year);
			//$api->where('items.year <=', (int)$json->end_year);
		}
	        $arr_location = [];
	        
	        foreach($json-> arr_location as $data){
			array_push($arr_location, $data->location);
	        	
	        }
	      	if ($arr_color[0] != "all") {
	      		$api->where_in('items.color_id', $arr_color);
	      	}
	      	
	      	
	      	
	      	if (($json->make) != "all") {
	      		 $api->where('items.make_id', $json->make);
	      	}
	       
	        if (($json->model) != "all"){
	        	$api->where('items.model_id', $json->model);
		}
		if ($arr_location[0] != "all"){
	        	$api->where_in('items.location_id', $arr_location);
		
		}
		
		if (($json->condition) != "all") {
	      		 $api->where('items.condition_id', $json->condition);
	      	}
	      	
	      	if (($json->partplacement) != "all") {
	      		 $api->where('items.partplacement_id', $json->partplacement);
	      	}
	      	if (($json->branch) != "all") {
	      		 $api->where('items.branch_id', $json->branch);
	     	}
	     	
		if ($json->text != "all") {
		 	$api->like("( `ospos_makes`.`make_name` LIKE '%$json->text%' 
		 		OR `ospos_models`.`model_name` LIKE '%$json->text%' 
		 		OR `ospos_colors`.`color_name` LIKE '%$json->text%' 
		 		OR `ospos_items`.`name` LIKE '%$json->text%' 
		 		OR `ospos_items`.`year` LIKE '%$json->text%' 
		 		OR `ospos_items`.`description` LIKE '%$json->text%')
		 	");
	 	}
	 	
	 	$api->where('items.deleted', 0);
	 	$api->order_by('items.item_id', 'desc');
	        
		

	        
		$row = $api->get();
		return $row->num_rows();
	}
	
}
?>