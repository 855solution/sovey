<?php
class modtrim extends CI_Model
{
	/*
	Determines if a given make_id is an make
	*/
	function exists( $trim_id )
	{
		$this->db->from('trim');
		$this->db->where('trim_id',$trim_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the makes
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('trim');
		$this->db->where('deleted',0);
		$this->db->order_by("trim_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('body');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular trim
	*/
	function get_info($trim_id)
	{
		$this->db->from('trim');
		$this->db->where('trim_id',$trim_id);
		// $this->db->where('deleted',0);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $trim_id is NOT an trim
			$trim_obj=new stdClass();

			//Get all the fields from trim table
			$fields = $this->db->list_fields('trim');

			foreach ($fields as $field)
			{
				$trim_obj->$field='';
			}

			return $trim_obj;
		}
	}

	/*
	Get an make id given an make number
	*/
	function get_make_id($make_name)
	{
		$this->db->from('makes');
		$this->db->where('make_name',$make_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->make_id;
		}

		return false;
	}

	/*
	Gets information about multiple makes
	*/
	function get_multiple_info($make_ids)
	{
		$this->db->from('makes');
		$this->db->where_in('make_id',$make_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("make_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a Trim
	*/
	// function save($trim_data,$trim_id)
	// {
	// 	if ($trim_id==-1)
	// 	{
	// 		$this->db->insert('trim',$trim_data);
	// 		return true;
			
	// 	}else{
	// 		$this->db->where('trim_id', $trim_id);
	// 		$this->db->update('trim',$trim_data);
	// 		return true;
	// 	}
	// 	return false;
	// 	// var_dump($body_data);die();

		
	// }
	function save($id,$data){
		if ($id!='') {
			$this->db->where('trim_id',$id)->update('ospos_trim',$data);
		}else{
			$this->db->insert('ospos_trim',$data);
		}
	}

	/*
	Updates multiple makes at once
	*/
	function update_multiple($make_data,$make_ids)
	{
		$this->db->where_in('make_id',$make_ids);
		return $this->db->update('makes',$make_data);
	}

	/*
	Deletes one make
	*/
	function delete($make_id)
	{
		$this->db->where('make_id', $make_id);
		return $this->db->update('makes', array('deleted' => 1));
	}

	/*
	Deletes a list of makes
	*/
	function delete_list($trim_ids)
	{
		$this->db->where_in('trim_id',$trim_ids);
		return $this->db->update('trim', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find makes
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('makes');
		$this->db->like('make_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("make_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->make_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on makes
	*/
	function search($search)
	{
		$this->db->from('makes');
		$this->db->where("make_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("make_name", "asc");
		return $this->db->get();	
	}
	
	public function get_make_description( $make_name )
	{
		if ( !$this->exists( $this->get_make_id($make_name)))
			return 0;
		
		$this->db->from('makes');
		$this->db->where('make_name',$make_name);
		return $this->db->get()->row()->description;
	}
	
	function update_make_description( $make_name, $description )
	{
		$this->db->where('trim_name', $make_name);
		$this->db->update('trim', array('description' => $description));
	}
	function check_name($n,$id){
		$w = '';
		if ($id!='') {
			$w = " AND trim_id <> $id";
		}
		return $this->db->query("SELECT * FROM ospos_trim WHERE trim_name = '$n' {$w}  AND deleted=0 LIMIT 1")->row();
	}

}
?>
