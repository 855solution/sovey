<?php
class Api_location extends CI_Model
{

	public function api_item_location($json_send)
	{
		$data = json_decode($json_send);
		$data = (array)$data;
		$location_name = $data['location_name'];
		$api = $this->load->database('api', TRUE);
		$api->select('*');
		$api->from('locations');
		$api->where('location_name', $location_name);
		$api->where('deleted', 0);
		$query  = $api->get();
		$result = $query->row();
		if(count($result) == 0)
		{
			echo json_encode(array('status' => 0, 'message' => 'No location'));
		}
		else{
			$api->select('*');
			$api->from('items');
			$api->where('location_id', $result->location_id);
			$query = $api->get();
			$qty_location_item = $query->num_rows();
			if($result->max_qty > 0){

				if($qty_location_item >= $result->max_qty)
				{
					echo json_encode(array('status' => 0, 'message' => 'Quatity of location items is full'));
				}else{
					$insert = $this->api_add_item_location($result->location_id, $json_send);
					
					
				}
			}else{
					$insert = $this->api_add_item_location($result->location_id, $json_send);
	
			}
		}
	}
	public function api_add_item_location($location_id, $json_send)
	{
		$api = $this->load->database('api', TRUE);
	
		$data = json_decode($json_send);
		$data = (array)$data;
		$failed = false;
		$barcode = [];
		foreach (json_decode($data['item_number_list']) as  $value) {
			$data_insert = array(
			   'location_id' => $location_id
			);
			
			$api->where('item_id', substr($value->item_number, 6));
			$update = $api->update('items', $data_insert);
			//var_dump($api->affected_rows());
			if($api->affected_rows() == 0) {
				$failed = true;
				array_push($barcode, $value->item_number);
			}
		}
		if($failed)
			echo json_encode(array('status' => 0, 'message' => 'failed', 'barcode' => $barcode));
		else{
			if($update)
				echo json_encode(array('status' => 1, 'message' => 'Your data insert successfully'));
			else
				echo json_encode(array('status' => 0, 'message' => 'failed'));
		}
	}

	public function list_location() {
		$api = $this->load->database('api', TRUE);
		if($this->input->post('txtname') != "")
		{
			$api->select('locations.location_name,locations.max_qty, COUNT("items.location_id") AS Qty');
			$api->from('items');
			$api->join('locations', 'items.location_id = locations.location_id');
			$api->where('locations.location_name', $this->input->post('txtname'));
			$api->where('locations.deleted', 0);
			$api->group_by('items.location_id');
		}else
		{
			$api->select('locations.location_name,locations.max_qty, COUNT("items.location_id") AS Qty');
			$api->from('items');
			$api->join('locations', 'items.location_id = locations.location_id');
			$api->where('locations.deleted', 0);
			$api->group_by('items.location_id');
		}
		$data = $api->get();
		return $data->result();
	}
}
?>