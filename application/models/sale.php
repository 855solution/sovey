<?php
class Sale extends CI_Model
{

	public function update_vin_part($vinid,$khmerid){
		$this->db->where('vin_id',$vinid)->where('khmername_id',$khmerid)->update('vinnumber_part',array('status'=>0));
	}

	public function get_info($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function exists($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$success = $this->db->update('sales',$sale_data);
		
		return $success;
	}
	function get_last_sale_id(){
		return $this->db->query("select invoiceid from ospos_sales ORDER BY sale_id desc LIMIT 1")->row()->invoiceid;
	}
	

	// GENERATE INVOICE NUMBER
	function generate_sale_id(){
		$cut = substr($this->get_last_sale_id(),2);
		$yesterday = substr($this->get_last_sale_id(),0,-strlen($cut));
		// var_dump(-strlen($cut));die();
		if ($yesterday==date('d')) {
			$i=substr($this->get_last_sale_id(), 6);
			$i++;
		}else{
			$i=1;
		}

		$sale_id = date('ymd').$i;
		return $sale_id;
	}

	function generate_invoice(){
		$last_invoice = substr($this->get_last_sale_id(),11);
		if ($last_invoice) {
			$i = (Int)$last_invoice + 1;
		}else{
			$i = 1;
		}
		$sale_id = date('ymd').'-1000'.$i;
		return $sale_id ;
	}
	function get_vin_total_sold($vin_num){
		return $this->db->select("total_sold")
						->from('vinnumbers')
						->where('vinnumber_name',$vin_num)
						->get()
						->row();
	}
	function update_total_sold($vin_num,$data){
		$this->db->where('vinnumber_name',$vin_num)
		         ->update('vinnumbers',$data);
	}
	function save ($items,$customer_id,$employee_id,$comment,$payments,$sale_id=false,$changedue,$totalallpayment)
	{
		$mode = $this->sale_lib->get_mode();

		// var_dump($customer_id);die();
		if(count($items)==0)
			return -1;

		$edit_sale_id = $this->session->userdata('edit_sale_id');

		//Alain Multiple payments
		//Build payment types string
		$pa = 0;
		foreach($payments as $payment_id=>$payment)
		{
			$payment_types='';
			
			if ($payment['payment_type']=='Cash') {
				$pa += $payment['payment_amount'];
			}
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($pa).'<br/>';

		}
		$valueDue='';
		
		// $yesterday = substr($this->get_last_sale_id(),0,-5);
		// if ($yesterday==date('d')) {
		// 	$i=substr($this->get_last_sale_id(), 6);
		// 	$i++;
		// }else{
		// 	$i=1;
		// }
		if (!$edit_sale_id) {
			// $invoice_id = $this->generate_sale_id(); //OLD INVOICE GENERATE
			$invoice_id = $this->generate_invoice();
		}else{
			$invoice_id = $this->db->query("SELECT invoiceid FROM ospos_sales WHERE sale_id = '$edit_sale_id'")->row()->invoiceid;
		}

		if ($this->session->userdata('sale_date')) {
			$sale_date = date('Y-m-d H:i:s',strtotime($this->session->userdata('sale_date')));
			
		}else{
			$sale_date = date('Y-m-d H:i:s');
		}
		// var_dump($sale_id);die();
		// $first_paid = (float)filter_var($this->Sale->get_sale_by_id($sd->sale_id)->payment_type, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
		$now = date('Y-m-d H:i:s');

		$sales_data = array(
			'sale_time' => $sale_date,
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'payment_type'=>$payment_types,
			'comment'=>$comment,
			'invoiceid'=>$invoice_id,
			'first_payment'=>$pa,
			'transaction_time'=>$now
		);
		
		$update_sales_data = array(
			'sale_time' => $sale_date,
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'payment_type'=>$payment_types,
			'comment'=>$comment,
			'first_payment'=>$pa
		);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		if ($mode!='return') {
			# code...
			if ($edit_sale_id){
				if($this->db->where('sale_id',$edit_sale_id)->update('sales',$update_sales_data)){
					$sale_id = $edit_sale_id;

				}else{
					return -1;
				}
			}else{
				if($this->db->insert('sales',$sales_data)){
					$sale_id = $this->db->insert_id('sales');
				}else{
					return -1;
				};
			}
			// UPDATE REGISTER CURRENT SALE AMOUNT
			$sale_submit = $totalallpayment;
			$register_user_id = $this->session->userdata('person_id');
			$register_info = $this->getUserRegister($register_user_id);
			$old_submit = $register_info->sale_submit;
			
			$sale_submit+=$old_submit;
			
			$register_data = array('sale_submit'=>$sale_submit);

			$register_id = $register_info->register_id;
		}

		if ($mode!='return') {
			$this->db->where('register_id',$register_id)->update('register',$register_data);
		}
		// 
		
		if ($edit_sale_id) {
			// DELETE OLD DATA FOR UPDATE
			// $this->db->where('sale_id',$edit_sale_id)->delete('sales_payments');

			//UPDATE OLD ITEM QUANTITY (in case they remove old itme)
			$old_items = $this->db->where('sale_id',$edit_sale_id)->get('sales_items')->result();
			foreach ($old_items as $ot) {
				if ($ot->is_new!=1 && $ot->is_return!=1) {
					$this->db->where('item_id',$ot->item_id)->update('items',['quantity'=>1]);
				}
				if ($ot->is_new==1) {
					$this->db->where('item_id',$ot->item_id)->update('items_from_sale',['quantity'=>1]);
				}
			}

			//
			$this->db->where('sale_id',$edit_sale_id)->delete('sales_payment_due');
			$this->db->where('sale_id',$edit_sale_id)->delete('sales_items');
		}

		// $total_paid = $pa;
		// var_dump($sale_id);die();
		foreach($payments as $payment_id=>$payment)
		{
			if ( substr( $payment['payment_type'], 0, strlen( $this->lang->line('sales_giftcard') ) ) == $this->lang->line('sales_giftcard') )
			{
				/* We have a gift card and we have to deduct the used value from the total value of the card. */
				$splitpayment = explode( ':', $payment['payment_type'] );
				$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $splitpayment[1] );
				$this->Giftcard->update_giftcard_value( $splitpayment[1], $cur_giftcard_value - $payment['payment_amount'] );
			}
			// $total_paid += $payment['payment_amount'];
			
			if ($mode!='return') {
				

				// SALE PAYMENTS
				$sales_payments_data = array
				(
					'sale_id'=>$sale_id,
					'payment_type'=>$payment['payment_type'],
					'payment_amount'=>$payment['payment_amount'],
					'dept'=>$changedue,
					'date'=>date('Y-m-d H:i:s',strtotime(str_replace('/','-', $payment['date']))),
					'emp_id'=>$payment['emp_id']
					
				);

				$this->db->insert('sales_payments',$sales_payments_data);
				

				// SALES PAYMENT DUE
				$sales_payments_due = array
				(
					'sale_id'=>$sale_id,
					'payment_amount'=>$totalallpayment,
					'total_due'=>$changedue,
					'created_date'=>date('Y-m-d H:i:s'),
					'updated_date'=>date('Y-m-d H:i:s'),
					'emp_id'=>$payment['emp_id']
					
				);
				$this->db->insert('sales_payment_due',$sales_payments_due);
				if ($changedue<=0) {
					$trans_type = "COMPLETE SALE";
					if ($edit_sale_id) {
						$trans_type = "UPDATE SALE";
					}
				}else{
					$trans_type = "COMPLETE SALE (A/R)";
					if ($edit_sale_id) {
						$trans_type = "UPDATE SALE (A/R)";
						
					}

				}

				//PAYMENT TRANSACTION
				$trans_data = [
								'transaction_time'=>$now,
								'transaction_type'=>$trans_type,
								'transaction_of_id'=>$sale_id,
								'transaction_of_user_id'=>$payment['emp_id'],
								'reference_no'=>$invoice_id,
								'amount'=>$payment['payment_amount']
							];

				$this->main_model->save_payment_transaction($trans_data);
				//END PAYMENT TRANSACTION
			}
			
		}
		$old_total=0;

		// RETURN
			if ($mode=='return') {
				$sale_id = $this->sale_lib->get_return_sale_id();
				// RECORD SALE RETURN
				$return_data = array(
								// 'return_no'=>date('Ymd'),
								'sale_id'=>$sale_id,
								'date'=>date('Y-m-d H:i:s'),
								'return_by'=>$this->session->userdata('person_id')
								);
				$this->db->insert('return_sale',$return_data);
				$return_id = $this->db->insert_id();

				$return_no = date('ymd')."RT-1000".$return_id;
				$this->db->where('return_id',$return_id)->update('ospos_return_sale',['return_no'=>$return_no]);


				$datarec = array(
                    'cus_id'=>$customer_id,
                    'date'=>$now,
                    'amount_usd'=>$this->sale_lib->get_return_total()<0?$this->sale_lib->get_return_total():0,
                    'deleted'=>0,
                    'employee_id'=>$this->session->userdata('person_id'),
                    'status'=>'return'
                    );
        
    			$rec_id = $this->past_due->save_payment_record($datarec);


				foreach($payments as $payment_id=>$payment){
					$return_payment = array
					(
						'return_id'=>$return_id,
						'payment_date'=>date('Y-m-d H:i:s',strtotime(str_replace('/','-', $payment['date']))),
						'payment_amount'=>$payment['payment_amount'],
						'emp_id'=>$payment['emp_id']
					);

					$this->db->insert('return_payment',$return_payment);

					//PAYMENT TRANSACTION
					$trans_type = "RETURN SALE";

					
					$trans_data = [
									'transaction_time'=>$now,
									'transaction_type'=>$trans_type,
									'transaction_of_id'=>$return_id,
									'transaction_of_user_id'=>$payment['emp_id'],
									'reference_no'=>$return_no,
									'amount'=>$payment['payment_amount']
								];

					$this->main_model->save_payment_transaction($trans_data);
					//END PAYMENT TRANSACTION

					$recdetail = array(
                    'rec_id'=>$rec_id,
                    'sale_id'=>$sale_id,
                    'amount_usd'=>$payment['payment_amount']<0?$payment['payment_amount']:0,
                    'payment_on'=>'return'
                            );
                	$this->past_due->save_record_detail($recdetail);

				}

				// INSERT TO SALE PAYMENT SO DUE PAYMENT WILL BE CALCULATED RESTOCK
				$recdetail = array(
                    'rec_id'=>$rec_id,
                    'sale_id'=>$sale_id,
                    'amount_usd'=>$this->sale_lib->get_total_restock()*(-1),
                    'payment_status'=>'restock',
                    'payment_on'=>'return'
                            );
                $this->past_due->save_record_detail($recdetail);

                //PAYMENT TRANSACTION FRO RESTOCK
					$trans_type = "RETURN RESTOCK";

					
					$trans_data = [
									'transaction_time'=>$now,
									'transaction_type'=>$trans_type,
									'transaction_of_id'=>$return_id,
									'transaction_of_user_id'=>$this->session->userdata('person_id'),
									'reference_no'=>$return_no,
									'amount'=>$this->sale_lib->get_total_restock()*(-1)
								];

					$this->main_model->save_payment_transaction($trans_data);
					//END PAYMENT TRANSACTION

                // $this->past_due->addSalePayment($sale_id,$amount_usd);
                // $this->past_due->update_sale_status($sale_id,$status);

			}
		$return_total = 0;
		foreach($items as $line=>$item)
		{
			if ($item['is_new']==1) {
				$cur_item_info = $this->Item->get_new_item_info($item['item_id']);
			}else{
				$cur_item_info = $this->Item->get_info($item['item_id']);
			}




			if ($item['is_return']==1) {

				// UPDATE RETURN ITEM IN SALE TABLE
				$return_items_data = array
				(
					'is_return' =>$item['is_return'],
					'return_restock'=>$item['discount'],
					'return_problem'=>$item['is_pro']
				);
				$this->db->where('item_id', $item['item_id']);
				$this->db->update('sales_items', $return_items_data); 
				// UPDATE QUANTITY BACK TO 1
				$this->db->where('item_id',$item['item_id']);
				$this->db->update('ospos_items',array('quantity'=>1));

				// RETURN ITEM
				$return_item = array(
								'return_id'=>$return_id,
								'item_id'=>$item['item_id'],
								'line'=>$item['line'],
								'disc'=>$item['discount'],
								'cost' => $cur_item_info->cost_price,
								'price'=>$item['price'],
								'is_new'=>$item['is_new'],
								'problem'=>$item['is_pro'],
								'description'=>$item['description']
								);
				$this->db->insert('return_item',$return_item);
				$return_total += $item['price'];

				// UPDATE TOTAL SOLD IN VINNUMBER
				if ($item['vin_num']!='') {
					$old_total = $this->get_vin_total_sold($item['vin_num'])->total_sold;
					$old_total -= $item['price'];
					$data = array(
								'total_sold'=>$old_total
							);
					$this->update_total_sold($item['vin_num'],$data);
				}
			}else{
				// SAVE SALE ITEMS

				$sales_items_data = array
				(
					'sale_id'=>$sale_id,
					'item_id'=>$item['item_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'serialnumber'=>$item['serialnumber'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_cost_price' => $cur_item_info->cost_price,
					'item_unit_price'=>$item['price'],
					'desc'=>$item['desc'],
					'is_new'=>$item['is_new'],
					'is_return'=>0
					// 'is_pro'=>$item['is_pro']
				);

				//CUT STOCK
				// if ($item['is_new']==1) {
				// 	$this->db->where('item_id',$item['item_id'])->update('items_from_sale',['quantity'=>0]);

				// }else{
				// 	$this->db->where('item_id',$item['item_id'])->update('items',['quantity'=>0]);

				// }

				$item_exist = $this->check_item_exist($item['item_id']);
				if ($item_exist) {
					$this->db->where('item_id',$item['item_id'])->update('sales_items',$sales_items_data);
				}else{
					$this->db->insert('sales_items',$sales_items_data);

				}
				$this->update_vin_part($item['vin_id'],$item['khmername_id']);
				
			}
			
			


			

			if ($item['mode']=='sale') {
			//Insert into vinnumber_sale
				if ($item['vin_id']) {
					$i_data = array('vinnumber_id'=>$item['vin_id'],
								'item_id'=>$item['item_id'],
								'is_new'=>$item['is_new']
								);
					$this->Item->save_vin_sale($i_data);

				}
				
			// UPDATE TOTAL SOLD IN VINNUMBER
				if ($item['vin_num']!='') {
					$old_total = $this->get_vin_total_sold($item['vin_num'])->total_sold;
					$old_total += $item['price'];
					$data = array(
								'total_sold'=>$old_total
							);
					$this->update_total_sold($item['vin_num'],$data);
				}
			// UPDATE NEW ITEM BARCODE
			
				$b_data = array('barcode'=>$item['item_number']);
				if ($item['is_new']==1) {
					$this->Item->update_item_sales($b_data,$item['item_id']);
				}
				//Update stock quantity
				$item_data = array('quantity'=>$cur_item_info->quantity - $item['quantity'],
									'is_pro'=>$item['is_pro'],
									'description'=>$item['desc']);
				if ($item['is_new']!=1) {
					$this->Item->save($item_data,$item['item_id']);
				}else{
					$this->db->where('item_id',$item['item_id'])->update('items_from_sale',['quantity'=>0]);

				}

			}
			
		
			
			// }else{
			// 	$this->Item->update_item_sales($item_data,$item['item_id']);
			// }
			
			//Ramel Inventory Tracking
			//Inventory Count Details
			$qty_buy = -$item['quantity'];
			$sale_remarks = $sale_id;
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item['item_id'],
				'trans_user'=>$employee_id,
				'trans_comment'=>$sale_remarks,
				'trans_inventory'=>$qty_buy
			);
			$this->Inventory->insert($inv_data);
			//------------------------------------Ramel

			$customer = $this->Customer->get_info($customer_id);
 			if ($customer_id == -1 or $customer->taxable)
 			{
 				if ($edit_sale_id) {
 					$this->db->where('sale_id')->delete('sales_items_taxes');
 				}
				foreach($this->Item_taxes->get_info($item['item_id']) as $row)
				{
					$this->db->insert('sales_items_taxes', array(
						'sale_id' 	=>$sale_id,
						'item_id' 	=>$item['item_id'],
						'line'      =>$item['line'],
						'name'		=>$row['name'],
						'percent' 	=>$row['percent']
					));
				}
			}
		}

		// UPDATE RETURN TOTAL
		$this->db->where('return_id',$return_id)->update('return_sale',array('total'=>$return_total));

		// UPDATE SALE STATUS
		if ($mode!='return') {
			if ($pa>=$totalallpayment) {
				$this->db->where('sale_id',$sale_id)->update('ospos_sales',array('payment_status'=>'paid'));
			}
			if ($pa<$totalallpayment) {
				$this->db->where('sale_id',$sale_id)->update('ospos_sales',array('payment_status'=>'partial'));
			}

		}
		if ($mode=='return') {
			// UPDATE SALE STATUS TO RETURN
			$all_sale_items = $this->db->query("SELECT * FROM ospos_sales_items WHERE sale_id = '$sale_id' AND is_return = 0")->result();

			if (!$all_sale_items) {
				$this->db->where('sale_id',$sale_id)->update('ospos_sales',['payment_status'=>'returned']);
			}
		}
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}
		if ($mode == 'return') {
			return $return_id;
		}
		return $sale_id;
	}
	function get_invoice_id($sale_id){
		return $this->db->query("select invoiceid from ospos_sales WHERE sale_id=$sale_id")->row()->invoiceid;
	}

	function check_item_vin($item_id){
		$sql=$this->db->query("SELECT * FROM ospos_items 
									WHERE item_id=$item_id
									AND category<>''
									AND deleted=0")->row();
		if ($sql) {
			return $sql->category;
		}
		return false;
	}
	function delete($sale_id)
	{
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		$this->db->delete('sales_payments', array('sale_id' => $sale_id)); 
		$this->db->delete('sales_items_taxes', array('sale_id' => $sale_id)); 
		$this->db->delete('sales_items', array('sale_id' => $sale_id)); 
		$this->db->delete('sales', array('sale_id' => $sale_id)); 
		
		$this->db->trans_complete();
				
		return $this->db->trans_status();
	}

	function get_sale_items($sale_id)
	{
		$this->db->from('sales_items');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_customer($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		return $this->Customer->get_info($this->db->get()->row()->customer_id);
	}

	//We create a temp table that allows us to do easy report/sales queries
	public function create_sales_items_temp_table()
	{
		$this->db->query("CREATE TEMPORARY TABLE ".$this->db->dbprefix('sales_items_temp')."
		(SELECT date(sale_time) as sale_date,
		 ".$this->db->dbprefix('sales_items').".sale_id,
		 ".$this->db->dbprefix('sales_payments').".dept,
		 ".$this->db->dbprefix('sales_payments').".payment_amount,
		 comment,
		".$this->db->dbprefix('sales').".payment_type,
		 customer_id, 
		 employee_id, 
		".$this->db->dbprefix('items').".item_id,
		 supplier_id, 
		 quantity_purchased, 
		 item_cost_price, 
		 item_unit_price, 
		 SUM(percent) as item_tax_percent,
		discount_percent, 
		(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as subtotal,
		SUM(dept)as total_dues,
		ishead,cost_price,
		
		".$this->db->dbprefix('sales_items').".line as line, 
		serialnumber, 
		".$this->db->dbprefix('sales_items').".description as description,
		ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(1+(SUM(percent)/100)),2) as total,
		ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(percent)/100),2) as tax,
		(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) - (item_cost_price*quantity_purchased) as profit
		
		
		FROM ".$this->db->dbprefix('sales_items')."
		INNER JOIN ".$this->db->dbprefix('sales')." ON  ".$this->db->dbprefix('sales_items').'.sale_id='.$this->db->dbprefix('sales').'.sale_id'."
		INNER JOIN ".$this->db->dbprefix('sales_payments')." ON  ".$this->db->dbprefix('sales').'.sale_id='.$this->db->dbprefix('sales_payments').'.sale_id'."
		INNER JOIN ".$this->db->dbprefix('items')." ON  ".$this->db->dbprefix('sales_items').'.item_id='.$this->db->dbprefix('items').'.item_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('suppliers')." ON  ".$this->db->dbprefix('items').'.supplier_id='.$this->db->dbprefix('suppliers').'.person_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('sales_items_taxes')." ON  "
		.$this->db->dbprefix('sales_items').'.sale_id='.$this->db->dbprefix('sales_items_taxes').'.sale_id'." and "
		.$this->db->dbprefix('sales_items').'.item_id='.$this->db->dbprefix('sales_items_taxes').'.item_id'." and "
		.$this->db->dbprefix('sales_items').'.line='.$this->db->dbprefix('sales_items_taxes').'.line'."
		GROUP BY sale_id, item_id, line)");

		//Update null item_tax_percents to be 0 instead of null
		$this->db->where('item_tax_percent IS NULL');
		$this->db->update('sales_items_temp', array('item_tax_percent' => 0));

		//Update null tax to be 0 instead of null
		$this->db->where('tax IS NULL');
		$this->db->update('sales_items_temp', array('tax' => 0));

		//Update null subtotals to be equal to the total as these don't have tax
		$this->db->query('UPDATE '.$this->db->dbprefix('sales_items_temp'). ' SET total=subtotal WHERE total IS NULL');
	}
	
	public function get_giftcard_value( $giftcardNumber )
	{
		if ( !$this->Giftcard->exists( $this->Giftcard->get_giftcard_id($giftcardNumber)))
			return 0;
		
		$this->db->from('giftcards');
		$this->db->where('giftcard_number',$giftcardNumber);
		return $this->db->get()->row()->value;
	}
	
	public function getPaymenttype($saleid){
		
		$this->db->select('payment_type');
		$this->db->from('sales');
		$this->db->where('sale_id',$saleid);
		return $this->db->get();
	}
	public function delete_new_item($item_id){
		$this->db->where('item_id',$item_id);
		$this->db->delete('items_from_sale');
	}
	
	public function get_all_return_items(){
		return $this->db->query("SELECT *,v.partplacement_id AS pname FROM ospos_v_select_all_items AS v 
									INNER JOIN ospos_sales_items s ON v.item_id=s.item_id AND v.is_new=s.is_new
									INNER JOIN ospos_sales sa ON s.sale_id=sa.sale_id
									LEFT JOIN ospos_makes ma ON v.make_id=ma.make_id
									LEFT JOIN ospos_models mo ON v.model_id=mo.model_id
									LEFT JOIN ospos_khmernames kh ON v.khmername_id=kh.khmername_id
									LEFT JOIN ospos_partplacements p ON v.partplacement_id=p.partplacement_id
									LEFT JOIN ospos_colors c ON v.color_id=c.color_id
									WHERE s.is_return=1 AND v.deleted=0");
	}
	function search_return_items($search){
		return $this->db->query("SELECT *,v.partplacement_id AS pname FROM ospos_v_select_all_items AS v 
									INNER JOIN ospos_sales_items s ON v.item_id=s.item_id AND v.is_new=s.is_new
									INNER JOIN ospos_sales sa ON s.sale_id=sa.sale_id
									LEFT JOIN ospos_makes ma ON v.make_id=ma.make_id
									LEFT JOIN ospos_models mo ON v.model_id=mo.model_id
									LEFT JOIN ospos_khmernames kh ON v.khmername_id=kh.khmername_id
									LEFT JOIN ospos_partplacements p ON v.partplacement_id=p.partplacement_id
									LEFT JOIN ospos_colors c ON v.color_id=c.color_id
									WHERE s.is_return=1 AND v.deleted=0 AND
									(v.barcode LIKE '%$search%' OR
									 v.name LIKE '%$search%' OR
									 v.year LIKE '%$search%' OR
									 sa.invoiceid LIKE '%$search%' OR
									 kh.khmername_name LIKE '%$search%' OR
									 ma.make_name LIKE '%$search%' OR
									 mo.model_name LIKE '%$search%' OR
									 c.color_name LIKE '%$search%' OR 
									 s.item_unit_price LIKE '%$search%' OR
									 s.quantity_purchased LIKE '%$search%' OR
									 p.partplacement_name LIKE '%$search%' OR
									 v.partplacement_id LIKE '%$search%')");
	}
	function get_edit_sale_items($sale_id){
		return $this->db->query("SELECT * FROM ospos_sales_items si
									INNER JOIN ospos_v_select_all_items v ON si.item_id=v.item_id
																			AND si.is_new=v.is_new
								WHERE si.sale_id = '$sale_id' AND si.is_return<>1 ORDER BY si.line ASC");
	}
	function getLastSaleID(){
		return $this->db->query("SELECT MAX(sale_id) as sale_id from ospos_sales")->row()->sale_id;
	}
	function get_return_info($return_id){
		$return = $this->db->query("SELECT * FROM ospos_return_sale WHERE return_id=$return_id")->row();
		return $return;
	}
	function get_sale_info($sale_id){
		return $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = $sale_id")->row();
	}
	function get_return_item($return_id){
		return $this->db->query("SELECT *,r.description as description FROM ospos_return_item  r 
									INNER JOIN ospos_v_select_all_items i ON r.item_id = i.item_id AND r.is_new=i.is_new
									LEFT JOIN ospos_khmernames k ON i.khmername_id = k.khmername_id
									LEFT JOIN ospos_makes m ON i.make_id = m.make_id
									LEFT JOIN ospos_models mo ON i.model_id=mo.model_id
									LEFT JOIN ospos_partplacements p ON i.partplacement_id=p.partplacement_id
									WHERE return_id = $return_id")->result();
	}
	function getUserRegister($user_id){
		return $this->db->query("SELECT * FROM ospos_register WHERE open_by=$user_id AND status='open'")->row();
	}
	function do_open_register($data){
		$this->db->insert('register',$data);

		$reg_id = $this->db->insert_id();
		$reg_ref = date('ymd')."OPR-1000".$reg_id;
		$this->db->where('register_id',$reg_id)->update('ospos_register',['reference_no'=>$reg_ref]);

		$reg_data['reg_id'] = $reg_id;
		$reg_data['reg_ref'] = $reg_ref;

		return $reg_data;
	}
	function do_close_register($reg_id,$user_id,$data){
		$this->db->where('register_id',$reg_id)
				->where('open_by',$user_id)
				->where('status','open')
				->update('register',$data);

				return true;
	}
	function getSaleDue($fd,$td){
		$eid = $this->session->userdata('person_id');
		$sql = $this->db->query("SELECT * FROM ospos_sales s
								INNER JOIN ospos_sales_payment_due d ON s.sale_id=d.sale_id
								WHERE s.transaction_time BETWEEN '$fd' AND '$td' AND s.employee_id='$eid'
								GROUP BY s.sale_id")->result();
		$total_due = 0;
		foreach ($sql as $s) {
			if ($s->total_due>0) {
				$total_due+=$s->total_due;
			}
		}
		return $total_due;
	}
	function getTotalDeposit($fd,$td){
		$eid = $this->session->userdata('person_id');
		$sql = $this->db->query("SELECT
										SUM(p.payment_amount) as total_deposit
									FROM
										ospos_sales_suspended s
									INNER JOIN ospos_sales_suspended_payments p ON s.sale_id=p.sale_id
									WHERE 1=1
									AND	(sale_time BETWEEN '$fd' AND '$td') 
									AND	(`date` BETWEEN '$fd' AND '$td') 
									AND (s.status IS NULL OR s.status ='') AND p.emp_id = $eid")->row();
		return $sql->total_deposit;

	}
	function getDepositDue($fd,$td){
		$total_deposit = $this->getTotalDeposit($fd,$td);
		$deposit_due=0;
		$sql = $this->db->query("SELECT
									SUM(payment_amount) as total_paid
								FROM
									ospos_sales_suspended_payments
								WHERE
									date BETWEEN '$fd'
								AND '$td'")->row();
		$deposit_due = $total_deposit-$sql->total_paid;

		return $deposit_due;
	}
	function getAllSale($fd,$td){
		$eid = $this->session->userdata('person_id');
		return $this->db->query("SELECT *,s.payment_type AS paid FROM ospos_sales s
								INNER JOIN ospos_people p ON s.customer_id=p.person_id
								WHERE
									s.sale_time BETWEEN '$fd'
								AND '$td' AND employee_id='$eid'")->result();
	}
	function getSaleItem($sale_id){
		return $this->db->query("SELECT 
										i.barcode,
										i.item_id,
										i.category,
										i.`year`,
										m.make_name,
										mo.model_name,
										i.khmer_name,
										i.`name`,
										s.description,
										s.`desc`,
										s.quantity_purchased,
										i.partplacement_id as ppl_id,
										p.partplacement_name,
										s.item_unit_price,
										s.is_return,
										s.is_new,
										s.return_restock
									FROM ospos_sales_items s
									INNER JOIN ospos_v_select_all_items i ON s.item_id = i.item_id AND s.is_new = i.is_new
									LEFT JOIN ospos_makes m ON i.make_id = m.make_id
									LEFT JOIN ospos_models mo ON i.model_id = mo.model_id
									LEFT JOIN ospos_partplacements p ON i.partplacement_id = p.partplacement_id
								WHERE s.sale_id=$sale_id AND (i.category='' OR i.category IS NULL) AND is_return <> 1")->result();
	}
	function getSalePayment($sale_id){
		// $eid = $this->session->userdata('person_id');
		return $this->db->query("SELECT * FROM ospos_sales_payment_due WHERE sale_id = $sale_id")->row();
	}

	function getAllVin(){
		return $this->db->query("SELECT * FROM ospos_vinnumbers WHERE deleted=0")->result();
	}
	function update_vin_sale($vin_id,$item_id,$is_new,$price){
		$vin_num = $this->getVinNum($vin_id)->vinnumber_name;
		//Insert into vinnumber_sale
		$i_data = array('vinnumber_id'=>$vin_id,
					'item_id'=>$item_id,
					'is_new'=>$is_new
					);
		$this->Item->save_vin_sale($i_data);

		// UPDATE TOTAL SOLD IN VINNUMBER
		$old_total = $this->get_vin_total_sold($vin_num)->total_sold;
		$old_total += $price;
		$data = array(
					'total_sold'=>$old_total
				);
		$this->update_total_sold($vin_num,$data);
			
	}
	function getVinNum($vin_id){
		return $this->db->query("SELECT * FROM ospos_vinnumbers WHERE vinnumber_id = $vin_id")->row();
	}
	function getRegisterInfo($reg_id){
		return $this->db->where('register_id',$reg_id)->get('register')->row();
	}
	function get_list_sale($user,$cus,$period,$fd,$td,$fm,$tm,$fy,$ty){
		$where='';
		$gr_by = '';
		if ($fd && $td) {
			$where.=" AND (DATE_FORMAT(s.sale_time,'%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($fd))."' AND '".date('Y-m-d',strtotime($td))."')";
		}
		if ($fm && $tm) {
			$where.=" AND (DATE_FORMAT(s.sale_time,'%m-%Y') BETWEEN '".$fm."' AND '".$tm."')";
		}
		if ($fy && $ty) {
			$where.=" AND (DATE_FORMAT(s.sale_time,'%Y') BETWEEN '".$fy."' AND '".$ty."')";
		}
		if ($cus) {
			$where.=" AND s.customer_id = '$cus'";
		}
		if ($user) {
			$where.=" AND s.employee_id = '$user'";
			
		}	
		if ($period=='Daily') {
			$gr_by = "GROUP BY DATE_FORMAT(s.sale_time,'%Y-%m-%d')";
		}
		if ($period=='Monthly') {
			$gr_by = "GROUP BY DATE_FORMAT(s.sale_time,'%Y-%m')";
		}
		if ($period=='Yearly') {
			$gr_by = "GROUP BY DATE_FORMAT(s.sale_time,'%Y')";
		}


		$sale = $this->db->query("SELECT s.sale_time,pe.title AS emp_title,pc.title AS cus_title,
										CONCAT(pe.last_name,' ',pe.first_name) AS emp_name,
										CONCAT(pc.last_name,' ',pc.first_name) AS cus_name,
										SUM(sp.payment_amount) AS sum_amount_to_pay,
										SUM(s.first_payment) AS sum_first_paid,
										SUM(rd.amount_usd) AS sum_paid,
										SUM(sp.total_due) AS sum_due
								FROM ospos_sales s
								INNER JOIN ospos_people pe ON s.employee_id = pe.person_id
								INNER JOIN ospos_people pc ON s.customer_id = pc.person_id
								INNER JOIN ospos_sales_payment_due sp ON s.sale_id = sp.sale_id
								LEFT JOIN ospos_payment_record_detail rd ON s.sale_id = rd.sale_id
								WHERE 1=1 {$where}
								{$gr_by}
								ORDER BY s.sale_time DESC")->result();

		return $sale;
	}
	function get_list_sale_detail($user,$cus,$fd,$td){
		$where='';
		if ($fd && $td) {
			$where.=" AND (DATE_FORMAT(s.sale_time,'%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($fd))."' AND '".date('Y-m-d',strtotime($td))."')";
		}
		// if ($fm && $tm) {
		// 	$where.=" AND (DATE_FORMAT(sale_time,'%Y-%m') BETWEEN '$fm' AND '$tm')";
		// }
		// if ($fy && $ty) {
		// 	$where.=" AND (DATE_FORMAT(sale_time,'%Y') BETWEEN '$fy' AND '$ty')";
		// }
		if ($cus) {
			$where.=" AND s.customer_id = '$cus'";
		}
		if ($user) {
			$where.=" AND s.employee_id = '$user'";
			
		}	
		

		$sale = $this->db->query("SELECT s.sale_time,pe.title AS emp_title,pc.title AS cus_title,s.sale_id,s.invoiceid,
										CONCAT(pe.last_name,' ',pe.first_name) AS emp_name,
										CONCAT(pc.last_name,' ',pc.first_name) AS cus_name,
										s.first_payment AS first_paid,
										s.payment_status
								FROM ospos_sales s
								INNER JOIN ospos_people pe ON s.employee_id = pe.person_id
								INNER JOIN ospos_people pc ON s.customer_id = pc.person_id
								WHERE 1=1 {$where} ORDER BY s.sale_time DESC")->result();

		return $sale;
	}
	function get_sale_due_paid($sale_id){
		$paid = $this->db->query("SELECT SUM(amount_usd) as paid FROM ospos_payment_record_detail WHERE sale_id='$sale_id'")->row()->paid;

		return $paid;
	}
	function get_sale_people($person_id){
		$peop = '';
		if ($person_id) {
			$peop = $this->db->query("SELECT * FROM ospos_people WHERE person_id = $person_id")->row();
		}
		return $peop;
	}
	function get_sale_by_id($sale_id){
		return $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = $sale_id")->row();
	}
	function get_grand_total($sale_id,$sum,$gr_by){
		$where = " AND sale_id='$sale_id'";
		if ($sum) {
			$where = '';
		}
		return $this->db->query("SELECT *{$sum} FROM ospos_sales_payment_due WHERE 1=1 {$where} {$gr_by}")->row();
	}
	function check_item_exist($item_id){
		return $this->db->query("SELECT * FROM ospos_sales_items WHERE item_id = $item_id")->row();
	}
	function get_return_payment($return_id){
		return $this->db->query("SELECT SUM(payment_amount) as return_paid FROM ospos_return_payment WHERE return_id='$return_id'")->row()->return_paid;
	}
	function getEmpReturnPayment($df,$dt){
		$emp_id = $this->session->userdata('person_id');
		return $this->db->query("SELECT SUM(payment_amount) as return_paid FROM ospos_return_payment
								 WHERE emp_id='$emp_id' AND payment_date BETWEEN '$df' AND '$dt'
								")->row()->return_paid;
	}
	function getEmpDuePayment($df,$dt){
		$emp_id = $this->session->userdata('person_id');
		return $this->db->query("SELECT SUM(amount_usd) as dpay FROM ospos_payment_record
								WHERE employee_id = '$emp_id' AND `date` BETWEEN '$df' AND '$dt'")->row()->dpay;
	}

	function get_sale_due($sale_id){
		return $this->db->query("SELECT * FROM ospos_sales_payment_due WHERE sale_id = $sale_id ORDER BY due_id DESC LIMIT 1")->row();
	}

	function get_filter_return($emp_id,$fd,$td){

		$where = '';
		if ($emp_id) {
			$where.=" AND r.return_by = '$emp_id'";
		}
		if ($fd && $td) {
			$where.=" AND DATE_FORMAT(r.date,'%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($fd))."' AND '".date('Y-m-d',strtotime($td))."'"; 
		}

		$query = $this->db->query("SELECT
										r.date as return_date,
										r.sale_id,
										r.return_id,
										r.return_by
									FROM
										ospos_return_sale r
									WHERE 1=1 {$where} GROUP BY DATE_FORMAT(r.date, '%Y-%m-%d') ORDER BY r.date DESC")->result();

		return $query;



	}
	function get_return_sale($return_date){
		$where='';

		$where.=" AND DATE_FORMAT(r.date,'%Y-%m-%d') = '".date('Y-m-d',strtotime($return_date))."'"; 

		$sales = $this->db->query("SELECT 
											r.sale_id,
											s.invoiceid,
											r.return_id
										FROM ospos_return_sale r
										INNER JOIN ospos_sales s ON r.sale_id=s.sale_id
										WHERE 1=1 {$where}
										GROUP BY r.sale_id ORDER BY r.date DESC")->result();
		
		return $sales;
	}

	function get_return_items($sale_id){

		$sale_item = $this->db->query("SELECT 
											i.barcode,
											i.item_id,
											i.category,
											i.`year`,
											m.make_name,
											mo.model_name,
											i.khmer_name,
											i.`name`,
											s.description,
											s.`desc`,
											s.quantity_purchased,
											i.partplacement_id as ppl_id,
											p.partplacement_name,
											s.item_unit_price,
											s.is_return,
											s.is_new,
											s.return_restock
										FROM ospos_sales_items s
										INNER JOIN ospos_v_select_all_items i ON s.item_id = i.item_id AND s.is_new = i.is_new
										LEFT JOIN ospos_makes m ON i.make_id = m.make_id
										LEFT JOIN ospos_models mo ON i.model_id = mo.model_id
										LEFT JOIN ospos_partplacements p ON i.partplacement_id = p.partplacement_id 
										WHERE sale_id = '$sale_id'")->result();

		return $sale_item;
	}

	function get_sale_return_items($return_id){

		$sale_item = $this->db->query("SELECT 
											i.barcode,
											i.item_id,
											i.category,
											i.`year`,
											m.make_name,
											mo.model_name,
											i.khmer_name,
											i.`name`,
											s.description,
											s.`desc`,
											s.quantity_purchased,
											i.partplacement_id as ppl_id,
											p.partplacement_name,
											s.item_unit_price,
											s.is_return,
											s.is_new,
											s.return_restock
										FROM ospos_sales_items s
										INNER JOIN ospos_v_select_all_items i ON s.item_id = i.item_id AND s.is_new = i.is_new
										LEFT JOIN ospos_makes m ON i.make_id = m.make_id
										LEFT JOIN ospos_models mo ON i.model_id = mo.model_id
										LEFT JOIN ospos_partplacements p ON i.partplacement_id = p.partplacement_id 
										LEFT JOIN ospos_return_item r ON r.item_id = s.item_id
										WHERE r.return_id = '$return_id' AND s.is_return = 1")->result();

		return $sale_item;
	}

	public function count_return_items($return_id)
	{
		$sql = $this->db->query("SELECT COUNT(*) as count FROM ospos_return_item WHERE return_id = '$return_id'")->row()->count;

		return $sql;
	}


	function get_last_sale(){
		$sale = $this->db->query("SELECT sale_id,invoiceid,sale_time FROM ospos_sales
ORDER BY sale_id DESC LIMIT 1")->row();

		return $sale;
	}

	function get_today_sale(){
		$sale = $this->db->query("SELECT * FROM ospos_sales WHERE DATE_FORMAT(transaction_time,'%Y-%m-%d') = '".date('Y-m-d')."'")->result();

		return $sale;
	}

	function get_sale_date($sale_id){
		$sql = $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = '$sale_id'")->row();

		$sale_date = date('d-m-Y',strtotime($sql->sale_time));
		return $sale_date;
	}

	function CR_sale_total($from_date,$to_date){
		$where = '';
		if ($from_date && $to_date) {
			$where .= "AND transaction_time BETWEEN '$from_date' AND '$to_date'";
		}


		$sql = $this->db->query("SELECT 
									SUM(amount) as total 
								FROM ospos_payment_transaction
								WHERE (
										transaction_type = 'COMPLETE SALE' 
										OR transaction_type = 'COMPLETE SALE (A/R)' 
										OR transaction_type = 'UPDATE SALE'
										OR transaction_type = 'UPDATE SALE (A/R)'
										)
									{$where}
								");
		return $sql->row()->total;
	}

	function CR_deposit_total($from_date,$to_date){
		$where = '';
		if ($from_date && $to_date) {
			$where .= "AND transaction_time BETWEEN '$from_date' AND '$to_date'";
		}
		$sql = $this->db->query("SELECT 
									SUM(amount) as total 
								FROM ospos_payment_transaction
								WHERE (
									transaction_type = 'DEPOSIT'
									OR transaction_type = 'UPDATE DEPOSIT'
									OR transaction_type = 'CANCEL DEPOSIT'
									)
									{$where}
								");
		return $sql->row()->total;
	}

	function CR_return_total($from_date,$to_date){
		$where = '';
		if ($from_date && $to_date) {
			$where .= "AND transaction_time BETWEEN '$from_date' AND '$to_date'";
		}
		$sql = $this->db->query("SELECT 
									SUM(amount) as total 
								FROM ospos_payment_transaction
								WHERE transaction_type = 'RETURN SALE'
									{$where}
								");
		return $sql->row()->total;
	}
	function CR_due_payment_total($from_date,$to_date){
		$where = '';
		if ($from_date && $to_date) {
			$where .= "AND transaction_time BETWEEN '$from_date' AND '$to_date'";
		}
		$sql = $this->db->query("SELECT 
									SUM(amount) as total 
								FROM ospos_payment_transaction
								WHERE transaction_type = 'DUE PAYMENT'
									{$where}
								");
		return $sql->row()->total;
	}

	// function CR_cancel_deposit_total($from_date,$to_date){
	// 	$where = '';
	// 	if ($from_date && $to_date) {
	// 		$where .= "AND transaction_time BETWEEN '$from_date' AND '$to_date'";
	// 	}
	// 	$sql = $this->db->query("SELECT 
	// 								SUM(amount) as total 
	// 							FROM ospos_payment_transaction
	// 							WHERE transaction_type = 'CANCEL DEPOSIT'
	// 								{$where}
	// 							");
	// 	return $sql->row()->total;
	// }
	public function get_old_return_payment($sale_id)
	{
		$sql = $this->db->query("SELECT SUM(amount_usd) as total FROM ospos_payment_record_detail WHERE sale_id = '$sale_id'")->row()->total;
		return $sql;
	}
	


}
?>
