<?php
class Main_model extends CI_Model
{
	function save_payment_transaction($data){
		$this->db->insert('ospos_payment_transaction',$data);
		return $this->db->insert_id();
	}

	function get_emp_transaction($where='',$group_by=''){
		// $opr_date = $this->db->query("SELECT * FROM ospos_register WHERE open_by=$emp_id AND status='open'")->row()->open_time;

		$trans = $this->db->query("SELECT
									CONCAT(p.last_name,' ',p.first_name) AS emp_name,
									t.transaction_time,
									t.transaction_type,
									t.reference_no,
									SUM(t.amount) as amount,
									t.transaction_of_id
								FROM
									ospos_payment_transaction t
								INNER JOIN ospos_people p ON t.transaction_of_user_id = p.person_id
								WHERE
									1=1 
								{$where}
								{$group_by}
								ORDER BY t.transaction_time DESC")->result();

		return $trans;
	}

	function get_emp_transaction_detail($where,$group_by){
		$trans = $this->db->query("SELECT
									CONCAT(p.last_name,' ',p.first_name) AS emp_name,
									t.transaction_time,
									t.transaction_type,
									t.reference_no,
									t.amount as amount,
									t.transaction_of_id
								FROM
									ospos_payment_transaction t
								INNER JOIN ospos_people p ON t.transaction_of_user_id = p.person_id
								WHERE
									1=1 
								{$where}
								ORDER BY t.transaction_time DESC")->result();

		return $trans;
	}

	function get_all_transaction_type(){
		$sql = $this->db->query("SELECT transaction_type as type_name FROM ospos_payment_transaction GROUP BY transaction_type ORDER BY transaction_type")->result();

		return $sql;
	}
}