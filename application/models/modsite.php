<?php
class Modsite extends CI_Model {
	function getdefimg($stockid){
		return $this->green->getValue("SELECT imagename FROM ospos_image_items where item_id='$stockid' ORDER BY first_image ASC limit 1");
    }
    function getmake(){
    	return $this->db->query("SELECT * FROM ospos_makes WHERE deleted=0")->result();
    }
    function getmodel($make_id){
    	return $this->db->query("SELECT * FROM ospos_models where make_id=$make_id AND deleted=0 order by model_name asc")->result();
    }
    function getpro_p(){
    	return $this->db->query("SELECT * FROM ospos_items where is_pro=1 AND deleted=0")->result();
    }
    function getarrimage($item_id){
        $data=$this->db->query("SELECT * FROM ospos_image_items WHERE item_id='$item_id' ORDER BY first_image ASC")->result();
        $arra=array();
        $arrid=array();
        foreach ($data as $a) {
           $arra[]=$a->imagename;
           $arrid[]=$a->image_id;
        }
        $arr['id']=$arrid;
        $arr['img']=$arra;
        return $arr;
    }
    function getslide($type){
        return $this->db->query("SELECT slide_id FROM ospos_slide WHERE type='".$type."' ORDER BY orders ASC")->result();
    }
    function getpage(){
        return $this->db->query("SELECT * FROM ospos_page where is_active=1 ORDER BY orders ")->result();
    }
    function getnumsearch($field,$name,$make_id,$model_id,$year,$color_id,$is_new,$is_feat,$is_pro){
        $where='';
        if($make_id!=''){
            $where.=" AND i.make_id='".$make_id."'";
        }
        if($model_id!=''){
            $where.=" AND i.model_id='".$model_id."'";
        }
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
                    $where_year.="i.year='".$arryear[$i]."'";
                    if($i<count($arryear)-1)
                        $where_year.=' OR ';
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where.=$where_year;

        }
        if($color_id!='' && $color_id!='_'){
            $arrcol=explode('_', $color_id);
            $where_color=' AND(';
            for ($i=0; $i < count($arrcol) ; $i++) { 
                    $where_color.="i.color_id='".$arrcol[$i]."'";
                    if($i<count($arrcol)-1)
                        $where_color.=' OR ';
                
            }
            $where_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($is_new!=''){
            $where.=" AND i.is_new='1'";
        }
        if($is_feat!=''){
            $where.=" AND i.is_feat='1'";
        }
        if($is_pro!=''){
            $where.=" AND i.is_pro='1'";
        }
        return $this->db->query("SELECT count(i.$field) as count
             FROM ospos_items i 
             LEFT JOIN ospos_makes m 
             ON(i.make_id=m.make_id)
             LEFT JOIN ospos_models mo 
             ON(i.model_id=mo.model_id)
             LEFT JOIN ospos_colors c 
             ON(i.color_id=c.color_id)
             where i.deleted='0'  and name LIKE '%$name%' {$where} ")->row()->count;
    }
    
}