<?php
class Locations_not extends CI_Model
{
	/*
	Determines if a given location_id is an location
	*/
	function exists( $location_id )
	{
		$this->db->from('locations');
		$this->db->where('location_id',$location_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the locations
	*/
	function get_all($limit=10000, $offset=0)
	{
		// $this->db->from('locationsk');
		// $this->db->where('deleted',0);
		// $this->db->order_by("location_name", "asc");
		// $this->db->limit($limit);
		// $this->db->offset($offset);
		$off=" LIMIT $limit";
		if($offset>0)
			$off=" LIMIT $offset, $limit";
		$data=$this->db->query("SELECT l.location_id,l.location_name,item.count ,l.description,l.max_qty
								FROM ospos_locations l
								LEFT JOIN 
									(SELECT count(location_id) as count,location_id FROM ospos_items WHERE deleted=0 group by location_id) as item
								ON(l.location_id=item.location_id)
								WHERE (item.count < l.max_qty OR ISNULL(count))
								AND l.deleted=0
								ORDER BY count asc , l.location_id desc
								{$off}");
		return $data;
	}
	
	function count_all()
	{
		$data=$this->db->query("SELECT count(*) as count
								FROM ospos_locations l
								LEFT JOIN 
									(SELECT count(location_id) as count,location_id FROM ospos_items WHERE deleted=0 group by location_id) as item
								ON(l.location_id=item.location_id)
								WHERE (item.count < l.max_qty OR ISNULL(count)) AND l.deleted=0 ")->row()->count;
		return $data;
	}/*
	function get_items($lid)
	{
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('location_id',$lid);
		$this->db->order_by("item_id", "desc");

		return $this->db->get();
	}*/
function get_items($locid){


	
	$sql="
	SELECT 
		ospos_items.item_id, ospos_items.name,ospos_items.fit,ospos_items.item_number,ospos_items.description,ospos_models.model_name,ospos_branchs.branch_name,ospos_partplacements.partplacement_name,ospos_image_items.imagename,
		MATCH(name,year)AGAINST('$searchfield')as yearrank,
		MATCH(model_name,model_description)AGAINST ('$searchfield') as modelrank,
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield') as makerank
		FROM
		ospos_items
		LEFT JOIN 
		ospos_models ON ospos_items.model_id=ospos_models.model_id
		LEFT JOIN
		ospos_makes ON ospos_makes.make_id=ospos_items.make_id
		LEFT JOIN
		ospos_branchs ON ospos_items.branch_id=ospos_branchs.branch_id
		LEFT JOIN
		ospos_partplacements ON ospos_items.partplacement_id=ospos_partplacements.partplacement_id
                LEFT JOIN
		ospos_image_items ON ospos_items.item_id=ospos_image_items.item_id

		
		WHERE
		ospos_items.deleted=0 and ishead=0
AND 
location_id=$locid		
";
                
		$query = $this->db->query($sql);
			return $query;
		
		
}	
	/*
	Gets information about a particular location
	*/
	function get_info($location_id)
	{
		$this->db->from('locations');
		$this->db->where('location_id',$location_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $location_id is NOT an location
			$location_obj=new stdClass();

			//Get all the fields from locations table
			$fields = $this->db->list_fields('locations');

			foreach ($fields as $field)
			{
				$location_obj->$field='';
			}

			return $location_obj;
		}
	}

	/*
	Get an location id given an location number
	*/
	function get_location_id($location_name)
	{
		$this->db->from('locations');
		$this->db->where('location_name',$location_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->location_id;
		}

		return false;
	}

	/*
	Gets information about multiple locations
	*/
	function get_multiple_info($location_ids)
	{
		$this->db->from('locations');
		$this->db->where_in('location_id',$location_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("location_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a location
	*/
	function save(&$location_data,$location_id=false)
	{
		if (!$location_id or !$this->exists($location_id))
		{
			if($this->db->insert('locations',$location_data))
			{
				$location_data['location_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('location_id', $location_id);
		return $this->db->update('locations',$location_data);
	}

	/*
	Updates multiple locations at once
	*/
	function update_multiple($location_data,$location_ids)
	{
		$this->db->where_in('location_id',$location_ids);
		return $this->db->update('locations',$location_data);
	}

	/*
	Deletes one location
	*/
	function delete($location_id)
	{
		$this->db->where('location_id', $location_id);
		return $this->db->update('locations', array('deleted' => 1));
	}

	/*
	Deletes a list of locations
	*/
	function delete_list($location_ids)
	{
		$this->db->where_in('location_id',$location_ids);
		return $this->db->update('locations', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find locations
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('locations');
		$this->db->like('location_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("location_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->location_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on locations
	*/
	function search($search)
	{
		$data=$this->db->query("SELECT l.location_id,l.location_name,item.count ,l.description,l.max_qty
								FROM ospos_locations l
								LEFT JOIN 
									(SELECT count(location_id) as count,location_id FROM ospos_items WHERE deleted=0 group by location_id) as item
								ON(l.location_id=item.location_id)
								WHERE (item.count < l.max_qty OR ISNULL(count))
								AND l.deleted=0
								AND l.location_name LIKE '%".$this->db->escape_like_str($search)."%'
								ORDER BY count asc , l.location_id desc");
		return $data;

	}
	
	public function get_location_description( $location_name )
	{
		if ( !$this->exists( $this->get_location_id($location_name)))
			return 0;
		
		$this->db->from('locations');
		$this->db->where('location_name',$location_name);
		return $this->db->get()->row()->description;
	}
	
	function update_location_description( $location_name, $description )
	{
		$this->db->where('location_name', $location_name);
		$this->db->update('locations', array('description' => $description));
	}
}
?>
