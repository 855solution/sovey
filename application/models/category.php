<?php
class Category extends CI_Model
{
	/*
	Determines if a given category_id is an category
	*/
	function exists( $category_id )
	{
		$this->db->from('categorys');
		$this->db->where('category_id',$category_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the categorys
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('categorys');
		$this->db->where('deleted',0);
		$this->db->order_by("category_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('categorys');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular category
	*/
	function get_info($category_id)
	{
		$this->db->from('categorys');
		$this->db->where('category_id',$category_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $category_id is NOT an category
			$category_obj=new stdClass();

			//Get all the fields from categorys table
			$fields = $this->db->list_fields('categorys');

			foreach ($fields as $field)
			{
				$category_obj->$field='';
			}

			return $category_obj;
		}
	}

	/*
	Get an category id given an category number
	*/
	function get_category_id($category_name)
	{
		$this->db->from('categorys');
		$this->db->where('category_name',$category_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->category_id;
		}

		return false;
	}

	/*
	Gets information about multiple categorys
	*/
	function get_multiple_info($category_ids)
	{
		$this->db->from('categorys');
		$this->db->where_in('category_id',$category_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("category_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a category
	*/
	function save(&$category_data,$category_id=false)
	{
		if (!$category_id or !$this->exists($category_id))
		{
			if($this->db->insert('categorys',$category_data))
			{
				$category_data['category_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('category_id', $category_id);
		return $this->db->update('categorys',$category_data);
	}

	/*
	Updates multiple categorys at once
	*/
	function update_multiple($category_data,$category_ids)
	{
		$this->db->where_in('category_id',$category_ids);
		return $this->db->update('categorys',$category_data);
	}

	/*
	Deletes one category
	*/
	function delete($category_id)
	{
		$this->db->where('category_id', $category_id);
		return $this->db->update('categorys', array('deleted' => 1));
	}

	/*
	Deletes a list of categorys
	*/
	function delete_list($category_ids)
	{
		$this->db->where_in('category_id',$category_ids);
		return $this->db->update('categorys', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find categorys
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('categorys');
		$this->db->like('category_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("category_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->category_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on categorys
	*/
	function search($search)
	{
		$this->db->from('categorys');
		$this->db->where("category_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("category_name", "asc");
		return $this->db->get();	
	}
	
	public function get_category_description( $category_name )
	{
		if ( !$this->exists( $this->get_category_id($category_name)))
			return 0;
		
		$this->db->from('categorys');
		$this->db->where('category_name',$category_name);
		return $this->db->get()->row()->description;
	}
	
	function update_category_description( $category_name, $description )
	{
		$this->db->where('category_name', $category_name);
		$this->db->update('categorys', array('description' => $description));
	}

}
?>
