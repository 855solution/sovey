<?php
class Sale_suspended extends CI_Model
{
	function get_all()
	{
		$this->db->from('sales_suspended s');
		$this->db->join('people p','s.customer_id=p.person_id','left');
		$this->db->where('status IS NULL');
		$this->db->order_by('sale_id','DESC');
		$this->db->limit(20);
		return $this->db->get();
	}
	
	public function get_info($sale_id)
	{
		$this->db->from('sales_suspended');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function exists($sale_id)
	{
		$this->db->from('sales_suspended');
		$this->db->where('sale_id',$sale_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$success = $this->db->update('sales_suspended',$sale_data);
		
		return $success;
	}
	
	function save ($items,$customer_id,$employee_id,$comment,$payments,$sale_id=false)
	{

		$now = date("Y-m-d H:i:s");
		if(count($items)==0)
			return -1;
		$d_id = $this->session->userdata('d_id');
		// if ($mode=='sale') {
		// 	if ($items[$line]['is_new']==1) {
		// 		$this->CI->Sale->delete_new_item($items[$line]['item_id']);
		// 		$this->CI->Sale->delete_qt_item($q_id,$items[$line]['item_id']);
		// 	}
		// }
		//Alain Multiple payments
		//Build payment types string
		
		$payment_types='';
		foreach($payments as $payment_id=>$payment)
		{
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
		}

		$sales_data = array(
			'sale_time' => date('Y-m-d H:i:s'),
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'payment_type'=>$payment_types,
			'comment'=>$comment
		);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		if ($d_id) {
				
				$this->db->where('sale_id',$d_id)->update('sales_suspended',$sales_data);
				$this->db->where('sale_id',$d_id)->delete('sales_suspended_items');
				// $this->db->where('sale_id',$d_id)->delete('sales_suspended_payments');

				$sale_id = $d_id;
		}else{
			$this->db->insert('sales_suspended',$sales_data);
			$sale_id = $this->db->insert_id();
		}

		$deposit_no = date('ymd')."DP-1000".$sale_id;

		$this->db->where('sale_id',$sale_id)->update('sales_suspended',['deposit_id'=>$deposit_no]);
		

		foreach($payments as $payment_id=>$payment)
		{
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$payment['payment_type'],
				'payment_amount'=>$payment['payment_amount'],
				'date'=>date('Y-m-d H:i:s',strtotime(str_replace('/','-', $payment['date']))),
				'emp_id'=>$payment['emp_id']
			);
			$this->db->insert('sales_suspended_payments',$sales_payments_data);

			//PAYMENT TRANSACTION
			$trans_type = "DEPOSIT";
			if ($d_id) {
				$trans_type = "UPDATE DEPOSIT";
			}
			$trans_data = [
							'transaction_time'=>$now,
							'transaction_type'=>$trans_type,
							'transaction_of_id'=>$sale_id,
							'transaction_of_user_id'=>$payment['emp_id'],
							'reference_no'=>$deposit_no,
							'amount'=>$payment['payment_amount']
						];

			$this->main_model->save_payment_transaction($trans_data);
			//END PAYMENT TRANSACTION
		}

		foreach($items as $line=>$item)
		{
			$cur_item_info = $this->Item->get_info($item['item_id']);
			if ($item['is_new']==1) {
				$cur_item_info = $this->Item->get_new_item_info($item['item_id']);
			}
			$sales_items_data = array
			(
				'sale_id'=>$sale_id,
				'item_id'=>$item['item_id'],
				'line'=>$item['line'],
				'desc'=>$item['description'],
				'serialnumber'=>$item['serialnumber'],
				'quantity_purchased'=>$item['quantity'],
				'discount_percent'=>$item['discount'],
				'item_cost_price' => $cur_item_info->cost_price,
				'item_unit_price'=>$item['price'],
				'desc'=>$item['desc'],
				'is_new'=>$item['is_new']
			);

			$this->db->insert('sales_suspended_items',$sales_items_data);

			//Update item pending
			$item_data = array('is_pending'=>1,
								'item_status'=>'Deposited');
			$this->db->where('item_id',$item['item_id'])->update('items',$item_data);
			
			$customer = $this->Customer->get_info($customer_id);
 			if ($customer_id == -1 or $customer->taxable)
 			{
				foreach($this->Item_taxes->get_info($item['item_id']) as $row)
				{
					$this->db->insert('sales_suspended_items_taxes', array(
						'sale_id' 	=>$sale_id,
						'item_id' 	=>$item['item_id'],
						'line'      =>$item['line'],
						'name'		=>$row['name'],
						'percent' 	=>$row['percent']
					));
				}
			}
		}
		if ($d_id) {
			$this->update_item_status($d_id);
			
		}
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}
		
		return $sale_id;
	}
	
	function delete($sale_id)
	{
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		$this->db->delete('sales_suspended_payments', array('sale_id' => $sale_id)); 
		$this->db->delete('sales_suspended_items_taxes', array('sale_id' => $sale_id)); 
		$this->db->delete('sales_suspended_items', array('sale_id' => $sale_id)); 
		$this->db->delete('sales_suspended', array('sale_id' => $sale_id)); 
		$this->update_item_status($sale_id);
		
		$this->db->trans_complete();
				
		return $this->db->trans_status();
	}
	function update_item_status($did){
		$dep_item = $this->db->query("SELECT item_id FROM ospos_items WHERE item_status = 'Deposited'")->result();
		foreach ($dep_item as $ditem) {
			$dpo = $this->db->query("SELECT * FROM ospos_sales_suspended s
										INNER JOIN ospos_sales_suspended_items i ON s.sale_id = i.sale_id
										WHERE s.sale_id = $did AND i.item_id = $ditem->item_id AND (s.`status` <> 'cancel' OR s.`status` IS NULL)")->row();
			if (!$dpo) {
				$this->db->where('item_id',$ditem->item_id)->update('items',array('item_status'=>'','is_pending'=>0));
			}
		}
	}

	function get_sale_items($sale_id)
	{
		// $this->db->from('v_select_all_items v');
		// $this->db->join('sales_suspended_items i','i.item_id=v.item_id','inner');
		// $this->db->join('sales_suspended_items n','n.is_new=v.is_new','inner');
		// $this->db->where('i.sale_id',$sale_id);
		$sql = $this->db->query("SELECT * FROM ospos_v_select_all_items v
								INNER JOIN ospos_sales_suspended_items i ON (i.item_id=v.item_id
																		AND i.is_new=v.is_new)
								WHERE i.sale_id=$sale_id");
		// $this->db->group_by('v.is_new');
		// var_dump($this->db->get()->result());
		// var_dump($sql->result());die();
		return $sql;
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('sales_suspended_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_customer($sale_id)
	{
		$this->db->from('sales_suspended');
		$this->db->where('sale_id',$sale_id);
		return $this->Customer->get_info($this->db->get()->row()->customer_id);
	}
	
	function get_comment($sale_id)
	{
		$this->db->from('sales_suspended');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get()->row()->comment;
	}
	function search_suspend($search){
			$sql = $this->db->query("SELECT * FROM ospos_sales_suspended q
										LEFT JOIN ospos_people p ON q.customer_id=p.person_id
										WHERE 1=1 
										AND (q.status IS NULL)
										AND (p.first_name LIKE '%".$search."%'
														OR p.last_name LIKE '%".$search."%'
														OR q.sale_time LIKE '%".date('Y-m-d',strtotime($search))."%'
														OR q.sale_id LIKE '%".$search."%'
														OR p.phone_number LIKE '%".$search."%'
														OR q.comment LIKE '%".$search."%'
														OR p.nick_name LIKE '%".$search."%')
										ORDER BY q.sale_id DESC
										LIMIT 20");
			return $sql;
		}
	function update_dstatus($d_id,$data){
		$now = date('Y-m-d H:i:s');
		$user_id = $this->session->userdata('person_id');


		$depo_total = $this->db->query("SELECT SUM(item_unit_price - discount_percent) as depo_total FROM ospos_sales_suspended_items WHERE sale_id = '$d_id'")->row()->depo_total;

		
		$this->db->where('sale_id',$d_id)->update('sales_suspended',$data);
		$this->update_item_status($d_id);

		$depinfo = $this->db->query("SELECT * FROM ospos_sales_suspended WHERE sale_id = '$d_id'")->row();

		$dpayment = $this->db->query("SELECT *,SUM(p.payment_amount) as payment FROM ospos_sales_suspended s
											INNER JOIN ospos_sales_suspended_payments p ON s.sale_id=p.sale_id
											WHERE s.sale_id = '$d_id'
											GROUP BY s.sale_id")->row()->payment;

		//PAYMENT TRANSACTION
		$trans_type = "CANCEL DEPOSIT";

		$payment_amount = $dpayment - $depinfo->cancel_restock;
		// if ($d_id) {
		// 	$trans_type = "UPDATE DEPOSIT";
		// }
		$trans_data = [
						'transaction_time'=>$now,
						'transaction_type'=>$trans_type,
						'transaction_of_id'=>$d_id,
						'transaction_of_user_id'=>$user_id,
						'reference_no'=>$depinfo->deposit_id,
						'amount'=>-$payment_amount
					];

		$this->main_model->save_payment_transaction($trans_data);
		//END PAYMENT TRANSACTION
	}
}
?>
