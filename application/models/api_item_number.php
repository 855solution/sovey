<?php
class Api_item_number extends CI_Model
{

	public function item_number()
	{
		$api = $this->load->database('api', TRUE);
		$api->select('barcode');
		$api->from('items');
		$api->where('deleted', 0);
		$api->where("barcode !=", "null");
		$api->where("barcode !=", "");
		$api->group_by("barcode");
		return $api->get();
	}
	
}
?>