<?php
class Partplacement extends CI_Model
{
	/*
	Determines if a given partplacement_id is an partplacement
	*/
	function exists( $partplacement_id )
	{
		$this->db->from('partplacements');
		$this->db->where('partplacement_id',$partplacement_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the partplacements
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('partplacements');
		$this->db->where('deleted',0);
		$this->db->order_by("partplacement_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('partplacements');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular partplacement
	*/
	function get_info($partplacement_id)
	{
		$this->db->from('partplacements');
		$this->db->where('partplacement_id',$partplacement_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $partplacement_id is NOT an partplacement
			$partplacement_obj=new stdClass();

			//Get all the fields from partplacements table
			$fields = $this->db->list_fields('partplacements');

			foreach ($fields as $field)
			{
				$partplacement_obj->$field='';
			}

			return $partplacement_obj;
		}
	}

	/*
	Get an partplacement id given an partplacement number
	*/
	function get_partplacement_id($partplacement_name)
	{
		$this->db->from('partplacements');
		$this->db->where('partplacement_name',$partplacement_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->partplacement_id;
		}

		return false;
	}

	/*
	Gets information about multiple partplacements
	*/
	function get_multiple_info($partplacement_ids)
	{
		$this->db->from('partplacements');
		$this->db->where_in('partplacement_id',$partplacement_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("partplacement_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a partplacement
	*/
	function save_old(&$partplacement_data,$partplacement_id=false)
	{
		if (!$partplacement_id or !$this->exists($partplacement_id))
		{
			if($this->db->insert('partplacements',$partplacement_data))
			{
				$partplacement_data['partplacement_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('partplacement_id', $partplacement_id);
		return $this->db->update('partplacements',$partplacement_data);
	}

	/*
	Updates multiple partplacements at once
	*/
	function update_multiple($partplacement_data,$partplacement_ids)
	{
		$this->db->where_in('partplacement_id',$partplacement_ids);
		return $this->db->update('partplacements',$partplacement_data);
	}

	/*
	Deletes one partplacement
	*/
	function delete($partplacement_id)
	{
		$this->db->where('partplacement_id', $partplacement_id);
		return $this->db->update('partplacements', array('deleted' => 1));
	}

	/*
	Deletes a list of partplacements
	*/
	function delete_list($partplacement_ids)
	{
		$this->db->where_in('partplacement_id',$partplacement_ids);
		return $this->db->update('partplacements', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find partplacements
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('partplacements');
		$this->db->like('partplacement_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("partplacement_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->partplacement_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on partplacements
	*/
	function search($search)
	{
		$this->db->from('partplacements');
		$this->db->where("partplacement_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("partplacement_name", "asc");
		return $this->db->get();	
	}
	
	public function get_partplacement_description( $partplacement_name )
	{
		if ( !$this->exists( $this->get_partplacement_id($partplacement_name)))
			return 0;
		
		$this->db->from('partplacements');
		$this->db->where('partplacement_name',$partplacement_name);
		return $this->db->get()->row()->description;
	}
	
	function update_partplacement_description( $partplacement_name, $description )
	{
		$this->db->where('partplacement_name', $partplacement_name);
		$this->db->update('partplacements', array('description' => $description));
	}

	function check_name($n,$id){
		$w = '';
		if ($id!='') {
			$w = " AND partplacement_id <> $id";
		}
		return $this->db->query("SELECT * FROM ospos_partplacements WHERE partplacement_name = '$n' {$w}  AND deleted=0 LIMIT 1")->row();
	}
	function save($id,$data){
		if ($id!='') {
			$this->db->where('partplacement_id',$id)->update('ospos_partplacements',$data);
		}else{
			$this->db->insert('ospos_partplacements',$data);
		}
	}
	function get_all_partplacement(){
		return $this->db->query("SELECT * FROM ospos_partplacements WHERE deleted=0")->result();
	}

}
?>
