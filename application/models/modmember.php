<?php
class modmember extends CI_Model
{
	function save(&$slide_data,$slide_id=false)
	{
		if ($slide_id==-1)
		{
			if($this->db->insert('member',$slide_data))
			{
				$slide_data['member_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('member_id', $slide_id);
		return $this->db->update('member',$slide_data);
	}
	function count_all()
	{
		//$this->db->from('models');
		//$this->db->where('deleted',0);
		$this->db->from('member');
		return $this->db->count_all_results();
	}
	function delete($model_id)
	{
		$this->db->where('member_id', $model_id);
		return $this->db->delete('member');
	}
	function delete_list($model_ids)
	{
		$this->db->where_in('member_id',$model_ids);
		return $this->db->delete('member');
 	}
	function get_all($limit=10000, $offset=0)
	{
//		$this->db->from('models');
//		$this->db->where('deleted',0);
//		$this->db->order_by("model_name", "asc");
//		$this->db->limit($limit);
//		$this->db->offset($offset);
//		return $this->db->get();

		$this->db->from('member');
		$this->db->order_by("member_id", "DESC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	
	}
	function get_info($slide_id)
	{
		$this->db->from('member')->where('member_id',$slide_id);
		
		//$this->db->from('models');
		//$this->db->where('model_id',$model_id);
		//$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $model_id is NOT an model
			$model_obj=new stdClass();

			//Get all the fields from models table
			$fields = $this->db->list_fields('member');

			foreach ($fields as $field)
			{
				$model_obj->$field='';
			}

			return $model_obj;
		}
	}
	function search($search)
	{
//		$this->db->from('models');
//		$this->db->where("model_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
//		$this->db->order_by("model_name", "asc");
//		return $this->db->get();
		
		$this->db->from('member');
		$this->db->where("last_name LIKE '%".$this->db->escape_like_str($search)."%' OR first_name LIKE '%".$this->db->escape_like_str($search)."%' ");
		$this->db->order_by("member_id", "desc");
		return $this->db->get();	
	}
}