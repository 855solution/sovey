<?php
class Branch extends CI_Model
{
	/*
	Determines if a given branch_id is an branch
	*/
	function exists( $branch_id )
	{
		$this->db->from('branchs');
		$this->db->where('branch_id',$branch_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the branchs
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('branchs');
		$this->db->where('deleted',0);
		$this->db->order_by("branch_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('branchs');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular branch
	*/
	function get_info($branch_id)
	{
		$this->db->from('branchs');
		$this->db->where('branch_id',$branch_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $branch_id is NOT an branch
			$branch_obj=new stdClass();

			//Get all the fields from branchs table
			$fields = $this->db->list_fields('branchs');

			foreach ($fields as $field)
			{
				$branch_obj->$field='';
			}

			return $branch_obj;
		}
	}

	/*
	Get an branch id given an branch number
	*/
	function get_branch_id($branch_name)
	{
		$this->db->from('branchs');
		$this->db->where('branch_name',$branch_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->branch_id;
		}

		return false;
	}

	/*
	Gets information about multiple branchs
	*/
	function get_multiple_info($branch_ids)
	{
		$this->db->from('branchs');
		$this->db->where_in('branch_id',$branch_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("branch_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a branch
	*/
	function save(&$branch_data,$branch_id=false)
	{
		if (!$branch_id or !$this->exists($branch_id))
		{
			if($this->db->insert('branchs',$branch_data))
			{
				$branch_data['branch_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('branch_id', $branch_id);
		return $this->db->update('branchs',$branch_data);
	}

	/*
	Updates multiple branchs at once
	*/
	function update_multiple($branch_data,$branch_ids)
	{
		$this->db->where_in('branch_id',$branch_ids);
		return $this->db->update('branchs',$branch_data);
	}

	/*
	Deletes one branch
	*/
	function delete($branch_id)
	{
		$this->db->where('branch_id', $branch_id);
		return $this->db->update('branchs', array('deleted' => 1));
	}

	/*
	Deletes a list of branchs
	*/
	function delete_list($branch_ids)
	{
		$this->db->where_in('branch_id',$branch_ids);
		return $this->db->update('branchs', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find branchs
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('branchs');
		$this->db->like('branch_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("branch_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->branch_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on branchs
	*/
	function search($search)
	{
		$this->db->from('branchs');
		$this->db->where("branch_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("branch_name", "asc");
		return $this->db->get();	
	}
	
	public function get_branch_description( $branch_name )
	{
		if ( !$this->exists( $this->get_branch_id($branch_name)))
			return 0;
		
		$this->db->from('branchs');
		$this->db->where('branch_name',$branch_name);
		return $this->db->get()->row()->description;
	}
	
	function update_branch_description( $branch_name, $description )
	{
		$this->db->where('branch_name', $branch_name);
		$this->db->update('branchs', array('description' => $description));
	}

}
?>
