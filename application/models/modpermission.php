<?php
class modpermission extends CI_Model 
{
	function getpermission(){
		return $this->db->query("SELECT * FROM ospos_group_permission")->result();
	}
	function get_perm($perm_id){
		return $this->db->query("SELECT * FROM ospos_group_permission WHERE perm_id='$perm_id'")->row();
	}
	function get_perm_detail($perm_id){
		return $this->db->where('perm_id',$perm_id)->get('permission_detail')->result();
	}
	function get_all_module($perm_id){
		return $this->db->query("SELECT *,m.module_id as module_id FROM `ospos_modules` m
								LEFT JOIN ospos_permission_detail p ON p.module_id=m.module_id
								AND p.perm_id = '$perm_id' ORDER BY m.module_id ASC")->result();
	}
	function save_detail($data){
		$this->db->insert('permission_detail',$data);
	}

	function save_perm($perm_id,$data){
		if ($perm_id) {
			$this->db->where('perm_id',$perm_id)->update('group_permission',$data);
		}else{
			$this->db->insert('group_permission',$data);
		}
	}
	function delete_perm($perm_id){
		$this->db->where('perm_id',$perm_id)->delete('group_permission');
		$this->db->where('perm_id',$perm_id)->delete('permission_detail');
	}
	function delete_detail($perm_id){
		$this->db->where('perm_id',$perm_id)->delete('permission_detail');
	}
	function get_all_perm(){
		return $this->db->get('group_permission')->result();
	}
	function save_set_user($data){
		$this->db->insert('user_permission',$data);
	}
	function save_emp_perm($data){
		$this->db->insert('permissions',$data);
	}
	function check_permission($user_id,$module_id,$field){
		$row = $this->db->query("SELECT * FROM ospos_user_permission u
							INNER JOIN ospos_permission_detail p ON u.perm_id=p.perm_id
							WHERE u.user_id = '$user_id' AND p.module_id='$module_id' AND p.$field=1")->row();
		return $row;
	}
	function user_permission_exist($perm_id,$user_id){
		$sql = $this->db->query('SELECT * FROM ospos_user_permission WHERE user_id = '.$user_id.'')->row();
		if ($sql) {
			$this->db->where('user_id',$user_id)->delete('user_permission');
			// $this->db->where('person_id',$user_id)->delete('permissions');
		}
	}
	function user_f($user_id,$f){
		$row = $this->db->query("SELECT * FROM ospos_user_permission u
							INNER JOIN ospos_permission_detail p ON u.perm_id=p.perm_id
							WHERE u.user_id = '$user_id' AND p.module_id='$module_id' AND p.$f=1")->row();
	}
}
?>