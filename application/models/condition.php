<?php
class Condition extends CI_Model
{
	/*
	Determines if a given condition_id is an condition
	*/
	function exists( $condition_id )
	{
		$this->db->from('conditions');
		$this->db->where('condition_id',$condition_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the conditions
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('conditions');
		$this->db->where('deleted',0);
		$this->db->order_by("condition_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('conditions');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular condition
	*/
	function get_info($condition_id)
	{
		$this->db->from('conditions');
		$this->db->where('condition_id',$condition_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $condition_id is NOT an condition
			$condition_obj=new stdClass();

			//Get all the fields from conditions table
			$fields = $this->db->list_fields('conditions');

			foreach ($fields as $field)
			{
				$condition_obj->$field='';
			}

			return $condition_obj;
		}
	}

	/*
	Get an condition id given an condition number
	*/
	function get_condition_id($condition_name)
	{
		$this->db->from('conditions');
		$this->db->where('condition_name',$condition_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->condition_id;
		}

		return false;
	}

	/*
	Gets information about multiple conditions
	*/
	function get_multiple_info($condition_ids)
	{
		$this->db->from('conditions');
		$this->db->where_in('condition_id',$condition_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("condition_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a condition
	*/
	function save_old(&$condition_data,$condition_id=false)
	{
		if (!$condition_id or !$this->exists($condition_id))
		{
			if($this->db->insert('conditions',$condition_data))
			{
				$condition_data['condition_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('condition_id', $condition_id);
		return $this->db->update('conditions',$condition_data);
	}

	/*
	Updates multiple conditions at once
	*/
	function update_multiple($condition_data,$condition_ids)
	{
		$this->db->where_in('condition_id',$condition_ids);
		return $this->db->update('conditions',$condition_data);
	}

	/*
	Deletes one condition
	*/
	function delete($condition_id)
	{
		$this->db->where('condition_id', $condition_id);
		return $this->db->update('conditions', array('deleted' => 1));
	}

	/*
	Deletes a list of conditions
	*/
	function delete_list($condition_ids)
	{
		$this->db->where_in('condition_id',$condition_ids);
		return $this->db->update('conditions', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find conditions
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('conditions');
		$this->db->like('condition_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("condition_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->condition_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on conditions
	*/
	function search($search)
	{
		$this->db->from('conditions');
		$this->db->where("condition_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("condition_name", "asc");
		return $this->db->get();	
	}
	
	public function get_condition_description( $condition_name )
	{
		if ( !$this->exists( $this->get_condition_id($condition_name)))
			return 0;
		
		$this->db->from('conditions');
		$this->db->where('condition_name',$condition_name);
		return $this->db->get()->row()->description;
	}
	
	function update_condition_description( $condition_name, $description )
	{
		$this->db->where('condition_name', $condition_name);
		$this->db->update('conditions', array('description' => $description));
	}
	function get_all_condition(){
		return $this->db->query("SELECT * FROM ospos_conditions WHERE deleted=0")->result();
	}
	function check_name($n,$id){
		$w = '';
		if ($id!='') {
			$w = " AND condition_id <> $id";
		}
		return $this->db->query("SELECT * FROM ospos_conditions WHERE condition_name = '$n' {$w}  AND deleted=0 LIMIT 1")->row();
	}
	function save($id,$data){
		if ($id!='') {
			$this->db->where('condition_id',$id)->update('ospos_conditions',$data);
		}else{
			$this->db->insert('ospos_conditions',$data);
		}
	}

}
?>
