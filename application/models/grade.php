<?php
class Grade extends CI_Model
{
	/*
	Returns all the grade
	*/
	function get_all_grade()
	{
		$this->db->from('grade');
		$this->db->order_by("grad_name", "asc");
		return $this->db->get();
	}
	
}
?>
