<?php
class Api_countingitem extends CI_Model
{

	public function get_item_by_location($location_name)
	{
		$api = $this->load->database('api', TRUE);
		$api->select('*');
		$api->from('locations');
		$api->where('location_name', $location_name);
		$api->where('deleted', 0);
		$query    = $api->get();
		$location = $query->row();
	
		$api->select('items.item_id,items.name, 
						items.unit_price,
						items.cost_price,
						items.year,
						items.quantity,
						items.barcode,
						items.fit,
						items.description,
						items.item_number,
						locations.location_name,
						colors.color_name,
						makes.make_name,
						conditions.condition_name,
						partplacements.partplacement_name,
						branchs.branch_name,
						categorys.category_name,
						khmernames.khmername_name,
						models.model_name,
						vinnumbers.vinnumber_name	
					');
		$api->from('items');
		$api->join('locations', 'items.location_id = locations.location_id');
		$api->join('colors', 'colors.color_id = items.color_id', 'left');
		$api->join('makes', 'makes.make_id = items.make_id', 'left');
		$api->join('conditions', 'conditions.condition_id = items.condition_id', 'left');
		$api->join('partplacements', 'partplacements.partplacement_id = items.partplacement_id', 'left');
		$api->join('branchs', 'branchs.branch_id = items.branch_id', 'left');
		$api->join('categorys', 'categorys.category_id = items.category_id', 'left');
		$api->join('khmernames', 'khmernames.khmername_id = items.khmername_id', 'left');
		$api->join('models', 'models.model_id = items.model_id', 'left');
		$api->join('vinnumbers', 'vinnumbers.make_id = items.year','left');

		$api->where('items.location_id', $location->location_id);
		$api->where('items.ishead', 0);
		$api->where('items.deleted', 0);
		$api->where('items.quantity >', 0);
		return $api->get();
	}
	
}
?>