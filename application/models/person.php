<?php
class Person extends CI_Model 
{
	/*Determines whether the given person exists*/
	function exists($person_id)
	{
		$this->db->from('people');	
		$this->db->where('people.person_id',$person_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}
	
	/*Gets all people*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('people');
		$this->db->order_by("last_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();		
	}
	
	function count_all()
	{
		$this->db->from('people');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	
	/*
	Gets information about a person as an array.
	*/
	function get_info($person_id)
	{
		$query = $this->db->get_where('people', array('person_id' => $person_id), 1);
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//create object with empty properties.
			$fields = $this->db->list_fields('people');
			$person_obj = new stdClass;
			
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	
	/*
	Get people with specific ids
	*/
	function get_multiple_info($person_ids)
	{
		$this->db->from('people');
		$this->db->where_in('person_id',$person_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}
	
	/*
	Inserts or updates a person
	*/
	function save(&$person_data,$person_id=false)
	{		

		$p1 = $person_data['phone_number'];
		$p2 = $person_data['phone_number_2'];
		if ($p1!='' || $p2!='') {
			if($this->check_phone($p1,$p2,$person_id)==false){
				redirect(site_url('customers?error=pn'));
			}
		}
			

		if (!$person_id or !$this->exists($person_id))
		{
			if ($this->db->insert('people',$person_data))
			{
				$person_data['person_id']=$this->db->insert_id();
				return true;
			}
			
			return false;
		}
		
		$this->db->where('person_id', $person_id);
		return $this->db->update('people',$person_data);
	}
	
	/*
	Deletes one Person (doesn't actually do anything)
	*/
	function delete($person_id)
	{
		return true;; 
	}
	
	/*
	Deletes a list of people (doesn't actually do anything)
	*/
	function delete_list($person_ids)
	{	
		return true;	
 	}
 	function check_phone($p1,$p2,$pid){

 		// FOR UPDATE NEW PHONE NUMBER
 		if ($pid!='-1') {
 			$p1 = $this->cur_phone1($pid,$p1);
 			$p2  = $this->cur_phone2($pid,$p2);
 		}

 		$where = '';
 		
 		$wp1 = '';
 		if ($p1!='') {
 			$wp1 .= " AND (p.phone_number = '$p1' OR p.phone_number_2 = '$p1')";
 		}
 		$wp2 = '';
 		if ($p2!='') {
 			$ao = ' AND';
 			if ($wp1) {
 				$ao = ' OR';
 			}
 			$wp2 .= " $ao (p.phone_number = '$p2' OR p.phone_number_2 = '$p2')";
 		}
 		if ($p1!='' || $p2!='') {
 			$ep = $this->db->query("SELECT * FROM ospos_people p
 									INNER JOIN ospos_customers c ON p.person_id=c.person_id
 									WHERE 1=1 {$wp1}{$wp2}
 									AND c.deleted='0'
 									{$where} 
 									LIMIT 1")->row();
 		}
 		// var_dump($ep);die();
 		
		if (!empty($ep)) {
			return false;
		}else{
			return true;
		}
 		
 		
 	}
 	function cur_phone1($pid,$p1){
 		$sql = $this->db->query("SELECT * FROM ospos_people WHERE person_id = $pid")->row();
 		if ($sql->phone_number == $p1) {
 			return false;
 		}
 		return $p1;
 	}
 	function cur_phone2($pid,$p2){
 		$sql = $this->db->query("SELECT * FROM ospos_people WHERE person_id = $pid")->row();
 		if ($sql->phone_number_2 == $p2) {
 			return false;
 		}
 		return $p2;
 	}
	
}
?>
