<?php 

	class sale_quotation extends CI_Model
	{
		
		function save ($items,$customer_id,$employee_id,$comment,$payments,$sale_id=false)
		{

			$q_id = $this->session->userdata('q_id');
			// if ($mode=='sale') {
			// 	if ($items[$line]['is_new']==1) {
			// 		$this->CI->Sale->delete_new_item($items[$line]['item_id']);
			// 		$this->CI->Sale->delete_qt_item($q_id,$items[$line]['item_id']);
			// 	}
			// }
			if(count($items)==0)
				return -1;

			//Alain Multiple payments
			//Build payment types string
			$payment_types='';
			foreach($payments as $payment_id=>$payment)
			{
				$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
			}

			$sales_data = array(
				'quote_time' => date('Y-m-d H:i:s'),
				'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
				'employee_id'=>$employee_id,
				'payment_type'=>$payment_types,
				'comment'=>$comment
			);

			//Run these queries as a transaction, we want to make sure we do all or nothing
			// $this->db->trans_start();
			if ($q_id) {
				$this->db->where('sale_id',$q_id)->update('sales_quote',$sales_data);
				$this->db->where('sale_id',$q_id)->delete('sales_quote_items');
				$this->db->where('sale_id',$q_id)->delete('sales_quote_payments');
				
				$sale_id = $q_id;
			}else{
				$this->db->insert('sales_quote',$sales_data);
				$sale_id = $this->db->insert_id();
			}
			
			$this->db->where('sale_id',$sale_id)->update('sales_quote',array('qt_number'=>str_pad($sale_id,6,'QT00',STR_PAD_LEFT)));
			foreach($payments as $payment_id=>$payment)
			{
				$sales_payments_data = array
				(
					'sale_id'=>$sale_id,
					'payment_type'=>$payment['payment_type'],
					'payment_amount'=>$payment['payment_amount']
				);
				$this->db->insert('sales_quote_payments',$sales_payments_data);
			}

			foreach($items as $line=>$item)
			{
				$cur_item_info = $this->Item->get_info($item['item_id']);
				if ($item['is_new']==1) {
					$cur_item_info = $this->Item->get_new_item_info($item['item_id']);
				}
				$sales_items_data = array
				(
					'sale_id'=>$sale_id,
					'item_id'=>$item['item_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'serialnumber'=>$item['serialnumber'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_cost_price' => $cur_item_info->cost_price,
					'item_unit_price'=>$item['price'],
					'desc'=>$item['desc'],
					'is_new'=>$item['is_new']
				);

				
				$this->db->insert('sales_quote_items',$sales_items_data);

				//Update item pending
				$item_data = array('is_pending'=>1);
				$this->Item->save($item_data,$item['item_id']);
				
				$customer = $this->Customer->get_info($customer_id);
	 			if ($customer_id == -1 or $customer->taxable)
	 			{
					foreach($this->Item_taxes->get_info($item['item_id']) as $row)
					{
						$this->db->insert('sales_quote_items_taxes', array(
							'sale_id' 	=>$sale_id,
							'item_id' 	=>$item['item_id'],
							'line'      =>$item['line'],
							'name'		=>$row['name'],
							'percent' 	=>$row['percent']
						));
					}
				}
			}
			// $this->db->trans_complete();
			
			// if ($this->db->trans_status() === FALSE)
			// {
			// 	return -1;
			// }
			
			return $sale_id;
		}
		function get_all()
		{
			$this->db->from('sales_quote s');
			$this->db->join('people p','s.customer_id=p.person_id','left');
			$this->db->where('status IS NULL');
			$this->db->order_by('sale_id','desc');
			$this->db->limit(20);
			return $this->db->get();
		}
		function get_sale_items($sale_id)
		{
			// $this->db->from('v_select_all_items v');
			// $this->db->join('sales_suspended_items i','i.item_id=v.item_id','inner');
			// $this->db->join('sales_suspended_items n','n.is_new=v.is_new','inner');
			// $this->db->where('i.sale_id',$sale_id);
			$sql = $this->db->query("SELECT * FROM ospos_v_select_all_items v
									INNER JOIN ospos_sales_quote_items i ON (i.item_id=v.item_id
																			AND i.is_new=v.is_new)
									WHERE i.sale_id=$sale_id");
			// $this->db->group_by('v.is_new');
			// var_dump($this->db->get()->result());
			// var_dump($sql->result());die();
			return $sql;
		}
		function get_sale_payments($sale_id)
		{
			$this->db->from('sales_quote_payments');
			$this->db->where('sale_id',$sale_id);
			return $this->db->get();
		}
		function get_customer($sale_id)
		{
			$this->db->from('sales_quote');
			$this->db->where('sale_id',$sale_id);
			return $this->Customer->get_info($this->db->get()->row()->customer_id);
		}
	
		function get_comment($sale_id)
		{
			$this->db->from('sales_quote');
			$this->db->where('sale_id',$sale_id);
			return $this->db->get()->row()->comment;
		}
		function delete($sale_id)
		{
			//Run these queries as a transaction, we want to make sure we do all or nothing
			$this->db->trans_start();
			
			$this->db->delete('sales_quote_payments', array('sale_id' => $sale_id)); 
			$this->db->delete('sales_quote_items_taxes', array('sale_id' => $sale_id)); 
			$this->db->delete('sales_quote_items', array('sale_id' => $sale_id)); 
			$this->db->delete('sales_quote', array('sale_id' => $sale_id)); 
			
			$this->db->trans_complete();
					
			return $this->db->trans_status();
		}
		function search_quote($search){
			$sql = $this->db->query("SELECT * FROM ospos_sales_quote q
										LEFT JOIN ospos_people p ON q.customer_id=p.person_id
										WHERE 1=1 
										AND (q.status IS NULL)
										AND (CONCAT(p.first_name,' ',p.last_name) LIKE '%".$search."%'
														OR CONCAT(p.last_name,' ',p.first_name) LIKE '%".$search."%'
														OR q.quote_time LIKE '%".date('Y-m-d',strtotime($search))."%'
														OR q.sale_id LIKE '%".$search."%'
														OR q.qt_number LIKE '%".$search."%'
														OR p.phone_number LIKE '%".$search."%'
														OR q.comment LIKE '%".$search."%'
														OR p.nick_name LIKE '%".$search."%')
										ORDER BY q.sale_id DESC
										LIMIT 20");
			return $sql;
		}
		function update_qstatus($q_id,$stat){
			$this->db->where('sale_id',$q_id)->update('sales_quote',array('status'=>$stat));
		}
		function getQinfo($sale_id){
			return $this->db->where('sale_id',$sale_id)->from('sales_quote')->get()->row();
		}
	}

?>