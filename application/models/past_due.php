<?php
class Past_due extends CI_Model 
{
	
  function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function get_all_pay(){

    	return $this->db->query("SELECT
									p.first_name,p.last_name,SUM(sp.payment_amount) as payment,SUM(sp.dept) as dept,
									p.person_id,p.phone_number,COUNT(s.sale_id) as sale
								FROM
									`ospos_sales_payments` sp
								JOIN `ospos_sales` s ON sp.`sale_id` = s.`sale_id`
								LEFT JOIN `ospos_people` p ON s.`customer_id` = p.`person_id`
								WHERE
									dept > 0
								GROUP BY s.customer_id")->result();
    }
	public function count_all()
	{
		
		$this->db->from('sales_payments');
		$this->db->join('sales','sales_payments.sale_id=sales.sale_id');	
		$st="dept>0";
  		$this->db->where($st, NULL, FALSE);  	
  		
	   	$query=$this->db->get();  
	   	return $query->num_rows();  
		
	}
	
	function get_all($cus_id,$limit=10000, $offset=0)
	{
		
		if (!$cus_id) {
			$cus_id = -1;
		}
		$df = $_GET['df'];
		$dt = $_GET['dt'];

		$this->db->select('*');
		$this->db->from('sales_payment_due');
		$this->db->join('sales','sales_payment_due.sale_id=sales.sale_id');	
		$this->db->join('people','sales.customer_id=people.person_id','left');	
		$st="total_due>0";
		$this->db->where($st, NULL, FALSE); 
		if ($df && $dt) {
			$this->db->where('DATE_FORMAT(sale_time,"%Y-%m-%d") BETWEEN "'.date('Y-m-d',strtotime($df)).'" AND "'.date('Y-m-d',strtotime($dt)).'"');
		}
		if ($cus_id) {
			$this->db->where('sales.customer_id',$cus_id);
		}

		$this->db->order_by("sales.sale_time", "DESC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();	
	}

	function get_due_detail($cus_id,$limit=10000,$offset=0){
		$where = '';
		if ($cus_id!='' && $cus_id!=-1) {
			$where.= " AND s.customer_id = $cus_id";
		}
		$df = $_GET['df'];
		$dt = $_GET['dt'];
		if ($df && $dt) {
			$where.= " AND s.sale_time DATE_FORMAT(sale_time,'%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($df))."' AND '".date('Y-m-d',strtotime($dt))."'";
			
		}

		// $query = $this->db->query("SELECT * FROM ospos_sales s 
		// 							LEFT JOIN ospos_people p ON s.customer_id = p.person_id
		// 							WHERE (s.payment_status <> 'paid' OR s.payment_status IS NULL) 
		// 							{$where} 
		// 							ORDER BY s.sale_time DESC");

		// NEW QUERY
		$query = $this->db->query("SELECT
										s.sale_id,
										s.invoiceid,
										s.payment_status,
										COALESCE(si.sale_total,0) as sale_total,
										COALESCE(rd.paid,0)+COALESCE(s.first_payment,0) as paid,
										COALESCE(si.sale_total,0) - (COALESCE(rd.paid,0) + COALESCE(s.first_payment,0)) as total_due,
										s.sale_time,
										p.nick_name,
										p.last_name,
										p.first_name
									FROM
										ospos_sales s
									INNER JOIN ospos_people p ON s.customer_id = p.person_id
									LEFT JOIN (SELECT SUM(item_unit_price - discount_percent) as sale_total,sale_id FROM ospos_sales_items WHERE is_return <> 1 GROUP BY sale_id) si ON si.sale_id = s.sale_id
									LEFT JOIN (SELECT SUM(amount_usd) as paid,sale_id FROM ospos_payment_record_detail GROUP BY sale_id) rd ON rd.sale_id=s.sale_id
									WHERE
										(
											s.payment_status <> 'paid'
											OR s.payment_status IS NULL 
										)
									AND COALESCE(si.sale_total,0) - (COALESCE(rd.paid,0) + COALESCE(s.first_payment,0)) >0
									{$where}
									GROUP BY s.sale_id
									ORDER BY
										s.sale_time DESC");

		return $query;
	}
	
	// function check_sale_payment($sale_id){

	// 	$sale_info = $this->get_sale_info($sale_id);

	// 	$grand_total = $this->get_sale_total($sale_id);

	// 	return $grand_total;
	// }

	function get_sale_total($sale_id){
		// $query = $this->db->query("SELECT payment_amount FROM ospos_sales_payment_due WHERE sale_id = $sale_id ORDER BY due_id DESC LIMIT 1")->row();
		$query = $this->db->query("SELECT SUM(item_unit_price - discount_percent) as sale_total FROM ospos_sales_items WHERE sale_id = '$sale_id' AND is_return<>1")->row();

		return $query->sale_total;
	}

	
	/*
	Gets information about a particular customer
	*/
	function get_info($sale_id)
	{
		$this->db->from('sales_payments');	
		$this->db->join('sales', 'sales_payments.sale_id = sales.sale_id');
		$this->db->where('sales_payments.sale_id',$sale_id);
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $sale_id is NOT an customer
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from sales_payments table
			$fields = $this->db->list_fields('sales_payments');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	
	
	/*
	 Select customer where sale id
	*/
	 
	function get_customer_name($sale_id){
			
		//$this->db->select('sales_payments as sale_pay');
		$this->db->from('sales as sal');
		$this->db->join('customers as cust','sal.customer_id=cust.person_id','left');
		$this->db->join('people as peoples','cust.person_id=peoples.person_id','left');

		$this->db->where('sal.sale_id',$sale_id);
		return $this->db->get();
		
	}
	
	/*
	Gets detail information about a particular past due
	*/
	function get_past_due_detail($sale_id)
	{
		$suggestions=array();
		$this->db->from('sales_payment_due');	
		//$this->db->join('sales', 'sales_payments.sale_id = sales.sale_id');
		$this->db->where('sales_payment_due.sale_id',$sale_id);
		$query = $this->db->get();
	
		foreach($query->result() as $row)
		{
			$suggestions[]=$row;		
		}
		
		return $suggestions;

		/*
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $sale_id is NOT an customer
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from sales_payments table
			$fields = $this->db->list_fields('sales_payment_due');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
		*/
	}
	
	/*
	Inserts or updates a customer
	*/
	function save($payment_due){

        $this->db->insert('sales_payment_due', $payment_due);

        return $this->db->insert_id();

    }
    
    
     function update($id, $data){

        $this->db->where('sale_id', $id);

        $this->db->update('sales_payments', $data);

    }
    
	/*
	function save($payment_due)
	{
		$success=false;
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		if(parent::save($payment_due))
		{
				//$payment_due['sale_id'] = $sale_id;
		$success = $this->db->insert('sales_payment_due',$payment_due);				
			
			
		}
		
		$this->db->trans_complete();		
		return $success;
	}
	*/
    
	/*
	Preform a search on customers
	*/
	function search($search)
	{
        
		$this->db->from('sales_payments as sale_pay');
		$this->db->join('sales as sal','sale_pay.sale_id=sal.sale_id','left');
		$this->db->join('customers as cust','sal.customer_id=cust.customer_id','left');
		$this->db->join('people as peoples','cust.person_id=peoples.person_id','left');
		
		$this->db->where(
		"(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		email LIKE '%".$this->db->escape_like_str($search)."%' or 
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		sale_id LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') 
		and dept!=0"
		);	
		
		$this->db->order_by("sale_id", "asc");
		
		return $this->db->get();	
	}
	
 	/*
	Get search suggestions to find customers
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		
		$this->db->from('sales_payments as sale_pay');
		$this->db->join('sales as sal','sale_pay.sale_id=sal.sale_id','left');
		$this->db->join('customers as cust','sal.customer_id=cust.customer_id','left');
		$this->db->join('people as peoples','cust.person_id=peoples.person_id','left');
		
		$this->db->where(
		"(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') 
		and dept!=0"
		);	
		
		$this->db->order_by("sale_id", "asc");		
		$by_name = $this->db->get();
		
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->first_name.' '.$row->last_name;		
		}
		$dept="dept!=0";
		
		$this->db->from('sales_payments as sale_pay');
		$this->db->join('sales as sal','sale_pay.sale_id=sal.sale_id','left');
		$this->db->join('customers as cust','sal.customer_id=cust.customer_id','left');
		$this->db->join('people as peoples','cust.person_id=peoples.person_id','left');
		
		$this->db->where($dept,Null,false);		
		$this->db->like("email",$search);
		
		$this->db->order_by("email", "asc");		
		$by_email = $this->db->get();
		foreach($by_email->result() as $row)
		{
			$suggestions[]=$row->email;		
		}

		
		
		$this->db->from('sales_payments as sale_pay');
		$this->db->join('sales as sal','sale_pay.sale_id=sal.sale_id','left');
		$this->db->join('customers as cust','sal.customer_id=cust.customer_id','left');
		$this->db->join('people as peoples','cust.person_id=peoples.person_id','left');
		
		$this->db->where($dept,Null,false);		
		$this->db->like("phone_number",$search);
		$this->db->order_by("phone_number", "asc");	

		
		$by_phone = $this->db->get();
		foreach($by_phone->result() as $row)
		{
			$suggestions[]=$row->phone_number;		
		}
		
		$this->db->from('sales_payments as sale_pay');
		$this->db->join('sales as sal','sale_pay.sale_id=sal.sale_id','left');
		$this->db->join('customers as cust','sal.customer_id=cust.customer_id','left');
		$this->db->join('people as peoples','cust.person_id=peoples.person_id','left');
		
		$this->db->where($dept,Null,false);			
		$this->db->like("account_number",$search);
		$this->db->order_by("account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=$row->account_number;		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	
	}
	
	function getEmployeeinfo($sale_id){
		
		$this->db->from('sales as sal');
		$this->db->join('employees as emp','sal.employee_id=emp.person_id','left');
		$this->db->join('people as peoples','emp.person_id=peoples.person_id','left');
		
		$this->db->where('sal.sale_id',$sale_id);
		return $this->db->get();

	}
	function get_selected_sale($sale_ids){
		$where =' AND(';
        for ($i=0; $i < count($sale_ids) ; $i++) { 
                $where.="s.sale_id='".$sale_ids[$i]."'";
                
                if($i<count($sale_ids)-1)
                    $where.=' OR ';
        }
        $where.=')';
        return $this->db->query("SELECT * FROM ospos_sales s
        							WHERE 1=1
        								AND (s.payment_status <> 'paid' OR s.payment_status IS NULL) 
        								{$where}
        							ORDER BY s.sale_time DESC")
                        ->result();
	}
	function get_selected_sale_items($sale_ids){
		$where =' AND(';
        for ($i=0; $i < count($sale_ids) ; $i++) { 
                $where.="s.sale_id='".$sale_ids[$i]."'";
                
                if($i<count($sale_ids)-1)
                    $where.=' OR ';
        }
        $where.=')';
		return $this->db->query("SELECT
										i. `name`,
										i.category,
										i.barcode,
										i.`year`,
										m.make_name,
										mo.model_name,
										k.khmername_name,
										k.english_name,
										s.invoiceid,
										si.quantity_purchased,
										si.discount_percent,
										si.item_unit_price,
										s.sale_time,
										v.vinnumber_id,
										s.`comment`,
										si.description,
										si.is_new,
										i.partplacement_id,
										ppl.partplacement_name,
										r.disc as return_restock,
										rs.return_no,
										si.is_return,
										s.sale_id

									FROM
										ospos_sales s
									INNER JOIN ospos_sales_items si ON s.sale_id = si.sale_id
									INNER JOIN ospos_v_select_all_items i ON si.item_id = i.item_id
									AND si.is_new = i.is_new
									LEFT JOIN ospos_makes m ON i.make_id = m.make_id
									LEFT JOIN ospos_models mo ON i.model_id = mo.model_id
									LEFT JOIN ospos_khmernames k ON i.khmername_id = k.khmername_id
									LEFT JOIN ospos_vinnumbers v ON i.category=v.vinnumber_name
									LEFT JOIN ospos_partplacements ppl ON i.partplacement_id=ppl.partplacement_id
									LEFT JOIN ospos_return_item r ON si.item_id = r.item_id
									LEFT JOIN ospos_return_sale rs ON r.return_id = rs.return_id
									WHERE
										1 = 1
								{$where}
								ORDER BY s.sale_time DESC")
							->result();
		
	}
	function get_selected_sale_due($sale_ids){
		$where =' AND(';
        for ($i=0; $i < count($sale_ids) ; $i++) { 
                $where.="sale_id='".$sale_ids[$i]."'";
                
                if($i<count($sale_ids)-1)
                    $where.=' OR ';
        }
        $where.=')';

		return $this->db->query("SELECT SUM(total_due) as all_due 
								FROM ospos_sales_payment_due
								WHERE 1=1
								{$where}")->row()->all_due;
	}

	function get_selected_sale_total($sale_ids){
		$where =' AND(';
        for ($i=0; $i < count($sale_ids) ; $i++) { 
                $where.="sale_id='".$sale_ids[$i]."'";
                
                if($i<count($sale_ids)-1)
                    $where.=' OR ';
        }
        $where.=')';

		// return $this->db->query("SELECT SUM(total_due) as all_due 
		// 						FROM ospos_sales_payment_due
		// 						WHERE 1=1
		// 						{$where}")->row()->all_due;

		$query = $this->db->query("SELECT SUM(item_unit_price - discount_percent) as sale_total FROM ospos_sales_items WHERE 1=1 {$where} AND is_return<>1")->row();

		return $query->sale_total;
		
	}
	function get_selected_sale_paid($sale_ids){
		// $where =' AND(';
		$paid = 0;
        for ($i=0; $i < count($sale_ids) ; $i++) { 
                // $where.="sale_id='".$sale_ids[$i]."'";
                $paid+= $this->get_sale_paid($sale_ids[$i]);
                // if($i<count($sale_ids)-1)
                //     $where.=' OR ';
        }
        // $where.=')';

		return $paid;
	}

	public function save_payment_record($data){
        if($this->db->insert('payment_record',$data)){
            return $this->db->insert_id();
        }
        return false;
    }
    public function save_record_detail($data){
        $this->db->insert('payment_record_detail',$data);
    }
    function addPayment($sale_id,$data){

    	$this->db->where('sale_id',$sale_id)->update('sales_payment_due',$data);

    	
    	return true;
    }

    function addSalePayment($sale_id,$paid){

    	$now = date('Y-m-d H:i:s');
    	$sales_payments_data = array
		(
			'sale_id'=>$sale_id,
			'payment_type'=>'Cash',
			'payment_amount'=>$paid,
			'date'=>$now,
			'emp_id'=>$this->session->userdata('person_id')
			
		);


		$this->db->insert('sales_payments',$sales_payments_data);

		$saleinfo = $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = '$sale_id'")->row();

		//PAYMENT TRANSACTION
		$trans_type = "DUE PAYMENT";

		// $payment_amount = $dpayment - $depinfo->cancel_restock;
		// if ($d_id) {
		// 	$trans_type = "UPDATE DEPOSIT";
		// }
		$trans_data = [
						'transaction_time'=>$now,
						'transaction_type'=>$trans_type,
						'transaction_of_id'=>$sale_id,
						'transaction_of_user_id'=>$this->session->userdata('person_id'),
						'reference_no'=>$saleinfo->invoiceid,
						'amount'=>$paid
					];

		$this->main_model->save_payment_transaction($trans_data);
		//END PAYMENT TRANSACTION
    }
    function update_sale_status($sale_id,$status){
    	$this->db->where('sale_id',$sale_id)->update('sales',array('payment_status'=>$status));
    }
    function get_sale_info($sale_id){
    	return $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = $sale_id")->row();
    }
    function get_search_past_due($cus_id,$key){
    	$where = '';
    	if ($cus_id) {
    		$where .= " AND s.customer_id = $cus_id";
    	}
    	if ($key) {
    		$where .= " AND (p.last_name LIKE '%$key%' OR
    						 p.first_name LIKE '%$key%' OR
    						 p.nick_name LIKE '%$key%' OR
    						 p.phone_number LIKE '%$key%'
    						)";
    	}

    	return $this->db->query("SELECT
									p.first_name,
									p.last_name,
									p.person_id,
									p.phone_number,
									p.nick_name,
									count(sale_id) as sale,
									p.person_id as cus_id
								FROM `ospos_sales` s
								LEFT JOIN `ospos_people` p ON s.`customer_id` = p.`person_id`
								WHERE (s.payment_status<>'paid' OR s.payment_status IS NULL) AND s.customer_id IS NOT NULL
								{$where}
								GROUP BY s.customer_id")->result();


    }
    function get_total_cus_sale($cus_id){

    	$query = $this->db->query("SELECT sale_id FROM ospos_sales WHERE customer_id = '$cus_id' AND (payment_status<>'paid' OR payment_status IS NULL) ")->result();
    	$sale_total = 0;
    	$paid_total = 0;
    	$sale_count = 0;
    	$return_total =0;
    	foreach ($query as $sale) {
    		$gtotal = $this->get_sale_total($sale->sale_id);
    		$paid = $this->get_sale_paid($sale->sale_id);
    		$return_amount = $this->get_sale_return_amount($sale->sale_id);
			$due = $gtotal - $paid;

			if ($due>0) {
				$sale_count++;
				$paid_total += $paid;
				$sale_total += $gtotal; 
				$return_total += $return_amount;
			}
					
    		
    	}
    	$data['sale_total'] = $sale_total;
    	$data['paid_total'] = $paid_total;
    	$data['sale_count'] = $sale_count;
    	$data['return_amount'] = $return_total;
    	
    	// var_dump($this->get_sale_return_amount($sale->sale_id));die();
    	return $data;
    }

    function get_sale_paid($sale_id){
    	$first_paid = $this->db->query("SELECT first_payment FROM ospos_sales WHERE sale_id = $sale_id")->row()->first_payment;
    	$paid = $this->db->query("SELECT SUM(amount_usd) as paid FROM ospos_payment_record_detail WHERE sale_id='$sale_id'")->row()->paid;

		return $first_paid+$paid;
    }

    function get_sale_return_amount($sale_id){
		$query = $this->db->query("SELECT SUM(rt.price - rt.disc) as return_amount FROM ospos_return_sale r
												INNER JOIN ospos_return_item rt ON r.return_id = rt.return_id
												WHERE r.sale_id = '$sale_id'")->row();

		return $query->return_amount;
	}
	
	
}
?>