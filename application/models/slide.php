<?php
class Slide extends CI_Model
{
	function save(&$slide_data,$slide_id=false)
	{
		if ($slide_id==-1)
		{
			if($this->db->insert('slide',$slide_data))
			{
				$slide_data['slide_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('slide_id', $slide_id);
		return $this->db->update('slide',$slide_data);
	}
	function count_all()
	{
		//$this->db->from('models');
		//$this->db->where('deleted',0);
		$this->db->from('slide');
		return $this->db->count_all_results();
	}
	function delete($model_id)
	{
		$this->db->where('slide_id', $model_id);
		return $this->db->delete('slide');
	}
	function delete_list($model_ids)
	{
		$this->db->where_in('slide_id',$model_ids);
		return $this->db->delete('slide');
 	}
	function get_all($limit=10000, $offset=0)
	{
//		$this->db->from('models');
//		$this->db->where('deleted',0);
//		$this->db->order_by("model_name", "asc");
//		$this->db->limit($limit);
//		$this->db->offset($offset);
//		return $this->db->get();

		$this->db->from('slide');
		$this->db->order_by("orders", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	
	}
	function get_info($slide_id)
	{
		$this->db->from('slide')->where('slide_id',$slide_id);
		
		//$this->db->from('models');
		//$this->db->where('model_id',$model_id);
		//$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $model_id is NOT an model
			$model_obj=new stdClass();

			//Get all the fields from models table
			$fields = $this->db->list_fields('slide');

			foreach ($fields as $field)
			{
				$model_obj->$field='';
			}

			return $model_obj;
		}
	}
	function search($search)
	{
//		$this->db->from('models');
//		$this->db->where("model_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
//		$this->db->order_by("model_name", "asc");
//		return $this->db->get();
		
		$this->db->from('slide');
		$this->db->where("name LIKE '%".$this->db->escape_like_str($search)."%' ");
		$this->db->order_by("orders", "asc");
		return $this->db->get();	
	}
}