<?php
class Khmername extends CI_Model
{
	function getimage($khmername_id){
        $data=$this->db->query("SELECT * FROM ospos_khmername_images WHERE khmername_id='$khmername_id' ORDER BY first_image ASC")->row();
       
        return $data;
    }
 	function deleteimg($imgid){
        $this->db->where('image_id',$imgid)->delete('ospos_customer_images');
    }
	/*
	Determines if a given khmername_id is an khmername
	*/
	function exists( $khmername_id )
	{
		$this->db->from('khmernames');
		$this->db->where('khmername_id',$khmername_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the khmernames
	*/
	function get_all($limit=20, $offset=0)
	{
		$this->db->from('khmernames');
		$this->db->where('deleted',0);
		$this->db->order_by("khmername_name", "asc");
		// $this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function get_khmername_data(){
		$this->db->from('khmernames')
				->where('deleted',0)
				->order_by('khmername_name','asc');
		return $this->db->get()->result();
	}
	

	
	function count_all()
	{
		$this->db->from('khmernames');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular khmername
	*/
	function get_info($khmername_id)
	{
		$this->db->from('khmernames');
		$this->db->where('khmername_id',$khmername_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $khmername_id is NOT an khmername
			$khmername_obj=new stdClass();

			//Get all the fields from khmernames table
			$fields = $this->db->list_fields('khmernames');

			foreach ($fields as $field)
			{
				$khmername_obj->$field='';
			}

			return $khmername_obj;
		}
	}

	/*
	Get an khmername id given an khmername number
	*/
	function get_khmername_id($khmername_name)
	{
		$this->db->from('khmernames');
		$this->db->where('khmername_name',$khmername_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->khmername_id;
		}

		return false;
	}

	/*
	Gets information about multiple khmernames
	*/
	function get_multiple_info($khmername_ids)
	{
		$this->db->from('khmernames');
		$this->db->where_in('khmername_id',$khmername_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("khmername_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a khmername
	*/
	function save(&$khmername_data,$khmername_id=false)
	{
		if (!$khmername_id or !$this->exists($khmername_id))
		{
			if($this->db->insert('khmernames',$khmername_data))
			{
				$khmername_data['khmername_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('khmername_id', $khmername_id);
		return $this->db->update('khmernames',$khmername_data);
	}

	/*
	Updates multiple khmernames at once
	*/
	function update_multiple($khmername_data,$khmername_ids)
	{
		$this->db->where_in('khmername_id',$khmername_ids);
		return $this->db->update('khmernames',$khmername_data);
	}

	/*
	Deletes one khmername
	*/
	function delete($khmername_id)
	{
		$this->db->where('khmername_id', $khmername_id);
		return $this->db->update('khmernames', array('deleted' => 1));
	}

	/*
	Deletes a list of khmernames
	*/
	function delete_list($khmername_ids)
	{
		$this->db->where_in('khmername_id',$khmername_ids);
		return $this->db->update('khmernames', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find khmernames
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('khmernames');
		$this->db->like('khmername_name', $search);
		$this->db->or_like('english_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("khmername_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->khmername_name.'-'.$row->english_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on khmernames
	*/
	function search($search)
	{
		$this->db->from('khmernames');
		$this->db->where("deleted = '0' AND (khmername_name LIKE '%".$this->db->escape_like_str($search)."%' 
										OR english_name LIKE '%".$this->db->escape_like_str($search)."%')
										OR khmername_id = '".$this->db->escape_like_str($search)."'");
		$this->db->order_by("khmername_name", "asc");
		$this->db->limit(20);
		return $this->db->get();	
	}
	
	public function get_khmername_description( $khmername_name )
	{
		if ( !$this->exists( $this->get_khmername_id($khmername_name)))
			return 0;
		
		$this->db->from('khmernames');
		$this->db->where('khmername_name',$khmername_name);
		return $this->db->get()->row()->description;
	}
	
	function update_khmername_description( $khmername_name, $description )
	{
		$this->db->where('khmername_name', $khmername_name);
		$this->db->update('khmernames', array('description' => $description));
	}
	function check_name($kh_en,$id,$ty){
		$where ='';
		if ($id!='') {
			$where.=" AND khmername_id <> '$id'";
		}
		if ($ty=='k') {
			$where.=" AND khmername_name = '$kh_en'";
		}
		if ($ty=='e') {
			$where.= " AND english_name = '$kh_en'";
		}

		$ex = $this->db->query("SELECT * FROM ospos_khmernames WHERE 1=1 {$where} AND deleted=0")->row();
		if ($ex) {
			return 1;
		}else{
			return 0;
		}
	}

}
?>
