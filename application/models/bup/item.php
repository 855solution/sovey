<?php
class Item extends CI_Model
{
function get_dropdown($name, $value)
    {
        $arr = array();
        switch($name)
        {
            case 'make_id' :
                $query = $this->db->where('make_id', $value)->get('models');
                
                if($query->num_rows() > 0)
                {
                    foreach($query->result() as $row)
                    {
                        $arr[] = array($row->model_id => $row->model_name);
                    }
                }
                else
                {
                    $arr[] = array('0' => 'No model');
                }
            break;
            default :
                $arr[] = array(
                    array('1' => 'Data 1'),
                    array('2' => 'Data 2'),    
                    array('3' => 'Data 3')
                );
            break;
        }
        return $arr;
    }

	/*
	Determines if a given item_id is an item
	*/
	function exists($item_id)
	{
		$this->db->from('items');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($limit=10000, $offset=0)
	{
		
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		return $this->db->count_all_results();
	}

	function get_all_filtered($low_inventory=0,$is_serialized=0,$no_description)
	{
		$this->db->from('items');
		if ($low_inventory !=0 )
		{
			$this->db->where('quantity <=','reorder_level', false);
		}
		if ($is_serialized !=0 )
		{
			$this->db->where('is_serialized',1);
		}
		if ($no_description!=0 )
		{
			$this->db->where('description','');
		}
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		return $this->db->get();
	}

	/*
	Gets information about a particular item
	*/
	function get_infokhmername($item_id)
	{
		$this->db->from('items');
		$this->db->join('khmernames', 'khmernames.khmername_id = items.khmername_id');
		$this->db->where('item_id',$item_id);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	
	function get_info($item_id)
	{
		//$this->db->from('items');
		//$this->db->select('*','items.description as item_desc');
		$this->db->select('*');
		$this->db->select('items.description as desc_item');
		$this->db->from('items');
		$this->db->join('khmernames', 'items.khmername_id = khmernames.khmername_id','left');
		$this->db->join('partplacements', 'items.partplacement_id = partplacements.partplacement_id','left');
		$this->db->where('item_id',$item_id);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	/*
	Get an item id given an item number
	*/
	function get_item_id($item_number)
	{
		$this->db->from('items');
		$this->db->where('item_number',$item_number);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->item_id;
		}

		return false;
	}

	/*
	Gets information about multiple items
	*/
	function get_multiple_info($item_ids)
	{
		$this->db->from('items');
		$this->db->where_in('item_id',$item_ids);
		$this->db->order_by("item_id", "desc");
		return $this->db->get();
	}

	/*
	Inserts or updates a item
	*/
	function save(&$item_data,$item_id=false)
	{
		if (!$item_id or !$this->exists($item_id))
		{
			if($this->db->insert('items',$item_data))
			{
				$item_data['item_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($item_data,$item_ids)
	{
		$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items',$item_data);
	}

	/*
	Deletes one item
	*/
	function delete($item_id)
	{
		$this->db->where('item_id', $item_id);
		return $this->db->update('items', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find items
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('items');
		$this->db->like('name', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->name;
		}
		$this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->like('item_number', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_number;
		}

		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->distinct();
		$this->db->like('category', $search);
		$this->db->order_by("item_id", "desc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		$this->db->from('items');
		$this->db->like('barcode', $search);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->barcode;
		}


		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_item_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('name', $search);
		$this->db->order_by("item_id", "desc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->name;
		}
		$this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('item_number', $search);
		$this->db->order_by("item_id", "desc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->item_number;
		}

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('barcode', $search);
		$this->db->order_by("item_id", "desc");
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->barcode;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_item_search_suggestions_sale($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->like('item_id', $search);
		$this->db->order_by("item_id", "desc");
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=$row->item_id.'|'.$row->item_id;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}


	function get_category_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('items');
		$this->db->like('category', $search);
		$this->db->where('deleted', 0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->category;
		}

		return $suggestions;
	}

	/*
	Preform a search on items
	*/
	function search($search)
	{
		$this->db->from('items');
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
		barcode LIKE '%".$this->db->escape_like_str($search)."%' or
		item_number LIKE '%".$this->db->escape_like_str($search)."%' or
		category LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		$this->db->order_by("item_id", "desc");
		return $this->db->get();	
	}

	function get_categories()
	{
		$this->db->select('category');
		$this->db->from('items');
		$this->db->where('deleted',0);
		$this->db->distinct();
		$this->db->order_by("item_id", "desc");

		return $this->db->get();
	}
	
	function get_partNumber_suggestions($search)
	{
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('item_number');
		$this->db->from('items');
		$this->db->like('item_number', $search);
		$this->db->where('deleted', 0);
		$this->db->order_by("item_id", "desc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=$row->item_number;
		}

		return $suggestions;
	}

	function make_name($makeId){
		$suggestions = array();
		
		$this->db->select('make_name');
		$this->db->from('makes');
		$this->db->where('make_id', $makeId);
		$get_result=$this->db->get();
		return $get_result;
		
	}
	
	function model_name($modelId){
		$suggestions = array();
		
		$this->db->select('model_name');
		$this->db->from('models');
		$this->db->where('model_id', $modelId);
		$get_result=$this->db->get();
		return $get_result;
		
	}
	
	
	
	function listAllInfoItem($item_number){
		$this->db->from('items');
		$this->db->where('item_number',$item_number);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$query = $this->db->get();
		
	}
	
	/*
	Gets information about a particular item
	*/
	function get_info_where_itemnumber($itemnumber)
	{
		$this->db->from('items');
		$this->db->where('item_number',$itemnumber);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	/*
	function get_image_source($itemId){
		
		//$this->db->select('imagename');
		$this->db->from('image_items');
		$this->db->where('item_id', $itemId);
		$get_result=$this->db->get();
		return $get_result;
		
	}	
	*/
	
	function get_image_source($item_id)
	{
		$this->db->from('image_items');
		$this->db->where('item_id', $item_id);
		$get_result=$this->db->get();
		return $get_result;
	}
	
	function get_all_grade($limit=10000, $offset=0)
	{
		$this->db->from('grade');
		$this->db->order_by("grad_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function get_unitPrice_costPrice($gradid)
	{
		$this->db->from('items');
		$this->db->where('grad_id', $gradid);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		$get_result=$this->db->get();
		return $get_result;
	}
	
function getLastId(){
	
		
		$query = $this->db->query('SELECT max(item_id) as maxid FROM ospos_items');
		$row = $query->row();
		return $max_id = $row->maxid; 
		
		}
		
function updateBarcodeAfterSave($item_id,$item_data){
	$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	
	}	
	function delete_Image_item($imageid){
	$query = $this->db->query("DELETE FROM ospos_image_items WHERE image_id = '$imageid'");
	$row = $query->row();
	
}		
function getImageName($imageid){
	$query = $this->db->query("SELECT imagename FROM ospos_image_items WHERE image_id = '$imageid'");
	$row = $query->row();
	return $row->imagename;
}	

function fullTextSearch($searchfield){


	$numrows=$this->seachWithoutFulltext($searchfield)->num_rows();
	
	$sql="
	SELECT 
		ospos_items.*,
		MATCH(name,year)AGAINST('$searchfield')as yearrank,
		MATCH(model_name,model_description)AGAINST ('$searchfield') as modelrank,
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield') as makerank
		FROM
		ospos_items
		INNER JOIN 
		ospos_models ON ospos_items.model_id=ospos_models.model_id
		INNER JOIN
		ospos_makes ON ospos_makes.make_id=ospos_items.make_id
		AND
		(
		MATCH (name,year)AGAINST ('$searchfield') OR 
		MATCH (model_name,model_description)AGAINST ('$searchfield')OR
		MATCH(ospos_makes.make_name,ospos_makes.description)AGAINST ('$searchfield')
		) 
		WHERE
		ospos_items.deleted=0 and ishead=0
		order by modelrank desc, makerank desc,yearrank desc limit 0, 30";

		
		
		if($numrows!=0){
			return $this->seachWithoutFulltext($searchfield);
		}else{
			$query = $this->db->query($sql);
			return $query;
		}
		
}

function seachWithoutFulltext($search){
	
		$this->db->from('items');
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or 
		item_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		barcode LIKE '%".$this->db->escape_like_str($search)."%') and (deleted=0 and ishead=0)");
		$this->db->order_by("item_id", "desc");
		$query=$this->db->get();
		return $query;
	
}

/*Sale*/
function convertBarcodetoItemid($barcode){
	
		$this->db->from('items');
		$this->db->where('barcode',$barcode);
		$this->db->where('deleted',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	
}



function khmer_name($khmer_id){
		$suggestions = array();
		
		$this->db->select('khmername_name');
		$this->db->from('khmernames');
		$this->db->where('khmername_id',$khmer_id);
		$get_result=$this->db->get();
		return $get_result;
		
	}

function get_all_filtered_make_model_year($make_filter=0,$model_fitler=0,$year_filter=1980,$yearto_filter=1980,$khmername_filter)
	{
		$this->db->from('items');
		
		
		
		if ($make_filter !=0 && $model_fitler!=0 && $year_filter!=0 && $yearto_filter!=0&&$khmername_filter!=0)
		{
			$this->db->where('make_id',$make_filter);
			$this->db->where('model_id',$model_fitler);
			$this->db->where('khmername_id',$khmername_filter);
			//$this->db->where('year',$year_filter);
			$this->db->where('year BETWEEN "'. $year_filter. '" and "'. $yearto_filter.'"');
		}
		
		if($make_filter !=0 && $model_fitler!=0)
		{
			$this->db->where('make_id',$make_filter);
			$this->db->where('model_id',$model_fitler);
			
		}
		if($make_filter !=0)
		{
			$this->db->where('make_id',$make_filter);
	
		}
		
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		//$this->db->order_by("item_id", "desc");
		$this->db->order_by("year", "desc");
		return $this->db->get();
	}	

	function get_info_where_makeId($makeid)
	{
		$this->db->from('items');
		$this->db->where('make_id',$makeid);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	function get_info_where_modelId($modelid)
	{
		$this->db->from('items');
		$this->db->where('model_id',$modelid);
		$this->db->where('deleted',0);
		$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}	
	
	function get_make_model_year($itemid)
	{
		$this->db->from('items');
		$this->db->join('makes', 'makes.make_id = items.make_id','left');
		$this->db->join('models', 'models.model_id = items.model_id','left');
		$this->db->join('colors', 'colors.color_id = items.color_id','left');
		$this->db->join('partplacements', 'partplacements.partplacement_id = items.partplacement_id','left');
		$this->db->join('branchs', 'branchs.branch_id = items.branch_id','left');
		$this->db->where('item_id',$itemid);
		$this->db->where('items.deleted',0);
		//$this->db->where('ishead',0);
		$this->db->order_by("item_id", "desc");
		$this->db->limit(1);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	
}
?>