<?php
class Location extends CI_Model
{
	/*
	Determines if a given location_id is an location
	*/
	function exists( $location_id )
	{
		$this->db->from('locations');
		$this->db->where('location_id',$location_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the locations
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('locations');
		$this->db->where('deleted',0);
		$this->db->order_by("location_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$this->db->from('locations');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular location
	*/
	function get_info($location_id)
	{
		$this->db->from('locations');
		$this->db->where('location_id',$location_id);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $location_id is NOT an location
			$location_obj=new stdClass();

			//Get all the fields from locations table
			$fields = $this->db->list_fields('locations');

			foreach ($fields as $field)
			{
				$location_obj->$field='';
			}

			return $location_obj;
		}
	}

	/*
	Get an location id given an location number
	*/
	function get_location_id($location_name)
	{
		$this->db->from('locations');
		$this->db->where('location_name',$location_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->location_id;
		}

		return false;
	}

	/*
	Gets information about multiple locations
	*/
	function get_multiple_info($location_ids)
	{
		$this->db->from('locations');
		$this->db->where_in('location_id',$location_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("location_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a location
	*/
	function save(&$location_data,$location_id=false)
	{
		if (!$location_id or !$this->exists($location_id))
		{
			if($this->db->insert('locations',$location_data))
			{
				$location_data['location_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('location_id', $location_id);
		return $this->db->update('locations',$location_data);
	}

	/*
	Updates multiple locations at once
	*/
	function update_multiple($location_data,$location_ids)
	{
		$this->db->where_in('location_id',$location_ids);
		return $this->db->update('locations',$location_data);
	}

	/*
	Deletes one location
	*/
	function delete($location_id)
	{
		$this->db->where('location_id', $location_id);
		return $this->db->update('locations', array('deleted' => 1));
	}

	/*
	Deletes a list of locations
	*/
	function delete_list($location_ids)
	{
		$this->db->where_in('location_id',$location_ids);
		return $this->db->update('locations', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find locations
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('locations');
		$this->db->like('location_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("location_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->location_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on locations
	*/
	function search($search)
	{
		$this->db->from('locations');
		$this->db->where("location_name LIKE '%".$this->db->escape_like_str($search)."%' and deleted=0");
		$this->db->order_by("location_name", "asc");
		return $this->db->get();	
	}
	
	public function get_location_description( $location_name )
	{
		if ( !$this->exists( $this->get_location_id($location_name)))
			return 0;
		
		$this->db->from('locations');
		$this->db->where('location_name',$location_name);
		return $this->db->get()->row()->description;
	}
	
	function update_location_description( $location_name, $description )
	{
		$this->db->where('location_name', $location_name);
		$this->db->update('locations', array('description' => $description));
	}
}
?>
