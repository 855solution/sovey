<?php
class Module extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function get_module_name($module_id)
	{
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return $this->lang->line($row->name_lang_key);
		}
		
		return $this->lang->line('error_unknown');
	}
	
	function get_module_desc($module_id)
	{
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return $this->lang->line($row->desc_lang_key);
		}
	
		return $this->lang->line('error_unknown');	
	}
	
	function get_all_modules()
	{
		$this->db->from('modules');
		$this->db->order_by("sort", "asc");
		return $this->db->get();		
	}
	
	function get_allowed_modules($person_id)
	{
		// $this->db->from('modules');
		// $this->db->join('permissions','permissions.module_id=modules.module_id');
		// $this->db->where("permissions.person_id",$person_id);
		// $this->db->order_by("sort", "asc");
		// return $this->db->get();		
		return $this->db->query("SELECT * FROM ospos_user_permission u
								INNER JOIN ospos_permission_detail p ON u.perm_id=p.perm_id
								INNER JOIN ospos_modules m ON p.module_id=m.module_id
								WHERE u.user_id = $person_id AND p.`view`=1 ORDER BY sort ASC");
	}
}
?>
