<?php
require_once("report.php");
class Detailed_sales_vinnumber extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array('summary' => array(
		$this->lang->line('reports_vinnumber'),
		//$this->lang->line('reports_date'), 
		$this->lang->line('reports_items_purchased'), 
		$this->lang->line('reports_subtotal'), 
		$this->lang->line('reports_total'), 
		//$this->lang->line('reports_profit'), 
		//$this->lang->line('reports_payment_amount'),
		'Cost Price',
		$this->lang->line('reports_payment_due'),
		'Balance'
		),
		
		'details' => array(
		$this->lang->line('reports_sale_id'),
		$this->lang->line('reports_itemid'),
		$this->lang->line('reports_date'), 
		$this->lang->line('reports_namekh'),
		//$this->lang->line('reports_nameen'),
		$this->lang->line('reports_vinnumber'), 
		$this->lang->line('reports_quantity_purchased'), 
		$this->lang->line('reports_subtotal'), 
		$this->lang->line('reports_total'), 
		//$this->lang->line('reports_profit'),
		$this->lang->line('reports_discount'),
		$this->lang->line('reports_payment_due'),
		$this->lang->line('reports_sold_by'), 
		$this->lang->line('reports_sold_to'), 

		)
		);		
	}
	
	public function getData(array $inputs)
	{
		$this->db->select('sale_date, 
		category as cat,
		count(category) as con_category, 
		sum(subtotal) as sum_subtotal,
		sum(total) as sum_total, 
		sum(profit) as sum_profit, 
		sum(discount_percent) as sum_discount_percent,
		payment_type,
		sum(dept) as sum_dept,
		vinnumbers.cost_price,
		
		'
		
		);
		
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		$this->db->join('vinnumbers', 'vinnumbers.vinnumber_name = items.category');
		$this->db->where('category !=""');
		$this->db->where('items.ishead',1);
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');

		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}

		//$this->db->group_by('sale_date');
		$this->db->group_by('cat');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();
		
		foreach($data['summary'] as $key=>$value)
		{
			$this->db->select('sale_id,sale_date, category,name,(select khmername_name from ospos_khmernames where khmername_id=ospos_items.khmername_id) as khname,sales_items_temp.item_id as itemid, 
			quantity_purchased, serialnumber, sales_items_temp.description, subtotal,total, profit, discount_percent,dept,
			employee.last_name as employee_name, 
			customer.last_name as customer_name');
			
			$this->db->from('sales_items_temp');
			
			$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
			$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id');
			$this->db->join('people as customer', 'sales_items_temp.customer_id = customer.person_id', 'left');
			
			$this->db->where('category = "'.$value['cat'].'"');
			//$this->db->where('sale_id = '.$value['sale_id']);
			
			$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
			
			if ($inputs['sale_type'] == 'sales')
			{
				$this->db->where('quantity_purchased > 0');
			}
			elseif ($inputs['sale_type'] == 'returns')
			{
				$this->db->where('quantity_purchased < 0');
			}
			
			//$this->db->group_by('category'); 
			
			$data['details'][$key] = $this->db->get()->result_array();
		}
		
		return $data;
	}
	
	
	
	public function getSummaryData(array $inputs)
	{
		/*
		$this->db->select(
		'sum(subtotal) as subtotal,
		sum(DISTINCT (ospos_vinnumbers.cost_price)) as sum_cost_price, 
		sum(total) as total, 
		sum(profit) as profit,
		sum(total_dues) as total_dues,
		
		');
		*/
		$this->db->select(
		'sum(subtotal) as total_sale,
		sum(DISTINCT (ospos_vinnumbers.cost_price)) as sum_cost_price, 
		(sum(subtotal)-sum(DISTINCT (ospos_vinnumbers.cost_price))) as total_balance, 
		
		sum(total_dues) as total_dues,
		
		');
		
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		$this->db->join('vinnumbers', 'vinnumbers.vinnumber_name = items.category');
		$this->db->where('items.ishead',1);
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		//$this->db->group_by('ospos_vinnumbers.cost_price'); 
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		return $this->db->get()->row_array();
	}
	
	
	
public function summaryDue(array $inputs)
	{
		
		$this->db->select(
		'
		
		sum(total_dues) as total_dues,
		
		');
		
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		$this->db->join('vinnumbers', 'vinnumbers.vinnumber_name = items.category');
		$this->db->where('items.ishead',1);
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		//$this->db->group_by('ospos_vinnumbers.cost_price'); 
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		return $this->db->get()->row_array();
	}

}
?>