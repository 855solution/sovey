<?php
require_once("report.php");
class Summary_sales extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(
		$this->lang->line('reports_date'), 
		$this->lang->line('reports_subtotal'), 
		$this->lang->line('reports_total'), 
		/*$this->lang->line('reports_tax'),*/ 
		$this->lang->line('reports_profit'),
		$this->lang->line('reports_payment_due')
		
		);
	}
	
	public function getData(array $inputs)
	{		
		$this->db->select('sale_date, sum(subtotal) as subtotal, sum(total) as total, sum(profit) as profit,
		sum(dept)as totaldues,payment_amount,dept');
		//$this->db->select('sale_date, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax,sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		$this->db->group_by('sale_date');
		$this->db->having('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		$this->db->order_by('sale_date');
		return $this->db->get()->result_array();
	}
	
	public function getSummaryData(array $inputs)
	{
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(profit) as profit');
		//$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax,sum(profit) as profit');
		$this->db->from('sales_items_temp');
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		return $this->db->get()->row_array();		
	}

	
	public function getCurrentProfit($inputs){
		$this->db->select(
		'sale_id, sale_date, 
		sum(quantity_purchased) as items_purchased, 
		CONCAT(employee.first_name," ",employee.last_name) as employee_name, 
		CONCAT(customer.first_name," ",customer.last_name) as customer_name, 
		sum(subtotal) as subtotal, 
		sum(total) as total, 
		sum(profit) as profit, 
		payment_type,dept,payment_amount, 
		comment', false
		);
		
		$this->db->from('sales_items_temp');
		$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id');
		$this->db->join('people as customer', 'sales_items_temp.customer_id = customer.person_id', 'left');
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		

		
		$this->db->group_by('sale_id');
		$this->db->order_by('sale_date');

		return $this->db->get()->result_array();	
		
	}
}
?>