<?php
require_once("report.php");
class Summary_payments extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(
		$this->lang->line('reports_payment_type'), 
		$this->lang->line('reports_total'));
	}
	
	public function getData(array $inputs)
	{
		$this->db->select(
		'sales_payments.payment_type, SUM(payment_amount) as payment_amount,SUM(dept)as sumdept', false);
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id=sales_payments.sale_id');
		$this->db->where('date(sale_time) BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('payment_amount > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('payment_amount < 0');
		}
		$this->db->group_by("payment_type");
		return $this->db->get()->result_array();
	}
	
	public function getSummaryData(array $inputs)
	{
		//$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		//$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(profit) as profit');
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total,sum(payment_amount-dept) as payment_amount');
		$this->db->from('sales_items_temp');
		$this->db->join('items', 'sales_items_temp.item_id = items.item_id');
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		if ($inputs['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($inputs['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		return $this->db->get()->row_array();
	}
	
	public function getCurrentProfit($inputs){
		$this->db->select(
		'sale_id, sale_date, 
		sum(quantity_purchased) as items_purchased, 
		CONCAT(employee.first_name," ",employee.last_name) as employee_name, 
		CONCAT(customer.first_name," ",customer.last_name) as customer_name, 
		sum(subtotal) as subtotal, 
		sum(total) as total, 
		sum(profit) as profit, 
		payment_type,dept,payment_amount, 
		comment', false
		);
		
		$this->db->from('sales_items_temp');
		$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id');
		$this->db->join('people as customer', 'sales_items_temp.customer_id = customer.person_id', 'left');
		$this->db->where('sale_date BETWEEN "'. $inputs['start_date']. '" and "'. $inputs['end_date'].'"');
		

		
		$this->db->group_by('sale_id');
		$this->db->order_by('sale_date');

		return $this->db->get()->result_array();	
		
	}
}
?>