<?php
class Partbarcodes extends CI_Model
{
	/*
	Determines if a given vinnumber_id is an vinnumber
	*/
	function exists( $vinnumber_id )
	{
		$this->db->from('ospos_partbarcode');
		$this->db->where('partbar_id',$vinnumber_id);
		$this->db->where('deleted',0);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the vinnumbers
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('ospos_partbarcode pb')
		->join('ospos_makes m','pb.make_id=m.make_id','inner')
		->join('ospos_models mo','pb.model_id=mo.model_id','inner');

		$this->db->where('pb.deleted',0);
		$this->db->order_by("pb.partbar_id", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function getdefaultimg($vinnumber_id){
            return $this->green->getValue("SELECT url_image FROM ospos_head_images where vin_number='$vinnumber_id' ORDER BY first_image ASC limit 1");
        }
	function getimage($vinnumber_id){
        $data=$this->db->query("SELECT * FROM ospos_head_images WHERE vin_number='$vinnumber_id' ORDER BY first_image ASC")->result();
       
        return $data;
    }
    function deleteimg($imgid){
            $this->db->where('vin_image_id',$imgid)->delete('ospos_head_images');
        }
	function count_all()
	{
		$this->db->from('ospos_partbarcode');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular vinnumber
	*/
	function get_info($vinnumber_id)
	{
		$this->db->from('ospos_partbarcode');
		
		$this->db->where('partbar_id',$vinnumber_id);

		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $vinnumber_id is NOT an vinnumber
			$vinnumber_obj=new stdClass();

			//Get all the fields from vinnumbers table
			$fields = $this->db->list_fields('vinnumbers');

			foreach ($fields as $field)
			{
				$vinnumber_obj->$field='';
			}

			return $vinnumber_obj;
		}
	}

	/*
	Get an vinnumber id given an vinnumber number
	*/
	function get_vinnumber_id($vinnumber_name)
	{
		$this->db->from('vinnumbers');
		$this->db->where('vinnumber_name',$vinnumber_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->vinnumber_id;
		}

		return false;
	}

	/*
	Gets information about multiple vinnumbers
	*/
	function get_multiple_info($vinnumber_ids)
	{
		$this->db->from('vinnumbers');
		$this->db->where_in('vinnumber_id',$vinnumber_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("vinnumber_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a vinnumber
	*/
	function save(&$vinnumber_data,$vinnumber_id=false)
	{
		if (!$vinnumber_id or !$this->exists($vinnumber_id))
		{
			if($this->db->insert('ospos_partbarcode',$vinnumber_data))
			{
				$vinnumber_data['partbar_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('partbar_id', $vinnumber_id);
		return $this->db->update('ospos_partbarcode',$vinnumber_data);
	}

	/*
	Updates multiple vinnumbers at once
	*/
	function update_multiple($vinnumber_data,$vinnumber_ids)
	{
		$this->db->where_in('vinnumber_id',$vinnumber_ids);
		return $this->db->update('vinnumbers',$vinnumber_data);
	}

	/*
	Deletes one vinnumber
	*/
	function delete($vinnumber_id)
	{
		$this->db->where('vinnumber_id', $vinnumber_id);
		return $this->db->update('vinnumbers', array('deleted' => 1));
	}

	/*
	Deletes a list of vinnumbers
	*/
	function delete_list($vinnumber_ids)
	{
		$this->db->where_in('partbar_id',$vinnumber_ids);
		return $this->db->update('ospos_partbarcode', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find vinnumbers
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('vinnumbers');
		$this->db->like('vinnumber_name', $search);
		$this->db->where('deleted',0);
		$this->db->order_by("vinnumber_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->vinnumber_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on vinnumbers
	*/
	function search($search)
	{
		$this->db->from('ospos_partbarcode pb')
		->join('ospos_makes m','pb.make_id=m.make_id','inner')
		->join('ospos_models mo','pb.model_id=mo.model_id','inner'); 
		$this->db->where("(pb.item_number LIKE '%".$this->db->escape_like_str($search)."%' OR pb.name LIKE '%".$this->db->escape_like_str($search)."%')");
		
		$this->db->where('pb.deleted',0);
		$this->db->order_by("pb.partbar_id", "asc");
		
		
		return $this->db->get();	
	}
	
	public function get_vinnumber_description( $vinnumber_name )
	{
		if ( !$this->exists( $this->get_vinnumber_id($vinnumber_name)))
			return 0;
		
		$this->db->from('vinnumbers');
		$this->db->where('vinnumber_name',$vinnumber_name);
		return $this->db->get()->row()->description;
	}
	
	function update_vinnumber_description( $vinnumber_name, $description )
	{
		$this->db->where('vinnumber_name', $vinnumber_name);
		$this->db->update('vinnumbers', array('description' => $description));
	}
	
	function getLastId_vinnumber(){
	
		
		$query = $this->db->query('SELECT max(vinnumber_id	) as maxid FROM ospos_vinnumbers');
		$row = $query->row();
		return $max_id = $row->maxid; 
		
		}
	function get_all_vinNumber($limit=10000, $offset=0)
	{
		$this->db->from('ospos_vinnumbers');
		$this->db->where('deleted',0);
		$this->db->order_by("vinnumber_id", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}	
	
	
function get_Vin_Info($vinnumberID)
	{
		//$this->db->from('ospos_vinnumbers');
		$this->db->from('vinnumbers');
		$this->db->where('vinnumber_id',$vinnumberID);
		$this->db->where('deleted',0);
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('ospos_vinnumbers');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	
	
	
	
function get_head_image_source($vinnumber)
	{
		$this->db->from('head_images');
		$this->db->where('vin_number', $vinnumber);
		$get_result=$this->db->get();
		return $get_result;
	}
	
function getImageName($imageid){
	$query = $this->db->query("SELECT url_image FROM ospos_head_images WHERE vin_image_id = '$imageid'");
	$row = $query->row();
	return $row->url_image;
}	

function delete_headImage_item($imageid){
	$query = $this->db->query("DELETE FROM ospos_head_images WHERE vin_image_id = '$imageid'");
	$row = $query->row();
	
}



}
?>
