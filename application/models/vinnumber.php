<?php
class Vinnumber extends CI_Model
{
	/*
	Determines if a given vinnumber_id is an vinnumber
	*/
	function count_have($khmerid){
		return $this->db->from('vinnumber_part')
						->where('khmername_id',$khmerid)
						->get()->row();
	}
	function get_vin_part($vinid,$khid){
		return $this->db->from('vinnumber_part')
						->where('vin_id',$vinid)
						->where('khmername_id',$khid)
						->get()->row();
	}
	function get_part_by_vin($vin){
		return $this->db->query("SELECT p.partid,k.khmername_name,k.english_name,p.khmernames_id,vp.status FROM ospos_vinnumbers v
								INNER JOIN ospos_part p ON v.model_id=p.model_id
														AND v.`year`=p.`year`
														AND v.trim=p.trim
														AND v.transmission=p.transmission
								LEFT JOIN ospos_khmernames k ON p.khmernames_id=k.khmername_id 
								LEFT JOIN ospos_vinnumber_part vp ON vp.vin_id=v.vinnumber_id
														AND vp.khmername_id=k.khmername_id
								WHERE v.vinnumber_id='$vin'
								GROUP BY p.khmernames_id")->result();
	}
	function getpartvin($vin){
		return $this->db->query("SELECT p.partid,p.part_number,k.khmername_name,k.english_name,p.khmernames_id,vp.status FROM ospos_vinnumbers v
								LEFT JOIN ospos_part p ON p.vinnumber_id=v.vinnumber_id
								LEFT JOIN ospos_khmernames k ON p.khmernames_id=k.khmername_id 
								LEFT JOIN ospos_vinnumber_part vp ON vp.vin_id=v.vinnumber_id
														AND vp.khmername_id=k.khmername_id
								WHERE v.vinnumber_id=$vin
								GROUP BY p.part_number")->result();
	}
	function exists( $vinnumber_id )
	{
		$this->db->from('vinnumbers');
		$this->db->join('colors', 'colors.color_id = vinnumbers.external_color','left');
		$this->db->join('models', 'models.model_id = vinnumbers.model_id','left');
		$this->db->where('vinnumbers.deleted',0);
		$this->db->where('vinnumber_id',$vinnumber_id);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the vinnumbers
	*/
	function get_all($limit=20, $offset=0)
	{
		$this->db->select("*,ex.color_name as ex_co,in_c.color_name as in_co");
		$this->db->from('vinnumbers');
		$this->db->join('colors ex', 'ex.color_id = vinnumbers.external_color','left');
		$this->db->join('colors in_c', 'in_c.color_id = vinnumbers.interior_color','left');
		$this->db->join('models', 'models.model_id = vinnumbers.model_id','left');
		$this->db->join('makes','makes.make_id = vinnumbers.make_id','left');
		$this->db->where('vinnumbers.deleted',0);
		$this->db->order_by("vinnumber_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function getdefaultimg($vinnumber_id){
            return $this->green->getValue("SELECT url_image FROM ospos_head_images where vin_number='$vinnumber_id' ORDER BY first_image ASC limit 1");
        }
	function getimage($vinnumber_id){
        $data=$this->db->query("SELECT * FROM ospos_head_images WHERE vin_number='$vinnumber_id' ORDER BY first_image ASC")->result();
       
        return $data;
    }
    function deleteimg($imgid){
            $this->db->where('vin_image_id',$imgid)->delete('ospos_head_images');
        }
	function count_all()
	{
		$this->db->from('vinnumbers');
		$this->db->join('colors', 'colors.color_id = vinnumbers.external_color','left');
		$this->db->join('models', 'models.model_id = vinnumbers.model_id','left');
		$this->db->where('vinnumbers.deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular vinnumber
	*/
	function get_info($vinnumber_id)
	{
		$this->db->from('vinnumbers');
		$this->db->join('colors', 'colors.color_id = vinnumbers.external_color','left');
		$this->db->join('models', 'models.model_id = vinnumbers.model_id','left');
		$this->db->where('vinnumber_id',$vinnumber_id);
		$this->db->where('vinnumbers.deleted',0);

		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $vinnumber_id is NOT an vinnumber
			$vinnumber_obj=new stdClass();

			//Get all the fields from vinnumbers table
			$fields = $this->db->list_fields('vinnumbers');

			foreach ($fields as $field)
			{
				$vinnumber_obj->$field='';
			}

			return $vinnumber_obj;
		}
	}

	/*
	Get an vinnumber id given an vinnumber number
	*/
	function get_vinnumber_id($vinnumber_name)
	{
		$this->db->from('vinnumbers');
		$this->db->where('vinnumber_name',$vinnumber_name);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->vinnumber_id;
		}

		return false;
	}

	/*
	Gets information about multiple vinnumbers
	*/
	function get_multiple_info($vinnumber_ids)
	{
		$this->db->from('vinnumbers');
		$this->db->where_in('vinnumber_id',$vinnumber_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("vinnumber_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a vinnumber
	*/
	function save(&$vinnumber_data,$vinnumber_id=false)
	{
		if (!$vinnumber_id or !$this->exists($vinnumber_id))
		{
			if($this->db->insert('vinnumbers',$vinnumber_data))
			{
				$vinnumber_data['vinnumber_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('vinnumber_id', $vinnumber_id);
		return $this->db->update('vinnumbers',$vinnumber_data);
	}

	/*
	Updates multiple vinnumbers at once
	*/
	function update_multiple($vinnumber_data,$vinnumber_ids)
	{
		$this->db->where_in('vinnumber_id',$vinnumber_ids);
		return $this->db->update('vinnumbers',$vinnumber_data);
	}

	/*
	Deletes one vinnumber
	*/
	function delete($vinnumber_id)
	{
		$this->db->where('vinnumber_id', $vinnumber_id);
		return $this->db->update('vinnumbers', array('deleted' => 1));
	}

	/*
	Deletes a list of vinnumbers
	*/
	function delete_list($vinnumber_ids)
	{
		$this->db->where_in('vinnumber_id',$vinnumber_ids);
		return $this->db->update('vinnumbers', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find vinnumbers
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		$this->db->from('vinnumbers');
		$this->db->like('vinnumber_name', $search,'after');
		$this->db->where('deleted',0);
		$this->db->order_by("vinnumber_name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=$row->vinnumber_name;
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	

	/*
	Preform a search on vinnumbers
	*/
	function search($search)
	{
		$this->db->from('vinnumbers');
		$this->db->join('colors', 'colors.color_id = vinnumbers.external_color','left');
		$this->db->join('models', 'models.model_id = vinnumbers.model_id','left');
		$this->db->where('vinnumbers.deleted',0);
		$this->db->where("vinnumber_name LIKE '%".$this->db->escape_like_str($search)."%'");		
		$this->db->order_by("vinnumber_name", "asc");
		return $this->db->get();	
	}
	
	
	public function get_vinnumber_description( $vinnumber_name )
	{
		if ( !$this->exists( $this->get_vinnumber_id($vinnumber_name)))
			return 0;
		
		$this->db->from('vinnumbers');
		$this->db->where('vinnumber_name',$vinnumber_name);
		return $this->db->get()->row()->description;
	}
	
	function update_vinnumber_description( $vinnumber_name, $description )
	{
		$this->db->where('vinnumber_name', $vinnumber_name);
		$this->db->update('vinnumbers', array('description' => $description));
	}
	
	function getLastId_vinnumber(){
	
		
		$query = $this->db->query('SELECT max(vinnumber_id	) as maxid FROM ospos_vinnumbers');
		$row = $query->row();
		return $max_id = $row->maxid; 
		
		}
	function get_all_vinNumber($limit=20, $offset=0)
	{
		$this->db->from('ospos_vinnumbers');
		$this->db->where('deleted',0);
		$this->db->order_by("vinnumber_id", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}	
	
	
	function get_Vin_Info($vinnumberID)
		{
			//$this->db->from('ospos_vinnumbers');
			$this->db->from('vinnumbers');
			$this->db->where('vinnumber_id',$vinnumberID);
			$this->db->where('deleted',0);
			
			$query = $this->db->get();

			if($query->num_rows()==1)
			{
				return $query->row();
			}
			else
			{
				//Get empty base parent object, as $item_id is NOT an item
				$item_obj=new stdClass();

				//Get all the fields from items table
				$fields = $this->db->list_fields('ospos_vinnumbers');

				foreach ($fields as $field)
				{
					$item_obj->$field='';
				}

				return $item_obj;
			}
		}
		
		
		
		
		
	function get_head_image_source($vinnumber)
		{
			$this->db->from('head_images');
			$this->db->where('vin_number', $vinnumber);
			$get_result=$this->db->get();
			return $get_result;
		}
		
	function getImageName($imageid){
		$query = $this->db->query("SELECT url_image FROM ospos_head_images WHERE vin_image_id = '$imageid'");
		$row = $query->row();
		return $row->url_image;
	}	

	function delete_headImage_item($imageid){
		$query = $this->db->query("DELETE FROM ospos_head_images WHERE vin_image_id = '$imageid'");
		$row = $query->row();
		
	}
	function get_sale_item($vin_id,$search=null){
		header("content-type: text/html; charset=UTF-8");

		$this->db->select('*,ai.partplacement_id as new_p')
				->from('vinnumber_sale vs')
				->where('vs.vinnumber_id',$vin_id)
				->join('v_select_all_items ai','vs.item_id=ai.item_id AND vs.is_new=ai.is_new','inner')
				->join('sales_items si','ai.item_id=si.item_id AND si.is_return=0','inner')
				->join('khmernames kh','ai.khmername_id=kh.khmername_id','left')
				->join('sales sa','si.sale_id=sa.sale_id')
				->join('people pe','sa.customer_id=pe.person_id','left')
				->join('partplacements pa','ai.partplacement_id=pa.partplacement_id','left');
		if ($search!='') {
			$this->db->where('(ai.name LIKE "%'.$search.'%" OR ai.barcode LIKE "'.$search.'%"
								OR kh.khmername_name LIKE "%'.$search.'%" OR sa.invoiceid LIKE "'.$search.'%"
								OR pe.first_name LIKE "'.$search.'%" OR pe.last_name LIKE "'.$search.'%"
								OR pa.partplacement_name LIKE "'.$search.'%" OR ai.partplacement_id LIKE "'.$search.'%")');
		}
		// var_dump($qdata);die();
		$qdata = $this->db->get()->result();

		return $qdata;
	}
	function get_invoice_items($in_id){
		return $this->db->select("*,CONCAT((emp.first_name),(' '),(emp.last_name)) AS empname,CONCAT((cus.first_name),(' '),(cus.last_name)) AS cusname,
									pa.partplacement_name AS pname,ai.partplacement_id AS pnamenew")
				->from('sales sa')
				->where('invoiceid',$in_id)
				->join('sales_items si','sa.sale_id=si.sale_id','inner')
				->join('v_select_all_items ai','si.item_id=ai.item_id AND si.is_new=ai.is_new','inner')
				->join('models mo','ai.model_id=mo.model_id','left')
				->join('makes ma','ai.make_id=ma.make_id','left')
				->join('partplacements pa','ai.partplacement_id=pa.partplacement_id','left')
				->join('people cus','sa.customer_id=cus.person_id','left')
				->join('people emp','sa.employee_id=emp.person_id','left')
				->where('is_return',0)
				->get()
				->result();

	}
	function get_all_vin(){
		return $this->db->from('vinnumbers')
						->where('deleted',0)
						->get()
						->result();
	}
	function getnumsearch_old($field,$search,$make_id,$model_id,$year,$in_col,$ex_col,$yf,$yt,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$df,$dt){
        $where='';
        if($make_id!=''){
            $where.=" AND i.make_id='".$make_id."'";
        }elseif ($make_id=='other') {
        	$where.=" AND i.make_id='' OR i.make_id=0 OR i.make_id IS NULL";
        }
        if($model_id!=''){
            $where.=" AND i.model_id='".$model_id."'";
        }elseif ($model_id=='other') {
        	$where.=" AND i.model_id='' OR i.model_id=0 OR i.model_id IS NULL";
        }
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
            	if ($arryear[$i]!='' && $arryear[$i]!='_') {
            		
            		if ($arryear[$i]=='other') {
            			$where_year.=" i.year='' OR i.year=0 OR i.year IS NULL";
            		}else{
                    	$where_year.="i.year='".$arryear[$i]."'";
            		}
                    if($i<count($arryear)-2)
                        $where_year.=' OR ';
                }
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where.=" AND i.year BETWEEN $yf AND $yt";
        }

        if ($df!='' && $dt!='') {
        	$where.=" AND i.date_arrived BETWEEN '$df' AND '$dt'";
        }

        if($in_col!='' && $in_col!='_'){
            $in_arrcol=explode('_', $in_col);
            $where_in_color=' AND(';
            for ($i=0; $i < count($in_arrcol) ; $i++) { 
            	if ($in_arrcol[$i]!='' && $in_arrcol[$i]!='_') {
            		# code...
            		if ($in_arrcol[$i]=='other') {
            			$where_in_color.="i.interior_color='' OR i.interior_color=0 OR i.interior_color IS NULL";
            		}else{
                    	$where_in_color.="i.interior_color='".$in_arrcol[$i]."'";
            		}
                    if($i<count($in_arrcol)-2)
                        $where_in_color.=' OR ';
            	}
            }
            $where_in_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_in_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($ex_col!='' && $ex_col!='_'){
            $ex_arrcol=explode('_', $ex_col);
            $where_ex_color=' AND(';
            for ($i=0; $i < count($ex_arrcol) ; $i++) {
            	if ($ex_arrcol[$i]!='' && $ex_arrcol[$i]!='_') {
            		
            		if ($ex_arrcol[$i]=='other') {
            			$where_ex_color.="i.external_color='' OR i.external_color=0 OR i.external_color IS NULL";
            		}else{
                    $where_ex_color.="i.external_color='".$ex_arrcol[$i]."'";

            		}
                    if($i<count($ex_arrcol)-2)
                        $where_ex_color.=' OR ';
            	}
                
            }
            $where_ex_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_ex_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($body_id!='' && $body_id!='_'){
            $bo_arr=explode('_', $body_id);
            $where_body=' AND(';
            for ($i=0; $i < count($bo_arr) ; $i++) { 
            	if ($bo_arr[$i]!='' && $bo_arr[$i]!='_') {
            		# code...
            		if ($bo_arr[$i]=='other') {
            			$where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
            		}else{
                    $where_body.="i.body='".$bo_arr[$i]."'";

            		}
                    if($i<count($bo_arr)-2)
                        $where_body.=' OR ';
            	}
                
            }
            $where_body.=')';
            // $s_year=$_GET['y'];
            $where.=$where_body;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($engine_id!='' && $engine_id!='_'){
            $en_arr=explode('_', $engine_id);
            $where_engine=' AND(';
            for ($i=0; $i < count($en_arr) ; $i++) { 
            	if ($en_arr[$i]!='' && $en_arr!='_') {
            		# code...
            		if ($en_arr[$i]=='other') {
            			$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
            		}else{
                    $where_engine.="i.engine='".$en_arr[$i]."'";

            		}
                    if($i<count($en_arr)-2)
                        $where_engine.=' OR ';
            	}
                
            }
            $where_engine.=')';
            // $s_year=$_GET['y'];
            $where.=$where_engine;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($fuel_id!='' && $fuel_id!='_'){
            $fuel_arr=explode('_', $fuel_id);
            $where_fuel=' AND(';
            for ($i=0; $i < count($fuel_arr) ; $i++) { 
            	if ($fuel_arr[$i]!='' && $fuel_arr[$i]!='_') {
            		# code...
           			if ($fuel_arr[$i]=='other') {
           				$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
           			}else{
                    $where_fuel.="i.fuel='".$fuel_arr[$i]."'";

           			}
                    if($i<count($fuel_arr)-2)
                        $where_fuel.=' OR ';
            	}
                
            }
            $where_fuel.=')';
            // $s_year=$_GET['y'];
            $where.=$where_fuel;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trans_id!='' && $trans_id!='_'){
            $trans_arr=explode('_', $trans_id);
            $where_trans=' AND(';
            for ($i=0; $i < count($trans_arr) ; $i++) { 
            	if ($trans_arr[$i]!='' && $trans_arr[$i]!='_') {
            		# code...
           			if ($trans_arr[$i]=='other') {
           				$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";
           			}else{
                    $where_trans.="i.transmission='".$trans_arr[$i]."'";

           			}
                    if($i<count($trans_arr)-2)
                        $where_trans.=' OR ';
            	}
                
            }
            $where_trans.=')';
            // $s_year=$_GET['y'];
            $where.=$where_trans;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trim_id!='' && $trim_id!='_'){
            $trim_arr=explode('_', $trim_id);
            $where_trim=' AND(';
            for ($i=0; $i < count($trim_arr) ; $i++) {
            	if ($trim_arr[$i]!='' && $trim_arr[$i]!='_') {
            	 	# code...
           			if ($trim_arr[$i]=='other') {
           				$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
           			}else{

                    $where_trim.="i.trim='".$trim_arr[$i]."'";
           			}
                    if($i<count($trim_arr)-2)
                        $where_trim.=' OR ';
            	 } 
                
            }
            $where_trim.=')';
            // $s_year=$_GET['y'];
            $where.=$where_trim;
            // $where.=" AND i.color_id='".$color_id."'";
        }
     
        return $this->db->query("SELECT count(i.$field) as count
		        FROM ospos_vinnumbers i 
		        LEFT JOIN `ospos_models` ON `ospos_models`.`model_id` = `i`.`model_id`
				LEFT JOIN `ospos_makes` ON `ospos_makes`.`make_id` = `i`.`make_id` 
				LEFT JOIN `ospos_colors` in_c ON `in_c`.`color_id` = `i`.`interior_color`
				LEFT JOIN `ospos_colors` ex_c ON `ex_c`.`color_id` = `i`.`external_color`
				LEFT JOIN ospos_body ON ospos_body.body_id=i.body
				LEFT JOIN ospos_engine ON ospos_engine.engine_id=i.engine
				LEFT JOIN ospos_fuel ON ospos_fuel.fuel_id=i.fuel
				LEFT JOIN ospos_transmission ON ospos_transmission.trans_id=i.transmission
				LEFT JOIN ospos_trim ON ospos_trim.trim_id=i.trim
	            WHERE i.deleted='0' 
	            AND (i.vinnumber_name LIKE '%$search%' 
					OR ospos_makes.make_name LIKE '%$search%' 
					OR ospos_models.model_name LIKE '%$search%' 
					OR in_c.color_name LIKE '%$search%'
					OR ex_c.color_name LIKE '%$search%'
					OR i.year like '%$search%' 
					OR ospos_body.body_name LIKE '%$search%'
					OR ospos_engine.engine_name LIKE '%$search%'
					OR ospos_fuel.fule_name LIKE '%$search%'
					OR ospos_transmission.transmission_name LIKE '%$search%'
					OR ospos_trim.trim_name LIKE '%$search%'
					OR i.vainnumber LIKE '%$search%'
					-- OR i.description LIKE '%$search%'
					OR i.cost_price LIKE '%$search%')
	        	{$where} ")->row()->count;
    }
    
    function getnumsearch_in_old($field,$search,$make_id,$model_id,$year,$color_id,$yf,$yt){
        $where='';
        if($make_id!=''){
            $where.=" AND i.make_id='".$make_id."'";
        }
        if($model_id!=''){
            $where.=" AND i.model_id='".$model_id."'";
        }
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
                    $where_year.="i.year='".$arryear[$i]."'";
                    if($i<count($arryear)-1)
                        $where_year.=' OR ';
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where.=" AND i.year BETWEEN $yf AND $yt";
        }
        
        if($color_id!='' && $color_id!='_'){
            $arrcol=explode('_', $color_id);
            $where_color=' AND(';
            for ($i=0; $i < count($arrcol) ; $i++) { 
            		
                    $where_color.="i.interior_color='".$arrcol[$i]."'";
                    if($i<count($arrcol)-1)
                        $where_color.=' OR ';
                
            }
            $where_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }

     
        return $this->db->query("SELECT count(i.$field) as count
             FROM ospos_vinnumbers i 
             LEFT JOIN `ospos_models` ON `ospos_models`.`model_id` = `i`.`model_id`
			LEFT JOIN `ospos_makes` ON `ospos_makes`.`make_id` = `i`.`make_id` 
			LEFT JOIN `ospos_colors` in_c ON `in_c`.`color_id` = `i`.`interior_color`
            WHERE i.deleted='0' 
            AND (i.vinnumber_name LIKE '%$search%' 
				OR ospos_makes.make_name LIKE '%$search%' 
				OR ospos_models.model_name LIKE '%$search%' 
				OR in_c.color_name LIKE '%$search%'
				OR i.year like '%$search%' 
				OR i.description LIKE '%$search%'
				OR i.cost_price LIKE '%$search%')
            {$where} ")->row()->count;
    }
    function getnumsearch_ex_old($field,$search,$make_id,$model_id,$year,$color_id,$in_color_id,$yf,$yt){
        $where='';
        if($make_id!=''){
            $where.=" AND i.make_id='".$make_id."'";
        }
        if($model_id!=''){
            $where.=" AND i.model_id='".$model_id."'";
        }
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
                    $where_year.="i.year='".$arryear[$i]."'";
                    if($i<count($arryear)-1)
                        $where_year.=' OR ';
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where.=" AND i.year BETWEEN $yf AND $yt";
        }
        if($in_color_id!='' && $in_color_id!='_'){
            $in_arrcol=explode('_', $in_color_id);
            $in_where_color=' AND(';
            for ($i=0; $i < count($in_arrcol) ; $i++) { 
        		if($in_arrcol[$i]!='_' && $in_arrcol[$i]!=''){
					$in_where_color.="i.interior_color='".$in_arrcol[$i]."'";
					if (count($in_arrcol)-2>$i) {
						$in_where_color.=" OR ";
					}
	           	}
                    // $in_where_color.="i.interior_color='".$in_arrcol[$i]."'";
                    // if($i<count($in_arrcol)-1)
                    //     $in_where_color.=' OR ';
                
            }
            $in_where_color.=')';
            // $s_year=$_GET['y'];
            $where.=$in_where_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($color_id!='' && $color_id!='_'){
            $arrcol=explode('_', $color_id);
            $where_color=' AND(';
            for ($i=0; $i < count($arrcol) ; $i++) { 
            		
                    $where_color.="i.external_color='".$arrcol[$i]."'";
                    if($i<count($arrcol)-1)
                        $where_color.=' OR ';
                
            }
            $where_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
      	// echo $where;
        return $this->db->query("SELECT count(i.$field) as count
             FROM ospos_vinnumbers i 
             LEFT JOIN `ospos_models` ON `ospos_models`.`model_id` = `i`.`model_id`
			LEFT JOIN `ospos_makes` ON `ospos_makes`.`make_id` = `i`.`make_id` 
			LEFT JOIN `ospos_colors` ex_c ON `ex_c`.`color_id` = `i`.`external_color`
            WHERE i.deleted='0' 
            AND (i.vinnumber_name LIKE '%$search%' 
				OR ospos_makes.make_name LIKE '%$search%' 
				OR ospos_models.model_name LIKE '%$search%' 
				OR ex_c.color_name LIKE '%$search%'
				OR i.year like '%$search%' 
				OR i.description LIKE '%$search%'
				OR i.cost_price LIKE '%$search%')
            {$where} ")->row()->count;
    }
function getnumsearch_other_old($field,$search,$make_id,$model_id,$year,$in_col,$ex_col,$yf,$yt,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$df,$dt){
        $where='';
        if($make_id!='' && $make_id!='other'){
            $where.=" AND i.make_id='".$make_id."'";
        }elseif($make_id=='other'){
        	$where.=" AND (i.make_id='' OR i.make_id=0 OR i.make_id IS NULL)";
        }

        if($model_id!='' && $model_id!='other'){
            $where.=" AND i.model_id='".$model_id."'";
        }elseif($model_id=='other'){
        	$where.=" AND (i.model_id='' OR i.model_id=0 OR i.model_id IS NULL)";
        }
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            	// var_dump($arryear);
            for ($i=0; $i < count($arryear) ; $i++) { 
            	if ($arryear[$i]!='' && $arryear[$i]!='_') {
            		# code...
            		if ($arryear[$i]=='other') {
                    	$where_year.="i.year='' OR i.year=0 OR i.year IS NULL";
            		}else{
                    	$where_year.="i.year='".$arryear[$i]."'";

            		}
            		if($i<count($arryear)-2)
                        $where_year.=' OR ';
            	}
                    
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where.=" AND i.year BETWEEN $yf AND $yt";
        }

        if ($df!='' && $dt!='') {
        	$where.=" AND i.date_arrived BETWEEN '$df' AND '$dt'";
        }
        if($in_col!='' && $in_col!='_'){
            $in_arrcol=explode('_', $in_col);
            $where_in_color=' AND(';
            	// var_dump($in_arrcol);
            for ($i=0; $i < count($in_arrcol) ; $i++) { 
            	if ($in_arrcol[$i]!='' && $in_arrcol[$i]!='_') {
            		# code...
            		if ($in_arrcol[$i]=='other') {
            			$where_in_color.="i.interior_color='' OR i.interior_color=0 OR i.interior_color IS NULL";
            		}else{
                    	$where_in_color.="i.interior_color='".$in_arrcol[$i]."'";

            		}
            		if($i<count($in_arrcol)-2)
                        $where_in_color.=' OR ';
            	}
                    
                
            }
            $where_in_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_in_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($ex_col!='' && $ex_col!='_'){
            $ex_arrcol=explode('_', $ex_col);
            $where_ex_color=' AND(';
            for ($i=0; $i < count($ex_arrcol) ; $i++) { 
            	if ($ex_arrcol[$i]!='' && $ex_arrcol[$i]!='_') {
            		# code...
            		if ($ex_arrcol[$i]=='other') {
            		$where_ex_color.="i.external_color='' OR i.external_color=0 OR i.external_color IS NULL";
            		}else{
                    $where_ex_color.="i.external_color='".$ex_arrcol[$i]."'";

            		}
            		if($i<count($ex_arrcol)-2)
                        $where_ex_color.=' OR ';
            	}
                    
                
            }
            $where_ex_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_ex_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        
        if($body_id!='' && $body_id!='_'){
            $bo_arr=explode('_', $body_id);
            $where_body=' AND(';
            for ($i=0; $i < count($bo_arr) ; $i++) {
            	if ($bo_arr[$i]!='' && $bo_arr[$i]!='_') {
            		if ($bo_arr[$i]=='other') {
                    $where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
            		}else{
                    $where_body.="i.body='".$bo_arr[$i]."'";

            		}
            		if(count($bo_arr)-2>$i)
                        $where_body.=' OR ';
            	}
            		
                    
                
            }
            $where_body.=')';
            // $s_year=$_GET['y'];
            $where.=$where_body;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($engine_id!='' && $engine_id!='_'){
            $en_arr=explode('_', $engine_id);
            $where_engine=' AND(';
            for ($i=0; $i < count($en_arr) ; $i++) { 
            	if ($en_arr[$i]!='' && $en_arr[$i]!='_') {
            		# code...
            		if ($en_arr[$i]=='other') {
            		$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
            		}else{
                    $where_engine.="i.engine='".$en_arr[$i]."'";

            		}
            		if($i<count($en_arr)-2)
                        $where_engine.=' OR ';
                    
            	}
                
            }
            $where_engine.=')';
            // $s_year=$_GET['y'];
            $where.=$where_engine;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($fuel_id!='' && $fuel_id!='_'){
            $fuel_arr=explode('_', $fuel_id);
            $where_fuel=' AND(';
            for ($i=0; $i < count($fuel_arr) ; $i++) { 
            	if ($fuel_arr[$i]!='' && $fuel_arr[$i]!='_') {
            		# code...
           			if ($fuel_arr[$i]=='other') {
           			$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
           			}else{
                    $where_fuel.="i.fuel='".$fuel_arr[$i]."'";

           			}
           			if($i<count($fuel_arr)-2)
                        $where_fuel.=' OR ';
            	}
                    
                
            }
            $where_fuel.=')';
            // $s_year=$_GET['y'];
            $where.=$where_fuel;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trans_id!='' && $trans_id!='_'){
            $trans_arr=explode('_', $trans_id);
            $where_trans=' AND(';
            for ($i=0; $i < count($trans_arr) ; $i++) { 
            	if ($trans_arr[$i]!='' && $trans_arr[$i]!='_') {
            		# code...
           			if ($trans_arr[$i]=='other') {
           			$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";

           			}else{
                    $where_trans.="i.transmission='".$trans_arr[$i]."'";

           			}
           			if($i<count($trans_arr)-2)
                        $where_trans.=' OR ';
            	}
                    
                
            }
            $where_trans.=')';
            // $s_year=$_GET['y'];
            $where.=$where_trans;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trim_id!='' && $trim_id!='_'){
            $trim_arr=explode('_', $trim_id);
            $where_trim=' AND(';
            for ($i=0; $i < count($trim_arr) ; $i++) { 
            	if ($trim_arr[$i]!='' && $trim_arr[$i]!='_') {
            		if ($trim_arr[$i]=='other') {
            		$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
            		}else{
                    $where_trim.="i.trim='".$trim_arr[$i]."'";
            		}
            		if($i<count($trim_arr)-2)
                        $where_trim.=' OR ';
            	}
                    
            }
            $where_trim.=')';
            // $s_year=$_GET['y'];
            $where.=$where_trim;
            // $where.=" AND i.color_id='".$color_id."'";
        }
     
        return $this->db->query("SELECT count(*) as count
		        FROM ospos_vinnumbers i 
		        LEFT JOIN `ospos_models` ON `ospos_models`.`model_id` = `i`.`model_id`
				LEFT JOIN `ospos_makes` ON `ospos_makes`.`make_id` = `i`.`make_id` 
				LEFT JOIN `ospos_colors` in_c ON `in_c`.`color_id` = `i`.`interior_color`
				LEFT JOIN `ospos_colors` ex_c ON `ex_c`.`color_id` = `i`.`external_color`
				LEFT JOIN ospos_body ON ospos_body.body_id=i.body
				LEFT JOIN ospos_engine ON ospos_engine.engine_id=i.engine
				LEFT JOIN ospos_fuel ON ospos_fuel.fuel_id=i.fuel
				LEFT JOIN ospos_transmission ON ospos_transmission.trans_id=i.transmission
				LEFT JOIN ospos_trim ON ospos_trim.trim_id=i.trim
	            WHERE i.deleted='0' 
	            AND (i.vinnumber_name LIKE '%$search%' 
					OR ospos_makes.make_name LIKE '%$search%' 
					OR ospos_models.model_name LIKE '%$search%' 
					OR in_c.color_name LIKE '%$search%'
					OR ex_c.color_name LIKE '%$search%'
					OR i.year like '%$search%' 
					OR ospos_body.body_name LIKE '%$search%'
					OR ospos_engine.engine_name LIKE '%$search%'
					OR ospos_fuel.fule_name LIKE '%$search%'
					OR ospos_transmission.transmission_name LIKE '%$search%'
					OR ospos_trim.trim_name LIKE '%$search%'
					OR i.vainnumber LIKE '%$search%'

					-- OR i.description LIKE '%$search%'
					OR i.cost_price LIKE '%$search%')
				AND (i.$field IS NULL OR i.$field = '' OR i.$field=0)
	        	{$where} ")->row()->count;
    }
    function getnumsearch($field,$where){
        // $where='';
        $where=str_replace("AND()","",$where);

        $group=" GROUP BY $field";

         $data = $this->db->query("SELECT count(i.$field) as count,i.$field FROM ospos_vinnumbers as i 
        	WHERE i.deleted=0 
        	-- AND i.quantity>0 
        	AND i.$field<>'' 
        	AND i.$field IS NOT NULL 
        	{$where} 
        	{$group}")->result();
        $arr=array();
       
        foreach ($data as $row) {
        	$arr[$row->$field]=$row->count;# code...
        }
        return $arr;

    }
    function getnumsearch_other($field,$where){
    	$where=str_replace("AND()","",$where);
        return $this->db->query("SELECT count(*) as count FROM ospos_vinnumbers i 
    								WHERE i.deleted=0 
    								-- AND i.quantity>0 
    								AND (i.$field IS NULL OR i.$field = '' OR i.$field=0) 
    								{$where}")->row()->count;
    }






}
?>
