<?php
class Api_scan_item extends CI_Model
{

	public function scan($data)
	{
		$json = json_decode($data);
	        $json = $json->json_send;
		$api = $this->load->database('api', TRUE);
		
		if (isset($json->location_name)) {
			
			$api->select('*');
			$api->from('locations');
			$api->where('location_name', $json->location_name);
			$api->where('deleted', 0);
			$query    = $api->get();
			$location = $query->row();
			
		} else {
			echo json_encode(array("status" => 3, "message" => "error"));
		}
		if ($location > 0) {
			$arr1   = [];
			$arr2   = [];
			$item   = [];
			$additional_item = [];
			$lose_item = [];
			
			$api->select('barcode');
			$api->from('items');
			$api->where('location_id', $location->location_id);
			$api->where('barcode IS NOT NULL');
			$result   = $api->get();
			$get_data = $result->result_array();
			$item_id = [];
			
			
			foreach ($get_data as  $value) {
				array_push($arr1, $value['barcode']);
			}
			
			foreach (json_decode($json->list_item) as $value) {
				array_push($arr2, $value->item);
				
			}
			
			if (count($arr1) > count($arr2))
			  	$item = array_intersect($arr1, $arr2);
			else
				$item = array_intersect($arr2, $arr1);
				
			
			
			if (count($item) > count($arr2))
				$additional_item = array_diff($item, $arr2);
			else
				$additional_item  = array_diff($arr2, $item);
				
		
				
			if (count($item) > count($arr1))
				$lose_item = array_diff($item, $arr1);
			else
				$lose_item = array_diff($arr1, $item);
			
			$arr_v_lost_item = [];
			$arr_v_add_item = [];
			
			foreach($lose_item as $v_lost_item) {
				array_push($arr_v_lost_item, (int)substr($v_lost_item, 6));
			}
			
			foreach($additional_item as $v_add_item) {
				array_push($arr_v_add_item, (int)substr($v_add_item, 6));
			}
			
			if (count($additional_item) > 0 && count($lose_item) > 0) {
				$this->update($arr_v_add_item,  $location->location_id);
				$this->update_location_null($arr_v_lost_item);
                                echo json_encode(array("status" => 3, "message" => count($additional_item)." Item(s) in addition and Lose Item(s) ".count($lose_item), "data" => array('additional_data'=>array_values($additional_item),"lose_data" => array_values($lose_item))));
			}

			elseif (count($additional_item) == 0 && count($lose_item) == 0) {
				echo json_encode(array("status" => 1, "message" => "Additional Item(s): 0 and Lose Item(s): 0", "data" => array('additional_data' => array_values($additional_item),"lose_data" => array_values($lose_item))));
				
			} 
			
			else {
				if (count($additional_item) > 0){
					$this->update($arr_v_add_item,  $location->location_id);
					 echo json_encode(array("status" => 2, "message" => count($additional_item)." Item(s) in addition", "data" => array('additional_data'=>array_values($additional_item),"lose_data" => array_values($lose_item))));
				}
				else if (count($lose_item) > 0){
					$this->update_location_null($arr_v_lost_item);
					echo json_encode(array("status" => 0, "message" => "Lose Item(s)", "data" => array('additional_data'=>array_values($additional_item),"lose_data" => array_values($lose_item))));
				}
			}
			
				
		} 
		
	}

	public function update($item, $location_id)
	{
		
		$api = $this->load->database('api', TRUE);
	
		$data = array(
			   'location_id' => $location_id
			);
			
		$api->where_in('item_id', $item);
		$update = $api->update('items', $data);
		if ($update)
			return true;
		else 
			return false;
		
	}

	public function update_location_null($item)
	{
		$api = $this->load->database('api', TRUE);
		$data = array(
			   'location_id' => null
			);
			
		$api->where_in('item_id', $item);
		$update = $api->update('items', $data);
		if ($update)
			return true;
		else 
			return false;
		
	}
	
}
?>