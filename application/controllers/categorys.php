<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Categorys extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('categorys');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/categorys/index');
		$config['total_rows'] = $this->Category->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_categorys_manage_table( $this->Category->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('categorys/manage',$data);
	}
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_categorys_manage_table_data_rows($this->Category->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Category->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$category_id = $this->input->post('row_id');
		$data_row=get_category_data_row($this->Category->get_info($category_id),$this);
		echo $data_row;
	}

	function view($category_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['category_info']=$this->Category->get_info($category_id);

		$this->load->view("categorys/form",$data);
	}
	
	function save($category_id=-1)
	{
		$this->permission_lib->checkPermission();

		$category_data = array(
		'category_name'=>$this->input->post('category_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Category->save( $category_data, $category_id ) )
		{
			//New category
			if($category_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('categorys_successful_adding').' '.
				$category_data['category_name'],'category_id'=>$category_data['category_id']));
				$category_id = $category_data['category_id'];
			}
			else //previous category
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('categorys_successful_updating').' '.
				$category_data['category_name'],'category_id'=>$category_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('categorys_error_adding_updating').' '.
			$category_data['category_name'],'category_id'=>-1));
		}

	}

	function delete()
	{
		$this->permission_lib->checkPermission();
		
		$categorys_to_delete=$this->input->post('ids');

		if($this->Category->delete_list($categorys_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('categorys_successful_deleted').' '.
			count($categorys_to_delete).' '.$this->lang->line('categorys_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('categorys_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
}
?>