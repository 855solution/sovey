<?php
require_once ("secure_area.php");
class Reports extends Secure_area 
{	
	function __construct()
	{
		parent::__construct('reports');
		// $this->load->helper('report');	

	}
	
	function index(){

		$this->load->view('reports/report');
	}
	function sale_report(){
		$data['customer'] = $this->Customer->get_all()->result();
		$this->load->view('reports/sale_report',$data);
	}
	function get_sale_report(){
		$fd = $this->input->post('fd');
		$td = $this->input->post('td');
		$cus_id = $this->input->post('cus_id');

		$list = '';
		$sale_data = $this->Sale->get_list_sale($fd,$td,$cus_id);
		$i=0;
		foreach ($sale_data as $sd) {
			$cus = $this->Sale->get_sale_people($sd->customer_id);
			$emp = $this->Sale->get_sale_people($sd->employee_id);
			$customer= '';
			if (!empty($cus)) {
				$customer = $cus->last_name.' '.$cus->first_name;
			}
			if (!empty($emp)) {
				$employee = $emp->last_name.' '.$emp->first_name;
			}
			$payment = $this->Sale->get_grand_total($sd->sale_id);
			$gtotal = $payment->payment_amount;
			
			$due = $payment->total_due;
			if ($due<0) {
				$due = 0;
			}
			
			
			$actions ='';
			$actions .= '
						<div class="dropdown">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    Actions
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						    <li><a target="'.rand(1,100).'" href="'.site_url('sales/sale_invoice/'.$sd->sale_id).'">Print Reciept</a></li>
						    <li><a href="'.site_url("sales/edit_sale/".$sd->sale_id).'">Edit Sale</a></li>
						  </ul>
						</div>
						';

			$paid = (float)filter_var($this->Sale->get_sale_by_id($sd->sale_id)->payment_type, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
			$i++;
			$list.="<tr>
						<td>".$i."</td>
						<td>".$sd->sale_time."</td>
						<td>".$sd->invoiceid."</td>
						<td>".$customer."</td>
						<td>$ ".number_format($gtotal,2)."</td>
						<td>$ ".number_format($paid,2)."</td>
						<td>$ ".number_format($due,2)."</td>
						<td>".$employee."</td>
					</tr>";
		}

		$tfoot = "<tr>
						<th colspan='4' style='text-align:right;' ><strong>Total</strong></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>";

		if ($fd && $td) {
			$label_date = "From $fd to $td";
		}
		if ($cus_id) {
			$cus = $this->Sale->get_sale_people($cus_id);
			$label_cus = "Customer: $cus->last_name $cus->first_name";
		}
		$data['label_date'] = $label_date;
		$data['label_cus'] = $label_cus;
		$data['tbody'] = $list;
		$data['tfoot'] = $tfoot;


		echo json_encode($data);
	}
	
}
?>