<?php
require_once ("secure_area.php");
class body extends secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('modbody','body');


	}
	
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Body';
		$body = $this->body->get_all_body();
		$table = '';
		$i=0;
		foreach ($body as $bo) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$bo->body_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$bo->body_name' f='$bo->body_id'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$bo->body_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('body/manage',$data);
		$this->load->view('partial/footer');
	}

	function save(){
		$name = $this->input->post('body_name');
		$id = $this->input->post('body_id');
		$exist = $this->body->check_name($name,$id);
		$trans_data = array(
						'body_name'=>$name,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('body'));
		}else{
			$this->body->save($id,$trans_data);
			redirect(site_url('body'));

		}
	}
	
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->body->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function delete($id){
		$this->db->where('body_id',$id)->update('ospos_body',array('deleted'=>1));
		redirect(site_url('body'));
	}

}
?>