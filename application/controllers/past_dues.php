<?php
require_once ("secure_area.php");
class Past_dues extends Secure_area
{
	function __construct()
	{
		parent::__construct('past_dues');
		$this->load->model('past_due');
		$this->load->model('Customer');
		$this->load->model('sale');
		
	}
	function index(){
		// redirect(site_url('past_dues/due_detail'));
		$this->permission_lib->checkPermission();
		$due = $this->past_due->get_all_pay();
		$data['cus_data'] = $this->Customer->get_all()->result();
		$data['due'] = $due;
		$this->load->view("past_due/all_due",$data);

	}

	public function due_detail($cus_id)
	{
		$cust_info=$this->Customer->get_info($cus_id);
		$com = '';
		if ($cust_info->nick_name!='') {
			$com = "($cust_info->nick_name)";
		}
		$data['customer']=$cust_info->last_name.' '.$cust_info->first_name.$com;
		$data['cus_id'] = $cus_id;
		$config['base_url'] = site_url('/past_dues/due_detail/'.$cus_id);
		$config['total_rows'] = count($this->past_due->get_due_detail($cus_id)->result());
		$config['per_page'] = '20';
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['model']=count($this->past_due->get_all($cus_id)->result());
		$data['controller_name']=strtolower(get_class());
		//$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_past_due_manage_table($this->past_due->get_due_detail($cus_id, $config['per_page'], $this->uri->segment($config['uri_segment'])), $this );
		// var_dump($this->past_due->get_all($cus_id, $config['per_page'], $this->uri->segment($config['uri_segment']))->result());die();
		$data['cus_data'] = $this->Customer->get_all()->result();


		$this->load->view("past_due/manage",$data);
	}

	function get_form_width()
	{			
		return 350;
	}
		/*
	Loads the past due edit form
	*/
	function view($sale_id=-1)
	{
		$this->permission_lib->checkPermission();
		$this->load->model('past_due');
		$data['person_info']=$this->past_due->get_info($sale_id);
		$this->load->view("past_due/form",$data);
	}
	
	/*
	Loads the past due view detail form
	*/
	

	function detail($sale_id=-1)
{
	$this->load->model('past_due');
	$data=array();
	
	$cust_info=$this->past_due->get_customer_name($sale_id)->row_array();
	$data['customer']=$cust_info['last_name'].' '.$cust_info['first_name'];
	$data['saleid']=$sale_id;
	
	$employee_info=$this->past_due->getEmployeeinfo($sale_id)->row_array();
	$data['employee']=$employee_info['last_name'].' '.$employee_info['first_name'];
	$data['person_info']=$this->past_due->get_past_due_detail($sale_id);
	
	
	
	$this->load->view("past_due/detail",$data);
}
	/*
	Inserts a sale payment
	*/
public function save()
{
	$model=$this->load->model('past_due');
	$payment_amount=$this->input->post('payment_amount');
	$total_due=$this->input->post('dept');
	$adding_charged=$this->input->post('adding_charge');


	$remain_due=($total_due)-($adding_charged);
	//$balance_due=$payment_amount-$remain_due;
	$erroreMs='';
	$goterror=false;
	if($adding_charged>$total_due){

	$goterror=true;
	}

		$payment_due = array(
		
			'sale_id'=>$this->input->post('sale_id'),
			'payment_amount'=>$payment_amount,
			'total_due'=>$remain_due,
			'created_date'=>date('Y-m-d'),
			'updated_date'=>date('Y-m-d')
		
		);

	$update_payment=array(
	'dept'=>$remain_due
	);


if($goterror){
$this->load->view("past_due/form",$goterror);
}else{
if($this->past_due->save($payment_due))
{
$this->past_due->update($this->input->post('sale_id'), $update_payment);

echo json_encode(array('success'=>true,'message'=>'You have successfully updated'));

}
else//failure
{	
echo json_encode(array('success'=>false,'message'=>'Invalid updated'));
}
}
}
	/*
	Returns customer table data rows. This will be called with AJAX.
	*/
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_past_due_manage_table_data_rows($this->past_due->search($search),$this);
		echo $data_rows;
	}
/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->past_due->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	function view_invoice($sale_id){

		// $data['items'] = $this->db->query("SELECT si.sale_id,
		// 										si.item_id,
		// 										si.quantity_purchased,
		// 										si.item_cost_price,
		// 										si.item_unit_price,
		// 										i.`name`,
		// 										i.barcode,
		// 										i.category,
		// 										i.partplacement_id,
		// 										si.discount_percent,
		// 										p.partplacement_name,
		// 										k.khmername_name,
		// 										m.make_name,
		// 										mo.model_name,
		// 										i.`year`
		// 								FROM ospos_sales_items si 
		// 								INNER JOIN ospos_v_select_all_items i ON si.item_id = i.item_id AND si.is_new=i.is_new
		// 								LEFT JOIN ospos_makes m ON i.make_id=m.make_id
		// 								LEFT JOIN ospos_models mo ON i.model_id=mo.model_id
		// 								LEFT JOIN ospos_partplacements p ON p.partplacement_id = i.partplacement_id
		// 								LEFT JOIN ospos_khmernames k ON k.khmername_id=i.khmername_id
		// 								WHERE sale_id=$sale_id")->result();
		// $sql = $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = $sale_id")->row();
		// $customer_id = $sql->customer_id;
		// $cust_info=$this->Customer->get_info($customer_id);
		// $data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		// $data['cus_info'] =$this->Customer->get_info($customer_id);
		// $data['invoice_id'] = $this->Sale->get_invoice_id($sale_id);
		// $data['sale_id'] = $sale_id;
		// $data['transaction_time']= date('d/m/Y',strtotime($sql->sale_time));
		// $payment_type = explode(":",$sql->payment_type);
		// $data['pay_type'] = $payment_type[0];
		// $data['pay_amount'] = $payment_type[1];
		// $data['total_pay'] = preg_replace('/[^0-9\.]/', "", $sql->payment_type);
		// $payment = $this->db->query("SELECT * FROM ospos_sales_payments WHERE sale_id=$sale_id")->result();
		// $data['payments']=$payment;
		// $employee_id = $sql->employee_id;
		// $emp_info=$this->Employee->get_info($employee_id);
		// $data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		// $this->load->view('past_due/due_invoice',$data);
		redirect(site_url('sales/sale_invoice/'.$sale_id));
	}
	function save_pay(){
        $cus_id = $this->input->post('cus_id');
        $tam_usd  = $this->input->post('amount_usd');
        $sale_ids = $this->input->post('sale_ids');
        $amount_usd = 0;
        $status = '';
      
        $sales_pay = $this->past_due->get_selected_sale($sale_ids);

        $this->db->trans_start();


        $datarec = array(
                    'cus_id'=>$cus_id,
                    'date'=>date('Y-m-d H:i:s'),
                    'amount_usd'=>$this->input->post('amount_usd'),
                    'deleted'=>0,
                    'employee_id'=>$this->session->userdata('person_id')
                    );
        
        $rec_id = $this->past_due->save_payment_record($datarec);
        foreach ($sales_pay as $val) {
            $amount_usd = 0;
            $paid = $this->sale->get_sale_due_paid($val->sale_id) + $val->first_payment;
			// $total_due = $CI->sale->get_sale_due($person->sale_id)->total_due;
			$grand_total = $this->sale->get_sale_due($val->sale_id)->payment_amount;
			$total_due = $grand_total - $paid;

           	$dept =  $total_due;
            $sale_id = $val->sale_id;

            // $balance_usd = ($val->grand_usd - $val->paid_usd);
            $balance_usd = $total_due;
            // echo "input USD $tam_usd -- $balance_usd <br>";
            // echo "input R $tam_r -- $balance_r <br>";
            // echo "input B $tam_b -- $balance_b <br><br>";
            // echo $balance_usd;die();
        
            if ($tam_usd>=$balance_usd) {
                $amount_usd = $balance_usd;
            }
            
            if ($tam_usd<$balance_usd && $tam_usd>0) {
                $amount_usd = $tam_usd;
            }
            
            if ($amount_usd==$balance_usd ) {
                $status = 'paid';
            }elseif($amount_usd==$tam_usd){
                $status = 'partial';
            }
            // $tam_r = ($tam_r-$balance_r);
            $tam_usd = ($tam_usd-$balance_usd);

            
            if ($amount_usd<$balance_usd) {
            	$dept = $balance_usd-$amount_usd;
            }else{
            	$dept = $amount_usd-$balance_usd;
            }
            // echo "Current USD $tam_usd -- $amount_usd <br>";
            // echo "Current R $tam_r -- $amount_r <br>";
            // echo "Current B $tam_b -- $amount_b <br>";
            // echo "Status ($status)<br><hr>";
            $payment = array(
                'total_due'=>$dept
            );
            if($this->past_due->addPayment($sale_id,$payment)){
                $recdetail = array(
                    'rec_id'=>$rec_id,
                    'sale_id'=>$sale_id,
                    'amount_usd'=>$amount_usd,
                    'payment_status'=>$status
                            );
                $this->past_due->save_record_detail($recdetail);
                $this->past_due->addSalePayment($sale_id,$amount_usd);
                $this->past_due->update_sale_status($sale_id,$status);
                $data['d'] = 'Payment Success';
                $data['rec_id'] = $rec_id;
                // echo json_encode($this->data);
            }
        }


        $this->db->trans_complete();

        $data['s'] = 's';
        echo json_encode($data);
    }
    function payment_history(){

    	$data['cus'] = $this->Customer->get_all();

		$this->load->view('past_due/payment_history',$data);

    }
    function get_payment_history(){
    	$cus_id = $this->input->post('cus_id');
    	$df = $this->input->post('date_from');
    	$dt = $this->input->post('date_to');

    	$where='';
    	$pay_data='';
    	if ($cus_id) {
    		$where.=" AND(r.cus_id=$cus_id)";
    	}
    	if ($df && $dt) {
    		$where.=" AND(DATE_FORMAT(r.date,'%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($df))."' AND '".date('Y-m-d',strtotime($dt))."')";
    	}
    	$sql = $this->db->query("SELECT * FROM ospos_payment_record r
    							 LEFT JOIN ospos_people p ON r.cus_id=p.person_id
    							 WHERE 1=1 {$where}")->result();


    	$i=0;
    	foreach ($sql as $p) {
    		$i++;
    		$emp = $this->Employee->get_info($p->employee_id);
    		$emp_name = $emp->last_name.' '.$emp->first_name;
    		$com = '';
    		if ($p->nick_name) {
    			$com = "($p->nick_name)";
    		}
    		$pay_data.="<tr>
							<td>$i</td>
							<td>$p->last_name $p->first_name$com</td>
							<td>".number_format($p->amount_usd,2)."</td>
							<td>".date('d/m/Y',strtotime($p->date))."</td>
							<td>$emp_name</td>
							<td><a href='".site_url('past_dues/view_voucher/'.$p->rec_id)."' target='_blank'>Voucher</a></td>
							
						</tr>";
    	}
    	if (empty($sql)) {
    		$pay_data.="
    					<tr>
    						<td colspan='5' style='text-align:center;background: #fcf6c6;'><strong>No Data</strong></td>
    					</tr>
    					";
    	}

    	$data['pay_his'] = $pay_data;
    	$data['selected_cus'] = $cus_id;

    	echo json_encode($data);
    	
    }
    function view_voucher($rec_id){

    	$data['row'] = $this->db->query("SELECT * FROM ospos_payment_record
    										 WHERE rec_id = $rec_id")->row();
		$cust_info=$this->Customer->get_info($data['row']->cus_id);
		$emp_info=$this->Customer->get_info($data['row']->cus_id);
		$emp_info=$this->Employee->get_info($data['row']->employee_id);
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name;
    	$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
    	$data['cus_info'] = $cust_info;
    	$data['payment'] = $this->db->query("SELECT * FROM ospos_payment_record_detail r 
    										INNER JOIN ospos_sales s ON r.sale_id = s.sale_id
    										WHERE r.rec_id=$rec_id")->result();
    	$data['rec_id']  = $rec_id;

    	$this->load->view('past_due/due_voucher',$data);
    }
    function print_dues($ids){
    	$sale_ids = explode(':', $ids);
    	// echo $sale_ids[0];die();
    	$data['sales_pay'] = $this->past_due->get_selected_sale($sale_ids);
    	$data['sales_total'] = $this->past_due->get_selected_sale_total($sale_ids); 
    	$data['sales_paid'] = $this->past_due->get_selected_sale_paid($sale_ids);
    	$data['sales_due'] = $data['sales_total'] - $data['sales_paid'];
    	
    	$sale_info = $this->past_due->get_sale_info($sale_ids[0]);
    	$cust_info=$this->Customer->get_info($sale_info->customer_id);
		$emp_info=$this->Customer->get_info($sale_info->customer_id);
		$emp_info=$this->Employee->get_info($this->session->userdata('person_id'));
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name;
    	$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
    	$data['cus_info'] = $cust_info;
    	$this->load->view('past_due/pdinvoice',$data);
    }
    function print_list_item($ids){
    	$sale_ids = explode(':', $ids);
    	// echo $sale_ids[0];die();
    	$data['sales_data'] = $this->past_due->get_selected_sale_items($sale_ids);
    	$data['sales_total'] = $this->past_due->get_selected_sale_total($sale_ids); 
    	$data['sales_paid'] = $this->past_due->get_selected_sale_paid($sale_ids);

    	$data['sales_due'] = $data['sales_total'] - $data['sales_paid'];

    	$sale_info = $this->past_due->get_sale_info($sale_ids[0]);
    	$cust_info=$this->Customer->get_info($sale_info->customer_id);
		$emp_info=$this->Customer->get_info($sale_info->customer_id);
		$emp_info=$this->Employee->get_info($this->session->userdata('person_id'));
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name.' ID: '.$emp_info->person_id;
    	$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
    	$data['cus_info'] = $cust_info;
    	$this->load->view('past_due/item_invoice',$data);
    }
    function search_past_due(){
    	$cus_id = $this->input->post('cus_id');
    	$keyword = $this->input->post('keyword');

    	$pdata = $this->past_due->get_search_past_due($cus_id,$keyword);

    	$table ='';
    	$i=0;
    	foreach ($pdata as $p) {

    		$total_sale = $this->past_due->get_total_cus_sale($p->cus_id);
    		// $total_paid_cus_sale = $this->past_due->get_total_paid_cus_sale($p->cus_id);
    		$total_due = $total_sale['sale_total'] - $total_sale['paid_total'];
    		// $total_due_cus_sale = $grand_total_cus_sale - $total_paid_cus_sale;
    		$i++;
    		$com = '';
    		if ($p->nick_name!='') {
    			$com = "($p->nick_name)";
    		}
    		if ($total_due>0) {

	    		$table .= "<tr>";
	    		$table .= "<td>".$i."</td>";
	    		$table .= "<td>".$p->last_name.' '.$p->first_name.$com."</td>";
	    		$table .= "<td>".$p->phone_number."</td>";
	    		$table .= "	<td>".number_format($total_sale['sale_total'],2)."</td>
							<td>".number_format($total_sale['paid_total'],2)."</td>
							<td>".number_format($total_due,2)."</td>";
				$table .= '<td><a href="'.site_url("past_dues/due_detail/".$p->person_id).'" target="_blank">Add Payment('.$total_sale['sale_count'].')</a></td>';
	    		$table .= "</tr>";
    		}



    	}
    	$data['table'] = $table;

    	echo json_encode($data);
    }
}
?>