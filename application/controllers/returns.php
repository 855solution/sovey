<?php
require_once ("sales.php");
class returns extends sales
{
	function __construct()
	{
		parent::__construct('config');
		// if ($this->sale_lib->get_mode()=='sale') {
		$mode = 'return';
		$this->sale_lib->set_mode($mode);
		// $this->sale_lib->remove_customer();
		// $this->remove_all_item();

			// $this->_reload();
			
		// }
	}
	function index(){
		if ($this->sale_lib->get_mode()=='sale') {
			$mode = 'return';
			$this->sale_lib->set_mode($mode);
			$this->sale_lib->remove_customer();
			$this->remove_all_item();
			// $this->_reload();
			
		}else{
			$this->_reload();
		}
		
	}
	function item_return_search(){
		$suggestions = $this->Item->get_item_search_suggestions_return($this->input->post('q'),$this->input->post('limit'));
		// $suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->post('q'),$this->input->post('limit')));
		echo implode("\n",$suggestions);
	}
	function add_return_item(){
		// var_dump($_POST);die();
		$this->sale_lib->clear_edit_sale();
		$this->sale_lib->empty_payments();
		$this->sale_lib->empty_old_payment();

		$data=array();
		// $newitem = false;
		$mode = $this->sale_lib->get_mode();
		$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item_returns");
		$quantity = $mode=="sale" ? 1:-1;
		// var_dump($item_id_or_number_or_item_kit_or_receipt);
		$sale_id = $this->Item->get_sale_id($item_id_or_number_or_item_kit_or_receipt);

		// die(var_dump($sale_id));

		// var_dump($sale_id);
		if($mode=='return')
		{
			if ($this->valid_sale($sale_id)) {
				if ($this->check_return_expire($sale_id)) {

					if ($this->sale_has_item($sale_id)) {
						
						$this->sale_lib->set_return_sale_id($sale_id);
						$this->sale_lib->return_entire_sale($sale_id);
					}else{
						$this->session->set_flashdata('warn_message',"Invoice has no Item to Return.");
						redirect(site_url('returns'));
					}
					
				}else{
					$this->session->set_flashdata('warn_message',"Sale Expired Can't Return.");
					redirect(site_url('returns'));
				}
			}else{
				$this->session->set_flashdata('warn_message',"Invalid Sale.");
				redirect(site_url('returns'));
			}
			
			
		}
		
		$this->_reload();
	}
	function add_payment_return()
	{		
		$data=array();
		$this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'numeric');
		
		if ($this->form_validation->run() == FALSE)
		{
			if ( $this->input->post('payment_type') == $this->lang->line('sales_gift_card') )
				$data['error']=$this->lang->line('sales_must_enter_numeric_giftcard');
			else
				$data['error']=$this->lang->line('sales_must_enter_numeric');
				
 			$this->_reload($data);
 			return;
		}
		
		$payment_type=$this->input->post('payment_type');
		if ( $payment_type == $this->lang->line('sales_giftcard') )
		{
			$payments = $this->sale_lib->get_payments();
			$payment_type=$this->input->post('payment_type').':'.$payment_amount=$this->input->post('amount_tendered');
			$current_payments_with_giftcard = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $current_payments_with_giftcard;
			if ( $cur_giftcard_value <= 0 )
			{
				$data['error']='Giftcard balance is '.to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) ).' !';
				$this->_reload($data);
				return;
			}
			elseif ( ( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ) > 0 )
			{
				$data['warning']='Giftcard balance is '.to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ).' !';
			}
			$payment_amount=min( $this->sale_lib->get_total(), $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) );
		}
		else
		{
			$payment_amount=$this->input->post('amount_tendered');
			//$payment_amount=$this->sale_lib->get_total();
		}
		$emp_id = $this->session->userdata('person_id');
		$emp = $this->Employee->get_info($emp_id);
		$emp_name = $emp->last_name.' '.$emp->first_name;
		
		if( !$this->sale_lib->add_payment( $payment_type, $payment_amount,date('d/m/Y H:i:s'),$emp_name,$emp_id ) )
		{
			$data['error']='Unable to Add Payment! Please try again!';
		}


		$this->_reload($data);
	}

	function delete_payment_return($payment_id)
	{
		$this->sale_lib->delete_payment($payment_id);
		$this->_reload();
	}
	function complete_return()
	{
		$data['ret_val'] = $this->input->post('returns');

		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a');
		$customer_id=$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		$data['payments']=$this->sale_lib->get_payments();
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$data['desc']=$this->input->post('desc');
		$totalallpayment=$this->sale_lib->get_total();
		// var_dump($data['ret_val']);die();
		foreach ($data['ret_val'] as $key) {

			$this->sale_lib->delete_item($key);
			
		}
		$data['cart']=$this->sale_lib->get_cart();

		// var_dump($data['return_items']);
		// var_dump($data['cart']);die();

		
		$gettotal=$this->sale_lib->get_total();
		$paymentTotal=$this->sale_lib->get_payments_total();
		$changedue=$gettotal-$paymentTotal;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		}

		//SAVE return to database
		$data['sale_id']=$this->Sale->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'],'',$changedue,$totalallpayment);
		
		echo json_encode($data['sale_id']);
		// $this->load->view("sales/receipt",$data);
		// $this->sale_lib->clear_all();
		// echo json_encode($data);
	}
	function edit_item_return($line)
	{
		$data= array();
		$data['no_qty']='';
		$this->form_validation->set_rules('price', 'lang:items_price', 'required|numeric');
		$this->form_validation->set_rules('quantity', 'lang:items_quantity', 'required|numeric');

		$is_return = $this->input->post("is_return");
		$discount = $this->input->post('disc');
		$price = $this->input->post('price');
		$quantity = $this->input->post('quantity');
		$problem = $this->input->post('problem');
		$amt_tender = $this->input->post('amt_tender');

		if ($amt_tender!=0) {
			# code...
			$this->sale_lib->set_return_payment($amt_tender);
		}
		// var_dump($is_return);die();
		$stock_qty='';
		// if ($discount> $price) {
		// 	$discount = $price;
		// }
		// if ($this->form_validation->run() != FALSE)
		// {
			$this->sale_lib->edit_item_return($line,$is_return,$discount,$problem);
			// var_dump($this->sale_lib->get_cart());die();
		// }
		// else
		// {
			// $data['error']=$this->lang->line('sales_error_editing_item');
		// }
		
		// if($this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line)))
		// {
		// 	$data['warning'] = $this->lang->line('sales_quantity_less_than_zero');

		// }
		// $data['problem'] = $problem;
		// $data['taxes']=$this->sale_lib->get_taxes();
		// $data['total']=to_currency($this->sale_lib->get_total());
		$data['payments']=to_currency($this->sale_lib->get_payments());
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$totalallpayment=$this->sale_lib->get_total();
		$data['subtotal']=to_currency($this->sale_lib->get_subtotal());
		$data['total_row'] = to_currency($quantity * $price - $discount);
		$data['payments_total']=to_currency($this->sale_lib->get_payments_total());
		$data['real_pay_total'] = $this->sale_lib->get_payments_total();
		$data['amount_due']=to_currency($this->sale_lib->get_amount_due());
		$old_payment_total = $this->sale_lib->total_old_payment();
		$data['amount_tendered']=$this->sale_lib->get_amount_tender();
		$data['amount_payable'] = to_currency($this->sale_lib->get_amount_payable());
		$data['return_payment'] = $this->sale_lib->get_return_payment();


		$data['total']= to_currency($this->sale_lib->get_return_total());
		$data['real_total'] = $this->sale_lib->get_return_total();
		$data['total_return_item'] = $this->sale_lib->total_return_item();
		$data['total_restock'] = to_currency($this->sale_lib->get_total_restock());
		// $data['amount_tendered']=($old_payment_total*-1) + $this->sale_lib->get_subtotal();
		$data['has_item'] = 1;
		if (!$this->sale_lib->has_item_return()) {
			$data['total'] = to_currency(0);
			$data['real_total'] = 0;
			$data['amount_tendered'] = 0;
			$data['has_item'] = 0;
			$data['amount_payable'] = to_currency(0);


		}


		$data['success'] = "SUCCESS";

		echo json_encode($data);
		// $this->_reload($data);
		// redirect('/sales');
	}
	function view_reciept($return_id){
		$return = $this->Sale->get_return_info($return_id);
		$customer_id = $this->Sale->get_sale_info($this->Sale->get_return_info($return_id)->sale_id)->customer_id;
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$com = '';
			if ($customer_id!=72) {
				if ($cust_info->nick_name!='') {
					$com = "($cust_info->nick_name)";
				}
			}
			
			$data['customer']=$cust_info->title.$cust_info->last_name.' '.$cust_info->first_name.$com.' ID: '.$cust_info->person_id;
		}
		$data['return'] = $return;
		$data['return_id'] = $return_id;
		$data['transaction_time'] = date('d/m/Y H:i:s',strtotime($return->date));
		$emp_info=$this->Employee->get_info($return->return_by);
		$data['employee']=$cust_info->title.$emp_info->last_name.' '.$emp_info->first_name." ID: ".$emp_info->person_id;
		$data['items'] = $this->Sale->get_return_item($return_id);
		$data['paid'] = $this->Sale->get_return_payment($return_id);
		$data['count_return_items'] = $this->Sale->count_return_items($return_id);

		// $data['due'] = $this->Sale->get_return_due($return_id);
		$this->load->view("sales/return_reciept",$data);
		$this->sale_lib->clear_all();
	}
	function check_return_expire($sale_id){
		$sale_day_count = $this->db->query("SELECT 
										DATEDIFF(DATE_FORMAT(now(),'%Y-%m-%d'),DATE_FORMAT(sale_time,'%Y-%m-%d')) as day_count
										FROM ospos_sales
										WHERE sale_id = '$sale_id'")->row()->day_count;
		if ($this->config->item('sale_expired')>0) {
			if ($sale_day_count > $this->config->item('sale_expired')) {
				return false;
			}else{
				return true;
			}
		}else{

			//WILL NOT CHECK IF VALUE IS 0
			return true;
		}
		
	}

	function sale_has_item($sale_id){

		$query = $this->db->query("SELECT * FROM ospos_sales_items WHERE sale_id = '$sale_id' AND is_return <> 1")->result();
		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	function valid_sale($sale_id){
		$query = $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = '$sale_id'")->row();
		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	public function unset_return_payment()
	{
		$payment = $this->sale_lib->unset_return_payment();

		echo $payment;
	}
}
?>