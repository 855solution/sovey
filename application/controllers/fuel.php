<?php
require_once ("secure_area.php");
class fuel extends secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('modfuel','fu');


	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Fuel';
		$trans = $this->fu->get_all_trans();
		$table = '';
		$i=0;
		foreach ($trans as $tra) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$tra->fule_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$tra->fule_name' f='$tra->fuel_id'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$tra->fuel_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('fuel/manage',$data);
		$this->load->view('partial/footer');
	}

	function save(){
		$name = $this->input->post('fuel_name');
		$id = $this->input->post('fuel_id');
		$exist = $this->fu->check_name($name,$id);
		$trans_data = array(
						'fule_name'=>$name,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('fuel'));
		}else{
			$this->fu->save($id,$trans_data);
			redirect(site_url('fuel'));

		}
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->fu->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function delete($id){
		$this->db->where('fuel_id',$id)->update('ospos_fuel',array('deleted'=>1));
		redirect(site_url('fuel'));
	}

}
?>