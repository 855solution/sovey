<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Page extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('models');
		$this->load->model('modPage','page');
		$this->load->model('slide','Slide');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/page/index');
		$config['total_rows'] = $this->Slide->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_page_manage_table( $this->page->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('page/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_page_manage_table_data_rows($this->page->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Model->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$model_id = $this->input->post('row_id');
		$data_row=get_model_data_row($this->Model->get_info($model_id),$this);
		echo $data_row;
	}
	
	function view($slide_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['page_info']=$this->page->get_info($slide_id);
		
		// $makes = array('' => $this->lang->line('items_none'));
		// foreach($this->Make->get_all()->result_array() as $row)
		// {
		// 	$makes[$row['make_id']] = $row['make_name'];
		// }		
		// // $data['makes']=$makes;
		// $data['selected_make'] = $this->Slide->get_info($slide_id)->make_id;

		$this->load->view("page/form",$data);
	}
	
	function save($slide_id=-1)
	{
		$active=0;
		if(isset($_POST['is_active']))
			$active=1;
		echo $this->input->post('content');
		$slide_data = array(
		'page_name'=>$this->input->post('Page_name'),
		'orders'=>$this->input->post('Page_order'),
		'article'=>$_POST['descr'],
		'keyword'=>$this->input->post('keyword'),
		'is_active'=>$active,
		
		);

		if( $this->page->save( $slide_data, $slide_id ) )
		{
			//New model
			if($slide_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>"Page add Succesfull".' '.
				$slide_data['page_name'],'pageid'=>$slide_data['pageid']));
				$slide_id = $slide_data['pageid'];
				// $this->do_upload($slide_id);
			}
			else //previous model
			{
				echo json_encode(array('success'=>true,'message'=>'Page has update succesfule'.' '.
				$slide_data['page_name'],'pageid'=>$slide_id));
				// $this->do_upload($slide_id);
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>"error".' '.
			$slide_data['page_name'],'pageid'=>-1));
		}

	}

	function delete()
	{
		$this->permission_lib->checkPermission();
		$models_to_delete=$this->input->post('ids');

		if($this->page->delete_list($models_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('Page_successful_deleted').' '.
			count($models_to_delete).' '.$this->lang->line('models_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('models_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function do_upload($slide_id)
		{
			if(!file_exists('./assets/upload/slide'))
			 {
			    if(mkdir('./assets/upload/slide',0755,true))
			    {
			                return true;
			    }
			 }
			$config['upload_path'] ='./assets/upload/slide';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$slide_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('userfile'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			// else
			// {		
			// 	//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
			// 	$data = array('upload_data' => $this->upload->data());			
			// 	//redirect('setting/user');
			// 	 	$config2['image_library'] = 'gd2';
   //                  $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
   //                  $config2['new_image'] = './assets/upload/slide_id/thum';
   //                  $config2['maintain_ratio'] = TRUE;
   //                  $config2['create_thumb'] = TRUE;
   //                  $config2['thumb_marker'] = FALSE;
   //                  $config2['width'] = 120;
   //                  $config2['height'] = 180;
   //                  $config2['overwrite']=true;
   //                  $this->load->library('image_lib',$config2); 

   //                  if ( !$this->image_lib->resize()){
	  //               	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
			// 		}else{
			// 			//unlink('./assets/upload/students/'.$student_id.'.jpg');
			// 		}
			// }
		}
}
?>