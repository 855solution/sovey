<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class template extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('makes');
		$this->load->database();
		$this->load->model('Modtemplate','tem');
		$this->load->model('item','item');
		$this->load->helper(array('form', 'url'));
        // $this->load->library('sale_lib');//New 02-08-2012
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$data['show_delete'] = $this->permission_lib->user_f('delete');
		$this->sale_lib->unset_filter_tem();
		if (!$this->uri->segment(2)) {
			$this->session->unset_userdata('make_id');
			$this->session->unset_userdata('model_id');
			$this->session->unset_userdata('year');
			$this->session->unset_userdata('yearto');
			$this->session->unset_userdata('search_filter');
			// $this->session->unset_userdata('search');
		}

		$item_id='';
		$numpage=30;
		if(isset($_GET['p']))
			$numpage=$_GET['p'];
		$per_page='';
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];
		$config['base_url'] = site_url('/template/index?p='.$numpage);
		$config['total_rows'] = $this->tem->count_all();
		$data['total_rows'] = $config['total_rows'];
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 3;
		$config['page_query_string']=TRUE;

		$this->pagination->initialize($config);
		
		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();

		$data['manage_table']=get_template_manage_table( $this->tem->get_all( $per_page, $numpage ), $this );
	//	$data['manage_table']=get_template_manage_table( $this->tem->get_all( $config['per_page'], $per_page), $this );

		/*make, model, year combo box for searching*/
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		//$data['selected_make'] = $this->Item->get_info_where_itemnumber($itemnumber)->make_id;
		$data['selected_make'] = '';
		
		/****************/
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		/******************************/
		// $khmernames = array('' => $this->lang->line('items_none'));
		// foreach($this->Khmername->get_all()->result_array() as $row)
		// {
		// 	$khmernames[$row['khmername_id']] = $row['khmername_name'];
		// }		
		// $data['khmernames']=$khmernames;
		// $data['selected_khmername'] = $this->Khmername->get_info($item_id)->khmername_id;
		/************************close******************************************/
		$data['vinnumber'] = $this->db->query("SELECT * FROM ospos_vinnumbers WHERE deleted=0")->result();
		
		if (isset($_GET['act'])) {
			if ($_GET['act']=='check_price') {
				$this->load->view('template/check_price',$data);
			}
		}else{
			$this->load->view('template/manage',$data);

		}
	}

	function search()
	{
		$search = '';
		if (isset($_GET['s'])) {
			$search = $_GET['s'];
		}
		$numpage=30;
		if(isset($_GET['p']))
			$numpage=$_GET['p'];
		$per_page='';
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];
		$config['base_url'] = site_url('/template/search?s='.$search.'&p='.$numpage);
		$config['total_rows'] = $this->tem->count_all_search($search);
		
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 3;
		$config['page_query_string']=TRUE;

		$this->pagination->initialize($config);
		
		$data_rows=get_template_manage_table($this->tem->fullTextSearch($search,$numpage,$per_page),$this);
		$data['manage_table']=$data_rows;
		$data['controller_name']=strtolower(get_class());

		$this->load->view('template/manage',$data);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->tem->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$make_id = $this->input->post('row_id');
		$data_row=get_template_data_row($this->tem->get_info($make_id),$this);
		echo $data_row;
	}

	function view($make_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['make_info']=$this->tem->get_info($make_id);
		if ($make_id!=-1) {
			$data['selected_khmername'] = 1;
		}
		$this->remove_tem_fitment();
		$this->load->view("template/form",$data);
	}
	
	function save($tem_id=-1)
	{

		$year = $this->input->post('year');
		$year_to = $this->input->post('year_to');
		
		
		$body = $this->input->post('vin_body');
		$trim = $this->input->post('vin_trim');
		$trans = $this->input->post('vin_trans');
		$fuel = $this->input->post('vin_fuel');
		$engine = $this->input->post('vin_engine');
		$part_number = $this->input->post('part_number');
		$part_name = $this->item->khmer_name($this->input->post('khmername_id'))->row()->english_name;
		$khmername_id = $this->input->post('khmername_id');
		$category_id = $this->input->post('category_id');
		$fit = $this->input->post('fit');
		$make_id = $this->input->post('make_id');
		$model_id = $this->input->post('model_id');
		$date = date('Y-m-d H:i:s');
		$make_name = $this->item->make_name($this->input->post('make_id'))->row()->make_name;
		$model_name = $this->item->model_name($this->input->post('model_id'))->row()->model_name;
		$color_name = $this->item->color_name($this->input->post('color_id'))->row()->color_name;
		$ppl_name = $this->item->part_placement_name($this->input->post('partplacement_id'))->row()->partplacement_name;
		$brand_name = $this->item->branch_name($this->input->post('branch_id'))->row()->branch_name;
		$khmername_name = $this->item->khmer_name($this->input->post('khmername_id'))->row()->khmername_name;
		$condition_name = $this->item->condition_name($this->input->post('condition_id'))->row()->condition_name;
		$color_id = $this->input->post('color_id');
		$condition_id = $this->input->post('condition_id');
		$ppl_id = $this->input->post('partplacement_id');
		$brand_id = $this->input->post('branch_id');
		$location_id = $this->input->post('location_id');
		$cost_price = $this->input->post('cost_price');
		$unit_price = $this->input->post('unit_price');
		$unit_price_to = $this->input->post('unit_price_to');
		$quantity = $this->input->post('quantity');



		$exist = $this->tem->check_part($part_number,$tem_id);
		if ($exist) {
			$this->session->set_flashdata('er','Part Number Exist!');
			redirect(site_url('template'));
		}
		$year_tag='';
		for ($i=$year; $i <= $year_to; $i++) { 
			$year_tag.= $i;
			if ($i<$year_to) {
				$year_tag.=",";
			}
		}
		$make_data = array(
		'part_number'=>$part_number,
		'part_name'=>$part_name,
		'khmernames_id'=>$khmername_id,
		'category_id'=>$category_id,
		'approve'=>isset($_POST['approve'])? 1 : 0,
		'date'=>$date,
		'part_placementname'=>$ppl_name,
		'khmername_name'=>$khmername_name,
		'engine'=>$engine,
		'transmission'=>$trans,
		'fuel'=>$fuel,
		'trim'=>$trim,
		'part_placement'=>$ppl_id,
		'location_id'=>$location_id,
		'unit_price_to'=>$unit_price_to,
		'fit'=>$fit,
		'make_id'=>$make_id,
		'make_name'=>$make_name,
		'model_id'=>$model_id,
		'model_name'=>$model_name,
		'year'=>$year,
		'year_to'=>$year_to,
		'year_tag'=>$year_tag,
		'color_id'=>$color_id,
		'color_name'=>$color_name,
		'body'=>$body,
		'codition_id'=>$condition_id,
		'condition_name'=>$condition_name,
		'brand_id'=>$brand_id,
		'branch_name'=>$brand_name,
		'cost_price'=>$cost_price,
		'unit_price'=>$unit_price
		);
		// if($tem_id==-1){
		
			
		// 	$arr2=array(
						
		// 			);
		// 	$make_data=array_merge($make_data,$arr2);
		// }
		// var_dump($make_data);die();
		// if ($tem_id!=-1) {
		// 	$this->update_item_same_template($tem_id);
		// }
		// die('no tem id');
		$old_partnumber = $this->db->query("SELECT * FROM ospos_part WHERE partid='$tem_id'")->row()->part_number;

		if( $this->tem->save( $make_data, $tem_id ) )
		{
			//New make
			if($tem_id==-1)
			{
			// 	echo json_encode(array('success'=>true,'message'=>$this->lang->line('makes_successful_adding').' '.
				// $make_data['make_name'],'make_id'=>$make_data['make_id']));
				$tem_id = $make_data['tem_id'];
				$def_data = array(
						'part_id'=>$tem_id,
						'def_make'=>$this->input->post('make_id'),
						'def_model'=>$this->input->post('model_id'),
						'def_year'=>$this->input->post('year'),
						'def_year_to'=>$this->input->post('year_to'),
						'def_year_tag'=>$year_tag
						);
				$this->update_fitment($tem_id,$def_data);
			}
			else //previous make
			{
				// $this->updateitem($tem_id);
				if ($old_partnumber!='NO') {
					$this->update_item_same_template($tem_id,$old_partnumber);
				}
			// 	echo json_encode(array('success'=>true,'message'=>$this->lang->line('makes_successful_updating').' '.
				// $make_data['make_name'],'make_id'=>$make_id));

			}
			// var_dump($_FILES);die();
			$this->upload($tem_id);
			// UPDATE ADD FITMENT
			
			redirect(site_url('template'));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('makes_error_adding_updating').' '.
			$make_data['tem_id'],'tem_id'=>-1));
		}

	}

	function update_item_same_template($tem_id,$old_partnumber){
		$temp = $this->db->query("SELECT * FROM ospos_part WHERE partid='$tem_id'")->row();
		$make_name = $this->item->make_name($temp->make_id)->row()->make_name;
		$model_name = $this->item->model_name($temp->model_id)->row()->model_name;

		$eng_name = $this->item->khmer_name($temp->khmernames_id)->row()->english_name;
		$khmer_name = $this->item->khmer_name($temp->khmernames_id)->row()->khmername_name;

		
		$item_data = array(
					'item_number'=>$temp->part_number,
					'category_id'=>$temp->category_id,
					'khmername_id'=>$temp->khmernames_id,
					'cost_price'=>0,
					'unit_price'=>$temp->unit_price_to,
					'body'=>$temp->body,
					'engine'=>$temp->engine,
					'transmission'=>$temp->transmission,
					'trim'=>$temp->trim,
					'fuel'=>$temp->fuel,
					'name'=>$eng_name,
					'khmer_name'=>$khmer_name,
					'partplacement_id'=>$temp->part_placement,
					'partplacement_name'=>$temp->part_placementname
					);


		
		$item_to_update = $this->db->query("SELECT * FROM ospos_items WHERE item_number = '$old_partnumber'")->result();


		foreach ($item_to_update as $item) {
			$make_name=$this->get2FirstString($temp->make_name);
			$year=$this->get2lastString($temp->year);
			$model_name=$this->get2FirstString($temp->model_name);

			$digit_padding=str_pad($item->item_id,6,"0",STR_PAD_LEFT);
			$barcode=$year.$make_name.$model_name.$digit_padding;


			if ($item->vinnumber_id==0 AND $item->vinnumber_id==NULL AND $item->vinnumber_id=='') {
				$have_vin_data = array(
					'make_id'=>$temp->make_id,
					'model_id'=>$temp->model_id,
					'year'=>$temp->year,
					'make_name'=>$make_name,
					'model_name'=>$model_name
				);
				$item_data = array_merge($item_data,$have_vin_data);
			}

			$this->Item->save($item_data,$item->item_id);


			$barcode_item_data = array(
				'name'=>$this->item->khmer_name($temp->khmernames_id)->row()->english_name,
				'item_number'=>$temp->part_number,
				'category_id'=>$temp->category_id,
				'fit'=>'',
				'model_id'=>$temp->model_id,
				'year'=>$temp->year,
				'make_id'=>$temp->make_id,
				'barcode'=>$barcode,
				'khmername_id'=>$temp->khmernames_id
				);
			$this->tem->updateBarcodeAfterSave($item->item_id,$barcode_item_data);
		}
		// var_dump($temp);
		// die();
	}

	// function updateitem($template_id){
	// 	$item_data = array(
	// 	'name'=>$this->item->khmer_name($this->input->post('khmername_id'))->row()->english_name,
	// 	'category_id'=>$this->input->post('category_id'),
	// 	'item_number'=>$this->input->post('part_number'),
	// 	'fit'=>$this->input->post('fit'),
	// 	'model_id'=>$this->input->post('model_id'),
	// 	'year'=>$this->input->post('year'),
	// 	'make_id'=>$this->input->post('make_id'),
	// 	'khmername_id'=>$this->input->post('khmername_id'),
	// 	'make_name'=>$this->item->make_name($this->input->post('make_id'))->row()->make_name,
	// 	'model_name'=>$this->item->model_name($this->input->post('model_id'))->row()->model_name,
	// 	'color_name'=>$this->item->color_name($this->input->post('color_id'))->row()->color_name,
	// 	'partplacement_name'=>$this->item->part_placement_name($this->input->post('partplacement_id'))->row()->partplacement_name,
	// 	'branch_name'=>$this->item->branch_name($this->input->post('brand_id'))->row()->branch_name,
	// 	'khmer_name'=>$this->item->khmer_name($this->input->post('khmername_id'))->row()->khmername_name
	// 	// 'condition_name'=>$this->item->condition_name($this->input->post('condition_id'))->row()->condition_name,
	// 	);
	// 	$data_row=$this->db->select('i.*,mo.model_name,ma.make_name')
	// 						->from('ospos_items i')
	// 						->join('ospos_models mo','i.model_id=mo.model_id','left')
	// 						->join('ospos_makes ma','i.make_id=ma.make_id','left')
	// 						->where('i.item_number',$this->input->post('part_number_old'))->get();
	// 	foreach ($data_row->result() as $row) {
	// 		$make_name=$this->get2FirstString($row['make_name']);
	// 		$year=$this->get2lastString($this->input->post('year'));
	// 		$model_name=$this->get2FirstString($row['model_name']);

	// 		$digit_padding=str_pad($row['item_id'],6,"0",STR_PAD_LEFT);
	// 		$barcode=$year.$make_name.$model_name.$digit_padding;
			
	// 		$this->Item->save($item_data,$row['item_id']);
	// 		$barcode_item_data = array(
	// 			'name'=>$this->item->khmer_name($this->input->post('khmername_id'))->row()->english_name,
	// 			'item_number'=>$this->input->post('part_number'),
	// 			'category_id'=>$this->input->post('category_id'),
	// 			'item_number'=>$this->input->post('part_number'),
	// 			'fit'=>$this->input->post('fit'),
	// 			'model_id'=>$this->input->post('model_id'),
	// 			'year'=>$this->input->post('year'),
	// 			'make_id'=>$this->input->post('make_id'),
	// 			'barcode'=>$barcode,
	// 			'khmername_id'=>$this->input->post('khmername_id')
	// 			);
	// 		$this->tem->updateBarcodeAfterSave($row['item_id'],$barcode_item_data);
	// 	}
		
	// }
	function deleteimage(){
 		$value=true;
 		$imageid=$this->input->post('imageid');
 		if($imageid){
 			$this->load->model('item');
 			// $image_name=$this->tem->getImageName($imageid);
		 		$delete=$this->tem->delete_image_item($imageid);
		 		// if($delete){
		 		// 	 unlink("./uploads/".$image_name);
		 		// }
 		
 		}
 		
 		
 	}
 	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
 	function get2FirstString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$end = $characters-$length;
		
		$firstString = substr($string ,0 ,$end);
		return $firstString;
		}
	}
	
	function generate_barcode_6digit($length) 
	{
		$consonants = '1234567890';

	    for ($i = 0; $i < $length; $i++) 
	    {
	    	
	    	$digit.= $consonants[(rand() % strlen($consonants))];
	    	
	    }
	    return $digit;
	}
	function delete()
	{
		$this->permission_lib->checkPermission();
		$makes_to_delete=$this->input->post('ids');

		if($this->tem->delete_list($makes_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('makes_successful_deleted').' '.
			count($makes_to_delete).' '.$this->lang->line('makes_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('makes_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}

	function upload($productid)
	{       

	    $this->load->library('upload');
	    $orders=$this->input->post('order');
	    $updimg=$this->input->post('updimg');
		$arrid=$this->input->post('deleteimg');
		// var_dump($arrid);die();
		// echo $updimg;
		$arrid=trim($arrid,',');
		$arr=explode(',',$arrid);
		
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_template_images where tem_image_id='".$arr[$i]."'")->row();
				// print_r($row);
				if(isset($row->image_name)){
					unlink('./uploads/'.$row->image_name);
					unlink('./uploads/thumb/'.$row->image_name);
					$this->db->where('tem_image_id',$row->tem_image_id)->delete('ospos_template_images');
				}
			}
		}
		// echo $arrid;
	 //    $this->unlinkpic($productid,$arrid);
	    $files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);
	    for($i=0; $i<$cpt; $i++)
	    {         

	    	if(isset($updimg[$i]) && $updimg[$i]!=''){
	    		$this->updatepic($updimg[$i],$orders[$i]);
	    	}  
	    	// echo $orders[$i];
	        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
	        
	        $this->upload->initialize($this->set_upload_options($productid,$_FILES['userfile']['name']));
	        // $this->upload->do_upload();
	        if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());	
				// echo $error['error'];		
			}else{	
				// echo $_FILES['userfile']['name'];
				$this->creatthumb($productid,$_FILES['userfile']['name'],$orders[$i]);
			}
	    }
	}
	function updatepic($picid,$order){
		$this->db->where('tem_image_id',$picid)->set('first_image',$order)->update('ospos_template_images');
	}
	function creatthumb($productid,$imagename,$order){
			$data = array('upload_data' => $this->upload->data());
		 	$config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = './uploads/thumb';
            $config2['maintain_ratio'] = false;
            $config2['create_thumb'] = "template_"."$productid".'.png';
            $config2['thumb_marker'] = false;
            $config2['height'] = 190;
            $config2['width'] = 195;
            $this->load->library('image_lib');
            $this->image_lib->initialize($config2); 
            if ( ! $this->image_lib->resize()){
            	// echo $this->image_lib->display_errors();
			}else{
				$this->saveimg($productid,$this->upload->file_name,$order);
			}
		
	}
	private function set_upload_options($productid,$imagename)
	{   
	   
	    $config = array();
	    $config['upload_path'] = './uploads/';
	    $config['allowed_types'] = 'gif|jpg|jpeg|png';
	    $config['max_size']      = '0';
	    $config['file_name']  	 = "template_"."$productid".'.png';
		$config['overwrite']	 = true;
		$config['file_type']='image/png';

	    return $config;
	}

	function saveimg($productid,$imagename,$order){
		$img_name = preg_replace('/[^a-zA-Z0-9.\']/', '_', $imagename);
		$new_img_name = str_replace("'", '', $img_name);

		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		$count=$this->db->query("SELECT count(*) as count FROM ospos_template_images where template_id='$productid' AND image_name='$new_img_name'")->row()->count;
		if($count==0){
			$data=array('template_id'=>$productid,
						'image_name'=>$new_img_name,
						'first_image'=>$order);
			$this->db->insert('ospos_template_images',$data);
		}
	}
	function unlinkpic($stockid,$arrids){
		$arrid=trim($arrids,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_template_images where tem_image_id='".$arr[$i]."'")->row();
				if(isset($row->pic_name)){
					unlink('./uploads/'.$row->image_name);
					unlink('./uploads/thumb/'.$row->image_name);
					$this->db->where('tem_image_id',$row->image_id)->delete('ospos_template_images');
				}
			}
		}
	}

	function refresh()
	{
		$ma = $this->input->post('make_id');
		$mo = $this->input->post('model_id');
		$ye = $this->input->post('year');
		$yeto = $this->input->post('yearto');
		$search = $this->input->post('search_filter');

		$s_s=$this->session->userdata('search_filter');
		$s_ma=$this->session->userdata('make_id');
		$s_mo=$this->session->userdata('model_id');
		$s_y=$this->session->userdata('year');
		$s_yt=$this->session->userdata('yearto');
		//Search
		if ($ma || $mo || $ye || $yeto || $search) {
			$this->session->set_userdata('make_id',$this->input->post('make_id'));
			$this->session->set_userdata('model_id',$this->input->post('model_id'));
			$this->session->set_userdata('year',$this->input->post('year'));
			$this->session->set_userdata('yearto',$this->input->post('yearto'));
			$this->session->set_userdata('search_filter',$this->input->post('search_filter'));
		}elseif($s_s || $s_ma || $s_mo || $s_y || $s_yt){
			$this->session->set_userdata('make_id',$s_ma);
			$this->session->set_userdata('model_id',$s_mo);
			$this->session->set_userdata('year',$s_y);
			$this->session->set_userdata('yearto',$s_yt);
			$this->session->set_userdata('search_filter',$s_s);
		}else{
			redirect(site_url('template'));
		}
		$name_filter=$this->session->userdata('search_filter');
		$make_filter=$this->session->userdata('make_id');
		$model_fitler=$this->session->userdata('model_id');
		$year_filter=$this->session->userdata('year');
		$yearto_filter=$this->session->userdata('yearto');
		// $khmername_filter=$this->session->userdata('khmername_id');
	
		$numpage=30;
		if(isset($_GET['p']))
			$numpage=$_GET['p'];

		$per_page='';
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];

		$config['base_url'] = site_url('/template/refresh?p='.$numpage);
		$config['total_rows'] = $this->tem->count_all_filtered($name_filter,$make_filter,$model_fitler,$year_filter,$yearto_filter);
		
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 3;
		$config['page_query_string']=TRUE;

		$this->pagination->initialize($config);
		
		$data['search_section_state']=$this->input->post('search_section_state');
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_template_manage_table($this->tem->get_all_filtered_name_make_model_year($name_filter,$make_filter,$model_fitler,$year_filter,$yearto_filter,$numpage,$per_page),$this);
	
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}	
			
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info_where_makeId($make_filter)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_model_depend_make($make_filter)->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info_where_modelId($model_fitler)->model_id;
		
		/******************************/
		$khmernames = array('' => $this->lang->line('items_none'));
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['khmername_name'];
		}		
		/******************************close******************************************/

		$this->load->view('template/manage',$data);
	}
	function save_tem_img_to_item($productid,$imagename,$order,$tem_image){
		// $date=date('Y-m-d H:i:s');
		
			$data=array('item_id'=>$productid,
						'imagename'=>$imagename.'.png',
						'tem_image'=>$tem_image,
						'first_image'=>$order);
			$this->db->insert('ospos_image_items',$data);
	}

	// function add_to_item(){
	// 	$this->load->helper('url');
	// 	$this->load->model('item');
	// 	$tid = $_GET['tid'];
	// 	$tem = $this->tem->get_info_by_partid($tid)->row();
	// 	// var_dump($tem);die();

	// 	$part_name = $tem->part_name;
	// 	$cat_id = $tem->category_id;
	// 	$item_number = $tem->part_number;
	// 	$fit = $tem->fit;
	// 	$model_id = $tem->model_id;
	// 	$make_id = $tem->make_id;
	// 	$year = $tem->year;
	// 	$color_id = $tem->color_id;
	// 	$condition_id = $tem->codition_id;

	// 	$make_name=$this->item->make_name($make_id)->row_array();
	// 	$name['getname']=$make_name['make_name'];
		
	// 	$modelname=$this->item->model_name($model_id)->row_array();
	// 	$name['modelname']=$modelname['model_name'];
		
	// 	$make_name=$this->get2FirstString($name['getname']);
	// 	$year_bar=$this->get2lastString($year);
	// 	$model_name=$this->get2FirstString($name['modelname']);

	// 	$item_data = array(
	// 	'name'=>$part_name,
	// 	'category_id'=>$cat_id==''? null:$cat_id,
	// 	'item_number'=>$item_number=='' ? null:$item_number,
	// 	'fit'=>$fit,
	// 	'make_id'=>$make_id=='' ? null:$make_id,
	// 	'model_id'=>$model_id=='' ? null:$model_id,
	// 	'year'=>$year,
	// 	'color_id'=>$color_id=='' ? null:$color_id,
	// 	'condition_id'=>$condition_id=='' ? null:$condition_id,
	// 	'khmername_id'=>$tem->khmernames_id,
	// 	'partplacement_id'=>$tem->part_placement,
	// 	'branch_id'=>$tem->brance_id,
	// 	'location_id'=>$tem->location_id,
	// 	'cost_price'=>$tem->cost_price==null?0:$tem->cost_price,
	// 	'unit_price'=>$tem->unit_price==null?0:$tem->unit_price,
	// 	'quantity'=>$tem->quantity==null?0:$tem->quantity,
	// 	'make_name'=>$tem->make_name,
	// 	'model_name'=>$tem->model_name,
	// 	'partplacement_name'=>$tem->part_placementname,
	// 	'branch_name'=>$tem->branch_name,
	// 	'khmer_name'=>$tem->khmername_name,
	// 	'color_name'=>$tem->color_name,
	// 	'engine'=>$tem->engine,
	// 	'body'=>$tem->body,
	// 	'trim'=>$tem->trim,
	// 	'transmission'=>$tem->transmission,
	// 	'vinnumber_id'=>$tem->vinnumber_id,
	// 	'fuel'=>$tem->fuel,
	// 	'add_date'=>date('Y-m-d')
	// 	);
		
	// 	$digit_padding='';
	// 	// echo "$year_bar$make_name$model_name$digit_padding";die();

	// 	$tsave = $this->Item->save($item_data,$item_id=-1);

	// 	$itemid = $this->tem->get_last_item_id($tid)->item_id;
	// 	$item_imgsave = $this->save_tem_img_to_item($itemid,$tid,1,1);

	// 	/*--------------------update barcode after save item sucessful-----------------------------------------*/
					
	// 				$digit_padding=str_pad($itemid,6,"0",STR_PAD_LEFT);
	// 				$barcode=$year_bar.$make_name.$model_name.$digit_padding;
					
	// 				$barcode_item_data = array(
	// 					'barcode'=>$barcode
	// 					);
	// 				$this->Item->updateBarcodeAfterSave($itemid,$barcode_item_data);

	// 	if ($tsave==true) {
	// 		redirect('/template');
	// 	}


	// }

	function view_tem_history($part_number){
		$data['history_data'] = $this->tem->get_history($part_number);
		$data['available_stock'] = $this->tem->get_template_stock($part_number)->stock;
		$this->load->view('template/history',$data);
	}
	function gemodel_pos(){
		$make_id=$this->input->post('make');
		$model_id=$this->input->post('model');
		$color_id=$this->input->post('color');
		$year=$this->input->post('year');

		// $ex_color_id=$this->input->post('ex_color');
		// $name=$this->input->post('name');
		$year_from =$this->input->post('year_from');
		$year_to = $this->input->post('year_to');
		$body_id = $this->input->post('body');
		$search = $this->input->post('search');
		$engine_id = $this->input->post('engine');
		$fuel_id = $this->input->post('fuel');
		$trans_id = $this->input->post('trans');
		$trim_id = $this->input->post('trim');
		$vinnumber =$this->input->post('vin');
		$place = $this->input->post('ppl');
		$location = $this->input->post('loc');
		$s_all = $this->input->post('search_all');
		$where='';
		$arr=array();
		$sql=$this->session->userdata('query_result_temp');
		// echo $sql;die();
		// $this->db->query("DROP TEMPORARY TABLE IF EXISTS tmp_result_temp");
  //       $this->db->query("CREATE TEMPORARY TABLE tmp_result_temp $sql");
        
        $where_makes='';
		$where_models='';
		$where_years='';
		$where_yearfs='';
		$where_cols='';

		$where_vins='';
		$where_bodys='';
		$where_engines='';
		$where_fuels='';
		$where_transs='';
		$where_trims='';
		$where_place='';
		$where_location='';
		$where_s_all = '';

		if ($s_all) {
			$ars = explode(' ', $s_all);
			for ($i=0; $i < count($ars); $i++) { 
				$where_s_all.= " AND(
									i.part_number LIKE '%$ars[$i]%' OR
									i.part_name LIKE '%$ars[$i]%' OR
									i.make_name LIKE '%$ars[$i]%' OR
									i.model_name LIKE '%$ars[$i]%' OR
									i.year LIKE '%$ars[$i]%' OR
									i.color_name LIKE '%$ars[$i]%' OR
									i.condition_name LIKE '%$ars[$i]%'
									)";
			}

			// $where.= $where_s_all;
			
		}


		 if($make_id!=''){
            $where_makes.=" AND i.make_id='".$make_id."'";
        }elseif ($make_id=='other') {
        	$where_makes.=" AND i.make_id='' OR i.make_id=0 OR i.make_id IS NULL";
        }
        if($model_id!=''){
            $where_models.=" AND i.model_id='".$model_id."'";
        }elseif ($model_id=='other') {
        	$where_models.=" AND i.model_id='' OR i.model_id=0 OR i.model_id IS NULL";
        }
        
       
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
            	if ($arryear[$i]!='' && $arryear[$i]!='_') {
            		
            		if ($arryear[$i]=='other') {
            			$where_year.=" i.year='' OR i.year=0 OR i.year IS NULL";
            		}else{
                    	$where_year.="i.year='".$arryear[$i]."'";
            		}
                    if($i<count($arryear)-2)
                        $where_year.=' OR ';
                }
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where.=$where_year;
            $where_years.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where.=" AND i.year BETWEEN $yf AND $yt";
        }
        if($col!='' && $col!='_'){
            $arrcol=explode('_', $col);
            $where_color=' AND(';
            for ($i=0; $i < count($arrcol) ; $i++) { 
            	if ($arrcol[$i]!='' && $arrcol[$i]!='_') {
            		# code...
            		if ($arrcol[$i]=='other') {
            			$where_color.="i.color_id='' OR i.color_id=0 OR i.color_id IS NULL";
            		}else{
                    	$where_color.="i.color_id='".$arrcol[$i]."'";
            		}
                    if($i<count($arrcol)-2)
                        $where_color.=' OR ';
            	}
            }
            $where_color.=')';
            // $s_year=$_GET['y'];
            $where.=$where_color;
            $where_cols.=$where_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }

        // VINNUMBER
        if($vinnumber!=''){
			$vin=$vinnumber;
			if ($vin!='other') {
				$re.="$vin";
				$where_vins.=" AND i.vinnumber_id='".$_POST['vin']."'";
			}
			else{
				$where_vins.=" AND (i.vinnumber_id='' OR i.vinnumber_id='0' OR i.vinnumber_id IS NULL)";
			}
		}

        // LOCATION

		if($location!='' && $location!='_'){
            $arrloc=explode('_', $location);
            $where_loc=' AND(';
            for ($i=0; $i < count($arrloc) ; $i++) { 
            	if ($arrloc[$i]!='' && $arrloc[$i]!='_') {
            		
            		if ($arrloc[$i]=='other') {
            			$where_loc.=" i.location_id='' OR i.location_id=0 OR i.location_id IS NULL";
            		}else{
                    	$where_loc.="i.location_id='".$arrloc[$i]."'";
            		}
                    if($i<count($arrloc)-2)
                        $where_loc.=' OR ';
                }
                
            }
            $where_loc.=')';
            // $s_year=$_GET['y'];
            $where_location.=$where_loc;

        }

        // BODY
        if($body_id!='' && $body_id!='_'){
            $bo_arr=explode('_', $body_id);
            $where_body=' AND(';
            for ($i=0; $i < count($bo_arr) ; $i++) { 
            	if ($bo_arr[$i]!='' && $bo_arr[$i]!='_') {
            		# code...
            		if ($bo_arr[$i]=='other') {
            			$where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
            		}else{
                    $where_body.="i.body='".$bo_arr[$i]."'";

            		}
                    if($i<count($bo_arr)-2)
                        $where_body.=' OR ';
            	}
                
            }
            $where_body.=')';
            // $s_year=$_GET['y'];
            $where_bodys.=$where_body;
            // $where.=" AND i.color_id='".$color_id."'";
        }

        // ENGINE
        if($engine_id!='' && $engine_id!='_'){
            $en_arr=explode('_', $engine_id);
            $where_engine=' AND(';
            for ($i=0; $i < count($en_arr) ; $i++) { 
            	if ($en_arr[$i]!='' && $en_arr!='_') {
            		# code...
            		if ($en_arr[$i]=='other') {
            			$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
            		}else{
                    $where_engine.="i.engine='".$en_arr[$i]."'";

            		}
                    if($i<count($en_arr)-2)
                        $where_engine.=' OR ';
            	}
                
            }
            $where_engine.=')';
            // $s_year=$_GET['y'];
            $where_engines.=$where_engine;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        // FUEL
        if($fuel_id!='' && $fuel_id!='_'){
            $fuel_arr=explode('_', $fuel_id);
            $where_fuel=' AND(';
            for ($i=0; $i < count($fuel_arr) ; $i++) { 
            	if ($fuel_arr[$i]!='' && $fuel_arr[$i]!='_') {
            		# code...
           			if ($fuel_arr[$i]=='other') {
           				$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
           			}else{
                    $where_fuel.="i.fuel='".$fuel_arr[$i]."'";

           			}
                    if($i<count($fuel_arr)-2)
                        $where_fuel.=' OR ';
            	}
                
            }
            $where_fuel.=')';
            // $s_year=$_GET['y'];
            $where_fuels.=$where_fuel;
            // $where.=" AND i.color_id='".$color_id."'";
        }

        // TRANSMISSION
        if($trans_id!='' && $trans_id!='_'){
            $trans_arr=explode('_', $trans_id);
            $where_trans=' AND(';
            for ($i=0; $i < count($trans_arr) ; $i++) { 
            	if ($trans_arr[$i]!='' && $trans_arr[$i]!='_') {
            		# code...
           			if ($trans_arr[$i]=='other') {
           				$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";
           			}else{
                    $where_trans.="i.transmission='".$trans_arr[$i]."'";

           			}
                    if($i<count($trans_arr)-2)
                        $where_trans.=' OR ';
            	}
                
            }
            $where_trans.=')';
            // $s_year=$_GET['y'];
            $where_transs.=$where_trans;
            // $where.=" AND i.color_id='".$color_id."'";
        }

        // TRIM
        if($trim_id!='' && $trim_id!='_'){
            $trim_arr=explode('_', $trim_id);
            $where_trim=' AND(';
            for ($i=0; $i < count($trim_arr) ; $i++) {
            	if ($trim_arr[$i]!='' && $trim_arr[$i]!='_') {
            	 	# code...
           			if ($trim_arr[$i]=='other') {
           				$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
           			}else{

                    $where_trim.="i.trim='".$trim_arr[$i]."'";
           			}
                    if($i<count($trim_arr)-2)
                        $where_trim.=' OR ';
            	 } 
                
            }
            $where_trim.=')';
            // $s_year=$_GET['y'];
            $where_trims.=$where_trim;
            // $where.=" AND i.color_id='".$color_id."'";
        }

        // PARTPLACEMENT
        if($place!='' && $place!='_'){
			$arrpart=explode('_', trim($place,'_'));
            $where_part=' AND(';
            $re.=' Partplacement:<b>';
            for ($i=0; $i < count($arrpart) ; $i++) { 
                     if($arrpart[$i]!='_' && $arrpart[$i]!=''){
                     	if ($arrpart[$i]=='other') {
                     		$re.='Other';
                     		$where_part.="i.part_placement='' OR i.part_placement=0 OR i.part_placement IS NULL";
                     	}else{
							$where_part.="i.part_placement='".$arrpart[$i]."'";
                     	}
						if (count($arrpart)-1>$i) {
							$where_part.=" OR ";
							$re.=', ';
						}
		           	}
            }
            $re.='</b>';
            $where_part.=')';
            $where_place.=$where_part;
		}



		$make_check='';
		$make_data='';
		if ($make_id=='') $make_check='checked';
		$make_data.="<li class='sf_filter'><input type='text' class='sf_input' f='make' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$make_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_make();' id='clear_make_btn'>Clear</li>";
		$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','');  name='ckmake' class='ckmake' $make_check value=''> All</label></li>";
		$make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0 ORDER BY make_name ASC")->result();
		$count=$this->tem->getnumsearch('make_id',$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		foreach ($make as $m) {
			
			$sele='';
			if($make_id==$m->make_id)
				$sele='checked';
			if($count[$m->make_id]>0)
				$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','$m->make_id') name='ckmake' $sele class='ckmake' value='$m->make_id'/> $m->make_name (".$count[$m->make_id].")</label></li>";
		}		
		$make_other=$this->tem->getnumsearch_other('make_id',$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);


		$make_other_check='';
		if ($make_id=='other') {
			$make_other_check='checked';
		}
		if ($make_other>0) {
			$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','other')  name='ckmake' class='ckmake' $make_other_check value='other'> OTHER($make_other)</label></li>";
			
		}

		$model_check='';
		$modeldata='';
		if($model_id=='') $model_check='checked';
		$modeldata.="<li class='sf_filter'><input type='text' class='sf_input' f='model' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$modeldata .="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_model();' class='clear_model_btn'>Clear</li>";
		$modeldata.="<li><label><input type='radio' onclick=search(1,event,0,'model','')  name='ckmodel' class='ckmodel' $model_check value=''> All</label></li>";
		$wheremodel='';
		if($make_id!='')
			$wheremodel=" AND make_id='$make_id'";
		$data=$this->db->query("SELECT * FROM ospos_models WHERE deleted='0' {$wheremodel} ORDER BY model_name")->result();
		$count=$this->tem->getnumsearch('model_id',$where_makes.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		foreach ($data as $mo) {
			$sele='';
			if($model_id==$mo->model_id)
					$sele='checked';
			if($count[$mo->model_id]>0)
				$modeldata.="<li rel='$mo->make_id'><label><input $sele onclick=search(1,event,0,'model','$mo->model_id') type='radio' name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name(".$count[$mo->model_id].")</label></li>";

		}
		$model_other=$this->tem->getnumsearch_other('model_id',$where_makes.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$model_other_check='';
		if ($model_id=='other') {
			$model_other_check='checked';
		}
		if ($model_other>0) {
			$model_data.="<li><label><input type='radio' onclick=search(1,event,0,'model','other')  name='ckmake' class='ckmake' $model_other_check value='other'> OTHER($model_other)</label></li>";
			
		}
		
		$yeararr=explode('_', $year);
		$arrofyear=array();
		for ($i=0; $i <count($yeararr) ; $i++) { 
			$arrofyear[$yeararr[$i]]=$yeararr[$i];
		}

		$year_check='';
		if($year=='' || $year=='_'){
			$year_check='checked';
		}
		$yeardata='';

		
		if ($year_check!='') {
			$year_from=$year_from;
			$year_to = $year_to;
		}else{
			$year_from='';
			$year_to='';
		}
		$yeardata.="
		<div align='center' style='border-bottom:1px solid #21759B;padding-bottom:10px;margin-bottom:5px;'>

			<input maxlength='4' class='year_ft' id='year_from' type='text' style='width:40px;' placeholder='From' onkeyup='chkYear();' value='$year_from'>
			
			<input maxlength='4' class='year_ft' id='year_to' type='text' style='width:40px;' placeholder='To' value='$year_to'>
			<button onclick='yearft();' class='btn btn-primary'>Go</button>
		</div>";
		$yeardata.="<li class='sf_filter'><input type='text' class='sf_input' f='year' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$yeardata.="<li style='float:right;cursor:pointer;color:#21759B' id='clear_year_btn' onclick='clear_year();'>Clear</li>";
		$yeardata.="<li><label><input type='checkbox' onclick=search(1,event,0,'year','') $year_check name='ckyear' class='ckyear' value=''> All</label></li>";
		$count=$this->tem->getnumsearch('year',$where_makes.$where_models.$where_vins.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		
		for($j=date('Y');$j>=1980;$j--){
				$sele='';

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count[$j]>0)
					$yeardata.= "<li><label><input onclick=search(1,event,0,'year','$j') type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j (".$count[$j].")</label></li>";# code...

		}
		$year_other=$this->tem->getnumsearch_other('year',$where_makes.$where_models.$where_vins.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$year_other_check='';
		if (isset($arrofyear['other'])) {
			$year_other_check='checked';
			
		}
		if ($year_other>0) {
			$yeardata.="<li><label><input type='checkbox' onclick=search(1,event,0,'year','other') $year_other_check name='ckyear' class='ckyear' value='other'> OTHER($year_other)</label></li>";
			
		}

		$colorarr=explode('_', $color_id);
		$arrofcolor=array();
		for ($i=0; $i <count($colorarr) ; $i++) { 
			$arrofcolor[$colorarr[$i]]=$colorarr[$i];
		}
		$color_check='';
		if($color_id=='' || $color_id=='_') {
			$color_check='checked';
		}
		$colordata='';
		$colordata.="<li class='sf_filter'><input type='text' class='sf_input' f='color' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$colordata.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_co();' class='clear_co_btn'>Clear</li>";
		$colordata.="<li><label><input type='checkbox' onclick=search(1,event,0,'co','') $color_check name='ckcolor' class='ckcolor' value=''> All</label></li>";
		$color=$this->db->query("SELECT * FROM ospos_colors c WHERE c.deleted=0 ORDER BY c.color_name ASC")->result();
		$count=$this->tem->getnumsearch('color_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
			
			foreach ($color as $col) {
				$sele='';
				if(isset($arrofcolor[$col->color_id]))
					$sele='checked';
				if($count[$col->color_id]>0)
					$colordata.= "<li><label><input type='checkbox' onclick=search(1,event,0,'co','$col->color_id') $sele name='ckcolor' class='ckcolor' value='$col->color_id'/> $col->color_name(".$count[$col->color_id].")</label></li>";# code...
				
			}
		$co_other=$this->tem->getnumsearch_other('color_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$co_other_check='';
		if (isset($arrofcolor['other'])) {
			$co_other_check='checked';
		}
		if ($co_other>0) {
		$colordata.="<li><label><input type='checkbox' onclick=search(1,event,0,'co','other') $co_other_check name='ckcolor' class='ckcolor' value='other'> OTHER($co_other)</label></li>";
			
		}

		// PARTPLACEMENT
		$partarr=explode('_', trim($place,'_'));
		$arrofpart=array();
		for ($i=0; $i <count($partarr) ; $i++) { 
			$arrofpart[$partarr[$i]]=$partarr[$i];
		}
		// print_r($arrofpart);
		$part_check='';
		if($part_id=='' || $part_id=='_') {
			$part_check='checked';
		}
		$partdata='';
		$partdata.="<li class='sf_filter'><input type='text' class='sf_input' f='part' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$partdata.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_part();' class='clear_part_btn'>Clear</li>";
		$partdata.="<li><label><input type='checkbox' onclick=search(1,event,0,'part','') $part_check name='ckpart' class='ckpart' value=''> All</label></li>";
		$part=$this->db->query("SELECT partplacement_id,partplacement_name FROM ospos_partplacements p WHERE p.deleted=0 ORDER BY p.partplacement_name ASC")->result();
		$count=$this->tem->getnumsearch('part_placement',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_location.$where_s_all.$where_s);
		// print_r($count);
			foreach ($part as $pa) {
				$sele='';
				if(isset($arrofpart[$pa->partplacement_id]))
					$sele='checked';
				if($count[$pa->partplacement_id]>0)
					$partdata.= "<li><label><input type='checkbox' onclick=search(1,event,0,'part','$pa->partplacement_id') $sele name='ckpart' class='ckpart' value='$pa->partplacement_id'/> $pa->partplacement_name(".$count[$pa->partplacement_id].")</label></li>";# code...
				
			}
		$pa_other=$this->tem->getnumsearch_other('part_placement',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_location.$where_s_all.$where_s);

		$pa_other_check='';
		if (isset($arrofpart['other'])) {
			$pa_other_check='checked';
		}
		if ($pa_other>0) {
		$partdata.="<li><label><input type='checkbox' onclick=search(1,event,0,'part','other') $pa_other_check name='ckpart' class='ckpart' value='other'> OTHER($pa_other)</label></li>";
			
		}
		// BODY
		$arrbody=explode('_', $body_id);
        $arrofbody=array();
        for ($i=0; $i < count($arrbody) ; $i++) { 
			$arrofbody[$arrbody[$i]]=$arrbody[$i];     
        }
		$body_check='';
		if($body_id=='' || $body_id=='_') {
			$body_check='checked';
		}
		$body_data='';
		$body_data.="<li class='sf_filter'><input type='text' class='sf_input' f='body' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$body_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_body();' class='clear_body_btn'>Clear</li>";
		$body_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'body','') $body_check name='ckbody' class='ckbody' value=''> All</label></li>";
		$body=$this->db->query("SELECT body_id,body_name FROM ospos_body b
								WHERE b.deleted=0 ORDER BY body_name ASC")->result();
		$count=$this->tem->getnumsearch('body',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($body as $bo) {
				$sele='';
				if(isset($arrofbody[$bo->body_id]))
					$sele='checked';
				if($count[$bo->body_id]>0)
					$body_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'body','$bo->body_id') $sele name='ckbody' class='ckbody' value='$bo->body_id'/> $bo->body_name(".$count[$bo->body_id].")</label></li>";# code...
				
			}
		$body_other=$this->tem->getnumsearch_other('body',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		// var_dump($body_other);
		$body_other_check='';
		if (isset($arrofbody['other'])) {
			$body_other_check='checked';
		}
		// echo $body_other_check;
		// echo $body_other;
		if ($body_other>0) {
		$body_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'body','other') $body_other_check name='ckbody' class='ckbody' value='other'> OTHER($body_other)</label></li>";
			
		}
		// ENGINE
        $arrengine=explode('_', $engine_id);
        $arrofengine=array();
        for ($i=0; $i < count($arrengine) ; $i++) { 
			$arrofengine[$arrengine[$i]]=$arrengine[$i];     
        }
		$engine_check='';
		if($engine_id=='' || $engine_id=='_') {
			$engine_check='checked';
		}
		$engine_data='';
		$engine_data.="<li class='sf_filter'><input type='text' class='sf_input' f='engine' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$engine_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_engine();' class='clear_engine_btn'>Clear</li>";
		$engine_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'engine','') $engine_check name='ckengine' class='ckengine' value=''> All</label></li>";
		$engine=$this->db->query("SELECT engine_id,engine_name FROM ospos_engine e
								WHERE e.deleted=0 ORDER BY engine_name ASC")->result();
		$count=$this->tem->getnumsearch('engine',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($engine as $en) {
				$sele='';
				if(isset($arrofengine[$en->engine_id]))
					$sele='checked';
				if($count[$en->engine_id]>0)
					$engine_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'engine','$en->engine_id') $sele name='ckengine' class='ckengine' value='$en->engine_id'/> $en->engine_name(".$count[$en->engine_id].")</label></li>";# code...
				
			}
		$engine_other=$this->tem->getnumsearch_other('engine',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		$engine_other_check='';
		if (isset($arrofengine['other'])) {
			$engine_other_check='checked';
		}
		if ($engine_other>0) {
		$engine_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'engine','other') $engine_other_check name='ckengine' class='ckengine' value='other'> OTHER($engine_other)</label></li>";
			
		}

		// FUEL

        $arrfuel=explode('_', $fuel_id);
        $arroffuel=array();
        for ($i=0; $i < count($arrfuel) ; $i++) { 
			$arroffuel[$arrfuel[$i]]=$arrfuel[$i];     
        }
		$fuel_check='';
		if($fuel_id=='' || $fuel_id=='_') {
			$fuel_check='checked';
		}
		$fuel_data='';
		$fuel_data.="<li class='sf_filter'><input type='text' class='sf_input' f='fuel' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$fuel_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_fuel();' class='clear_fuel_btn'>Clear</li>";
		$fuel_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','') $fuel_check name='ckfuel' class='ckfuel' value=''> All</label></li>";
		$fuel=$this->db->query("SELECT fuel_id,fule_name FROM ospos_fuel f
								WHERE f.deleted=0 ORDER BY fule_name ASC")->result();
		$count=$this->tem->getnumsearch('fuel',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($fuel as $fu) {
				$sele='';
				if(isset($arroffuel[$fu->fuel_id]))
					$sele='checked';
				if($count[$fu->fuel_id]>0)
					$fuel_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','$fu->fuel_id') $sele name='ckfuel' class='ckfuel' value='$fu->fuel_id'/> $fu->fule_name(".$count[$fu->fuel_id].")</label></li>";# code...
				
			}
		$fuel_other=$this->tem->getnumsearch_other('fuel',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		$fuel_other_check='';
		if (isset($arroffuel['other'])) {
			$fuel_other_check='checked';
		}
		if ($fuel_other>0) {
		$fuel_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','other') $fuel_other_check name='ckfuel' class='ckfuel' value='other'> OTHER($fuel_other)</label></li>";
			
		}
			// TRANSMISSION
	
        $arrtrans=explode('_', $trans_id);
        $arroftrans=array();
        for ($i=0; $i < count($arrtrans) ; $i++) { 
			$arroftrans[$arrtrans[$i]]=$arrtrans[$i];     
        }
	
		$trans_check='';
		if($trans_id=='' || $trans_id=='_') {
			$trans_check='checked';
		}
		$trans_data='';
		$trans_data.="<li class='sf_filter'><input type='text' class='sf_input' f='trans' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$trans_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_trans();' class='clear_trans_btn'>Clear</li>";
		$trans_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trans','') $trans_check name='cktrans' class='cktrans' value=''> All</label></li>";
		$trans=$this->db->query("SELECT trans_id,transmission_name FROM ospos_transmission t
								WHERE t.deleted=0 ORDER BY transmission_name ASC")->result();
		$count=$this->tem->getnumsearch('transmission',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($trans as $tr) {
				$sele='';
				if(isset($arroftrans[$tr->trans_id]))
					$sele='checked';
				if($count[$tr->trans_id]>0)
					$trans_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'trans','$tr->trans_id') $sele name='cktrans' class='cktrans' value='$tr->trans_id'/> $tr->transmission_name(".$count[$tr->trans_id].")</label></li>";# code...
				
			}
		$trans_other=$this->tem->getnumsearch_other('transmission',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		$trans_other_check='';
		if (isset($arroftrans['other'])) {
			$trans_other_check='checked';
		}
		if ($trans_other>0) {
		$trans_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trans','other') $trans_other_check name='cktrans' class='cktrans' value='other'> OTHER($trans_other)</label></li>";
			
		}

				// TRIM
        $arrtrim=explode('_', $trim_id);
        $arroftrim=array();
        for ($i=0; $i < count($arrtrim) ; $i++) { 
			$arroftrim[$arrtrim[$i]]=$arrtrim[$i];     
        }
	   
		$trim_check='';
		if($trim_id=='' || $trim_id=='_') {
			$trim_check='checked';
		}
		$trim_data='';
		$trim_data.="<li class='sf_filter'><input type='text' class='sf_input' f='trim' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$trim_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_trim();' class='clear_trim_btn'>Clear</li>";
		$trim_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trim','') $trim_check name='cktrim' class='cktrim' value=''> All</label></li>";
		$trim=$this->db->query("SELECT trim_id,trim_name FROM ospos_trim t
								WHERE t.deleted=0 ORDER BY trim_name ASC")->result();
		$count=$this->tem->getnumsearch('trim',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($trim as $tri) {
				$sele='';
				if(isset($arroftrim[$tri->trim_id]))
					$sele='checked';
				if($count[$tri->trim_id]>0)
					$trim_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'trim','$tri->trim_id') $sele name='cktrim' class='cktrim' value='$tri->trim_id'/> $tri->trim_name(".$count[$tri->trim_id].")</label></li>";# code...
				
			}
		$trim_other=$this->tem->getnumsearch_other('trim',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_place.$where_location.$where_s_all.$where_s);

		$trim_other_check='';
		if (isset($arroftrim['other'])) {
			$trim_other_check='checked';
		}
		if ($trim_other>0) {
		$trim_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trim','other') $trim_other_check name='cktrim' class='cktrim' value='other'> OTHER($trim_other)</label></li>";
			
		}
		// LOCATION
		$locarr=explode('_', $location);
		$arrofloc=array();
		for ($i=0; $i <count($locarr) ; $i++) { 
			$arrofloc[$locarr[$i]]=$locarr[$i];
		}
		$loc_check='';
		if($location=='' || $location=='_') {
			$loc_check='checked';
		}
		$locdata='';

		$locdata="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_location();' class='clear_location'>Clear</li>";
		$locdata.="<li><label><input type='checkbox' onclick='search(1,event,0);' $loc_check name='cklocation' class='cklocation' value=''> All</label></li>";
		$location_data=$this->db->query("SELECT location_id,location_name FROM ospos_locations l WHERE l.deleted=0 ORDER BY l.location_name ASC")->result();
		$count=$this->tem->getnumsearch('location_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_cols);
		
			foreach ($location_data as $loc) {
				$sele='';
				if(isset($arrofloc[$loc->location_id]))
					$sele='checked';
				if($count[$loc->location_id]>0)
					$locdata.= "<li><label><input type='checkbox' onclick='search(1,event,0);' $sele name='cklocation' class='cklocation' value='$loc->location_id'/> $loc->location_name(".$count[$loc->location_id].")</label></li>";# code...
				
			}
		$loc_other=$this->tem->getnumsearch_other('location_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_cols);

		$loc_other_check='';
		if (isset($arrofloc['other'])) {
			$loc_other_check='checked';
		}
		if ($loc_other>0) {
		$locdata.="<li><label><input type='checkbox' onclick='search(1,event,0);' $loc_other_check name='cklocation' class='cklocation' value='other'> OTHER($loc_other)</label></li>";
			
		}

		$vin_data='';
		$vin_data.="<div style='margin-top:-1px' align='center'>
						<input type='text' name='v' id='vin_select' onchange='search()' value='$vinnumber' size='18'>
					</div>";

		$arr['vin']=$vin_data;
		$arr['trim']=$trim_data;
		$arr['trans']=$trans_data;
		$arr['fuel']=$fuel_data;
		$arr['engine']=$engine_data;
		$arr['body']=$body_data;
		$arr['ppl']=$partdata;
		$arr['loc']=$locdata;
		$arr['make']=$make_data;
		$arr['model']=$modeldata;
		$arr['year']=$yeardata;
		$arr['color']=$colordata;
		// $arr['ex_color']=$ex_colordata;
		header("Content-type:text/x-json");
		echo json_encode($arr);

	}
	function search_vin(){
		$data['controller_name']=strtolower(get_class());
		$controller_name=$data['controller_name'];
		$search='';
		$make_id='';
		$model_id='';
		$s_year='';
		$in_color_id='';
		$ex_color_id='';
		$where='';
		$pro_sta='';
		$body_id='';
		$engine_id='';
		$fuel_id='';
		$vin='';
		$data['title']="";
		$check_price = $this->input->post('ck_price');


		$filters_applied = array();
		$filter_array = array();

		$cl_f = $this->input->post('cl_f');
		$cl_fid = $this->input->post('cl_fid');

		if ($cl_f) {
			$this->sale_lib->unset_filter_tem($cl_f,$cl_fid);
		}

		$re=' of ';
		if(isset($_POST['search'])){
			$search=$_POST['search'];
			$data['title']="Search Results";
			if ($search!='') {
				$this->sale_lib->add_filter_tem('sl',$search,'s');

				$re.=" Keywords:<b> $search</b>";
				# code...
			}

		}

		if(isset($_POST['m']) && $_POST['m']!='' && $_POST['m']!='undefined' && $_POST['m']!='0'){
			$make_id=$_POST['m'];
			$re.=' Make:<b>';
			$this->sale_lib->add_filter_tem($make_id,$this->item->make_name($make_id)->row()->make_name,'make');

			if ($make_id=='other') {
				$re.='Other';

				$where.=" AND (i.make_id='' OR i.make_id='0' OR i.make_id IS NULL)";
			}else{
				$re.=$this->item->make_name($make_id)->row()->make_name;
				$where.=" AND i.make_id='".$_POST['m']."'";
			}
			$re.='</b>';


		}
		if(isset($_POST['mo']) && $_POST['mo']!=''&& $_POST['mo']!='undefined' && $_POST['mo']!='0'){
			$model_id=$_POST['mo'];
			$re.=' Model:<b>';
			$this->sale_lib->add_filter_tem($model_id,$this->item->model_name($model_id)->row()->model_name,'model');

			if ($model_id=='other') {
				$re.="Other";

				$where.=" AND (i.model_id='' OR i.mdoel_id='0' OR i.model_id IS NULL)";
			}else{
				$re.=$this->item->model_name($model_id)->row()->model_name;

				$where.=" AND i.model_id='".$_POST['mo']."'";
			}
			$re.='</b>';

		}
		if(isset($_POST['vin']) && $_POST['vin']!='' && $_POST['vin']!='undefined' && $_POST['vin']!='0'){
			$vin=$_POST['vin'];
			$re.=' Head:<b>';

			if ($vin!='other') {
				$re.="$vin";

				$where.=" AND i.vinnumber_id='".$_POST['vin']."'";

			}
			else{
				$where.=" AND (i.vinnumber_id='' OR i.vinnumber_id='0' OR i.vinnumber_id IS NULL)";
				
			}
			$re.='</b>';

		}
		if(isset($_POST['y']) && $_POST['y']!='' && $_POST['y']!='_' && $_POST['y']!='undefined' && $_POST['y']!='0'){
			$arryear=explode('_', $_POST['y']);
            $where_year=' AND(';
			$re.=" Year:<b>";

            for ($i=0; $i < count($arryear) ; $i++) { 
            		if ($arryear[$i]!='_' && $arryear[$i]!='') {
						$this->sale_lib->add_filter_tem($arryear[$i],$arryear[$i],'year');

            			if ($arryear[$i]=='other') {
            				$where_year.="i.year='' OR i.year=0 OR i.year IS NULL";

            				$re.="Other";
            				
            			}else{
            				$where_year.="i.year='".$arryear[$i]."' OR f.year_tag LIKE '%".$arryear[$i]."%'";
            				$re.=$arryear[$i];

            			}
	                    if(count($arryear)-2>$i){
	                        $where_year.=' OR ';
	                        $re.=', ';

	                    }
            		}
                    

            }
            $re.='</b>';

            $where_year.=')';
            $s_year=$_POST['y'];
            $where.=$where_year;

		}
		if(isset($_POST['c']) && $_POST['c']!='' && $_POST['c']!='_' && $_POST['c']!='undefined' && $_POST['c']!='0'){
			$arrcol=explode('_', $_POST['c']);
            $where_color=' AND(';
            $re.=' Color:<b>';

            for ($i=0; $i < count($arrcol) ; $i++) { 
                	if($arrcol[$i]!='_' && $arrcol[$i]!=''){
						$this->sale_lib->add_filter_tem($arrcol[$i],$this->item->color_name($arrcol[$i])->row()->color_name,'co');

                		if ($arrcol[$i]=='other') {
            				$where_color.="i.color_id='' OR i.color_id=0 OR i.color_id IS NULL";
                			$re.='Other';
                			
                		}else{
							$where_color.="i.color_id='".$arrcol[$i]."'";
							$re.=$this->item->color_name($arrcol[$i])->row()->color_name;

                		}
						if (count($arrcol)-2>$i) {
							$where_color.=" OR ";
							$re.=', ';

						}
		           	}
            }
		    $re.='</b>';
            $where_color.=')';
            $color_id=$_POST['c'];
            // echo $in_where_color;
            $where.=$where_color;

		}
		
		$year_from = $_POST['year_from'];
		$year_to = $_POST['year_to'];
		$yearft='';
		if ($_POST['year_from']!='' && $_POST['year_to']!='') {
			$this->sale_lib->add_filter_tem('yft',"$year_from - $year_to",'yearrange');

			$yearft .= " AND i.year BETWEEN $year_from AND $year_to";
			$where.=$yearft;
			$re.=" Year:from <b>$year_from</b> to <b>$year_to</b>";

		}

		if(isset($_POST['body']) && $_POST['body']!='' && $_POST['body']!='_' && $_POST['body']!='undefined' && $_POST['body']!='0'){
			$arrbody=explode('_', $_POST['body']);
            $where_body=' AND(';
            $re.=' Body:<b>';
            for ($i=0; $i < count($arrbody) ; $i++) { 
                     if($arrbody[$i]!='_' && $arrbody[$i]!=''){
						$this->sale_lib->add_filter_tem($arrbody[$i],$this->item->body_name($arrbody[$i])->row()->body_name,'body');

                     	if ($arrbody[$i]=='other') {
                     		$re.='Other';
                     		$where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
                     	}else{
							$where_body.="i.body='".$arrbody[$i]."'";
							$re.=$this->item->body_name($arrbody[$i])->row()->body_name;
                     	}
						if (count($arrbody)-2>$i) {
							$where_body.=" OR ";
							$re.=', ';
						}
		           	}
            }
            $re.='</b>';
            $where_body.=')';
            $where.=$where_body;
		}
		if(isset($_POST['ppl']) && $_POST['ppl']!='' && $_POST['ppl']!='_' && $_POST['ppl']!='undefined' && $_POST['ppl']!='0'){
			$arrpart=explode('_', trim($_POST['ppl'],'_'));
			// print_r($arrpart); die();
			$total_ppl= count($arrpart);
            $where_part=' AND(';
            $re.=' Partplacement:<b>';
            for ($i=0; $i < count($arrpart) ; $i++) { 
                     if($arrpart[$i]!='_' && $arrpart[$i]!=''){
						$this->sale_lib->add_filter_tem($arrpart[$i],$this->item->part_placement_name($arrpart[$i])->row()->partplacement_name,'part');

                     	if ($arrpart[$i]=='other') {
                     		$re.='Other';
                     		$where_part.="i.part_placement='' OR i.part_placement=0 OR i.part_placement IS NULL";
                     	}else{
							$re.=$this->item->part_placement_name($arrpart[$i])->row()->partplacement_name;
							$where_part.="i.part_placement='".$arrpart[$i]."'";
                     	}
                     	// echo $
						if ($total_ppl-1>$i) {
							$where_part.=" OR ";
							$re.=', ';
						}
		           	}
            }
            $re.='</b>';
            $where_part.=')';
            $where.=$where_part;
		}

		if(isset($_POST['engine']) && $_POST['engine']!='' && $_POST['engine']!='_' && $_POST['engine']!='undefined' && $_POST['engine']!='0'){
			$arrengine=explode('_', $_POST['engine']);
            $where_engine=' AND(';
            $re.=' Engine:<b>';
            for ($i=0; $i < count($arrengine) ; $i++) { 
                    if($arrengine[$i]!='_' && $arrengine[$i]!=''){
						$this->sale_lib->add_filter_tem($arrengine[$i],$this->item->engine_name($arrengine[$i])->row()->engine_name,'engine');

                    	if ($arrengine[$i]=='other') {
                    		$re.='Other';
                    		$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
                    	}else{
                    		$re.=$this->item->engine_name($arrengine[$i])->row()->engine_name;
							$where_engine.="i.engine='".$arrengine[$i]."'";
                    	}
						if (count($arrengine)-2>$i) {
							$re.=', ';
							$where_engine.=" OR ";
						}
		           	}
                
            }
            $re.='</b>';
            $where_engine.=')';
            $where.=$where_engine;

		}
		if(isset($_POST['fuel']) && $_POST['fuel']!='' && $_POST['fuel']!='_' && $_POST['fuel']!='undefined' && $_POST['fuel']!='0'){
			$arrfuel=explode('_', $_POST['fuel']);
            $where_fuel=' AND(';
            $re.=' Fuel:<b>';
            for ($i=0; $i < count($arrfuel) ; $i++) { 
                    if($arrfuel[$i]!='_' && $arrfuel[$i]!=''){
						$this->sale_lib->add_filter_tem($arrfuel[$i],$this->item->fuel_name($arrfuel[$i])->row()->fule_name,'fuel');

                    	if ($arrfuel[$i]=='other') {
                    		$re.='Other';
                    		$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
                    	}else{
							$where_fuel.="i.fuel='".$arrfuel[$i]."'";
							$re.=$this->item->fuel_name($arrfuel[$i])->row()->fule_name;
                    	}
						if (count($arrfuel)-2>$i) {
							$where_fuel.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_fuel.=')';
            $where.=$where_fuel;

		}
		if(isset($_POST['trans']) && $_POST['trans']!='' && $_POST['trans']!='_' && $_POST['trans']!='undefined' && $_POST['trans']!='0'){
			$arrtrans=explode('_', $_POST['trans']);
            $where_trans=' AND(';
            $re.=' Transmission:<b>';
            for ($i=0; $i < count($arrtrans) ; $i++) { 
                    if($arrtrans[$i]!='_' && $arrtrans[$i]!=''){
						$this->sale_lib->add_filter_tem($arrtrans[$i],$this->item->trans_name($arrtrans[$i])->row()->transmission_name,'trans');

                    	if ($arrtrans[$i]=='other') {
                    		$re.='Other';
                    		$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";
                    	}else{
							$where_trans.="i.transmission='".$arrtrans[$i]."'";
							$re.=$this->item->trans_name($arrtrans[$i])->row()->transmission_name;
                    	}
						if (count($arrtrans)-2>$i) {
							$where_trans.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_trans.=')';
            $where.=$where_trans;

		}

		if(isset($_POST['trim']) && $_POST['trim']!='' && $_POST['trim']!='_' && $_POST['trim']!='undefined' && $_POST['trim']!='0'){
			$arrtrim=explode('_', $_POST['trim']);
            $where_trim=' AND(';
            $re.=' Trim:<b>';
            for ($i=0; $i < count($arrtrim) ; $i++) { 
                    if($arrtrim[$i]!='_' && $arrtrim[$i]!=''){
						$this->sale_lib->add_filter_tem($arrtrim[$i],$this->item->trim_name($arrtrim[$i])->row()->trim_name,'trim');

                    	if ($arrtrim[$i]=='other') {
                    		$re.="Other";
                    		$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
                    	}else{
							$where_trim.="i.trim='".$arrtrim[$i]."'";
							$re.=$this->item->trim_name($arrtrim[$i])->row()->trim_name;

                    	}
						if (count($arrtrim)-2>$i) {
							$where_trim.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_trim.=')';
            $where.=$where_trim;

		}
		// LOCATION
		if(isset($_POST['loc']) && $_POST['loc']!='' && $_POST['loc']!='_' && $_POST['loc']!='undefined' && $_POST['loc']!='0'){
			$arrloc=explode('_', $_POST['loc']);
            $where_loc=' AND(';
            $re.=' Location:<b>';
            for ($i=0; $i < count($arrloc) ; $i++) { 
                	if($arrloc[$i]!='_' && $arrloc[$i]!=''){
                		if ($arrloc[$i]=='other') {
            				$where_loc.="i.location_id='' OR i.location_id=0 OR i.location_id IS NULL";
                			$re.='Other';
                		}else{
							$where_loc.="i.location_id='".$arrloc[$i]."'";
							$re.=$this->item->location_name($arrloc[$i])->row()->location_name;
                		}
						if (count($arrloc)-2>$i) {
							$where_loc.=" OR ";
							$re.=', ';
						}
		           	}
            }
		    $re.='</b>';
            $where_loc.=')';
            $location_id=$_POST['loc'];
            $where.=$where_loc;
            // echo $where_loc;die();
		}

		$s = $this->input->post('search_all');
		$match = '';
		// $order_by = ' i.item_id DESC';
		$where_match = '';
		if ($s) {
			$this->sale_lib->add_filter_tem('sa',$s,'sa');
			
			$ars = explode(' ', $s);
			for ($i=0; $i < count($ars); $i++) { 
				$where_match.= "AND(
									i.part_number LIKE '%$ars[$i]%' OR
									i.part_name LIKE '%$ars[$i]%' OR
									i.make_name LIKE '%$ars[$i]%' OR
									i.model_name LIKE '%$ars[$i]%' OR
									i.year LIKE '%$ars[$i]%' OR
									i.color_name LIKE '%$ars[$i]%' OR
									i.condition_name LIKE '%$ars[$i]%'
									)";
			}

			$where.= $where_match;
			
		}
		// SORTING
		$stype = $this->input->post('dud');
		$sfield = $this->input->post('dsn');
		if ($stype && $sfield) {
			$order_by = "ORDER BY i.$sfield $stype";
			if ($sfield=='barcode') {
				$order_by = "ORDER BY i.part_number $stype";
			}
		}

		$sql="SELECT 
					 i.partid,
					 i.part_number,
					 i.part_name,
					 i.khmername_name,
					 i.fit,
					 i.make_name,
					 i.model_name,
					 i.year,
					 i.color_name,
					 i.condition_name,
					 i.approve,
					 i.make_id,
					 i.model_id,
					 i.color_id,
					 i.year_to,
					 i.part_placement,
					 i.location_id,
					 i.body,
					 i.engine,
					 i.fuel,
					 i.transmission,
					 i.trim,
					 i.vinnumber_id,
					 i.cost_price,
					 i.unit_price,
					 i.unit_price_to
		FROM ospos_part i 
		LEFT JOIN ospos_part_fitment f ON i.partid=f.part_id
	            WHERE 1=1
	            AND (i.part_name LIKE '%$search%' 
					OR i.make_name LIKE '%$search%' 
					OR i.model_name LIKE '%$search%' 
					OR i.color_name LIKE '%$search%'
					OR i.year LIKE '%$search%' 
					OR i.part_number LIKE '%$search%'
					OR i.fit LIKE '%$search%'
					OR i.condition_name LIKE '%$search%')
		{$where}
		GROUP BY i.partid
		$order_by";
		


		// $sql_count = "SELECT 
		// 			 i.partid,
		// 			 i.part_number,
		// 			 i.part_name,
		// 			 i.khmername_name,
		// 			 i.fit,
		// 			 i.make_name,
		// 			 i.model_name,
		// 			 i.year,
		// 			 i.color_name,
		// 			 i.condition_name,
		// 			 i.approve,
		// 			 i.make_id,
		// 			 i.model_id,
		// 			 i.color_id,
		// 			 i.year_to,
		// 			 i.part_placement,
		// 			 i.location_id,
		// 			 i.body,
		// 			 i.engine,
		// 			 i.fuel,
		// 			 i.transmission,
		// 			 i.trim,
		// 			 i.vinnumber_id
		// FROM ospos_part i 
	 //            WHERE 1=1
	 //            AND (i.part_name LIKE '%$search%' 
		// 			OR i.make_name LIKE '%$search%' 
		// 			OR i.model_name LIKE '%$search%' 
		// 			OR i.color_name LIKE '%$search%'
		// 			OR i.year LIKE '%$search%' 
		// 			OR i.part_number LIKE '%$search%'
		// 			OR i.fit LIKE '%$search%'
		// 			OR i.condition_name LIKE '%$search%')";
		
		
		$totalrow=$this->db->query($sql)->result();
		// $this->session->unset_userdata("query_result_temp");
		// $this->session->set_userdata("query_result_temp",$sql_count);
		// ++++++++++++++++pagination++++++++++++
		$page_num=10;
		$per_page='';
		if(isset($_POST['p_num']) && $_POST['p_num']!='undefined' && $_POST['p_num']!='0' && $_POST['p_num']!='')
			$page_num=$_POST['p_num'];

		if(isset($_POST['perpage']))
			$per_page=$_POST['perpage'];
		$data['total_rows'] =count($this->db->query($sql)->result());
		
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("template/search_vin"),$page_num);
		$limit='';
		if ($page_num!='all') {
			$limit=" limit ".$paging['limit'];
		// if($per_page>0){
			$limit=" LIMIT {$paging['start']}, {$paging['limit']}";
		// }
		}
		
		$sql.=" {$limit}";
		$table='';
		foreach ($this->db->query($sql)->result() as $make) {
           $width = $this->get_form_width();
           // $width = '';
		$img_name=$this->tem->getimage($make->partid)->image_name;
		$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
		if ($img_name) {
			if(file_exists(FCPATH."uploads/thumb/".$img_name)){
				$im=array('src'=>'uploads/thumb/'.$img_name.'?'.rand(0,999),'width'=>"120");
		        //$img_path=img("uploads/thumb/".$make->tem_image_id.'.jpg');
		    }
		}
		

	 
	     $img_path=img($im);
	     $class='';
	    $approve='Approved';
	    if($make->approve==0){
	    	$approve='Pending';
	    	$class='mark';
	    }
	    $count_stock = count($this->tem->get_part_item($make->part_number));
	     // var_dump($make);
	    if ($check_price==1) {
	    	$table.='<tr class="'.$class.'">';
			$table.='<td width="15%"><a data-fancybox href='.'uploads/'.$img_name.'>'.$img_path.'</a></td>';
			$table.='<td width="15%">'.$make->part_number.'<input id="part_id" type="hidden" value='.$make->partid.'>'.$_GET['act'].'</td>';
			$table.='<td width="20%">'.$make->khmername_name.' / '.$make->part_name.'</td>';
			// $table.='<td width="20%">'.$make->fit.'</td>';
			$table.='<td width="20%">'.$make->make_name.'</td>';
			$table.='<td width="20%">'.$make->model_name.'</td>';
			$table.='<td width="20%">'.$make->year.'</td>';
			$table.='<td width="20%">'.$make->color_name.'</td>';
			$table.='<td width="20%">$'.number_format($make->cost_price,2).'</td>';
			$table.='<td width="20%">$'.number_format($make->unit_price,2).'</td>';
			$table.='<td width="20%">$'.number_format($make->unit_price_to,2).'</td>';
			$table.='<td width="20%">'.$make->condition_name.'</td>';
			$table.='<td width="20%">'.$approve.'</td>';
			$table.='<td>'.anchor("$controller_name/view_tem_history/$make->part_number",
				"History",
				array('class'=>'thickbox none','title'=>'Sale History')).'</td>';			
			

			$table.='</tr>';
	    }else{
	    	$table.='<tr class="'.$class.'">';
			$table.="<td width='3%'><input type='checkbox' id='make_$make->partid' value='".$make->partid."'/></td>";
			$table.='<td width="15%"><a data-fancybox href='.'uploads/'.$img_name.'>'.$img_path.'</a></td>';
			
			$table.='<td width="15%">'.$make->part_number.'<input id="part_id" type="hidden" value='.$make->partid.'>'.$_GET['act'].'</td>';
			$table.='<td width="20%">'.$make->khmername_name.' / '.$make->part_name.'</td>';
			// $table.='<td width="20%">'.$make->fit.'</td>';
			$table.='<td width="20%">'.$make->make_name.'</td>';
			$table.='<td width="20%">'.$make->model_name.'</td>';
			$table.='<td width="20%">'.$make->year.'</td>';
			$table.='<td width="20%">'.$make->color_name.'</td>';
			$table.='<td width="20%">'.$make->condition_name.'</td>';
			$table.='<td width="20%">'.$approve.'</td>';
			$table.='<td width="5%">'.anchor($controller_name."/view/$make->partid/width:720", $this->lang->line('common_edit'),array('class'=>'thickbox disable_select','title'=>$this->lang->line($controller_name.'_update'))).'</td>';			

			$table.='<td width="5%">'.anchor("$controller_name/add_item_form/$make->partid/width:260",
										"Add Item",
										array('class'=>'thickbox none','title'=>'Add Item Based on '.$make->part_number)).'</td>';

			$table.='<td>'.anchor("$controller_name/view_tem_history/$make->part_number",
				"History",
				array('class'=>'thickbox none','title'=>'Sale History')).'</td>';			
			$table.='<td>'.anchor("$controller_name/view_tem_stock/$make->part_number",
				"View Stock($count_stock)",
				array('class'=>'thickbox none','title'=>'View Stock')).'</td>';

			$table.='</tr>';
	    }
		

		}
		$data['se']=1;
		$val=0;
		$per=0;
		if ($limit!='') {
			$val= $paging['limit'];
			$per=$paging['start'];
		}
		
		$total_rows = $data['total_rows'];
		if ($per=='') {
			$r1=1;
		}else{
			$r1=$per+1;
		}
		if ($per+$val<$total_rows && $per+$val!=0) {
			$r2 = $per+$val;
		}else{
			$r2 = $total_rows;
		}

		$results="<p>Showing ".$r1." to ".$r2." of ".$total_rows." results";
		// if ($re!=' of ') {
		// 	$results.=$re;
		// }
		$results.="</p>";

		$new_button = anchor("$controller_name/view/-1/width:720",
		"<div class='big_button' style='float: left;'><span>NEW</span></div>",
		array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));

		$ft_ap = '<b>Filters Applied: </b>';
		$current_filter = $this->sale_lib->get_filter_tem();
		foreach ($current_filter as $fi) {
			if ($fi['fi']!='sa') {
				$ft_ap.=' <span class="ft_'.$fi['fi'].'" onclick=clear_'.$fi['fi'].'("'.$fi['fid'].'")>';
				$ft_ap.=$fi['fname'];
				$ft_ap.='&nbsp;<i class="fa fa-times-circle"></i></span>';
			}
			
		}
		if ($ft_ap=='<b>Filters Applied: </b>') {
			$ft_ap='';
		}

		$move_btn = anchor("$controller_name/view_move","Move To Vinnumber",array('class'=>'thickbox none','title'=>'Confirm Moving'));
		$data['ck_price'] = $check_price;
		$data['move_btn'] = $move_btn;
		$data['new_btn'] = $new_button;
		$data['result_row'] = $results;
		$data['data']=$table;
		$data['nodata']="No Results";
		$data['pagination']=$paging;
		$data['filters_applied'] = $ft_ap;
		$data['real_filter'] = $current_filter;

		
		echo json_encode($data);
		// $this->load->view('partial/header',$data);
		// $this->load->view('vinnumbers/search_result',$data);
		// $this->load->view('partial/footer');
	}
	function view_tem_stock($partnumber){
		$data['partnumber'] = urldecode($partnumber);
		$data['stock_data'] = $this->tem->get_part_item(urldecode($partnumber));
		$this->load->view('template/view_stock',$data);
	}
	function move_template(){
		$vin_id = $this->input->post('vin_id');
		$template = $this->input->post('tem_id');
		for ($i=0; $i < count($template); $i++) { 
			$this->db->where('partid',$template[$i])->update('part',array('vinnumber_id'=>$vin_id));
		}
		$data['t'] = $t;

		echo json_encode($data);
	}

	function get_part_fitment(){
		$s = $this->input->post('search');
		$partid = $this->input->post('partid');
		$fit_data ='';
		$where = '';
		if ($s!='') {
			$where .=" AND year_tag LIKE '%$s%'";
		}
		if ($partid!='') {
			$where.=" AND part_id=$partid";
		}else{
			$where.=" AND part_id=0";
		}
		$fitment = $this->db->query("SELECT * FROM ospos_part_fitment WHERE deleted=0 {$where}")->result();
		$i=0;
		foreach ($fitment as $fit) {
			$i++;
			$fit_data.="<tr>
							<td>$i</td>
							<td>".$this->item->make_name($fit->make_id)->row()->make_name."</td>
							<td>".$this->item->model_name($fit->model_id)->row()->model_name."</td>
							<td>$fit->year_from</td>
							<td>$fit->year_to</td>
							<td><button onclick='delete_fitment($fit->part_fitment_id)'>Delete</button></td>
						</tr>";
		}

		$data['fitment_data'] = $fit_data;
		echo json_encode($data);
	}
	function get_model_fit(){
		$make_id = $this->input->post('make_id');
		$data['model_data'] = '<option value="" >Please Select Make</option>';
		$modata = '';
		if ($make_id) {
			$sql = $this->db->query("SELECT * FROM ospos_models WHERE make_id = $make_id")->result();
			foreach ($sql as $model) {
				$modata.="<option value='$model->model_id'>$model->model_name</option>";
			}
		$data['model_data'] = $modata;

		}
		echo json_encode($data);
	}
	function delete_fitment(){
		$id = $this->input->post('pfid');
		if ($id!='') {
			$this->db->where('part_fitment_id',$id)->delete('part_fitment');
		}
	}
	function add_fitment(){
		$part_id = $this->input->post('partid')?$this->input->post('partid'):0;
		$fit_make = $this->input->post('fit_make');
		$fit_model = $this->input->post('fit_model');
		$fit_year = $this->input->post('fit_year');
		$fit_year_to = $this->input->post('fit_year_to');
		$fit_year_tag = '';
		for ($i=$fit_year; $i <= $fit_year_to; $i++) { 
			$fit_year_tag.= $i;
			if ($i<$fit_year_to) {
				$fit_year_tag.=",";
			}
		}

		$def_make = $this->input->post('def_make');
		$def_model = $this->input->post('def_model');
		$def_year = $this->input->post('def_year');
		$def_year_to = $this->input->post('def_year_to');
		$def_year_tag = '';
		for ($i=$def_year; $i <= $def_year_to; $i++) { 
			$def_year_tag.= $i;
			if ($i<$def_year_to) {
				$def_year_tag.=",";
			}
		}

		$data_fitment = array(
						'part_id'=>$part_id,
						'make_id'=>$fit_make,
						'model_id'=>$fit_model,
						'year_from'=>$fit_year,
						'year_to'=>$fit_year_to,
						'year_tag'=>$fit_year_tag,
						'def_make'=>$def_make,
						'def_model'=>$def_model,
						'def_year'=>$def_year,
						'def_year_to'=>$def_year_to,
						'def_year_tag'=>$def_year_tag,
						'deleted'=>0
						);
		
		$this->db->insert('part_fitment',$data_fitment);
		$data['s']= 's';

		echo json_encode($data);
	}
	function remove_tem_fitment(){
		$this->db->where('part_id',0)->delete('part_fitment');

		// echo json_encode('s');
	}
	function update_fitment($part_id,$data){

		$this->db->where('part_id',0)->update('part_fitment',$data);
	}


	
	function check_part(){
		$pn = $this->input->post('pn');
		$pid = $this->input->post('pid');

		$exist = $this->tem->check_part($pn,$pid);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}



	/////////////////////RUN MANUALLY
	function delete_duplicate_template(){
		// CHECK ITEM BEFORE RUN
		$dup = $this->db->query("SELECT part_number, COUNT(*) c FROM ospos_part GROUP BY part_number HAVING c > 1")->result();

    	foreach ($dup as $d) {
    		$this->db->where('part_number',$d->part_number)->delete('ospos_part');
    	}
	}

	function update_non_exist_name(){
		header('Content-Type: text/html; charset=utf-8');
		$no_name_id_item = $this->db->query("SELECT * FROM ospos_part WHERE khmernames_id = 0 OR khmernames_id IS NULL")->result();

		foreach ($no_name_id_item as $item) {
			$has_name = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_name = '$item->part_name' OR english_name = '$item->part_name'")->row();

			if ($has_name) {
				$udata = array(
							'khmernames_id'=>$has_name->khmername_id,
							'khmername_name' => $has_name->khmername_name
						);
				$this->db->where('partid',$item->partid)->update('ospos_part',$udata);
			}else{
				$dcr =  date('Y-m-d H:i:s');
				$idata = array(
								"khmername_name"=>$item->part_name,
								"english_name"=>$item->part_name,
								"date_create"=>$dcr,
								"deleted"=>0
							);
				$this->db->insert('ospos_khmernames',$idata);
				$kh_id = $this->db->insert_id();
				if ($kh_id) {
					$kh_name = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_id = '$kh_id'")->row();
					$udata = array(
							'khmernames_id'=>$kh_name->khmername_id,
							'khmername_name' => $kh_name->khmername_name
						);
					$this->db->where('partid',$item->partid)->update('ospos_part',$udata);
				}
			}
		}
		redirect(site_url('template'));
	}

	function add_item_form($partid){
		$part = $this->db->query("SELECT * FROM ospos_part WHERE partid = '$partid'")->row();
		$data['part_info'] = $part;
		$data['part_number'] = $part->part_number;
		$data['vinnumber'] = $this->db->query("SELECT * FROM ospos_vinnumbers WHERE deleted = 0")->result();
		$this->load->view("template/add_item_form",$data);

	}
	function add_to_item($partid){
		$head_number = $this->input->post('head_number');
		$qty = $this->input->post('quantity');
		if ($qty<=0) {
			$this->session->set_flashdata('er',"Quantity Can't Be 0");
			redirect(site_url('template'));
		}

		$part = $this->db->query("SELECT * FROM ospos_part WHERE partid = '$partid'")->row();

		for ($i=1; $i <= $qty; $i++) { 
			$item_number = $part->part_number;
			$khmername_id = $part->khmernames_id;
			$category_id = $part->category_id;
			$make_id = $part->make_id;
			$make_name = $this->item->make_name($make_id)->row()->make_name;
			$model_id = $part->model_id;
			$model_name = $this->item->model_name($model_id)->row()->model_name;
			$year = $part->year;
			$body = $part->body;
			$engine = $part->engine;
			$transmission = $part->transmission;
			$trim = $part->trim;
			$fuel = $part->fuel;
			$partplacement_id = $part->part_placement;
			$partplacement_name = $this->item->part_placement_name($partplacement_id)->row()->partplacement_name;
			$cost_price = 0;
			$unit_price = $part->unit_price_to;
			$quantity = 1;
			$vinnumber_id = NULL;
			$category = '';
			$add_date = date('Y-m-d');
			$name = $this->item->khmer_name($khmername_id)->row()->english_name;
			$khmer_name = $this->item->khmer_name($khmername_id)->row()->khmername_name;


			if ($head_number!='') {
				$head_info = $this->db->query("SELECT * FROM ospos_vinnumbers WHERE vinnumber_id='$head_number'")->row();
				$make_id = $head_info->make_id;
				$make_name = $this->item->make_name($make_id)->row()->make_name;
				$model_id = $head_info->model_id;
				$model_name = $this->item->model_name($model_id)->row()->model_name;
				$year = $head_info->year;
				$vinnumber_id = $head_info->vinnumber_id;
				$category = $head_info->vinnumber_name;
			}



			$to_item = array(
						'name' => $name,
						'khmer_name' => $khmer_name,
						'item_number' => $item_number,
						'khmername_id' => $khmername_id,
						'category_id' => $category_id,
						'make_id' => $make_id,
						'make_name' => $make_name,
						'model_id' => $model_id,
						'model_name' => $model_name,
						'year' => $year,
						'body' => $body,
						'engine' => $engine,
						'transmission' => $transmission,
						'trim' => $trim,
						'fuel' => $fuel,
						'partplacement_id' => $partplacement_id,
						'partplacement_name' => $partplacement_name,
						'cost_price' => $cost_price,
						'unit_price' => $unit_price,
						'quantity' => $quantity,
						'vinnumber_id' => $vinnumber_id,
						'category' => $category,
						'add_date' => $add_date
						);

			$this->db->insert('ospos_items',$to_item);
			$item_id = $this->db->insert_id();

			$make_name_barcode = $this->get2FirstString($make_name);
			$year_barcode = $this->get2lastString($year);
			$model_name_barcode = $this->get2FirstString($model_name);

			$digit_padding=str_pad($item_id,6,"0",STR_PAD_LEFT);
			$barcode=$year_barcode.$make_name_barcode.$model_name_barcode.$digit_padding;
			
			$barcode_item_data = array(
				'category_id'=>$category_id,
				'item_number'=>$item_number,
				'cost_price'=>$cost_price,
				'unit_price'=>$unit_price,
				'quantity'=>1,
				'model_id' => $model_id,
				'year'=>$year,
				'partplacement_id'=>$partplacement_id,
				'make_id'=>$make_id,
				'category'=>$category,
				'barcode'=>$barcode,
				'khmername_id'=>$khmername_id
				);
			$this->Item->updateBarcodeAfterSave($item_id,$barcode_item_data);
		}

		$this->session->set_flashdata('suc',$qty.' Items Added.');
		
		redirect(site_url('template'));
		
	}


}
?>