<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Colors extends Secure_area
{
	function __construct()
	{
		parent::__construct('colors');
	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/colors/index');
		$config['total_rows'] = $this->Color->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_colors_manage_table( $this->Color->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('colors/manage',$data);
	}


	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_colors_manage_table_data_rows($this->Color->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Color->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$color_id = $this->input->post('row_id');
		$data_row=get_color_data_row($this->Color->get_info($color_id),$this);
		echo $data_row;
	}

	function view($color_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['color_info']=$this->Color->get_info($color_id);

		$this->load->view("colors/form",$data);
	}
	
	function save_old($color_id=-1)
	{
		$color_data = array(
		'color_name'=>$this->input->post('color_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Color->save( $color_data, $color_id ) )
		{
			//New color
			if($color_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('colors_successful_adding').' '.
				$color_data['color_name'],'color_id'=>$color_data['color_id']));
				$color_id = $color_data['color_id'];
			}
			else //previous color
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('colors_successful_updating').' '.
				$color_data['color_name'],'color_id'=>$color_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('colors_error_adding_updating').' '.
			$color_data['color_name'],'color_id'=>-1));
		}

	}

	function delete_old()
	{
		$this->permission_lib->checkPermission();
		$colors_to_delete=$this->input->post('ids');

		if($this->Color->delete_list($colors_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('colors_successful_deleted').' '.
			count($colors_to_delete).' '.$this->lang->line('colors_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('colors_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Colors';
		$color = $this->Color->get_all_color();
		$table = '';
		$i=0;
		foreach ($color as $col) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$col->color_name</td>  
						<td>$col->description</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$col->color_name' f='$col->color_id' d='$col->description'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$col->color_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('colors/manage',$data);
		$this->load->view('partial/footer');
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->Color->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function save(){
		$name = $this->input->post('color_name');
		$id = $this->input->post('color_id');
		$desc = $this->input->post('description');
		$exist = $this->Color->check_name($name,$id);
		$ppl_data = array(
						'color_name'=>$name,
						'description'=>$desc,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('colors'));
		}else{
			$this->Color->save($id,$ppl_data);
			redirect(site_url('colors'));

		}
	}
	function delete($id){
		$this->db->where('color_id',$id)->update('ospos_colors',array('deleted'=>1));
		redirect(site_url('colors'));
	}
}
?>