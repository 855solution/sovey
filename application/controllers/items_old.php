<?php

require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Items extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('items');
		//$this->load->helper(array('form','url','file'));
		//parent::Controller();
		//$this->load->helper('url');
		$this->load->database();
        $this->load->helper(array('form', 'url'));
	}

	function index()
	{
		
		
		$config['base_url'] = site_url('/items/index');
		$config['total_rows'] = $this->Item->count_all();
		$config['per_page'] = '30';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_items_manage_table( $this->Item->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('items/manage',$data);
	}


	
	function refresh()
	{
		$low_inventory=$this->input->post('low_inventory');
		$is_serialized=$this->input->post('is_serialized');
		$no_description=$this->input->post('no_description');

		$data['search_section_state']=$this->input->post('search_section_state');
		$data['low_inventory']=$this->input->post('low_inventory');
		$data['is_serialized']=$this->input->post('is_serialized');
		$data['no_description']=$this->input->post('no_description');
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_items_manage_table($this->Item->get_all_filtered($low_inventory,$is_serialized,$no_description),$this);
		$this->load->view('items/manage',$data);
	}

	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Item->find_item_info($item_number));
	}

	function search()
	{
		$search=$this->input->post('search');
	
		$rows=$this->Item->fullTextSearch($search);
		
		$data_rows=get_items_manage_table_data_rows($rows,$this);
		echo $data_rows;
		
	
		
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Item->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Item->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_item_data_row($this->Item->get_info($item_id),$this);
		echo $data_row;
	}

	function view($item_id=-1)
	{

		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['location_id']] = $row['location_name'];
		}		
		$data['locations']=$locations;
		$data['selected_location'] = $this->Item->get_info($item_id)->location_id;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info($item_id)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info($item_id)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info($item_id)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info($item_id)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info($item_id)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info($item_id)->category_id;

		
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		
		
		/*Grade of Items*/
		
		$grade = array('' => $this->lang->line('items_none'));
		
		foreach($this->Item->get_all_grade()->result_array() as $row)
		{
			$grade[$row['grad_id']] = $row['grad_name'];
		}	
			
		$data['grade']=$grade;
		$data['selected_grade'] = $this->Item->get_info($item_id)->grad_id;
		
		/*get unit price and cost price where grade_id*/
		$gradeA=$this->Item->get_unitPrice_costPrice(1)->row_array();
		$data['unitpriceA']=$gradeA['unit_price'];
		$data['costpriceA']=$gradeA['cost_price'];
		
		$gradeB=$this->Item->get_unitPrice_costPrice(2)->row_array();
		$data['unitpriceB']=$gradeB['unit_price'];
		$data['costpriceB']=$gradeB['cost_price'];
		
		$gradeC=$this->Item->get_unitPrice_costPrice(3)->row_array();
		$data['unitpriceC']=$gradeC['unit_price'];
		$data['costpriceC']=$gradeC['cost_price'];
		
		/*==-========================================================================*/
		
		
		
		$data['image_name']=$this->Item->get_image_source($item_id)->result_array();
		
		if($item_id==-1){
		$this->load->view("items/form_insert_new_item",$data);	
		}else{
		$this->load->view("items/form",$data);
		}
	}
	
	
	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/inventory",$data);
	}
	
	function count_details($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/count_details",$data);
	} //------------------------------------------- Ramel

	function generate_barcodes($item_ids,$barcode_time=1)
	{
		$result = array();

		$item_ids = explode(':', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);

			$result[] = array('barcode' =>$item_info->barcode, 'id'=> $item_id,'name' =>$item_info->name, 'item_id' =>$item_info->item_id);
		}

		$data['items'] = $result;
		$data['barcodetime']=$barcode_time;
		$this->load->view("barcode_sheet", $data);
	}

	function bulk_edit()
	{
		$data = array();
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['first_name'] .' '. $row['last_name'];
		}
		$data['suppliers'] = $suppliers;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['lcoation_id']] = $row['location_name'];
		}
		$data['locations'] = $locations;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}
		$data['makes'] = $makes;
		
		$data['allow_alt_desciption_choices'] = array(
			''=>$this->lang->line('items_do_nothing'), 
			1 =>$this->lang->line('items_change_all_to_allow_alt_desc'),
			0 =>$this->lang->line('items_change_all_to_not_allow_allow_desc'));
				
		$data['serialization_choices'] = array(
			''=>$this->lang->line('items_do_nothing'), 
			1 =>$this->lang->line('items_change_all_to_serialized'),
			0 =>$this->lang->line('items_change_all_to_unserialized'));
		$this->load->view("items/form_bulk", $data);
	}
	function save($item_id=-1)
	{
		
		$this->load->helper('url');
		$this->load->model('item');
		$name=array();
		$makeid=$this->input->post('make_id');
		$modelid=$this->input->post('model_id');
		$year_input=$this->input->post('year');
		$grad_id=$this->input->post('grade_id');
		
		$make_name=$this->item->make_name($makeid)->row_array();
		$name['getname']=$make_name['make_name'];
		
		$modelname=$this->item->model_name($modelid)->row_array();
		$name['modelname']=$modelname['model_name'];
		
		$make_name=$this->get2FirstString($name['getname']);
		$year=$this->get2lastString($year_input);
		$model_name=$this->get2FirstString($name['modelname']);
		
				//$digit=$this->generate_barcode_6digit(6);
		//$digit=$this->input->post('item_number')+1;
		//if($item_id==-1){
		//$lastid=$this->Item->getLastId();
		//$id=$lastid['item_id'];
		
		//$digit=$lastid+1;
		//$digit_padding=str_pad($digit,6,"1",STR_PAD_LEFT);
		
		//$barcode=$year.$make_name.$model_name.$digit_padding;
		//}else{
			//$barcode=$this->Item->get_info($item_id);
			//$barcode=$item_data['barcode'];
			//}
		
		
		$item_data = array(
		'name'=>$this->input->post('name'),
		'description'=>$this->input->post('description'),
		'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
		'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
		'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
		'cost_price'=>$this->input->post('cost_price'),
		'unit_price'=>$this->input->post('unit_price'),
		'quantity'=>$this->input->post('quantity'),
		'reorder_level'=>$this->input->post('reorder_level'),
		'location'=>$this->input->post('location'),
		'fit'=>$this->input->post('fit'),
		'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
		'year'=>$this->input->post('year'),
		'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
		'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
		'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
		'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
		'allow_alt_description'=>$this->input->post('allow_alt_description'),
		'is_serialized'=>$this->input->post('is_serialized'),
		'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
		'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
		'category'=>$this->input->post('category'),
		'barcode'=>$barcode,
		'grad_id'=>$grad_id
		);
		
		
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		
		if($this->Item->save($item_data,$item_id))
		{
			
			//$itemid=$this->db->insert_id();
			
			//New item
			if($item_id==-1)
			{
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_adding').' '.
				//$item_data['name'],'item_id'=>$item_data['item_id']));
				$item_id = $item_data['item_id'];
			}
			else //previous item
			{
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
				//$item_data['name'],'item_id'=>$item_id));
			}
			
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item_id,
				'trans_user'=>$employee_id,
				'trans_comment'=>$this->lang->line('items_manually_editing_of_quantity'),
				'trans_inventory'=>$cur_item_info ? $this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity')
			);
			
			$this->Inventory->insert($inv_data);
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			
						/*--------------------update barcode after save item sucessful-----------------------------------------*/
			
			$digit_padding=str_pad($item_id,6,"0",STR_PAD_LEFT);
			$barcode=$year.$make_name.$model_name.$digit_padding;
			
			$barcode_item_data = array(
				'name'=>$this->input->post('name'),
				'description'=>$this->input->post('description'),
				'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
				'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
				'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
				'cost_price'=>$this->input->post('cost_price'),
				'unit_price'=>$this->input->post('unit_price'),
				'quantity'=>$this->input->post('quantity'),
				'reorder_level'=>$this->input->post('reorder_level'),
				'location'=>$this->input->post('location'),
				'fit'=>$this->input->post('fit'),
				'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
				'year'=>$this->input->post('year'),
				'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
				'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
				'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
				'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
				'allow_alt_description'=>$this->input->post('allow_alt_description'),
				'is_serialized'=>$this->input->post('is_serialized'),
				'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
				'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
				'category'=>$this->input->post('category'),
				'barcode'=>$barcode,
				'grad_id'=>$grad_id
				);
			$this->Item->updateBarcodeAfterSave($item_id,$barcode_item_data);
			
			/*-----------------------------------------------------------------------------*/

			
			$this->Item_taxes->save($items_taxes_data, $item_id);
			
			
			$this->addImage($item_id);
			
			if($this->input->post('generate_barcode')){
			redirect('items/generate_barcodes/'.$item_id.'/'.$this->input->post("quantity").'');
			}else{
			redirect('items/');	
			}
		}
		else//failure
		{
			//echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			//$item_data['name'],'item_id'=>-1));
		}
	}
	
	//Ramel Inventory Tracking
	function save_inventory($item_id=-1)
	{	
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		$inv_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_items'=>$item_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$this->input->post('trans_comment'),
			'trans_inventory'=>$this->input->post('newquantity')
		);
		$this->Inventory->insert($inv_data);
		
		//Update stock quantity
		$item_data = array(
		'quantity'=>$cur_item_info->quantity + $this->input->post('newquantity')
		);
		if($this->Item->save($item_data,$item_id))
		{			
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_item_info->name,'item_id'=>$item_id));
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_item_info->name,'item_id'=>-1));
		}

	}//---------------------------------------------------------------------Ramel

	function bulk_update()
	{
		$items_to_update=$this->input->post('item_ids');
		$item_data = array();

		foreach($_POST as $key=>$value)
		{
			//This field is nullable, so treat it differently
			if ($key == 'supplier_id')
			{
				$item_data["$key"]=$value == '' ? null : $value;
			}
			elseif($value!='' and !(in_array($key, array('item_ids', 'tax_names', 'tax_percents'))))
			{
				$item_data["$key"]=$value;
			}
		}

		//Item data could be empty if tax information is being updated
		if(empty($item_data) || $this->Item->update_multiple($item_data,$items_to_update))
		{
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			$this->Item_taxes->save_multiple($items_taxes_data, $items_to_update);

			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_bulk_edit')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_updating_multiple')));
		}
	}

	function delete()
	{
		$items_to_delete=$this->input->post('ids');

		if($this->Item->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_deleted').' '.
			count($items_to_delete).' '.$this->lang->line('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_cannot_be_deleted')));
		}
	}
	
	function excel()
	{
		$data = file_get_contents("import_items.csv");
		$name = 'import_items.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->load->view("items/excel_import", null);
	}

	function do_excel_import()
	{
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = $this->lang->line('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				
				$i=1;
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					$item_data = array(
					'name'			=>	$data[1],
					'description'	=>	$data[13],
					'location'		=>	$data[12],
					'make'			=>	$data[16],
					'category_id'	=>	$data[2],
					'cost_price'	=>	$data[4],
					'unit_price'	=>	$data[5],
					'quantity'		=>	$data[10],
					'reorder_level'	=>	$data[11],
					'supplier_id'	=>  $this->Supplier->exists($data[3]) ? $data[3] : null,
					'allow_alt_description'=> $data[14] != '' ? '1' : '0',
					'is_serialized'=>$data[15] != '' ? '1' : '0',
					'category'		=>	$data[17]
					);
					$item_number = $data[0];
					
					if ($item_number != "")
					{
						$item_data['item_number'] = $item_number;
					}
					
					if($this->Item->save($item_data)) 
					{
						$items_taxes_data = null;
						//tax 1
						if( is_numeric($data[7]) && $data[6]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[6], 'percent'=>$data[7] );
						}

						//tax 2
						if( is_numeric($data[9]) && $data[8]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[8], 'percent'=>$data[9] );
						}

						// save tax values
						if(count($items_taxes_data) > 0)
						{
							$this->Item_taxes->save($items_taxes_data, $item_data['item_id']);
						}
						
							$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
							$emp_info=$this->Employee->get_info($employee_id);
							$comment ='Qty CSV Imported';
							$excel_data = array
								(
								'trans_items'=>$item_data['item_id'],
								'trans_user'=>$employee_id,
								'trans_comment'=>$comment,
								'trans_inventory'=>$data[10]
								);
								$this->db->insert('inventory',$excel_data);
						//------------------------------------------------Ramel
					}
					else//insert or update item failure
					{
						$failCodes[] = $i;
					}
				}
				
				$i++;
			}
			else 
			{
				echo json_encode( array('success'=>false,'message'=>'Your upload file has no data or not in supported format.') );
				return;
			}
		}

		$success = true;
		if(count($failCodes) > 1)
		{
			$msg = "Most items imported. But some were not, here is list of their CODE (" .count($failCodes) ."): ".implode(", ", $failCodes);
			$success = false;
		}
		else
		{
			$msg = "Import items successful";
		}

		echo json_encode( array('success'=>$success,'message'=>$msg) );
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	
	public function combobox()
    {
        
        $name = $this->input->get('_name');
        $value = $this->input->get('_value');
         
        $this->load->model('item');
        
        echo json_encode( $this->item->get_dropdown($name, $value) );
    }
    
    /*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_partNumber()
	{
		$suggestions = $this->Item->get_partNumber_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}
	
	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
	
	function get2FirstString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$end = $characters-$length;
		
		$firstString = substr($string ,0 ,$end);
		return $firstString;
		}
	}
	
	function generate_barcode_6digit($length) 
	{
	$consonants = '1234567890';

    for ($i = 0; $i < $length; $i++) 
    {
    	
    	$digit.= $consonants[(rand() % strlen($consonants))];
    	
    }
    return $digit;
}

function picupload($itemid)
    {
        //Load Model
        $this->load->model('process_image');

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '2048'; //2 meg


        $this->load->library('upload');

        foreach($_FILES as $key => $value)
        {
            if( ! empty($key['name']))
            {
                $this->upload->initialize($config);
        
                if ( ! $this->upload->do_upload($key))
                {
                    $errors[] = $this->upload->display_errors();
                    
                }    
                else
                {
                    $this->process_image->process_pic($itemid);

                }
             }
        
        }
        $data['success'] = 'Thank You, Files Upladed!';
    }
  
    
    /*
    function do_upload()
    {
        $config['upload_path'] = './uploads/'; // server directory
        $config['allowed_types'] = 'gif|jpg|png'; // by extension, will check for whether it is an image
        $config['max_size']    = '1000'; // in kb
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        
        $this->load->library('upload', $config);
        $this->load->library('Multi_upload');
    
        $files = $this->multi_upload->go_upload();
        
        if ( ! $files )        
        {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('upload_form', $error);
        }    
        
        //else
       // {
        //    $data = array('upload_data' => $files);
            
           // $this->load->view('upload_success', $data);
        //}
    }
    */
	function _example_output($output = null)
	{
		$this->load->view('items/upload_file',$output);	
		//return $output;
		
	}
	
	function example1()
	{
		$this->load->library('Image_CRUD');
		$image_crud = new Image_CRUD();
		
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('imagename');
		$image_crud->set_table('image_items');
		$image_crud->set_image_path('assets/uploads');
			
		$output = $image_crud->render();
		return $output;
		//$this->_example_output($output);
	}
	
	
	
		function getExtension($str) {
				$i = strrpos($str,".");
				if (!$i) { return ""; }
				$l = strlen($str) - $i;
				$ext = substr($str,$i+1,$l);
				return $ext;
			}
	
	
 	function uploadImage($itemid,$image_id){
 		
 		
 		for($i=0; $i<count($_FILES['txtnews_photo']['name']); $i++){
				$change_new="";
				define ("MAX_SIZE","1024");
				$errors_new=0;

			
				//if($_SERVER["REQUEST_METHOD"] == "POST"){
					$image_new = $_FILES['txtnews_photo']['name'][$i];
					$uploadedfile_new = $_FILES['txtnews_photo']['tmp_name'][$i];
					 
					$rand_new=rand(0000,9999);
					$replace_new = $rand_new;
					$subject_new = $image_new;
					$image_new=substr_replace($subject_new,$replace_new,0,-4);		
						
					if ($image_new) 
					{
						$filename_new = stripslashes($image_new);
						$extension_new = getExtension($filename_new);
						$extension_new = strtolower($extension_new);
						
						if (($extension_new != "jpg") && ($extension_new != "jpeg")) 
						{
							$change_new='<div class="msgdiv">Unknown Image extension </div> ';
							$errors_new=1;
						}
						else
						{
						
							$size_new=filesize($_FILES['txtnews_photo']['tmp_name'][$i]);
							if ($size_new > MAX_SIZE*2000000)
							{
								$change_new='<div class="msgdiv">You have exceeded the size limit!</div> ';
								$errors_new=1;
							}
							
							if($extension_new=="jpg" || $extension_new=="jpeg" )
							{
								$uploadedfile_new = $_FILES['txtnews_photo']['tmp_name'][$i];
								$src_new = imagecreatefromjpeg($uploadedfile_new);}
							else if($extension_new=="png")
							{
								$uploadedfile_new = $_FILES['txtnews_photo']['tmp_name'][$i];
								$src_new = imagecreatefrompng($uploadedfile_new);
							}
							else 
							{
								$src_new = imagecreatefromgif($uploadedfile_new);
							}
						
							//echo $scr;
							list($width_new,$height_new)=getimagesize($uploadedfile_new);
							$newheight_new=414;
							$newwidth_new=($width_new/$height_new)*$newheight_new;
							$tmp_new=imagecreatetruecolor($newwidth_new,$newheight_new);
					
							imagecopyresampled($tmp_new,$src_new,0,0,0,0,$newwidth_new,$newheight_new,$width_new,$height_new);
					
							$filename_new = ".uploads/".$image_new;
							
							
							imagejpeg($tmp_new,$filename_new,100);
							
							///////////////
							
							list($width_new1,$height_new1)=getimagesize($uploadedfile_new);
							$newheight_new1=105;
							$newwidth_new1=($width_new1/$height_new1)*$newheight_new1;
							$tmp_new1=imagecreatetruecolor($newwidth_new1,$newheight_new1);
					
							imagecopyresampled($tmp_new1,$src_new,0,0,0,0,$newwidth_new1,$newheight_new1,$width_new1,$height_new1);
					
							$filename_new1 = ".uploads/thum/".$image_new;
							
							
							imagejpeg($tmp_new1,$filename_new1,100);
							
							imagedestroy($tmp_new1);
							
							imagedestroy($src_new);
							imagedestroy($tmp_new);
						}
					}
								
				
				//}	
			
		$this->load->model('image_model');	
		$image_data = array(
		'item_id'=>$itemid,
		'imagename'=>$image_new
		);
				
		$this->image_model->uploadimage($image_data,$image_id);
 				}
 	}   

 	function do_uploadimage(){
 		
 		  $config['upload_path'] = './uploads';
          $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
          $config['max_size']  = '0';
          $config['max_width']  = '0';
          $config['max_height']  = '0';
          $this->load->library('upload', $config);


          $configThumb = array();
          $configThumb['image_library'] = 'gd2';
          $configThumb['source_image'] = '';
          $configThumb['create_thumb'] = TRUE;
          $configThumb['maintain_ratio'] = TRUE;

          $configThumb['width'] = 100;
          $configThumb['height'] = 120;
          $this->load->library('image_lib');

          for($i = 1; $i < 6; $i++) {
            $upload = $this->upload->do_upload('file'.$i);
            if($upload === FALSE) continue;
            $data = $this->upload->data();

            $uploadedFiles[$i] = $data;

            $imgName = $this->pictures_m->addPicture(array('listing_id' => $listing_id, 'ext' => $data['file_ext'], 'picture_name' => $this->input->post('file'.$i.'name')));

            if($data['is_image'] == 1) {
              $configThumb['source_image'] = $data['full_path'];
              $configThumb['new_image'] = $data['file_path'].$imgName.$data['file_ext'];
              $this->image_lib->initialize($configThumb);
              $this->image_lib->resize();
            }
    rename($data['full_path'], $data['file_path'].$imgName.$data['file_ext']);

          }
 		
 	}
 	
 	function go(){
 		header('Content-Type: application/json');
 	
 		$itemnumber=-1;
 		//if ($this->input->server('REQUEST_METHOD') === 'POST')
		//{
			
 		//$itemnumber=$this->input->post('item_number');
 		//$itemnumber=$this->input->post('itemvalue');
 		$itemnumber=$_REQUEST['itemvalue'];
 		
 		$data['item_info']=$this->Item->get_info_where_itemnumber($itemnumber);
 		$data['itemid']=$this->Item->get_info_where_itemnumber($itemnumber)->item_id;
		$data['item_tax_info']=$this->Item_taxes->get_info($data['itemid']);
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info_where_itemnumber($itemnumber)->supplier_id;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['location_id']] = $row['location_name'];
		}		
		$data['locations']=$locations;
		$data['selected_location'] = $this->Item->get_info_where_itemnumber($itemnumber)->location_id;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info_where_itemnumber($itemnumber)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info_where_itemnumber($itemnumber)->model_id;
		
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info_where_itemnumber($itemnumber)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info_where_itemnumber($itemnumber)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info_where_itemnumber($itemnumber)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info_where_itemnumber($itemnumber)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info_where_itemnumber($itemnumber)->category_id;

		
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		
		
			
		//}

		/*Grade of Items*/
		
		$grade = array('' => $this->lang->line('items_none'));
		
		foreach($this->Item->get_all_grade()->result_array() as $row)
		{
			$grade[$row['grad_id']] = $row['grad_name'];
		}	
			
		
		$data['grade']=$grade;
		$data['selected_grade'] = $this->Item->get_info($data['itemid'])->grad_id;
		
		/*get unit price and cost price where grade_id*/
		$gradeA=$this->Item->get_unitPrice_costPrice(1)->row_array();
		$data['unitpriceA']=$gradeA['unit_price'];
		$data['costpriceA']=$gradeA['cost_price'];
		
		$gradeB=$this->Item->get_unitPrice_costPrice(2)->row_array();
		$data['unitpriceB']=$gradeB['unit_price'];
		$data['costpriceB']=$gradeB['cost_price'];
		
		$gradeC=$this->Item->get_unitPrice_costPrice(3)->row_array();
		$data['unitpriceC']=$gradeC['unit_price'];
		$data['costpriceC']=$gradeC['cost_price'];
		
		
		$data['image_name']=$this->Item->get_image_source($itemnumber)->result_array();
		//$data['itemid']='';
		//$this->load->view("items/form_insert_new_item",$data);
		echo json_encode($data);
	
 	}
 
 	
 	function addImage($item_id){
 		
 	//if($_SERVER["REQUEST_METHOD"] == "POST"){	
 		
 		for($j=0;$j<count($_FILES['path']['name']);$j++){	
			if ($_FILES['path']['name'][$j]!=""){

			
				
				$image_directory = "./uploads/";
				$big_directory = "./uploads/";
				$allowed_ext = "jpg, JPG, gif, GIF , bmp, BMP, jpeg, JPEG , png, PNG"; 
				$extension = pathinfo($_FILES['path']['name'][$j]);
				$extension = $extension["extension"];
				$allowed_paths = explode(", ", $allowed_ext);
				for($i = 0; $i < count($allowed_paths); $i++) {
					if ($allowed_paths[$i] == $extension) {
						 $ok="1";
					}
				}
				if($ok=="1"){
						$concatenate_str1 = time();
						if(is_uploaded_file($_FILES['path']['tmp_name'][$j])){	
								$picture_name=$_FILES['path']['name'][$j];	
								move_uploaded_file($_FILES['path']['tmp_name'][$j],$image_directory.$concatenate_str1.$picture_name);
							  $imageurl=$concatenate_str1.$picture_name;
						}
		
		$file_data=array('item_id'=>$item_id,'imagename'=>$concatenate_str1.$picture_name);
		
 		$this->load->database();
	    $ret=$this->db->insert('image_items',$file_data);		}
					
				}		   
 		}
 	//}
 	
 }
 	
 	function doUploadFileImage(){
	 	$config['upload_path'] = './uploads/';      
	    $config['allowed_types'] = 'gif|jpg|png|doc|txt|pdf';
	    $config['max_size'] = '5000';
	    $config['max_width']  = '2500';
	    $config['max_height']  = '2500';
	    $config['remove_spaces']= 'true';
	
	    $this->load->library('upload', $config);
	    $this->load->library('image_lib');  
	
	    
	    for($i=0; $i<count($_FILES['txtnews_photo']['name']); $i++){
	    
	    
	    $data= array('txtnews_photo' => $this->upload->do_upload()); 
	    
	    //DEFINE POSTED FILE INTO VARIABLE
	    $name= $data['txtnews_photo']['name'][$i];
	    $tmpname= $data['txtnews_photo']['tmpname'][$i];
	    $type= $data['txtnews_photo']['type'][$i];
	    $size= $data['txtnews_photo']['size'][$i];
	    
	    $file_data=array('imagename'=>$name);
	    
	 	//if ( ! $this->upload->do_upload())
	   // {
	    //    $error = array('error' => $this->upload->display_errors());
	
	        //$this->load->view('form_insert_new_item', $error);
	   // }   
	   // else
	   // {
	        $data = array('txtnews_photo' => $this->upload->data());
	        $this->load->database();
	        $ret=$this->db->insert('image_items',$file_data);
	        unlink($tmpname);
	        
	    //}
 		
 	}
 	}
 	
 	function deleteImage(){
 		$value=true;
 		$imageid=$this->input->post('imageid');
 		if($imageid){
 			$this->load->model('item');
 			$image_name=$this->Item->getImageName($imageid);
 		$delete=$this->Item->delete_Image_item($imageid);
 		if($delete){
 			 unlink("./uploads/".$image_name);
 		}
 		
 		}
 		
 		
 	}
}
?>