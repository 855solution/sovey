<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Models extends Secure_area
{
	function __construct()
	{
		parent::__construct('models');
		$this->load->model('Make');
	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/models/index');
		$config['total_rows'] = $this->Model->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_models_manage_table( $this->Model->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('models/manage',$data);
	}
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Models';
		$data['make_data'] = $this->Make->get_all_make();
		$model_data = $this->Model->get_all_model();
		$table = '';
		$i=0;
		foreach ($model_data as $mo) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$mo->model_name</td>  
						<td>$mo->description</td>  
						<td>$mo->make_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$mo->model_name' m='$mo->make_id' f='$mo->model_id' d='$mo->description'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$mo->model_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('models/manage',$data);
		$this->load->view('partial/footer');
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_models_manage_table_data_rows($this->Model->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Model->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$model_id = $this->input->post('row_id');
		$data_row=get_model_data_row($this->Model->get_info($model_id),$this);
		echo $data_row;
	}
	
	function view($model_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['model_info']=$this->Model->get_info($model_id);
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Model->get_info($model_id)->make_id;

		$this->load->view("models/form",$data);
	}
	
	function save_old($model_id=-1)
	{
		$model_data = array(
		'model_name'=>$this->input->post('model_name'),
		'model_description'=>$this->input->post('model_description'),
		'make_id'=>$this->input->post('make_id')
		);

		if( $this->Model->save( $model_data, $model_id ) )
		{
			//New model
			if($model_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('models_successful_adding').' '.
				$model_data['model_name'],'model_id'=>$model_data['model_id']));
				$model_id = $model_data['model_id'];
			}
			else //previous model
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('models_successful_updating').' '.
				$model_data['model_name'],'model_id'=>$model_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('models_error_adding_updating').' '.
			$model_data['model_name'],'model_id'=>-1));
		}

	}

	function delete_old()
	{
		$this->permission_lib->checkPermission();
		$models_to_delete=$this->input->post('ids');

		if($this->Model->delete_list($models_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('models_successful_deleted').' '.
			count($models_to_delete).' '.$this->lang->line('models_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('models_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$m = $this->input->post('m');
		$exist = $this->Model->check_name($n,$id,$m);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function save(){
		$name = $this->input->post('model_name');
		$id = $this->input->post('model_id');
		$desc = $this->input->post('description');
		$make_id = $this->input->post('make_id');
		if ($make_id=='') {
			$this->session->set_flashdata('er','No Make Selected !');
			redirect(site_url('models'));
		}

		$exist = $this->Model->check_name($name,$id,$make_id);
		$model_data = array(
						'model_name'=>$name,
						'model_description'=>$desc,
						'make_id'=>$make_id,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('models'));
		}else{
			$this->Model->save($id,$model_data);
			redirect(site_url('models'));

		}
	}
	function delete($id){
		$this->db->where('model_id',$id)->update('ospos_models',array('deleted'=>1));
		redirect(site_url('models'));
	}
}
?>