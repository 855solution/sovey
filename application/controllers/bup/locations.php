<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Locations extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('locations');
	}

	function index()
	{
		$config['base_url'] = site_url('/locations/index');
		$config['total_rows'] = $this->Location->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_locations_manage_table( $this->Location->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('locations/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_locations_manage_table_data_rows($this->Location->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Location->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$location_id = $this->input->post('row_id');
		$data_row=get_location_data_row($this->Location->get_info($location_id),$this);
		echo $data_row;
	}

	function view($location_id=-1)
	{
		$data['location_info']=$this->Location->get_info($location_id);

		$this->load->view("locations/form",$data);
	}
	
	function save($location_id=-1)
	{
		$location_data = array(
		'location_name'=>$this->input->post('location_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Location->save( $location_data, $location_id ) )
		{
			//New location
			if($location_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('locations_successful_adding').' '.
				$location_data['location_name'],'location_id'=>$location_data['location_id']));
				$location_id = $location_data['location_id'];
			}
			else //previous location
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('locations_successful_updating').' '.
				$location_data['location_name'],'location_id'=>$location_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('locations_error_adding_updating').' '.
			$location_data['location_name'],'location_id'=>-1));
		}

	}

	function delete()
	{
		$locations_to_delete=$this->input->post('ids');

		if($this->Location->delete_list($locations_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('locations_successful_deleted').' '.
			count($locations_to_delete).' '.$this->lang->line('locations_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('locations_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
}
?>