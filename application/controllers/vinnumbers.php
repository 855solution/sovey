<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Vinnumbers extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('vinnumbers');
        $this->load->model('Vinnumber','vinnm');
        $this->load->model('item');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$this->sale_lib->unset_filter_vin();
		$numpage=10;
		if(isset($_GET['page']))
			$numpage=$_GET['page'];
		$per_page=10;
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];
		
		$config['base_url'] = site_url('/vinnumbers/index?page='.$numpage);
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 4;
		$config['page_query_string']=TRUE;
		$config['total_rows'] = $this->Vinnumber->count_all();
		$this->pagination->initialize($config);
		
		$data['total_rows'] =$this->Vinnumber->count_all();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_vinnumbers_manage_table( $this->Vinnumber->get_all( $config['per_page'], $per_page ) , $this );
		$this->load->view('vinnumbers/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_vinnumbers_manage_table_data_rows($this->Vinnumber->search($search),$this);
		echo $data_rows;
	}
	function search_vin(){
		$data['controller_name']=strtolower(get_class());
		$controller_name=$data['controller_name'];
		$search='';
		$make_id='';
		$model_id='';
		$s_year='';
		$in_color_id='';
		$ex_color_id='';
		$where='';
		$pro_sta='';
		$body_id='';
		$engine_id='';
		$fuel_id='';
		$re=' of ';
		$data['title']="";
		$year_from = $_POST['year_from'];
		$year_to = $_POST['year_to'];
		$filters_applied = array();
		$filter_array = array();

		$cl_f = $this->input->post('cl_f');
		$cl_fid = $this->input->post('cl_fid');

		if ($cl_f) {
			$this->sale_lib->unset_filter_vin($cl_f,$cl_fid);
		}

		if(isset($_POST['search'])){
			$search=$_POST['search'];
			$data['title']="Search Results";
			if ($search!='') {
				$this->sale_lib->add_filter_vin('sl',$search,'s');

				$re.=" Keywords: <b>$search</b>";
			}
		}

		if(isset($_POST['vin']) && $_POST['vin']!='' && $_POST['vin']!='undefined' && $_POST['vin']!='0'){
			$vin=$_POST['vin'];
			$re.=' Head:<b>';
			$this->sale_lib->add_filter_vin('vin',$vin,'head');

			if ($vin!='other') {
				$re.="$vin";
				$where.=" AND ospos_vinnumbers.vinnumber_id='".$_POST['vin']."'";
			}
			// else{
			// 	$where.=" AND (i.category='' OR i.category='0' OR i.category IS NULL)";
			// }
			$re.='</b>';
		}

		if(isset($_POST['m']) && $_POST['m']!='' && $_POST['m']!='undefined' && $_POST['m']!='0'){
			$re.=' Make: <b>';
			$make_id=$_POST['m'];
			$this->sale_lib->add_filter_vin($make_id,$this->item->make_name($make_id)->row()->make_name,'make');

			if ($make_id=='other') {
				$re.='Other';
				$where.=" AND (ospos_vinnumbers.make_id='' OR ospos_vinnumbers.make_id='0' OR ospos_vinnumbers.make_id IS NULL)";
			}else{
				$re.=$this->item->make_name($make_id)->row()->make_name;
				$where.=" AND ospos_vinnumbers.make_id='".$_POST['m']."'";
			}
			$re.='</b>';

		}
		if(isset($_POST['mo']) && $_POST['mo']!=''&& $_POST['mo']!='undefined' && $_POST['mo']!='0'){
			$model_id=$_POST['mo'];
			$re.=' Model: <b>';
			$this->sale_lib->add_filter_vin($model_id,$this->item->model_name($model_id)->row()->model_name,'model');

			if ($model_id=='other') {
				$re.='Other';
				$where.=" AND (ospos_vinnumbers.model_id='' OR ospos_vinnumbers.mdoel_id='0' OR ospos_vinnumbers.model_id IS NULL)";
			}else{
				$re.=$this->item->model_name($model_id)->row()->model_name;
				$where.=" AND ospos_vinnumbers.model_id='".$_POST['mo']."'";
			}
			$re.='</b>';
		}
		if(isset($_POST['y']) && $_POST['y']!='' && $_POST['y']!='_' && $_POST['y']!='undefined' && $_POST['y']!='0'){
			$year_from = '';
			$year_to = '';
			$arryear=explode('_', $_POST['y']);
			$re.=' Year: <b>';
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
            		if ($arryear[$i]!='_' && $arryear[$i]!='') {
						$this->sale_lib->add_filter_vin($arryear[$i],$arryear[$i],'year');

            			if ($arryear[$i]=='other') {
            				$re.='Other';
            				$where_year.="ospos_vinnumbers.year='' OR ospos_vinnumbers.year=0 OR ospos_vinnumbers.year IS NULL";
            				
            			}else{
            				$re.=$arryear[$i];
            				$where_year.="ospos_vinnumbers.year='".$arryear[$i]."'";

            			}
	                    if(count($arryear)-2>$i){
	                    	$re.=', ';
	                        $where_year.=' OR ';
	                    }
            		}
                    

            }
            $re.='</b>';
            $where_year.=')';
            $s_year=$_POST['y'];
            $where.=$where_year;

		}
		if(isset($_POST['in_c']) && $_POST['in_c']!='' && $_POST['in_c']!='_' && $_POST['in_c']!='undefined' && $_POST['in_c']!='0'){
			$in_arrcol=explode('_', $_POST['in_c']);
            $in_where_color=' AND(';
            	$re.=' Interior Color: <b>';
            for ($i=0; $i < count($in_arrcol) ; $i++) { 
                	if($in_arrcol[$i]!='_' && $in_arrcol[$i]!=''){
						$this->sale_lib->add_filter_vin($in_arrcol[$i],$this->item->color_name($in_arrcol[$i])->row()->color_name,'in_co');
                		if ($in_arrcol[$i]=='other') {
                			$re.='Other';
            				$in_where_color.="ospos_vinnumbers.interior_color='' OR ospos_vinnumbers.interior_color=0 OR ospos_vinnumbers.interior_color IS NULL";
                			
                		}else{
							$in_where_color.="ospos_vinnumbers.interior_color='".$in_arrcol[$i]."'";
							$re.=$this->item->color_name($in_arrcol[$i])->row()->color_name;
                		}
						if (count($in_arrcol)-2>$i) {
							$in_where_color.=" OR ";
							$re.=', ';
						}
		           	}
            }
            $re.='</b>';
            $in_where_color.=')';
            $in_color_id=$_POST['in_c'];
            // echo $in_where_color;
            $where.=$in_where_color;

		}
		if(isset($_POST['ex_c']) && $_POST['ex_c']!='' && $_POST['ex_c']!='_' && $_POST['ex_c']!='undefined' && $_POST['ex_c']!='0'){
			$ex_arrcol=explode('_', $_POST['ex_c']);
            $ex_where_color=' AND(';
            	$re.=' Exterior Color: <b>';
            for ($i=0; $i < count($ex_arrcol) ; $i++) { 
                   if($ex_arrcol[$i]!='_' && $ex_arrcol[$i]!=''){
						$this->sale_lib->add_filter_vin($ex_arrcol[$i],$this->item->color_name($ex_arrcol[$i])->row()->color_name,'ex_co');

                   		if ($ex_arrcol[$i]=='other') {
                   			$re.='Other';

                   			$ex_where_color.="ospos_vinnumbers.external_color='' OR ospos_vinnumbers.external_color=0 OR ospos_vinnumbers.external_color IS NULL";
                   		}else{
                   			$re.=$this->item->color_name($ex_arrcol[$i])->row()->color_name;
							$ex_where_color.="ospos_vinnumbers.external_color='".$ex_arrcol[$i]."'";

                   		}
						if (count($ex_arrcol)-2>$i) {
							$re.=', ';
							$ex_where_color.=" OR ";
						}
		           	}
                
            }
            $re.='</b>';
            $ex_where_color.=')';
            $ex_color_id=$_POST['ex_c'];
            $where.=$ex_where_color;

		}
		
		$yearft='';
		if ($year_from!='' && $year_to!='') {
			$yearft .= " AND ospos_vinnumbers.year BETWEEN $year_from AND $year_to";
			$this->sale_lib->add_filter_vin('yft',"$year_from - $year_to",'yearrange');

			$re.=" Year: <b>$year_from</b> to <b>$year_to</b>";
			$where.=$yearft;
		}

		if(isset($_POST['body']) && $_POST['body']!='' && $_POST['body']!='_' && $_POST['body']!='undefined' && $_POST['body']!='0'){
			$arrbody=explode('_', $_POST['body']);
            $where_body=' AND(';
            	$re.=' Body: <b>';
            for ($i=0; $i < count($arrbody) ; $i++) { 
                     if($arrbody[$i]!='_' && $arrbody[$i]!=''){
						$this->sale_lib->add_filter_vin($arrbody[$i],$this->item->body_name($arrbody[$i])->row()->body_name,'body');

                     	if ($arrbody[$i]=='other') {
                     		$re.='Other';
                     		$where_body.="ospos_vinnumbers.body='' OR ospos_vinnumbers.body=0 OR ospos_vinnumbers.body IS NULL";
                     	}else{
                     		$re.=$this->item->body_name($arrbody[$i])->row()->body_name;
							$where_body.="ospos_vinnumbers.body='".$arrbody[$i]."'";
                     	}
						if (count($arrbody)-2>$i) {
							$where_body.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_body.=')';
            $where.=$where_body;

		}

		if(isset($_POST['engine']) && $_POST['engine']!='' && $_POST['engine']!='_' && $_POST['engine']!='undefined' && $_POST['engine']!='0'){
			$arrengine=explode('_', $_POST['engine']);
            $where_engine=' AND(';
            	$re.=' Engine: <b>';
            for ($i=0; $i < count($arrengine) ; $i++) { 
                    if($arrengine[$i]!='_' && $arrengine[$i]!=''){
						$this->sale_lib->add_filter_vin($arrengine[$i],$this->item->engine_name($arrengine[$i])->row()->engine_name,'engine');

                    	if ($arrengine[$i]=='other') {
                    		$re.='Other';
                    		$where_engine.="ospos_vinnumbers.engine='' OR ospos_vinnumbers.engine=0 OR ospos_vinnumbers.engine IS NULL";
                    	}else{
							$where_engine.="ospos_vinnumbers.engine='".$arrengine[$i]."'";
							$re.=$this->item->engine_name($arrengine[$i])->row()->engine_name;
                    	}
						if (count($arrengine)-2>$i) {
							$where_engine.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_engine.=')';
            $where.=$where_engine;

		}
		if(isset($_POST['fuel']) && $_POST['fuel']!='' && $_POST['fuel']!='_' && $_POST['fuel']!='undefined' && $_POST['fuel']!='0'){
			$arrfuel=explode('_', $_POST['fuel']);
            $where_fuel=' AND(';
            	$re.=' Fuel: <b>';
            for ($i=0; $i < count($arrfuel) ; $i++) { 
                    if($arrfuel[$i]!='_' && $arrfuel[$i]!=''){
						$this->sale_lib->add_filter_vin($arrfuel[$i],$this->item->fuel_name($arrfuel[$i])->row()->fule_name,'fuel');

                    	if ($arrfuel[$i]=='other') {
                    		$re.='Other';
                    		$where_fuel.="ospos_vinnumbers.fuel='' OR ospos_vinnumbers.fuel=0 OR ospos_vinnumbers.fuel IS NULL";
                    	}else{
                    		$re.=$this->item->fuel_name($arrfuel[$i])->row()->fule_name;
							$where_fuel.="ospos_vinnumbers.fuel='".$arrfuel[$i]."'";
                    	}
						if (count($arrfuel)-2>$i) {
							$where_fuel.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_fuel.=')';
            $where.=$where_fuel;

		}
		if(isset($_POST['trans']) && $_POST['trans']!='' && $_POST['trans']!='_' && $_POST['trans']!='undefined' && $_POST['trans']!='0'){
			$arrtrans=explode('_', $_POST['trans']);
            $where_trans=' AND(';
            	$re.=" Transmission: <b>";
            for ($i=0; $i < count($arrtrans) ; $i++) { 
                    if($arrtrans[$i]!='_' && $arrtrans[$i]!=''){
						$this->sale_lib->add_filter_vin($arrtrans[$i],$this->item->trans_name($arrtrans[$i])->row()->transmission_name,'trans');

                    	if ($arrtrans[$i]=='other') {
                    		$re.="Other";
                    		$where_trans.="ospos_vinnumbers.transmission='' OR ospos_vinnumbers.transmission=0 OR ospos_vinnumbers.transmission IS NULL";
                    	}else{
							$where_trans.="ospos_vinnumbers.transmission='".$arrtrans[$i]."'";
							$re.=$this->item->trans_name($arrtrans[$i])->row()->transmission_name;
                    	}
						if (count($arrtrans)-2>$i) {
							$where_trans.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_trans.=')';
            $where.=$where_trans;

		}
		if(isset($_POST['trim']) && $_POST['trim']!='' && $_POST['trim']!='_' && $_POST['trim']!='undefined' && $_POST['trim']!='0'){
			$arrtrim=explode('_', $_POST['trim']);
            $where_trim=' AND(';
            	$re.=' Trim: <b>';
            for ($i=0; $i < count($arrtrim) ; $i++) { 
                    if($arrtrim[$i]!='_' && $arrtrim[$i]!=''){
						$this->sale_lib->add_filter_vin($arrtrim[$i],$this->item->trim_name($arrtrim[$i])->row()->trim_name,'trim');

                    	if ($arrtrim[$i]=='other') {
                    		$re.='Other';
                    		$where_trim.="ospos_vinnumbers.trim='' OR ospos_vinnumbers.trim=0 OR ospos_vinnumbers.trim IS NULL";
                    	}else{
                    		$re.=$this->item->trim_name($arrtrim[$i])->row()->trim_name;
						$where_trim.="ospos_vinnumbers.trim='".$arrtrim[$i]."'";

                    	}
						if (count($arrtrim)-2>$i) {
							$re.=', ';
							$where_trim.=" OR ";
						}
		           	}
                
            }
            $re.='</b>';
            $where_trim.=')';
            $where.=$where_trim;

		}

		$date_from = $_POST['date_from'];
		$date_to = $_POST['date_to'];
		$dateft='';
		if ($_POST['date_from']!='' && $_POST['date_to']!='') {
			$dateft .= " AND ospos_vinnumbers.date_arrived BETWEEN '$date_from' AND '$date_to'";
			$where.=$dateft;
		}
		$s = $this->input->post('search_all');
		$match = '';
		// $order_by = ' i.item_id DESC';
		$where_match = '';
		if ($s) {
			$ars = explode(' ', $s);
			for ($i=0; $i < count($ars); $i++) { 
				$where_match.= " AND( `year` LIKE '%$ars[$i]%'
									OR vinnumber_id LIKE '%$ars[$i]%'
									OR vainnumber LIKE '%$ars[$i]%'
									OR make_name LIKE '%$ars[$i]%'
									OR model_name LIKE '%$ars[$i]%'
									OR in_color_name LIKE '%$ars[$i]%'
									OR ex_color_name LIKE '%$ars[$i]%')";
			}

			$where.= $where_match;
			// $sql_mode = '';
			// if (strpos($s,' ')) {
			// 	$sql_mode = 'NATURAL LANGUAGE';
			// }else{
			// 	$sql_mode = 'BOOLEAN';
			// }

			// 	$where.= " AND (
			// 					MATCH(`year`,make_name,model_name) AGAINST('$s' IN $sql_mode MODE)
			// 					)";
			// 	$match .= ",MATCH(`year`,make_name,model_name) AGAINST('$s' IN $sql_mode MODE) as rel";
		}


		// $order_by = 'ORDER BY `vinnumber_name` ASC';

		// SORTING
		$stype = $this->input->post('dud');
		$sfield = $this->input->post('dsn');
		if ($stype && $sfield) {
			$order_by = "ORDER BY ospos_vinnumbers.$sfield $stype";
			if ($sfield=='barcode' || $sfield=='vinnumber_name') {
				$order_by = "ORDER BY ospos_vinnumbers.vinnumber_id $stype";
			}
		}

		

	
		$sql="SELECT
			*
		FROM
			(`ospos_vinnumbers`) 

		WHERE
			`ospos_vinnumbers`.`deleted` = 0
		{$where}
		AND (ospos_vinnumbers.vinnumber_name LIKE '%$search%' 
			OR ospos_vinnumbers.make_name LIKE '%$search%' 
			OR ospos_vinnumbers.model_name LIKE '%$search%' 
			OR ospos_vinnumbers.in_color_name LIKE '%$search%'
			OR ospos_vinnumbers.ex_color_name LIKE '%$search%'
			OR ospos_vinnumbers.year like '%$search%' 
			OR ospos_vinnumbers.cost_price LIKE '%$search%'
			OR ospos_vinnumbers.vainnumber LIKE '%$search%'
			)
		$order_by
		";
		
		$totalrow=$this->db->query($sql)->result();
		// ++++++++++++++++pagination++++++++++++
		$page_num=50;
		$per_page='';
		if(isset($_POST['p_num']) && $_POST['p_num']!='undefined' && $_POST['p_num']!='0' && $_POST['p_num']!='')
			$page_num=$_POST['p_num'];

		if(isset($_POST['perpage']))
			$per_page=$_POST['perpage'];
		// $config['base_url']=site_url("vinnumbers/search_vin?n=$search&m=$make_id&mo=$model_id&y=$s_year&in_c=$in_color_id&ex_c=$ex_color_id&p_num=$page_num");
		// $config['per_page']=$page_num;
		// $config['page_query_string']=TRUE;
		// $config['num_link']=3;
		// $config['total_rows']=count($totalrow);
		// $this->pagination->initialize($config);
		$data['total_rows'] =count($this->db->query($sql)->result());
		
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("vinnumbers/search_vin"),$page_num);
		$limit='';
		if ($page_num!='all') {
			$limit=" limit ".$paging['limit'];
		// if($per_page>0){
			$limit=" LIMIT {$paging['start']}, {$paging['limit']}";
		// }
		}
		
		$sql.=" {$limit}";
		$table='';
		foreach ($this->db->query($sql)->result() as $vin) {
            $image=$this->vinnm->getdefaultimg($vin->vinnumber_id);
            $im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
            if($image!=''){
                if(file_exists(FCPATH."uploads/thumb/".$image)){
                    $im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
                  }
            }

            $img_path=img($im);
			$y = get2lastString($vin->year);

			$color_vin = anchor($controller_name."/view/$vin->vinnumber_id/width:300",'<strong>'.'H'."<span style='color:#7a0606; z-index:9999'>".$vin->vinnumber_id."</span><span style='color:#067a34'>".$vin->model_id."</span><span style='color:#7a0668'>".$y.'</span></strong>',array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_update')));

			$table.='<tr>';
			$table.='<td><input type="checkbox" id="vinnumber_'.$vin->vinnumber_id.'"value="'.$vin->vinnumber_id.'"/></td>';
			$table.='<td>'.$img_path.'<div class="v_photo"><a target="_blank" href='.site_url("$controller_name/view_all_v_photo/$vin->vinnumber_id").'>View all photos</a></div></td>';
			$table.='<td>'.$color_vin.'</td>';
			$table.='<td>'.$vin->vainnumber.'</td>';
			$table.='<td>'.$vin->year.'</td>';
			$table.='<td>'.$vin->make_name.'</td>';
			$table.='<td>'.$vin->model_name.'</td>';
			$table.='<td>'.$vin->in_color_name.'</td>';
			$table.='<td>'.$vin->ex_color_name.'</td>';
			$table.='<td>'.to_currency($vin->cost_price).'</td>';
			$table.='<td>'.to_currency($vin->total_sold).'</td>';
			$table.='<td>'.$vin->date_arrived.'</td>';
			$table.='<td>
						<a target="_blank" href="'.site_url("$controller_name/view_vin_sale/$vin->vinnumber_id").'">Part Sold</a><br><br>
						<a target="_blank" href="'.site_url("$controller_name/add_vin_part/$vin->vinnumber_id").'">Available</a>

					</td>';
			$table.='</tr>';

		}
		$data['se']=1;
		$val=0;
		$per=0;
		if ($limit!='') {
			$val= $paging['limit'];
			$per=$paging['start'];
		}
		
		$total_rows = $data['total_rows'];
		if ($per=='') {
			$r1=1;
		}else{
			$r1=$per+1;
		}
		if ($per+$val<$total_rows && $per+$val!=0) {
			$r2 = $per+$val;
		}else{
			$r2 = $total_rows;
		}

		$results="<p>Showing ".$r1." to ".$r2." of ".$total_rows." results";
		// if ($re!=' of ') {
		// 	$results.=$re;
		// }
		$results.="</p>";
		
		$new_button = anchor("$controller_name/view/-1/width:$form_width",
						"<div class='big_button' style='float: left;'><span>NEW</span></div>",
						array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));
		$ft_ap = '<b>Filters Applied: </b>';
		$current_filter = $this->sale_lib->get_filter_vin();
		foreach ($current_filter as $fi) {
			if ($fi['fi']!='sa') {
				$ft_ap.=' <span class="ft_'.$fi['fi'].'" onclick=clear_'.$fi['fi'].'("'.$fi['fid'].'")>';
				$ft_ap.=$fi['fname'];
				$ft_ap.='&nbsp;<i class="fa fa-times-circle"></i></span>';
			}
			
		}
		if ($ft_ap=='<b>Filters Applied: </b>') {
			$ft_ap='';
		}
		$data['result_row'] = $results;
		$data['new_button'] = $new_button;
		$data['pager']=$datapager;
		$data['data']=$table;
		$data['nodata']="No Results";
		$data['pagination']=$paging;
		$data['filters_applied'] = $ft_ap;
		$data['real_filter'] = $current_filter;

		// var_dump($paging);
		echo json_encode($data);
		// $this->load->view('partial/header',$data);
		// $this->load->view('vinnumbers/search_result',$data);
		// $this->load->view('partial/footer');
	}
	function search_vin_old(){
		$data['controller_name']=strtolower(get_class());
		$controller_name=$data['controller_name'];
		$search='';
		$make_id='';
		$model_id='';
		$s_year='';
		$in_color_id='';
		$ex_color_id='';
		$where='';
		$pro_sta='';
		$body_id='';
		$engine_id='';
		$fuel_id='';
		$re=' of ';
		$data['title']="";
		$year_from = $_POST['year_from'];
		$year_to = $_POST['year_to'];

		if(isset($_POST['search'])){
			$search=$_POST['search'];
			$data['title']="Search Results";
			if ($search!='') {
				$re.=" Keywords: <b>$search</b>";
			}
		}

		if(isset($_POST['m']) && $_POST['m']!='' && $_POST['m']!='undefined' && $_POST['m']!='0'){
			$re.=' Make: <b>';
			$make_id=$_POST['m'];
			if ($make_id=='other') {
				$re.='Other';
				$where.=" AND (ospos_vinnumbers.make_id='' OR ospos_vinnumbers.make_id='0' OR ospos_vinnumbers.make_id IS NULL)";
			}else{
				$re.=$this->item->make_name($make_id)->row()->make_name;
				$where.=" AND ospos_vinnumbers.make_id='".$_POST['m']."'";
			}
			$re.='</b>';

		}
		if(isset($_POST['mo']) && $_POST['mo']!=''&& $_POST['mo']!='undefined' && $_POST['mo']!='0'){
			$model_id=$_POST['mo'];
			$re.=' Model: <b>';
			if ($model_id=='other') {
				$re.='Other';
				$where.=" AND (ospos_vinnumbers.model_id='' OR ospos_vinnumbers.mdoel_id='0' OR ospos_vinnumbers.model_id IS NULL)";
			}else{
				$re.=$this->item->model_name($model_id)->row()->model_name;
				$where.=" AND ospos_vinnumbers.model_id='".$_POST['mo']."'";
			}
			$re.='</b>';
		}
		if(isset($_POST['y']) && $_POST['y']!='' && $_POST['y']!='_' && $_POST['y']!='undefined' && $_POST['y']!='0'){
			$year_from = '';
			$year_to = '';
			$arryear=explode('_', $_POST['y']);
			$re.=' Year: <b>';
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
            		if ($arryear[$i]!='_' && $arryear[$i]!='') {
            			if ($arryear[$i]=='other') {
            				$re.='Other';
            				$where_year.="ospos_vinnumbers.year='' OR ospos_vinnumbers.year=0 OR ospos_vinnumbers.year IS NULL";
            				
            			}else{
            				$re.=$arryear[$i];
            				$where_year.="ospos_vinnumbers.year='".$arryear[$i]."'";

            			}
	                    if(count($arryear)-2>$i){
	                    	$re.=', ';
	                        $where_year.=' OR ';
	                    }
            		}
                    

            }
            $re.='</b>';
            $where_year.=')';
            $s_year=$_POST['y'];
            $where.=$where_year;

		}
		if(isset($_POST['in_c']) && $_POST['in_c']!='' && $_POST['in_c']!='_' && $_POST['in_c']!='undefined' && $_POST['in_c']!='0'){
			$in_arrcol=explode('_', $_POST['in_c']);
            $in_where_color=' AND(';
            	$re.=' Interior Color: <b>';
            for ($i=0; $i < count($in_arrcol) ; $i++) { 
                	if($in_arrcol[$i]!='_' && $in_arrcol[$i]!=''){
                		if ($in_arrcol[$i]=='other') {
                			$re.='Other';
            				$in_where_color.="ospos_vinnumbers.interior_color='' OR ospos_vinnumbers.interior_color=0 OR ospos_vinnumbers.interior_color IS NULL";
                			
                		}else{
							$in_where_color.="ospos_vinnumbers.interior_color='".$in_arrcol[$i]."'";
							$re.=$this->item->color_name($in_arrcol[$i])->row()->color_name;
                		}
						if (count($in_arrcol)-2>$i) {
							$in_where_color.=" OR ";
							$re.=', ';
						}
		           	}
            }
            $re.='</b>';
            $in_where_color.=')';
            $in_color_id=$_POST['in_c'];
            // echo $in_where_color;
            $where.=$in_where_color;

		}
		if(isset($_POST['ex_c']) && $_POST['ex_c']!='' && $_POST['ex_c']!='_' && $_POST['ex_c']!='undefined' && $_POST['ex_c']!='0'){
			$ex_arrcol=explode('_', $_POST['ex_c']);
            $ex_where_color=' AND(';
            	$re.=' Exterior Color: <b>';
            for ($i=0; $i < count($ex_arrcol) ; $i++) { 
                   if($ex_arrcol[$i]!='_' && $ex_arrcol[$i]!=''){
                   		if ($ex_arrcol[$i]=='other') {
                   			$re.='Other';

                   			$ex_where_color.="ospos_vinnumbers.external_color='' OR ospos_vinnumbers.external_color=0 OR ospos_vinnumbers.external_color IS NULL";
                   		}else{
                   			$re.=$this->item->color_name($ex_arrcol[$i])->row()->color_name;
							$ex_where_color.="ospos_vinnumbers.external_color='".$ex_arrcol[$i]."'";

                   		}
						if (count($ex_arrcol)-2>$i) {
							$re.=', ';
							$ex_where_color.=" OR ";
						}
		           	}
                
            }
            $re.='</b>';
            $ex_where_color.=')';
            $ex_color_id=$_POST['ex_c'];
            $where.=$ex_where_color;

		}
		
		$yearft='';
		if ($year_from!='' && $year_to!='') {
			$yearft .= " AND ospos_vinnumbers.year BETWEEN $year_from AND $year_to";
			$re.=" Year: <b>$year_from</b> to <b>$year_to</b>";
			$where.=$yearft;
		}

		if(isset($_POST['body']) && $_POST['body']!='' && $_POST['body']!='_' && $_POST['body']!='undefined' && $_POST['body']!='0'){
			$arrbody=explode('_', $_POST['body']);
            $where_body=' AND(';
            	$re.=' Body: <b>';
            for ($i=0; $i < count($arrbody) ; $i++) { 
                     if($arrbody[$i]!='_' && $arrbody[$i]!=''){
                     	if ($arrbody[$i]=='other') {
                     		$re.='Other';
                     		$where_body.="ospos_vinnumbers.body='' OR ospos_vinnumbers.body=0 OR ospos_vinnumbers.body IS NULL";
                     	}else{
                     		$re.=$this->item->body_name($arrbody[$i])->row()->body_name;
							$where_body.="ospos_vinnumbers.body='".$arrbody[$i]."'";
                     	}
						if (count($arrbody)-2>$i) {
							$where_body.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_body.=')';
            $where.=$where_body;

		}

		if(isset($_POST['engine']) && $_POST['engine']!='' && $_POST['engine']!='_' && $_POST['engine']!='undefined' && $_POST['engine']!='0'){
			$arrengine=explode('_', $_POST['engine']);
            $where_engine=' AND(';
            	$re.=' Engine: <b>';
            for ($i=0; $i < count($arrengine) ; $i++) { 
                    if($arrengine[$i]!='_' && $arrengine[$i]!=''){
                    	if ($arrengine[$i]=='other') {
                    		$re.='Other';
                    		$where_engine.="ospos_vinnumbers.engine='' OR ospos_vinnumbers.engine=0 OR ospos_vinnumbers.engine IS NULL";
                    	}else{
							$where_engine.="ospos_vinnumbers.engine='".$arrengine[$i]."'";
							$re.=$this->item->engine_name($arrengine[$i])->row()->engine_name;
                    	}
						if (count($arrengine)-2>$i) {
							$where_engine.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_engine.=')';
            $where.=$where_engine;

		}
		if(isset($_POST['fuel']) && $_POST['fuel']!='' && $_POST['fuel']!='_' && $_POST['fuel']!='undefined' && $_POST['fuel']!='0'){
			$arrfuel=explode('_', $_POST['fuel']);
            $where_fuel=' AND(';
            	$re.=' Fuel: <b>';
            for ($i=0; $i < count($arrfuel) ; $i++) { 
                    if($arrfuel[$i]!='_' && $arrfuel[$i]!=''){
                    	if ($arrfuel[$i]=='other') {
                    		$re.='Other';
                    		$where_fuel.="ospos_vinnumbers.fuel='' OR ospos_vinnumbers.fuel=0 OR ospos_vinnumbers.fuel IS NULL";
                    	}else{
                    		$re.=$this->item->fuel_name($arrfuel[$i])->row()->fule_name;
							$where_fuel.="ospos_vinnumbers.fuel='".$arrfuel[$i]."'";
                    	}
						if (count($arrfuel)-2>$i) {
							$where_fuel.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_fuel.=')';
            $where.=$where_fuel;

		}
		if(isset($_POST['trans']) && $_POST['trans']!='' && $_POST['trans']!='_' && $_POST['trans']!='undefined' && $_POST['trans']!='0'){
			$arrtrans=explode('_', $_POST['trans']);
            $where_trans=' AND(';
            	$re.=" Transmission: <b>";
            for ($i=0; $i < count($arrtrans) ; $i++) { 
                    if($arrtrans[$i]!='_' && $arrtrans[$i]!=''){
                    	if ($arrtrans[$i]=='other') {
                    		$re.="Other";
                    		$where_trans.="ospos_vinnumbers.transmission='' OR ospos_vinnumbers.transmission=0 OR ospos_vinnumbers.transmission IS NULL";
                    	}else{
							$where_trans.="ospos_vinnumbers.transmission='".$arrtrans[$i]."'";
							$re.=$this->item->trans_name($arrtrans[$i])->row()->transmission_name;
                    	}
						if (count($arrtrans)-2>$i) {
							$where_trans.=" OR ";
							$re.=', ';
						}
		           	}
                
            }
            $re.='</b>';
            $where_trans.=')';
            $where.=$where_trans;

		}
		if(isset($_POST['trim']) && $_POST['trim']!='' && $_POST['trim']!='_' && $_POST['trim']!='undefined' && $_POST['trim']!='0'){
			$arrtrim=explode('_', $_POST['trim']);
            $where_trim=' AND(';
            	$re.=' Trim: <b>';
            for ($i=0; $i < count($arrtrim) ; $i++) { 
                    if($arrtrim[$i]!='_' && $arrtrim[$i]!=''){
                    	if ($arrtrim[$i]=='other') {
                    		$re.='Other';
                    		$where_trim.="ospos_vinnumbers.trim='' OR ospos_vinnumbers.trim=0 OR ospos_vinnumbers.trim IS NULL";
                    	}else{
                    		$re.=$this->item->trim_name($arrtrim[$i])->row()->trim_name;
						$where_trim.="ospos_vinnumbers.trim='".$arrtrim[$i]."'";

                    	}
						if (count($arrtrim)-2>$i) {
							$re.=', ';
							$where_trim.=" OR ";
						}
		           	}
                
            }
            $re.='</b>';
            $where_trim.=')';
            $where.=$where_trim;

		}
		$date_from = $_POST['date_from'];
		$date_to = $_POST['date_to'];
		$dateft='';
		if ($_POST['date_from']!='' && $_POST['date_to']!='') {
			$dateft .= " AND ospos_vinnumbers.date_arrived BETWEEN '$date_from' AND '$date_to'";
			$where.=$dateft;
		}

	
		$sql="SELECT
			*, `ex`.`color_name` AS ex_co,
			`in_c`.`color_name` AS in_co
		FROM
			(`ospos_vinnumbers`)
		LEFT JOIN `ospos_colors` ex ON `ex`.`color_id` = `ospos_vinnumbers`.`external_color`
		LEFT JOIN `ospos_colors` in_c ON `in_c`.`color_id` = `ospos_vinnumbers`.`interior_color`
		LEFT JOIN `ospos_models` ON `ospos_models`.`model_id` = `ospos_vinnumbers`.`model_id`
		LEFT JOIN `ospos_makes` ON `ospos_makes`.`make_id` = `ospos_vinnumbers`.`make_id`
		LEFT JOIN ospos_body ON ospos_body.body_id=ospos_vinnumbers.body
		LEFT JOIN ospos_engine ON ospos_engine.engine_id=ospos_vinnumbers.engine
		LEFT JOIN ospos_fuel ON ospos_fuel.fuel_id=ospos_vinnumbers.fuel
		LEFT JOIN ospos_transmission ON ospos_transmission.trans_id=ospos_vinnumbers.transmission
		LEFT JOIN ospos_trim ON ospos_trim.trim_id=ospos_vinnumbers.trim

		WHERE
			`ospos_vinnumbers`.`deleted` = 0
		{$where}
		AND (ospos_vinnumbers.vinnumber_name LIKE '%$search%' 
			OR ospos_makes.make_name LIKE '%$search%' 
			OR ospos_models.model_name LIKE '%$search%' 
			OR ex.color_name LIKE '%$search%'
			OR in_c.color_name LIKE '%$search%'
			OR ospos_vinnumbers.year like '%$search%' 
			OR ospos_vinnumbers.description LIKE '%$search%'
			OR ospos_vinnumbers.cost_price LIKE '%$search%'
			OR ospos_engine.engine_name LIKE '%$search%'
			OR ospos_fuel.fule_name LIKE '%$search%'
			OR ospos_transmission.transmission_name LIKE '%$search%'
			OR ospos_trim.trim_name LIKE '%$search%'
			OR ospos_vinnumbers.vainnumber LIKE '%$search%'

			OR ospos_body.body_name LIKE '%$search%')
		ORDER BY
			`vinnumber_name` ASC";
		
		$totalrow=$this->db->query($sql)->result();
		// ++++++++++++++++pagination++++++++++++
		$page_num=10;
		$per_page='';
		if(isset($_POST['p_num']) && $_POST['p_num']!='undefined' && $_POST['p_num']!='0' && $_POST['p_num']!='')
			$page_num=$_POST['p_num'];

		if(isset($_POST['perpage']))
			$per_page=$_POST['perpage'];
		// $config['base_url']=site_url("vinnumbers/search_vin?n=$search&m=$make_id&mo=$model_id&y=$s_year&in_c=$in_color_id&ex_c=$ex_color_id&p_num=$page_num");
		// $config['per_page']=$page_num;
		// $config['page_query_string']=TRUE;
		// $config['num_link']=3;
		// $config['total_rows']=count($totalrow);
		// $this->pagination->initialize($config);
		$data['total_rows'] =count($this->db->query($sql)->result());
		
		$paging=$this->green->ajax_pagination(count($this->db->query($sql)->result()),site_url("vinnumbers/search_vin"),$page_num);
		$limit='';
		if ($page_num!='all') {
			$limit=" limit ".$paging['limit'];
		// if($per_page>0){
			$limit=" LIMIT {$paging['start']}, {$paging['limit']}";
		// }
		}
		
		$sql.=" {$limit}";
		$table='';
		foreach ($this->db->query($sql)->result() as $vin) {
            $image=$this->vinnm->getdefaultimg($vin->vinnumber_id);
            $im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
            if($image!=''){
                if(file_exists(FCPATH."uploads/thumb/".$image)){
                    $im=array('src'=>'uploads/thumb/'.$image,'width'=>"120");
                  }
            }

            $img_path=img($im);
			$y = get2lastString($vin->year);

			$color_vin = anchor($controller_name."/view/$vin->vinnumber_id/width:300",'<strong>'.'H'."<span style='color:#7a0606; z-index:9999'>".$vin->vinnumber_id."</span><span style='color:#067a34'>".$vin->model_id."</span><span style='color:#7a0668'>".$y.'</span></strong>',array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_update')));

			$table.='<tr>';
			$table.='<td><input type="checkbox" id="vinnumber_'.$vin->vinnumber_id.'"value="'.$vin->vinnumber_id.'"/></td>';
			$table.='<td>'.$img_path.'<div class="v_photo"><a target="_blank" href='.site_url("$controller_name/view_all_v_photo/$vin->vinnumber_id").'>View all photos</a></div></td>';
			$table.='<td>'.$color_vin.'</td>';
			$table.='<td>'.$vin->vainnumber.'</td>';
			$table.='<td>'.$vin->year.'</td>';
			$table.='<td>'.$vin->make_name.'</td>';
			$table.='<td>'.$vin->model_name.'</td>';
			$table.='<td>'.$vin->in_co.'</td>';
			$table.='<td>'.$vin->ex_co.'</td>';
			$table.='<td>'.to_currency($vin->cost_price).'</td>';
			$table.='<td>'.to_currency($vin->total_sold).'</td>';
			$table.='<td>'.$vin->date_arrived.'</td>';
			$table.='<td>
						<a target="_blank" href="'.site_url("$controller_name/view_vin_sale/$vin->vinnumber_id").'">Part Sold</a><br><br>
						<a target="_blank" href="'.site_url("$controller_name/add_vin_part/$vin->vinnumber_id").'">Available</a>

					</td>';
			$table.='</tr>';

		}
		$data['se']=1;
		$val=0;
		$per=0;
		if ($limit!='') {
			$val= $paging['limit'];
			$per=$paging['start'];
		}
		
		$total_rows = $data['total_rows'];
		if ($per=='') {
			$r1=1;
		}else{
			$r1=$per+1;
		}
		if ($per+$val<$total_rows && $per+$val!=0) {
			$r2 = $per+$val;
		}else{
			$r2 = $total_rows;
		}

		$results="<p>Showing ".$r1." to ".$r2." of ".$total_rows." results";
		if ($re!=' of ') {
			$results.=$re;
		}
		$results.="</p>";
		
		$new_button = anchor("$controller_name/view/-1/width:$form_width",
						"<div class='big_button' style='float: left;'><span>NEW</span></div>",
						array('class'=>'thickbox none','title'=>$this->lang->line($controller_name.'_new')));

		$data['result_row'] = $results;
		$data['new_button'] = $new_button;
		$data['pager']=$datapager;
		$data['data']=$table;
		$data['nodata']="No Results";
		$data['pagination']=$paging;
		// var_dump($paging);
		echo json_encode($data);
		// $this->load->view('partial/header',$data);
		// $this->load->view('vinnumbers/search_result',$data);
		// $this->load->view('partial/footer');
	}
	function search_sold_item($vin_id)
	{
		$search=$this->input->post('search');
		// echo $search;die();
		$data['controller_name']=strtolower(get_class());

		$data['sale_data']=$this->vinnm->get_sale_item($vin_id,$search);
		// print_r($data['sale_data']);die();
		$this->load->view("vinnumbers/form_sale",$data);
	}
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Vinnumber->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$vinnumber_id = $this->input->post('row_id');
		$data_row=get_vinnumber_data_row($this->Vinnumber->get_info($vinnumber_id),$this);
		echo $data_row;
	}

	function view($vinnumber_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['vinnumber_info']=$this->Vinnumber->get_info($vinnumber_id);

		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Vinnumber->get_info($vinnumber_id)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Vinnumber->get_info($vinnumber_id)->model_id;
		
		/*************************internal color**************************************/
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Vinnumber->get_info($vinnumber_id)->interior_color;
		/*************************end internal color**************************************/
		
		/*************************external color**************************************/
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['enternal_colors']=$colors;
		$data['enternal_selected_color'] = $this->Vinnumber->get_info($vinnumber_id)->external_color;
		/*************************end external color**************************************/
		//$data['head_image_name']=$this->Vinnumber->get_head_image_source($vinnumber_id)->result_array();
		

		$this->load->view("vinnumbers/form",$data);
	}
	function view_vin_sale($vin_id){
		$data['controller_name']=strtolower(get_class());

		$data['sale_data']=$this->vinnm->get_sale_item($vin_id);
		// var_dump($data['sale_data']);
		$this->load->view("vinnumbers/form_sale",$data);
	}
	function view_invoice_items($invoiceid){
		$data['items'] = $this->vinnm->get_invoice_items($invoiceid);
		$this->load->view("vinnumbers/invoice_items",$data);
	}
	function save($vinnumber_id=-1)
	{
		$this->load->helper('url');
		$makeid=$this->input->post('make_id');
		$modelid=$this->input->post('model_id');
		$year_input=$this->input->post('year');
		$year=$this->get2lastString($year_input);
		
		$cost_price=$this->input->post('vin_cost_price');
		$internal_color=$this->input->post('internal_color');
		$external_color=$this->input->post('external_color');
		$vinnumber_code = $this->input->post('vinnumber_code');
		$date_arrived = $this->input->post('date_arrived');
		$engine = $this->input->post('vin_engine');
		$trans = $this->input->post('vin_trans');
		$trim = $this->input->post('vin_trim');
		$fuel = $this->input->post('vin_fuel');
		$body = $this->input->post('vin_body');

		$make_name = $this->item->make_name($makeid)->row()->make_name;
		$model_name =$this->item->model_name($modelid)->row()->model_name;
		$in_color_name = $this->item->color_name($internal_color)->row()->color_name;
		$ex_color_name = $this->item->color_name($external_color)->row()->color_name;
		
		$model_vinnumber=$this->load->model('vinnumber');
		
		
		if($vinnumber_id==-1){
		
		$vinnumber='H'.($this->vinnumber->getLastId_vinnumber()+1).$modelid.$year;
		
		}else{
		$vinnumber='H'.$vinnumber_id.$modelid.$year;
		
		}
		
		$vinnumber_data = array(
		'vinnumber_name'=>$vinnumber,
		'cost_price'=>$cost_price,
		'interior_color'=>$internal_color,
		'external_color'=>$external_color,
		'description'=>$this->input->post('description'),
		'make_id'=>$makeid,
		'model_id'=>$modelid,
		'year'=>$year_input,
		'date_arrived'=>$date_arrived,
		'vainnumber'=>$vinnumber_code,
		'engine'=>$engine,
		'transmission'=>$trans,
		'trim'=>$trim,
		'fuel'=>$fuel,
		'body'=>$body,
		'make_name'=>$make_name,
		'model_name'=>$model_name,
		'in_color_name'=>$in_color_name,
		'ex_color_name'=>$ex_color_name
		);
		
		// var_dump($vinnumber_data);die();
		if( $this->Vinnumber->save( $vinnumber_data, $vinnumber_id ) )
		{
			//New vinnumber
			if($vinnumber_id==-1)
			{
				// echo json_encode(array('success'=>true,'message'=>$this->lang->line('vinnumbers_successful_adding').' '.
				// $vinnumber_data['vinnumber_name'],'vinnumber_id'=>$vinnumber_data['vinnumber_id']));
				$vinnumber_id = $vinnumber_data['vinnumber_id'];
				// $this->addHeadImage($vinnumber_id);
				$this->upload($vinnumber_id);
			}
			else //previous vinnumber
			{
				// echo json_encode(array('success'=>true,'message'=>$this->lang->line('vinnumbers_successful_updating').' '.
				// $vinnumber_data['vinnumber_name'],'vinnumber_id'=>$vinnumber_id));
				// $this->addHeadImage($vinnumber_id);
				$this->upload($vinnumber_id);
			}
			
		}
		else//failure
		{
			// echo json_encode(array('success'=>false,'message'=>$this->lang->line('vinnumbers_error_adding_updating').' '.
			// $vinnumber_data['vinnumber_name'],'vinnumber_id'=>-1));
		}
		redirect('vinnumbers/');

	}



// ================================new upload==========================
	function upload($productid)
	{       

	    $this->load->library('upload');
	    $orders=$this->input->post('order');
	    $updimg=$this->input->post('updimg');
		$arrid=$this->input->post('deleteimg');
		$arrid=trim($arrid,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_head_images where vin_image_id='".$arr[$i]."'")->row();
				if(isset($row->url_image)){
					unlink('./uploads/'.$row->url_image);
					unlink('./uploads/thumb/'.$row->url_image);
					$this->db->where('vin_image_id',$row->vin_image_id)->delete('ospos_head_images');
				}
			}
		}
		// echo $arrid;
	 //    $this->unlinkpic($productid,$arrid);

	    $files = $_FILES;
	    // var_dump($productid);die();
	    $cpt = count($_FILES['userfile']['name']);
	    for($i=0; $i<$cpt; $i++)
	    {         
	    	if(isset($updimg[$i]) && $updimg[$i]!=''){
	    		$this->updatepic($updimg[$i],$orders[$i]);
	    	}  
	    	// echo $orders[$i];
	        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
	        
	        $this->upload->initialize($this->set_upload_options($productid,$_FILES['userfile']['name']));
	        // $this->upload->do_upload();
	        if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());	
				 // echo $error['error'];	die();	
			}else{	
				// echo $_FILES['userfile']['name'];
				$this->creatthumb($productid,$_FILES['userfile']['name'],$orders[$i]);
			}
	    }
	}
	function updatepic($picid,$order){
		$this->db->where('vin_image_id',$picid)->set('first_image',$order)->update('ospos_head_images');
	}
	function creatthumb($productid,$imagename,$order){
			$data = array('upload_data' => $this->upload->data());
		 	$config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = './uploads/thumb';
            $config2['maintain_ratio'] = false;
            $config2['create_thumb'] = "$productid".'_head_'."$imagename";
            $config2['thumb_marker'] = false;
            $config2['height'] = 190;
            $config2['width'] = 195;
            $this->load->library('image_lib');
            $this->image_lib->initialize($config2); 
            if ( ! $this->image_lib->resize()){
            	echo $this->image_lib->display_errors();
			}else{
				$this->saveimg($productid,$this->upload->file_name,$order);
			}
		
	}
	private function set_upload_options($productid,$imagename)
	{   
	    //upload an image options
	    if(!file_exists('./uploads/') || !file_exists('./uploads/')){
		    if(mkdir('./uploads/',0755,true)){
		        return true;
		    }
	    	if(mkdir('./uploads/thumb',0755,true)){
		                return true;
		    }
		}
	    $config = array();
	    $config['upload_path'] = './uploads/';
	    $config['allowed_types'] = 'gif|jpg|jpeg|png';
	    $config['max_size']      = '0';
	    $config['file_name']  	 = "$productid".'_head_'."$imagename";
		$config['overwrite']	 = true;
		$config['file_type']='image/png';

	    return $config;
	}
	function saveimg($productid,$imagename,$order){
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		$count=$this->db->query("SELECT count(*) as count FROM ospos_head_images where vin_number='$productid' AND url_image='$imagename'")->row()->count;
		if($count==0){
			$data=array('vin_number'=>$productid,
						'url_image'=>$imagename,
						'first_image'=>$order);
			$this->db->insert('ospos_head_images',$data);
		}
	}
	function unlinkpic($stockid,$arrids){
		$arrid=trim($arrids,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_head_images where vin_image_id='".$arr[$i]."'")->row();
				if(isset($row->pic_name)){
					unlink('./uploads/'.$row->url_image);
					unlink('./uploads/thumb/'.$row->url_image);
					$this->db->where('image_id',$row->vin_image_id)->delete('ospos_head_images');
				}
			}
		}
	}

	// ============================================================================


	function delete()
	{
		$this->permission_lib->checkPermission();
		$vinnumbers_to_delete=$this->input->post('ids');
		// var_dump($vinnumbers_to_delete);die();

		if($this->Vinnumber->delete_list($vinnumbers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('vinnumbers_successful_deleted').' '.
			count($vinnumbers_to_delete).' '.$this->lang->line('vinnumbers_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('vinnumbers_cannot_be_deleted')));
		}
	}
	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	
 	function addHeadImage($vin_head_id){
 			
 		for($j=0;$j<count($_FILES['path']['name']);$j++){	
			if ($_FILES['path']['name'][$j]!=""){

			
				
				$image_directory = "./uploads/";
				$big_directory = "./uploads/";
				$allowed_ext = "jpg, JPG, gif, GIF , bmp, BMP, jpeg, JPEG , png, PNG"; 
				$extension = pathinfo($_FILES['path']['name'][$j]);
				$extension = $extension["extension"];
				$allowed_paths = explode(", ", $allowed_ext);
				for($i = 0; $i < count($allowed_paths); $i++) {
					if ($allowed_paths[$i] == $extension) {
						 $ok="1";
					}
				}
				if($ok=="1"){
						$concatenate_str1 = time();
						if(is_uploaded_file($_FILES['path']['tmp_name'][$j])){	
								$picture_name=$_FILES['path']['name'][$j];	
								move_uploaded_file($_FILES['path']['tmp_name'][$j],$image_directory.$concatenate_str1.$picture_name);
							  $imageurl=$concatenate_str1.$picture_name;
						}
		
		$file_data=array('vin_number'=>$vin_head_id,'url_image'=>$concatenate_str1.$picture_name);
		
 		$this->load->database();
	    $ret=$this->db->insert('head_images',$file_data);		
				}
					
			}		   
 		}

 	
 }

 	function deleteHeadImage(){
 		$value=true;
 		$imageid=$this->input->post('imageid');
 		if($imageid){
 			$this->load->model('vinnumber');
 			$image_name=$this->Vinnumber->getImageName($imageid);
 		$delete=$this->Vinnumber->delete_headImage_item($imageid);
 		if($delete){
 			 unlink("./uploads/".$image_name);
 		}
 		
 		}
	
 	} 
 

 function go(){
 		header('Content-Type: application/json');
 	
 		$itemnumber=-1;
 		
 		$itemnumber=$_REQUEST['vinvalue'];
 		
 		$data['item_info']=$this->Vinnumber->get_Vin_Info($itemnumber);
 
		
		echo json_encode($data);
	
 	}		
 	function view_all_v_photo($vin_id){
		$image=$this->Vinnumber->getimage($vin_id);
		$data['img']=array();
		foreach ($image as $image) {
			$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");

			if($image->url_image!=''){
				if(file_exists(FCPATH."uploads/".$image->url_image)){
					$im=array('src'=>'uploads/'.$image->url_image);
			      }
			}
			array_push($data['img'], $im);
		}
		$this->load->view('vinnumbers/view_photos',$data);
 	}
 	function gemodel_pos_old(){
		$make_id=$this->input->post('make');
		$model_id=$this->input->post('model');
		$in_color_id=$this->input->post('in_color');
		$ex_color_id=$this->input->post('ex_color');
		$year=$this->input->post('year');
		$name=$this->input->post('name');
		$year_from =$this->input->post('year_from');
		$year_to = $this->input->post('year_to');
		$body_id = $this->input->post('body');
		$search = $this->input->post('search');
		$engine_id = $this->input->post('engine');
		$fuel_id = $this->input->post('fuel');
		$trans_id = $this->input->post('trans');
		$trim_id = $this->input->post('trim');
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$where='';
		$arr=array();

		$make_check='';
		$make_data='';
		if ($make_id=='') $make_check='checked';
		$make_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_make();' id='clear_make_btn'>Clear</li>";
			
		$make_data.="<li><label><input type='radio' onclick='search(1,e,0);'  name='ckmake' class='ckmake' $make_check value=''> All</label></li>";
		$make=$this->db->query("SELECT * FROM ospos_makes WHERE deleted=0 ORDER BY make_name ASC")->result();
		foreach ($make as $m) {
			$count=$this->vinnm->getnumsearch('make_id',$search,$m->make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
			$sele='';
			if($make_id==$m->make_id)
				$sele='checked';
			if($count>0)
				$make_data.="<li><label><input type='radio' onclick='search(1,e,0);' name='ckmake' $sele class='ckmake' value='$m->make_id'/> $m->make_name ($count)</label></li>";
		}		
		$make_other=$this->vinnm->getnumsearch_other('make_id',$search,'',$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);

		$make_other_check='';
		if ($make_id=='other') {
			$make_other_check='checked';
		}
		if ($make_other>0) {
			$make_data.="<li><label><input type='radio' onclick='search(1,e,0);'  name='ckmake' class='ckmake' $make_other_check value='other'> OTHER($make_other)</label></li>";
			
		}

		$model_check='';
		$modeldata='';
		if($model_id=='') $model_check='checked';
		$modeldata .="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_model();' class='clear_model_btn'>Clear</li>";
		$modeldata.="<li><label><input type='radio' onclick='search(1,e,0);'  name='ckmodel' class='ckmodel' $model_check value=''> All</label></li>";
		$wheremodel='';
		if($make_id!='')
			$wheremodel=" AND make_id='$make_id'";
		$data=$this->db->query("SELECT * FROM ospos_models WHERE deleted='0' {$wheremodel} ORDER BY model_name")->result();
		foreach ($data as $mo) {
			$count=$this->vinnm->getnumsearch('model_id',$search,$make_id,$mo->model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
			$sele='';
			if($model_id==$mo->model_id)
					$sele='checked';
			if($count>0)
				$modeldata.="<li rel='$mo->make_id'><label><input $sele onclick='search(1,e,0);' type='radio' name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name($count)</label></li>";

		}
		$model_other=$this->vinnm->getnumsearch_other('model_id',$search,$make_id,'',$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);

		$model_other_check='';
		if ($model_id=='other') {
			$model_other_check='checked';
		}
		if ($model_other>0) {
			$model_data.="<li><label><input type='radio' onclick='getmodel(event,0);'  name='ckmake' class='ckmake' $model_other_check value='other'> OTHER($model_other)</label></li>";
			
		}
		


		$year_check='';
		if($year=='' || $year=='_'){
			$year_check='checked';
		}
		$yeardata='';

		$yeararr=explode('_', $year);
		$arrofyear=array();
		$where_year=' AND(';
		for ($i=0; $i <count($yeararr) ; $i++) { 
			$arrofyear[$yeararr[$i]]=$yeararr[$i];
			if ($yeararr[$i]!='' && $yeararr[$i]!='_'){
				$where_year.="v.year='".$yeararr[$i]."'";
				if(count($yeararr)-2>$i){
                	$where_year.=' OR ';
            	}
			}
            
			
		}
		$where_year.=')';
		if ($year_check!='') {
			$year_from=$year_from;
			$year_to = $year_to;
		}else{
			$year_from='';
			$year_to='';
		}
		$yeardata.="
		<div align='center' style='border-bottom:1px solid #21759B;padding-bottom:10px;margin-bottom:5px;width: 160px'>
			
			<input style='width:40px;' maxlength='4' class='year_ft' id='year_from' type='text' placeholder='Year' onkeyup='chkYear();' value='$year_from'>
			To
			<input style='width:40px;' maxlength='4' class='year_ft' id='year_to' type='text' placeholder='Year' value='$year_to'>
			<button onclick='yearft();' class='btn btn-primary'>Go</button>
		</div>";
		$yeardata.="<li style='float:right;cursor:pointer;color:#21759B' id='clear_year_btn' onclick='clear_year();'>Clear</li>";
		$yeardata.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $year_check name='ckyear' class='ckyear' value=''> All</label></li>";
		for($j=date('Y');$j>=1980;$j--){
				$sele='';
				$count=$this->vinnm->getnumsearch('year',$search,$make_id,$model_id,$j,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count>0)
					$yeardata.= "<li><label><input onclick='getmodel(event,0);' type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j ($count)</label></li>";# code...

		}
		$year_other=$this->vinnm->getnumsearch_other('year',$search,$make_id,$model_id,'',$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);

		$year_other_check='';
		if (isset($arrofyear['other'])) {
			$year_other_check='checked';
			
		}
		if ($year_other>0) {
			$yeardata.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $year_other_check name='ckyear' class='ckyear' value='other'> OTHER($year_other)</label></li>";
			
		}

		$whereinc='';
		$in_color_check='';
		if($in_color_id=='' || $in_color_id=='_') {
			$in_color_check='checked';
		}
		$in_colordata='';

		$where_in_co='';
		$where_in_co=' AND(';
		$in_colorarr=explode('_', $in_color_id);
		$in_arrofcolor=array();
		for ($i=0; $i <count($in_colorarr) ; $i++) { 
			$in_arrofcolor[$in_colorarr[$i]]=$in_colorarr[$i];
           	if($in_colorarr[$i]!='_' && $in_colorarr[$i]!=''){
				$where_in_co.="v.interior_color='".$in_colorarr[$i]."'";
				if (count($in_colorarr)-2>$i) {
					$where_in_co.=" OR ";
				}
           	}
			
			// echo $in_colorarr[$i];
		}
		// $where_in_co.=')';
		// echo $where_in_co;
		// if ($make_id!='') {
		// 	$whereinc .=" AND v.make_id='$make_id'";
		// }
		// if ($model_id !='') {
		// 	$whereinc .= " AND v.model_id='$model_id'";
		// }
		// if ($year!='_' && $year!='') {
		// 	$whereinc .= $where_year;
		// }
		$in_colordata="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_in();' class='clear_in_btn'>Clear</li>";
		$in_colordata.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $in_color_check name='ck-incolor' class='ckcolor' value=''> All</label></li>";
		$in_color=$this->db->query("SELECT * FROM ospos_colors c
								WHERE c.deleted=0 ORDER BY c.color_name ASC")->result();
			
			foreach ($in_color as $in_col) {
				$sele='';
				$count=$this->vinnm->getnumsearch('interior_color',$search,$make_id,$model_id,$year,$in_col->color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
				if(isset($in_arrofcolor[$in_col->color_id]))
					$sele='checked';
				if($count>0)
					$in_colordata.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ck-incolor' class='ckcolor' value='$in_col->color_id'/> $in_col->color_name($count)</label></li>";# code...
				
			}
		$in_other=$this->vinnm->getnumsearch_other('interior_color',$search,$make_id,$model_id,$year,'',$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);

		$in_other_check='';
		if (isset($in_arrofcolor['other'])) {
			$in_other_check='checked';
		}
		if ($in_other>0) {
		$in_colordata.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $in_other_check name='ck-incolor' class='ckcolor' value='other'> OTHER($in_other)</label></li>";
			
		}

		$ex_color_check='';
		if($ex_color_id=='' || $ex_color_id=='_'){
			$ex_color_check='checked';
		}
		$ex_colordata='';

		$ex_colorarr=explode('_', $ex_color_id);
		$ex_arrofcolor=array();
		for ($i=0; $i <count($ex_colorarr) ; $i++) { 
			$ex_arrofcolor[$ex_colorarr[$i]]=$ex_colorarr[$i];
		}
		
		$ex_colordata="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_ex();' class='clear_ex_btn'>Clear</li>";
		$ex_colordata.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $ex_color_check name='ck-excolor' class='ck-excolor' value=''> All</label></li>";
		$ex_color=$this->db->query("SELECT * FROM ospos_colors c
								INNER JOIN ospos_vinnumbers v ON v.external_color=c.color_id
								WHERE c.deleted=0 GROUP BY c.color_id ORDER BY color_name ASC")->result();
			foreach ($ex_color as $ex_col) {
				$sele='';
				$count=$this->vinnm->getnumsearch('external_color',$search,$make_id,$model_id,$year,$in_color_id,$ex_col->color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);

				if(isset($ex_arrofcolor[$ex_col->color_id]))
					$sele='checked';
				if($count>0)
					$ex_colordata.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ck-excolor' class='ck-excolor' value='$ex_col->color_id'/> $ex_col->color_name($count)</label></li>";# code...
			}
		$ex_other=$this->vinnm->getnumsearch_other('external_color',$search,$make_id,$model_id,$year,$in_color_id,'',$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
		$ex_other_check='';
		if (isset($ex_arrofcolor['other'])) {
			$ex_other_check='checked';
		}
		if ($ex_other>0) {
		$ex_colordata.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $ex_other_check name='ck-excolor' class='ck-excolor' value='other'> OTHER($ex_other)</label></li>";
			
		}

		// BODY
		$arrbody=explode('_', $body_id);
        $arrofbody=array();
        for ($i=0; $i < count($arrbody) ; $i++) { 
			$arrofbody[$arrbody[$i]]=$arrbody[$i];     
        }
		$body_check='';
		if($body_id=='' || $body_id=='_') {
			$body_check='checked';
		}
		$body_data='';
		$body_data="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_body();' class='clear_body_btn'>Clear</li>";
		$body_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $body_check name='ckbody' class='ckbody' value=''> All</label></li>";
		$body=$this->db->query("SELECT * FROM ospos_body b
								INNER JOIN ospos_vinnumbers v ON v.body=b.body_id
								WHERE b.deleted=0 GROUP BY b.body_id ORDER BY body_name ASC")->result();
			foreach ($body as $bo) {
				$sele='';
				$count=$this->vinnm->getnumsearch('body',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$bo->body_id,$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
				if(isset($arrofbody[$bo->body_id]))
					$sele='checked';
				if($count>0)
					$body_data.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ckbody' class='ckbody' value='$bo->body_id'/> $bo->body_name($count)</label></li>";# code...
				
			}
		$body_other=$this->vinnm->getnumsearch_other('body',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,'',$engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
		// var_dump($body_other);
		$body_other_check='';
		if (isset($arrofbody['other'])) {
			$body_other_check='checked';
		}
		// echo $body_other_check;
		// echo $body_other;
		if ($body_other>0) {
		$body_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $body_other_check name='ckbody' class='ckbody' value='other'> OTHER($body_other)</label></li>";
			
		}
		// ENGINE
        $arrengine=explode('_', $engine_id);
        $arrofengine=array();
        for ($i=0; $i < count($arrengine) ; $i++) { 
			$arrofengine[$arrengine[$i]]=$arrengine[$i];     
        }
		$engine_check='';
		if($engine_id=='' || $engine_id=='_') {
			$engine_check='checked';
		}
		$engine_data='';
		$engine_data="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_engine();' class='clear_engine_btn'>Clear</li>";
		$engine_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $engine_check name='ckengine' class='ckengine' value=''> All</label></li>";
		$engine=$this->db->query("SELECT * FROM ospos_engine e
								INNER JOIN ospos_vinnumbers v ON v.engine=e.engine_id
								WHERE e.deleted=0 GROUP BY e.engine_id ORDER BY engine_name ASC")->result();
			foreach ($engine as $en) {
				$sele='';
				$count=$this->vinnm->getnumsearch('engine',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$en->engine_id,$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
				if(isset($arrofengine[$en->engine_id]))
					$sele='checked';
				if($count>0)
					$engine_data.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ckengine' class='ckengine' value='$en->engine_id'/> $en->engine_name($count)</label></li>";# code...
				
			}
		$engine_other=$this->vinnm->getnumsearch_other('engine',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,'',$body_id,'',$fuel_id,$trans_id,$trim_id,$date_from,$date_to);
		$engine_other_check='';
		if (isset($arrofengine['other'])) {
			$engine_other_check='checked';
		}
		if ($engine_other>0) {
		$engine_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $engine_other_check name='ckengine' class='ckengine' value='other'> OTHER($engine_other)</label></li>";
			
		}

		// FUEL

        $arrfuel=explode('_', $fuel_id);
        $arroffuel=array();
        for ($i=0; $i < count($arrfuel) ; $i++) { 
			$arroffuel[$arrfuel[$i]]=$arrfuel[$i];     
        }
		$fuel_check='';
		if($fuel_id=='' || $fuel_id=='_') {
			$fuel_check='checked';
		}
		$fuel_data='';
		$fuel_data="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_fuel();' class='clear_fuel_btn'>Clear</li>";
		$fuel_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $fuel_check name='ckfuel' class='ckfuel' value=''> All</label></li>";
		$fuel=$this->db->query("SELECT * FROM ospos_fuel f
								INNER JOIN ospos_vinnumbers v ON v.fuel=f.fuel_id
								WHERE f.deleted=0 GROUP BY f.fuel_id ORDER BY fule_name ASC")->result();
			foreach ($fuel as $fu) {
				$sele='';
				$count=$this->vinnm->getnumsearch('fuel',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fu->fuel_id,$trans_id,$trim_id,$date_from,$date_to);
				if(isset($arroffuel[$fu->fuel_id]))
					$sele='checked';
				if($count>0)
					$fuel_data.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ckfuel' class='ckfuel' value='$fu->fuel_id'/> $fu->fule_name($count)</label></li>";# code...
				
			}
		$fuel_other=$this->vinnm->getnumsearch_other('fuel',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,'',$trans_id,$trim_id,$date_from,$date_to);
		$fuel_other_check='';
		if (isset($arroffuel['other'])) {
			$fuel_other_check='checked';
		}
		if ($fuel_other>0) {
		$fuel_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $fuel_other_check name='ckfuel' class='ckfuel' value='other'> OTHER($fuel_other)</label></li>";
			
		}
			// TRANSMISSION
	
        $arrtrans=explode('_', $trans_id);
        $arroftrans=array();
        for ($i=0; $i < count($arrtrans) ; $i++) { 
			$arroftrans[$arrtrans[$i]]=$arrtrans[$i];     
        }
	
		$trans_check='';
		if($trans_id=='' || $trans_id=='_') {
			$trans_check='checked';
		}
		$trans_data='';
		$trans_data="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_trans();' class='clear_trans_btn'>Clear</li>";
		$trans_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $trans_check name='cktrans' class='cktrans' value=''> All</label></li>";
		$trans=$this->db->query("SELECT * FROM ospos_transmission t
								INNER JOIN ospos_vinnumbers v ON v.transmission=t.trans_id
								WHERE t.deleted=0 GROUP BY t.trans_id ORDER BY transmission_name ASC")->result();
			foreach ($trans as $tr) {
				$sele='';
				$count=$this->vinnm->getnumsearch('transmission',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$tr->trans_id,$trim_id,$date_from,$date_to);
				if(isset($arroftrans[$tr->trans_id]))
					$sele='checked';
				if($count>0)
					$trans_data.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='cktrans' class='cktrans' value='$tr->trans_id'/> $tr->transmission_name($count)</label></li>";# code...
				
			}
		$trans_other=$this->vinnm->getnumsearch_other('transmission',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,'',$trim_id,$date_from,$date_to);
		$trans_other_check='';
		if (isset($arroftrans['other'])) {
			$trans_other_check='checked';
		}
		if ($trans_other>0) {
		$trans_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $trans_other_check name='cktrans' class='cktrans' value='other'> OTHER($trans_other)</label></li>";
			
		}

				// TRIM
        $arrtrim=explode('_', $trim_id);
        $arroftrim=array();
        for ($i=0; $i < count($arrtrim) ; $i++) { 
			$arroftrim[$arrtrim[$i]]=$arrtrim[$i];     
        }
	   
		$trim_check='';
		if($trim_id=='' || $trim_id=='_') {
			$trim_check='checked';
		}
		$trim_data='';
		$trim_data="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_trim();' class='clear_trim_btn'>Clear</li>";
		$trim_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $trim_check name='cktrim' class='cktrim' value=''> All</label></li>";
		$trim=$this->db->query("SELECT * FROM ospos_trim t
								INNER JOIN ospos_vinnumbers v ON v.trim=t.trim_id
								WHERE t.deleted=0 GROUP BY t.trim_id ORDER BY trim_name ASC")->result();
			foreach ($trim as $tri) {
				$sele='';
				$count=$this->vinnm->getnumsearch('trim',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,$tri->trim_id,$date_from,$date_to);
				if(isset($arroftrim[$tri->trim_id]))
					$sele='checked';
				if($count>0)
					$trim_data.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='cktrim' class='cktrim' value='$tri->trim_id'/> $tri->trim_name($count)</label></li>";# code...
				
			}
		$trim_other=$this->vinnm->getnumsearch_other('trim',$search,$make_id,$model_id,$year,$in_color_id,$ex_color_id,$year_from,$year_to,$body_id,$engine_id,$fuel_id,$trans_id,'',$date_from,$date_to);
		$trim_other_check='';
		if (isset($arroftrim['other'])) {
			$trim_other_check='checked';
		}
		if ($trim_other>0) {
		$trim_data.="<li><label><input type='checkbox' onclick='getmodel(event,0);' $trim_other_check name='cktrim' class='cktrim' value='other'> OTHER($trim_other)</label></li>";
			
		}
		$dadata='';
		$dadata.="
		<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_date();' class='clear_date_btn'>Clear</li>
		<div align='center' style='padding-bottom:10px;margin-bottom:5px;width:175px;'>
			
			<input class='da_ft' id='date_from' type='text' size='18px' placeholder='Date From' value='$date_from'>
			<br><label>To</label><br>
			<input class='da_ft' id='date_to' type='text' size='18px' placeholder='Date To' value='$date_to'><br>
			<button onclick='da_ft();' class='btn btn-primary' style='float:right;width:90px;'>Go</button>
		</div>";

		$arr['dateft']=$dadata;
		$arr['trim']=$trim_data;
		$arr['trans']=$trans_data;
		$arr['fuel']=$fuel_data;
		$arr['engine']=$engine_data;
		$arr['body']=$body_data;
		$arr['make']=$make_data;
		$arr['model']=$modeldata;
		$arr['year']=$yeardata;
		$arr['in_color']=$in_colordata;
		$arr['ex_color']=$ex_colordata;
		header("Content-type:text/x-json");
		echo json_encode($arr);

	}

	function test_search(){
		$make_id = $this->input->post('m');
		$model_id = $this->input->post('mo');
		$year = $this->input->post('y');
		$in_c = $this->input->post('in_c');
		$ex_c = $this->input->post('ex_c');

		$data['data'] = "";
		
	}
	function add_vin_part($vinid){
		$temp = $this->vinnm->getpartvin($vinid);
		$data['vin_name'] = $this->vinnm->get_info($vinid)->vinnumber_name;
		$data['part'] = $temp;
		$data['vin_id'] = $vinid;
		$this->load->view('vinnumbers/add_vin_part',$data);
		
	}
 	function submit_part($vinid){
 			$part_number = $this->input->post('part_number');
 			$khmer_name = $this->input->post('khmer_name');
 			$grade = $this->input->post('part_grade');

 			// var_dump($part_number);
 			// var_dump($khmer_name);
 			// var_dump($grade);die();
 			
 			if ($part_number) {
 				$this->db->where('vin_id',$vinid)->delete('vinnumber_part');
 				for ($i=0; $i < count($part_number); $i++) { 
	 				$this->db->insert('vinnumber_part',array('vin_id'=>$vinid,
	 														'khmername_id'=>$khmer_name[$i],
	 														'grade'=>$grade[$i],
	 														'part_number'=>$part_number[$i]));
 				}
 			}
 			
 			$data['vin_id'] = $vinid;
 			// echo json_encode($data['vin_id']);

 			redirect(site_url('vinnumbers/add_vin_part/'.$vinid));
 	}
 	function gemodel_pos(){
		$make_id=$this->input->post('make');
		$model_id=$this->input->post('model');
		$year=$this->input->post('year');
		$in_color_id=$this->input->post('in_color');
		$ex_color_id=$this->input->post('ex_color');
		$year_from =$this->input->post('year_from');
		$year_to = $this->input->post('year_to');
		$body_id = $this->input->post('body');
		$search = $this->input->post('search');
		$engine_id = $this->input->post('engine');
		$fuel_id = $this->input->post('fuel');
		$trans_id = $this->input->post('trans');
		$trim_id = $this->input->post('trim');
		$vinnumber =$this->input->post('vin');
		$s_all = $this->input->post('search_all');
		$where='';
		$arr=array();
		// $sql=$this->session->userdata('query_result');
		// echo $sql;die();
		// $this->db->query("DROP TEMPORARY TABLE IF EXISTS tmp_result");
  //       $this->db->query("CREATE TEMPORARY TABLE tmp_result $sql");

		$vin_check='';
		$vin_data='';
		$vin_data.="<li class='sf_filter' style='margin-left:-15px;'>
						<input type='text' name='v' id='vin_select' onchange=search(1,event,0,'head','$vinnumber_id') value='$vinnumber'>
					</li>";


		$where_makes='';
		$where_models='';
		$where_vins='';
		$where_years='';
		$where_yearfs='';
		$where_cols='';
		$where_bodys='';
		$where_engines='';
		$where_fuels='';
		$where_transs='';
		$where_trims='';
		$where_place='';
		$where_location='';
		$where_s_all = "";
		$where_s = "";
		$where_incols = '';
		$where_excols = '';
		

		if($search!='') {
			$where_s .= "AND (i.vinnumber_name LIKE '%$search%'
												OR i.vainnumber LIKE '%$search%'
												OR i.year LIKE '%$search%'
												OR i.make_name LIKE '%$search%' 
												OR i.model_name LIKE '%$search%' 
												OR i.in_color_name LIKE '%$search%'
												OR i.ex_color_name LIKE '%$search%'
											)";
		}
		if ($s_all) {
			$ars = explode(' ', $s_all);
			for ($i=0; $i < count($ars); $i++) { 
				$where_s_all.= " AND( `year` LIKE '%$ars[$i]%'
									OR vinnumber_id LIKE '%$ars[$i]%'
									OR vainnumber LIKE '%$ars[$i]%'
									OR make_name LIKE '%$ars[$i]%'
									OR model_name LIKE '%$ars[$i]%'
									OR in_color_name LIKE '%$ars[$i]%'
									OR ex_color_name LIKE '%$ars[$i]%')";
			}
		}
		if($make_id!=''){
            $where_makes.=" AND i.make_id='".$make_id."'";
        }elseif ($make_id=='other') {
        	$where_makes.=" AND i.make_id='' OR i.make_id=0 OR i.make_id IS NULL";

        }
        if($model_id!=''){
            $where_models.=" AND i.model_id='".$model_id."'";

        }elseif ($model_id=='other') {
        	$where_models.=" AND i.model_id='' OR i.model_id=0 OR i.model_id IS NULL";
        }
        // if ($vinnumber!='') {
        // 	$where_vins.=" AND i.category='".$vinnumber."'";

        // }
        // elseif($vin=='other'){
        // 	$where_vins.=" AND i.category='' OR i.category=0 OR i.category IS NULL";

        // }
        if($vinnumber!=''){
			$vin=$vinnumber;
			if ($vin!='other') {
				$re.="$vin";
				$where_vins.=" AND i.vinnumber_id='".$_POST['vin']."'";
			}
		}
		
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
            	if ($arryear[$i]!='' && $arryear[$i]!='_') {
            		
            		if ($arryear[$i]=='other') {
            			$where_year.=" i.year='' OR i.year=0 OR i.year IS NULL";
            		}else{
                    	$where_year.="i.year='".$arryear[$i]."'";
            		}
                    if($i<count($arryear)-2)
                        $where_year.=' OR ';
                }
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where_years.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where_yearfs.=" AND i.year BETWEEN $yf AND $yt";
        }
        
        if($body_id!='' && $body_id!='_'){
            $bo_arr=explode('_', $body_id);
            $where_body=' AND(';
            for ($i=0; $i < count($bo_arr) ; $i++) { 
            	if ($bo_arr[$i]!='' && $bo_arr[$i]!='_') {
            		# code...
            		if ($bo_arr[$i]=='other') {
            			$where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
            		}else{
                    $where_body.="i.body='".$bo_arr[$i]."'";

            		}
                    if($i<count($bo_arr)-2)
                        $where_body.=' OR ';
            	}
                
            }
            $where_body.=')';
            // $s_year=$_GET['y'];
            $where_bodys.=$where_body;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($engine_id!='' && $engine_id!='_'){
            $en_arr=explode('_', $engine_id);
            $where_engine=' AND(';
            for ($i=0; $i < count($en_arr) ; $i++) { 
            	if ($en_arr[$i]!='' && $en_arr!='_') {
            		# code...
            		if ($en_arr[$i]=='other') {
            			$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
            		}else{
                    $where_engine.="i.engine='".$en_arr[$i]."'";

            		}
                    if($i<count($en_arr)-2)
                        $where_engine.=' OR ';
            	}
                
            }
            $where_engine.=')';
            // $s_year=$_GET['y'];
            $where_engines.=$where_engine;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($fuel_id!='' && $fuel_id!='_'){
            $fuel_arr=explode('_', $fuel_id);
            $where_fuel=' AND(';
            for ($i=0; $i < count($fuel_arr) ; $i++) { 
            	if ($fuel_arr[$i]!='' && $fuel_arr[$i]!='_') {
            		# code...
           			if ($fuel_arr[$i]=='other') {
           				$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
           			}else{
                    $where_fuel.="i.fuel='".$fuel_arr[$i]."'";

           			}
                    if($i<count($fuel_arr)-2)
                        $where_fuel.=' OR ';
            	}
                
            }
            $where_fuel.=')';
            // $s_year=$_GET['y'];
            $where_fuels.=$where_fuel;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trans_id!='' && $trans_id!='_'){
            $trans_arr=explode('_', $trans_id);
            $where_trans=' AND(';
            for ($i=0; $i < count($trans_arr) ; $i++) { 
            	if ($trans_arr[$i]!='' && $trans_arr[$i]!='_') {
            		# code...
           			if ($trans_arr[$i]=='other') {
           				$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";
           			}else{
                    $where_trans.="i.transmission='".$trans_arr[$i]."'";

           			}
                    if($i<count($trans_arr)-2)
                        $where_trans.=' OR ';
            	}
                
            }
            $where_trans.=')';
            // $s_year=$_GET['y'];
            $where_transs.=$where_trans;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trim_id!='' && $trim_id!='_'){
            $trim_arr=explode('_', $trim_id);
            $where_trim=' AND(';
            for ($i=0; $i < count($trim_arr) ; $i++) {
            	if ($trim_arr[$i]!='' && $trim_arr[$i]!='_') {
            	 	# code...
           			if ($trim_arr[$i]=='other') {
           				$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
           			}else{

                    $where_trim.="i.trim='".$trim_arr[$i]."'";
           			}
                    if($i<count($trim_arr)-2)
                        $where_trim.=' OR ';
            	 } 
                
            }
            $where_trim.=')';
            // $s_year=$_GET['y'];
            $where_trims.=$where_trim;
            // $where.=" AND i.color_id='".$color_id."'";
        }
     	$where_inco = '';
		if($in_color_id!='' && $in_color_id!='_'){
            $incol_arr=explode('_', $in_color_id);
            $where_inco=' AND(';
            for ($i=0; $i < count($incol_arr) ; $i++) {
            	if ($incol_arr[$i]!='' && $incol_arr[$i]!='_') {
            	 	# code...
           			if ($incol_arr[$i]=='other') {
           				$where_inco.="i.interior_color='' OR i.interior_color=0 OR i.interior_color IS NULL";
           			}else{

                    $where_inco.="i.interior_color='".$incol_arr[$i]."'";
           			}
                    if($i<count($incol_arr)-2)
                        $where_inco.=' OR ';
            	 } 
                
            }
            $where_inco.=')';
            // $s_year=$_GET['y'];
            $where_incols.=$where_inco;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        $where_exco = '';
		if($ex_color_id!='' && $ex_color_id!='_'){
            $excol_arr=explode('_', $ex_color_id);
            $where_exco=' AND(';
            for ($i=0; $i < count($excol_arr) ; $i++) {
            	if ($excol_arr[$i]!='' && $excol_arr[$i]!='_') {
            	 	# code...
           			if ($excol_arr[$i]=='other') {
           				$where_exco.="i.external_color='' OR i.external_color=0 OR i.external_color IS NULL";
           			}else{

                    $where_exco.="i.external_color='".$excol_arr[$i]."'";
           			}
                    if($i<count($excol_arr)-2)
                        $where_exco.=' OR ';
            	 } 
                
            }
            $where_exco.=')';
            // $s_year=$_GET['y'];
            $where_excols.=$where_exco;
            // $where.=" AND i.color_id='".$color_id."'";
        }
		


		$make_check='';
		$make_data='';
		if ($make_id=='') $make_check='checked';
		$make_data.="<li class='sf_filter'><input type='text' class='sf_input' f='make' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$make_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_make();' id='clear_make_btn'>Clear</li>";
		$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','')  name='ckmake' class='ckmake' $make_check value=''> All</label></li>";
		$make=$this->db->query("SELECT make_name,make_id FROM ospos_makes WHERE deleted=0 ORDER BY make_name ASC")->result();
		$count=$this->vinnm->getnumsearch('make_id',$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		foreach ($make as $m) {
			$sele='';
			if($make_id==$m->make_id)
				$sele='checked';
			if($count[$m->make_id]>0)
				$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make',$m->make_id) name='ckmake' $sele class='ckmake' value='$m->make_id'/> $m->make_name (".$count[$m->make_id].")</label></li>";
		}		
		$make_other=$this->vinnm->getnumsearch_other('make_id',$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

		$make_other_check='';
		if ($make_id=='other') {
			$make_other_check='checked';
		}
		if ($make_other>0) {
			$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','other')  name='ckmake' class='ckmake' $make_other_check value='other'> OTHER($make_other)</label></li>";
			
		}

		$model_check='';
		$modeldata='';
		if($model_id=='') $model_check='checked';
		$modeldata.="<li class='sf_filter'><input type='text' class='sf_input' f='model' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$modeldata .="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_model();' class='clear_model_btn'>Clear</li>";
		$modeldata.="<li><label><input type='radio' onclick=search(1,event,0,'model','');  name='ckmodel' class='ckmodel' $model_check value=''> All</label></li>";
		$wheremodel='';
		if($make_id!='')
			$wheremodel=" AND make_id='$make_id'";
		$data=$this->db->query("SELECT model_id,model_name FROM ospos_models WHERE deleted='0' {$wheremodel} ORDER BY model_name")->result();
		$count=$this->vinnm->getnumsearch('model_id',$where_makes.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

		foreach ($data as $mo) {
			$sele='';
			if($model_id==$mo->model_id)
					$sele='checked';
			if($count[$mo->model_id]>0)
				$modeldata.="<li rel='$mo->make_id'><label><input $sele onclick=search(1,event,1,'model',$mo->model_id) type='radio' name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name(".$count[$mo->model_id].")</label></li>";

		}
		$model_other=$this->vinnm->getnumsearch_other('model_id',$where_makes.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

		$model_other_check='';
		if ($model_id=='other') {
			$model_other_check='checked';
		}
		if ($model_other>0) {
			$model_data.="<li><label><input type='radio' onclick=search(1,event,1,'model','other')  name='ckmake' class='ckmake' $model_other_check value='other'> OTHER($model_other)</label></li>";
			
		}
		
		$yeararr=explode('_', $year);
		$arrofyear=array();
		for ($i=0; $i <count($yeararr) ; $i++) { 
			$arrofyear[$yeararr[$i]]=$yeararr[$i];
		}

		$year_check='';
		if($year=='' || $year=='_'){
			$year_check='checked';
		}
		$yeardata='';

		
		if ($year_check!='') {
			$year_from=$year_from;
			$year_to = $year_to;
		}else{
			$year_from='';
			$year_to='';
		}
		$yeardata.="
		<div align='center' style='border-bottom:1px solid #21759B;padding-bottom:10px;margin-bottom:5px;'>

			<input style='width:40px;' maxlength='4' class='year_ft' id='year_from' type='text' placeholder='year' onkeyup='chkYear();' value='$year_from'>
			To
			<input style='width:40px;' maxlength='4' class='year_ft' id='year_to' type='text' placeholder='year' value='$year_to'>
			<button onclick='yearft();' class='btn btn-primary'>Go</button>
		</div>";
		$yeardata.="<li class='sf_filter'><input type='text' class='sf_input' f='year' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$yeardata.="<li style='float:right;cursor:pointer;color:#21759B' id='clear_year_btn' onclick='clear_year();'>Clear</li>";
		$yeardata.="<li><label><input type='checkbox' onclick=search(1,event,0,'year','') $year_check name='ckyear' class='ckyear' value=''> All</label></li>";
		$count=$this->vinnm->getnumsearch('year',$where_makes.$where_models.$where_vins.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		// var_dump($count);
		for($j=date('Y');$j>=1980;$j--){
				$sele='';

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count[$j]>0)
					$yeardata.= "<li><label><input onclick=search(1,event,0,'year','$j') type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j (".$count[$j].")</label></li>";# code...

		}
		$year_other=$this->vinnm->getnumsearch_other('year',$where_makes.$where_models.$where_vins.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

		$year_other_check='';
		if (isset($arrofyear['other'])) {
			$year_other_check='checked';
			
		}
		if ($year_other>0) {
			$yeardata.="<li><label><input type='checkbox' onclick=search(1,event,0,'year','other') $year_other_check name='ckyear' class='ckyear' value='other'> OTHER($year_other)</label></li>";
			
		}

		
		// BODY
		$arrbody=explode('_', $body_id);
        $arrofbody=array();
        for ($i=0; $i < count($arrbody) ; $i++) { 
			$arrofbody[$arrbody[$i]]=$arrbody[$i];     
        }
		$body_check='';
		if($body_id=='' || $body_id=='_') {
			$body_check='checked';
		}
		$body_data='';
		$body_data.="<li class='sf_filter'><input type='text' class='sf_input' f='body' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$body_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_body();' class='clear_body_btn'>Clear</li>";
		$body_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'body','') $body_check name='ckbody' class='ckbody' value=''> All</label></li>";
		$body=$this->db->query("SELECT body_id,body_name FROM ospos_body b
								WHERE b.deleted=0 ORDER BY body_name ASC")->result();
		$count=$this->vinnm->getnumsearch('body',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

			foreach ($body as $bo) {
				$sele='';
				if(isset($arrofbody[$bo->body_id]))
					$sele='checked';
				if($count[$bo->body_id]>0)
					$body_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'body','$bo->body_id') $sele name='ckbody' class='ckbody' value='$bo->body_id'/> $bo->body_name(".$count[$bo->body_id].")</label></li>";# code...
				
			}
		$body_other=$this->vinnm->getnumsearch_other('body',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		// var_dump($body_other);
		$body_other_check='';
		if (isset($arrofbody['other'])) {
			$body_other_check='checked';
		}
		// echo $body_other_check;
		// echo $body_other;
		if ($body_other>0) {
		$body_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'body','other') $body_other_check name='ckbody' class='ckbody' value='other'> OTHER($body_other)</label></li>";
			
		}
		// ENGINE
        $arrengine=explode('_', $engine_id);
        $arrofengine=array();
        for ($i=0; $i < count($arrengine) ; $i++) { 
			$arrofengine[$arrengine[$i]]=$arrengine[$i];     
        }
		$engine_check='';
		if($engine_id=='' || $engine_id=='_') {
			$engine_check='checked';
		}
		$engine_data='';
		$engine_data.="<li class='sf_filter'><input type='text' class='sf_input' f='engine' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$engine_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_engine();' class='clear_engine_btn'>Clear</li>";
		$engine_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'engine','') $engine_check name='ckengine' class='ckengine' value=''> All</label></li>";
		$engine=$this->db->query("SELECT engine_id,engine_name FROM ospos_engine e
								WHERE e.deleted=0 ORDER BY engine_name ASC")->result();
		$count=$this->vinnm->getnumsearch('engine',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

			foreach ($engine as $en) {
				$sele='';
				if(isset($arrofengine[$en->engine_id]))
					$sele='checked';
				if($count[$en->engine_id]>0)
					$engine_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'engine','$en->engine_id') $sele name='ckengine' class='ckengine' value='$en->engine_id'/> $en->engine_name(".$count[$en->engine_id].")</label></li>";# code...
				
			}
		$engine_other=$this->vinnm->getnumsearch_other('engine',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		$engine_other_check='';
		if (isset($arrofengine['other'])) {
			$engine_other_check='checked';
		}
		if ($engine_other>0) {
		$engine_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'engine','other') $engine_other_check name='ckengine' class='ckengine' value='other'> OTHER($engine_other)</label></li>";
			
		}

		// FUEL

        $arrfuel=explode('_', $fuel_id);
        $arroffuel=array();
        for ($i=0; $i < count($arrfuel) ; $i++) { 
			$arroffuel[$arrfuel[$i]]=$arrfuel[$i];     
        }
		$fuel_check='';
		if($fuel_id=='' || $fuel_id=='_') {
			$fuel_check='checked';
		}
		$fuel_data='';
		$fuel_data.="<li class='sf_filter'><input type='text' class='sf_input' f='fuel' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$fuel_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_fuel();' class='clear_fuel_btn'>Clear</li>";
		$fuel_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','') $fuel_check name='ckfuel' class='ckfuel' value=''> All</label></li>";
		$fuel=$this->db->query("SELECT fuel_id,fule_name FROM ospos_fuel f
								WHERE f.deleted=0 ORDER BY fule_name ASC")->result();
		$count=$this->vinnm->getnumsearch('fuel',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

			foreach ($fuel as $fu) {
				$sele='';
				if(isset($arroffuel[$fu->fuel_id]))
					$sele='checked';
				if($count[$fu->fuel_id]>0)
					$fuel_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','$fu->fuel_id') $sele name='ckfuel' class='ckfuel' value='$fu->fuel_id'/> $fu->fule_name(".$count[$fu->fuel_id].")</label></li>";# code...
				
			}
		$fuel_other=$this->vinnm->getnumsearch_other('fuel',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		$fuel_other_check='';
		if (isset($arroffuel['other'])) {
			$fuel_other_check='checked';
		}
		if ($fuel_other>0) {
		$fuel_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','other') $fuel_other_check name='ckfuel' class='ckfuel' value='other'> OTHER($fuel_other)</label></li>";
			
		}
			// TRANSMISSION
	
        $arrtrans=explode('_', $trans_id);
        $arroftrans=array();
        for ($i=0; $i < count($arrtrans) ; $i++) { 
			$arroftrans[$arrtrans[$i]]=$arrtrans[$i];     
        }
	
		$trans_check='';
		if($trans_id=='' || $trans_id=='_') {
			$trans_check='checked';
		}
		$trans_data='';
		$trans_data.="<li class='sf_filter'><input type='text' class='sf_input' f='trans' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$trans_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_trans();' class='clear_trans_btn'>Clear</li>";
		$trans_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trans','') $trans_check name='cktrans' class='cktrans' value=''> All</label></li>";
		$trans=$this->db->query("SELECT trans_id,transmission_name FROM ospos_transmission t
								WHERE t.deleted=0 ORDER BY transmission_name ASC")->result();
		$count=$this->vinnm->getnumsearch('transmission',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);

			foreach ($trans as $tr) {
				$sele='';
				if(isset($arroftrans[$tr->trans_id]))
					$sele='checked';
				if($count[$tr->trans_id]>0)
					$trans_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'trans','$tr->trans_id') $sele name='cktrans' class='cktrans' value='$tr->trans_id'/> $tr->transmission_name(".$count[$tr->trans_id].")</label></li>";# code...
				
			}
		$trans_other=$this->vinnm->getnumsearch_other('transmission',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_trims.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		$trans_other_check='';
		if (isset($arroftrans['other'])) {
			$trans_other_check='checked';
		}
		if ($trans_other>0) {
		$trans_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trans','other') $trans_other_check name='cktrans' class='cktrans' value='other'> OTHER($trans_other)</label></li>";
			
		}

				// TRIM
        $arrtrim=explode('_', $trim_id);
        $arroftrim=array();
        for ($i=0; $i < count($arrtrim) ; $i++) { 
			$arroftrim[$arrtrim[$i]]=$arrtrim[$i];     
        }
	   
		$trim_check='';
		if($trim_id=='' || $trim_id=='_') {
			$trim_check='checked';
		}
		$trim_data='';
		$trim_data.="<li class='sf_filter'><input type='text' class='sf_input' f='trim' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$trim_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_trim();' class='clear_trim_btn'>Clear</li>";
		$trim_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trim','') $trim_check name='cktrim' class='cktrim' value=''> All</label></li>";
		$trim=$this->db->query("SELECT trim_id,trim_name FROM ospos_trim t
								WHERE t.deleted=0 ORDER BY trim_name ASC")->result();
		$count=$this->vinnm->getnumsearch('trim',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);


			foreach ($trim as $tri) {
				$sele='';
				if(isset($arroftrim[$tri->trim_id]))
					$sele='checked';
				if($count[$tri->trim_id]>0)
					$trim_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'trim','$tri->trim_id') $sele name='cktrim' class='cktrim' value='$tri->trim_id'/> $tri->trim_name(".$count[$tri->trim_id].")</label></li>";# code...
				
			}
		$trim_other=$this->vinnm->getnumsearch_other('trim',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_place.$where_location.$where_s_all.$where_s.$where_incols.$where_excols);
		$trim_other_check='';
		if (isset($arroftrim['other'])) {
			$trim_other_check='checked';
		}
		if ($trim_other>0) {
		$trim_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trim','other') $trim_other_check name='cktrim' class='cktrim' value='other'> OTHER($trim_other)</label></li>";
			
		}


		// IN COLOR
		$arrincolor=explode('_', $in_color_id);
        $arrofincolor=array();
        for ($i=0; $i < count($arrincolor) ; $i++) { 
			$arrofincolor[$arrincolor[$i]]=$arrincolor[$i];     
        }
		$incol_check='';
		if($in_color_id=='' || $in_color_id=='_') {
			$incol_check='checked';
		}
		$incol_data='';
		$incol_data.="<li class='sf_filter'><input type='text' class='sf_input' f='in_color' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$incol_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_in();' class='clear_in_btn'>Clear</li>";
		$incol_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'in_co','') $incol_check name='ck-incolor' class='ck-incolor' value=''> All</label></li>";
		$in_color=$this->db->query("SELECT * FROM ospos_colors c WHERE deleted=0 ORDER BY c.color_name ASC")->result();
		$count=$this->vinnm->getnumsearch('interior_color',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_engines.$where_fuels.$where_transs.$where_trims.$where_s_all.$where_s.$where_excols);

			foreach ($in_color as $inco) {
				$sele='';
				if(isset($arrofincolor[$inco->color_id]))
					$sele='checked';
				if($count[$inco->color_id]>0)
					$incol_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'in_co','$inco->color_id') $sele name='ck-incolor' class='ck-incolor' value='$inco->color_id'/> $inco->color_name(".$count[$inco->color_id].")</label></li>";# code...
				
			}
		$inco_other=$this->vinnm->getnumsearch_other('interior_color',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_engines.$where_fuels.$where_transs.$where_trims.$where_s_all.$where_s.$where_excols);
		// var_dump($body_other);
		$inco_other_check='';
		if (isset($arrofincolor['other'])) {
			$inco_other_check='checked';
		}
		// echo $body_other_check;
		// echo $body_other;
		if ($inco_other>0) {
		$incol_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'in_co','other') $inco_other_check name='ck-incolor' class='ck-incolor' value='other'> OTHER($inco_other)</label></li>";
			
		}
		// EX COLOR
		$arrexcolor=explode('_', $ex_color_id);
        $arrofexcolor=array();
        for ($i=0; $i < count($arrexcolor) ; $i++) { 
			$arrofexcolor[$arrexcolor[$i]]=$arrexcolor[$i];     
        }
		$excol_check='';
		if($ex_color_id=='' || $ex_color_id=='_') {
			$excol_check='checked';
		}
		$excol_data='';
		$excol_data.="<li class='sf_filter'><input type='text' class='sf_input' f='ex_color' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$excol_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_ex();' class='clear_ex_btn'>Clear</li>";
		$excol_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'ex_co','') $excol_check name='ck-excolor' class='ck-excolor' value=''> All</label></li>";
		$ex_color=$this->db->query("SELECT * FROM ospos_colors c WHERE deleted=0 ORDER BY c.color_name ASC")->result();
		$count=$this->vinnm->getnumsearch('external_color',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_engines.$where_fuels.$where_transs.$where_trims.$where_s_all.$where_s.$where_incols);

			foreach ($ex_color as $exco) {
				$sele='';
				if(isset($arrofexcolor[$exco->color_id]))
					$sele='checked';
				if($count[$exco->color_id]>0)
					$excol_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'in_co','$exco->color_id') $sele name='ck-excolor' class='ck-excolor' value='$exco->color_id'/> $exco->color_name(".$count[$exco->color_id].")</label></li>";# code...
				
			}
		$exco_other=$this->vinnm->getnumsearch_other('external_color',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_engines.$where_fuels.$where_transs.$where_trims.$where_s_all.$where_s.$where_incols);
		// var_dump($body_other);
		$exco_other_check='';
		if (isset($arrofexcolor['other'])) {
			$exco_other_check='checked';
		}
		// echo $body_other_check;
		// echo $body_other;
		if ($exco_other>0) {
		$excol_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'in_co','other') $exco_other_check name='ck-excolor' class='ck-excolor' value='other'> OTHER($exco_other)</label></li>";
			
		}

		$dadata='';
		$dadata.="
		<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_date();' class='clear_date_btn'>Clear</li>
		<div align='center' style='padding-bottom:10px;margin-bottom:5px;width:175px;'>
			
			<input class='da_ft' id='date_from' type='text' size='18px' placeholder='Date From' value='$date_from'>
			<br><label>To</label><br>
			<input class='da_ft' id='date_to' type='text' size='18px' placeholder='Date To' value='$date_to'><br>
			<button onclick='da_ft();' class='btn btn-primary' style='float:right;width:90px;'>Go</button>
		</div>";
		$arr['dateft']=$dadata;
		$arr['vin']=$vin_data;
		$arr['trim']=$trim_data;
		$arr['trans']=$trans_data;
		$arr['fuel']=$fuel_data;
		$arr['engine']=$engine_data;
		$arr['body']=$body_data;
		$arr['make']=$make_data;
		$arr['model']=$modeldata;
		$arr['year']=$yeardata;
		$arr['s_all'] = $s_all;
		$arr['in_color']=$incol_data;
		$arr['ex_color']=$excol_data;
		// $arr['ex_color']=$ex_colordata;
		header("Content-type:text/x-json");
		echo json_encode($arr);

	}
 
}
?>