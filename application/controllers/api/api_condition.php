<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_condition extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->conditions();
		$result = $result->result_array();
		$arr = [];
		foreach ($result as  $value) {
			$data['condition_id']   = $value['condition_id'];
			$data['condition_name'] = $value['condition_name'];
			array_push($arr, $data);
		}
		echo json_encode($arr);
	}
	
}
?>