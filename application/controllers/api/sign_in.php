<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class sign_in extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
	}
	
	function index()
	{		
		$password = $this->input->get("password");	
		$username = $this->input->get("username");	
		if(!$this->Employee->login($username,$password))
			$msg = array('status' => 0, 'message' => 'Login Invalid Username and Password');
		else
			$msg = array('status' => 1, 'message' => 'Login Success');
		
		echo (json_encode($msg));
	}	
}
?>