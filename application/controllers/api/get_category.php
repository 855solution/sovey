<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class get_category extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_category');
	}
	
	function index()
	{
	
		$data = $this->Api_category->get_category();
		$result = $data->result_array();
		$arr = [];
		$new_arr = [];
		foreach ($result as $value) {
			$arr['category_id'] = $value['category_id'];
			$arr['category_name'] = $value['category_name'];
			array_push($new_arr, $arr);
		}
		if(count($result) > 0)
			echo json_encode(array("status" => 1, "data" => $new_arr));
		else
			echo json_encode(array("status" => 0, "No data"));
	}		
	

	
}
?>