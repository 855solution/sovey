<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class add_item extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');
	}
	
	function index()
	{
		$data = array("year" => $this->Api_add_item->year(), "khmer_name" => $this->Api_add_item->khmernames());
		//print_r($this->Api_add_item->khmernames());
		echo (json_encode($data));	
			
	}

	function get_part_number()
	{
		$part_number = $this->input->post('part_number');
		$data =  $this->Api_add_item->part_number($part_number);
		
		if (count($data) > 0)
			echo json_encode(array("status" => 1, "data" => $data));
		else
			echo json_encode(array("status" => 0, "message" => "No Data"));
				
			
	}


	function update_part()
	{
		$part_number = $this->input->post('part_number');
		$part_name = $this->input->post('part_name');
		$khmername_id = $this->input->post('khmername_id');
		$category_id = $this->input->post('category_id');
		$fitment = $this->input->post('fitment');
		$make_id = $this->input->post('make_id');
		$model_id = $this->input->post('model_id');
		$year = $this->input->post('year');
		$color_id = $this->input->post('color_id');
		$condition_id = $this->input->post('condition_id');
		$part_placement = $this->input->post('part_placement');
		$brand_id = $this->input->post('brand_id');
		$location_id = $this->input->post('location_id');
		$cost_price = $this->input->post('cost_price');
		$unit_price = $this->input->post('unit_price');
		$quantity = $this->input->post('quantity');
		$data =  $this->Api_add_item->update_part($part_number,$part_name,$khmername_id,$category_id,$fitment,$make_id,$model_id,$year,$color_id,$condition_id,$part_placement,$brand_id,$location_id,$cost_price,$unit_price,$quantity);
		if($update)
			echo json_encode(array('status' => 1, 'message' => 'success'));
		else
			echo json_encode(array('status' => 0, 'message' => 'failed'));	
			
	}
	
}
?>