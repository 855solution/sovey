<?php

class get_location extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
	}
	
	function list_location()
	{

	 	$locations = $this->Location->get_all(10000, 0);
	 	$locations = $locations->result_array();
	 	$arr       = [];
	 	foreach ($locations as $location) {
	 		$new_array['location_id']   = $location['location_id'];
	 		$new_array['location_name'] = $location['location_name'];
	 		array_push($arr, $new_array);
	 	}
	 	header('Content-Type: application/json');
    	echo json_encode($arr);
	}	
}
?>