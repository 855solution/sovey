<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_branch extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->branchs();
		$result = $result->result_array();
		$arr = [];
		foreach ($result as  $value) {
			$data['branch_id']   = $value['branch_id'];
			$data['branch_name'] = $value['branch_name'];
			array_push($arr, $data);
		}
		echo json_encode($arr);
	}
	
}
?>