<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class item_location extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_location');


	}
	
	function index()
	{
	    //$json_send='{"item_number_list":"[{\"item_number\":\"1\"}, {\"item_number\":\"1\"}, {\"item_number\":\"1\"}]","location_name":"3-1-F-10-4"}';
	    	$json_send=$this->input->post("json_send");
		$result = $this->Api_location->api_item_location($json_send);

	}

	function location()
	{
	
		$result = $this->Api_location->list_location();
		return $this->load->view('list_location', array('data' => $result));

	}
	
}
?>