<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class item_entry extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');
	}
	
	function index()
	{
	
		//$json_send = '{"image_list":"[{\"image\":\"file_name.jpg\"},{\"image\":\"file_name.jpg\"}]","description":"text","category_id":1,"cost_price": "0.3","unit_price":"0.2","quantity":"0.1","fit":"feat", "year":1,"location_id":1,"model_id":1,"branch_id":1,"color_id":1,"condition_id":1,"partplacement_id": 1,"make_id": 1,"khmername_id":1, "is_new":0, "is_feat":0, "is_pro":0, "barcode":"0321", "allow_alt_description":0,"is_serialized":0,"reorder_level":0}';
		$json_send = $this->input->post('json_send');
		//$json_send = '{"condition_id":"34","cost_price":"0","is_pro":"1","is_feat":"1","image_list":"[{\"image\":\"\\\/storage\\\/sdcard\\\/Pictures\\\/JPEG_20160209_154014_-1917434202.jpg\"}, {\"image\":\"\\\/storage\\\/sdcard\\\/Pictures\\\/JPEG_20160209_154029_-1051379428.jpg\"}]","khmername_id":"353","category_id":"32","unit_price":"0","allow_alt_description":"1","fit":"LE","is_new":"1","reorder_level":"1","partplacement_id":"38","color_id":"27","description":"pp","model_id":"27","make_id":"26","quantity":"0","year":"2012","branch_id":"31","is_serialized":"1","location_id":1, "make_name":"Tuntra", "model_name": "Toyota"}';
		$insert = $this->Api_add_item->add_item($json_send);
		if($insert){
			echo json_encode(array("status" => 1, "message" => "Insert successfully"));
		}else
			echo json_encode(array("status" => 0, "message" => "Failed"));
			
	}
	function post()
	{
		return $this->load->view("post");
		// $json_send = '{"image_list":"[{\"image\":\"file_name.jpg\"},{\"image\":\"file_name.jpg\"}]","description":"text","category_id":1,"cost_price": "0.3","unit_price":"0.2","quantity":"0.1","fit":1, "year":1,"location_id":1,"model_id":1,"branch_id":1,"color_id":1,"condition_id":1,"partplacement_id": 1,"make_id": 1,"khmername_id":1}';
		// //$json_send = $this->input->post('json_send');
		// $insert = $this->Api_add_item->add_item($json_send);
		// if($insert)
		// 	echo json_encode(array("status" => 1, "message" => "Insert successfully"));
		// else
		// 	echo json_encode(array("status" => 0, "message" => "Failed"));
			
	}

	
}
?>