<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class counting_item extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_countingitem');
	}

	function index()
	{
	
		$location_name = $this->input->post("location_name");
		//$location_name = "1-1-A-3-2-B";
		$result = $this->Api_countingitem->get_item_by_location($location_name);
		$result = $result->result_array();
		$arr = [];
		//var_dump(count($result));
		if(count($result) > 0) {
			foreach ($result as  $value) {
				$value['imagename'] = "http://soveyautoparts.com/uploads/".$value['imagename'];
				array_push($arr, $value);
			}
			echo json_encode(array("status" => 1, "data" => $arr));
		}else
			echo json_encode(array("status" => 0, "message" => "No Data"));
			
	}	
	
}
?>