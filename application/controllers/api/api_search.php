<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_search extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_search_data');
	}

	function index()
	{
		
		/*
	        $data = $this->input->post('json_send');
	        $json = json_decode($data);
	        $arr = [];
	        foreach($json->arr_color as $data){
	        	array_push($arr, $data->color);
	        }
	        var_dump($arr);
		*/
		$data = $this->input->post('json_send');
		
		$result = $this->Api_search_data->search($data);
		$result = $result->result_array();
		$count = $this->Api_search_data->count($data);
		if(count($result) > 0) {
			echo json_encode(array("status" => 1, "message" => "success", "result" => $result, "total"=>$count));
		}else
			echo json_encode(array("status" => 0, "message" => "No Data", "result" => []));
			
			
	}
	function searchText(){
		$data = $this->input->post('json_send');
		$result = $this->Api_search_data->search_text($data);
		$result = $result->result_array();
		
		if(count($result) > 0) {
			echo json_encode(array("status" => 1, "message" => "success", "result" => $result));
		}else
			echo json_encode(array("status" => 0, "message" => "No Data", "result" => []));
	}	
	
}
?>