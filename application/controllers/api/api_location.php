<?php
class Api_location extends CI_Model
{

	public function api_item_location($location_name, $item_number)
	{
		$api = $this->load->database('api', TRUE);
		$api->select('*');
		$api->from('locations');
		$api->where('location_name', $location_name);
		$query = $api->get();
		$result = $query->row();

		$api->select('*');
		$api->from('items');
		$api->where('item_number', $item_number);
		$api->where('location_id', $result->location_id);
		$query = $api->get();
		$qty_location_item = $query->num_rows();
		if($qty_location_item > $result->max_qty)
		{
			return false;
		}else{
			return $this->api_add_item_location($result->location_id, $item_number);
		}	 
	}
	public function api_add_item_location($location_id, $item_numbers)
	{
		$api = $this->load->database('api', TRUE);
		$data = array(
			   'item_number' => $item_numbers ,
			   'location_id' => $location_id
			);
		foreach ($item_numbers as $item_number) {
			$this->db->insert('items', $data);
		}
	}
}
?>
