<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_model extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->models();
		$result = $result->result_array();
		$arr = [];
		foreach ($result as  $value) {
			$data['model_id']   = $value['model_id'];
			$data['model_name'] = $value['model_name'];
			$data['make_name'] = $value['make_name'];
			array_push($arr, $data);
		}
		echo json_encode($arr);
	}
	
}
?>