<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class get_item_number extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_item_number');
	}
	
	function index()
	{
	
		$data = $this->Api_item_number->item_number();
		$result = $data->result_array();
		if(count($result) > 0)
			echo json_encode(array('status' => 1, 'data' => $result));
		else
			echo json_encode(array('status' => 0, 'No Data'));
	}		
	
}
?>