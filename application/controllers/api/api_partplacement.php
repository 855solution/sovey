<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_partplacement extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->partplacements();
		$result = $result->result_array();
		$arr = [];
		foreach ($result as  $value) {
			$data['partplacement_id']   = $value['partplacement_id'];
			$data['partplacement_name'] = $value['partplacement_name'];
			array_push($arr, $data);
		}
		echo json_encode($arr);
	}
	
}
?>