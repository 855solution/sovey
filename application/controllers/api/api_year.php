<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_year extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->year();
		$result = $result->result_array();
		
		echo json_encode($result );
	}
	
}
?>