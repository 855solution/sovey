<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class get_image extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');
	}
	
	function index()
	{
	
		$data = $this->Api_add_item->getdefaultimg($this->input->post("item_id"));
		$arr = [];
		if(count($data) > 0) {
			foreach ($data as  $value) {
				$value['imagename'] = "http://soveyautoparts.com/uploads/".$value['imagename'];
				array_push($arr, $value);
			}
			echo json_encode(array("status" => 1, "data" => $arr));
		}else
			echo json_encode(array("status" => 0, "message" => "No Data"));

		
	}		
	

	
}
?>