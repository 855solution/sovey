<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_color extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->colors();
		$result = $result->result_array();
		$arr = [];
		foreach ($result as  $value) {
			$data['color_id']   = $value['color_id'];
			$data['color_name'] = $value['color_name'];
			array_push($arr, $data);
		}
		echo json_encode($arr);
	}
	
}
?>