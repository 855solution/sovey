<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class filter_partnumber extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');
	}
	
	function index()
	{

		$data =  $this->Api_add_item->search_part_number();
		$result = $data->result_array();
		
		if (count($result) == 0)
			echo json_encode(array("status" => 0, "message"=> "failed", "data" => "No Data"));
		else{
			echo json_encode(array("status" => 1, "message"=> "success", "data" => $result));
		}

	}
			
}
?>