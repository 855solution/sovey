<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class api_khmername extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_add_item');


	}
	
	function index()
	{
		
		$result = $this->Api_add_item->khmernames();
		$arr = [];
		
		foreach ($result as  $value) {
			$data['khmername_id']   = $value['khmername_id'];
			$data['khmername_name'] = $value['khmername_name'];
			array_push($arr, $data);
		}
		if(count($result) > 0)
			echo json_encode(array("status" => 1, "data" =>$arr));
		else
			echo json_encode(array("status" => 0, "No data"));
	}
	
}
?>