<?php
require_once ("person_controller.php");
class Customers extends Person_controller
{
	function __construct()
	{
		parent::__construct('customers');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/customers/index');
		$config['total_rows'] = $this->Customer->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_people_manage_table( $this->Customer->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('people/manage',$data);
	}
	
	/*
	Returns customer table data rows. This will be called with AJAX.
	*/
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_people_manage_table_data_rows($this->Customer->search($search),$this);
		echo $data_rows;
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Customer->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	/*
	Loads the customer edit form
	*/
	function view($customer_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['person_info']=$this->Customer->get_info($customer_id);
		$data['cg'] = $this->db->query("SELECT * FROM ospos_customer_group WHERE is_active=1")->result();
		$data['select_title'] = $data['person_info']->title;
		$this->load->view("customers/form",$data);
	}
	
	/*
	Inserts/updates a customer
	*/
	function save_from_sale(){
		$person_data = array(
						'first_name'=>$this->input->post('name'),
						'phone_number'=>$this->input->post('phone_1'),
						'phone_number_2'=>$this->input->post('phone_2')
						);
			
		$customer_id = $this->Customer->save_from_sale($person_data);
		$data['customer_id']=$customer_id;

		echo json_encode($data);

		// if ($customer_id) {
		// 	redirect(site_url('sales/select_customer/'.$customer_id));
		// }
	}
	function save($customer_id=-1)
	{
		$person_data = array(
		'first_name'=>$this->input->post('first_name'),
		'last_name'=>$this->input->post('last_name'),
		'nick_name'=>$this->input->post('nick_name'),
		'email'=>$this->input->post('email'),
		'phone_number'=>$this->input->post('phone_number'),
		'phone_number_2'=>$this->input->post('phone_number2'),
		'address_1'=>$this->input->post('address_1'),
		'address_2'=>$this->input->post('address_2'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'zip'=>$this->input->post('zip'),
		'country'=>$this->input->post('country'),
		'comments'=>$this->input->post('comments'),
		'title' => $this->input->post('cus_title'),
		'payment_term' => $this->input->post('pay_term'),
		'customer_group'=> $this->input->post('cg')
		);
		$customer_data=array(
		'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),
		'taxable'=>$this->input->post('taxable')=='' ? 0:1,
		);
		if($this->Customer->save($person_data,$customer_data,$customer_id))
		{
			//New customer
			if($customer_id==-1)
			{
				// echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_successful_adding').' '.
				// $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_data['person_id']));
				$customer_id = $customer_data['person_id'];
				$this->upload($customer_id);
			}
			else //previous customer
			{
				// echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_successful_updating').' '.
				// $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id));
				$this->upload($customer_id);
			}
		}
		else//failure
		{	
			// echo json_encode(array('success'=>false,'message'=>$this->lang->line('customers_error_adding_updating').' '.
			// $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
		redirect('customers/');
	}
	
	/*
	This deletes customers from the customers table
	*/
	function delete()
	{
		$this->permission_lib->checkPermission();
		$customers_to_delete=$this->input->post('ids');
		
		if($this->Customer->delete_list($customers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_successful_deleted').' '.
			count($customers_to_delete).' '.$this->lang->line('customers_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('customers_cannot_be_deleted')));
		}
	}
	
	function excel()
	{
		$data = file_get_contents("import_customers.csv");
		$name = 'import_customers.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->load->view("customers/excel_import", null);
	}

	function do_excel_import()
	{
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = $this->lang->line('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				
				$i=1;
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					$person_data = array(
					'first_name'=>$data[0],
					'last_name'=>$data[1],
					'nick_name'=>$data[2],
					'email'=>$data[3],
					'phone_number'=>$data[4],
					'address_1'=>$data[5],
					'address_2'=>$data[6],
					'city'=>$data[7],
					'state'=>$data[8],
					'zip'=>$data[9],
					'country'=>$data[10],
					'comments'=>$data[11]
					);
					
					$customer_data=array(
					'account_number'=>$data[12]=='' ? null:$data[12],
					'taxable'=>$data[13]=='' ? 0:1,
					);
					
					if(!$this->Customer->save($person_data,$customer_data))
					{	
						$failCodes[] = $i;
					}
					
					$i++;
				}
			}
			else 
			{
				echo json_encode( array('success'=>false,'message'=>'Your upload file has no data or not in supported format.') );
				return;
			}
		}

		$success = true;
		if(count($failCodes) > 1)
		{
			$msg = "Most customers imported. But some were not, here is list of their CODE (" .count($failCodes) ."): ".implode(", ", $failCodes);
			$success = false;
		}
		else
		{
			$msg = "Import Customers successful";
		}

		echo json_encode( array('success'=>$success,'message'=>$msg) );
	}
	
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{			
		return 350;
	}
	// ================================new upload==========================
	function upload($productid)
	{       

	    $this->load->library('upload');
	    $orders=$this->input->post('order');
	    $updimg=$this->input->post('updimg');
		$arrid=$this->input->post('deleteimg');
		$arrid=trim($arrid,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_customer_images where image_id='".$arr[$i]."'")->row();
				if(isset($row->imagename)){
					unlink('./uploads/'.$row->imagename);
					unlink('./uploads/thumb/'.$row->imagename);
					$this->db->where('image_id',$row->image_id)->delete('ospos_customer_images');
				}
			}
		}
		// echo $arrid;
	 //    $this->unlinkpic($productid,$arrid);
	    $files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);
	    for($i=0; $i<$cpt; $i++)
	    {         
	    	if(isset($updimg[$i]) && $updimg[$i]!=''){
	    		$this->updatepic($updimg[$i],$orders[$i]);
	    	}  
	    	// echo $orders[$i];
	        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
	        
	        $this->upload->initialize($this->set_upload_options($productid,$_FILES['userfile']['name']));
	        // $this->upload->do_upload();
	        if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());	
				 // echo $error['error'];		
			}else{	
				// echo $_FILES['userfile']['name'];
				$this->creatthumb($productid,$_FILES['userfile']['name'],$orders[$i]);
			}
	    }
	}
	function updatepic($picid,$order){
		$this->db->where('image_id',$picid)->set('first_image',$order)->update('ospos_customer_images');
	}
	function creatthumb($productid,$imagename,$order){
			$data = array('upload_data' => $this->upload->data());
		 	$config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = './uploads/thumb';
            $config2['maintain_ratio'] = false;
            $config2['create_thumb'] = "$productid".'_cus_'."$imagename";
            $config2['thumb_marker'] = false;
            $config2['height'] = 190;
            $config2['width'] = 195;
            $this->load->library('image_lib');
            $this->image_lib->initialize($config2); 
            if ( ! $this->image_lib->resize()){
            	echo $this->image_lib->display_errors();
			}else{
				$this->saveimg($productid,$this->upload->file_name,$order);
			}
		
	}
	private function set_upload_options($productid,$imagename)
	{   
	    //upload an image options
	    if(!file_exists('./uploads/') || !file_exists('./uploads/')){
		    if(mkdir('./uploads/',0755,true)){
		        return true;
		    }
	    	if(mkdir('./uploads/thumb',0755,true)){
		                return true;
		    }
		}
	    $config = array();
	    $config['upload_path'] = './uploads/';
	    $config['allowed_types'] = 'gif|jpg|jpeg|png';
	    $config['max_size']      = '5000';
	    $config['file_name']  	 = "$productid".'_cus_'."$imagename";
		$config['overwrite']	 = true;
		$config['file_type']='image/png';

	    return $config;
	}
	function saveimg($productid,$imagename,$order){
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		$count=$this->db->query("SELECT count(*) as count FROM ospos_customer_images where customer_id='$productid' AND imagename='$imagename'")->row()->count;
		if($count==0){
			$data=array('customer_id'=>$productid,
						'imagename'=>$imagename,
						'first_image'=>$order);
			$this->db->insert('ospos_customer_images',$data);
		}
	}
	function unlinkpic($stockid,$arrids){
		$arrid=trim($arrids,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_customer_images where image_id='".$arr[$i]."'")->row();
				if(isset($row->pic_name)){
					unlink('./uploads/'.$row->imagename);
					unlink('./uploads/thumb/'.$row->imagename);
					$this->db->where('image_id',$row->image_id)->delete('ospos_customer_images');
				}
			}
		}
	}

	// ============================================================================
	function view_all_customer(){

		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_customer_manage_table( $this->Customer->get_all(), $this );
		$this->load->view('customers/select_form',$data);
	}
	function search_cus_sale(){
		$s = $this->input->post('search');
		$data['manage_table']=get_customer_manage_table( $this->Customer->search($s), $this );

		echo json_encode($data);
	}
	function check_phone_number(){
		$p = $this->input->post('val');
		$phone_exist = $this->db->query("SELECT * FROM ospos_people p
										INNER JOIN ospos_customers c ON p.person_id=c.person_id
										WHERE p.phone_number = '$p' OR p.phone_number_2='$p' AND c.deleted=0")->result();
		$data['t'] = false;
		
		if (count($phone_exist)>0) {
			$data['t'] = true;
		}
		echo json_encode($data);
	}
	function submit_check(){
		$p1 = $this->input->post('p1');
		$p2 = $this->input->post('p2');
		if ($p1!='') {
			$cp1 = $this->db->query("SELECT * FROM ospos_people WHERE phone_number = '$p1' OR phone_number_2 = '$p1'")->result();
		}
		if ($p2!='') {
			$cp2 = $this->db->query("SELECT * FROM ospos_people WHERE phone_number = '$p2' OR phone_number_2 = '$p2'")->result();
		}


		$data['tp1'] = 'n';
		$data['tp2'] = 'n';

		if (count($cp1)>0) {
			$data['tp1'] = 'y';
		}
		if (count($cp2)>0) {
			$data['tp2'] = 'y';
		}

		// $data['t'] = $p1.'-'.$p2;
		echo json_encode($data);
	}
	function view_customer_group(){

		// $this->permission_lib->checkPermission();

		// $data['person_info']=$this->Customer->get_info($customer_id);
		// $data['select_title'] = $data['person_info']->title;
		$this->load->view("customers/customer_group_list",$data);

	}

	function check_group_name(){
		$name = $this->input->post('name');

		$is_dup = $this->db->query("SELECT cg_id FROM ospos_customer_group WHERE cg_name = '$name' AND is_active=1 LIMIT 1")->row();

		if (!empty($is_dup)) {
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}

	function save_cg(){
		$name = $this->input->post('name');
		$cg_id = $this->input->post('id');

		$data = array('cg_name'=>$name);

		if ($cg_id!='') {
			$this->db->where('cg_id',$cg_id)->update('ospos_customer_group',$data);
		}else{
			$this->db->insert('ospos_customer_group',$data);
		}

		echo json_encode(1);


	}

	function get_all_cg(){
		$cg = $this->db->query("SELECT * FROM ospos_customer_group WHERE is_active=1")->result();

		$row = '';
		$i=0;

		foreach ($cg as $c) {
			$edit_link = "<button class='btn btn-primary btn_edit' onclick=action_edit(".$c->cg_id.")><i class='fa fa-pencil'></i></button>";
			$del_link = "<button class='btn btn-danger btn_del' onclick=action_del(".$c->cg_id.")><i class='fa fa-close'></i></button>";
			$i++;
			$row.="<tr>";
				$row.="<td>".$i."</td>";
				$row.="<td>".$c->cg_name."</td>";
				$row.="<td align='right'>".$edit_link.'&nbsp;'.$del_link."</td>";
			$row.="</tr>";
		}

		$data['tb'] = $row;

		echo json_encode($data);
	}

	function del_cg(){
		$id = $this->input->post('id');

		$this->db->where('cg_id',$id)->delete('ospos_customer_group');

		echo json_encode(1);
	}

	function get_cg(){
		$id = $this->input->post('id');
		$row = $this->db->query("SELECT * FROM ospos_customer_group WHERE cg_id='$id'")->row();

		echo json_encode($row);
	}
}
?>