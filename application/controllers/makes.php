<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Makes extends Secure_area
{
	function __construct()
	{
		parent::__construct('makes');
	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/makes/index');
		$config['total_rows'] = $this->Make->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_makes_manage_table( $this->Make->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('makes/manage',$data);
	}
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Makes';
		$make_data = $this->Make->get_all_make();
		$table = '';
		$i=0;
		foreach ($make_data as $mk) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$mk->make_name</td>  
						<td>$mk->description</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$mk->make_name' f='$mk->make_id' d='$mk->description'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$mk->make_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('makes/manage',$data);
		$this->load->view('partial/footer');
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_makes_manage_table_data_rows($this->Make->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Make->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$make_id = $this->input->post('row_id');
		$data_row=get_make_data_row($this->Make->get_info($make_id),$this);
		echo $data_row;
	}

	function view($make_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['make_info']=$this->Make->get_info($make_id);

		$this->load->view("makes/form",$data);
	}
	
	function save_old($make_id=-1)
	{
		$make_data = array(
		'make_name'=>$this->input->post('make_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Make->save( $make_data, $make_id ) )
		{
			//New make
			if($make_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('makes_successful_adding').' '.
				$make_data['make_name'],'make_id'=>$make_data['make_id']));
				$make_id = $make_data['make_id'];
			}
			else //previous make
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('makes_successful_updating').' '.
				$make_data['make_name'],'make_id'=>$make_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('makes_error_adding_updating').' '.
			$make_data['make_name'],'make_id'=>-1));
		}

	}

	function delete_old()
	{
		$this->permission_lib->checkPermission();
		$makes_to_delete=$this->input->post('ids');

		if($this->Make->delete_list($makes_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('makes_successful_deleted').' '.
			count($makes_to_delete).' '.$this->lang->line('makes_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('makes_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->Make->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function save(){
		$name = $this->input->post('make_name');
		$id = $this->input->post('make_id');
		$desc = $this->input->post('description');
		$exist = $this->Make->check_name($name,$id);
		$make_data = array(
						'make_name'=>$name,
						'description'=>$desc,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('makes'));
		}else{
			$this->Make->save($id,$make_data);
			redirect(site_url('makes'));

		}
	}
	function delete($id){
		$this->db->where('make_id',$id)->update('ospos_makes',array('deleted'=>1));
		redirect(site_url('makes'));
	}
}
?>