<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Branchs extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('branchs');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/branchs/index');
		$config['total_rows'] = $this->Branch->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_branchs_manage_table( $this->Branch->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('branchs/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_branchs_manage_table_data_rows($this->Branch->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Branch->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$branch_id = $this->input->post('row_id');
		$data_row=get_branch_data_row($this->Branch->get_info($branch_id),$this);
		echo $data_row;
	}

	function view($branch_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['branch_info']=$this->Branch->get_info($branch_id);

		$this->load->view("branchs/form",$data);
	}
	
	function save($branch_id=-1)
	{
		$branch_data = array(
		'branch_name'=>$this->input->post('branch_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Branch->save( $branch_data, $branch_id ) )
		{
			//New branch
			if($branch_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('branchs_successful_adding').' '.
				$branch_data['branch_name'],'branch_id'=>$branch_data['branch_id']));
				$branch_id = $branch_data['branch_id'];
			}
			else //previous branch
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('branchs_successful_updating').' '.
				$branch_data['branch_name'],'branch_id'=>$branch_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('branchs_error_adding_updating').' '.
			$branch_data['branch_name'],'branch_id'=>-1));
		}

	}

	function delete()
	{
		$this->permission_lib->checkPermission();
		$branchs_to_delete=$this->input->post('ids');

		if($this->Branch->delete_list($branchs_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('branchs_successful_deleted').' '.
			count($branchs_to_delete).' '.$this->lang->line('branchs_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('branchs_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
}
?>