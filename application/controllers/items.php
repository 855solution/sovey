<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Items  extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('items');
		//$this->load->helper(array('form','url','file'));
		//parent::Controller();
		//$this->load->helper('url');
		$this->load->database();
		$this->load->model('Modtemplate','tem');
		$this->load->model('item');
        $this->load->helper(array('form', 'url'));
        $this->load->library('sale_lib');//New 02-08-2012
        $this->load->model('Sale');
	}

	function index()
	{
		$this->permission_lib->checkPermission();
		$data['show_delete'] = $this->permission_lib->user_f('delete');
		$data['show_sold_item'] = $this->permission_lib->user_f('sold_item');
		$this->sale_lib->unset_filter();
		if (!$this->uri->segment(2)) {
			$this->session->unset_userdata('make_id');
			$this->session->unset_userdata('model_id');
			$this->session->unset_userdata('year');
			$this->session->unset_userdata('yearto');
			$this->session->unset_userdata('khmername');
			$this->session->unset_userdata('low_inventory');
			$this->session->unset_userdata('is_serialized');
			$this->session->unset_userdata('no_description');
			$this->session->unset_userdata('vin');
		}
		$item_id='';
		$numpage=50;
		if(isset($_GET['p']))
			$numpage=$_GET['p'];
		$per_page='';
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];
		$config['base_url'] = site_url('/items/index?p='.$numpage);
		$config['total_rows'] = $this->Item->count_all();
		$data['total_rows'] = $config['total_rows'];
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 3;
		$config['page_query_string']=TRUE;

		$this->pagination->initialize($config);
		
		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		
		$data['manage_table']=get_items_manage_table( $this->Item->get_all( $config['per_page'], $per_page), $this );
		
		/*make, model, year combo box for searching*/
		$makes = array('' => 'Please Select');
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		//$data['selected_make'] = $this->Item->get_info_where_itemnumber($itemnumber)->make_id;
		$data['selected_make'] = '';
		
		/****************/
		$models = array('' => 'Please Select');
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		/******************************/
		$khmernames = array('' => 'Please Select');
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['khmername_name'];
		}		
		$data['khmernames']=$khmernames;
		$data['selected_khmername'] = $this->Khmername->get_info($item_id)->khmername_id;
		/************************close******************************************/
		$this->load->model('vinnumber');
		$data['vinnumber'] = $this->vinnumber->get_all_vin();
		$this->load->view('items/manage',$data);
	}
	function clearitem(){
		$this->db->query("UPDATE ospos_items SET deleted='1' WHERE quantity<1");
		$itemdata=$this->Item->getremoveitem();
		foreach ($itemdata as $row) {
			$this->db->where('item_id',$row->trans_items)->set('deleted',1)->update('ospos_items');
		}
		echo "Clear successful";
	}
	// function getdefimage($item_id){
		
	// }
	function get_item_khmer($khmer_id){
		$name=array();
		$this->load->model('item');

		$khmername=$this->item->khmer_name($khmer_id)->row_array();
		if(isset($khmername['khmername_name'])){
			$name['khmer_name']=$khmername['khmername_name'];
			return $name['khmer_name'];
		}
		
	}

	
	function refresh()
	{

		$li = $this->input->post('low_inventory');
		$iss = $this->input->post('is_serialized');
		$nod = $this->input->post('no_description');

		// echo $li;die();
		// if ($li==0) {
		// 	$this->session->unset_userdata('low_inventory');
		// }
		// if (!$iss) {
		// 	$this->session->unset_userdata('is_serialized');
		// }
		// if (!$nod) {
		// 	$this->session->unset_userdata('no_description');
		// }

		$ma = $this->input->post('make_id');
		$mo = $this->input->post('model_id');
		$y = $this->input->post('year');
		$yt = $this->input->post('yearto');
		$kn = $this->input->post('khmername_id');

		$af = $this->input->post('all_fields');
		$nf = $this->input->post('name_file');

		$s_s=$this->session->userdata('khmername');
		$s_ma=$this->session->userdata('make_id');
		$s_mo=$this->session->userdata('model_id');
		$s_y=$this->session->userdata('year');
		$s_yt=$this->session->userdata('yearto');
		$s_li = $this->session->userdata('low_inventory');
		$s_iss = $this->session->userdata('is_serialized');
		$s_nod = $this->session->userdata('no_description');


		if ($ma || $mo || $y || $yt || $kn || $li || $iss || $nod) {
			$this->session->set_userdata('make_id',$ma);
			$this->session->set_userdata('model_id',$mo);
			$this->session->set_userdata('year',$y);
			$this->session->set_userdata('yearto',$yt);
			$this->session->set_userdata('khmername',$kn);
			$this->session->set_userdata('low_inventory',$li);
			$this->session->set_userdata('is_serialized',$iss);
			$this->session->set_userdata('no_description',$nod);
			
		}elseif($s_s || $s_ma || $s_mo || $s_y || $s_yt || $s_li || $s_iss || $s_nod){
			$this->session->set_userdata('make_id',$s_ma);
			$this->session->set_userdata('model_id',$s_mo);
			$this->session->set_userdata('year',$s_y);
			$this->session->set_userdata('yearto',$s_yt);
			$this->session->set_userdata('khmername',$s_s);
			$this->session->set_userdata('low_inventory',$s_li);
			$this->session->set_userdata('is_serialized',$s_iss);
			$this->session->set_userdata('no_description',$s_nod);
		}else{
			redirect(site_url('items'));
		}

		$low_inventory=$this->session->userdata('low_inventory');
		$is_serialized=$this->session->userdata('is_serialized');
		$no_description=$this->session->userdata('no_description');
		
		//Search
		$make_filter=$this->session->userdata('make_id');
		$model_fitler=$this->session->userdata('model_id');
		$year_filter=$this->session->userdata('year');
		$yearto_filter=$this->session->userdata('yearto');
		$khmername_filter=$this->session->userdata('khmername');
		
		$all_fields = $this->input->post('all_fields');
		$name_file = $this->input->post('name_file');

		//echo $name_file ;
		if($all_fields==1 ){
     			//$this->make_name($name_file );
		}			
		/*close fields of filter search*/
		$numpage=30;
		if(isset($_GET['p']))
			$numpage=$_GET['p'];

		$per_page='';
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];

		$config['base_url'] = site_url('/items/refresh?p='.$numpage);
		
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 3;
		$config['page_query_string']=TRUE;

		
		$data['form_width']=$this->get_form_width();
		///////	
		$data['search_section_state']=$this->input->post('search_section_state');
		$data['low_inventory']=$this->session->userdata('low_inventory');
		$data['is_serialized']=$this->session->userdata('is_serialized');
		$data['no_description']=$this->session->userdata('no_description');
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		
		if($make_filter||$model_fitler||$year_filter || $khmername_filter){
		$config['total_rows'] = $this->Item->count_all_filtered($make_filter,$model_fitler,$year_filter,$yearto_filter,$khmername_filter);
		$data['manage_table']=get_items_manage_table($this->Item->get_all_filtered_make_model_year($make_filter,$model_fitler,$year_filter,$yearto_filter,$khmername_filter,$numpage,$per_page),$this);
		}
		
		if($low_inventory || $is_serialized || $no_description){
		$config['total_rows'] = $this->Item->count_filtered($low_inventory,$is_serialized,$no_description);
		$data['manage_table']=get_items_manage_table($this->Item->get_all_filtered($low_inventory,$is_serialized,$no_description,$numpage,$per_page),$this);
		}

		$this->pagination->initialize($config);

		/*make, model, year combo box for searching*/
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}	
			
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info_where_makeId($make_filter)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_model_depend_make($make_filter)->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info_where_modelId($model_fitler)->model_id;
		
		/******************************/
		$khmernames = array('' => $this->lang->line('items_none'));
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['khmername_name'];
		}		
		$data['khmernames']=$khmernames;
		$data['selected_khmername'] = $this->Khmername->get_info($khmername_filter)->khmername_id;
		
		/******************************close******************************************/
		//echo "jfhghfjghdfh";
		
		
		
		$this->load->view('items/manage',$data);
	}

	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Item->find_item_info($item_number));
	}


	
	
	function search()
	{
		
		redirect(site_url('items/s='.$_POST['search']));
		// $this->search_vin();
		// return;
		$search = '';
		
		$search=$this->input->post('search');
		if (isset($_GET['s'])) {
			$search = $_GET['s'];
		}
		$numpage=30;
		if(isset($_GET['p']))
			$numpage=$_GET['p'];
		$per_page='';
		if(isset($_GET['per_page']))
			$per_page=$_GET['per_page'];
		$config['base_url'] = site_url('/items/search?s='.$search.'&p='.$numpage);
		$config['total_rows'] = $this->Item->count_all_search($vin,$search);
		$data['total_rows'] = $config['total_rows'];
		$config['per_page'] = $numpage;
		$config['uri_segment'] = 3;
		$config['page_query_string']=TRUE;

		$this->pagination->initialize($config);
		$rows=$this->Item->fullTextSearch($search,$numpage,$per_page);
		
		$data_rows=get_items_manage_table($rows,$this);
		
		$data['manage_table']=$data_rows;
		$data['controller_name']=strtolower(get_class());

		$this->load->view('items/search_result',$data);
		
	
		
	}

	/*
	Gives search suggestions based on what is being searched for
	*/

	function get_vin(){
		$vin_s = $this->input->post('v');
		$this->session->set_userdata('vin',$vin_s);
		echo $this->session->userdata('vin');
	}
	function suggest()
	{
		
		// $vin = $this->session->userdata('vin');
		$suggestions = $this->Item->get_search_suggestions($vin,$this->input->post('q'),$this->input->post('limit'));
		// var_dump(implode("\n",$suggestions));die();
		echo implode("\n",$suggestions);
	}
	
	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Item->get_category_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_item_data_row($this->Item->get_info($item_id),$this);
		echo $data_row;
	}
function view_mobile($item_id=-1)
	{

		$data['item_info']=$this->Item->get_info($item_id);
		//$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		
		$data['selected_supplier'] = $this->Supplier->get_info($this->Item->get_info($item_id)->supplier_id)->supplier_name;	
				
		$data['selected_location'] = $this->Location->get_info($this->Item->get_info($item_id)->location_id)->location_name;	
		$data['selected_make'] = $this->Make->get_info($this->Item->get_info($item_id)->make_id)->make_name;		
		$data['selected_model'] = $this->Model->get_info($this->Item->get_info($item_id)->model_id)->model_name;
		$data['selected_color'] = $this->Color->get_info($this->Item->get_info($item_id)->color_id)->color_name;
		$data['selected_condition'] = $this->Condition->get_info($this->Item->get_info($item_id)->condition_id)->condition_name;	
		$data['selected_partplacement'] = $this->Partplacement->get_info($this->Item->get_info($item_id)->partplacement_id)->partplacement_name;
		$data['selected_brand'] = $this->Branch->get_info($this->Item->get_info($item_id)->branch_id)->branch_name;		
		$data['selected_category'] = $this->Category->get_info($this->Item->get_info($item_id)->category_id)->category_name;
		$data['selected_khmername'] = $this->Khmername->get_info($this->Item->get_info($item_id)->khmername_id)->khmername_name;
				/*==-========================================================================*/
				
				
				
				$data['image_name']=$this->Item->get_image_source($item_id)->result_array();
				/*
				if($item_id==-1){
				$this->load->view("items/form_insert_new_item",$data);	
				}else{
				$this->load->view("items/form",$data);
				}*/
		print(json_encode($data));
	}
	function view($item_id=-1)
	{
		$data['can_edit'] = $this->permission_lib->user_f('edit');

		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['location_id']] = $row['location_name'];
		}		
		$data['locations']=$locations;
		$data['selected_location'] = $this->Item->get_info($item_id)->location_id;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info($item_id)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info($item_id)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info($item_id)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info($item_id)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info($item_id)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info($item_id)->category_id;

		$khmernames = array('' => $this->lang->line('items_none'));
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['english_name'].' / '.$row['khmername_name'];
		}		
		$data['khmernames']=$khmernames;
		$data['selected_khmername'] = $this->Item->get_info($item_id)->khmername_id;
		$data['selected_khmername_name'] = $this->Item->get_info($item_id)->english_name.' / '.$this->Item->get_info($item_id)->khmername_name;
		
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		
		
		/*Grade of Items*/
		
		$grade = array('' => $this->lang->line('items_none'));
		
		foreach($this->Item->get_all_grade()->result_array() as $row)
		{
			$grade[$row['grad_id']] = $row['grad_name'];
		}	
			
		$data['grade']=$grade;
		$data['selected_grade'] = $this->Item->get_info($item_id)->grad_id;
		
		/*get unit price and cost price where grade_id*/
		$gradeA=$this->Item->get_unitPrice_costPrice(1)->row_array();
		$data['unitpriceA']=$gradeA['unit_price'];
		$data['costpriceA']=$gradeA['cost_price'];
		
		$gradeB=$this->Item->get_unitPrice_costPrice(2)->row_array();
		$data['unitpriceB']=$gradeB['unit_price'];
		$data['costpriceB']=$gradeB['cost_price'];
		
		$gradeC=$this->Item->get_unitPrice_costPrice(3)->row_array();
		$data['unitpriceC']=$gradeC['unit_price'];
		$data['costpriceC']=$gradeC['cost_price'];
		
		/*==-========================================================================*/
		$data['khmername_image'] = $this->Khmername->get_all()->result();
		
		
		$data['image_name']=$this->Item->get_image_source($item_id)->result_array();
		$data['vin'] = $this->Item->get_vinnumber();
		// $data['name_id'] = $this->input->post('name_id');
		if($item_id==-1){
			$this->check_valid_photo();
			$data['next_item_id'] = $this->db->query("SELECT MAX(item_id) AS max_id FROM ospos_items")->row()->max_id + 1;
			$this->load->view("items/form_insert_new_item",$data);	
		}else{
			$data['next_item_id'] = $item_id;
			$this->load->view("items/form",$data);
		}
	}

	function check_valid_photo()
	{
		$photo_max_item_id = $this->db->query("select max(item_id) as max_id from ospos_image_items")->row()->max_id;
		$count_exist = $this->db->get_where('ospos_items', ['item_id'=>$photo_max_item_id])->num_rows();
		if($count_exist == 0)
		{
			$photos = $this->db->get_where('ospos_image_items', ['item_id'=>$photo_max_item_id])->result();
			// delete files
			foreach ($photos as $photo) {
				$img_path = FCPATH.'uploads/'.$photo->imagename;
				$thumb_path = FCPATH.'uploads/thumb/'.$photo->imagename;
				unlink($img_path);
				unlink($thumb_path);
			}
			// delete from database
			$this->db->where('item_id', $photo_max_item_id)->delete('ospos_image_items');
		}
		

	}
	
	/*public function indexeee()
	{
		echo 'Hello World!';
	}*/
	
	
	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/inventory",$data);
	}
	
	function count_details($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/count_details",$data);
	} //------------------------------------------- Ramel
	function get_item_ft_date(){
		$df = $this->input->post('db_from');
		$dt = $this->input->post('db_to');
		$dfd = $this->input->post('db_from_digit');
		$dtd = $this->input->post('db_to_digit');

		if ($df && $dt) {
			$item_ids= array();

			$items=$this->Item->get_item_ft_date($df,$dt);
			foreach ($items as $it) {
				array_push($item_ids,$it->item_id);
			}
			$item_ids=implode(':',$item_ids);
		}

		if ($dfd && $dtd) {
			$item_ids= array();

			// $fd=$this->item->convert6DigitToID($dfd);
			// $td=$this->item->convert6DigitToID($dtd);
			$fd=$dfd;
			$td=$dtd;

			if ($fd && $td) {
				$items=$this->Item->get_item_ft_digit($fd,$td);
				foreach ($items as $it) {
					array_push($item_ids,$it->item_id);
				}
				$item_ids=implode(':',$item_ids);
			}
			
		}
		
		echo $item_ids;
		// echo $fd;
		
	}
	function generate_barcodes($item_ids,$barcode_time=1)
	{
		$result = array();
		$item_info=array();
		$item_ids = explode(':', $item_ids);

		foreach ($item_ids as $item_id)
		{

			$item_info[] = $this->Item->get_info($item_id);

			// $result[] = array('barcode' =>$item_info->barcode, 'id'=> $item_id,'name' =>$item_info->name, 'item_id' =>$item_info->item_id);
		}

		$data['items'] = $item_info;
		$data['barcodetime']=$barcode_time;
		$this->load->view("barcode_sheet", $data);
	}

	function bulk_edit()
	{
		$data = array();
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['first_name'] .' '. $row['last_name'];
		}
		$data['suppliers'] = $suppliers;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['lcoation_id']] = $row['location_name'];
		}
		$data['locations'] = $locations;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}
		$data['makes'] = $makes;
		
		$data['allow_alt_desciption_choices'] = array(
			''=>$this->lang->line('items_do_nothing'), 
			1 =>$this->lang->line('items_change_all_to_allow_alt_desc'),
			0 =>$this->lang->line('items_change_all_to_not_allow_allow_desc'));
				
		$data['serialization_choices'] = array(
			''=>$this->lang->line('items_do_nothing'), 
			1 =>$this->lang->line('items_change_all_to_serialized'),
			0 =>$this->lang->line('items_change_all_to_unserialized'));
		$this->load->view("items/form_bulk", $data);
	}
	
	function mobile_save($item_id=-1)
	{
		
		$this->load->helper('url');
		$this->load->model('item');
		$name=array();
		$makeid=$this->input->post('make_id');
		$modelid=$this->input->post('model_id');
		$year_input=$this->input->post('year');
		$grad_id=$this->input->post('grade_id');
		
		$make_name=$this->item->make_name($makeid)->row_array();
		$name['getname']=$make_name['make_name'];
		
		$modelname=$this->item->model_name($modelid)->row_array();
		$name['modelname']=$modelname['model_name'];
		
		$make_name=$this->get2FirstString($name['getname']);
		$year=$this->get2lastString($year_input);
		$model_name=$this->get2FirstString($name['modelname']);
		
		//$digit=$this->generate_barcode_6digit(6);
		//$digit=$this->input->post('item_number')+1;
		//if($item_id==-1){
		//$lastid=$this->Item->getLastId();
		//$id=$lastid['item_id'];
		
		//$digit=$lastid+1;
		//$digit_padding=str_pad($digit,6,"1",STR_PAD_LEFT);
		
		//$barcode=$year.$make_name.$model_name.$digit_padding;
		//}else{
			//$barcode=$this->Item->get_info($item_id);
			//$barcode=$item_data['barcode'];
			//}
		
		
		$item_data = array(
		'name'=>$this->input->post('name'),
		'description'=>$this->input->post('description'),
		'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
		'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
		'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
		'cost_price'=>$this->input->post('cost_price'),
		'unit_price'=>$this->input->post('unit_price'),
		'quantity'=>$this->input->post('quantity'),
		'reorder_level'=>$this->input->post('reorder_level'),
		'location'=>$this->input->post('location'),
		'fit'=>$this->input->post('fit'),
		'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
		'year'=>$this->input->post('year'),
		'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
		'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
		'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
		'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
		'allow_alt_description'=>$this->input->post('allow_alt_description'),
		'is_serialized'=>$this->input->post('is_serialized'),
		'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
		'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
		'category'=>$this->input->post('category'),
		'is_new'=>$this->input->post('is_new'),
		'is_pro'=>$this->input->post('is_pro'),
		'is_feat'=>$this->input->post('is_feat'),
		'grad_id'=>$grad_id,
		'barcode'=>$barcode,
		'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id')
		);
		
		
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		
		if($this->Item->save($item_data,$item_id))
		{
			
			//$itemid=$this->db->insert_id();
			
			//New item
			if($item_id==-1)
			{
				
				$item_id = $item_data['item_id'];
			}
			else //previous item
			{
				
			}
			
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item_id,
				'trans_user'=>$employee_id,
				'trans_comment'=>$this->lang->line('items_manually_editing_of_quantity'),
				'trans_inventory'=>$cur_item_info ? $this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity')
			);
			
			$this->Inventory->insert($inv_data);
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			


			
			
			$this->addImage($item_id);


/*--------------------update barcode after save item sucessful-----------------------------------------*/
			
			$digit_padding=str_pad($item_id,6,"0",STR_PAD_LEFT);
			$barcode=$year.$make_name.$model_name.$digit_padding;
			
			$barcode_item_data = array(
				'name'=>$this->input->post('name'),
				'description'=>$this->input->post('description'),
				'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
				'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
				'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
				'cost_price'=>$this->input->post('cost_price'),
				'unit_price'=>$this->input->post('unit_price'),
				'quantity'=>$this->input->post('quantity'),
				'reorder_level'=>$this->input->post('reorder_level'),
				'location'=>$this->input->post('location'),
				'fit'=>$this->input->post('fit'),
				'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
				'year'=>$this->input->post('year'),
				'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
				'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
				'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
				'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
				'allow_alt_description'=>$this->input->post('allow_alt_description'),
				'is_serialized'=>$this->input->post('is_serialized'),
				'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
				'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
				'category'=>$this->input->post('category'),
				'is_new'=>$this->input->post('is_new'),
		        'is_pro'=>$this->input->post('is_pro'),
		        'is_feat'=>$this->input->post('is_feat'),
				'barcode'=>$barcode,
				'grad_id'=>$grad_id,
				'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id')
				);
			$this->Item->updateBarcodeAfterSave($item_id,$barcode_item_data);
			
			/*-----------------------------------------------------------------------------*/

		
			echo "300 OK";
		}
		else//failure
		{
			//echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			//$item_data['name'],'item_id'=>-1));
		}
	}
	function save($item_id=-1)
	{	
		$this->permission_lib->checkPermission();
		// var_dump($_POST);die();
		$is_new_item=0;
		if ($item_id==-1) {
			$is_new_item = 1;
		}
		$this->load->helper('url');
		$this->load->model('item');

		$name=array();
		$makeid=$this->input->post('make_id');
		$modelid=$this->input->post('model_id');
		$year_input=$this->input->post('year');
		$grad_id=$this->input->post('grade_id');
		// $newpart=$this->input->post('countpart');
		$body = $this->input->post('vin_body');
		$trim = $this->input->post('vin_trim');
		$trans = $this->input->post('vin_trans');
		$fuel = $this->input->post('vin_fuel');
		$engine = $this->input->post('vin_engine');



		$add_date= date("Y-m-d");
		$make_name=$this->item->make_name($makeid)->row_array();
		$name['getname']=$make_name['make_name'];
		$make_name_name = $this->item->make_name($makeid)->row()->make_name;
		
		$modelname=$this->item->model_name($modelid)->row_array();
		$name['modelname']=$modelname['model_name'];
		$model_name_name =$this->item->model_name($modelid)->row()->model_name;

		$color_name = $this->item->color_name($this->input->post('color_id'))->row()->color_name;
		$khmer_name = $this->item->khmer_name($this->input->post('khmername_id'))->row()->khmername_name;
		$eng_name = $this->item->khmer_name($this->input->post('khmername_id'))->row()->english_name;
		$bran_name = $this->item->branch_name($this->input->post('branch_id'))->row()->branch_name;
		$partp_name = $this->item->part_placement_name($this->input->post('partplacement_id'))->row()->partplacement_name;
		$vin_id = $this->item->get_vin_info($this->input->post('category'))->vinnumber_id;

		$make_name=$this->get2FirstString($name['getname']);
		$year=$this->get2lastString($year_input);
		$model_name=$this->get2FirstString($name['modelname']);
		$tem_img=$this->input->post('tem_img');
		$is_new_template = $this->input->post('is_new_template');
		$item_ids = array();
		// var_dump($is_new_template);die();

		// CHECK APPROVED TEMPLATE

		// $tem_approve = $this->item->checktempapprove($this->input->post('item_number'));


		// if ($tem_approve!=1) {
		// 	$this->session->set_flashdata('er','Partnumber Not Approved!');
		// 	redirect(site_url('items'));
		// }

		
		// echo "$make_name_name -- $model_name_name -- $color_name -- $khmer_name -- $bran_name -- $partp_name";die();
		//$digit=$this->generate_barcode_6digit(6);
		//$digit=$this->input->post('item_number')+1;
		//if($item_id==-1){
		//$lastid=$this->Item->getLastId();
		//$id=$lastid['item_id'];
		
		//$digit=$lastid+1;
		//$digit_padding=str_pad($digit,6,"1",STR_PAD_LEFT);
		
		//$barcode=$year.$make_name.$model_name.$digit_padding;
		//}else{
			//$barcode=$this->Item->get_info($item_id);
			//$barcode=$item_data['barcode'];
			//}
		
		
		
		$newpart = $this->part_exist($this->input->post('item_number'));
		
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		$count_item = 1;

		if ($this->input->post('quantity')>0) {
			$count_item = $this->input->post('quantity');
		}

		$item_str='';

		for ($item_qty=0; $item_qty < $count_item ; $item_qty++) {
				
				$item_data = array(
								'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
								'color_name'=>$color_name,
								'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
								'branch_name'=>$bran_name,
								'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
								'add_date'=>$add_date,
								);

				// if ($item_id==-1) {
					// $is_new_item = 1;
					$data_sec = array(
						'name'=>$eng_name,
						'description'=>$this->input->post('description'),
						'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
						'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
						'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
						// 'cost_price'=>$this->input->post('cost_price'),
						'cost_price'=>0,
						'unit_price'=>$this->input->post('unit_price'),
						'reorder_level'=>$this->input->post('reorder_level'),
						'location'=>$this->input->post('location'),
						'fit'=>$this->input->post('fit'),
						'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
						'year'=>$this->input->post('year'),
						'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
						'allow_alt_description'=>$this->input->post('allow_alt_description'),
						'is_serialized'=>$this->input->post('is_serialized'),
						'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
						'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
						'category'=>$this->input->post('category'),
						'barcode'=>$barcode,
						'grad_id'=>$grad_id,
						'is_new'=>$this->input->post('is_new'),
						'is_pro'=>$this->input->post('is_pro'),
						'is_feat'=>$this->input->post('is_feat'),
						'khmername_id'=>$this->input->post('khmername_id'),
						'engine'=>$engine,
						'body'=>$body,
						'trim'=>$trim,
						'transmission'=>$trans,
						'fuel'=>$fuel,
						'model_name'=>$model_name_name,
						'make_name'=>$make_name_name,
						'khmer_name'=>$khmer_name,
						'partplacement_name'=>$partp_name,
						'vinnumber_id'=>$vin_id
					);
					if($is_new_item == 1)
					{
						$data_sec = array_merge($data_sec, array('quantity'=>1));
					}
					$item_data=array_merge($item_data,$data_sec);
				// }
				// var_dump($item_id);
				// var_dump($item_data);die();
				if($this->Item->save($item_data,$item_id))
				{
					// var_dump($item_data);die();
					//$itemid=$this->db->insert_id();
					
					// //New item
					if($item_id==-1){
					// {
					// 	// if ($item_qty==0) {
					// 	// 	var_dump($this->input->post('tem_img'));
					// 	// 	var_dump($_POST);die();
					// 	// }
					// 	//  json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_adding').' '.
					// 	//$item_data['name'],'item_id'=>$item_data['item_id']));
						$item_id = $item_data['item_id'];
					// 	for ($i=0; $i < count($tem_img) ; $i++) { 
					// 		if($tem_img[$i]!=''){
					// 			$this->saveimg($item_id,$tem_img[$i],1,1);
					// 		}
					// 	}
					}
					else //previous item
					{
					// 	// echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
					// 	// $item_data['name'],'item_id'=>$item_id));
						// $item_id = $item_id;
					// 	for ($i=0; $i < count($tem_img) ; $i++) { 
					// 		if($tem_img[$i]!=''){
					// 			$this->saveimg($item_id,$tem_img[$i],1,1);
					// 		}
					// 	}
					}
					// if($newpart){
					// 	$this->savepart($item_data['item_id']);
						
					// }
					$inv_data = array
					(
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_items'=>$item_id,
						'trans_user'=>$employee_id,
						'trans_comment'=>$this->lang->line('items_manually_editing_of_quantity'),
						'trans_inventory'=>$cur_item_info ? 1 - $cur_item_info->quantity : 1//$this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity')
					);
					
					$this->Inventory->insert($inv_data);
					$items_taxes_data = array();
					$tax_names = $this->input->post('tax_names');
					$tax_percents = $this->input->post('tax_percents');
					for($k=0;$k<count($tax_percents);$k++)
					{
						if (is_numeric($tax_percents[$k]))
						{
							$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
						}
					}
					
		/*--------------------update barcode after save item sucessful-----------------------------------------*/
					
					$digit_padding=str_pad($item_id,6,"0",STR_PAD_LEFT);
					$barcode=$year.$make_name.$model_name.$digit_padding;
					
					$barcode_item_data = array(
						'name'=>$eng_name,
						'description'=>$this->input->post('description'),
						'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
						'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
						'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
						'cost_price'=>$this->input->post('cost_price'),
						'unit_price'=>$this->input->post('unit_price'),

						'reorder_level'=>$this->input->post('reorder_level'),
						'location'=>$this->input->post('location'),
						'fit'=>$this->input->post('fit'),
						'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
						'year'=>$this->input->post('year'),
						'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
						'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
						'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
						'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
						'allow_alt_description'=>$this->input->post('allow_alt_description'),
						'is_serialized'=>$this->input->post('is_serialized'),
						'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
						'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
						'category'=>$this->input->post('category'),
						'barcode'=>$barcode,
						'grad_id'=>$grad_id,
						'is_new'=>$this->input->post('is_new'),
						'is_pro'=>$this->input->post('is_pro'),
						'is_feat'=>$this->input->post('is_feat'),
						'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id')
						);
					if($is_new_item == 1)
					{
						$barcode_item_data = array_merge($barcode_item_data, array('quantity'=>1));
					}
					$this->Item->updateBarcodeAfterSave($item_id,$barcode_item_data);
					
					/*-----------------------------------------------------------------------------*/

					
					$this->Item_taxes->save($items_taxes_data, $item_id);
					$item_str.=$item_id.':';
					
					// $this->upload($item_id);
					array_push($item_ids, $item_id);
					
					
				}
				else//failure
				{
					//echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
					//$item_data['name'],'item_id'=>-1));
					// var_dump($item_data);die();
				}
				$item_id=-1;
		}
		// var_dump($item_ids);die();
		$this->upload($item_ids);

		if ($is_new_template==1) {
			$this->add_new_template($item_data);
		}

		// var_dump($item_qty);
		// die();
		if($this->input->post('generate_barcode')){
			redirect('items/generate_barcodes/'.trim($item_str,':'));
		}else{
			if ($is_new_item==1) {
				redirect('items/view/-1');	
				
			}else{
				redirect('items/');	

			}
		}
		
	}
	function save_photo()
	{
		$productid = $this->input->post('product_id');
		$order = 1;
		if($_FILES['file'])
		{
			// echo "save photo";die();
			$this->load->library('upload');
	        $img_name = preg_replace('/[^a-zA-Z0-9.\']/', '_', $_FILES['file']['name']);
			$new_img_name = str_replace("'", '', $img_name);
			$this->upload->initialize($this->set_upload_options($productid,$new_img_name));
			 if ( ! $this->upload->do_upload('file')){
					$error = array('error' => $this->upload->display_errors());	
					echo $error['error'];
				}else
				{
					$this->creatthumb($productid,$new_img_name,$order);
				}
		}else
		{
			echo "no file";
		}
	}
	function delete_photo()
	{
		$name = $this->input->post('name');
		$productid = $this->input->post('product_id');
		$name = preg_replace('/[^a-zA-Z0-9.\']/', '_', $name);
		$target = $productid.'_'.$name;

		$img_path = FCPATH.'uploads/'.$target;
		$thumb_path = FCPATH.'uploads/thumb/'.$target;
		 if (file_exists($img_path)) {
	       $status = unlink($img_path);
	       $this->db->where(['item_id'=>$productid, 'imagename'=>$target])->delete('ospos_image_items');
	    }
	    if (file_exists($thumb_path)) {
	        unlink($thumb_path);
	    }
		echo json_encode(['success'=>$status]);
	}
	function part_exist($part_number){
		if ( preg_match('/\s/',$part_number) ){
			return false;
		}
		$part = $this->db->query("SELECT COUNT(*) as count FROM ospos_part WHERE part_number = '$part_number'")->row()->count;
		if ($part>0) {
			return false; //is exist
		}else{
			return true;
		}
	}
	function savepart($item_id){
		
		// $partdata = $this->db->query("SELECT * FROM ospos_part WHERE ")->row();


		$make_name_name = $this->item->make_name($this->input->post('make_id'))->row()->make_name;
		$model_name_name =$this->item->model_name($this->input->post('model_id'))->row()->model_name;
		$color_name = $this->item->color_name($this->input->post('color_id'))->row()->color_name;
		$khmer_name = $this->item->khmer_name($this->input->post('khmername_id'))->row()->khmername_name;
		$bran_name = $this->item->branch_name($this->input->post('branch_id'))->row()->branch_name;
		$partp_name = $this->item->part_placement_name($this->input->post('partplacement_id'))->row()->partplacement_name;

		$item_data = array(
		'part_name'=>$this->input->post('name'),
		'category_id'=>$this->input->post('category_id'),
		'part_number'=>$this->input->post('item_number'),
		'cost_price'=>$this->input->post('cost_price'),
		'unit_price'=>$this->input->post('unit_price'),
		'quantity'=>$this->input->post('quantity'),
		'fit'=>$this->input->post('fit'),
		'model_id'=>$this->input->post('model_id'),
		'year'=>$this->input->post('year'),
		'color_id'=>$this->input->post('color_id'),
		'codition_id'=>$this->input->post('condition_id'),
		'part_placement'=>$this->input->post('partplacement_id'),
		'brand_id'=>$this->input->post('branch_id'),
		'location_id'=>$this->input->post('location_id'),
		'make_id'=>$this->input->post('make_id'),
		'khmernames_id'=>$this->input->post('khmername_id'),
		'model_name'=>$model_name_name,
		'make_name'=>$make_name_name,
		'color_name'=>$color_name,
		'khmername_name'=>$khmer_name,
		'branch_name'=>$bran_name,
		'condition_name'=>$this->Condition->get_info($this->Item->get_info($item_id)->condition_id)->condition_name,
		'part_placementname'=>$partp_name,
		'part_name'=>$this->item->khmer_name($this->input->post('khmername_id'))->row()->english_name
		);
		$this->db->insert("ospos_part",$item_data);
	}
	// ================================new upload==========================
	function upload($productid)
	{       
		// var_dump($_FILES);die();
	    $this->load->library('upload');
	    // $orders=$this->input->post('order');
	    $updimg=$this->input->post('updimg');
		$arrid=$this->input->post('deleteimg');
		$arrid=trim($arrid,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=1; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_image_items where image_id='".$arr[$i]."'")->row();
				if(isset($row->imagename)){
					unlink('./uploads/'.$row->imagename);
					unlink('./uploads/thumb/'.$row->imagename);
					$this->db->where('image_id',$row->image_id)->delete('ospos_image_items');
				}
			}
		}
		// echo $arrid;
	 //    $this->unlinkpic($productid,$arrid);
	    $files = $_FILES;
	    $cpt = count($_FILES['userfile']['name']);

	    for ($j=0; $j < count($productid); $j++) { 
	    	for($i=0; $i<$cpt; $i++)
		    {         
		    	if(isset($updimg[$i]) && $updimg[$i]!=''){
		    		$this->updatepic($updimg[$i],$i);
		    	}  
		    	// echo $orders[$i];
		        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
		        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
		        $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

		        $img_name = preg_replace('/[^a-zA-Z0-9.\']/', '_', $_FILES['userfile']['name']);
				$new_img_name = str_replace("'", '', $img_name);

		        $this->upload->initialize($this->set_upload_options($productid[$j],$new_img_name));
		        // $this->upload->do_upload();
		        if ( ! $this->upload->do_upload()){
					$error = array('error' => $this->upload->display_errors());	
					// echo $error['error'];		
				}else{	
					// echo $_FILES['userfile']['name'];
					$this->creatthumb($productid[$j],$new_img_name,$i);
				}
		    }
	    }
	    
	}
	function updatepic($picid,$order){
		$this->db->where('image_id',$picid)->set('first_image',$order)->update('ospos_image_items');
	}
	function creatthumb($productid,$imagename,$order){
			$data = array('upload_data' => $this->upload->data());
		 	$config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = './uploads/thumb';
            $config2['maintain_ratio'] = false;
            $config2['create_thumb'] = "$productid".'_'."$imagename";
            $config2['thumb_marker'] = false;
            $config2['height'] = 190;
            $config2['width'] = 195;
            $this->load->library('image_lib');
            $this->image_lib->initialize($config2); 
            if ( ! $this->image_lib->resize()){
            	echo $this->image_lib->display_errors();
			}else{
				$this->saveimg($productid,$this->upload->file_name,$order,0);
			}
		
	}
	private function set_upload_options($productid,$imagename)
	{   
	    //upload an image options
	    if(!file_exists('./uploads/') || !file_exists('./uploads/')){
		    if(mkdir('./uploads/',0755,true)){
		        return true;
		    }
	    	if(mkdir('./uploads/thumb',0755,true)){
		                return true;
		    }
		}
	    $config = array();
	    $config['upload_path'] = './uploads/';
	    $config['allowed_types'] = 'gif|jpg|jpeg|png';
	    $config['max_size']      = '0';
	    $config['file_name']  	 = "$productid".'_'."$imagename";
		$config['overwrite']	 = true;
		$config['file_type']='image/png';

	    return $config;
	}

	function saveimg($productid,$imagename,$order,$tem_image){
		$img_name = preg_replace('/[^a-zA-Z0-9.\']/', '_', $imagename);
		$new_img_name = str_replace("'", '', $img_name);

		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		$count=$this->db->query("SELECT count(*) as count FROM ospos_image_items where item_id='$productid' AND imagename='$new_img_name'")->row()->count;
		if($count==0){
			$data=array('item_id'=>$productid,
						'imagename'=>$new_img_name,
						'tem_image'=>$tem_image,
						'first_image'=>$order);
			$this->db->insert('ospos_image_items',$data);
		}
	}
	function unlinkpic($stockid,$arrids){
		$arrid=trim($arrids,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_image_items where image_id='".$arr[$i]."'")->row();
				if(isset($row->pic_name)){
					// unlink('./uploads/'.$row->pic_name);
					// unlink('./uploads/thumb/'.$row->pic_name);
					$this->db->where('image_id',$row->image_id)->delete('ospos_image_items');
				}
			}
		}
	}

	// ============================================================================
	function comment(){
		echo 'look!!!';	
	}
	
	//Ramel Inventory Tracking
	function save_inventory($item_id=-1)
	{	
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		$inv_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_items'=>$item_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$this->input->post('trans_comment'),
			'trans_inventory'=>$this->input->post('newquantity')
		);
		$this->Inventory->insert($inv_data);
		
		//Update stock quantity
		$item_data = array(
		'quantity'=>$cur_item_info->quantity + $this->input->post('newquantity')
		);
		if($this->Item->save($item_data,$item_id))
		{			
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
			$cur_item_info->name,'item_id'=>$item_id));
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			$cur_item_info->name,'item_id'=>-1));
		}

	}//---------------------------------------------------------------------Ramel

	function bulk_update()
	{
		$items_to_update=$this->input->post('item_ids');
		$item_data = array();

		foreach($_POST as $key=>$value)
		{
			//This field is nullable, so treat it differently
			if ($key == 'supplier_id')
			{
				$item_data["$key"]=$value == '' ? null : $value;
			}
			elseif($value!='' and !(in_array($key, array('item_ids', 'tax_names', 'tax_percents'))))
			{
				$item_data["$key"]=$value;
			}
		}

		//Item data could be empty if tax information is being updated
		if(empty($item_data) || $this->Item->update_multiple($item_data,$items_to_update))
		{
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			$this->Item_taxes->save_multiple($items_taxes_data, $items_to_update);

			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_bulk_edit')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_updating_multiple')));
		}
	}

	function delete()
	{
		$this->permission_lib->checkPermission();
		$items_to_delete=$this->input->post('ids');

		if($this->Item->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_deleted').' '.
			count($items_to_delete).' '.$this->lang->line('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_cannot_be_deleted')));
		}
	}
	
	function excel()
	{
		$data = file_get_contents("import_items.csv");
		$name = 'import_items.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->load->view("items/excel_import", null);
	}

	function do_excel_import()
	{
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = $this->lang->line('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				
				$i=1;
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					$item_data = array(
					'name'			=>	$data[1],
					'description'	=>	$data[13],
					'location'		=>	$data[12],
					'make'			=>	$data[16],
					'category_id'	=>	$data[2],
					'cost_price'	=>	$data[4],
					'unit_price'	=>	$data[5],
					'quantity'		=>	$data[10],
					'reorder_level'	=>	$data[11],
					'supplier_id'	=>  $this->Supplier->exists($data[3]) ? $data[3] : null,
					'allow_alt_description'=> $data[14] != '' ? '1' : '0',
					'is_serialized'=>$data[15] != '' ? '1' : '0',
					'category'		=>	$data[17]
					);
					$item_number = $data[0];
					
					if ($item_number != "")
					{
						$item_data['item_number'] = $item_number;
					}
					
					if($this->Item->save($item_data)) 
					{
						$items_taxes_data = null;
						//tax 1
						if( is_numeric($data[7]) && $data[6]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[6], 'percent'=>$data[7] );
						}

						//tax 2
						if( is_numeric($data[9]) && $data[8]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[8], 'percent'=>$data[9] );
						}

						// save tax values
						if(count($items_taxes_data) > 0)
						{
							$this->Item_taxes->save($items_taxes_data, $item_data['item_id']);
						}
						
							$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
							$emp_info=$this->Employee->get_info($employee_id);
							$comment ='Qty CSV Imported';
							$excel_data = array
								(
								'trans_items'=>$item_data['item_id'],
								'trans_user'=>$employee_id,
								'trans_comment'=>$comment,
								'trans_inventory'=>$data[10]
								);
								$this->db->insert('inventory',$excel_data);
						//------------------------------------------------Ramel
					}
					else//insert or update item failure
					{
						$failCodes[] = $i;
					}
				}
				
				$i++;
			}
			else 
			{
				echo json_encode( array('success'=>false,'message'=>'Your upload file has no data or not in supported format.') );
				return;
			}
		}

		$success = true;
		if(count($failCodes) > 1)
		{
			$msg = "Most items imported. But some were not, here is list of their CODE (" .count($failCodes) ."): ".implode(", ", $failCodes);
			$success = false;
		}
		else
		{
			$msg = "Import items successful";
		}

		echo json_encode( array('success'=>$success,'message'=>$msg) );
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	
	public function combobox()
    {
        
        $name = $this->input->get('_name');
        $value = $this->input->get('_value');
         
        $this->load->model('item');
        
        echo json_encode( $this->item->get_dropdown($name, $value) );
    }
    
    /*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_partNumber()
	{
		$suggestions = $this->Item->get_partNumber_suggestions($this->input->post('q'));
		echo implode("\n",$suggestions);
	}
	
	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
	
	function get2FirstString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$end = $characters-$length;
		
		$firstString = substr($string ,0 ,$end);
		return $firstString;
		}
	}
	
	function generate_barcode_6digit($length) 
	{
	$consonants = '1234567890';

    for ($i = 0; $i < $length; $i++) 
    {
    	
    	$digit.= $consonants[(rand() % strlen($consonants))];
    	
    }
    return $digit;
}

function picupload($itemid)
    {
        //Load Model
        $this->load->model('process_image');

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '2048'; //2 meg


        $this->load->library('upload');

        foreach($_FILES as $key => $value)
        {
            if( ! empty($key['name']))
            {
                $this->upload->initialize($config);
        
                if ( ! $this->upload->do_upload($key))
                {
                    $errors[] = $this->upload->display_errors();
                    
                }    
                else
                {
                    $this->process_image->process_pic($itemid);

                }
             }
        
        }
        $data['success'] = 'Thank You, Files Upladed!';
    }
  
    
    
	function _example_output($output = null)
	{
		$this->load->view('items/upload_file',$output);	
		//return $output;
		
	}
	
	function example1()
	{
		$this->load->library('Image_CRUD');
		$image_crud = new Image_CRUD();
		
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('imagename');
		$image_crud->set_table('image_items');
		$image_crud->set_image_path('assets/uploads');
			
		$output = $image_crud->render();
		return $output;
		//$this->_example_output($output);
	}
	
	
	
		function getExtension($str) {
				$i = strrpos($str,".");
				if (!$i) { return ""; }
				$l = strlen($str) - $i;
				$ext = substr($str,$i+1,$l);
				return $ext;
			}
	
	
 	function uploadImage($itemid,$image_id){
 		
 		
 		for($i=0; $i<count($_FILES['txtnews_photo']['name']); $i++){
				$change_new="";
				define ("MAX_SIZE","1024");
				$errors_new=0;

			
				//if($_SERVER["REQUEST_METHOD"] == "POST"){
					$image_new = $_FILES['txtnews_photo']['name'][$i];
					$uploadedfile_new = $_FILES['txtnews_photo']['tmp_name'][$i];
					 
					$rand_new=rand(0000,9999);
					$replace_new = $rand_new;
					$subject_new = $image_new;
					$image_new=substr_replace($subject_new,$replace_new,0,-4);		
						
					if ($image_new) 
					{
						$filename_new = stripslashes($image_new);
						$extension_new = getExtension($filename_new);
						$extension_new = strtolower($extension_new);
						
						if (($extension_new != "jpg") && ($extension_new != "jpeg")) 
						{
							$change_new='<div class="msgdiv">Unknown Image extension </div> ';
							$errors_new=1;
						}
						else
						{
						
							$size_new=filesize($_FILES['txtnews_photo']['tmp_name'][$i]);
							if ($size_new > MAX_SIZE*2000000)
							{
								$change_new='<div class="msgdiv">You have exceeded the size limit!</div> ';
								$errors_new=1;
							}
							
							if($extension_new=="jpg" || $extension_new=="jpeg" )
							{
								$uploadedfile_new = $_FILES['txtnews_photo']['tmp_name'][$i];
								$src_new = imagecreatefromjpeg($uploadedfile_new);}
							else if($extension_new=="png")
							{
								$uploadedfile_new = $_FILES['txtnews_photo']['tmp_name'][$i];
								$src_new = imagecreatefrompng($uploadedfile_new);
							}
							else 
							{
								$src_new = imagecreatefromgif($uploadedfile_new);
							}
						
							//echo $scr;
							list($width_new,$height_new)=getimagesize($uploadedfile_new);
							$newheight_new=414;
							$newwidth_new=($width_new/$height_new)*$newheight_new;
							$tmp_new=imagecreatetruecolor($newwidth_new,$newheight_new);
					
							imagecopyresampled($tmp_new,$src_new,0,0,0,0,$newwidth_new,$newheight_new,$width_new,$height_new);
					
							$filename_new = ".uploads/".$image_new;
							
							
							imagejpeg($tmp_new,$filename_new,100);
							
							///////////////
							
							list($width_new1,$height_new1)=getimagesize($uploadedfile_new);
							$newheight_new1=105;
							$newwidth_new1=($width_new1/$height_new1)*$newheight_new1;
							$tmp_new1=imagecreatetruecolor($newwidth_new1,$newheight_new1);
					
							imagecopyresampled($tmp_new1,$src_new,0,0,0,0,$newwidth_new1,$newheight_new1,$width_new1,$height_new1);
					
							$filename_new1 = ".uploads/thum/".$image_new;
							
							
							imagejpeg($tmp_new1,$filename_new1,100);
							
							imagedestroy($tmp_new1);
							
							imagedestroy($src_new);
							imagedestroy($tmp_new);
						}
					}
								
				
				//}	
			
		$this->load->model('image_model');	
		$image_data = array(
		'item_id'=>$itemid,
		'imagename'=>$image_new
		);
				
		$this->image_model->uploadimage($image_data,$image_id);
 				}
 	}   

 	function do_uploadimage(){
 		
 		  $config['upload_path'] = './uploads';
          $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
          $config['max_size']  = '0';
          $config['max_width']  = '0';
          $config['max_height']  = '0';
          $this->load->library('upload', $config);


          $configThumb = array();
          $configThumb['image_library'] = 'gd2';
          $configThumb['source_image'] = '';
          $configThumb['create_thumb'] = TRUE;
          $configThumb['maintain_ratio'] = TRUE;

          $configThumb['width'] = 100;
          $configThumb['height'] = 120;
          $this->load->library('image_lib');

          for($i = 1; $i < 6; $i++) {
            $upload = $this->upload->do_upload('file'.$i);
            if($upload === FALSE) continue;
            $data = $this->upload->data();

            $uploadedFiles[$i] = $data;

            $imgName = $this->pictures_m->addPicture(array('listing_id' => $listing_id, 'ext' => $data['file_ext'], 'picture_name' => $this->input->post('file'.$i.'name')));

            if($data['is_image'] == 1) {
              $configThumb['source_image'] = $data['full_path'];
              $configThumb['new_image'] = $data['file_path'].$imgName.$data['file_ext'];
              $this->image_lib->initialize($configThumb);
              $this->image_lib->resize();
            }
    rename($data['full_path'], $data['file_path'].$imgName.$data['file_ext']);

          }
 		
 	}
 	function mobile_go(){
 		header('Content-Type: application/json');
 	
 		$itemnumber=$_REQUEST['itemvalue'];
 		
 		$data['item_info']=$this->Item->get_info_where_itemnumber($itemnumber);
 		$data['itemid']=$this->Item->get_info_where_itemnumber($itemnumber)->item_id;
		$data['item_tax_info']=$this->Item_taxes->get_info($data['itemid']);
			$makes = array();
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info_where_itemnumber($itemnumber)->make_id;
		
		$models = array();
	
		foreach($this->Model->get_all()->result_array() as $row)
		{
			
			
			$models[$row['model_id']] = array($row['model_name'],$row['make_id']);
			
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info_where_itemnumber($itemnumber)->model_id;
		
		$colors = array();
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info_where_itemnumber($itemnumber)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info_where_itemnumber($itemnumber)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info_where_itemnumber($itemnumber)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info_where_itemnumber($itemnumber)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
/*

*/
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info_where_itemnumber($itemnumber)->category_id;
		
		
			
		

	
			
		
		
		echo json_encode($data);
	
 	}
 	function go(){
 		header('Content-Type: application/json');
 	
 		$itemnumber=-1;
 		//if ($this->input->server('REQUEST_METHOD') === 'POST')
		//{
			
 		//$itemnumber=$this->input->post('item_number');
 		//$itemnumber=$this->input->post('itemvalue');
 		$itemnumber=$_REQUEST['itemvalue'];
 		
 		$data['item_info']=$this->Item->get_info_where_itemnumber($itemnumber);
 		$data['itemid']=$this->Item->get_info_where_itemnumber($itemnumber)->item_id;
		$data['item_tax_info']=$this->Item_taxes->get_info($data['itemid']);
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info_where_itemnumber($itemnumber)->supplier_id;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['location_id']] = $row['location_name'];
		}		
		$data['locations']=$locations;
		$data['selected_location'] = $this->Item->get_info_where_itemnumber($itemnumber)->location_id;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info_where_itemnumber($itemnumber)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info_where_itemnumber($itemnumber)->model_id;
		
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info_where_itemnumber($itemnumber)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info_where_itemnumber($itemnumber)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info_where_itemnumber($itemnumber)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info_where_itemnumber($itemnumber)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info_where_itemnumber($itemnumber)->category_id;

		$khmernames = array('' => $this->lang->line('items_none'));
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['khmername_name'];
		}		
		$data['khmernames']=$khmernames;
		$data['selected_khmername'] = $this->Item->get_info_where_itemnumber($itemnumber)->khmername_id;
		
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		
		
			
		//}

		/*Grade of Items*/
		
		$grade = array('' => $this->lang->line('items_none'));
		
		foreach($this->Item->get_all_grade()->result_array() as $row)
		{
			$grade[$row['grad_id']] = $row['grad_name'];
		}	
			
		
		$data['grade']=$grade;
		$data['selected_grade'] = $this->Item->get_info($data['itemid'])->grad_id;
		
		/*get unit price and cost price where grade_id*/
		$gradeA=$this->Item->get_unitPrice_costPrice(1)->row_array();
		$data['unitpriceA']=$gradeA['unit_price'];
		$data['costpriceA']=$gradeA['cost_price'];
		
		$gradeB=$this->Item->get_unitPrice_costPrice(2)->row_array();
		$data['unitpriceB']=$gradeB['unit_price'];
		$data['costpriceB']=$gradeB['cost_price'];
		
		$gradeC=$this->Item->get_unitPrice_costPrice(3)->row_array();
		$data['unitpriceC']=$gradeC['unit_price'];
		$data['costpriceC']=$gradeC['cost_price'];
		
		
		$data['image_name']=$this->Item->get_image_source($itemnumber)->result_array();
		//$data['itemid']='';
		//$this->load->view("items/form_insert_new_item",$data);
		echo json_encode($data);
	
 	}
 	function test(){
 		$itemnumber=$_REQUEST['itemvalue'];
 		
 		$data=array();
 		$row=$this->db->query("SELECT * FROM ospos_part WHERE part_number='$itemnumber' LIMIT 1")->row();
 		if ($itemnumber=='') {
 			$row=0;
 		}
 		if(count($row)>0){
 			$data['item_info']=$row;
 			$img_table='';
 			 $ma=1;
             $img_path=base_url('assets/site/image/no_img.png');
 			 foreach ($this->tem->getimage($row->partid) as $img) { 
                        if($ma<$img->first_image)
                            $ma=$img->first_image;
                        if(file_exists(FCPATH."/uploads/thumb/".$img->image_name)){
                            $img_paths=base_url("/uploads/thumb/".$img->image_name);
                            $img_table.="<div style='width:400px;' id='img_row'>
                                <div class='img' style='width:130px; float:left'> 
                                    <!--<img onclick='remove_img(event);' rel='' class='remove_img' src='".base_url('assets/site/image/remove.png')."'/>-->
                                    <img id='imgpreview' src='".$img_paths."' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'>
                                    <input id='uploadImage' accept='image/*' type='file' class='uploadimg ext' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
                                    <input type='text' class='hide' value='".$img->image_name."'  name='tem_img[]' id='tem_img'/>
                                </div>
                                <div class='txt_order' style='width:130px; float:left; margin-left:30px; margin-top:18px;'>
                                    <label class='col-lg-1 control-label'style='margin-top:15px !important;' >Order</label>
                                    <div class='col-lg-4' style='margin-top:15px !important;'> 
                                        <div class='col-md-12'>
                                            <input type='text' disabled onkeypress='return isNumberKey(event);' class='form-control input-sm order'  name='order[]' id='order' rel='' value='".$img->first_image."'>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <p style='clear:both;'></p> ";
                    }
            }
            $img_table.="<div style='width:400px;' id='img_row'>
		                    <div class='img' style='width:130px; float:left'> 
		                        <img onclick='remove_img(event);' class='hide remove_img' src='".base_url('assets/site/image/remove.png')."'/>
		                        <img  onclick='choosimg(event);' id='imgpreview' src='".$img_path."' style='width:100%; border:solid 1px #CCCCCC; padding:3px;'/>
		                        <input id='uploadImage' accept='image/*' type='file' class='uploadimg bl' name='userfile[]' onchange='PreviewImage(event);' style='visibility:hidden; display:none' />
		                    </div>
		                    <div class='txt_order' style='width:130px; float:left; margin-left:30px; margin-top:18px;'>
		                        <label class='col-lg-1 control-label' >Order</label>
		                        <div class=' col-lg-4'> 
		                            <div class='col-md-12'>
		                                <input type='text'  class='form-control input-sm' id='order'  name='order[]' value='".($ma+1)."'>
		                            </div> 
		                        </div>
		                    </div>
		                </div> 
		                <p style='clear:both;'></p>";
 			$data['img_table']=$img_table;
 			$data['khmername_img'] = $this->db->query("SELECT k.khmername_id,k.khmername_name,k.english_name,ki.imagename FROM ospos_khmernames k
														LEFT JOIN ospos_khmername_images ki ON k.khmername_id=ki.khmername_id
														WHERE k.deleted=0")->result();
 		}else{
 			$data["no"]="no";
 		}
 		header("Content-type:text/x-json");
		echo json_encode($data);

 	}
 	
 	function addImage($item_id){
 		
 	//if($_SERVER["REQUEST_METHOD"] == "POST"){	
 		
 		for($j=0;$j<count($_FILES['path']['name']);$j++){	
			if ($_FILES['path']['name'][$j]!=""){
				$image_directory = "./uploads/";
				$big_directory = "./uploads/";
				$allowed_ext = "jpg, JPG, gif, GIF , bmp, BMP, jpeg, JPEG , png, PNG"; 
				$extension = pathinfo($_FILES['path']['name'][$j]);
				$extension = $extension["extension"];
				$allowed_paths = explode(", ", $allowed_ext);
				for($i = 0; $i < count($allowed_paths); $i++) {
					if ($allowed_paths[$i] == $extension) {
						 $ok="1";
					}
				}
				if($ok=="1"){
						$concatenate_str1 = time();
						if(is_uploaded_file($_FILES['path']['tmp_name'][$j])){	
								$picture_name=$_FILES['path']['name'][$j];	
								move_uploaded_file($_FILES['path']['tmp_name'][$j],$image_directory.$concatenate_str1.$picture_name);
							  $imageurl=$concatenate_str1.$picture_name;
						}
		if($j==0){//set first image boolean
		      $file_data=array('item_id'=>$item_id,'imagename'=>$concatenate_str1.$picture_name,'first_image'=>true);
		}else{
		      $file_data=array('item_id'=>$item_id,'imagename'=>$concatenate_str1.$picture_name,'first_image'=>false);
                }
 		$this->load->database();
	    $ret=$this->db->insert('image_items',$file_data);		}
					
				}		   
 		}
 	//}
 	
 }
 	
 	function doUploadFileImage(){
	 	$config['upload_path'] = './uploads/';      
	    $config['allowed_types'] = 'gif|jpg|png|doc|txt|pdf';
	    $config['max_size'] = '5000';
	    $config['max_width']  = '2500';
	    $config['max_height']  = '2500';
	    $config['remove_spaces']= 'true';
	
	    $this->load->library('upload', $config);
	    $this->load->library('image_lib');  
	
	    
	    for($i=0; $i<count($_FILES['txtnews_photo']['name']); $i++){
	    
	    
	    $data= array('txtnews_photo' => $this->upload->do_upload()); 
	    
	    //DEFINE POSTED FILE INTO VARIABLE
	    $name= $data['txtnews_photo']['name'][$i];
	    $tmpname= $data['txtnews_photo']['tmpname'][$i];
	    $type= $data['txtnews_photo']['type'][$i];
	    $size= $data['txtnews_photo']['size'][$i];
	    
	    $file_data=array('imagename'=>$name);
	    
	 	//if ( ! $this->upload->do_upload())
	   // {
	    //    $error = array('error' => $this->upload->display_errors());
	
	        //$this->load->view('form_insert_new_item', $error);
	   // }   
	   // else
	   // {
	        $data = array('txtnews_photo' => $this->upload->data());
	        $this->load->database();
	        $ret=$this->db->insert('image_items',$file_data);
	        unlink($tmpname);
	        
	    //}
 		
 	}
 	}
 	
 	function deleteImage(){
 		$value=true;
 		$imageid=$this->input->post('imageid');
 		if($imageid){
 			$this->load->model('item');
 			$image_name=$this->Item->getImageName($imageid);
 		$delete=$this->Item->delete_Image_item($imageid);
 		if($delete){
 			 unlink("./uploads/".$image_name);
 		}
 		
 		}
 		
 		
 	}
	
	
	//New 02-08-2012
	function view_sale_item($item_id=-1)
	{

		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$suppliers = array('' => $this->lang->line('items_none'));
		
		
		
		/*Vin Number*/
		$vinNumber = array('' => $this->lang->line('items_none'));
		foreach($this->Vinnumber->get_all_vinNumber()->result_array() as $row)
		{
			$vinNumber[$row['vinnumber_id']] = $row['vinnumber_name'];
		}		
		$data['vinnumber']=$vinNumber;
		$data['selected_vinnumber'] ='';
		
		/***********/
		
		
		
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['location_id']] = $row['location_name'];
		}		
		$data['locations']=$locations;
		$data['selected_location'] = $this->Item->get_info($item_id)->location_id;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info($item_id)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info($item_id)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info($item_id)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info($item_id)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info($item_id)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info($item_id)->category_id;

		$khmernames = array('' => $this->lang->line('items_none'));
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['khmername_name'];
		}		
		$data['khmernames']=$khmernames;
		$data['selected_khmername'] = $this->Item->get_info($item_id)->khmername_id;
		
		
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		
		
		/*Grade of Items*/
		
		$grade = array('' => $this->lang->line('items_none'));
		
		foreach($this->Item->get_all_grade()->result_array() as $row)
		{
			$grade[$row['grad_id']] = $row['grad_name'];
		}	
			
		$data['grade']=$grade;
		$data['selected_grade'] = $this->Item->get_info($item_id)->grad_id;
		
		/*get unit price and cost price where grade_id*/
		$gradeA=$this->Item->get_unitPrice_costPrice(1)->row_array();
		$data['unitpriceA']=$gradeA['unit_price'];
		$data['costpriceA']=$gradeA['cost_price'];
		
		$gradeB=$this->Item->get_unitPrice_costPrice(2)->row_array();
		$data['unitpriceB']=$gradeB['unit_price'];
		$data['costpriceB']=$gradeB['cost_price'];
		
		$gradeC=$this->Item->get_unitPrice_costPrice(3)->row_array();
		$data['unitpriceC']=$gradeC['unit_price'];
		$data['costpriceC']=$gradeC['cost_price'];
		
		/*==-========================================================================*/

		$data['image_name']=$this->Item->get_image_source($item_id)->result_array();
		
	
		$this->load->view("items/form_on_sale",$data);
	
	}
	
	function add_item_on_sale($id)
	{
		
		$data=array();
		$mode = $this->sale_lib->get_mode();
		$item_id_or_number_or_item_kit_or_receipt = $id;
		$quantity = $mode=="sale" ? 1:-1;
		if($this->sale_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt) && $mode=='return')
		{
			$this->sale_lib->return_entire_sale($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt))
		{
			$this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif(!$this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$quantity))
		{
			$data['error']=$this->lang->line('sales_unable_to_add_item');
		}
		
		/*
		if($this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt))
		{
			$data['warning'] = $this->lang->line('sales_quantity_less_than_zero');
		}
		$this->_reload($data);
		*/
	}
	
	function save_item_on_sale($item_id=-1)
	{
		
		$this->load->helper('url');
		$this->load->model('item');
		$name=array();
		$makeid=$this->input->post('make_id');
		$modelid=$this->input->post('model_id');
		$year_input=$this->input->post('year');
		$grad_id=$this->input->post('grade_id');
		
		$make_name=$this->item->make_name($makeid)->row_array();
		$name['getname']=$make_name['make_name'];
		
		$modelname=$this->item->model_name($modelid)->row_array();
		$name['modelname']=$modelname['model_name'];
		
		$make_name=$this->get2FirstString($name['getname']);
		$year=$this->get2lastString($year_input);
		$model_name=$this->get2FirstString($name['modelname']);

		$item_data = array(
		'name'=>$this->input->post('name'),
		'description'=>$this->input->post('description'),
		'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
		'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
		'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
		'cost_price'=>$this->input->post('cost_price'),
		'unit_price'=>$this->input->post('unit_price'),
		'quantity'=>$this->input->post('quantity'),
		'reorder_level'=>$this->input->post('reorder_level'),
		'location'=>$this->input->post('location'),
		'fit'=>$this->input->post('fit'),
		'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
		'year'=>$this->input->post('year'),
		'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
		'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
		'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
		'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
		'allow_alt_description'=>$this->input->post('allow_alt_description'),
		'is_serialized'=>$this->input->post('is_serialized'),
		'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
		'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
		'category'=>$this->input->post('vinnumber_id'),
		'barcode'=>$barcode,
		'grad_id'=>$grad_id,
		'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id'),
		'ishead'=>1
		);
		
		
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		
		if($this->Item->save($item_data,$item_id))
		{
			
			//$itemid=$this->db->insert_id();
			
			//New item
			if($item_id==-1)
			{
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_adding').' '.
				//$item_data['name'],'item_id'=>$item_data['item_id']));
				$item_id = $item_data['item_id'];
			}
			else //previous item
			{
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
				//$item_data['name'],'item_id'=>$item_id));
			}
			
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item_id,
				'trans_user'=>$employee_id,
				'trans_comment'=>$this->lang->line('items_manually_editing_of_quantity'),
				'trans_inventory'=>$cur_item_info ? $this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity')
			);
			
			$this->Inventory->insert($inv_data);
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			
						/*--------------------update barcode after save item sucessful-----------------------------------------*/
			
			$digit_padding=str_pad($item_id,6,"0",STR_PAD_LEFT);
			//$barcode=$year.$make_name.$model_name.$digit_padding;
			$vinIfo = $this->Vinnumber->get_Vin_Info($this->input->post('vinnumber_id'));
			$vinName=$vinIfo->vinnumber_name;
			$barcode_item_data = array(
				'name'=>$this->input->post('name'),
				'description'=>$this->input->post('description'),
				'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
				'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
				'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
				'cost_price'=>$this->input->post('cost_price'),
				'unit_price'=>$this->input->post('unit_price'),
				'quantity'=>$this->input->post('quantity'),
				'reorder_level'=>$this->input->post('reorder_level'),
				'location'=>$this->input->post('location'),
				'fit'=>$this->input->post('fit'),
				'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
				'year'=>$this->input->post('year'),
				'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
				'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
				'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
				'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
				'allow_alt_description'=>$this->input->post('allow_alt_description'),
				'is_serialized'=>$this->input->post('is_serialized'),
				'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
				'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
				//'category'=>$this->input->post('category'),
				'category'=>$vinName,
				'barcode'=>$vinName,
				'grad_id'=>$grad_id,
				'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id'),
				'ishead'=>1
				);
			$this->Item->updateBarcodeAfterSave($item_id,$barcode_item_data);
			
			/*-----------------------------------------------------------------------------*/

			
			$this->Item_taxes->save($items_taxes_data, $item_id);
			
			
			$this->addImage($item_id);
			
			/*
			if($this->input->post('generate_barcode')){
			redirect('items/generate_barcodes/'.$item_id.'/'.$this->input->post("quantity").'');
			}
			*/
			
			$this->add_item_on_sale($item_id);
			redirect('sales','refresh');

		}
	
	}

	
	function make_model_year_filter()
	{
		$make_filter=$this->input->post('make_id');
		$model_fitler=$this->input->post('model_id');
		$year_filter=$this->input->post('year');

		$data['search_section_state']=$this->input->post('search_section_state');
		$data['make_filter']=$this->input->post('make_id');
		$data['model_fitler']=$this->input->post('model_id');
		$data['year_filter']=$this->input->post('year');
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_items_manage_table($this->Item->get_all_filtered_make_model_year($make_filter,$model_fitler,$year_filter),$this);
		
		/*make, model, year combo box for searching*/
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info_where_itemnumber($itemnumber)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		/******************************close******************************************/
		
		$this->load->view('items/manage',$data);
	}
	
	
	function refresh_search(){
		
		if(isset($_POST['search'])){
			$search_filter = $this->input->post('searchquery');
			echo 'search = '.$search_filter;
			
		}
		$this->load->view('items/manage',$data);
	}
	
	
	function mobile_search()
		{
		
		$data_rows['controller_name']=strtolower(get_class());
			$search=$this->input->get('searchquery');
			$cat = $this->input->get('cat');
			$model = $this->input->get('model');
			$make = $this->input->get('make');
			$color = $this->input->get('color');
			$brand = $this->input->get('brand');
			$condition = $this->input->get('condition');
			$hasstock=$this->input->get('hasstock');
			if($cat > 0 || $model > 0 || $make >0 || $color >0 || $brand >0 || $condition >0){
				if($search=="(null)")
					$search=null;
				$rows=$this->Item->mobile_fullTextSearch2($search,$cat,$model,$make,$color,$brand,$condition,$hasstock);
			}
			else{
				$rows=$this->Item->mobile_fullTextSearch($search,$hasstock);
			}
		
		if($rows->num_rows()==1000){
			$result=array('result_array'=>array(),'error_message'=>"Too many results!");
			echo json_encode($result);		
		}else{
			
	                foreach($rows->result_array() as $row)
					{
						//$categorys[$row['category_id']] = $row['imagename'];
					}		
					//$data['categorys']=$categorys;

	                	echo json_encode($rows);	
		}
	}
	
	function gemodel_pos(){
		$make_id=$this->input->post('make');
		$model_id=$this->input->post('model');
		$color_id=$this->input->post('color');
		$year=$this->input->post('year');

		// $ex_color_id=$this->input->post('ex_color');
		// $name=$this->input->post('name');
		$year_from =$this->input->post('year_from');
		$year_to = $this->input->post('year_to');
		$body_id = $this->input->post('body');
		$search = $this->input->post('search');
		$engine_id = $this->input->post('engine');
		$fuel_id = $this->input->post('fuel');
		$trans_id = $this->input->post('trans');
		$trim_id = $this->input->post('trim');
		$vinnumber =$this->input->post('vin');
		$place = $this->input->post('ppl');
		$location = $this->input->post('loc');
		$s_all = $this->input->post('search_all');
		$where='';
		$arr=array();
		$sql=$this->session->userdata('query_result');
		// echo $sql;die();
		$this->db->query("DROP TEMPORARY TABLE IF EXISTS tmp_result");
        $this->db->query("CREATE TEMPORARY TABLE tmp_result $sql");


		$vin_check='';
		$vin_data='';
		$vin_data.="<li class='sf_filter' style='margin-left:-15px;'>
						<input type='text' name='v' id='vin_select' onchange=search(1,event,0,'head','') value='$vinnumber'>
					</li>";
		$where_makes='';
		$where_models='';
		$where_vins='';
		$where_years='';
		$where_yearfs='';
		$where_cols='';
		$where_bodys='';
		$where_engines='';
		$where_fuels='';
		$where_transs='';
		$where_trims='';
		$where_place='';
		$where_location='';
		$where_s_all = "";
		$where_s = "";

		if($search!='') {
			$where_s .= "AND (i.name LIKE '%$search%'
												OR i.item_number LIKE '%$search%'
												OR i.barcode LIKE '%$search%'
												OR i.make_name LIKE '%$search%' 
												OR i.model_name LIKE '%$search%' 
												OR i.color_name LIKE '%$search%'
												OR i.unit_price LIKE '%$search%'
												OR i.khmer_name LIKE '%$search%'
												OR i.branch_name LIKE '%$search%'
												OR i.year LIKE '%$search%' 
												OR i.category LIKE '%$search%'
												OR i.partplacement_name LIKE '%$search%'
												OR i.vinnumber_id LIKE '%$search%'
												OR i.cost_price LIKE '%$search%'
											)";
		}
		// if ($s_all!='') {
		// 	$sql_mode = '';
		// 	if (strpos($s_all,' ')) {
		// 		$sql_mode = 'NATURAL LANGUAGE';
		// 	}else{
		// 		$sql_mode = 'BOOLEAN';
		// 	}
		// 	$where_s_all .= "AND (
		// 							MATCH(`name`,`year`,model_name,make_name,color_name,branch_name,partplacement_name,item_number,category,barcode) AGAINST('$s_all' IN $sql_mode MODE)
		// 						)";
		// }
		$where_s_all = '';
		if ($s_all) {
			$ars = explode(' ', $s_all);
			for ($i=0; $i < count($ars); $i++) { 
				$where_s_all.= " AND(
									i.barcode LIKE '%$ars[$i]%' OR
									i.item_number LIKE '%$ars[$i]%' OR
									i.name LIKE '%$ars[$i]%' OR
									i.make_name LIKE '%$ars[$i]%' OR
									i.model_name LIKE '%$ars[$i]%' OR
									i.year LIKE '%$ars[$i]%' OR
									i.color_name LIKE '%$ars[$i]%' OR
									i.category LIKE '%$ars[$i]%'
									)";
			}

			// $where.= $where_s_all;
			
		}
		if($make_id!=''){
            $where_makes.=" AND i.make_id='".$make_id."'";
        }elseif ($make_id=='other') {
        	$where_makes.=" AND i.make_id='' OR i.make_id=0 OR i.make_id IS NULL";

        }
        if($model_id!=''){
            $where_models.=" AND i.model_id='".$model_id."'";

        }elseif ($model_id=='other') {
        	$where_models.=" AND i.model_id='' OR i.model_id=0 OR i.model_id IS NULL";
        }
        // if ($vinnumber!='') {
        // 	$where_vins.=" AND i.category='".$vinnumber."'";

        // }
        // elseif($vin=='other'){
        // 	$where_vins.=" AND i.category='' OR i.category=0 OR i.category IS NULL";

        // }
        if($vinnumber!=''){
			$vin=$vinnumber;
			if ($vin!='other') {
				$re.="$vin";
				$where_vins.=" AND i.vinnumber_id='".$_POST['vin']."'";
			}
			else{
				$where_vins.=" AND (i.category='' OR i.category='0' OR i.category IS NULL)";
			}
		}
		if($location!='' && $location!='_'){
            $arrloc=explode('_', $location);
            $where_loc=' AND(';
            for ($i=0; $i < count($arrloc) ; $i++) { 
            	if ($arrloc[$i]!='' && $arrloc[$i]!='_') {
            		
            		if ($arrloc[$i]=='other') {
            			$where_loc.=" i.location_id='' OR i.location_id=0 OR i.location_id IS NULL";
            		}else{
                    	$where_loc.="i.location_id='".$arrloc[$i]."'";
            		}
                    if($i<count($arrloc)-2)
                        $where_loc.=' OR ';
                }
                
            }
            $where_loc.=')';
            // $s_year=$_GET['y'];
            $where_location.=$where_loc;

        }
        if($year!='' && $year!='_'){
            $arryear=explode('_', $year);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
            	if ($arryear[$i]!='' && $arryear[$i]!='_') {
            		
            		if ($arryear[$i]=='other') {
            			$where_year.=" i.year='' OR i.year=0 OR i.year IS NULL";
            		}else{
                    	$where_year.="i.year='".$arryear[$i]."'";
            		}
                    if($i<count($arryear)-2)
                        $where_year.=' OR ';
                }
                
            }
            $where_year.=')';
            // $s_year=$_GET['y'];
            $where_years.=$where_year;

        }
        if ($yf!='' && $yt!='') {
        	$where_yearfs.=" AND i.year BETWEEN $yf AND $yt";
        }
        if($color_id!='' && $color_id!='_'){
            $arrcol=explode('_', $color_id);
            $where_color=' AND(';
            for ($i=0; $i < count($arrcol) ; $i++) { 
            	if ($arrcol[$i]!='' && $arrcol[$i]!='_') {
            		# code...
            		if ($arrcol[$i]=='other') {
            			$where_color.="i.color_id='' OR i.color_id=0 OR i.color_id IS NULL";
            		}else{
                    	$where_color.="i.color_id='".$arrcol[$i]."'";
            		}
                    if($i<count($arrcol)-2)
                        $where_color.=' OR ';
            	}
            }
            $where_color.=')';
            // $s_year=$_GET['y'];
            $where_cols.=$where_color;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        // echo $where_cols;die();
        if($body_id!='' && $body_id!='_'){
            $bo_arr=explode('_', $body_id);
            $where_body=' AND(';
            for ($i=0; $i < count($bo_arr) ; $i++) { 
            	if ($bo_arr[$i]!='' && $bo_arr[$i]!='_') {
            		# code...
            		if ($bo_arr[$i]=='other') {
            			$where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
            		}else{
                    $where_body.="i.body='".$bo_arr[$i]."'";

            		}
                    if($i<count($bo_arr)-2)
                        $where_body.=' OR ';
            	}
                
            }
            $where_body.=')';
            // $s_year=$_GET['y'];
            $where_bodys.=$where_body;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($engine_id!='' && $engine_id!='_'){
            $en_arr=explode('_', $engine_id);
            $where_engine=' AND(';
            for ($i=0; $i < count($en_arr) ; $i++) { 
            	if ($en_arr[$i]!='' && $en_arr!='_') {
            		# code...
            		if ($en_arr[$i]=='other') {
            			$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
            		}else{
                    $where_engine.="i.engine='".$en_arr[$i]."'";

            		}
                    if($i<count($en_arr)-2)
                        $where_engine.=' OR ';
            	}
                
            }
            $where_engine.=')';
            // $s_year=$_GET['y'];
            $where_engines.=$where_engine;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($fuel_id!='' && $fuel_id!='_'){
            $fuel_arr=explode('_', $fuel_id);
            $where_fuel=' AND(';
            for ($i=0; $i < count($fuel_arr) ; $i++) { 
            	if ($fuel_arr[$i]!='' && $fuel_arr[$i]!='_') {
            		# code...
           			if ($fuel_arr[$i]=='other') {
           				$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
           			}else{
                    $where_fuel.="i.fuel='".$fuel_arr[$i]."'";

           			}
                    if($i<count($fuel_arr)-2)
                        $where_fuel.=' OR ';
            	}
                
            }
            $where_fuel.=')';
            // $s_year=$_GET['y'];
            $where_fuels.=$where_fuel;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trans_id!='' && $trans_id!='_'){
            $trans_arr=explode('_', $trans_id);
            $where_trans=' AND(';
            for ($i=0; $i < count($trans_arr) ; $i++) { 
            	if ($trans_arr[$i]!='' && $trans_arr[$i]!='_') {
            		# code...
           			if ($trans_arr[$i]=='other') {
           				$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";
           			}else{
                    $where_trans.="i.transmission='".$trans_arr[$i]."'";

           			}
                    if($i<count($trans_arr)-2)
                        $where_trans.=' OR ';
            	}
                
            }
            $where_trans.=')';
            // $s_year=$_GET['y'];
            $where_transs.=$where_trans;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($trim_id!='' && $trim_id!='_'){
            $trim_arr=explode('_', $trim_id);
            $where_trim=' AND(';
            for ($i=0; $i < count($trim_arr) ; $i++) {
            	if ($trim_arr[$i]!='' && $trim_arr[$i]!='_') {
            	 	# code...
           			if ($trim_arr[$i]=='other') {
           				$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
           			}else{

                    $where_trim.="i.trim='".$trim_arr[$i]."'";
           			}
                    if($i<count($trim_arr)-2)
                        $where_trim.=' OR ';
            	 } 
                
            }
            $where_trim.=')';
            // $s_year=$_GET['y'];
            $where_trims.=$where_trim;
            // $where.=" AND i.color_id='".$color_id."'";
        }
        if($place!='' && $place!='_'){
			$arrpart=explode('_', trim($place,'_'));
            $where_part=' AND(';
            $re.=' Partplacement:<b>';
            for ($i=0; $i < count($arrpart) ; $i++) { 
                     if($arrpart[$i]!='_' && $arrpart[$i]!=''){
                     	if ($arrpart[$i]=='other') {
                     		$re.='Other';
                     		$where_part.="i.partplacement_id='' OR i.partplacement_id=0 OR i.partplacement_id IS NULL";
                     	}else{
							$where_part.="i.partplacement_id='".$arrpart[$i]."'";
                     	}
						if (count($arrpart)-1>$i) {
							$where_part.=" OR ";
							$re.=', ';
						}
		           	}
            }
            $re.='</b>';
            $where_part.=')';
            $where_place.=$where_part;
		}




		$make_check='';
		$make_data='';
		if ($make_id=='') $make_check='checked';
		$make_data.="<li class='sf_filter'><input type='text' class='sf_input' f='make' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$make_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_make();' id='clear_make_btn'>Clear</li>";
		$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','');  name='ckmake' class='ckmake' $make_check value=''> All</label></li>";
		$make=$this->db->query("SELECT make_name,make_id FROM ospos_makes WHERE deleted=0 ORDER BY make_name ASC")->result();
		$count=$this->item->getnumsearch('make_id',$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		foreach ($make as $m) {
			$sele='';
			if($make_id==$m->make_id)
				$sele='checked';
			if($count[$m->make_id]>0)
				$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','$m->make_id') name='ckmake' $sele class='ckmake' value='$m->make_id'/> $m->make_name (".$count[$m->make_id].")</label></li>";
		}		
		$make_other=$this->item->getnumsearch_other('make_id',$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$make_other_check='';
		if ($make_id=='other') {
			$make_other_check='checked';
		}
		if ($make_other>0) {
			$make_data.="<li><label><input type='radio' onclick=search(1,event,1,'make','other')  name='ckmake' class='ckmake' $make_other_check value='other'> OTHER($make_other)</label></li>";
		}

		$model_check='';
		$modeldata='';
		if($model_id=='') $model_check='checked';
		$modeldata.="<li class='sf_filter'><input type='text' class='sf_input' f='model' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$modeldata .="<li style='float:right;cursor:pointer;color:#21759B' onclick='clear_model();' class='clear_model_btn'>Clear</li>";
		$modeldata.="<li><label><input type='radio' onclick=search(1,event,0,'model','')  name='ckmodel' class='ckmodel' $model_check value=''> All</label></li>";
		$wheremodel='';
		if($make_id!='')
			$wheremodel=" AND make_id='$make_id'";
		$data=$this->db->query("SELECT model_id,model_name FROM ospos_models WHERE deleted='0' {$wheremodel} ORDER BY model_name")->result();
		$count=$this->item->getnumsearch('model_id',$where_makes.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		foreach ($data as $mo) {
			$sele='';
			if($model_id==$mo->model_id)
					$sele='checked';
			if($count[$mo->model_id]>0)
				$modeldata.="<li rel='$mo->make_id'><label><input $sele onclick=search(1,event,0,'model','$mo->model_id') type='radio' name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name(".$count[$mo->model_id].")</label></li>";

		}
		$model_other=$this->item->getnumsearch_other('model_id',$where_makes.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$model_other_check='';
		if ($model_id=='other') {
			$model_other_check='checked';
		}
		if ($model_other>0) {
			$model_data.="<li><label><input type='radio' onclick=search(1,event,0,'model','other')  name='ckmake' class='ckmake' $model_other_check value='other'> OTHER($model_other)</label></li>";
			
		}
		
		$yeararr=explode('_', $year);
		$arrofyear=array();
		for ($i=0; $i <count($yeararr) ; $i++) { 
			$arrofyear[$yeararr[$i]]=$yeararr[$i];
		}

		$year_check='';
		if($year=='' || $year=='_'){
			$year_check='checked';
		}
		$yeardata='';

		
		if ($year_check!='') {
			$year_from=$year_from;
			$year_to = $year_to;
		}else{
			$year_from='';
			$year_to='';
		}
		$yeardata.="
		<div align='center' style='border-bottom:1px solid #21759B;padding-bottom:10px;margin-bottom:5px;'>

			<input style='width:40px;' maxlength='4' class='year_ft' id='year_from' type='text' placeholder='year' onkeyup='chkYear();' value='$year_from'>
			To
			<input style='width:40px;' maxlength='4' class='year_ft' id='year_to' type='text' placeholder='year' value='$year_to'>
			<button onclick='yearft();' class='btn btn-primary'>Go</button>
		</div>";
		$yeardata.="<li class='sf_filter'><input type='text' class='sf_input' f='year' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$yeardata.="<li style='float:right;cursor:pointer;color:#21759B' id='clear_year_btn' onclick=clear_year('');>Clear</li>";
		$yeardata.="<li><label><input type='checkbox' onclick=search(1,event,0,'year','') $year_check name='ckyear' class='ckyear' value=''> All</label></li>";
		$count=$this->item->getnumsearch('year',$where_makes.$where_models.$where_vins.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		// var_dump($count);
		for($j=date('Y');$j>=1980;$j--){
				$sele='';

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count[$j]>0)
					$yeardata.= "<li><label><input onclick=search(1,event,0,'year','$j') type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j (".$count[$j].")</label></li>";# code...

		}
		$year_other=$this->item->getnumsearch_other('year',$where_makes.$where_models.$where_vins.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$year_other_check='';
		if (isset($arrofyear['other'])) {
			$year_other_check='checked';
			
		}
		if ($year_other>0) {
			$yeardata.="<li><label><input type='checkbox' onclick=search(1,event,0,'year','other') $year_other_check name='ckyear' class='ckyear' value='other'> OTHER($year_other)</label></li>";
			
		}

		$colorarr=explode('_', $color_id);
		$arrofcolor=array();
		for ($i=0; $i <count($colorarr) ; $i++) { 
			$arrofcolor[$colorarr[$i]]=$colorarr[$i];
		}
		$color_check='';
		if($color_id=='' || $color_id=='_') {
			$color_check='checked';
		}
		$colordata='';
		$colordata.="<li class='sf_filter'><input type='text' class='sf_input' f='color' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$colordata.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_co(''); class='clear_co_btn'>Clear</li>";
		$colordata.="<li><label><input type='checkbox' onclick=search(1,event,0,'co','') $color_check name='ckcolor' class='ckcolor' value=''> All</label></li>";
		$color=$this->db->query("SELECT color_id,color_name FROM ospos_colors c WHERE c.deleted=0 ORDER BY c.color_name ASC")->result();
		$count=$this->item->getnumsearch('color_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		
			foreach ($color as $col) {
				$sele='';
				if(isset($arrofcolor[$col->color_id]))
					$sele='checked';
				if($count[$col->color_id]>0)
					$colordata.= "<li><label><input type='checkbox' onclick=search(1,event,0,'co','$col->color_id') $sele name='ckcolor' class='ckcolor' value='$col->color_id'/> $col->color_name(".$count[$col->color_id].")</label></li>";# code...
				
			}
		$co_other=$this->item->getnumsearch_other('color_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

		$co_other_check='';
		if (isset($arrofcolor['other'])) {
			$co_other_check='checked';
		}
		if ($co_other>0) {
		$colordata.="<li><label><input type='checkbox' onclick=search(1,event,0,'co','other') $co_other_check name='ckcolor' class='ckcolor' value='other'> OTHER($co_other)</label></li>";
			
		}
		// PARTPLACEMENT
		$partarr=explode('_', trim($place,'_'));
		$arrofpart=array();
		for ($i=0; $i <count($partarr) ; $i++) { 
			$arrofpart[$partarr[$i]]=$partarr[$i];
		}
		// print_r($arrofpart);
		$part_check='';
		if($place=='' || $place=='_') {
			$part_check='checked';
		}
		$partdata='';
		$partdata.="<li class='sf_filter'><input type='text' class='sf_input' f='part' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$partdata.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_part(''); class='clear_part_btn'>Clear</li>";
		$partdata.="<li><label><input type='checkbox' onclick=search(1,event,0,'part','') $part_check name='ckpart' class='ckpart' value=''> All</label></li>";
		$part=$this->db->query("SELECT partplacement_id,partplacement_name FROM ospos_partplacements p WHERE p.deleted=0 ORDER BY p.partplacement_name ASC")->result();
		$count=$this->item->getnumsearch('partplacement_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_location.$where_s_all.$where_s);
		// print_r($count);
			foreach ($part as $pa) {
				$sele='';
				if(isset($arrofpart[$pa->partplacement_id]))
					$sele='checked';
				if($count[$pa->partplacement_id]>0)
					$partdata.= "<li><label><input type='checkbox' onclick=search(1,event,0,'part','$pa->partplacement_id') $sele name='ckpart' class='ckpart' value='$pa->partplacement_id'/> $pa->partplacement_name(".$count[$pa->partplacement_id].")</label></li>";# code...
				
			}
		$pa_other=$this->item->getnumsearch_other('partplacement_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_location.$where_s_all.$where_s);

		$pa_other_check='';
		if (isset($arrofpart['other'])) {
			$pa_other_check='checked';
		}
		if ($pa_other>0) {
		$partdata.="<li><label><input type='checkbox' onclick=search(1,event,0,'part','other') $pa_other_check name='ckpart' class='ckpart' value='other'> OTHER($pa_other)</label></li>";
			
		}
		// BODY
		$arrbody=explode('_', $body_id);
        $arrofbody=array();
        for ($i=0; $i < count($arrbody) ; $i++) { 
			$arrofbody[$arrbody[$i]]=$arrbody[$i];     
        }
		$body_check='';
		if($body_id=='' || $body_id=='_') {
			$body_check='checked';
		}
		$body_data='';
		$body_data.="<li class='sf_filter'><input type='text' class='sf_input' f='body' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$body_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_body(''); class='clear_body_btn'>Clear</li>";
		$body_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'body','') $body_check name='ckbody' class='ckbody' value=''> All</label></li>";
		$body=$this->db->query("SELECT body_id,body_name FROM ospos_body b
								WHERE deleted=0 ORDER BY body_name ASC")->result();
		$count=$this->item->getnumsearch('body',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($body as $bo) {
				$sele='';
				if(isset($arrofbody[$bo->body_id]))
					$sele='checked';
				if($count[$bo->body_id]>0)
					$body_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'body','$bo->body_id') $sele name='ckbody' class='ckbody' value='$bo->body_id'/> $bo->body_name(".$count[$bo->body_id].")</label></li>";# code...
				
			}
		$body_other=$this->item->getnumsearch_other('body',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		// var_dump($body_other);
		$body_other_check='';
		if (isset($arrofbody['other'])) {
			$body_other_check='checked';
		}
		// echo $body_other_check;
		// echo $body_other;
		if ($body_other>0) {
		$body_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'body','other') $body_other_check name='ckbody' class='ckbody' value='other'> OTHER($body_other)</label></li>";
			
		}
		// ENGINE
        $arrengine=explode('_', $engine_id);
        $arrofengine=array();
        for ($i=0; $i < count($arrengine) ; $i++) { 
			$arrofengine[$arrengine[$i]]=$arrengine[$i];     
        }
		$engine_check='';
		if($engine_id=='' || $engine_id=='_') {
			$engine_check='checked';
		}
		$engine_data='';
		$engine_data.="<li class='sf_filter'><input type='text' class='sf_input' f='engine' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$engine_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_engine(''); class='clear_engine_btn'>Clear</li>";
		$engine_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'engine','') $engine_check name='ckengine' class='ckengine' value=''> All</label></li>";
		$engine=$this->db->query("SELECT engine_id,engine_name FROM ospos_engine e
								WHERE e.deleted=0 ORDER BY engine_name ASC")->result();
		$count=$this->item->getnumsearch('engine',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($engine as $en) {
				$sele='';
				if(isset($arrofengine[$en->engine_id]))
					$sele='checked';
				if($count[$en->engine_id]>0)
					$engine_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'engine','$en->engine_id') $sele name='ckengine' class='ckengine' value='$en->engine_id'/> $en->engine_name(".$count[$en->engine_id].")</label></li>";# code...
				
			}
		$engine_other=$this->item->getnumsearch_other('engine',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_fuels.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		$engine_other_check='';
		if (isset($arrofengine['other'])) {
			$engine_other_check='checked';
		}
		if ($engine_other>0) {
		$engine_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'engine','other') $engine_other_check name='ckengine' class='ckengine' value='other'> OTHER($engine_other)</label></li>";
			
		}

		// FUEL

        $arrfuel=explode('_', $fuel_id);
        $arroffuel=array();
        for ($i=0; $i < count($arrfuel) ; $i++) { 
			$arroffuel[$arrfuel[$i]]=$arrfuel[$i];     
        }
		$fuel_check='';
		if($fuel_id=='' || $fuel_id=='_') {
			$fuel_check='checked';
		}
		$fuel_data='';
		$fuel_data.="<li class='sf_filter'><input type='text' class='sf_input' f='fuel' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$fuel_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_fuel(''); class='clear_fuel_btn'>Clear</li>";
		$fuel_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','') $fuel_check name='ckfuel' class='ckfuel' value=''> All</label></li>";
		$fuel=$this->db->query("SELECT fuel_id,fule_name FROM ospos_fuel f
								WHERE f.deleted=0 ORDER BY fule_name ASC")->result();
		$count=$this->item->getnumsearch('fuel',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($fuel as $fu) {
				$sele='';
				if(isset($arroffuel[$fu->fuel_id]))
					$sele='checked';
				if($count[$fu->fuel_id]>0)
					$fuel_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','$fu->fuel_id') $sele name='ckfuel' class='ckfuel' value='$fu->fuel_id'/> $fu->fule_name(".$count[$fu->fuel_id].")</label></li>";# code...
				
			}
		$fuel_other=$this->item->getnumsearch_other('fuel',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_transs.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		$fuel_other_check='';
		if (isset($arroffuel['other'])) {
			$fuel_other_check='checked';
		}
		if ($fuel_other>0) {
		$fuel_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'fuel','other') $fuel_other_check name='ckfuel' class='ckfuel' value='other'> OTHER($fuel_other)</label></li>";
			
		}
			// TRANSMISSION
	
        $arrtrans=explode('_', $trans_id);
        $arroftrans=array();
        for ($i=0; $i < count($arrtrans) ; $i++) { 
			$arroftrans[$arrtrans[$i]]=$arrtrans[$i];     
        }
	
		$trans_check='';
		if($trans_id=='' || $trans_id=='_') {
			$trans_check='checked';
		}
		$trans_data='';
		$trans_data.="<li class='sf_filter'><input type='text' class='sf_input' f='trans' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";
		$trans_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_trans(''); class='clear_trans_btn'>Clear</li>";
		$trans_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trans','') $trans_check name='cktrans' class='cktrans' value=''> All</label></li>";
		$trans=$this->db->query("SELECT trans_id,transmission_name FROM ospos_transmission t
								WHERE t.deleted=0 ORDER BY transmission_name ASC")->result();
		$count=$this->item->getnumsearch('transmission',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_trims.$where_place.$where_location.$where_s_all.$where_s);

			foreach ($trans as $tr) {
				$sele='';
				if(isset($arroftrans[$tr->trans_id]))
					$sele='checked';
				if($count[$tr->trans_id]>0)
					$trans_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'trans','$tr->trans_id') $sele name='cktrans' class='cktrans' value='$tr->trans_id'/> $tr->transmission_name(".$count[$tr->trans_id].")</label></li>";# code...
				
			}
		$trans_other=$this->item->getnumsearch_other('transmission',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_trims.$where_place.$where_location.$where_s_all.$where_s);
		$trans_other_check='';
		if (isset($arroftrans['other'])) {
			$trans_other_check='checked';
		}
		if ($trans_other>0) {
		$trans_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trans','other') $trans_other_check name='cktrans' class='cktrans' value='other'> OTHER($trans_other)</label></li>";
			
		}

				// TRIM
        $arrtrim=explode('_', $trim_id);
        $arroftrim=array();
        for ($i=0; $i < count($arrtrim) ; $i++) { 
			$arroftrim[$arrtrim[$i]]=$arrtrim[$i];     
        }
	   
		$trim_check='';
		if($trim_id=='' || $trim_id=='_') {
			$trim_check='checked';
		}
		$trim_data='';
		$trim_data.="<li class='sf_filter'><input type='text' class='sf_input' f='trim' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$trim_data.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_trim(''); class='clear_trim_btn'>Clear</li>";
		$trim_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trim','') $trim_check name='cktrim' class='cktrim' value=''> All</label></li>";
		$trim=$this->db->query("SELECT trim_id,trim_name FROM ospos_trim t
								WHERE t.deleted=0 ORDER BY trim_name ASC")->result();
		$count=$this->item->getnumsearch('trim',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_place.$where_location.$where_s_all.$where_s);


			foreach ($trim as $tri) {
				$sele='';
				if(isset($arroftrim[$tri->trim_id]))
					$sele='checked';
				if($count[$tri->trim_id]>0)
					$trim_data.= "<li><label><input type='checkbox' onclick=search(1,event,0,'trim','$tri->trim_id') $sele name='cktrim' class='cktrim' value='$tri->trim_id'/> $tri->trim_name(".$count[$tri->trim_id].")</label></li>";# code...
				
			}
		$trim_other=$this->item->getnumsearch_other('trim',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_cols.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_place.$where_location.$where_s_all.$where_s);
		$trim_other_check='';
		if (isset($arroftrim['other'])) {
			$trim_other_check='checked';
		}
		if ($trim_other>0) {
		$trim_data.="<li><label><input type='checkbox' onclick=search(1,event,0,'trim','other') $trim_other_check name='cktrim' class='cktrim' value='other'> OTHER($trim_other)</label></li>";
			
		}
		// LOCATION
		$locarr=explode('_', $location);
		$arrofloc=array();
		for ($i=0; $i <count($locarr) ; $i++) { 
			$arrofloc[$locarr[$i]]=$locarr[$i];
		}
		$loc_check='';
		if($location=='' || $location=='_') {
			$loc_check='checked';
		}
		$locdata='';
		$locdata.="<li class='sf_filter'><input type='text' class='sf_input' f='location' onkeyup=search_filter($(this).val(),$(this).attr('f'));></li>";

		$locdata.="<li style='float:right;cursor:pointer;color:#21759B' onclick=clear_location(''); class='clear_location'>Clear</li>";
		$locdata.="<li><label><input type='checkbox' onclick=search(1,event,0,'location','') $loc_check name='cklocation' class='cklocation' value=''> All</label></li>";
		$location_data=$this->db->query("SELECT location_id,location_name FROM ospos_locations l WHERE l.deleted=0 ORDER BY l.location_name ASC")->result();
		$count=$this->item->getnumsearch('location_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_cols.$where_s_all.$where_s);
		
			foreach ($location_data as $loc) {
				$sele='';
				if(isset($arrofloc[$loc->location_id]))
					$sele='checked';
				if($count[$loc->location_id]>0)
					$locdata.= "<li><label><input type='checkbox' onclick=search(1,event,0,'location','$loc->location_id') $sele name='cklocation' class='cklocation' value='$loc->location_id'/> $loc->location_name(".$count[$loc->location_id].")</label></li>";# code...
				
			}
		$loc_other=$this->item->getnumsearch_other('location_id',$where_makes.$where_models.$where_vins.$where_years.$where_yearfs.$where_bodys.$where_engines.$where_fuels.$where_transs.$where_trims.$where_place.$where_cols.$where_s_all.$where_s);

		$loc_other_check='';
		if (isset($arrofloc['other'])) {
			$loc_other_check='checked';
		}
		if ($loc_other>0) {
		$locdata.="<li><label><input type='checkbox' onclick=search(1,event,0,'location','other') $loc_other_check name='cklocation' class='cklocation' value='other'> OTHER($loc_other)</label></li>";
			
		}
		$arr['vin']=$vin_data;
		$arr['trim']=$trim_data;
		$arr['trans']=$trans_data;
		$arr['fuel']=$fuel_data;
		$arr['engine']=$engine_data;
		$arr['body']=$body_data;
		$arr['make']=$make_data;
		$arr['model']=$modeldata;
		$arr['year']=$yeardata;
		$arr['color']=$colordata;
		$arr['ppl']=$partdata;
		$arr['loc']=$locdata;
		$arr['s_all'] = $s_all;
		// $arr['ex_color']=$ex_colordata;
		header("Content-type:text/x-json");
		echo json_encode($arr);

	}
	function search_vin(){
		$data['controller_name']=strtolower(get_class());
		$controller_name=$data['controller_name'];
		$search='';
		$make_id='';
		$model_id='';
		$s_year='';
		$in_color_id='';
		$ex_color_id='';
		$where='';
		$pro_sta='';
		$body_id='';
		$engine_id='';
		$fuel_id='';
		$vin='';
		$data['title']="";
		$year_from = $_POST['year_from'];
		$year_to = $_POST['year_to'];
		$from_site = isset($_POST['from_site']) ? isset($_POST['from_site']) : false;
		$filters_applied = array();
		$filter_array = array();

		$cl_f = $this->input->post('cl_f');
		$cl_fid = $this->input->post('cl_fid');

		if ($cl_f) {
			$this->sale_lib->unset_filter($cl_f,$cl_fid);
		}

		$re='<b>Filters Applied: </b>';

		if(isset($_POST['search'])){
			$search=$_POST['search'];
			$data['title']="Search Results";
			if ($search!='') {
			$this->sale_lib->add_filter('sl',$search,'s');
			$re.="<span onclick='clear_s()'> $search&nbsp;<i class='fa fa-times-circle'></i></span>";
				# code...
			}
		}

		if(isset($_POST['m']) && $_POST['m']!='' && $_POST['m']!='undefined' && $_POST['m']!='0'){
			$make_id=$_POST['m'];
			$re.='<span onclick="clear_make()">';
			$this->sale_lib->add_filter($make_id,$this->item->make_name($make_id)->row()->make_name,'make');

			if ($make_id=='other') {
				$re.='Other';
				$where.=" AND (i.make_id='' OR i.make_id='0' OR i.make_id IS NULL)";
			}else{

				$re.=$this->item->make_name($make_id)->row()->make_name;
				$where.=" AND i.make_id='".$_POST['m']."'";
			}
			$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';
		}
		if(isset($_POST['mo']) && $_POST['mo']!=''&& $_POST['mo']!='undefined' && $_POST['mo']!='0'){
			$model_id=$_POST['mo'];
			$re.='<span onclick="clear_model()">';
			$this->sale_lib->add_filter($model_id,$this->item->model_name($model_id)->row()->model_name,'model');

			if ($model_id=='other') {
				$re.="Other";
				$where.=" AND (i.model_id='' OR i.mdoel_id='0' OR i.model_id IS NULL)";
			}else{
				$re.=$this->item->model_name($model_id)->row()->model_name;
				$where.=" AND i.model_id='".$_POST['mo']."'";
			}
			$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';
		}
		if(isset($_POST['vin']) && $_POST['vin']!='' && $_POST['vin']!='undefined' && $_POST['vin']!='0'){
			$vin=$_POST['vin'];
			$re.='<span onclick="clear_head()">';
			$this->sale_lib->add_filter('vin',$vin,'head');

			if ($vin!='other') {
				$re.="H$vin";
				$where.=" AND i.vinnumber_id='".$_POST['vin']."'";
			}
			else{
				$where.=" AND (i.category='' OR i.category='0' OR i.category IS NULL)";
			}
			$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';
			
		}
		if(isset($_POST['y']) && $_POST['y']!='' && $_POST['y']!='_' && $_POST['y']!='undefined' && $_POST['y']!='0'){
			$year=$_POST['y'];
			$year_from='';
			$year_to='';
			$arryear=explode('_', $_POST['y']);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 

            		if ($arryear[$i]!='_' && $arryear[$i]!='') {
						$re.=' <span onclick=clear_year("'.$arryear[$i].'")>';						
						$this->sale_lib->add_filter($arryear[$i],$arryear[$i],'year');
            			if ($arryear[$i]=='other') {
            				$re.="Other";
            				$where_year.="i.year='' OR i.year=0 OR i.year IS NULL";
            			}else{
            				$where_year.="i.year='".$arryear[$i]."'";
            				$re.=$arryear[$i];
            			}
	                    if(count($arryear)-2>$i){
	                        $where_year.=' OR ';
	                        // $re.=', ';
	                    }
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

            		}

            }
            

            $where_year.=')';
            $s_year=$_POST['y'];
            $where.=$where_year;

		}
		if(isset($_POST['c']) && $_POST['c']!='' && $_POST['c']!='_' && $_POST['c']!='undefined' && $_POST['c']!='0'){
			$arrcol=explode('_', $_POST['c']);
            $where_color=' AND(';
            // $re.=' Color:<b>';
            for ($i=0; $i < count($arrcol) ; $i++) { 
                	if($arrcol[$i]!='_' && $arrcol[$i]!=''){
						$re.=' <span onclick=clear_co("'.$arrcol[$i].'")>';
						$this->sale_lib->add_filter($arrcol[$i],$this->item->color_name($arrcol[$i])->row()->color_name,'co');
						
						
						// $this->sale_lib->set_filter($arrcol[$i]);
						

                		if ($arrcol[$i]=='other') {
            				$where_color.="i.color_id='' OR i.color_id=0 OR i.color_id IS NULL";
                			$re.='Other';
                		}else{
							$where_color.="i.color_id='".$arrcol[$i]."'";
							$re.=$this->item->color_name($arrcol[$i])->row()->color_name;
                		}
						if (count($arrcol)-2>$i) {
							$where_color.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
            }
		    // $re.='</b>';
            $where_color.=')';
            $color_id=$_POST['c'];
            // echo $in_where_color;
            $where.=$where_color;
		}

		// LOCATION
		if(isset($_POST['loc']) && $_POST['loc']!='' && $_POST['loc']!='_' && $_POST['loc']!='undefined' && $_POST['loc']!='0'){
			$arrloc=explode('_', $_POST['loc']);
            $where_loc=' AND(';
            // $re.=' Location:<b>';
            for ($i=0; $i < count($arrloc) ; $i++) { 
                	if($arrloc[$i]!='_' && $arrloc[$i]!=''){
					$this->sale_lib->add_filter($arrloc[$i],$this->item->location_name($arrloc[$i])->row()->location_name,'location');

                		// $filter_line++;
						// $filter_array[$arrloc[$i]] = $this->item->location_name($arrloc[$i])->row()->location_name;

						$re.=' <span onclick=clear_location("'.$arrloc[$i].'")>';
                		if ($arrloc[$i]=='other') {
            				$where_loc.="i.location_id='' OR i.location_id=0 OR i.location_id IS NULL";
                			$re.='Other';
                		}else{
							$where_loc.="i.location_id='".$arrloc[$i]."'";
							$re.=$this->item->location_name($arrloc[$i])->row()->location_name;
                		}
						if (count($arrloc)-2>$i) {
							$where_loc.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
            }
		    // $re.='</b>';
            $where_loc.=')';
            $location_id=$_POST['loc'];
            $where.=$where_loc;
            // echo $where_loc;die();
		}
		// $data['fa'] = $filter_array;
		

		$yearft='';
		if ($year_from!='' && $year_to!='') {
			$yearft .= " AND i.year BETWEEN $year_from AND $year_to";
			$where.=$yearft;
			$this->sale_lib->add_filter('yft',"$year_from - $year_to",'yearrange');

			$re.=" <span onclick='clear_yearrange()'> $year_from - $year_to&nbsp;<i class='fa fa-times-circle'></i></span>";
		}

		if(isset($_POST['body']) && $_POST['body']!='' && $_POST['body']!='_' && $_POST['body']!='undefined' && $_POST['body']!='0'){
			$arrbody=explode('_', $_POST['body']);
            $where_body=' AND(';
            // $re.=' Body:<b>';
            for ($i=0; $i < count($arrbody) ; $i++) { 
                     if($arrbody[$i]!='_' && $arrbody[$i]!=''){
						$re.=' <span onclick=clear_body("'.$arrbody[$i].'")>';
						$this->sale_lib->add_filter($arrbody[$i],$this->item->body_name($arrbody[$i])->row()->body_name,'body');

                     	if ($arrbody[$i]=='other') {
                     		$re.='Other';
                     		$where_body.="i.body='' OR i.body=0 OR i.body IS NULL";
                     	}else{
							$where_body.="i.body='".$arrbody[$i]."'";
							$re.=$this->item->body_name($arrbody[$i])->row()->body_name;
                     	}
						if (count($arrbody)-2>$i) {
							$where_body.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
            }
            // $re.='</b>';
            $where_body.=')';
            $where.=$where_body;
		}
		if(isset($_POST['ppl']) && $_POST['ppl']!='' && $_POST['ppl']!='_' && $_POST['ppl']!='undefined' && $_POST['ppl']!='0'){
			$arrpart=explode('_', trim($_POST['ppl'],'_'));
			// print_r($arrpart); die();
			$total_ppl= count($arrpart);
            $where_part=' AND(';
            // $re.=' Partplacement:<b>';
            for ($i=0; $i < count($arrpart) ; $i++) { 
                     if($arrpart[$i]!='_' && $arrpart[$i]!=''){
						$this->sale_lib->add_filter($arrpart[$i],$this->item->part_placement_name($arrpart[$i])->row()->partplacement_name,'part');

						$re.=' <span onclick=clear_part("'.$arrpart[$i].'")>';

                     	if ($arrpart[$i]=='other') {
                     		$re.='Other';
                     		$where_part.="i.partplacement_id='' OR i.partplacement_id=0 OR i.partplacement_id IS NULL";
                     	}else{
							$re.=$this->item->part_placement_name($arrpart[$i])->row()->partplacement_name;
							$where_part.="i.partplacement_id='".$arrpart[$i]."'";
                     	}
                     	// echo $
						if ($total_ppl-1>$i) {
							$where_part.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
            }
            // $re.='</b>';
            $where_part.=')';
            $where.=$where_part;
		}

		if(isset($_POST['engine']) && $_POST['engine']!='' && $_POST['engine']!='_' && $_POST['engine']!='undefined' && $_POST['engine']!='0'){
			$arrengine=explode('_', $_POST['engine']);
            $where_engine=' AND(';
            // $re.=' Engine:<b>';
            for ($i=0; $i < count($arrengine) ; $i++) { 
                    if($arrengine[$i]!='_' && $arrengine[$i]!=''){
						$re.=' <span onclick=clear_engine("'.$arrengine[$i].'")>';
						$this->sale_lib->add_filter($arrengine[$i],$this->item->engine_name($arrengine[$i])->row()->engine_name,'engine');

                    	if ($arrengine[$i]=='other') {
                    		$re.='Other';
                    		$where_engine.="i.engine='' OR i.engine=0 OR i.engine IS NULL";
                    	}else{
                    		$re.=$this->item->engine_name($arrengine[$i])->row()->engine_name;
							$where_engine.="i.engine='".$arrengine[$i]."'";
                    	}
						if (count($arrengine)-2>$i) {
							// $re.=', ';
							$where_engine.=" OR ";
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
                
            }
            // $re.='</b>';
            $where_engine.=')';
            $where.=$where_engine;

		}
		if(isset($_POST['fuel']) && $_POST['fuel']!='' && $_POST['fuel']!='_' && $_POST['fuel']!='undefined' && $_POST['fuel']!='0'){
			$arrfuel=explode('_', $_POST['fuel']);
            $where_fuel=' AND(';
            // $re.=' Fuel:<b>';
            for ($i=0; $i < count($arrfuel) ; $i++) { 
                    if($arrfuel[$i]!='_' && $arrfuel[$i]!=''){
						$re.=' <span onclick=clear_fuel("'.$arrfuel[$i].'")>';
						$this->sale_lib->add_filter($arrfuel[$i],$this->item->fuel_name($arrfuel[$i])->row()->fule_name,'fuel');

                    	if ($arrfuel[$i]=='other') {
                    		$re.='Other';
                    		$where_fuel.="i.fuel='' OR i.fuel=0 OR i.fuel IS NULL";
                    	}else{
							$where_fuel.="i.fuel='".$arrfuel[$i]."'";
							$re.=$this->item->fuel_name($arrfuel[$i])->row()->fule_name;
                    	}
						if (count($arrfuel)-2>$i) {
							$where_fuel.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
                
            }
            // $re.='</b>';
            $where_fuel.=')';
            $where.=$where_fuel;

		}
		if(isset($_POST['trans']) && $_POST['trans']!='' && $_POST['trans']!='_' && $_POST['trans']!='undefined' && $_POST['trans']!='0'){
			$arrtrans=explode('_', $_POST['trans']);
            $where_trans=' AND(';
            // $re.=' Transmission:<b>';
            for ($i=0; $i < count($arrtrans) ; $i++) { 
                    if($arrtrans[$i]!='_' && $arrtrans[$i]!=''){
						$re.=' <span onclick=clear_trans("'.$arrtrans[$i].'")>';
						$this->sale_lib->add_filter($arrtrans[$i],$this->item->trans_name($arrtrans[$i])->row()->transmission_name,'trans');

                    	if ($arrtrans[$i]=='other') {
                    		$re.='Other';
                    		$where_trans.="i.transmission='' OR i.transmission=0 OR i.transmission IS NULL";
                    	}else{
							$where_trans.="i.transmission='".$arrtrans[$i]."'";
							$re.=$this->item->trans_name($arrtrans[$i])->row()->transmission_name;
                    	}
						if (count($arrtrans)-2>$i) {
							$where_trans.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
                
            }
            // $re.='</b>';
            $where_trans.=')';
            $where.=$where_trans;

		}
		if(isset($_POST['trim']) && $_POST['trim']!='' && $_POST['trim']!='_' && $_POST['trim']!='undefined' && $_POST['trim']!='0'){
			$arrtrim=explode('_', $_POST['trim']);
            $where_trim=' AND(';
            // $re.=' Trim:<b>';
            for ($i=0; $i < count($arrtrim) ; $i++) { 
                    if($arrtrim[$i]!='_' && $arrtrim[$i]!=''){
						$re.=' <span onclick=clear_trim("'.$arrtrim[$i].'")>';
						$this->sale_lib->add_filter($arrtrim[$i],$this->item->trim_name($arrtrim[$i])->row()->trim_name,'trim');

                    	if ($arrtrim[$i]=='other') {
                    		$re.="Other";
                    		$where_trim.="i.trim='' OR i.trim=0 OR i.trim IS NULL";
                    	}else{
							$where_trim.="i.trim='".$arrtrim[$i]."'";
							$re.=$this->item->trim_name($arrtrim[$i])->row()->trim_name;

                    	}
						if (count($arrtrim)-2>$i) {
							$where_trim.=" OR ";
							// $re.=', ';
						}
						$re.='&nbsp;<i class="fa fa-times-circle"></i></span>';

		           	}
                
            }
            // $re.='</b>';
            $where_trim.=')';
            $where.=$where_trim;

		}
    	
		// $where_new = str_replace("AND()","",$where);
		$s = $this->input->post('search_all');
		$match = '';
		// $order_by = ' i.item_id DESC';
		$where_match = '';
		if ($s) {
			$this->sale_lib->add_filter('sa',$s,'sa');
			
			$ars = explode(' ', $s);
			for ($i=0; $i < count($ars); $i++) { 
				$where_match.= " AND(
									i.barcode LIKE '%$ars[$i]%' OR
									i.item_number LIKE '%$ars[$i]%' OR
									i.name LIKE '%$ars[$i]%' OR
									i.make_name LIKE '%$ars[$i]%' OR
									i.model_name LIKE '%$ars[$i]%' OR
									i.year LIKE '%$ars[$i]%' OR
									i.color_name LIKE '%$ars[$i]%' OR
									i.category LIKE '%$ars[$i]%'
									)";
			}

			$where.= $where_match;
			
		}
		// SORTING
		$stype = $this->input->post('dud');
		$sfield = $this->input->post('dsn');
		if ($stype && $sfield) {
			$order_by = "ORDER BY i.$sfield $stype";
			if ($sfield=='barcode') {
				$order_by = "ORDER BY i.item_id $stype";
			}
		}

		// if ($s) {
		// 	$sql_mode = '';
		// 	if (strpos($s,' ')) {
		// 		$sql_mode = 'NATURAL LANGUAGE';
		// 	}else{
		// 		$sql_mode = 'BOOLEAN';
		// 	}

		// 		$where.= " AND (
		// 						MATCH(`name`,`year`,model_name,make_name,color_name,branch_name,partplacement_name,item_number,category,barcode) AGAINST('$s' IN $sql_mode MODE)
		// 						)";
		// 		$match .= ",MATCH(`name`,`year`,model_name,make_name,color_name,branch_name,partplacement_name,item_number,category,barcode) AGAINST('$s' IN $sql_mode MODE)";

				

			
		// }
		// $order_by = '';
		// if ($match=='') {
		// 	$order_by = "ORDER BY i.item_id";
		// }

		if ($this->config->item('show_item_quantiy_0')!=1) {
			$where.= " AND i.quantity > 0";
		}


		$sql="SELECT i.item_id,
					i.name,
					i.unit_price,
					i.barcode,
					i.year,
					i.category,
					i.cost_price,
					i.quantity,
					i.item_number,
					i.make_id,
					i.model_id,
					i.color_id,
					i.khmername_id,
					i.partplacement_id,
					i.body,
					i.engine,
					i.fuel,
					i.transmission,
					i.trim,
					i.model_name,
					i.make_name,
					i.color_name,
					i.khmer_name,
					i.branch_name,
					i.partplacement_name,
					i.vinnumber_id,
					i.location_id,
					i.item_status
					
		FROM ospos_items i
	    WHERE i.deleted='0' 
	            AND (i.name LIKE '%$search%' 
					OR i.make_name LIKE '%$search%' 
					OR i.model_name LIKE '%$search%' 
					OR i.color_name LIKE '%$search%'
					OR i.unit_price LIKE '%$search%'
					OR i.khmer_name LIKE '%$search%'
					OR i.branch_name LIKE '%$search%'
					OR i.barcode LIKE '%$search%'
					OR i.year like '%$search%' 
					OR i.category LIKE '%$search%'
					OR i.partplacement_name LIKE '%$search%'
					OR i.item_number LIKE '%$search%'
					OR i.vinnumber_id LIKE '%$search%'
					OR i.cost_price LIKE '%$search%')
		{$where}
		$order_by
		";
		$sql_count="SELECT i.item_id,
					i.name,
					i.unit_price,
					i.barcode,
					i.year,
					i.category,
					i.cost_price,
					i.quantity,
					i.item_number,
					i.make_id,
					i.model_id,
					i.color_id,
					i.khmername_id,
					i.partplacement_id,
					i.body,
					i.engine,
					i.fuel,
					i.transmission,
					i.trim,
					i.model_name,
					i.make_name,
					i.color_name,
					i.khmer_name,
					i.branch_name,
					i.partplacement_name,
					i.vinnumber_id,
					i.location_id
		FROM ospos_items i
	    WHERE i.deleted='0' 
	    		-- AND i.quantity>0
	            AND (i.name LIKE '%$search%' 
					OR i.make_name LIKE '%$search%' 
					OR i.model_name LIKE '%$search%' 
					OR i.color_name LIKE '%$search%'
					OR i.unit_price LIKE '%$search%'
					OR i.khmer_name LIKE '%$search%'
					OR i.branch_name LIKE '%$search%'
					OR i.barcode LIKE '%$search%'
					OR i.year like '%$search%' 
					OR i.category LIKE '%$search%'
					OR i.partplacement_name LIKE '%$search%'
					OR i.item_number LIKE '%$search%'
					OR i.vinnumber_id LIKE '%$search%'
					OR i.cost_price LIKE '%$search%')";
		

		$array=array('test'=>'a','tt'=>'12');
		$totalrow=$this->db->query($sql);
		$this->session->unset_userdata("query_result");
		$this->session->set_userdata("query_result",$sql_count);
		// $this->session->unset_userdata("where");
		// $this->session->set_userdata("where",$where);
		// print_r($totalrow->result()); die();
		// // $json = json_decode($totalrow);
		// $array=array('test'=>'123','test1'=>'123');
		// $array=array('test'=>'123','test1'=>'123');
		// print_r($array); die();
		
		// ++++++++++++++++pagination++++++++++++
		$page_num=50;
		$per_page='';
		if(isset($_POST['p_num']) && $_POST['p_num']!='undefined' && $_POST['p_num']!='0' && $_POST['p_num']!='')
			$page_num=$_POST['p_num'];

		if(isset($_POST['perpage']))
			$per_page=$_POST['perpage'];
		$data['total_rows'] =$totalrow->num_rows();
		$paging=$this->green->ajax_pagination($totalrow->num_rows(),site_url("items/search_vin"),$page_num);
		$limit='';
		if ($page_num!='all') {
			$limit=" limit ".$paging['limit'];
		// if($per_page>0){
			$limit=" LIMIT {$paging['start']}, {$paging['limit']}";
		// }
		}
		
		$sql.=" {$limit}";
		$table='';

		foreach ($this->db->query($sql)->result() as $item) {
            $image=$this->Item->getdefaultimg($item->item_id);
			$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");
			$count_img = $this->Item->countAllItemImage($item->item_id);
			$not_actual = 0;
			if (!$image) {
					$image = $this->Item->getItemTempImage($item->item_number);
				if ($image) {
					$not_actual = 1;
				}
				
			}
			if($image!=''){
				if(file_exists(FCPATH."uploads/thumb/".$image)){
					$im=array('src'=>'uploads/thumb/'.$image.'?'.rand(0,999),'width'=>"120");
			            // $img_path=img("assets/upload/slide/".$model->slide_id.'.jpg');
			      }else{
			      	$image = 'no_img.png';
			      }
			}

		    $img_path=img($im);
			$head = '';
			$loc_name ='';
			if ($item->location_id!='') {
				$loc_name = $this->Item->get_loc_name($item->location_id);
			}
			if ($item->vinnumber_id!='' && $item->vinnumber_id!=0) {
				$head = 'H'.$item->vinnumber_id;
			}
			$pend = '';
			if ($item->is_pending == 1) {
				$pend = 'ispending';
			}
			$link_below = '<div class="v_photo"><a target="_blank" href='.site_url("$controller_name/view_all_v_photo/$item->item_id").'>View all photos('.$count_img.')</a></div>';

			// $item_name = $this->Item->khmer_name($item->khmername_id)->row();


			if ($not_actual==1) {
				$img_path = '<a data-fancybox href="'.'uploads/'.$image.'?'.rand(0,999).'"><img width="120px" src="'.'uploads/thumb/'.$image.'?'.rand(0,999).'"></a>';
				$link_below = '<div class="v_photo"><span>Not Actual Image</span></div>';
				
			}

			$table.='<tr id="'.$pend.'">';
			$table.="<td style='vertical-align:middle;' width='2%'>


			<input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></td>";
			// $table.='<td width="15%">'.$img_path.'</td>';
			$table.='<td>'.$img_path.$link_below.'</td>';

			$table.='<td style="vertical-align:middle;" width="15%">'.anchor($controller_name."/view/$item->item_id/width:$width", $item->barcode,array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_update'))).'<br>
				<strong>'.$head.'</strong> / '.$loc_name.'
				</td>';
			
			$table.='<td style="vertical-align:middle;white-space: nowrap;" width="15%">'.$item->item_number.'</td>';
			
			$table.='<td style="vertical-align:middle;white-space: nowrap;" width="20%">'.$item->name.' / '.$item->khmer_name.'</td>';
			
			//$table.='<td width="14%">'.$item->category.'</td>';
			// $table.='<td style="vertical-align:middle;" width="20%">'.$item->khmer_name.'</td>';
			
			$table.='<td style="vertical-align:middle;" width="15%">'.$item->make_name.'</td>';
			$table.='<td style="vertical-align:middle;" width="15%">'.$item->model_name.'</td>';
			$table.='<td style="vertical-align:middle;" width="15%">'.$item->year.'</td>';
			$table.='<td style="vertical-align:middle;" width="15%">'.$item->color_name.'</td>';
			$table.='<td style="vertical-align:middle;" width="15%">'.$item->partplacement_name.'</td>';
			$table.='<td style="vertical-align:middle;" width="15%">'.$item->branch_name.'</td>';
			
			$unit_price = to_currency($item->unit_price);
			$class = "hide";
			$display = "display:none;";
			
			if(($this->session->userdata('u_inf') AND isset($_POST['from_site'])) || ($this->session->userdata('person_id') AND !(isset($_POST['from_site']))) )
			{
				
				$class = "";
				$display = "";

			}

			$table.='<td style="vertical-align:middle;" width="14%">'.to_currency($item->cost_price).'</td>';
			$table.='<td style="vertical-align:middle; '.$display.'" width="14%">'.$unit_price.$class.'</td>';
			//$table.='<td width="14%">'.$tax_percents.'</td>';	
			//$table.='<td width="14%">'.$location.'</td>';	
			
			$table.='<td style="vertical-align:middle;" width="14%">'.anchor($controller_name."/inventory/$item->item_id/width:$width", $item->quantity,array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_count'))).'</td>';
			
			// $table.='<td style="vertical-align:middle;" width="0%">'./*anchor($controller_name."/view/$item->item_id/width:$width", $CI->lang->line('common_edit'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_update'))).*/'</td>';		
			
			//Ramel Inventory Tracking
			$table.='<td style="vertical-align:middle;" width="5%">'./*anchor($controller_name."/inventory/$item->item_id/width:$width", $CI->lang->line('common_inv'),array('class'=>'thickbox','title'=>$CI->lang->line($controller_name.'_count')))./'</td>';//inventory count	
			
			$table.='<td width="5%">'*/'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/width:$width", $this->lang->line('common_det'),array('class'=>'thickbox','title'=>$this->lang->line($controller_name.'_details_count'))).'</td>';//inventory details	
			
			$table.='<td style="vertical-align:middle;">'.$item->item_status.'</td>';
			$table.='</tr>';

		}
		$data['se']=1;
		$val=0;
		$per=0;
		if ($limit!='') {
			$val= $paging['limit'];
			$per=$paging['start'];
		}
		
		$total_rows = $data['total_rows'];
		if ($per=='') {
			$r1=1;
		}else{
			$r1=$per+1;
		}
		if ($per+$val<$total_rows && $per+$val!=0) {
			$r2 = $per+$val;
		}else{
			$r2 = $total_rows;
		}

		$results="<p>Showing ".$r1." to ".$r2." of ".$total_rows." results";
		// if ($re!=' of ') {
		// 	$results.=$re;
		// }
		$results.="</p>";
		if ($re=='<b>Filters Applied: </b>') {
			$re='';
		}
		$ft_ap = '<b>Filters Applied: </b>';
		$current_filter = $this->sale_lib->get_filter();
		foreach ($current_filter as $fi) {
			if ($fi['fi']!='sa') {
				$ft_ap.=' <span class="ft_'.$fi['fi'].'" onclick=clear_'.$fi['fi'].'("'.$fi['fid'].'")>';
				$ft_ap.=$fi['fname'];
				$ft_ap.='&nbsp;<i class="fa fa-times-circle"></i></span>';
			}
			
		}
		if ($ft_ap=='<b>Filters Applied: </b>') {
			$ft_ap='';
		}
		$data['filters_applied'] = $ft_ap;
		$data['result_row'] = $results;
		$data['data']=$table;
		$data['nodata']="No Results";
		$data['pagination']=$paging;
		$data['sql'] = $sql;
		$data['real_filter'] = $current_filter;
		// $data['cs']=count($this->sale_lib->get_filter());
		// $data['sf']=$this->sale_lib->get_filter();
		// var_dump($paging);
		// print_r($this->session->userdata('search_result'));
		echo json_encode($data);
		// $this->load->view('partial/header',$data);
		// $this->load->view('vinnumbers/search_result',$data);
		// $this->load->view('partial/footer');
	}
	function view_all_v_photo($itemid){
		$image=$this->Item->getimage($itemid);
		$data['img']=array();
		foreach ($image as $image) {
			$im=array('src'=>'assets/site/image/no_img.png','width'=>"120");

			if($image->imagename!=''){
				if(file_exists(FCPATH."uploads/".str_replace(' ' , '_', $image->imagename))){
					$im=array('src'=>'uploads/'.str_replace(' ' , '_', $image->imagename));
			      }
			}
			array_push($data['img'], $im);
		}
		$this->load->view('vinnumbers/view_photos',$data);
 	}
 // 	function select_name()
	// {
	// 	$name_id = $this->input->post('name_id');


	// 	echo json_encode($name_id);
	// 	// $this->session->set_userdata('name_id',$name_id);
	// 	// $this->_reload();
	// 	// redirect(site_url('items'))
	// }
 	function sold_item(){
 		$data['w'] = 'w';
		$data['controller_name']=strtolower(get_class());

 		$this->load->view('items/sold_item',$data);
 	}
 	function get_sold_item(){
 		$items = $this->Item->getAllSaleItem();
 		$vinnumbers = $this->Item->getAllVin();
 		$j=0;
 		$tbody = '';
		foreach ($items as $item) {
			if (!$this->Item->getVinSale($item->item_id,$item->is_new)) {
				
				$j++;
				$tbody.= "<tr>
							<td style='text-align:right'>$j</td>
							<td>";
					if ($item->category!='') {
						$tbody.=$item->category;
					}else{
						$tbody.="<select class='vin_sel' name='vin_sel[]'>
									<option value=''>Select</option>";
									foreach ($vinnumbers as $v) {
										$tbody.= "<option value='".$v->vinnumber_id."'>H".$v->vinnumber_id."</option>";
									}
						$tbody.="</select>
								<input type='hidden' class='item_id' name='item_id[]' value='".$item->item_id."'>
								<input type='hidden' class='is_new' name='is_new[]' value='".$item->is_new."'>
								<input type='hidden' class='price' name='price[]' value='".$item->item_unit_price."'>";
					}
					
					$tbody.="</td>
							<td>$item->item_number</td>
							<td>$item->khmername_name / $item->english_name</td>
							<td>$item->make_name</td>
							<td>$item->model_name</td>
							<td>$item->year</td>
						</tr>";
			}
					
		}

		$data['tbody'] = $tbody;

 		echo json_encode($data);
 	}
 	function update_item_vin(){
 		$vin = $this->input->post('vin_sel');
 		$item_id = $this->input->post('item_id');
 		$is_new = $this->input->post('is_new');
 		$price = $this->input->post('price');

 		// UPDATE VINNUMBER SOLD

		if (count(item_id)>0) {
			for ($i=0; $i < count($item_id); $i++) { 
				if ($vin[$i]!='') {
					// echo $item_id[$i];
					// echo "<br>".$vin[$i];
					// echo "<br>".$is_new[$i].'<br>';
					// echo $price[$i].'<br>';

					$this->Sale->update_vin_sale($vin[$i],$item_id[$i],$is_new[$i],$price[$i]);
				}

			}
		}
		
		// 

		$data['s'] = 's';
		echo json_encode($data);
 	}
 	function get_item_suggest(){
 		$text = $this->input->post('q');
 		$its = $this->Item->get_item_suggest($text);
 		
 		echo json_encode($its);
 	}
 	function unset_filter(){
 		$f = $this->input->post('f');
 		$id = $this->input->post('id');

 		$this->sale_lib->unset_filter($f,$id);

 		echo json_encode('unset');
 	}
 	function get_change_data(){
 		$value = $this->input->post('value');
 		$type = $this->input->post('vtype');

 		$da = array();
 		$r = $this->Item->get_change_data($value,$type);
 		
 		if ($type=='v') {
 			$da = array(
				'type'=>$type,
				'id'=>$r->vinnumber_id,
				'make_id'=>$r->make_id?$r->make_id:'',
				'model_id'=>$r->model_id?$r->model_id:'',
				'year'=>$r->year?$r->year:'',
				'body_id'=>$r->body?$r->body:'',
				'engine_id'=>$r->engine?$r->engine:'',
				'transmission_id'=>$r->transmission?$r->transmission:'',
				'trim_id'=>$r->trim?$r->trim:'',
				'fuel_id'=>$r->fuel?$r->fuel:''
				);
 		}
 		if ($type=='p') {
 			$da = array(
				'type'=>$type,
				'id'=>$r->partid,
				'name_id'=>$r->khmernames_id,
				'name_text'=>$this->Item->khmer_name($r->khmernames_id)->row()->english_name.' / '.$this->Item->khmer_name($r->khmernames_id)->row()->khmername_name,
				'category_id'=>$r->category_id==0?'':$r->category_id,
				'make_id'=>$r->make_id==0?'':$r->make_id,
				'model_id'=>$r->model_id==0?'':$r->model_id,
				'year'=>$r->year==0?'':$r->year,
				'color_id'=>$r->color_id==0?'':$r->color_id,
				'body_id'=>$r->body==0?'':$r->body,
				'engine_id'=>$r->engine==0?'':$r->engine,
				'transmission_id'=>$r->transmission==0?'':$r->transmission,
				'trim_id'=>$r->trim==0?'':$r->transmission,
				'fuel_id'=>$r->fuel==0?'':$r->fuel,
				'condition_id'=>$r->codition_id==0?'':$r->codition_id,
				'part_placement'=>$r->part_placement==0?'':$r->part_placement,
				'brand_id'=>$r->brand_id==0?'':$r->brand_id,
				'location_id'=>$r->location_id==0?'':$r->location_id,
				'cost_price'=>0,
				'unit_price_to'=>$r->unit_price_to,
				'is_approve'=>$r->approve
				);
 		}

 		if($r===0){
 			$da = array('is_new_template'=>1);
 		
 		}
		
 		
 		$data['res'] = $da;
 		// $data['vtype'] = $type;


 		echo json_encode($data);
	}
	function update_non_exist_name(){
		header('Content-Type: text/html; charset=utf-8');
		$no_name_id_item = $this->db->query("SELECT * FROM ospos_items WHERE khmername_id = 0 OR khmername_id IS NULL")->result();
		// var_dump($no_name_id_item);die();

		foreach ($no_name_id_item as $item) {
			$has_name = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_name = '$item->name' OR english_name = '$item->name'")->row();

			if ($has_name) {
				$udata = array(
							'khmername_id'=>$has_name->khmername_id,
							'khmer_name' => $has_name->khmername_name
						);
				$this->db->where('item_id',$item->item_id)->update('ospos_items',$udata);
			}else{
				$dcr =  date('Y-m-d H:i:s');
				$idata = array(
								"khmername_name"=>$item->name,
								"english_name"=>$item->name,
								"date_create"=>$dcr,
								"deleted"=>0
							);
				$this->db->insert('ospos_khmernames',$idata);
				$kh_id = $this->db->insert_id();
				if ($kh_id) {
					$kh_name = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_id = '$kh_id'")->row();
					$udata = array(
							'khmername_id'=>$kh_name->khmername_id,
							'khmer_name' => $kh_name->khmername_name
						);
					$this->db->where('item_id',$item->item_id)->update('ospos_items',$udata);
				}
			}
		}
		redirect(site_url('items'));
	}

	function delete_item_id_zero_in_image(){
		
		$this->db->where('item_id',0)->delete('ospos_image_items');
		redirect(site_url('items'));
		

	}
	function remove_same_name(){
		header('Content-Type: text/html; charset=utf-8');

		$same_name = $this->db->query("SELECT * FROM ospos_khmernames WHERE DATE_FORMAT(date_create,'%Y-%m-%d') = '2017-10-13' AND khmername_name = english_name")->result();
		foreach ($same_name as $name) {
			// echo $name->khmername_name.'-'.$name->english_name.'</br>';
			$this->db->where('khmername_id',$name->khmername_id)->delete('ospos_khmernames');
		}

		redirect(site_url('items'));
	}
	// function sync_quantity(){
	// 	$all_items = $this->db->query("SELECT * FROM ospos_items WHERE deleted=0")->result();
	// 	$all_sold_items = $this->db->query("SELECT * FROM ospos_sales_items")->result();

	// 	foreach ($all_items as $ => $value) {
	// 		# code...
	// 	}


	// 	var_dump($all_sold_items);die();
	// }


	function update_to_old_data(){
		$old_data = $this->db->query("SELECT * FROM ospos_items_old WHERE item_number = 'no'")->result();
		foreach ($old_data as $od) {

			$item_data = array(
				'color_id'=>$od->color_id,
				'color_name'=>$od->color_name,
				'branch_id'=>$od->branch_id,
				'branch_name'=>$od->branch_name,
				'condition_id'=>$od->condition_id,
				'add_date'=>$od->add_date,
			);

				
			$data_sec = array(
				'name'=>$od->name,
				'description'=>$od->description,
				'category_id'=>$od->category_id,
				'supplier_id'=>$od->supplier_id,
				'item_number'=>$od->item_number,
				'cost_price'=>$od->cost_price,
				'unit_price'=>$od->unit_price,
				'quantity'=>$od->quantity,
				'reorder_level'=>$od->reorder_level,
				'location'=>$od->location,
				'fit'=>$od->fit,
				'model_id'=>$odmodel_id,
				'year'=>$od->year,
				'partplacement_id'=>$odpartplacement_id,
				'allow_alt_description'=>$od->allow_alt_description,
				'is_serialized'=>$od->is_serialized,
				'location_id'=>$od->location_id,
				'make_id'=>$od->make_id,
				'category'=>$od->category,
				'barcode'=>$od->barcode,
				'grad_id'=>$od->grad_id,
				'is_new'=>$od->is_new,
				'is_pro'=>$od->is_pro,
				'is_feat'=>$od->is_feat,
				'khmername_id'=>$od->khmername_id,
				'engine'=>$od->engine,
				'body'=>$od->body,
				'trim'=>$od->trim,
				'transmission'=>$od->trans,
				'fuel'=>$od->fuel,
				'model_name'=>$od->model_name,
				'make_name'=>$od->make_name,
				'khmer_name'=>$od->khmer_name,
				'partplacement_name'=>$od->partplacement_name,
				'vinnumber_id'=>$od->vinnumber_id
			);
			
			$item_data=array_merge($item_data,$data_sec);


			$this->db->where('item_id',$od->item_id)->update('ospos_items',$item_data);
		}
		
	}

	function add_new_template($data){
		// var_dump($data);die();
		$data_tempate = array(
						'part_number'=>$data['item_number'],
						'part_name' => $data['name'],
						'khmernames_id'=>$data['khmername_id'],
						'category_id'=>$data['category_id'],
						'make_id' => $data['make_id'],
						'model_id'=> $data['model_id'],
						'year' => $data['year'],
						'year_to'=>$data['year'],
						'year_tag'=>$data['year'],
						'color_id'=>$data['color_id'],
						'codition_id'=>$data['condition_id'],
						'part_placement'=>$data['partplacement_id'],
						'brand_id'=>$data['branch_id'],
						'cost_price'=>$data['cost_price'],
						'unit_price'=>$data['unit_price'],
						'approve'=>0,
						'date'=>date('Y-m-d H:i:s'),
						'make_name'=>$data['make_name'],
						'model_name'=>$data['model_name'],
						'part_placementname'=>$data['partplacement_name'],
						'branch_name' => $data['branch_name'],
						'khmername_name' => $data['khmer_name'],
						'color_name' => $data['color_name'],
						'engine' => $data['engine'],
						'body' => $data['body'],
						'trim' => $data['trim'],
						'transmission' => $data['transmission'],
						'fuel' => $data['fuel'],
						'vinnumber_id' => $data['vinnumber_id']
							);

		$this->tem->add_new_from_item($data_tempate);
	}

	function get_photos(){

		$item_id = $_GET['id'];
		$result  = array();
        $obj = [];
        $item_images = $this->db->query("SELECT * FROM ospos_image_items WHERE item_id = '$item_id'")->result();
        
        foreach ($item_images as $img) {
            $img_url = $img->imagename;        
            $file_name = '';         //1
            if ($img_url!= null ) {
                    
                    // $br = explode('/', $img_url);

                    // $file_name = $br[2];



                    if (file_exists('uploads/thumb/'.$img->imagename)) {
                        $file_size = filesize('uploads/thumb/'.$img->imagename);

                        $result[] = array_merge($result,[
                                                    'name'=>str_replace($item_id.'_','', $img->imagename),
                                                    'size'=>$file_size
                                                ]);
                    }
                    

            }
        }

        echo json_encode($result);
	}
}
?>