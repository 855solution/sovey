<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class site extends CI_Controller {
	
	protected $thead;
	protected $idfield;
	protected $searchrow;	
	function __construct(){
		parent::__construct();
		// $this->lang->load('stock', 'english');
		$this->load->library('session');
		$this->load->model("modsite","sit");			
		// $this->thead=array("No"=>'no',
		// 					 "Store Name"=>'store_name',	 
		// 					 "Business Type"=>'description',
		// 					 "Contact Person"=>'contact_person',	
		// 					 "Tel"=>'contact_tel',
		// 					 "Address"=>'address',	
		// 					 "Country"=>'country',
		// 					 "Action"=>'Action'							 	
		// 					);
		// $this->idfield="categoryid";
		
	}
	function index()
	{
		
		$data['title']="Featured Products";
		$this->load->view('site/header');
		$this->load->view('site/home',$data);
		$this->load->view('site/feature_product',$data);
		$this->load->view('site/promoproduct',$data);

		$this->load->view('site/footer');
	}
	function chklogin(){
		$pwd=$this->input->post('log_pwd');
		$email=$this->input->post('log_email');
		$row=$this->db->query("SELECT * FROM ospos_member WHERE email='$email' AND password='".md5($pwd)."'")->row();
		if(count($row)>0){
			$this->session->set_userdata('search',$row);
			$this->session->set_userdata('u_inf', $row);
			// var_dump($this->session->userdata('search')->member_id);die();
			redirect('site/index');
		}else{
			redirect('site/login?log=error');
		}
	}
	function login()
	{
		
		$data['title']="Featured Products";
		$this->load->view('site/header');
		// $this->load->view('site/home',$data);
		$this->load->view('site/register',$data);
		$this->load->view('site/footer');
	}
	function reset()
	{
		
		$data['title']="Featured Products";
		$this->load->view('site/header');
		// $this->load->view('site/home',$data);
		$this->load->view('site/reset',$data);
		$this->load->view('site/footer');
	}
	function mailsuccess()
	{
		
		$data['title']="Featured Products";
		$this->load->view('site/header');
		$data['mail']=1;
		// $this->load->view('site/home',$data);
		$this->load->view('site/register',$data);
		$this->load->view('site/footer');
	}
	function resetemail(){
		$email=$this->input->post("email");
		$count=$this->db->query("SELECT count(*) as count FROM ospos_member WHERE email='$email'")->row()->count;
		echo $count;
	}

	function resetpwd(){
		$member_id=$_GET['m'];
		$password=$this->input->post('password');
		// echo $password;
		$this->db->where('member_id',$member_id)->set('password',md5($password))->update("ospos_member");
		redirect('site/login?succ=1');
	}
	function deletesearch(){
		$search_id=$this->input->post('search_id');
		$this->db->where('search_id',$search_id)->delete('ospos_savesearch');
	}
	function sendmail()
	{
	    $config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'ssl://mail.soveyautoparts.com',
		  'smtp_port' => 465,
		  'smtp_user' => 'mail@soveyautoparts.com', // change it to yours
		  'smtp_pass' => 'sovey123', // change it to yours
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE
		);
		$row=$this->db->query("SELECT * FROM ospos_member where email='".$this->input->post('email')."' limit 1")->row();


	        $message = "<h5 aligh='center'>Sovey Auto part</h5><br><p>CLick link below to reset your password</p><br>http://soveyautoparts.com/index.php/site/reset?m=$row->member_id";
	        $this->load->library('email', $config);
		      $this->email->set_newline("\r\n");
		      $this->email->from('mail@soveyautoparts.com'); // change it to yours
		      $this->email->to($row->email);// change it to yours
		      $this->email->subject('Reset your password');
		      $this->email->message($message);
		      if($this->email->send())
		     {
		      echo 'success';
		     }
		     else
		    {
		     show_error($this->email->print_debugger());
		    }

	}
	function logout(){
		$this->session->unset_userdata('u_inf');
		redirect('site/login');
	}
	function changpwd(){
		$o_pwd=$this->input->post("o_pwd");
		$n_pwd=$this->input->post("n_pwd");
		if($this->session->userdata('u_inf')){
			$member_id=$this->session->userdata("u_inf")->member_id;
			$memrow=$this->db->query("SELECT * FROM ospos_member WHERE member_id='".$member_id."'")->row();
			if(md5($o_pwd)==$memrow->password){
				$this->db->where("member_id",$member_id)->set('password',md5($n_pwd))->update("ospos_member");
				echo "Password Has Change success";
			}else{
				echo "Your Old Password Not match.";
			}
		}else{
			echo "You are Not Log in .";
		}
	}
	function register(){
		$data=array('last_name'=>$this->input->post('last_name'),
					'first_name'=>$this->input->post('first_name'),
					'gender'=>$this->input->post('gender'),
					'email'=>$this->input->post('email'),
					'address'=>$this->input->post('address'),
					'password'=>md5($this->input->post('password')),

					);
		$count=$this->db->query("SELECT count(*) as count FROM ospos_member WHERE email='".$this->input->post('email')."'")->row()->count;
		if($count>0){
			redirect("site/login?reg=error");

		}else{
			$this->db->insert('ospos_member',$data);
			redirect("site/login?reg=success");
		}
	}
	function savenum(){
		$page_num=$this->input->post('num');
		if($this->session->userdata('u_inf')){
			$member_id=$this->session->userdata('u_inf')->member_id;
			$this->db->where('member_id',$member_id)->set('page_num',$page_num)->update('ospos_member');
		}
	}
	function page()
	{
		$pageid=$_GET['p'];
		$data['page']=$this->db->where('pageid',$pageid)->get('ospos_page')->row();
		$this->load->view('site/header');
		$this->load->view('site/page',$data);
		$this->load->view('site/footer');
	}
	function detail($item_id){
		$row=$this->db->query("SELECT * FROM ospos_items i
								INNER JOIN ospos_makes m 
								ON i.make_id=m.make_id 
								INNER JOIN ospos_models mo 
								ON i.model_id=mo.model_id WHERE i.item_id='$item_id'")->row();
		$data['row']=$row;
		$this->load->view('site/header');
		$this->load->view('site/leftside',$data);

		$this->load->view('site/item_detail',$data);
		$this->load->view('site/footer');
	}
	function viewimage($item_id){
		$row=$this->db->query("SELECT * FROM ospos_items i
								INNER JOIN ospos_makes m 
								ON i.make_id=m.make_id 
								INNER JOIN ospos_models mo 
								ON i.model_id=mo.model_id WHERE i.item_id='$item_id'")->row();
		$data['row']=$row;
		$this->load->view('site/header');
		$this->load->view('site/leftside',$data);
		
		$this->load->view('site/view_allimg',$data);
		$this->load->view('site/footer');
	}
	function savesearch(){
		
		if($this->session->userdata('u_inf')){
			$data=array('make_id'=>$this->input->post('make'),
					'model_id'=>$this->input->post('model'),
					'color_id'=>$this->input->post('color'),
					'year'=>$this->input->post('year'),
					'name'=>$this->input->post('name'),
					'search_name'=>$this->input->post('savename'),
					'member_id'=>$this->session->userdata('u_inf')->member_id
					);
			$this->db->insert('ospos_savesearch',$data);
		}
	}
	function search()
	{
		$part_name='';
		$make_id='';
		$model_id='';
		$s_year='';
		$color_id='';
		// $is_pro='';
		// $is_feat='';
		// $is_new='';
		$where='';
		$pro_sta='';
		$page_num=10;
		if(isset($_GET['p_num']) && $_GET['p_num']!='undefined' && $_GET['p_num']!='0' && $_GET['p_num']!='')
			$page_num=$_GET['p_num'];
		$data['title']="";

		if(isset($_GET['n'])){
			$part_name=$_GET['n'];
			$data['title']="Search Results";
		}
		if(isset($_GET['pro'])){
			$data['title']="Products";
			$pro_sta="&pro=1";

		}
		if($this->session->userdata('u_inf')){
			$member_id=$this->session->userdata('u_inf')->member_id;
			$num=$this->db->query("SELECT page_num FROM ospos_member WHERE member_id='$member_id'")->row()->page_num;
			if($num>0 && $num!='')
				$page_num=$num;
		}


		if(isset($_GET['m']) && $_GET['m']!='' && $_GET['m']!='undefined' && $_GET['m']!='0'){
			$where.=" AND i.make_id='".$_GET['m']."'";
			$make_id=$_GET['m'];
		}
		if(isset($_GET['mo']) && $_GET['mo']!=''&& $_GET['mo']!='undefined' && $_GET['mo']!='0'){
			$where.=" AND i.model_id='".$_GET['mo']."'";
			$model_id=$_GET['mo'];

		}
		if(isset($_GET['y']) && $_GET['y']!='' && $_GET['y']!='_' && $_GET['y']!='undefined' && $_GET['y']!='0'){
			$arryear=explode('_', $_GET['y']);
            $where_year=' AND(';
            for ($i=0; $i < count($arryear) ; $i++) { 
                    $where_year.="i.year='".$arryear[$i]."'";
                    if($i<count($arryear)-1)
                        $where_year.=' OR ';
                
            }
            $where_year.=')';
            $s_year=$_GET['y'];
            $where.=$where_year;

		}
		if(isset($_GET['c']) && $_GET['c']!='' && $_GET['c']!='_' && $_GET['c']!='undefined' && $_GET['c']!='0'){
			$arrcol=explode('_', $_GET['c']);
            $where_color=' AND(';
            for ($i=0; $i < count($arrcol) ; $i++) { 
                    $where_color.="i.color_id='".$arrcol[$i]."'";
                    if($i<count($arrcol)-1)
                        $where_color.=' OR ';
                
            }
            $where_color.=')';
            $color_id=$_GET['c'];
            $where.=$where_color;

		}
		if(isset($_GET['ne'])){
			$where.=" AND i.is_new='1'";
			$pro_sta.='&ne=1';
			$data['title']="New Products";

		}
		elseif (isset($_GET['f'])){
			$where.=" AND i.is_feat='1'";
			$pro_sta.='&f=1';
			$data['title']="Featured Products";


		}
		elseif (isset($_GET['pr'])){
			$where.=" AND i.is_pro='1'";
			$pro_sta.='&pr=1';
			$data['title']="Promotion Products";


		}

		$sql="SELECT * 
			 FROM ospos_items i 
			 LEFT JOIN ospos_makes m 
			 ON(i.make_id=m.make_id)
			 LEFT JOIN ospos_models mo 
			 ON(i.model_id=mo.model_id)
			 LEFT JOIN ospos_colors c 
			 ON(i.color_id=c.color_id)
			 where i.deleted=0  
			 and (i.name LIKE '%$part_name%' OR m.make_name LIKE '%$part_name%' OR mo.model_name LIKE '%$part_name%' OR c.color_name LIKE '%$part_name%' OR i.year like '%$part_name%' OR i.description LIKE '%$part_name%') 
			 {$where} order by item_id DESC";
		$totalrow=$this->db->query($sql)->result();
		// ++++++++++++++++pagination++++++++++++
		$per_page='';
		if(isset($_GET['per_page']))
		$per_page=$_GET['per_page'];
		$config['base_url']=site_url("site/search?n=$part_name&m=$make_id&mo=$model_id&y=$s_year&c=$color_id&p_num=$page_num".''."$pro_sta");
		$config['per_page']=$page_num;
		$config['full_tag_open'] = '<li id="pg">';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a><u>';
		$config['cur_tag_close'] = '</u></a>';
		$config['page_query_string']=TRUE;
		$config['num_link']=3;
		$config['total_rows']=count($totalrow);
		$this->pagination->initialize($config);

		$limi=" limit ".$config['per_page'];
		if($per_page>0){
			$limi=" limit ".$per_page.",".$config['per_page'];
		}
		$sql.=" {$limi}";
		$data['data']=$this->db->query($sql)->result();
		$data['se']=1;
		$this->load->view('site/header',$data);
		$this->load->view('site/leftside_new',$data);
		$this->load->view('site/search_result_new',$data);
		$this->load->view('site/footer');
	}
	function about()
	{
		$data['status']='About Us';
		$this->load->view('site/header');
		// $this->load->view('site/newproduct',$data);
		$this->load->view('site/page',$data);
		$this->load->view('site/footer');
	}
	function gemodel(){
		$make_id=$this->input->post('make');
		$model_id=$this->input->post('model');
		$color_id=$this->input->post('color');
		$year=$this->input->post('year');
		$name=$this->input->post('name');
		$where='';
		$model_check='';
		if($model_id=='') $model_check='checked';
		$modeldata="<li><label><input type='radio' onclick='getmodel(event,0);'  name='ckmodel' class='ckmodel' $model_check value=''> All</label></li>";
		$arr=array();
		$wheremodel='';
		if($make_id!='')
			$wheremodel=" AND make_id='$make_id'";
		$data=$this->db->query("SELECT * FROM ospos_models WHERE deleted='0' {$wheremodel}")->result();
		foreach ($data as $mo) {
			$count=$this->sit->getnumsearch('model_id',$name,$make_id,$mo->model_id,$year,$color_id,'','','');
			$sele='';
			if($model_id==$mo->model_id)
					$sele='checked';
			if($count>0)
				$modeldata.="<li rel='$mo->make_id'><label><input $sele onclick='getmodel(event,0);' type='radio' name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name ($count)</label></li>";# code...
		}
		$year_check='';
		if($year=='' || $year=='_') $year_check='checked';
		$yeardata='';
		$yeararr=explode('_', $year);
		$arrofyear=array();
		for ($i=0; $i <count($yeararr) ; $i++) { 
			$arrofyear[$yeararr[$i]]=$yeararr[$i];
		}
		$yeardata="<li><label><input type='checkbox' onclick='getmodel(event,0);' $year_check name='ckyear' class='ckyear' value=''> All</label></li>";
		for($j=date('Y');$j>=1980;$j--){
				$sele='';
				$count=$this->sit->getnumsearch('year',$name,$make_id,$model_id,$j,$color_id,'','','');

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count>0)
					$yeardata.= "<li><label><input onclick='getmodel(event,0);' type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j ($count)</label></li>";# code...
				
		}
		$color_check='';
		if($color_id=='' || $color_id=='_') $color_check='checked';
		$colordata='';

		$colorarr=explode('_', $color_id);
		$arrofcolor=array();
		for ($i=0; $i <count($colorarr) ; $i++) { 
			$arrofcolor[$colorarr[$i]]=$colorarr[$i];
		}

		$colordata="<li><label><input type='checkbox' onclick='getmodel(event,0);' $color_check name='ckcolor' class='ckcolor' value=''> All</label></li>";
		$color=$this->db->query("SELECT * FROM ospos_colors WHERE deleted=0")->result();
			foreach ($color as $col) {
				$sele='';
				$count=$this->sit->getnumsearch('color_id',$name,$make_id,$model_id,$year,$col->color_id,'','','');

				if(isset($arrofcolor[$col->color_id]))
					$sele='checked';
				if($count>0)
					$colordata.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ckcolor' class='ckcolor' value='$col->color_id'/> $col->color_name ($count)</label></li>";# code...
			}
		$arr['model']=$modeldata;
		$arr['year']=$yeardata;
		$arr['color']=$colordata;
		header("Content-type:text/x-json");
		echo json_encode($arr);

	}
	function gemodel_pos(){
		$make_id=$this->input->post('make');
		$model_id=$this->input->post('model');
		$in_color_id=$this->input->post('in_color');
		$ex_color_id=$this->input->post('ex_color');
		$year=$this->input->post('year');
		$name=$this->input->post('name');
		$where='';
		$model_check='';
		if($model_id=='') $model_check='checked';
			$modeldata="<li><label><input type='radio' onclick='getmodel(event,0);'  name='ckmodel' class='ckmodel' $model_check value=''> All</label></li>";
		$arr=array();
		$wheremodel='';
		if($make_id!='')
			$wheremodel=" AND make_id='$make_id'";
		$data=$this->db->query("SELECT * FROM ospos_models WHERE deleted='0' {$wheremodel}")->result();
		foreach ($data as $mo) {
			$count=$this->vinnu->getnumsearch('model_id',$name,$make_id,$mo->model_id,$year);
			$sele='';
			if($model_id==$mo->model_id)
					$sele='checked';
			// if($count>0)
				$modeldata.="<li rel='$mo->make_id'><label><input $sele onclick='getmodel(event,0);' type='radio' name='ckmodel' class='ckmodel' value='$mo->model_id'/> $mo->model_name($count)</label></li>";# code...
		}
		$year_check='';
		if($year=='' || $year=='_') $year_check='checked';
		$yeardata='';
		$yeararr=explode('_', $year);
		$arrofyear=array();
		$where_year=' AND(';
		for ($i=0; $i <count($yeararr) ; $i++) { 
			$arrofyear[$yeararr[$i]]=$yeararr[$i];
			$where_year.="v.year='".$yeararr[$i]."'";
                    if($i<count($yeararr)-1)
                        $where_year.=' OR ';
		
		}
		$where_year.=')';
		$yeardata="<li><label><input type='checkbox' onclick='getmodel(event,0);' $year_check name='ckyear' class='ckyear' value=''> All</label></li>";
		for($j=date('Y');$j>=1980;$j--){
				$sele='';
				$count=$this->sit->getnumsearch('year',$name,$make_id,$model_id,$j,$color_id);

				if(isset($arrofyear[$j]))
					$sele='checked';
				if($count>0)
					$yeardata.= "<li><label><input onclick='getmodel(event,0);' type='checkbox' $sele name='ckyear' class='ckyear' value='$j'/> $j ($count)</label></li>";# code...
				
		}
		$whereinc='';
		$in_color_check='';
		if($in_color_id=='' || $in_color_id=='_') $in_color_check='checked';
		$in_colordata='';

		$in_colorarr=explode('_', $in_color_id);
		$in_arrofcolor=array();
		for ($i=0; $i <count($in_colorarr) ; $i++) { 
			$in_arrofcolor[$in_colorarr[$i]]=$in_colorarr[$i];
		}

		if ($make_id!='' || $model_id !='') {
			$whereinc .=" AND v.make_id='$make_id' AND v.model_id='$model_id'";
		}
		if ($year!='_') {
			$whereinc .= $where_year;
		}
		$in_colordata="<li><label><input type='checkbox' onclick='getmodel(event,0);' $in_color_check name='ck-incolor' class='ckcolor' value=''> All</label></li>";
		$in_color=$this->db->query("SELECT * FROM ospos_colors c
								INNER JOIN ospos_vinnumbers v ON v.interior_color=c.color_id
								WHERE c.deleted=0 {$whereinc} GROUP BY c.color_id")->result();
			foreach ($in_color as $in_col) {
				$sele='';
				$count=$this->sit->getnumsearch('color_id',$name,$make_id,$model_id,$year,$in_col->color_id);

				if(isset($in_arrofcolor[$in_col->color_id]))
					$sele='checked';
				if($count>0)
					$in_colordata.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ck-incolor' class='ckcolor' value='$in_col->color_id'/> $in_col->color_name($count)</label></li>";# code...
			}

		$ex_color_check='';
		if($ex_color_id=='' || $ex_color_id=='_') $ex_color_check='checked';
		$ex_colordata='';

		$ex_colorarr=explode('_', $ex_color_id);
		$ex_arrofcolor=array();
		for ($i=0; $i <count($ex_colorarr) ; $i++) { 
			$ex_arrofcolor[$ex_colorarr[$i]]=$ex_colorarr[$i];
		}
		$whereexc='';
		if ($make_id!='' || $model_id !='') {
			$whereexc .=" AND v.make_id='$make_id' AND v.model_id='$model_id'";
		}
		if ($year != '_' ) {
			$whereexc .= $where_year;
		}
		$ex_colordata="<li><label><input type='checkbox' onclick='getmodel(event,0);' $ex_color_check name='ck-excolor' class='ckcolor' value=''> All</label></li>";
		$ex_color=$this->db->query("SELECT * FROM ospos_colors c
								INNER JOIN ospos_vinnumbers v ON v.external_color=c.color_id
								WHERE c.deleted=0 {$whereexc} GROUP BY c.color_id")->result();
			foreach ($ex_color as $ex_col) {
				$sele='';
				$count=$this->sit->getnumsearch('color_id',$name,$make_id,$model_id,$year,$ex_col->color_id);

				if(isset($ex_arrofcolor[$ex_col->color_id]))
					$sele='checked';
				if($count>0)
					$ex_colordata.= "<li><label><input type='checkbox' onclick='getmodel(event,0);' $sele name='ck-excolor' class='ckcolor' value='$ex_col->color_id'/> $ex_col->color_name($count)</label></li>";# code...
			}
		$arr['model']=$modeldata;
		$arr['year']=$yeardata;
		$arr['in_color']=$in_colordata;
		$arr['ex_color']=$ex_colordata;
		header("Content-type:text/x-json");
		echo json_encode($arr);

	}
	
	function contact()
	{
		$data['status']='Contact Us';
		$this->load->view('site/header');
		// $this->load->view('site/newproduct',$data);
		$this->load->view('site/page',$data);
		$this->load->view('site/footer');
	}
	function product()
	{
		
		$data['se']=1;
		$data['title']="Products";
		$this->load->view('site/header',$data);
		// $this->load->view('site/newproduct',$data);
		$this->load->view('site/feature_product',$data);
		$this->load->view('site/footer');
	}
	function getfeatured(){
		$perpage=$this->input->post('perpage');
		$part_name=$this->input->post('part_name');
		$make=$this->input->post('make');
		$model=$this->input->post('model');
		$s_year=$this->input->post('s_year');
		$t_year=$this->input->post('t_year');
		$s_price=$this->input->post('s_price');
		$t_price=$this->input->post('t_price'); 
		$s=$this->input->post('s'); 
		$where='';
		if($part_name!='')
			$where.=" AND name LIKE '%$part_name%'";
		if($make!='')
			$where.=" AND make_id = '$make'";
		if($model!='')
			$where.=" AND model_id = '$model_id'";
		if($s_year!='' && $t_year!='')
			$where.=" AND year BETWEEN '$s_year' AND '$t_year'";
		if($s_price!='' && $t_price!='')
			$where.=" AND unit_price BETWEEN '$s_price' AND '$t_price'";
		if($s!='')
			$where.=" AND (is_new=1 OR is_feat=1 OR is_pro=1)";
		else
			$where.=" AND is_feat=1";

		$sql="SELECT * FROM ospos_items where deleted=0 {$where}";
		$table='';
		$pagina='';
		$paging=$this->green->p_ajax_pagination(count($this->db->query($sql)->result()),site_url("site/getfeatured"),$perpage);
		$i=1;
		$limit=" LIMIT {$paging['start']}, {$paging['limit']}";
		$sql.=" {$limit}";
		foreach ($this->db->query($sql)->result() as $f_p) {
			$p_name=$f_p->name;
			if (strlen($f_p->name) > 15)
			   $p_name = substr($f_p->name, 0, 15) . '...';


			$img=$this->sit->getdefimg($f_p->item_id);
			if($img=='')
				$img='no_img';
			$img_path=base_url('assets/site/image/no_img.png');
			if(file_exists(FCPATH."/uploads/thumb/$img"))
                $img_path=base_url("/uploads/thumb/$img");
			if(($i-1)%3==0)
				$table.="</ul><ul class='col-sm-12 list'>";
			$table.="<li class='col-sm-4'"; if($i%3==0) $table.="style='border-right:none;'";
					$table.=">
					<img class='p_img' src='".$img_path."'>
					<label class='col-sm-12 label-control p_name'>".$p_name."</label>
					<div class='col-sm-5'></div>
					<div class='col-sm-7 details'>
						<a href='".site_url('site/detail').'/'.$f_p->item_id."'><img src='".site_url('../assets/site/image/details.png')."'></a>
					</div>
				</li>";
			$i++;
			}
		$arr['data']=$table;
		$arr['pagina']=$paging;
		header("Content-type:text/x-json");
		echo json_encode($arr);
	}

	function getnew(){
		$perpage=3;
		$sql="SELECT * FROM ospos_items where is_new=1 AND deleted=0";
		$table='';
		$pagina='';
		$paging=$this->green->p_ajax_pagination(count($this->db->query($sql)->result()),site_url("site/getfeatured"),$perpage);
		$i=1;
		$limit=" LIMIT {$paging['start']}, {$paging['limit']}";
		$sql.=" {$limit}";
		foreach ($this->db->query($sql)->result() as $f_p) {
			$p_name=$f_p->name;
			if (strlen($f_p->name) > 15)
			   $p_name = substr($f_p->name, 0, 15) . '...';
			$img=$this->sit->getdefimg($f_p->item_id);
			if($img=='')
				$img='no_img';
			// echo $img;
			$img_path=base_url('assets/site/image/no_img.png');
			if(file_exists(FCPATH."/uploads/thumb/$img"))
                $img_path=base_url("/uploads/thumb/$img");
			if(($i-1)%3==0)
				$table.="</ul><ul class='col-sm-12 list'>";
			$table.="<li class='col-sm-4'"; if($i%3==0) $table.="style='border-right:none;'";
					$table.=">
					<img class='p_img' src='".$img_path."'>
					<label class='col-sm-12 label-control p_name'>".$p_name."</label>
					<div class='col-sm-5'></div>
					<div class='col-sm-7 details'>
						<a href='".site_url('site/detail').'/'.$f_p->item_id."'><img src='".site_url('../assets/site/image/details.png')."'></a>
					</div>
				</li>";
			$i++;
			}
		$arr['data']=$table;
		$arr['pagina']=$paging;
		header("Content-type:text/json");
		echo json_encode($arr);
	}
	function getmodels(){
		$make_id=$this->input->post('make_id');
		$last_model_id = $this->input->post('last_model_id');
		// $data="<option value=''>Any";

		foreach ($this->sit->getmodel($make_id) as $m) {
			$sel='';

            if($last_model_id==$m->model_id){
            	$sel='selected';

            }
				$data.="<option $sel value='".$m->model_id."'>".$m->model_name."</option>";
			# code...
		}
		$re['data'] = $data;
		header("Content-type:text/x-json");
		echo json_encode($data);
	}
	function qr_code(){
		$item_id = $_GET['id'];
		$data['id'] = $item_id;
		$data['item'] = $this->db->query("SELECT * FROM ospos_items WHERE item_id = '$item_id' AND deleted=0")->row();

		$this->load->view('site/qr_view',$data);
	}
}