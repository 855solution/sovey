<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Khmernames extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('khmernames');
	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/khmernames/index');
		$config['total_rows'] = $this->Khmername->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_khmernames_manage_table( $this->Khmername->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('khmernames/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_khmernames_manage_table_data_rows($this->Khmername->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Khmername->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$khmername_id = $this->input->post('row_id');
		$data_row=get_khmername_data_row($this->Khmername->get_info($khmername_id),$this);
		echo $data_row;
	}

	function view($khmername_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['khmername_info']=$this->Khmername->get_info($khmername_id);

		$this->load->view("khmernames/form",$data);
	}
	
	function save($khmername_id=-1)
	{
		$cur_date = date("Y-m-d H:i:s");
		$emp_id = $this->session->userdata('person_id');
		$khmername_id = $this->input->post('khmer_id');

		$khmername_data = array(
		'khmername_name'=>$this->input->post('khmername_name'),
		'english_name'=>$this->input->post('english_name'),
		'description'=>$this->input->post('description'),
		'date_create'=>$cur_date,
		'emp_id'=>$emp_id
		);
		// echo $khmername_id;die();
		$kh = $this->input->post('khmername_name');
		$en = $this->input->post('english_name');

		// $data['name'] = $kh.'-'.$en;
		$where = '';
		if ($khmername_id!='') {
			$where.=" AND khmername_id<>$khmername_id";
		}
		$ckh = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_name='$kh' {$where} AND deleted=0")->result();
		$cen = $this->db->query("SELECT * FROM ospos_khmernames WHERE english_name='$en' {$where} AND deleted=0")->result();
		if (count($ckh)>0) {
			$this->session->set_flashdata('er','Khmername Already Exist !');
			redirect(site_url('khmernames'));
		}
		if (count($cen)>0) {
			$this->session->set_flashdata('er','English Name Already Exist !');
			redirect(site_url('khmernames'));
		}
		
		if( $this->Khmername->save( $khmername_data, $khmername_id ) )
		{
			//New khmername
			if($khmername_id==-1)
			{
				// echo json_encode(array('success'=>true,'message'=>$this->lang->line('khmernames_successful_adding').' '.
				// $khmername_data['khmername_name'],'khmername_id'=>$khmername_data['khmername_id']));
				$khmername_id = $khmername_data['khmername_id'];
				$this->upload($khmername_id);
			}
			else //previous khmername
			{
				// echo json_encode(array('success'=>true,'message'=>$this->lang->line('khmernames_successful_updating').' '.
				// $khmername_data['khmername_name'],'khmername_id'=>$khmername_id));

				$this->upload($khmername_id);

			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('khmernames_error_adding_updating').' '.
			$khmername_data['khmername_name'],'khmername_id'=>-1));
		}
		redirect(site_url('khmernames'));
	}
	function save_new()
	{
		$cur_date = date("Y-m-d H:i:s");
		$emp_id = $this->session->userdata('person_id');
		$khmername_id = $this->input->post('khmer_id');

		$khmername_data = array(
		'khmername_name'=>$this->input->post('khmername_name'),
		'english_name'=>$this->input->post('english_name'),
		'description'=>$this->input->post('description'),
		'date_create'=>$cur_date,
		'emp_id'=>$emp_id
		);
		// echo $khmername_id;die();
		$kh = $this->input->post('khmername_name');
		$en = $this->input->post('english_name');

		// $data['name'] = $kh.'-'.$en;
		$where = '';
		if ($khmername_id!='') {
			$where.=" AND khmername_id<>$khmername_id";
		}
		$ckh = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_name='$kh' {$where} AND deleted=0")->result();
		$cen = $this->db->query("SELECT * FROM ospos_khmernames WHERE english_name='$en' {$where} AND deleted=0")->result();
		if (count($ckh)>0) {
			$this->session->set_flashdata('er','Khmername Already Exist !');
			redirect(site_url('khmernames'));
		}
		if (count($cen)>0) {
			$this->session->set_flashdata('er','English Name Already Exist !');
			redirect(site_url('khmernames'));
		}
		if ($khmername_id!='') {
			$old_img = $this->db->query("SELECT * FROM ospos_khmername_images WHERE khmername_id = '$khmername_id'")->result();
			// var_dump($old_img);die();
			foreach ($old_img as $img) {
				unlink('./uploads/khmername/'.$img->imagename);
				unlink('./uploads/khmername/thumb/'.$img->imagename);
				$this->db->where('image_id',$img->image_id)->delete('ospos_khmername_images');
			}
		}

		if( $this->Khmername->save( $khmername_data, $khmername_id ) )
		{
			//New khmername
			// if($khmername_id==-1)
			// {
			// 	// echo json_encode(array('success'=>true,'message'=>$this->lang->line('khmernames_successful_adding').' '.
			// 	// $khmername_data['khmername_name'],'khmername_id'=>$khmername_data['khmername_id']));
			// 	$khmername_id = $khmername_data['khmername_id'];
			// 	$this->upload($khmername_id);
			// }
			// else //previous khmername
			// {
			// 	// echo json_encode(array('success'=>true,'message'=>$this->lang->line('khmernames_successful_updating').' '.
			// 	// $khmername_data['khmername_name'],'khmername_id'=>$khmername_id));

			// 	$this->upload($khmername_id);

			// }
			$this->upload($khmername_id);
		}
		else//failure
		{
			// echo json_encode(array('success'=>false,'message'=>$this->lang->line('khmernames_error_adding_updating').' '.
			// $khmername_data['khmername_name'],'khmername_id'=>-1));
		}
		redirect(site_url('khmernames'));
	}

	function delete()
	{
		$this->permission_lib->checkPermission();
		$khmernames_to_delete=$this->input->post('ids');

		if($this->Khmername->delete_list($khmernames_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('khmernames_successful_deleted').' '.
			count($khmernames_to_delete).' '.$this->lang->line('khmernames_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('khmernames_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	// ================================new upload==========================
	function upload($productid)
	{       

	    $this->load->library('upload');
	    $orders=$this->input->post('order');
	    $updimg=$this->input->post('updimg');
		$arrid=$this->input->post('deleteimg');
		$arrid=trim($arrid,',');
		$arr=explode(',',$arrid);
		// if($arrid!=''){
		// 	for ($i=1; $i < count($arr); $i++) {
		// 		$row=$this->db->query("SELECT * FROM ospos_khmername_images where image_id='".$arr[$i]."'")->row();
		// 		if(isset($row->imagename)){
		// 			unlink('./uploads/khmername/'.$row->imagename);
		// 			unlink('./uploads/khmername/thumb/'.$row->imagename);
		// 			$this->db->where('image_id',$row->image_id)->delete('ospos_khmername_images');
		// 		}
		// 	}
		// }
		
		// DELETE OLD IMAGE
		// echo $arrid;
	 //    $this->unlinkpic($productid,$arrid);
	    $files = $_FILES;
	    // var_dump($files);die();
	    $cpt = count($_FILES['userfile']['name']);
	    for($i=0; $i<$cpt; $i++)
	    {         
	    	
	    	// if(isset($updimg[$i]) && $updimg[$i]!=''){
	    	// 	$this->updatepic($updimg[$i],$orders[$i]);
	    	// }  
	    	// echo $orders[$i];
	        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
	        
	        $this->upload->initialize($this->set_upload_options($productid,$_FILES['userfile']['name']));
	        // $this->upload->do_upload();
	        // var_dump($_FILES);die();
	        if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());	
				 // echo $error['error'];		die();
			}else{	
				// echo $_FILES['userfile']['name'];
				$this->creatthumb($productid,$_FILES['userfile']['name'],$orders[$i]);
			}
	    }
	}
	function updatepic($picid,$order){
		$this->db->where('image_id',$picid)->set('first_image',$order)->update('ospos_khmername_images');
	}
	function creatthumb($productid,$imagename,$order){
			$data = array('upload_data' => $this->upload->data());
		 	$config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = './uploads/khmername/thumb';
            $config2['maintain_ratio'] = false;
            $config2['create_thumb'] = "$productid".'_name_'."$imagename";
            $config2['thumb_marker'] = false;
            $config2['height'] = 190;
            $config2['width'] = 195;
            $this->load->library('image_lib');
            $this->image_lib->initialize($config2); 
            if ( ! $this->image_lib->resize()){
            	echo $this->image_lib->display_errors();
			}else{
				$this->saveimg($productid,$this->upload->file_name,$order);
			}
		
	}
	private function set_upload_options($productid,$imagename)
	{   
	    //upload an image options
	    if(!file_exists('./uploads/khmername/') || !file_exists('./uploads/khmername/')){
		    if(mkdir('./uploads/khmername/',0755,true)){
		        return true;
		    }
	    	if(mkdir('./uploads/khmername/thumb',0755,true)){
		                return true;
		    }
		}
	    $config = array();
	    $config['upload_path'] = './uploads/khmername/';
	    $config['allowed_types'] = 'gif|jpg|jpeg|png';
	    $config['max_size']      = '0';
	    $config['file_name']  	 = "$productid".'_name_'."$imagename";
		$config['overwrite']	 = true;
		$config['file_type']='image/png';

	    return $config;
	}
	function saveimg($productid,$imagename,$order){
		$date=date('Y-m-d H:i:s');
		$user=$this->session->userdata('user_name');
		$count=$this->db->query("SELECT count(*) as count FROM ospos_khmername_images where khmername_id='$productid' AND imagename='$imagename'")->row()->count;
		if($count==0){
			$data=array('khmername_id'=>$productid,
						'imagename'=>$imagename,
						'first_image'=>$order);
			$this->db->insert('ospos_khmername_images',$data);
		}
	}
	function unlinkpic($stockid,$arrids){
		$arrid=trim($arrids,',');
		$arr=explode(',',$arrid);
		if($arrid!=''){
			for ($i=0; $i < count($arr); $i++) {
				$row=$this->db->query("SELECT * FROM ospos_khmername_images where image_id='".$arr[$i]."'")->row();
				if(isset($row->pic_name)){
					unlink('./uploads/khmername/'.$row->imagename);
					unlink('./uploads/khmername/thumb/'.$row->imagename);
					$this->db->where('image_id',$row->image_id)->delete('ospos_khmername_images');
				}
			}
		}
	}

	// ============================================================================

	function view_all_khmername($from){
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_khmernames_manage_table( $this->Khmername->get_all(), $this,$from );
		$this->load->view('khmernames/select_form',$data);
	}
	function search_name_sale(){
		$from_sale =1;
		
		$s = $this->input->post('search');
		$data['manage_table']=get_khmernames_manage_table( $this->Khmername->search($s), $this,$from_sale );

		echo json_encode($data);
	}
	function search_name_item(){
		$s = $this->input->post('search');
		$khname = $this->Khmername->search($s)->result();
		$table_data_row='';
		foreach ($khname as $khmername) {
			$img_path=base_url('assets/site/image/no_img.png');
			$img=$this->Khmername->getimage($khmername->khmername_id);
			    if ($img->khmername_id==$khmername->khmername_id) {
		            if(file_exists(FCPATH."/uploads/khmername/thumb/".$img->imagename)){
		                $img_path=base_url("/uploads/khmername/thumb/".$img->imagename);
		            }
		        }
	        $table_data_row.='<tr>';
			$table_data_row.="<td width='3%'><input type='checkbox' id='khmername_$khmername->khmername_id' value='".$khmername->khmername_id."'/></td>";
			$table_data_row.='<td width="3%"><img style="width:80px;" src="'.$img_path.'"></td>';
			$table_data_row.='<td width="15%">'.$khmername->khmername_name.'</td>';
			$table_data_row.='<td width="15%">'.$khmername->english_name.'</td>';
			$table_data_row.='<td width="20%">'.$khmername->description.'</td>';
			// $url_link = site_url("sales/select_name/$khmername->khmername_id");
			$table_data_row.='<td width="5%">
								<a>
								<div class="small_button">
									<span en="'.$khmername->english_name.'" kh="'.$khmername->khmername_name.'" rel="'.$khmername->khmername_id.'" name="select_btn" id="select_btn" style="font-size:14px;" class="select_btn">Select</span>
								</div>
								</a></td>';	
			$table_data_row.='</tr>';

		}
		$data['manage_table'] = $table_data_row;
		echo json_encode($data);
	}
	function save_other(){
		$s_name = $this->input->post('s_name');
		if ($s_name!='') {
			$khmername_data = array('khmername_name'=>$s_name,'english_name'=>$s_name);
			$this->db->insert('khmernames',$khmername_data);
		}
		$data['kh_id'] = $this->db->insert_id();
		echo json_encode($data);
	}
	// function check_names(){
	// 	$kh = $this->input->post('kh');
	// 	$en = $this->input->post('en');
	// 	$id = $this->input->post('kh_id');

	// 	// $data['name'] = $kh.'-'.$en;
	// 	$where = '';
	// 	if ($id>0) {
	// 		$where.=" AND khmername_id<>$id";
	// 	}
	// 	$data['tkh'] = 'n';
	// 	$data['ten'] = 'n';
	// 	$ckh = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_name='$kh' {$where}")->result();
	// 	$cen = $this->db->query("SELECT * FROM ospos_khmernames WHERE english_name='$en' {$where}")->result();
	// 	if (count($ckh)>0) {
	// 		$data['tkh'] = 'y';
	// 	}
	// 	if (count($cen)>0) {
	// 		$data['ten'] = 'y';
	// 	}
	// 	echo json_encode($data);
	// }

	function index()
	{
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Khmernames';
		$khmername = $this->Khmername->get_all()->result();
		$table = '';
		$i=0;
		foreach ($khmername as $kh) {
			$i++;
			$img_path=base_url('assets/site/image/no_img.png');
			$real_img = $img_path;
			$img=$this->Khmername->getimage($kh->khmername_id);
			    if ($img->khmername_id==$kh->khmername_id) {
		            if(file_exists(FCPATH."/uploads/khmername/thumb/".$img->imagename)){
		                $img_path=base_url("/uploads/khmername/thumb/".$img->imagename);
		                $real_img = base_url("uploads/khmername/".$img->imagename);
		            }
		        }
			$table.="
					<tr>
						<td>$i</td>
						<td><a href='".$real_img."' data-fancybox><img style='width:50px;' src='".$img_path."'></a></td>
						<td>$kh->khmername_name</td>
						<td>$kh->english_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' imgid='$img->image_id' rimg='$real_img' timg='$img_path' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' e='$kh->english_name' k='$kh->khmername_name' f='$kh->khmername_id'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$kh->khmername_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('khmernames/manage',$data);
		$this->load->view('partial/footer');
	}
	function check_name(){
		$kh_en = $this->input->post('name');
		$kh_id = $this->input->post('id');
		$ty = $this->input->post('t');
		$exist = $this->Khmername->check_name($kh_en,$kh_id,$ty);

		// $data['ex'] = $exist;
		echo json_encode($exist);
	}
	function delete_new($khmername_id){
		// DELETE IMAGE
		if ($khmername_id!='') {
			$old_img = $this->db->query("SELECT * FROM ospos_khmername_images WHERE khmername_id = '$khmername_id'")->result();
			// var_dump($old_img);die();
			foreach ($old_img as $img) {
				unlink('./uploads/khmername/'.$img->imagename);
				unlink('./uploads/khmername/thumb/'.$img->imagename);
				$this->db->where('image_id',$img->image_id)->delete('ospos_khmername_images');
			}
		}

		// 
		$this->db->where('khmername_id',$khmername_id)->update('ospos_khmernames',array('deleted'=>1));

		redirect(site_url('khmernames'));
	}
}
?>