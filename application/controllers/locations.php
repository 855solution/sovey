<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Locations extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('locations');
	}
function mobile_locationItems(){
$location_name=$this->input->get('location_name');

$locid=$this->Location->get_location_id($location_name);

$rows=$this->Location->mobile_get_items($locid);
//echo json_encode($locid);
                foreach($rows->result_array() as $row)
		{
			$items['result_array'][] = array('item_id'=>$row['item_id'],
			'name'=>$row['name'],
			'make_name'=>$row['make_name'],
			'model_name'=>$row['model_name'],
			'fit'=>$row['fit'],
			'partplacement_name'=>$row['partplacement_name']);
		}
$items['location_id']=$locid;
		echo json_encode($items);
}
	function index()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/locations/index');
		$config['total_rows'] = $this->Location->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_locations_manage_table( $this->Location->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('locations/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_locations_manage_table_data_rows($this->Location->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Location->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$location_id = $this->input->post('row_id');
		$data_row=get_location_data_row($this->Location->get_info($location_id),$this);
		echo $data_row;
	}

	function view($location_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['location_info']=$this->Location->get_info($location_id);

		$this->load->view("locations/form",$data);
	}
	
	function save($location_id=-1)
	{
		$location_data = array(
		'location_name'=>$this->input->post('location_name'),
		'max_qty'=>$this->input->post('max_qty'),
		'description'=>$this->input->post('description')
		);
		$where='';
		if($location_id>0){
			$where.=" AND location_id<>'$location_id'";
		}
		$exist=$this->db->query("SELECT count(*) as count FROM ospos_locations WHERE location_name='".$this->input->post('location_name')."' {$where} AND deleted='0'")->row()->count;
		if($exist>0){
			echo json_encode(array('success'=>false,'message'=>'Location : '.$location_data['location_name'].' is already exist.','location_id'=>-1));
		}else{
			if( $this->Location->save( $location_data, $location_id ) )
			{
				//New location
				if($location_id==-1)
				{
					echo json_encode(array('success'=>true,'message'=>$this->lang->line('locations_successful_adding').' '.
					$location_data['location_name'],'location_id'=>$location_data['location_id']));
					$location_id = $location_data['location_id'];
				}
				else //previous location
				{
					echo json_encode(array('success'=>true,'message'=>$this->lang->line('locations_successful_updating').' '.
					$location_data['location_name'],'location_id'=>$location_id));
				}
				$this->do_upload($location_id);
			}
			else//failure
			{
				echo json_encode(array('success'=>false,'message'=>$this->lang->line('locations_error_adding_updating').' '.
				$location_data['location_name'],'location_id'=>-1));
			}
		}
		


	}
	function do_upload($slide_id)
		{
			if(!file_exists('./assets/upload/location'))
			 {
			    if(mkdir('./assets/upload/location',0755,true))
			    {
			                return true;
			    }
			 }
			$config['upload_path'] ='./assets/upload/location';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$slide_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('userfile'))
			{
				$error = array('error' => $this->upload->display_errors());			
			}
			// else
			// {		
			// 	//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
			// 	$data = array('upload_data' => $this->upload->data());			
			// 	//redirect('setting/user');
			// 	 	$config2['image_library'] = 'gd2';
   //                  $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
   //                  $config2['new_image'] = './assets/upload/slide_id/thum';
   //                  $config2['maintain_ratio'] = TRUE;
   //                  $config2['create_thumb'] = TRUE;
   //                  $config2['thumb_marker'] = FALSE;
   //                  $config2['width'] = 120;
   //                  $config2['height'] = 180;
   //                  $config2['overwrite']=true;
   //                  $this->load->library('image_lib',$config2); 

   //                  if ( !$this->image_lib->resize()){
	  //               	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
			// 		}else{
			// 			//unlink('./assets/upload/students/'.$student_id.'.jpg');
			// 		}
			// }
		}
	function delete()
	{
		$this->permission_lib->checkPermission();
		$locations_to_delete=$this->input->post('ids');

		if($this->Location->delete_list($locations_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('locations_successful_deleted').' '.
			count($locations_to_delete).' '.$this->lang->line('locations_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('locations_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
}
?>