<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Trim extends Secure_area
{
	function __construct()
	{
		parent::__construct('trim');
		$this->load->model('modtrim','Tr');

	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/trim/index');
		$config['total_rows'] = $this->Tr->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_trim_manage_table( $this->Tr->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('trim/manage',$data);
	}
	function index(){
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Trim';
		$trim = $this->Tr->get_all()->result();
		$table = '';
		$i=0;
		foreach ($trim as $tri) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$tri->trim_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$tri->trim_name' f='$tri->trim_id'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$tri->trim_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('trim/manage',$data);
		$this->load->view('partial/footer');
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_makes_manage_table_data_rows($this->Tr->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Tr->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$trim_id = $this->input->post('row_id');
		$data_row=get_trim_data_row($this->Tr->get_info($trim_id),$this);
		echo $data_row;
	}

	function view($trim_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['trim_info']=$this->Tr->get_info($trim_id);

		$this->load->view("trim/form",$data);
	}
	
	// function save($trim_id=-1)
	// {
	// 	$trim_data = array(
	// 	'trim_name'=>$this->input->post('trim_name'),
	// 	'deleted'=>0
	// 	);

	// 	$c = $this->input->post('trim_name');
	// 	$msg='';

	// 	$where= '';
	// 	if ($trim_id!=-1) {
	// 		$where.= " AND trim_id <> $trim_id";
	// 	}
	// 	$sql = $this->db->query("SELECT * FROM ospos_trim WHERE trim_name = '$c' AND deleted=0 {$where}")->result();

	// 	if ($sql) {
	// 		$e = 1;
	// 	}else{
	// 		$e = 0;
	// 	}
	// 	if ($e==1) {
	// 		$this->session->set_flashdata('er','Name Already Exist!');
	// 		redirect(site_url('trim'));
	// 	}else{
	// 		if ($this->Tr->save($trim_data,$trim_id)) {
	// 			$msg = "success";
	// 		}else{
	// 			$msg = "Failed";
	// 		}
	// 	}
		
	// 	// echo $msg;
	// 	redirect(site_url('trim'));
	// }
	function save(){
		$name = $this->input->post('trim_name');
		$id = $this->input->post('trim_id');
		$exist = $this->Tr->check_name($name,$id);
		$trim_data = array(
						'trim_name'=>$name,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('trim'));
		}else{
			$this->Tr->save($id,$trim_data);
			redirect(site_url('trim'));

		}
	}

	// function delete()
	// {
	// 	$this->permission_lib->checkPermission();
	// 	$trim_to_delete=$this->input->post('ids');

	// 	if($this->Tr->delete_list($trim_to_delete))
	// 	{
	// 		echo json_encode(array('success'=>true,'message'=>$this->lang->line('Trim_successful_deleted').' '.
	// 		count($trim_to_delete).' '.$this->lang->line('trim_one_or_multiple')));
	// 	}
	// 	else
	// 	{
	// 		echo json_encode(array('success'=>false,'message'=>$this->lang->line('trim_cannot_be_deleted')));
	// 	}
	// }
	function delete($id){
		$this->db->where('trim_id',$id)->update('ospos_trim',array('deleted'=>1));
		redirect(site_url('trim'));
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function check_exist(){
		$c = $this->input->post('name');
		$id = $this->input->post('id');
		$w = '';
		if ($id!=-1 && $id!='') {
			$w .= " AND trim_id <> $id";
		}

		$sql = $this->db->query("SELECT * FROM ospos_trim WHERE trim_name = '$c' AND deleted=0 {$w}")->result();

		if ($sql) {
			$e = 1;
		}else{
			$e = 0;
		}
		echo json_encode($e);
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->Tr->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
}
?>