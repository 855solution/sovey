<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class permission extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('modpermission','perm');
		$this->load->model('employee','emp');
	}
	function index(){

		// $numpage=50;
		// if(isset($_GET['p']))
		// 	$numpage=$_GET['p'];
		// $per_page='';
		// if(isset($_GET['per_page']))
		// 	$per_page=$_GET['per_page'];
		// $config['base_url'] = site_url('/permission/index?p='.$numpage);
		// $config['total_rows'] = $this->Item->count_all();
		// $data['total_rows'] = $config['total_rows'];
		// $config['per_page'] = $numpage;
		// $config['uri_segment'] = 3;
		// $config['page_query_string']=TRUE;

		// $this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		
		
	

		$data['permission'] = $this->perm->getpermission();
		$this->load->view('permission/manage',$data);
	}
	function get_detail(){
		$perm_id = $this->input->post('perm_id');
		$perm_data = $this->perm->get_perm($perm_id);
		$perm_detail = $this->perm->get_all_module($perm_id);
			$data['is_edit'] = 1;

		if (!$perm_id) {
			$perm_detail = $this->perm->get_all_module();
			$data['is_edit'] = 0;

		}
		
		$table = '';
		$i=0;
		foreach ($perm_detail as $pd) {
			$i++;
			$chk = '';
			if ($pd->access && $pd->access==1) {
				$chk = 'checked';
			}
			$chk_v = '';
			if ($pd->view==1) {
				$chk_v = 'checked';
			}
			$chk_a = '';
			if ($pd->add==1) {
				$chk_a = 'checked';
			}
			$chk_u = '';
			if ($pd->edit==1) {
				$chk_u = 'checked';
			}
			$chk_d = '';
			if ($pd->delete==1) {
				$chk_d = 'checked';
			}
			$chk_cq = '';
			if ($pd->create_quote==1) {
				$chk_cq = 'checked';
			}
			$chk_cd = '';
			if ($pd->create_deposit==1) {
				$chk_cd = 'checked';
			}
			$chk_cs = '';
			if ($pd->create_sale==1) {
				$chk_cs = 'checked';
			}
			$chk_vd = '';
			if ($pd->view_deposit==1) {
				$chk_vd = 'checked';
			}
			$chk_vq = '';
			if ($pd->view_quote==1) {
				$chk_vq = 'checked';
			}
			$chk_ap = '';
			if ($pd->add_payment==1) {
				$chk_ap = 'checked';
			}
			$chk_ls = '';
			if ($pd->list_sale==1) {
				$chk_ls = 'checked';
			}
			$chk_st = '';
			if ($pd->sold_item==1) {
				$chk_st = 'checked';
			}
			$table.="<tr>
						<td>".$i."</td>
						<td>".$this->lang->line('module_'.$pd->module_id)."</td>
						<td style='display:none;'><input type='hidden' class='m_id' value='".$pd->module_id."'><input ".$chk." type='checkbox' name='m-access' class='m-access'></td>
						<td><input type='checkbox' class='form-control m-view' ".$chk_v."></td>";
						if ($pd->module_id!='check_price') {
							$table.="<td><input type='checkbox' class='form-control m-add' ".$chk_a."></td>
									<td><input type='checkbox' class='form-control m-update' ".$chk_u."></td>
									<td><input type='checkbox' class='form-control m-delete' ".$chk_d."></td>";
						}
			
						if ($pd->module_id=='items') {
							$table.="<td><input type='checkbox' class='form-control m-sold-item' ".$chk_st."></td>";
						}
						if ($pd->module_id=='sales') {
							$table.="
									<td></td>
									<td><input type='checkbox' class='form-control m-create-q' ".$chk_cq."></td>
									<td><input type='checkbox' class='form-control m-create-d' ".$chk_cd."></td>
									<td><input type='checkbox' class='form-control m-create-s' ".$chk_cs."></td>
									<td><input type='checkbox' class='form-control m-view-d' ".$chk_vd."></td>
									<td><input type='checkbox' class='form-control m-view-q' ".$chk_vq."></td>
									<td><input type='checkbox' class='form-control m-add-payment' ".$chk_ap."></td>
									<td><input type='checkbox' class='form-control m-list-sale' ".$chk_ls."></td>
									";
						}
			$table.="</tr>";
		}
		$data['perm_id'] = $perm_id;
		$data['perm_title'] = $perm_data->perm_name;
		$data['perm_desc'] = $perm_data->perm_desc;



		$data['perm_table'] = $table;
		echo json_encode($data);
	}
	function save_detail(){
		$perm_id = $this->input->post('perm_id');
		$is_edit = $this->input->post('is_edit');

		$this->permission_lib->checkPermission($is_edit);

		$access = $this->input->post('perm_detail');
		if ($is_edit ==1) {
			$this->perm->delete_detail($perm_id);
		}
		foreach ($access as $a) {
			 $perm_detail = array('perm_id'=>$perm_id,
			 					'module_id'=>$a['m_id'],
			 					'access'=>$a['access'],
			 					'view'=>$a['view'],
			 					'add'=>$a['add'],
			 					'edit'=>$a['update'],
			 					'delete'=>$a['delete'],
			 					'create_quote'=>$a['create_q'],
			 					'create_deposit'=>$a['create_d'],
			 					'create_sale'=>$a['create_s'],
			 					'view_quote'=>$a['view_quote'],
			 					'view_deposit'=>$a['view_deposit'],
			 					'add_payment'=>$a['add_payment'],
			 					'list_sale'=>$a['list_sale'],
			 					'sold_item'=>$a['sold_item']
			 					);

			 $this->perm->save_detail($perm_detail);
		}
		$data['s'] = 's';
		echo json_encode($data);
	}
	function save_perm(){
		$is_edit = 0;
		$perm_id = $this->input->post('perm_id');
		if ($perm_id) {
			$is_edit = 1;
		}
		$this->permission_lib->checkPermission($is_edit);

		$perm_name = $this->input->post('perm_name');
		$perm_desc = $this->input->post('perm_desc');
		$data = array(
						'perm_name'=>$perm_name,
						'perm_desc'=>$perm_desc
					);

		$this->perm->save_perm($perm_id,$data);

		redirect(site_url('permission'));
	}
	function get_perm(){
		$perm_id = $this->input->post('perm_id');
		$perm = $this->perm->get_perm($perm_id);

		$data['perm_name'] = $perm->perm_name;
		$data['perm_desc'] = $perm->perm_desc;
		$data['perm_id'] = $perm_id;

		echo json_encode($data);
	}
	function delete_perm($perm_id){
		$this->perm->delete_perm($perm_id);

		redirect(site_url('permission'));
	}
	function get_set_user(){
		$user = $this->emp->get_all()->result();
		$perm = $this->perm->get_all_perm();

		$data['all_user'] = '<option value="">Select</option>';
		foreach ($user as $u) {
			$data['all_user'] .= "<option value='".$u->person_id."'>".$u->last_name.' '.$u->first_name."</option>";
		}

		$data['all_perm'] = '<option value="">Select</option>';
		foreach ($perm as $p) {
			$data['all_perm'] .= "<option value='".$p->perm_id."'>".$p->perm_name."</opton>";;
		}


		echo json_encode($data);
	}
	function save_set_user(){
		$this->permission_lib->checkPermission();

		$perm_id = $this->input->post('user_perm_id');
		$user_id = $this->input->post('user_user_id');
		$this->perm->user_permission_exist($perm_id,$user_id);
		if ($perm_id!='' && $user_id!='') {
			$perm_detail = $this->perm->get_perm_detail($perm_id);
			$user_perm_data = array(
								'user_id'=>$user_id,
								'perm_id'=>$perm_id
							);
			$this->perm->save_set_user($user_perm_data);
			// foreach ($perm_detail as $pd) {
			// 	if ($pd->view==1) {
			// 		$pdata = array(
			// 						'module_id'=>$pd->module_id,
			// 						'person_id'=>$user_id
			// 					);
			// 		// $this->perm->save_emp_perm($pdata);
			// 	}
			// }
		}


		echo json_encode($data);
	}


}

?>