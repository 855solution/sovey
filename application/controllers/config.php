<?php
require_once ("secure_area.php");
class Config extends Secure_area 
{
	function __construct()
	{
		parent::__construct('config');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$this->load->view("config");
	}
		
	function save()
	{
		$batch_save_data=array(
		'company'=>$this->input->post('company'),
		'address'=>$this->input->post('address'),
		'phone'=>$this->input->post('phone'),
		'email'=>$this->input->post('email'),
		'fax'=>$this->input->post('fax'),
		'website'=>$this->input->post('website'),
		'default_tax_1_rate'=>$this->input->post('default_tax_1_rate'),		
		'default_tax_1_name'=>$this->input->post('default_tax_1_name'),		
		'default_tax_2_rate'=>$this->input->post('default_tax_2_rate'),	
		'default_tax_2_name'=>$this->input->post('default_tax_2_name'),		
		'currency_symbol'=>$this->input->post('currency_symbol'),
		'return_policy'=>$this->input->post('return_policy'),
		'language'=>$this->input->post('language'),
		'timezone'=>$this->input->post('timezone'),
		'print_after_sale'=>$this->input->post('print_after_sale'),
		'sale_expired'=>$this->input->post('sale_expired'),
		'sale_select_date'=>$this->input->post('sale_select_date'),
		'sale_note'=>$this->input->post('sale_note'),
		'due_note'=>$this->input->post('due_note'),
		'return_note'=>$this->input->post('return_note'),
		'deposit_note'=>$this->input->post('deposit_note'),
		'show_item_quantiy_0'=>$this->input->post('show_item_quantiy_0')
		);
		
		if($_SERVER['HTTP_HOST'] !='ospos.pappastech.com' && $this->Appconfig->batch_save($batch_save_data))
		{
			// echo json_encode(array('success'=>true,'message'=>$this->lang->line('config_saved_successfully')));
			redirect(site_url('config'));
		}
		else
		{
			// echo json_encode(array('success'=>false,'message'=>$this->lang->line('config_saved_unsuccessfully')));
			redirect(site_url('config'));
			
		}
	}
}
?>