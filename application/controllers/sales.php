<?php

require_once ("secure_area.php");
class Sales extends Secure_area
{

	function __construct()
	{
		parent::__construct('sales');
		$this->load->library('sale_lib');
		$this->load->model('item');
		$this->load->model('customer','cus');
		$this->load->model('main_model');
		$this->load->model('past_due');
	}

	function index()
	{
		$this->permission_lib->checkPermission();


		$this->session->unset_userdata('sale_name_id');
		// if ($this->sale_lib->get_mode()=='return' && ($this->session->userdata('q_id')=='' && $this->session->userdata('d_id')=='') && $this->session->userdata('edit_sale_id')=='') {
		if($this->sale_lib->get_mode()=='return'){
			$mode = 'sale';
			// $this->session->unset_userdata('quote');
			// $this->session->unset_userdata('q_id');
			$this->sale_lib->set_mode($mode);
			$this->sale_lib->remove_customer();
			$this->sale_lib->empty_cart();
			$this->sale_lib->empty_payments();
			$this->sale_lib->clear_all();
			$this->_reload();
			
			// $this->remove_all_item();
		}else{

			$this->_reload();
		}
		// echo $this->sale_lib->get_mode();

	}
	
	function item_search()
	{
		//$suggestions = $this->Item->get_item_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		$suggestions = $this->Item->get_item_search_suggestions_sale($this->input->post('q'),$this->input->post('limit'));
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->post('q'),$this->input->post('limit')));
		echo implode("\n",$suggestions);
	}

	function customer_search()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function select_customer($cus_id='')
	{
		$customer_id = $this->input->post("customer");
		if ($cus_id!='') {
			$customer_id=$cus_id;
		}
		$this->sale_lib->set_customer($customer_id);
		$this->_reload();
	}
	// function set_sale_date($date){
	// 	// $date = $this->input->post('sale_date');
	// 	if ($date!='') {
	// 		$this->sale_lib->set_sale_date($date);
	// 	}

	// 	// echo $date;
	// }
	function select_name($name_id='')
	{
		$this->session->set_userdata('sale_name_id',$name_id);
		$this->_reload();
	}


	function change_mode()
	{
		$mode = $this->input->post("mode");
		$this->sale_lib->set_mode($mode);
		$this->_reload();
	}
	
	function set_comment() 
	{
 	  $this->sale_lib->set_comment($this->input->post('comment'));
	}
	
	function set_email_receipt()
	{
 	  $this->sale_lib->set_email_receipt($this->input->post('email_receipt'));
	}

	//Alain Multiple Payments
	function add_payment()
	{		
		
		$this->permission_lib->checkPermission();
		
		$data=array();
		$this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'numeric');
		
		if ($this->form_validation->run() == FALSE)
		{
			if ( $this->input->post('payment_type') == $this->lang->line('sales_gift_card') )
				$data['error']=$this->lang->line('sales_must_enter_numeric_giftcard');
			else
				$data['error']=$this->lang->line('sales_must_enter_numeric');
				
 			$this->_reload($data);
 			return;
		}
		
		$payment_type=$this->input->post('payment_type');
		if ( $payment_type == $this->lang->line('sales_giftcard') )
		{
			$payments = $this->sale_lib->get_payments();
			$payment_type=$this->input->post('payment_type').':'.$payment_amount=$this->input->post('amount_tendered');
			$current_payments_with_giftcard = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $current_payments_with_giftcard;
			if ( $cur_giftcard_value <= 0 )
			{
				$data['error']='Giftcard balance is '.to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) ).' !';
				$this->_reload($data);
				return;
			}
			elseif ( ( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ) > 0 )
			{
				$data['warning']='Giftcard balance is '.to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ).' !';
			}
			$payment_amount=min( $this->sale_lib->get_total(), $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) );
		}
		else
		{
			$payment_amount=$this->input->post('amount_tendered');
			//$payment_amount=$this->sale_lib->get_total();
		}
		$emp_id = $this->session->userdata('person_id');
		$emp = $this->Employee->get_info($emp_id);
		$emp_name = $emp->last_name.' '.$emp->first_name;
		if( !$this->sale_lib->add_payment( $payment_type, $payment_amount,date('d/m/Y H:i:s'),$emp_name,$emp_id) )
		{
			$data['warning']='មិនអាចដាក់ Payment បាន។ Payment ត្រូវតែច្រើនជាង 0';
		}
		
		$this->_reload($data);
	}

	//Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$this->sale_lib->delete_payment($payment_id);
		$this->_reload();
	}

	function add()
	{
		$this->permission_lib->checkPermission();
		$data=array();
		$newitem = false;
		$mode = $this->sale_lib->get_mode();
		$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item");
		$quantity = $mode=="sale" ? 1:-1;

		if($this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt))
		{
			$data['warning'] = $this->lang->line('sales_quantity_less_than_zero_cant_sell');

		}
		if($this->sale_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt) && $mode=='return')
		{
			$this->sale_lib->return_entire_sale($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt))
		{
			$this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif(!$this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$newitem,$quantity))
		{
			$data['warning']=$this->lang->line('sales_unable_to_add_item');
		}
		
		
		
		
		$this->_reload($data);
	}

	function edit_item($line=null)
	{
		$data= array();
		$data['no_qty']='';
		$this->form_validation->set_rules('price', 'lang:items_price', 'required|numeric');
		$this->form_validation->set_rules('quantity', 'lang:items_quantity', 'required|numeric');
		$edit_sale_id = $this->session->userdata('edit_sale_id');
		$did = $this->session->userdata('d_id');


        $description = $this->input->post("desc");
        $serialnumber = $this->input->post("serialnumber");
		$price = $this->input->post("price");
		$quantity = $this->input->post("quantity");
		$discount = $this->input->post("discount");
		$desc = $this->input->post("desc");
		$is_new = $this->input->post('is_new');
		$problem = $this->input->post("is_pro");
		$problem2 = $this->input->post("is_pro2");
		$sale_date = $this->input->post('sale_date');


		$this->sale_lib->set_sale_date($sale_date);
	
		// $is_return = $this->input->post("is_return");
		// var_dump($is_return);die();

		// $stock_qty='';

		// if ($is_new==1) {
		// 	$stock_qty = $this->Item->get_new_item_info($this->sale_lib->get_item_id($line))->quantity;
		// }else{
		// 	$stock_qty = $this->Item->get_info($this->sale_lib->get_item_id($line))->quantity;
		// }
		// if ($edit_sale_id) {
		// 	$stock_qty = 1;
		// }



		// if ($this->sale_lib->get_mode()=='sale') {
		// 	if ($stock_qty < $quantity) {
		// 		$quantity = $stock_qty;
		// 		// $this->session->set_flashdata('no_qty',$this->lang->line('sales_quantity_less_than_zero_cant_sell'));
		// 		$data['max_qty']=$stock_qty;
		// 		$data['no_qty'] = $this->lang->line('sales_quantity_less_than_zero_cant_sell');
		// 	}
		// }
		

		if ($problem2!=0) {
			$problem = $problem2;
		}
		if ($this->form_validation->run() != FALSE)
		{
			$this->sale_lib->edit_item($line,$description,$serialnumber,$quantity,$discount,$price,$desc,$problem,$problem2);
			// var_dump($this->sale_lib->get_cart());die();
		}
		else
		{
			$data['error']=$this->lang->line('sales_error_editing_item');
		}
		
		// if($this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line)))
		// {
		// 	$data['warning'] = $this->lang->line('sales_quantity_less_than_zero');

		// }
		if ($did) {
			$this->sale_lib->set_updatable();
		}
		$data['updatable'] = $this->sale_lib->get_updatable();
		$data['problem'] = $problem;
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=to_currency($this->sale_lib->get_total());
		$data['payments']=to_currency($this->sale_lib->get_payments());
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$totalallpayment=$this->sale_lib->get_total();
		$data['subtotal']=to_currency($this->sale_lib->get_subtotal());
		$data['total_row'] = to_currency($quantity * $price - $discount);
		$data['payments_total']=to_currency($this->sale_lib->get_payments_total());
		$data['amount_due']=to_currency($this->sale_lib->get_amount_due());
		$data['amount_due_no_currency'] = $this->sale_lib->get_amount_due();
		$data['amount_tendered']=$this->sale_lib->get_amount_due();
		$data['success'] = "SUCCESS";

		echo json_encode($data);
		// $this->_reload($data);
		// redirect('/sales');
	}
	function remove_all_item(){
		// var_dump($this->sale_lib->get_mode());die();
			$this->sale_lib->set_updatable();

		$edit_sale_id = $this->session->userdata('edit_sale_id');
		$q_id = $this->session->userdata('q_id');
		$d_id = $this->session->userdata('d_id');
		if (!$q_id && !$d_id && !$edit_sale_id) {
			if ($this->sale_lib->get_mode()=='sale') {
				$items=$this->sale_lib->get_cart();
				foreach ($items as $line => $data) {
					
						if ($data['is_new']==1) {
							$this->Sale->delete_new_item($data['item_id']);
						}
				}
			}
		}
		$this->sale_lib->empty_cart();
		$this->_reload();
	}
	function remove_item(){
			$this->sale_lib->set_updatable();

		$lines = $this->input->post('lines');
		foreach ($lines as $l) {
			// if ($l['is_new']==1) {
			// 	$this->Sale->delete_new_item($l['item_id']);
			// }
			$this->sale_lib->delete_item($l);
		}

		$q_id = $this->session->userdata('q_id');

		$data['qid'] = $q_id;
		$data['r'] = 'd';
		echo json_encode($data);

	}
	function delete_item($item_number)
	{
		$this->sale_lib->delete_item($item_number);
		$this->_reload();
	}

	function remove_customer()
	{
		$this->sale_lib->remove_customer();
		$this->_reload();
	}

	function complete()
	{
		$this->permission_lib->checkPermission();
		$cart = $this->sale_lib->get_cart();

		// var_dump($this->sale_lib->get_all_payment());die();
		// $payment = $this->sale_lib->get_payments();
		// var_dump($payment);die();
		// if (!$cart) {
		// 	echo '-1';
		// 	die();
		// }
		// sleep(3);
		$edit_sale_id = $this->session->userdata('edit_sale_id');

		$old_payment  = $this->session->userdata('old_payment');
		$new_payment = $this->session->userdata('payments');
		$all_payment = $new_payment;
		if ($old_payment) {
			if (!$edit_sale_id) {
				$all_payment = array_merge($old_payment,$new_payment);
			}
		}
		// var_dump($all_payment);die();


		$data['cart']=$this->sale_lib->get_cart();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a');
		$customer_id=$this->sale_lib->get_customer()==-1?72:$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		$data['payments']=$all_payment;
		// $data['payments']=$this->sale_lib->get_payments();
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due());
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$data['desc']=$this->input->post('desc');
		$data['cus_info'] =$this->cus->get_info($customer_id);
		$totalallpayment=$this->sale_lib->get_total();
		$q_id = $this->session->userdata('q_id');
		$d_id = $this->session->userdata('d_id');
		
		// var_dump($_POST);die();

		
		$gettotal=$this->sale_lib->get_total();
		$paymentTotal=$this->sale_lib->get_payments_total();
		$changedue=$gettotal-$paymentTotal;



		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
		}

		//SAVE sale to databases
		
		$data['sale_id']=$this->Sale->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'],'',$changedue,$totalallpayment);
		
		// UPDATE quote status
		if ($q_id) {
			$this->db->where('sale_id',$q_id)->update('sales_quote',array('status'=>'complete'));
		}
		// UPDATE deposit status
		if ($d_id) {
			$this->db->where('sale_id',$d_id)->update('sales_suspended',array('status'=>'complete'));
		}

		if ($data['sale_id'] == '-1')
		{
			$data['error_message'] = $this->lang->line('sales_transaction_failed');
		}
		else
		{
			$data['invoice_id'] = $this->Sale->get_invoice_id($data['sale_id']);
			if ($this->sale_lib->get_email_receipt() && !empty($cust_info->email))
			{
				$this->load->library('email');
				$config['mailtype'] = 'html';				
				$this->email->initialize($config);
				$this->email->from($this->config->item('email'), $this->config->item('company'));
				$this->email->to($cust_info->email); 

				$this->email->subject($this->lang->line('sales_receipt'));
				$this->email->message($this->load->view("sales/receipt_email",$data, true));	
				$this->email->send();
			}
		}
		$this->session->unset_userdata('q_id');
		$this->session->unset_userdata('d_id');
		$this->sale_lib->clear_all();
		$this->session->unset_userdata('edit_sale_id');
		
		// $this->load->view("sales/receipt",$data);
		// redirect(site_url('sales/sale_invoice/'.$data['sale_id']));
		echo $data['sale_id'];	
	}
	
	function receipt($sale_id)
	{
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a', strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$data['desc']=$this->input->post('desc');
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		}
		$data['sale_id']='POS '.$sale_id;
		$this->load->view("sales/receipt",$data);
		$this->sale_lib->clear_all();

	}
	
	function edit($sale_id)
	{
		$data = array();

		$data['customers'] = array('' => 'No Customer');
		foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}

		$data['employees'] = array();
		foreach ($this->Employee->get_all()->result() as $employee)
		{
			$data['employees'][$employee->person_id] = $employee->first_name . ' '. $employee->last_name;
		}

		$data['sale_info'] = $this->Sale->get_info($sale_id)->row_array();
				
		
		$this->load->view('sales/edit', $data);
	}
	
	function delete($sale_id)
	{
		$data = array();
		
		if ($this->Sale->delete($sale_id))
		{
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}
		
		$this->load->view('sales/delete', $data);
		
	}
	
	function save($sale_id)
	{
		$sale_data = array(
			'sale_time' => date('Y-m-d', strtotime($this->input->post('date'))),
			'customer_id' => $this->input->post('customer_id') ? $this->input->post('customer_id') : null,
			'employee_id' => $this->input->post('employee_id'),
			'comment' => $this->input->post('comment'),
			'desc' => $this->input->post('desc')
		);
		
		if ($this->Sale->update($sale_data, $sale_id))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('sales_successfully_updated')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('sales_unsuccessfully_updated')));
		}
	}
	
	function _payments_cover_total()
	{
		$total_payments = 0;

		foreach($this->sale_lib->get_payments() as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		/* Changed the conditional to account for floating point rounding */
		if ( ( $this->sale_lib->get_mode() == 'sale' ) && ( ( to_currency_no_money( $this->sale_lib->get_total() ) - $total_payments ) > 1e-6 ) )
		{
			return false;
		}
		
		return true;
	}
	
	function _reload($data=array())
	{
		$mode = $this->sale_lib->get_mode();
		$user_id = $this->session->userdata('person_id');
		if (!$this->Sale->getUserRegister($user_id)) {
			$this->session->set_flashdata('msg','Register is not Open');
			redirect(site_url('sales/open_register'));
		}
		$edit_sale_id = $this->session->userdata('edit_sale_id');

		if ($edit_sale_id) {
			$data['sale_info'] = $this->Sale->get_sale_info($edit_sale_id);
		}
		$person_info = $this->Employee->get_logged_in_employee_info();
		$data['cart']=$this->sale_lib->get_cart();
		// var_dump($data['cart']);die();
		
		$data['modes']=array('sale'=>$this->lang->line('sales_sale'),'return'=>$this->lang->line('sales_return'));
		$data['mode']=$this->sale_lib->get_mode();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
		$data['comment'] = $this->sale_lib->get_comment();
		$data['email_receipt'] = $this->sale_lib->get_email_receipt();
		$data['payments_total']=$this->sale_lib->get_payments_total();
		$data['amount_due']=$this->sale_lib->get_amount_due();
		$data['payments']=$this->sale_lib->get_payments();
		$data['payment_options']=array($this->lang->line('sales_cash') => $this->lang->line('sales_cash')
			// $this->lang->line('sales_check') => $this->lang->line('sales_check'),
			// $this->lang->line('sales_giftcard') => $this->lang->line('sales_giftcard'),
			// $this->lang->line('sales_debit') => $this->lang->line('sales_debit'),
			// $this->lang->line('sales_credit') => $this->lang->line('sales_credit')
			// 'AR'=>'AR'

		);

		$data['sale_date'] = $this->sale_lib->get_sale_date();


		$customer_id=$this->sale_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer_info'] = $info;
			$data['customer']=$info->last_name.' '.$info->first_name;
			$data['customer_phone']=$info->phone_number;
			$data['customer_address']=$info->address_1;
			// print_r($info);
			// die();
			$data['customer_email']=$info->email;
		}
		$item_id=-1;
		$data['item_info']=$this->Item->get_info($item_id);
		$data['new_item_info']=$this->Item->get_new_item_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$suppliers = array('' => $this->lang->line('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		
		$locations = array('' => $this->lang->line('items_none'));
		foreach($this->Location->get_all()->result_array() as $row)
		{
			$locations[$row['location_id']] = $row['location_name'];
		}		
		$data['locations']=$locations;
		$data['selected_location'] = $this->Item->get_info($item_id)->location_id;
		
		$makes = array('' => $this->lang->line('items_none'));
		foreach($this->Make->get_all()->result_array() as $row)
		{
			$makes[$row['make_id']] = $row['make_name'];
		}		
		$data['makes']=$makes;
		$data['selected_make'] = $this->Item->get_info($item_id)->make_id;
		
		$models = array('' => $this->lang->line('items_none'));
		foreach($this->Model->get_all()->result_array() as $row)
		{
			$models[$row['model_id']] = $row['model_name'];
		}		
		$data['models']=$models;
		$data['selected_model'] = $this->Item->get_info($item_id)->model_id;
		
		$colors = array('' => $this->lang->line('items_none'));
		foreach($this->Color->get_all()->result_array() as $row)
		{
			$colors[$row['color_id']] = $row['color_name'];
		}		
		$data['colors']=$colors;
		$data['selected_color'] = $this->Item->get_info($item_id)->color_id;
		
		$conditions = array('' => $this->lang->line('items_none'));
		foreach($this->Condition->get_all()->result_array() as $row)
		{
			$conditions[$row['condition_id']] = $row['condition_name'];
		}		
		$data['conditions']=$conditions;
		$data['selected_condition'] = $this->Item->get_info($item_id)->condition_id;
		
		$partplacements = array('' => $this->lang->line('items_none'));
		foreach($this->Partplacement->get_all()->result_array() as $row)
		{
			$partplacements[$row['partplacement_id']] = $row['partplacement_name'];
		}		
		$data['partplacements']=$partplacements;
		$data['selected_partplacement'] = $this->Item->get_info($item_id)->partplacement_id;
		
		$branchs = array('' => $this->lang->line('items_none'));
		foreach($this->Branch->get_all()->result_array() as $row)
		{
			$branchs[$row['branch_id']] = $row['branch_name'];
		}		
		$data['branchs']=$branchs;
		$data['selected_branch'] = $this->Item->get_info($item_id)->branch_id;
		
		$categorys = array('' => $this->lang->line('items_none'));
		foreach($this->Category->get_all()->result_array() as $row)
		{
			$categorys[$row['category_id']] = $row['category_name'];
		}		
		$data['categorys']=$categorys;
		$data['selected_category'] = $this->Item->get_info($item_id)->category_id;

		$khmernames = array('' => $this->lang->line('items_none'));
		foreach($this->Khmername->get_all()->result_array() as $row)
		{
			$khmernames[$row['khmername_id']] = $row['khmername_name'];
		}		
		$data['khmernames']=$khmernames;
		$data['selected_khmername'] = $this->Item->get_info($item_id)->khmername_id;
		
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		
		
		/*Grade of Items*/
		
		$grade = array('' => $this->lang->line('items_none'));
		
		foreach($this->Item->get_all_grade()->result_array() as $row)
		{
			$grade[$row['grad_id']] = $row['grad_name'];
		}	
			
		$data['grade']=$grade;
		$data['selected_grade'] = $this->Item->get_info($item_id)->grad_id;
		
		/*get unit price and cost price where grade_id*/
		$gradeA=$this->Item->get_unitPrice_costPrice(1)->row_array();
		$data['unitpriceA']=$gradeA['unit_price'];
		$data['costpriceA']=$gradeA['cost_price'];
		
		$gradeB=$this->Item->get_unitPrice_costPrice(2)->row_array();
		$data['unitpriceB']=$gradeB['unit_price'];
		$data['costpriceB']=$gradeB['cost_price'];
		
		$gradeC=$this->Item->get_unitPrice_costPrice(3)->row_array();
		$data['unitpriceC']=$gradeC['unit_price'];
		$data['costpriceC']=$gradeC['cost_price'];
		
		/*==-========================================================================*/
		
		$data['khmername_data'] = $this->Khmername->get_khmername_data();
		
		$data['image_name']=$this->Item->get_image_source($item_id)->result_array();
		$name_id = $this->session->userdata('sale_name_id');
		$data['sale_select_name'] = $this->Khmername->get_info($name_id);
		$qid = $this->session->userdata('q_id');
		if ($qid) {
			$data['qt_number'] = $this->Sale_quotation->getQinfo($qid)->qt_number;
		}

		$this->session->set_flashdata('warn_message',$data['warning']);

		$data['is_updatable'] = $this->sale_lib->get_updatable();
		$data['is_admin'] = $this->Employee->get_user_group_permission($user_id);
		$data['old_payment_total'] = $this->sale_lib->total_old_payment();

		if ($mode=='return') {

			$old_payment_total = $this->sale_lib->total_old_payment();
			// $total_restock = $this->
			// $data['amount_tendered']=$this->sale_lib->get_amount_due();
			$data['total'] = 0;
			$data['amount_due'] = 0;
			$data['has_return_item'] = 0;
			$data['total_restock'] = $this->sale_lib->get_total_restock();
			$data['amount_tendered'] = 0;
			$data['amount_payable'] = to_currency(0);
			$data['total_sale'] = $this->sale_lib->get_cart_total();

			if ($this->sale_lib->has_item_return()) {
				$data['amount_tendered'] = $this->sale_lib->get_amount_tender();
				$data['has_return_item'] = 1;
				$data['total']= $this->sale_lib->get_return_total();
				$data['amount_due']=$this->sale_lib->get_return_total();
				$data['amount_payable'] = to_currency($this->sale_lib->get_amount_payable());

				
			}
			// var_dump($data['total']);die();
		}
		
		$data['payments_cover_total'] = $this->_payments_cover_total();
		$this->load->view("sales/register",$data);
	}

    function cancel_sale()
    {
    	$q_id = $this->session->userdata('q_id');
    	$d_id = $this->session->userdata('d_id');
    	
    	if ($q_id) {
			$this->Sale_quotation->update_qstatus($q_id,'cancel');
    	}

    	if ($d_id) {

    		$this->Sale_suspended->update_dstatus($d_id,'cancel');
    		$this->session->unset_userdata('edit_sale_id');
	    	$this->session->unset_userdata('q_id');
	    	$this->session->unset_userdata('d_id');
	    	$this->sale_lib->clear_all();
    		// redirect(site_url('sales/dprint/'.$d_id.'/?c=cancel'));
    	}

		$this->session->unset_userdata('edit_sale_id');
    	$this->session->unset_userdata('q_id');
    	$this->session->unset_userdata('d_id');
    	$this->sale_lib->clear_all();
    	$this->_reload();

    }

    function cancel_deposit(){
    	$did = $this->session->userdata('d_id');
    	$data['dep_info'] = $this->db->query("SELECT * FROM ospos_sales_suspended WHERE sale_id = '$did'")->row();

    	$data['total_amount'] = $this->sale_lib->get_total();
    	$this->load->view('sales/cancel_deposit_form',$data);
    }
    function do_cancel_deposit(){
    	$did = $this->session->userdata('d_id');

    	$deposit_restock = $this->input->post('restock_amount');
    	$cancel_note = $this->input->post('cancel_deposit_note');

    	$depo = [
    				'cancel_restock'=>$deposit_restock,
    				'cancel_note'=>$cancel_note,
    				'status'=>'cancel'
    			];

    	// var_dump($depo);die();

		$this->Sale_suspended->update_dstatus($did,$depo);
		$this->session->unset_userdata('edit_sale_id');
    	$this->session->unset_userdata('q_id');
    	$this->session->unset_userdata('d_id');
    	$this->sale_lib->clear_all();


    	// redirect(site_url('sales'));
    	redirect(site_url('sales/dprint/'.$did.'/?c=cancel'));

    }
	
	function suspend()
	{

		$this->permission_lib->checkPermission();
		// $cart = $this->sale_lib->get_cart();
		// $payment = $this->sale_lib->get_payments();
		// var_dump($payment);die();
		// var_dump($cart);die();

		// $this->sale_lib->begin_submit();
		$data['cart']=$this->sale_lib->get_cart();
		if (!$data['cart']) {
			echo '-1';
			die();
		}
		// if (!$data['cart']) {
		// 	redirect('sales');
		// }

		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a');
		$customer_id=$this->sale_lib->get_customer()==-1?72:$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->input->post('comment');
		$desc = $this->input->post('desc');
		$emp_info=$this->Employee->get_info($employee_id);
		$payment_type = $this->input->post('payment_type');
		$data['payment_type']=$this->input->post('payment_type');
		//Alain Multiple payments
		$data['payments']=$this->sale_lib->get_payments();
		// $data['payments'] = array_merge($this->sale_lib->get_old_sale_payment(),$this->sale_lib->get_payments());

		// var_dump($data);die();
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$q_id = $this->session->userdata('q_id');
		// var_dump($data['cart']);die();
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		}

		$total_payments = 0;

		foreach($data['payments'] as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		//SAVE sale to database
		$data['sale_id']=$this->Sale_suspended->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'],$desc);
		if ($data['sale_id'] == '-1')
		{
			$data['error_message'] = $this->lang->line('sales_transaction_failed');
		}
		// UPDATE QUOTE
		if ($q_id) {
			$this->db->where('sale_id',$q_id)->update('sales_quote',array('status'=>'deposit'));
		}
		$this->session->unset_userdata('edit_sale_id');
		$this->session->unset_userdata('q_id');
		$this->session->unset_userdata('d_id');
		$this->sale_lib->clear_all();
		
		// $this->new_sale();
		// $this->_reload(array('success' => $this->lang->line('sales_successfully_suspended_sale')));
		// $this->sale_lib->finish_submit();
		// redirect(site_url('sales/dprint/'.$data['sale_id']));
		echo $data['sale_id'];

	}
	
	function suspended()
	{
		
		$this->permission_lib->checkPermission();
		$data = array();
		$data['suspended_sales'] = $this->Sale_suspended->get_all()->result_array();
		$this->load->view('sales/suspended', $data);
	}
	
	function unsuspend()
	{
		$sale_id = $this->input->post('suspended_sale_id');
		$this->session->set_userdata('d_id',$sale_id);
		$this->sale_lib->clear_all();
		$this->sale_lib->copy_entire_suspended_sale($sale_id);
		$this->Sale_suspended->delete($sale_id);
    	$this->_reload();
	}
	function view_deposit(){
		$this->sale_lib->remove_customer();
		$this->sale_lib->empty_cart();
		$this->sale_lib->empty_payments();
		// $this->sale_lib->empty_old_payments();
		$this->sale_lib->clear_all();

		$sale_id = $this->input->post('suspended_sale_id');
		$this->session->unset_userdata('edit_sale_id');

		$this->session->set_userdata('d_id',$sale_id);
		$this->session->unset_userdata('q_id');

		$this->sale_lib->clear_all();
		$this->sale_lib->copy_entire_suspended_sale($sale_id);
		$this->_reload();
	}
	function delete_suspend(){
		$sale_id = $this->input->post('suspended_sale_id_del');
		// $this->sale_lib->clear_all();
		$this->Sale_suspended->delete($sale_id);
    	$this->_reload();
	}
	function search_suspend(){
		$search = $this->input->post('search');
		$suspend=$this->Sale_suspended->search_suspend($search)->result();
		$table='';
		if (!$suspend) {
			$table.="<div>No Result</div>";
		}
		foreach ($suspend as $s) {
			$table.= '<tr>';
			$table.="<td>".$s->sale_id."</td>";
			$table.="<td>".date('d/m/Y',strtotime($s->sale_time))."</td>";
			if (isset($s->customer_id))
			{
				$customer = $this->Customer->get_info($s->customer_id);
				$company = '';
					if ($customer->nick_name!='') {
						$company = "($customer->nick_name)";
					}
				$table.="<td>".$customer->last_name. ' '. $customer->first_name.$company."</td>";
			}
			else
			{
				$table.="<td></td>";
			}
			$cm = '';
			if ($s->comment!='0') {
				$cm = $s->comment;
			}
			$table.="<td>".$customer->phone_number."</td>";
			$table.="<td>".$cm."</td>";
			$table.="<td>";
			$table.= form_open('sales/view_deposit');
			$table.= form_hidden('suspended_sale_id', $s->sale_id);
			$table.="<input type='submit' name='submit' value='View Deposit' id='submit' class='submit_button float_right' style='width:100%;'>";
			$table.= form_close();
			$table.="</td>";
			$table.="<td>";
			$table.= form_open('sales/delete_suspend');
			$table.= form_hidden('suspended_sale_id_del', $s->sale_id);
			$table.="<input onclick='return confirm('Are you sure ?')' type='submit' name='subdelete' value='Delete' class='submit_button float_right'>";
			$table.= form_close();
			$table.="</td>";
			$table.="<td>
						<a target='_target' href=".site_url('sales/dprint/'.$s->sale_id)." id='dprint' name='dprint' class='submit_button float_right'>
							Print
						</a>
					</td>";
			$table.="</tr>";

		}
		$data['data']=$table;
		echo json_encode($data);
	}

	// QOUTATION
	function add_quotation()
	{	
		$this->permission_lib->checkPermission();
		// $this->session->unset_userdata('d_id');
		$data['cart']=$this->sale_lib->get_cart();
		if (!$data['cart']) {
			echo '-1';
			die();
		}
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=$this->lang->line('sales_receipt');
		$data['transaction_time']= date('m/d/Y h:i:s a');
		$customer_id=$this->sale_lib->get_customer()==-1?72:$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->input->post('comment');
		$desc = $this->input->post('desc');
		$emp_info=$this->Employee->get_info($employee_id);
		$payment_type = $this->input->post('payment_type');
		$data['payment_type']=$this->input->post('payment_type');
		//Alain Multiple payments
		$data['payments']=$this->sale_lib->get_payments();
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$d_id = $this->session->userdata('d_id');
		// var_dump($data['cart']);die();
		if($customer_id!=-1)
		{

			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;
		}

		$total_payments = 0;

		foreach($data['payments'] as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		//SAVE sale to database
		$data['sale_id']=$this->Sale_quotation->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'],$desc);
		
		

		// UPDATE deposit status
		if ($d_id) {
			$this->db->where('sale_id',$d_id)->update('sales_suspended',array('status'=>'quoted'));
		}

			// $this->session->unset_userdata('d_id');


		if ($data['sale_id'] == '-1')
		{
			$data['error_message'] = $this->lang->line('sales_transaction_failed');
		}
		// $this->sale_lib->clear_all();
		// $this->_reload(array('success' => $this->lang->line('sales_successfully_quoted_sale')));
		
		$data['desc']=$this->input->post('desc');
		$totalallpayment=$this->sale_lib->get_total();
		
		// var_dump($data['cart']);die();

		
		$gettotal=$this->sale_lib->get_total();
		$paymentTotal=$this->sale_lib->get_payments_total();
		$changedue=$gettotal-$paymentTotal;

		// $this->sale_lib->clear_all();
		// $this->load->view("sales/receipt_quote",$data);
		// $this->qprint($data['sale_id']);
		// redirect(site_url('sales/qprint/'.$data['sale_id']));
		echo $data['sale_id'];

	}
	function quoted_sale()
	{
		$this->permission_lib->checkPermission();
		$data = array();
		$data['quoted_sales'] = $this->Sale_quotation->get_all()->result_array();
		$this->load->view('sales/quoted', $data);
	}
	function view_quoted()
	{
		$this->session->unset_userdata('edit_sale_id');
		
		$sale_id = $this->input->post('quoted_sale_id');
		$this->session->set_userdata('q_id',$sale_id);
		$this->session->unset_userdata('d_id');

		$this->sale_lib->clear_all();
		$this->sale_lib->copy_entire_quoted_sale($sale_id);
    	$this->_reload();
	}
	function delete_quote(){
		$sale_id = $this->input->post('quoted_sale_id_del');
		// $this->sale_lib->clear_all();
		$this->Sale_quotation->delete($sale_id);
    	redirect(site_url('sales'));
	}
	function search_quote(){
		$search = $this->input->post('search');
		$quote=$this->Sale_quotation->search_quote($search)->result();
		$table='';
		if (!$quote) {
			$table.="<div>No Result</div>";
		}
		foreach ($quote as $q) {
			$table.= '<tr>';
			$table.="<td>".$q->qt_number."</td>";
			$table.="<td>".date('d/m/Y',strtotime($q->quote_time))."</td>";
			if (isset($q->customer_id))
			{
				$customer = $this->Customer->get_info($q->customer_id);
				$company = '';
				if ($customer->nick_name!='') {
					$company = "($customer->nick_name)";
				}
				$table.="<td>".$customer->last_name. ' '. $customer->first_name.$company."</td>";
			}
			else
			{
				$table.="<td></td>";
			}
			$cm = '';
			if ($q->comment!='0') {
				$cm = $q->comment;
			}
			$table.="<td>".$customer->phone_number."</td>";
			$table.="<td>".$cm."</td>";
			$table.="<td>";
			$table.= form_open('sales/view_quoted');
			$table.= form_hidden('quoted_sale_id', $q->sale_id);
			$table.="<input type='submit' name='submit' value='View' id='submit' class='submit_button float_right' style='width:100%;'>";
			$table.= form_close();
			$table.="</td>";
			$table.="<td>";
			$table.= form_open('sales/delete_quote');
			$table.= form_hidden('quoted_sale_id_del', $q->sale_id);
			$table.="<input onclick='return confirm('Are you sure ?')' type='submit' name='subdelete' value='Delete' class='submit_button float_right'>";
			$table.= form_close();
			$table.="</td>";
			$table.="<td>
						<a target='_target' href=".site_url('sales/qprint/'.$q->sale_id)." id='qprint' name='qprint' class='submit_button float_right'>
							Print
						</a>
					</td>";
			$table.="</tr>";

		}
		$data['data']=$table;
		echo json_encode($data);
	}
	// QOUTATION END

	/*sale*/
	function convertBarcodeToItemNumber(){
		header('Content-Type: application/json');
		$barcode=$_REQUEST['getbarcode'];
		$data['itemid']=$this->Item->convertBarcodetoItemid($barcode)->item_id;
		echo json_encode($data);
	}
	function add_new_item(){
		$this->load->helper('url');
		$this->load->model('item');
		$this->load->model('sale');
		$this->load->model('khmername');
		$newitem = true;	//mark as new item
		$this->sale_lib->set_updatable();

		$vin_num = $this->input->post('s_vin_number');
		$name_s = $this->input->post('s_name');
		$make_id = $this->input->post('s_make_id');
		$model_id = $this->input->post('s_model_id');
		$year_i = $this->input->post('s_year');
		$partplacement = $this->input->post('s_partplacement_id');
		$color = $this->input->post('s_color_id');
		$price = $this->input->post('s_price');
		$qty = $this->input->post('s_quantity');
		$desc = $this->input->post('s_desc');
		$khmernameid = $this->input->post('s_name_select');

		// var_dump($_POST);die();
		// if ($khmernameid) {
		// 	$name_s = $this->Item->get_infokhmername($item_id)->khmername_name;
		// }

		
		// echo "Vin = $vin_num / kh = $khmername / make = $make_id
		// 	/ mo = $model_id / year = $year / pp = $partplacement / 
		// 	color = $color / pr = $price / qty = $qty / desc = $desc";die();
		if ($khmernameid!='' && $khmernameid!='other') {
			$khmer_name = $this->item->khmer_name($khmernameid)->row()->khmername_name;
			$eng_name = $this->item->khmer_name($khmernameid)->row()->english_name;
		}else{
			$eng_name = $name_s;
			$khmer_name= $name_s;
		}
		
		
		$item_data = array(
					'name'=>$eng_name,
					// 'item_number'=>$vin_num,
					'khmername_id'=>$khmernameid,
					'make_id'=>$make_id,
					'category'=>$vin_num,
					'model_id'=>$model_id,
					'year'=>$year_i,
					'partplacement_id'=>$partplacement,
					'color_id'=>$color,
					'unit_price'=>$price,
					'quantity'=>$qty,
					'description'=>$desc,
					'is_new'=>1,
					'khmer_name'=>$khmer_name
					
				);
		$mode = $this->sale_lib->get_mode();
		$quantity = $mode=="sale" ? 1:-1;

		
		// SAVE NEW NAME
		// if ($name_s!='') {
		// 	$this->db->insert('khmernames',array('khmername_name'=>$name_s,'english_name'=>$name_s));
		// }
		$new_item_id = $this->Item->save_item_sale($item_data);
		if($new_item_id){
			// $new_item_id = $this->Item->get_new_item_id($vin_num);
			$barcode = $this->newitem_barcode($new_item_id);
			// $item_barcode = array(
			// 	'barcode'=>$barcode
			// 					);
			// $this->Item->updateNewItemBarcodeAfterSave($new_item_id,$item_barcode);

			///add new item to cart
			$this->sale_lib->add_item($new_item_id,$newitem,$quantity,$barcode,0,$price,$desc);//to cart
			$this->_reload();	
		}
	}
	
	function newitem_barcode($new_item_id){

		// var_dump($new_item_id);
		$this->load->model('item');
		$this->load->model('sale');
		// $sale_id = $this->sale->generate_invoice();
		// $last_sale_id = $this->sale->get_last_sale()->invoiceid;
		$items = $this->sale_lib->get_cart();
		// $item_number = '';
		// $last_sale_day = date('d',strtotime($this->sale->get_last_sale()->sale_time));
		// $today = date('d');

		$count_today_sale = count($this->sale->get_today_sale());
		// // GET THE LAST BARCOD
		$new_item_line = 1;
		// if ($last_sale_id==$sale_id) {
		// 	// $i = substr($this->item->get_new_item_last_barcode(),strlen($sale_id));
		// 	$new_item_line++;
		// }else{
		// 	$new_item_line=1;
		// }
		// $sid = 1;
		foreach ($items as $key => $value) {
			if ($value['is_new']==1) {
				$new_item_line++;;
			}
		}

		if ($count_today_sale>0) {
			$count_today_sale += 1;
		}else{
			$count_today_sale = 1;
		}
		// $i = substr($item_number,strlen($sale_id));
		// $cur_sale_id = $sale_id;
		// $last_sale_id = substr($item_number,6,-strlen($i));
		
		// var_dump($cur_sale_id);
		// var_dump($last_sale_id);die();


		
		$barcode = date('ymd').$count_today_sale.$new_item_line;
		return $barcode;
	}

	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
	
	function get2FirstString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$end = $characters-$length;
		
		$firstString = substr($string ,0 ,$end);
		return $firstString;
		}
	}
	function view_all_return_items(){
		$this->load->model('sale');
		$data['return_items'] = $this->Sale->get_all_return_items()->result();
		$data['employee'] = $this->Employee->get_all()->result();

		$this->load->view('sales/return_items', $data);
	}
	function search_return_items(){
		$search = $this->input->post('search');
		$table='';
		$return_items = $this->Sale->search_return_items($search)->result();
		if (!return_items) {
			$table.="<div class='warning_message' align='center'>No Result</div>";
		}else{
			$i=0;
			foreach ($return_items as $item) {
				$part = $item->partplacement_name;
				if ($item->is_new==1) {
					$part = $item->pname;
				}
				$i++;
				$table.="
				<tr>
					<td>$item->invoiceid</td>
					<td>$item->barcode</td>
					<td>$item->name</td>
					<td>$item->khmername_name</td>
					<td>$item->make_name</td>
					<td>$item->model_name</td>
					<td>$item->year</td>
					<td>$item->color_name</td>
					<td>$part</td>
					<td>$item->item_unit_price</td>
					<td>$item->quantity_purchased</td>
				</tr>";
			}
		}
		$data['data'] = $table;
		echo json_encode($data);;
	}

	function update_qt_all_number(){
		$q = $this->db->query("SELECT * FROM ospos_sales_quote")->result();
		foreach ($q as $s) {
			$this->db->where('sale_id',$s->sale_id)->update('sales_quote',array('qt_number'=>str_pad($s->sale_id,6,'QT00',STR_PAD_LEFT)));
		}

	}

	function qprint($qid){
		$this->session->unset_userdata('d_id');
		$data['id'] = $qid;

		$data['qdata'] = $this->db->query("SELECT * FROM ospos_sales_quote WHERE sale_id='$qid'")->row();
		$customer_id = $data['qdata']->customer_id;
		$employee_id = $data['qdata']->employee_id;
		$data['items'] = $this->db->query("SELECT *,v.partplacement_id as ppl_id FROM ospos_sales_quote_items qi 
											LEFT JOIN ospos_v_select_all_items v ON qi.item_id=v.item_id
														AND qi.is_new=v.is_new
											LEFT JOIN ospos_khmernames k ON v.khmername_id=k.khmername_id
											LEFT JOIN ospos_makes m ON v.make_id=m.make_id
											LEFT JOIN ospos_models mo ON v.model_id=mo.model_id
											LEFT JOIN ospos_partplacements ppl ON v.partplacement_id=ppl.partplacement_id
											WHERE qi.sale_id = '$qid'")->result();
		$data['cus_info'] = $this->db->query("SELECT * FROM ospos_people WHERE person_id='$customer_id'")->row();
		$cust_info=$this->Customer->get_info($customer_id);
		$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
		$emp_info=$this->Employee->get_info($employee_id);
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name;
		$this->load->view('sales/qinvoice',$data);
	}
	function dprint($did){
		$data['id'] = $did;

		$data['ddata'] = $this->db->query("SELECT *,SUM(p.payment_amount) as payment FROM ospos_sales_suspended s
											INNER JOIN ospos_sales_suspended_payments p ON s.sale_id=p.sale_id
											WHERE s.sale_id = '$did'
											GROUP BY s.sale_id")->row();
		$customer_id = $data['ddata']->customer_id;
		$employee_id = $data['ddata']->employee_id;
		$data['items'] = $this->db->query("SELECT *,v.partplacement_id as ppl_id,qi.description as description FROM ospos_sales_suspended_items qi 
											LEFT JOIN ospos_v_select_all_items v ON qi.item_id=v.item_id
														AND qi.is_new=v.is_new
											LEFT JOIN ospos_khmernames k ON v.khmername_id=k.khmername_id
											LEFT JOIN ospos_makes m ON v.make_id=m.make_id
											LEFT JOIN ospos_models mo ON v.model_id=mo.model_id
											LEFT JOIN ospos_partplacements ppl ON v.partplacement_id=ppl.partplacement_id
											WHERE qi.sale_id = '$did'")->result();
		$data['cus_info'] = $this->db->query("SELECT * FROM ospos_people WHERE person_id='$customer_id'")->row();
		$cust_info=$this->Customer->get_info($customer_id);
		$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
		$emp_info=$this->Employee->get_info($employee_id);
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name;
		$this->load->view('sales/dinvoice',$data);
	}
	function new_sale(){
		$this->session->unset_userdata('edit_sale_id');
		$this->session->unset_userdata('q_id');
		$this->session->unset_userdata('d_id');
		$this->sale_lib->set_mode($mode);
		$this->sale_lib->remove_customer();
		$this->sale_lib->empty_cart();
		$this->sale_lib->empty_payments();
		$this->sale_lib->empty_old_payment();
		$this->_reload();
	}
	function sale_invoice($sale_id){
		$data['sale_id'] = $sale_id;
		$data['sdata'] = $this->db->query("SELECT * FROM ospos_sales WHERE sale_id='$sale_id'")->row();
		$customer_id = $data['sdata']->customer_id;
		$employee_id = $data['sdata']->employee_id;
		$data['invoice_id'] = $data['sdata']->invoiceid;
		$data['transaction_time'] = $data['sdata']->sale_time;
		$data['items'] = $this->db->query("SELECT 
											*,
											v.is_new,
											v.item_id,
											v.barcode as barcode,
											v.partplacement_id as ppl_id,
											qi.description as description 
											FROM ospos_sales_items qi 
											LEFT JOIN ospos_v_select_all_items v ON qi.item_id=v.item_id
														AND qi.is_new=v.is_new
											LEFT JOIN ospos_khmernames k ON v.khmername_id=k.khmername_id
											LEFT JOIN ospos_makes m ON v.make_id=m.make_id
											LEFT JOIN ospos_models mo ON v.model_id=mo.model_id
											LEFT JOIN ospos_partplacements ppl ON v.partplacement_id=ppl.partplacement_id
											LEFT JOIN ospos_return_item re ON re.item_id = qi.item_id
											LEFT JOIN ospos_return_sale rs ON re.return_id = rs.return_id
											WHERE qi.sale_id = '$sale_id'")->result();

		$data['cus_info'] = $this->db->query("SELECT * FROM ospos_people WHERE person_id='$customer_id'")->row();
		$data['return_amount'] = $this->db->query("SELECT SUM(rt.price - rt.disc) as return_amount FROM ospos_return_sale r
												INNER JOIN ospos_return_item rt ON r.return_id = rt.return_id
												WHERE sale_id = '$sale_id'")->row()->return_amount;
		$cust_info=$this->Customer->get_info($customer_id);
		$data['customer']=$cust_info->last_name.' '.$cust_info->first_name;
		$emp_info=$this->Employee->get_info($employee_id);
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name.' ID: '.$emp_info->person_id;
		$data['payment'] = $this->db->query("SELECT SUM(amount_usd) as payment_amount FROM ospos_payment_record_detail WHERE sale_id = $sale_id")->row();
		$data['sale_pay_amount'] = $data['sdata']->payment_type;
		$this->load->view('sales/receipt',$data);
	}
	function edit_sale($sale_id){
		$this->session->set_userdata('edit_sale_id',$sale_id);
		$this->session->unset_userdata('q_id');
		$this->session->unset_userdata('d_id');
		// $edit_invoice = $this->db->query("SELECT * FROM ospos_sales WHERE sale_id = '$sale_id'")->row()->invoiceid;\
		$this->sale_lib->clear_all();
		$this->sale_lib->edit_entire_sale($sale_id);
		redirect(site_url('sales'));
	}

	function chk_new_name(){
		$name = $this->input->post('name');

		$data['t'] = 'n';
		$sql = $this->db->query("SELECT * FROM ospos_khmernames WHERE khmername_name = '$name' OR english_name='$name'")->result();
		if (count($sql)>0) {
			$data['t']='y';
		}
		echo json_encode($data);
	}
	function open_register(){
		$data['r'] = 'Register';
		$data['controller_name'] = 'Open Register';
		$this->load->view('sales/open_register',$data);
	}
	function do_open_register(){
		$cash = $this->input->post('open_cash');
		$user_id = $this->session->userdata('person_id');

		$now = date('Y-m-d H:i:s');
		if ($cash<0) {
			redirect(site_url('sales'));
		}

		$data = array(	'cash_in_hand'=>$cash,
						'open_by'=>$user_id,
						'status'=>'open',
						'open_time'=>$now);
		$register = $this->Sale->do_open_register($data);
		if($register['reg_id']){

			//PAYMENT TRANSACTION
			$trans_data = [
							'transaction_time'=>$now,
							'transaction_type'=>'OPEN REGISTER',
							'transaction_of_id'=>$register['reg_id'],
							'transaction_of_user_id'=>$user_id,
							'reference_no'=>$register['reg_ref'],
							'amount'=>$cash
						];

			$this->main_model->save_payment_transaction($trans_data);
			//END PAYMENT TRANSACTION


			redirect(site_url('sales'));
		}
	}
	function close_register(){
		$user_id = $this->session->userdata('person_id');
		if (!$this->Sale->getUserRegister($user_id)) {
			$this->session->set_flashdata('msg','Register is not Open');
			redirect(site_url('sales/open_register'));
		}
		$close_time = date('Y-m-d H:i:s');
		$data['r'] = 'Register';
		$data['controller_name'] = 'Close Register';

		$user_id = $this->session->userdata('person_id');
		$data['reg_info'] = $this->Sale->getUserRegister($user_id);
		$r = $data['reg_info'];
		// $data['sale_due'] = $this->Sale->getSaleDue($r->open_time,$close_time);
		// $data['deposit'] = $this->Sale->getTotalDeposit($r->open_time,$close_time);
		// $data['deposit_due'] = $this->Sale->getDepositDue($r->open_time,$close_time);
		// $data['return'] = $this->Sale->getEmpReturnPayment($r->open_time,$close_time);
		// $data['due_payment'] = $this->Sale->getEmpDuePayment($r->open_time,$close_time);
		// $data['total_cash'] = (($r->sale_submit + $data['deposit'] + $r->cash_in_hand + $data['due_payment']) - $data['sale_due']) - $data['return'];
		$all_sale = $this->Sale->getAllSale($r->open_time,$close_time);

		$data['cash_in_hand'] = $r->cash_in_hand;
		$data['total_sale'] = $this->Sale->CR_sale_total($r->open_time,$close_time);
		$data['total_deposit'] = $this->Sale->CR_deposit_total($r->open_time,$close_time);
		$data['total_return'] = $this->Sale->CR_return_total($r->open_time,$close_time);
		$data['total_due_payment'] = $this->Sale->CR_due_payment_total($r->open_time,$close_time);

		// $data['total_cash'] = $r->cash_in_hand + $data['total_sale'] + $data['total_deposit'] + $data['total_due_payment'] + $data['total_return'];
		$data['total_cash'] = array_sum($data);







		$vinnumbers = $this->Sale->getAllVin();
		$tbody='';
		$i=0;
		foreach ($all_sale as $sale) {
			$i++;
			$paid= (float)filter_var($sale->paid, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION); 
			$ets = $this->Sale->getSalePayment($sale->sale_id)->payment_amount;
			$esd = $this->Sale->getSalePayment($sale->sale_id)->total_due;
			// $tbody.="<tr>
			// 			<td>$i</td>
			// 			<td>$sale->invoiceid</td>
			// 			<td>".date('d-m-Y H:i:s',strtotime($sale->sale_time))."</td>
			// 			<td>$sale->title $sale->last_name $sale->first_name</td>
			// 			<td>$ets</td>
			// 			<td>$paid</td>
			// 			<td>$esd</td>";
			$sale_item = $this->Sale->getSaleItem($sale->sale_id);
			// if ($sale_item) {
			// 	$tbody.="<tr class='danger danger-item'>
			// 				<th></th>
			// 				<th>Vin</th>
			// 				<th>Part Number</th>
			// 				<th>Part Name</th>
			// 				<th>Make</th>
			// 				<th>Model</th>
			// 				<th>Year</th>
			// 			</tr>";
			// }
			$j=0;
			foreach ($sale_item as $item) {
				$j++;
				$name = $item->khmer_name;
				if (!$item->khmer_name) {
					$name = $item->name;
				}
				$ppl = $item->ppl_id;
				if ($item->is_new==0) {
					$ppl = $item->partplacement_name;
				}
				$tbody.= "<tr class='info danger-item'>
							<td style='text-align:right'>$j</td>
							<td>
								<select class='vin_sel' name='vin_sel[]'>
									<option value=''>Select</option>";
									foreach ($vinnumbers as $v) {
										$tbody.= "<option value='".$v->vinnumber_id."'>H".$v->vinnumber_id."</option>";
									}

						$tbody.="</select>
							<input type='hidden' name='item_id[]' value='".$item->item_id."'>
							<input type='hidden' name='is_new[]' value='".$item->is_new."'>
							<input type='hidden' name='price[]' value='".$item->item_unit_price."'>
							</td>
							<td>$item->barcode</td>
							<td>$name</td>
							<td>$item->make_name</td>
							<td>$item->model_name</td>
							<td>$item->year</td>
			 				<td>$sale->invoiceid</td>

						</tr>";
			}
			// $tbody.="</tr>";
		}
		$data['tbody'] = $tbody;
		$this->load->view('sales/close_register',$data);
	}
	function do_close_register(){
		$user_id = $this->session->userdata('person_id');
		$reg_info = $this->Sale->getUserRegister($user_id);
		$total_sale = $reg_info->sale_submit;
		$note = $this->input->post('register_note');
		$data = array(
					'status'=>'close',
					'total_sale'=>$total_sale,
					'close_time'=>date('Y-m-d H:i:s'),
					'close_by'=>$user_id,
					'note'=>$note
				);

		// UPDATE VINNUMBER SOLD
		$item_id = $this->input->post('item_id');
		$vin = $this->input->post('vin_sel');
		$is_new = $this->input->post('is_new');
		$price = $this->input->post('price');
		if (count(item_id)>0) {
			for ($i=0; $i < count($item_id); $i++) { 
				if ($vin[$i]!='') {
					// echo $item_id[$i];
					// echo "<br>".$vin[$i];
					// echo "<br>".$is_new[$i].'<br>';
					// echo $price[$i].'<br>';

					$this->Sale->update_vin_sale($vin[$i],$item_id[$i],$is_new[$i],$price[$i]);
				}

			}
		}
		
		// 

		if($this->Sale->do_close_register($reg_info->register_id,$user_id,$data)){
			$this->session->unset_userdata('q_id');
			$this->session->unset_userdata('d_id');
			$this->sale_lib->clear_all();
			$this->session->unset_userdata('edit_sale_id');
			
			redirect(site_url('sales/print_close_register/'.$reg_info->register_id));
		}

		
	}
	function print_close_register($reg_id){
		$data['controller_name'] = 'Print Close Register';
		$data['reg_info'] = $this->Sale->getRegisterInfo($reg_id);
		$data['deposit'] = $this->Sale->getTotalDeposit($data['reg_info']->open_time,$data['reg_info']->close_time);
		$data['sale_due'] = $this->Sale->getSaleDue($data['reg_info']->open_time,$data['reg_info']->close_time);

		$emp_info=$this->Employee->get_info($data['reg_info']->close_by);
		$data['employee']=$emp_info->last_name.' '.$emp_info->first_name;
		$all_sale = $this->Sale->getAllSale($data['reg_info']->open_time,$data['reg_info']->close_time);
		$vinnumbers = $this->Sale->getAllVin();
		$tbody='';
		$i=0;
		$ts = 0;
		$tp = 0;
		$td = 0;
		foreach ($all_sale as $sale) {
			$i++;
			$paid= (float)filter_var($sale->paid, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION); 

			$ets = $this->Sale->getSalePayment($sale->sale_id)->payment_amount;
			$esd = $this->Sale->getSalePayment($sale->sale_id)->total_due;

			$ts += $ets;
			$tp += $paid;
			$td += $esd;
			$tbody.="<tr>
						<td>$i</td>
						<td>$sale->invoiceid</td>
						<td>$sale->sale_time</td>
						<td>$sale->last_name $sale->first_name</td>
						<td>$ets</td>
						<td>$paid</td>
						<td>$esd</td>";
			// $sale_item = $this->Sale->getSaleItem($sale->sale_id);
			// if ($sale_item) {
			// 	$tbody.="<tr class='danger danger-item'>
			// 				<th colspan='1'></th>
			// 				<th>Vin</th>
			// 				<th>Part Number</th>
			// 				<th>Part Name</th>
			// 				<th>Make</th>
			// 				<th>Model</th>
			// 				<th>Year</th>
			// 			</tr>";
			// }
			// $j=0;
			// foreach ($sale_item as $item) {
			// 	$j++;
			// 	$tbody.= "<tr class='info danger-item'>
			// 				<td style='text-align:right'>$j</td>
			// 				<td>
			// 					<select class='vin_sel' name='vin_sel[]'>
			// 						<option value=''>Select</option>";
			// 						foreach ($vinnumbers as $v) {
			// 							$tbody.= "<option value='".$v->vinnumber_id."'>H".$v->vinnumber_id."</option>";
			// 						}

			// 			$tbody.="</select>
			// 				<input type='hidden' name='item_id[]' value='".$item->item_id."'>
			// 				<input type='hidden' name='is_new[]' value='".$item->is_new."'>
			// 				<input type='hidden' name='price[]' value='".$item->item_unit_price."'>
			// 				</td>
			// 				<td>$item->item_number</td>
			// 				<td>$item->khmername_name / $item->english_name</td>
			// 				<td>$item->make_name</td>
			// 				<td>$item->model_name</td>
			// 				<td>$item->year</td>
			// 			</tr>";
			// }
			$tbody.="</tr>";
		}
		$data['ts'] = $ts;
		$data['tp'] = $tp;
		$data['td'] = $td;
		$data['tbody'] = $tbody;
		$this->load->view('sales/print_close_register',$data);

	}
	function check_submit(){
		echo $this->sale_lib->get_submit();
	}
	function list_sales(){
		$this->permission_lib->checkPermission();
		$is_admin = $this->Employee->get_info($this->session->userdata('person_id'))->is_admin;
		$data['employee'] = $this->Employee->get_all()->result();
		$data['customer'] = $this->Customer->get_all()->result();
		if ($is_admin==1) {
			$this->load->view('sales/list_sales',$data);
		}else{
			$this->load->view('sales/list_sales_user',$data);

		}
	}
	function get_list_sale(){
		$user = $this->input->post('user');
		$cus = $this->input->post('cus');
		$period = $this->input->post('per');
		$fd = $this->input->post('fd');
		$td = $this->input->post('td');
		$fm = $this->input->post('fm');
		$tm = $this->input->post('tm');
		$fy = $this->input->post('fy');
		$ty = $this->input->post('ty');

		$list = '';
		$sale_data = $this->Sale->get_list_sale($user,$cus,$period,$fd,$td,$fm,$tm,$fy,$ty);

		$i=0;
		foreach ($sale_data as $sd) {
			$cus = $this->Sale->get_sale_people($sd->customer_id);
			$emp = $this->Sale->get_sale_people($sd->employee_id);
			$customer= '';
			if (!empty($cus)) {
				$customer = $cus->title.$cus->last_name.' '.$cus->first_name;
			}
			if (!empty($emp)) {
				$employee = $cus->title.$emp->last_name.' '.$emp->first_name;
			}
			
			
			$sale_date = date('d/m/Y',strtotime($sd->sale_time));
			if ($period=='Monthly') {
				$sale_date = date('m/Y',strtotime($sd->sale_time));
			}
			if ($period=='Yearly') {
				$sale_date = date('Y',strtotime($sd->sale_time));
			}
			
			
			// $payment = $this->Sale->get_grand_total($sd->sale_id);

			// $gtotal = $payment->payment_amount;
			// $due = $payment->total_due;
			$gtotal = $sd->sum_amount_to_pay;
			$due = $sd->sum_due;
			

			if ($due<0) {
				$due = 0;
			}
			$actions ='';
			// $actions .= '
			// 			<div class="dropdown">
			// 			  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			// 			    Actions
			// 			    <span class="caret"></span>
			// 			  </button>
			// 			  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			// 			    <li><a target="'.rand(1,100).'" href="'.site_url('sales/sale_invoice/'.$sd->sale_id).'">Print Reciept</a></li>
			// 			    <li><a href="'.site_url("sales/edit_sale/".$sd->sale_id).'">Edit Sale</a></li>
			// 			  </ul>
			// 			</div>
			// 			';

			// $actions .= "<a href='".site_url('sales/detail_list_sale?e='.$user.'&c='.$cus.'&p='.$period.'&fd='.$fd.'&td='.$td.'&fm='.$fm.'&tm='.$tm.'&fy='.$fy.'&ty='.$ty)."'>Detail</a>";

			// $first_paid = $sd->sum_first;
			$paid = $sd->sum_first_paid + $sd->sum_paid;
			$i++;
			$list.="<tr>
						<td>".$i."</td>
						<td>".$sale_date."</td>
						<td>".$sd->cus_title.$sd->cus_name."</td>
						<td>$ ".number_format($gtotal,2)."</td>
						<td>$ ".number_format($paid,2)."</td>
						<td>$ ".number_format($due,2)."</td>
						<td>".$sd->emp_title.$sd->emp_name."</td>
					</tr>";
		}

		$tfoot = "<tr>
						<th colspan='3' style='text-align:right;' ><strong>Total</strong></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>";


		$data['tbody'] = $list;
		$data['tfoot'] = $tfoot;


		echo json_encode($data);
	}
	function get_list_sale_detail(){
		$user = $this->input->post('user');
		$cus = $this->input->post('cus');
		// $period = $this->input->post('per');
		$fd = $this->input->post('fd');
		$td = $this->input->post('td');
		// $fm = $this->input->post('fm');
		// $tm = $this->input->post('tm');
		// $fy = $this->input->post('fy');
		// $ty = $this->input->post('ty');

		$list = '';
		$sale_data = $this->Sale->get_list_sale_detail($user,$cus,$fd,$td);

		$i=0;
		foreach ($sale_data as $sd) {
			
			$sale_date = date('d/m/Y',strtotime($sd->sale_time));
			
			// $payment = $this->Sale->get_grand_total($sd->sale_id);

			// $gtotal = $payment->payment_amount;
			// $due = $payment->total_due;
			// $gtotal = $sd->sum_amount_to_pay;
			// $due = $sd->sum_due;
			

			// if ($due<0) {
			// 	$due = 0;
			// }
			$actions ='';
			$actions .= '
						<div class="dropdown">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    Actions
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						    <li><a target="'.rand(1,100).'" href="'.site_url('sales/sale_invoice/'.$sd->sale_id).'">Print Invoice</a></li>
						    <li><a href="'.site_url("sales/edit_sale/".$sd->sale_id).'">Edit Sale</a></li>
						  </ul>
						</div>
						';

			// $actions .= "<a href='".site_url('sales/detail_list_sale?e='.$user.'&c='.$cus.'&p='.$period.'&fd='.$fd.'&td='.$td.'&fm='.$fm.'&tm='.$tm.'&fy='.$fy.'&ty='.$ty)."'>Detail</a>";

			// $first_paid = $sd->sum_first;
			$sale_total = $this->past_due->get_sale_total($sd->sale_id);
			$paid = $this->past_due->get_sale_paid($sd->sale_id);
			$sale_due = $sale_total - $paid;

			// $due = $amount_to_pay-$paid;

			$i++;

			$label = 'info';
			if ($sd->payment_status=='paid') {
				$label = 'success';
			}
			$list.="<tr>
						<td>".$i."</td>
						<td>".$sale_date."</td>
						<td>".$sd->invoiceid."</td>
						<td>".$sd->cus_title.$sd->cus_name."</td>
						<td>$ ".number_format($sale_total,2)."</td>
						<td>$ ".number_format(($paid),2)."</td>
						<td>$ ".number_format(($sale_due<0?0:$sale_due),2)."</td>
						<td>".$sd->emp_title.$sd->emp_name."</td>
						<td style='text-align:center;text-transform: capitalize;'><label class='label label-".$label."'>".$sd->payment_status."</label></td>
						<td>".$actions."</td>

					</tr>";
		}

		$tfoot = "<tr>
						<th colspan='4' style='text-align:right;' ><strong>Total</strong></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>";


		$data['tbody'] = $list;
		$data['tfoot'] = $tfoot;


		echo json_encode($data);
	}
	function list_detail_sale(){
		$is_admin = $this->Employee->get_info($this->session->userdata('person_id'))->is_admin;
		$data['employee'] = $this->Employee->get_all()->result();
		$data['customer'] = $this->Customer->get_all()->result();
		if ($is_admin==1) {
			$this->load->view('sales/list_detail',$data);
		}else{
			$this->load->view('sales/list_detail_user',$data);
		}
	}
	
	function clear_sale_data(){
		$cn = $this->input->post('cn');
		$this->sale_lib->remove_customer();
		$this->sale_lib->empty_cart();
		$this->sale_lib->empty_payments();
		$this->sale_lib->clear_all();
		echo $cn;
	}

	function update_sale_first_pay(){
		$sales = $this->db->query("SELECT * FROM ospos_sales")->result();
		foreach ($sales as $sd) {
			$first_paid = (float)filter_var($this->Sale->get_sale_by_id($sd->sale_id)->payment_type, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
			// var_dump($first_paid);
			$data = array('first_payment'=>$first_paid);
			$this->db->where('sale_id',$sd->sale_id)->update('ospos_sales',$data);
		}

		redirect(site_url('sales'));
	}
	
	function get_list_return(){
		$emp_id = $this->input->post('emp_id');
		$fd = $this->input->post('fd');
		$td = $this->input->post('td');

		$return = $this->Sale->get_filter_return($emp_id,$fd,$td);
		$html = '';
		$row_id = 0;

		foreach ($return as $re) {

			$sales = $this->Sale->get_return_sale($re->return_date);

					$html .='<div class="col-md-12">
								<div class="col-md-12 bg-info">
									<span class="text_f">'.date('d-m-Y',strtotime($re->return_date)).'</span>
								</div>';

					foreach ($sales as $sale) {
						$row_id++;
						$sale_item = $this->Sale->get_return_items($sale->sale_id);

						$html.='<div class="col-md-12">
									<div class="col-md-12 bg-secondary return_no_div" row-id="1" onclick="toggleItems('.$row_id.')" title="Click to Expand">
										<label>Invoice #:</label>
										<span class="text_f">'.$sale->invoiceid.'</span>
									</div>
								</div>
								<div class="col-md-12 return_item_div" id="row_'.$row_id.'">
									<div class="col-md-12">
										<div class="col-md-12">
											<table class="table table-bordered">
												<thead>
													<th>No</th>
													<th>Barcode</th>
													<th>Head#</th>
													<th>Description</th>
													<th>Qty</th>
													<th>PPL</th>
													<th>Price</th>
													<th>Restock</th>
													<th>Status</th>
												</thead>
												<tbody>';
										$i=0;
										foreach ($sale_item as $st) {
											$i++;
											$name = $st->khmername_name;
											if (!$st->khmername_name) {
												$name = $st->name;
											}
											$ppl = $st->ppl_id;
											if ($st->is_new==0) {
												$ppl = $st->partplacement_name;
											}
											$stat = '<label class="label label-info">Not Returned</label>';
											if ($st->is_return==1) {
												$stat = '<label class="label label-danger">Returned</label>';
											}
											$html.='<tr>
														<td>'.$i.'</td>
														<td>'.$st->barcode.'</td>
														<td>'.$st->category.'</td>
														<td>'.$st->year.' '.$st->make_name.' '.$st->model_name.' '.$name.' '.$st->description.'</td>
														<td>'.$st->quantity_purchased.'</td>
														<td>'.$ppl.'</td>
														<td>'.$st->item_unit_price.'</td>
														<td>'.number_format($st->return_restock,2).'</td>
														<td>'.$stat.'</td>
														
													</tr>';
										}
											

										$html.='</tbody>

											</table>
										</div>
									</div>
								</div>';
					}
					
					
					$html.='</div>';
		}
		



		$data['return_data'] = $html;
		echo json_encode($data);
	}

	function view_transaction(){
		$this->permission_lib->checkPermission();
		$user_id = $this->session->userdata('person_id');
		$data['is_admin'] = $this->Employee->get_user_group_permission($user_id);
		$data['employee'] = $this->Employee->get_all()->result();
		$data['trtype'] = $this->main_model->get_all_transaction_type();

		// $filter_date = $this->input->post('filter_date');

		// $data['transaction'] = $trans;
		$this->load->view('sales/list_transaction', $data);

	}
 	
 	function get_transaction(){
		$user_id = $this->session->userdata('person_id');
		$is_admin = $this->Employee->get_user_group_permission($user_id);

		if ($is_admin) {
			$user_id = '';
			$filter_user = $this->input->post('user');
			if ($filter_user!='') {
				$user_id = $filter_user;
			}
			
		}
		$filter_date_from = $this->input->post('fd');
		$filter_date_to = $this->input->post('td');
		$period = $this->input->post('per');
		$fm = $this->input->post('fm');
		$tm = $this->input->post('tm');
		$fy = $this->input->post('fy');
		$ty = $this->input->post('ty');

		$ftype = $this->input->post('ftype');

		$where='';
		$gr_by = '';

		if ($period=='Daily') {
			$gr_by = "GROUP BY t.reference_no,t.transaction_type,DATE_FORMAT(t.transaction_time,'%Y-%m-%d')";
			if ($filter_date_from=='' && $fitler_date_to=='') {
				$filter_date_from = date('Y-m-d');
				$filter_date_to = date('Y-m-d');
			}

		}
		if ($period=='Monthly') {
			$gr_by = "GROUP BY t.transaction_type,DATE_FORMAT(t.transaction_time,'%Y-%m')";
			if ($fm=='' && $tm=='') {
				$fm = date('Y-m');
				$tm = date('Y-m');
			}
		}
		if ($period=='Yearly') {
			$gr_by = "GROUP BY t.transaction_type,DATE_FORMAT(t.transaction_time,'%Y')";

			if ($fy=='' && $ty=='') {
				$fy = date('Y');
				$ty = date('Y');
			}
		}

		if (!$is_admin) {
			$gr_by.= ",t.transaction_of_user_id";
		}

		if ($ftype) {
			$where.= " AND t.transaction_type = '$ftype'";
		}

		if ($user_id) {
			$where.= "AND t.transaction_of_user_id = '$user_id'";
		}

		// echo json_encode($where);die();
		if ($filter_date_from && $filter_date_to) {
			$where.=" AND DATE_FORMAT(t.transaction_time,'%Y-%m-%d') BETWEEN "."'".date('Y-m-d',strtotime($filter_date_from))."' AND '".date('Y-m-d',strtotime($filter_date_to))."'";
		}

		
		if ($fm && $tm) {
			$where.=" AND (DATE_FORMAT(t.transaction_time,'%Y-%m') BETWEEN '".$fm."' AND '".$tm."')";
		}
		if ($fy && $ty) {
			$where.=" AND (DATE_FORMAT(t.transaction_time,'%Y') BETWEEN '".$fy."' AND '".$ty."')";
		}
	
		
		

		// $where = '';
		$trans = $this->main_model->get_emp_transaction($where,$gr_by);
		$trs = "";
		$i=0;
		$total= 0;
		$tfoot = "";

		if ($trans) {
			foreach ($trans as $tr) {
				$type = $tr->transaction_type;
				$total += $tr->amount;
				$i++;

				$transaction_time = date('d/m/Y',strtotime($tr->transaction_time));
				if ($period=='Monthly') {
					$transaction_time = date('m/Y',strtotime($tr->transaction_time));
				}
				if ($period=='Yearly') {
					$transaction_time = date('Y',strtotime($tr->transaction_time));
				}

				$trs.= "<tr>";
					$trs.="<td>$i</td>";
					$trs.="<td>$tr->emp_name</td>";
					$trs.="<td>".($transaction_time?$transaction_time:'N/A')."</td>";
					$trs.="<td>$type</td>";
					$trs.="<td><a title='Transaction History' href='".site_url('sales/transaction_detail/'.$tr->transaction_of_id)."' target='".'TRA_'.rand(0,9999)."'>".$tr->reference_no."</a></td>";
					$trs.="<td>".number_format($tr->amount,2)."$</td>";
				$trs.= "</tr>";

				// $total+= $tr->amount;
			}
			$tfoot .= "<tr>";
				$tfoot .= "<td align='right' colspan=5><b>Total</b></td>";
				$tfoot .= "<td>".number_format($total,2)."$</td>";
			$tfoot .= "</tr>";

		}
		
		

		$data['tra'] = $trans;
		$data['tbody'] = $trs;
		$data['tfoot'] = $tfoot;

		echo json_encode($data);

 	}

 	function transaction_detail($of_id){
 		$this->permission_lib->checkPermission();
		$data['of_id'] = $of_id;


		$this->load->view('partial/header',$data);
		$this->load->view('sales/transaction_detail', $data);
		$this->load->view('partial/footer',$data);

 	}

 	function get_transaction_detail(){
 		$user_id = $this->session->userdata('person_id');
		$data['is_admin'] = $this->Employee->get_user_group_permission($user_id);
		$is_admin = $data['is_admin'];
		$of_id = $this->input->post('of_id');

		if ($is_admin) {
			$user_id='';
		}
		if ($user_id) {
			$where.= "AND t.transaction_of_user_id = '$user_id'";
		}

		if ($of_id) {
			$where.= "AND t.transaction_of_id = '$of_id'";
			
		}



		$trans = $this->main_model->get_emp_transaction_detail($where);
		$trs = "";
		$i=0;
		$total= 0;
		$tfoot = "";
		



		if ($trans) {
			foreach ($trans as $tr) {
				$type = $tr->transaction_type;
				$total += $tr->amount;
				$i++;

				// $transaction_time = date('d/m/Y',strtotime($tr->transaction_time));
				// if ($period=='Monthly') {
				// 	$transaction_time = date('m/Y',strtotime($tr->transaction_time));
				// }
				// if ($period=='Yearly') {
				// 	$transaction_time = date('Y',strtotime($tr->transaction_time));
				// }

				$trs.= "<tr>";
					$trs.="<td>$i</td>";
					$trs.="<td>$tr->emp_name</td>";
					$trs.="<td>".date('d/m/Y H:i:s',strtotime($tr->transaction_time))."</td>";
					$trs.="<td>$type</td>";
					$trs.="<td>$tr->reference_no</td>";
					$trs.="<td>".number_format($tr->amount,2)."$</td>";
				$trs.= "</tr>";

				// $total+= $tr->amount;
			}
			$tfoot .= "<tr>";
				$tfoot .= "<td align='right' colspan=5><b>Total</b></td>";
				$tfoot .= "<td>".number_format($total,2)."$</td>";
			$tfoot .= "</tr>";

		}

		$data['tra'] = $trans;
		$data['tbody'] = $trs;
		$data['tfoot'] = $tfoot;

		echo json_encode($data);
 	}


 	public function view_returned_items($return_id)
 	{
 		
 		$data['return_id'] = $return_id;
 		$this->load->view('sales/returned_items',$data);
 	}

 	public function get_return_items()
 	{
 		$return_id = $this->input->post('return_id');

 		// echo $sale_id;die();
 		$return_items = $this->Sale->get_sale_return_items($return_id);
		$trs = "";
		$i=0;
		$total= 0;
		$tfoot = "";
		



		if ($return_items) {
			foreach ($return_items as $tr) {
				// $type = $tr->transaction_type;
				$total += $tr->item_unit_price;
				$i++;

				$item_desc = '';
				if ($item->description) {
					$item_desc = "($item->description)";
				}

				// $transaction_time = date('d/m/Y',strtotime($tr->transaction_time));
				// if ($period=='Monthly') {
				// 	$transaction_time = date('m/Y',strtotime($tr->transaction_time));
				// }
				// if ($period=='Yearly') {
				// 	$transaction_time = date('Y',strtotime($tr->transaction_time));
				// }

				$trs.= "<tr>";
					$trs.="<td>$i</td>";
					$trs.="<td>$tr->barcode</td>";
					$trs.="<td>$tr->category</td>";
					$trs.="<td>$tr->year $tr->make_name $tr->model_name $name $ppl $item_desc</td>";
					$trs.="<td>".number_format($tr->item_unit_price,2)."</td>";
					$trs.="<td>".number_format($tr->return_restock,2)."</td>";
					// $trs.="<td>".number_format(($tr->item_unit_price - $tr->return_restock),2)."</td>";

				$trs.= "</tr>";

				// $total+= $tr->amount;
			}
			// $tfoot .= "<tr>";
			// 	$tfoot .= "<td align='right' colspan=5><b>Total</b></td>";
			// 	$tfoot .= "<td>".number_format($total,2)."$</td>";
			// $tfoot .= "</tr>";

		}

		// $data['tra'] = $trans;
		$data['tbody'] = $trs;
		// $data['tfoot'] = $tfoot;

		echo json_encode($data);
 	}
	
}
?>