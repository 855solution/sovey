<?php
require_once ("secure_area.php");
class engine extends secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('modengine','en');


	}
	
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Engine';
		$engine = $this->en->get_all_engine();
		$table = '';
		$i=0;
		foreach ($engine as $eng) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$eng->engine_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$eng->engine_name' f='$eng->engine_id'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$eng->engine_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('engine/manage',$data);
		$this->load->view('partial/footer');
	}

	function save(){
		$name = $this->input->post('engine_name');
		$id = $this->input->post('engine_id');
		$exist = $this->en->check_name($name,$id);
		$engine_data = array(
						'engine_name'=>$name,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('engine'));
		}else{
			$this->en->save($id,$engine_data);
			redirect(site_url('engine'));

		}
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->en->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function delete($id){
		$this->db->where('engine_id',$id)->update('ospos_engine',array('deleted'=>1));
		redirect(site_url('engine'));
	}

}
?>