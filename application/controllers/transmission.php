<?php
require_once ("secure_area.php");
class transmission extends secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('modtrans','trans');


	}
	
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Transmission';
		$trans = $this->trans->get_all_trans();
		$table = '';
		$i=0;
		foreach ($trans as $tra) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$tra->transmission_name</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$tra->transmission_name' f='$tra->trans_id'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$tra->trans_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('transmission/manage',$data);
		$this->load->view('partial/footer');
	}

	function save(){
		$name = $this->input->post('trans_name');
		$id = $this->input->post('trans_id');
		$exist = $this->trans->check_name($name,$id);
		$trans_data = array(
						'transmission_name'=>$name,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('transmission'));
		}else{
			$this->trans->save($id,$trans_data);
			redirect(site_url('transmission'));

		}
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->trans->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function delete($id){
		$this->db->where('trans_id',$id)->update('ospos_transmission',array('deleted'=>1));
		redirect(site_url('transmission'));
	}

}
?>