<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class subpart extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('vinnumbers');
        $this->load->model('Vinnumber','vinnm');
	}
	
	function index()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/vinnumbers/index');
		$config['total_rows'] = $this->subparts->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_subpartbar_manage_table( $this->subparts->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('subpart/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_subpartbar_manage_table_data_rows($this->subparts->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Vinnumber->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$vinnumber_id = $this->input->post('row_id');
		$data_row=get_vinnumber_data_row($this->Vinnumber->get_info($vinnumber_id),$this);
		echo $data_row;
	}

	function view($vinnumber_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['partbar_info']=$this->subparts->get_info($vinnumber_id);

		
		//$data['head_image_name']=$this->Vinnumber->get_head_image_source($vinnumber_id)->result_array();
		

		$this->load->view("subpart/form",$data);
	}
	
	function save($vinnumber_id=-1)
	{
		$this->load->helper('url');
		$item_number=$this->input->post('part_number');
		
		
		$vinnumber_data = array('item_number'=>$item_number,
								'partbars_id'=>$this->input->post('partbar_id'),
								'name'=>$this->input->post('part_name'),
								'make_id'=>$this->input->post('make'),
								'model_id'=>$this->input->post('model_id'),
								'year'=>$this->input->post('year'));
		$where='';
		if($vinnumber_id>0)
			$where=" AND subpartbar_id <> '$vinnumber_id'";
		$count=$this->db->query("SELECT count(*) as count FROM ospos_subpartbarcode WHERE item_number='$item_number' AND deleted='0' {$where}")->row()->count;
		if($count==0){	
			if( $this->subparts->save( $vinnumber_data, $vinnumber_id ))
			{
				//New vinnumber
				if($vinnumber_id==-1)
				{
					echo json_encode(array('success'=>true,'message'=>$this->lang->line('vinnumbers_successful_adding').' '.
					 $vinnumber_data['item_number'],'vinnumber_id'=>$vinnumber_data['subpartbar_id']));
					$vinnumber_id = $vinnumber_data['subpartbar_id'];
					// $this->addHeadImage($vinnumber_id);
					$this->do_upload($vinnumber_id);
				}
				else //previous vinnumber
				{
					echo json_encode(array('success'=>true,'message'=>$this->lang->line('vinnumbers_successful_updating').' '.
					 $vinnumber_data['item_number'],'vinnumber_id'=>$vinnumber_id));
					// $this->addHeadImage($vinnumber_id);
					$this->do_upload($vinnumber_id);
				}
				
			}
			else//failure
			{
				echo json_encode(array('success'=>false,'message'=>$this->lang->line('vinnumbers_error_adding_updating').' '.
				 $vinnumber_data['item_number'],'vinnumber_id'=>-1));
			}

		}else{
			echo json_encode(array('success'=>false,'message'=>'Item number is already exists'.' '.
				 $vinnumber_data['item_number'],'vinnumber_id'=>-1));
		}
		 redirect('subpart/');

	}
	function generate_barcodes($item_ids,$barcode_time=1)
	{
		$result = array();

		$item_ids = explode(':', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->db->query("SELECT * FROM ospos_partbarcode WHERE partbar_id='$item_id'")->row();

			$result[] = array('barcode' =>$item_info->item_number, 'id'=> $partbar_id,'name' =>$item_info->item_number, 'item_id' =>$item_info->partbar_id);
		}

		$data['items'] = $result;
		$data['barcodetime']=$barcode_time;
		$this->load->view("partbarcode", $data);
	}

	function do_upload($slide_id)
		{
			if(!file_exists('./uploads/partbar/'))
			 {
			    if(mkdir('./uploads/partbar/',0755,true))
			    {
			                return true;
			    }
			 }

			$config['upload_path'] ='./uploads/partbar/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '5000';
			$config['file_name']  = "$slide_id.jpg";
			$config['overwrite']=true;
			$config['file_type']='image/png';
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('userfile'))
			{
				$error = array('error' => $this->upload->display_errors());

			}
			else
			{	
				//$this->generate_thumb($this->upload->file_name,$this->upload->upload_path)	;	
				$data = array('upload_data' => $this->upload->data());			
				//redirect('setting/user');
				 	$config2['image_library'] = 'gd2';
                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                    $config2['new_image'] = './uploads/partbar/thumb/';
                    $config2['maintain_ratio'] = TRUE;
                    $config2['create_thumb'] = TRUE;
                    $config2['thumb_marker'] = FALSE;
                    $config2['width'] = 180;
                    $config2['height'] = 180;
                    $config2['overwrite']=true;
                    $this->load->library('image_lib',$config2); 

                    if ( !$this->image_lib->resize()){
	                	$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
					}else{
						//unlink('./assets/upload/students/'.$student_id.'.jpg');
					}
			}
		}



	function delete()
	{
		$this->permission_lib->checkPermission();
		$vinnumbers_to_delete=$this->input->post('ids');

		if($this->subparts->delete_list($vinnumbers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>'Delete sucessful'.' '.
			count($vinnumbers_to_delete).' '.$this->lang->line('vinnumbers_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'Delete Error Please ...'));
		}
	}
	function get2lastString($string){
		$length = strlen($string);
		if($length==2){
			return $string;
		}else{
		$characters = 2;
		$start = $length - $characters;
		$lastString = substr($string , $start ,$characters);
		return $lastString;
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	
 	function addHeadImage($vin_head_id){
 			
 		for($j=0;$j<count($_FILES['path']['name']);$j++){	
			if ($_FILES['path']['name'][$j]!=""){

			
				
				$image_directory = "./uploads/";
				$big_directory = "./uploads/";
				$allowed_ext = "jpg, JPG, gif, GIF , bmp, BMP, jpeg, JPEG , png, PNG"; 
				$extension = pathinfo($_FILES['path']['name'][$j]);
				$extension = $extension["extension"];
				$allowed_paths = explode(", ", $allowed_ext);
				for($i = 0; $i < count($allowed_paths); $i++) {
					if ($allowed_paths[$i] == $extension) {
						 $ok="1";
					}
				}
				if($ok=="1"){
						$concatenate_str1 = time();
						if(is_uploaded_file($_FILES['path']['tmp_name'][$j])){	
								$picture_name=$_FILES['path']['name'][$j];	
								move_uploaded_file($_FILES['path']['tmp_name'][$j],$image_directory.$concatenate_str1.$picture_name);
							  $imageurl=$concatenate_str1.$picture_name;
						}
		
		$file_data=array('vin_number'=>$vin_head_id,'url_image'=>$concatenate_str1.$picture_name);
		
 		$this->load->database();
	    $ret=$this->db->insert('head_images',$file_data);		
				}
					
			}		   
 		}

 	
 }

 	function deleteHeadImage(){
 		$value=true;
 		$imageid=$this->input->post('imageid');
 		if($imageid){
 			$this->load->model('vinnumber');
 			$image_name=$this->Vinnumber->getImageName($imageid);
 		$delete=$this->Vinnumber->delete_headImage_item($imageid);
 		if($delete){
 			 unlink("./uploads/".$image_name);
 		}
 		
 		}
	
 	} 
 

 function go(){
 		header('Content-Type: application/json');
 	
 		$itemnumber=-1;
 		
 		$itemnumber=$_REQUEST['vinvalue'];
 		
 		$data['item_info']=$this->Vinnumber->get_Vin_Info($itemnumber);
 
		
		echo json_encode($data);
	
 	}		
 	
 
}
?>