<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Conditions extends Secure_area
{
	function __construct()
	{
		parent::__construct('conditions');
	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/conditions/index');
		$config['total_rows'] = $this->Condition->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_conditions_manage_table( $this->Condition->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('conditions/manage',$data);
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_conditions_manage_table_data_rows($this->Condition->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Condition->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$condition_id = $this->input->post('row_id');
		$data_row=get_condition_data_row($this->Condition->get_info($condition_id),$this);
		echo $data_row;
	}

	function view($condition_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['condition_info']=$this->Condition->get_info($condition_id);

		$this->load->view("conditions/form",$data);
	}
	
	function save_old($condition_id=-1)
	{
		$condition_data = array(
		'condition_name'=>$this->input->post('condition_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Condition->save( $condition_data, $condition_id ) )
		{
			//New condition
			if($condition_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('conditions_successful_adding').' '.
				$condition_data['condition_name'],'condition_id'=>$condition_data['condition_id']));
				$condition_id = $condition_data['condition_id'];
			}
			else //previous condition
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('conditions_successful_updating').' '.
				$condition_data['condition_name'],'condition_id'=>$condition_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('conditions_error_adding_updating').' '.
			$condition_data['condition_name'],'condition_id'=>-1));
		}

	}

	function delete_old()
	{
		$this->permission_lib->checkPermission();
		$conditions_to_delete=$this->input->post('ids');

		if($this->Condition->delete_list($conditions_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('conditions_successful_deleted').' '.
			count($conditions_to_delete).' '.$this->lang->line('conditions_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('conditions_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Conditions';
		$condition = $this->Condition->get_all_condition();
		$table = '';
		$i=0;
		foreach ($condition as $con) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$con->condition_name</td>  
						<td>$con->description</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$con->condition_name' f='$con->condition_id' d='$con->description'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$con->condition_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('conditions/manage',$data);
		$this->load->view('partial/footer');
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->Condition->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function save(){
		$name = $this->input->post('condition_name');
		$id = $this->input->post('condition_id');
		$desc = $this->input->post('description');
		$exist = $this->Condition->check_name($name,$id);
		$ppl_data = array(
						'condition_name'=>$name,
						'description'=>$desc,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('conditions'));
		}else{
			$this->Condition->save($id,$ppl_data);
			redirect(site_url('conditions'));

		}
	}
	function delete($id){
		$this->db->where('condition_id',$id)->update('ospos_conditions',array('deleted'=>1));
		redirect(site_url('conditions'));
	}
	
}
?>