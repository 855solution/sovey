<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Partplacements extends Secure_area
{
	function __construct()
	{
		parent::__construct('partplacements');
	}
	
	function index_old()
	{
		$this->permission_lib->checkPermission();
		$config['base_url'] = site_url('/partplacements/index');
		$config['total_rows'] = $this->Partplacement->count_all();
		$config['per_page'] = '20';
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_partplacements_manage_table( $this->Partplacement->get_all( $config['per_page'], $this->uri->segment( $config['uri_segment'] ) ), $this );
		$this->load->view('partplacements/manage',$data);
	}
	function index()
	{
		
		$this->permission_lib->checkPermission();
		$data['can_add']=$this->permission_lib->user_f('add');
		$can_e = $this->permission_lib->user_f('edit');
		$can_d = $this->permission_lib->user_f('delete');
		$data['controller_name'] = 'Partplacement';
		$ppl = $this->Partplacement->get_all_partplacement();
		$table = '';
		$i=0;
		foreach ($ppl as $pp) {
			$i++;
			$table.="
					<tr>
						<td>$i</td>
						<td>$pp->partplacement_name</td>  
						<td>$pp->description</td>";  
						$table.="<td>";
						if ($can_e==1) {
							$table.="<button data-toggle='modal' data-target='#myModal' class='btn btn-primary btn-sm btn_edit' n='$pp->partplacement_name' f='$pp->partplacement_id' d='$pp->description'>Edit</button>";
						}	
						$table.="</td>";

						$table.="<td>";
						if ($can_d==1) {
							$table.="<button class='btn btn-danger btn-sm btn_del' style='margin-left:5px' f='$pp->partplacement_id' >Delete</button>";
						}
						$table.="</td>";

			$table.="</tr>
					";
		}
		$data['table'] = $table;
		$this->load->view('partial/header');
		$this->load->view('partplacements/manage',$data);
		$this->load->view('partial/footer');
	}


	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_partplacements_manage_table_data_rows($this->Partplacement->search($search),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Partplacement->get_search_suggestions($this->input->post('q'),$this->input->post('limit'));
		echo implode("\n",$suggestions);
	}

	function get_row()
	{
		$partplacement_id = $this->input->post('row_id');
		$data_row=get_partplacement_data_row($this->Partplacement->get_info($partplacement_id),$this);
		echo $data_row;
	}

	function view($partplacement_id=-1)
	{
		$this->permission_lib->checkPermission();
		$data['partplacement_info']=$this->Partplacement->get_info($partplacement_id);

		$this->load->view("partplacements/form",$data);
	}
	
	function save_old($partplacement_id=-1)
	{
		$partplacement_data = array(
		'partplacement_name'=>$this->input->post('partplacement_name'),
		'description'=>$this->input->post('description')
		);

		if( $this->Partplacement->save( $partplacement_data, $partplacement_id ) )
		{
			//New partplacement
			if($partplacement_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('partplacements_successful_adding').' '.
				$partplacement_data['partplacement_name'],'partplacement_id'=>$partplacement_data['partplacement_id']));
				$partplacement_id = $partplacement_data['partplacement_id'];
			}
			else //previous partplacement
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('partplacements_successful_updating').' '.
				$partplacement_data['partplacement_name'],'partplacement_id'=>$partplacement_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('partplacements_error_adding_updating').' '.
			$partplacement_data['partplacement_name'],'partplacement_id'=>-1));
		}

	}

	function delete_old()
	{
		$this->permission_lib->checkPermission();
		$partplacements_to_delete=$this->input->post('ids');

		if($this->Partplacement->delete_list($partplacements_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>$this->lang->line('partplacements_successful_deleted').' '.
			count($partplacements_to_delete).' '.$this->lang->line('partplacements_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('partplacements_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 360;
	}
	function check_name(){
		$n = $this->input->post('name');
		$id = $this->input->post('id');
		$exist = $this->Partplacement->check_name($n,$id);
		if ($exist) {
			$e= 1;
		}else{
			$e= 0;
		}

		echo json_encode($e);
	}
	function save(){
		$name = $this->input->post('partplacement_name');
		$id = $this->input->post('partplacement_id');
		$desc = $this->input->post('description');
		$exist = $this->Partplacement->check_name($name,$id);
		$ppl_data = array(
						'partplacement_name'=>$name,
						'description'=>$desc,
						'deleted'=>0
					);
		if ($exist) {
			$this->session->set_flashdata('er','Name Exist !');
			redirect(site_url('partplacements'));
		}else{
			$this->Partplacement->save($id,$ppl_data);
			redirect(site_url('partplacements'));

		}
	}
	function delete($id){
		$this->db->where('partplacement_id',$id)->update('ospos_partplacements',array('deleted'=>1));
		redirect(site_url('partplacements'));
	}
}
?>