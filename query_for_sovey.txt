CREATE TABLE `ospos_permission_detail` (
  `perm_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `perm_id` int(11) DEFAULT NULL,
  `module_id` varchar(255) DEFAULT NULL,
  `access` int(1) DEFAULT NULL,
  `view` int(1) DEFAULT NULL,
  `add` int(1) DEFAULT NULL,
  `edit` int(1) DEFAULT NULL,
  `delete` int(1) DEFAULT NULL,
  `create_quote` int(1) DEFAULT NULL,
  `create_deposit` int(1) DEFAULT NULL,
  `create_sale` int(1) DEFAULT NULL,
  `view_deposit` int(1) DEFAULT NULL,
  `view_quote` int(1) DEFAULT NULL,
  `add_payment` int(1) DEFAULT NULL,
  PRIMARY KEY (`perm_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1057 DEFAULT CHARSET=latin1;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
INSERT INTO `ospos_permission_detail` VALUES ('991', '1', 'body', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('992', '1', 'branchs', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('993', '1', 'categorys', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('994', '1', 'colors', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('995', '1', 'conditions', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('996', '1', 'config', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('997', '1', 'customers', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('998', '1', 'employees', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('999', '1', 'engine', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1000', '1', 'giftcards', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1001', '1', 'items', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1002', '1', 'item_kits', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1003', '1', 'khmernames', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1004', '1', 'locations', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1005', '1', 'location_not', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1006', '1', 'makes', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1007', '1', 'member', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1008', '1', 'models', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1009', '1', 'page', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1010', '1', 'partbarcode', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1011', '1', 'partplacements', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1012', '1', 'past_dues', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1013', '1', 'permission', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1014', '1', 'receivings', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1015', '1', 'reports', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1016', '1', 'returns', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1017', '1', 'sales', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', null);
INSERT INTO `ospos_permission_detail` VALUES ('1018', '1', 'slider', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1019', '1', 'subpart', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1020', '1', 'suppliers', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1021', '1', 'template', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1022', '1', 'trim', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1023', '1', 'vinnumbers', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null);
INSERT INTO `ospos_permission_detail` VALUES ('1024', '4', 'body', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1025', '4', 'branchs', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1026', '4', 'categorys', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1027', '4', 'colors', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1028', '4', 'conditions', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1029', '4', 'config', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1030', '4', 'customers', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1031', '4', 'employees', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1032', '4', 'engine', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1033', '4', 'giftcards', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1034', '4', 'items', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1035', '4', 'item_kits', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1036', '4', 'khmernames', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1037', '4', 'locations', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1038', '4', 'location_not', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1039', '4', 'makes', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1040', '4', 'member', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1041', '4', 'models', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1042', '4', 'page', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1043', '4', 'partbarcode', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1044', '4', 'partplacements', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1045', '4', 'past_dues', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1046', '4', 'permission', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1047', '4', 'receivings', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1048', '4', 'reports', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1049', '4', 'returns', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1050', '4', 'sales', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `ospos_permission_detail` VALUES ('1051', '4', 'slider', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1052', '4', 'subpart', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1053', '4', 'suppliers', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1054', '4', 'template', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1055', '4', 'trim', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
INSERT INTO `ospos_permission_detail` VALUES ('1056', '4', 'vinnumbers', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_user_permission` (
  `set_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `perm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
------------------------------------------------------------------------------------------------
CREATE TABLE `ospos_permission_detail` (
  `perm_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `perm_id` int(11) DEFAULT NULL,
  `module_id` varchar(255) DEFAULT NULL,
  `access` int(1) DEFAULT NULL,
  PRIMARY KEY (`perm_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=latin1;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_group_permission` (
  `perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `perm_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `perm_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`perm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_permission_detail`
ADD COLUMN `view`  int(1) NULL AFTER `access`,
ADD COLUMN `add`  int(1) NULL AFTER `view`,
ADD COLUMN `edit`  int(1) NULL AFTER `add`,
ADD COLUMN `delete`  int(1) NULL AFTER `edit`,
ADD COLUMN `create_quote`  int(1) NULL AFTER `delete`,
ADD COLUMN `create_deposit`  int(1) NULL AFTER `create_quote`,
ADD COLUMN `create_sale`  int(1) NULL AFTER `create_deposit`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_permission_detail`
ADD COLUMN `view_deposit`  int(1) NULL AFTER `create_sale`,
ADD COLUMN `view_quote`  int(1) NULL AFTER `view_deposit`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_permission_detail`
ADD COLUMN `add_payment`  int(1) NULL AFTER `view_quote`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_customer_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `imagename` varchar(255) DEFAULT NULL,
  `first_image` int(11) DEFAULT NULL,
  `tem_image` int(1) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_people`
ADD COLUMN `phone_number_2`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `phone_number`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_people`
ADD COLUMN `payment_term`  varchar(30) NULL AFTER `title`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_people`
ADD COLUMN `payment_term`  varchar(30) NULL AFTER `title`;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_khmernames`
ADD COLUMN `english_name`  varchar(255) NOT NULL AFTER `deleted`;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
ADD COLUMN `is_problem`  int(1) NULL AFTER `is_pro`;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
MODIFY COLUMN `is_problem`  int(1) NULL DEFAULT 0 AFTER `is_pro`;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
ADD COLUMN `is_pending`  int(1) NULL AFTER `is_problem`;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
ADD COLUMN `engine`  int(11) NULL AFTER `is_pending`,
ADD COLUMN `body`  int(11) NULL AFTER `engine`,
ADD COLUMN `fuel`  int(11) NULL AFTER `body`,
ADD COLUMN `trim`  int(11) NULL AFTER `fuel`,
ADD COLUMN `transmission`  int(11) NULL AFTER `trim`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
ADD COLUMN `add_date`  date NULL AFTER `transmission`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
ADD COLUMN `vinnumber_id`  int(11) NULL AFTER `partplacement_name`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_body` (
  `body_id` int(11) NOT NULL AUTO_INCREMENT,
  `body_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`body_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_engine` (
  `engine_id` int(11) NOT NULL AUTO_INCREMENT,
  `engine_name` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`engine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_fuel` (
  `fuel_id` int(11) NOT NULL AUTO_INCREMENT,
  `fule_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`fuel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_transmission` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `transmission_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE `ospos_trim` (
  `trim_id` int(11) NOT NULL AUTO_INCREMENT,
  `trim_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`trim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
ADD FULLTEXT INDEX `all_item` (`name`, `year`, `model_name`, `make_name`, `color_name`, `branch_name`, `partplacement_name`) ;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_items`
DROP INDEX `ospos_items_ibfk_1`,
DROP INDEX `name`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_template_images`
MODIFY COLUMN `first_image`  int(11) NULL DEFAULT NULL AFTER `image_name`;
++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_template_images`
CHANGE COLUMN `tem_iamge_id` `tem_image_id`  int(11) NOT NULL AUTO_INCREMENT FIRST ;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_part`
ADD COLUMN `approve`  int(1) NULL DEFAULT 0 AFTER `quantity`,
ADD COLUMN `date`  datetime NULL AFTER `approve`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE `ospos_sales_items`
ADD COLUMN `desc`  text NULL AFTER `discount_percent`;
+++++++++++++++++++++++++++++++++++++++++++++++++++++++