<?php 

	function save($item_id=-1)
	{
		
		$this->load->helper('url');
		$this->load->model('item');
		$name=array();
		$makeid=$this->input->post('make_id');
		$modelid=$this->input->post('model_id');
		$year_input=$this->input->post('year');
		$grad_id=$this->input->post('grade_id');
		
		$make_name=$this->item->make_name($makeid)->row_array();
		$name['getname']=$make_name['make_name'];
		
		$modelname=$this->item->model_name($modelid)->row_array();
		$name['modelname']=$modelname['model_name'];
		
		$make_name=$this->get2FirstString($name['getname']);
		$year=$this->get2lastString($year_input);
		$model_name=$this->get2FirstString($name['modelname']);
		
		//$digit=$this->generate_barcode_6digit(6);
		//$digit=$this->input->post('item_number')+1;
		//if($item_id==-1){
		//$lastid=$this->Item->getLastId();
		//$id=$lastid['item_id'];
		
		//$digit=$lastid+1;
		//$digit_padding=str_pad($digit,6,"1",STR_PAD_LEFT);
		
		//$barcode=$year.$make_name.$model_name.$digit_padding;
		//}else{
			//$barcode=$this->Item->get_info($item_id);
			//$barcode=$item_data['barcode'];
			//}
		
		
		$item_data = array(
		'name'=>$this->input->post('name'),
		'description'=>$this->input->post('description'),
		'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
		'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
		'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
		'cost_price'=>$this->input->post('cost_price'),
		'unit_price'=>$this->input->post('unit_price'),
		'quantity'=>$this->input->post('quantity'),
		'reorder_level'=>$this->input->post('reorder_level'),
		'location'=>$this->input->post('location'),
		'fit'=>$this->input->post('fit'),
		'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
		'year'=>$this->input->post('year'),
		'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
		'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
		'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
		'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
		'allow_alt_description'=>$this->input->post('allow_alt_description'),
		'is_serialized'=>$this->input->post('is_serialized'),
		'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
		'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
		'category'=>$this->input->post('category'),
		'barcode'=>$barcode,
		'grad_id'=>$grad_id,
		'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id')
		);
		
		
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		
		if($this->Item->save($item_data,$item_id))
		{
			
			//$itemid=$this->db->insert_id();
			
			//New item
			if($item_id==-1)
			{
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_adding').' '.
				//$item_data['name'],'item_id'=>$item_data['item_id']));
				$item_id = $item_data['item_id'];
			}
			else //previous item
			{
				//echo json_encode(array('success'=>true,'message'=>$this->lang->line('items_successful_updating').' '.
				//$item_data['name'],'item_id'=>$item_id));
			}
			
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item_id,
				'trans_user'=>$employee_id,
				'trans_comment'=>$this->lang->line('items_manually_editing_of_quantity'),
				'trans_inventory'=>$cur_item_info ? $this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity')
			);
			
			$this->Inventory->insert($inv_data);
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k] );
				}
			}
			
/*--------------------update barcode after save item sucessful-----------------------------------------*/
			
			$digit_padding=str_pad($item_id,6,"0",STR_PAD_LEFT);
			$barcode=$year.$make_name.$model_name.$digit_padding;
			
			$barcode_item_data = array(
				'name'=>$this->input->post('name'),
				'description'=>$this->input->post('description'),
				'category_id'=>$this->input->post('category_id')=='' ? null:$this->input->post('category_id'),
				'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
				'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
				'cost_price'=>$this->input->post('cost_price'),
				'unit_price'=>$this->input->post('unit_price'),
				'quantity'=>$this->input->post('quantity'),
				'reorder_level'=>$this->input->post('reorder_level'),
				'location'=>$this->input->post('location'),
				'fit'=>$this->input->post('fit'),
				'model_id'=>$this->input->post('model_id')=='' ? null:$this->input->post('model_id'),
				'year'=>$this->input->post('year'),
				'color_id'=>$this->input->post('color_id')=='' ? null:$this->input->post('color_id'),
				'condition_id'=>$this->input->post('condition_id')=='' ? null:$this->input->post('condition_id'),
				'partplacement_id'=>$this->input->post('partplacement_id')=='' ? null:$this->input->post('partplacement_id'),
				'branch_id'=>$this->input->post('branch_id')=='' ? null:$this->input->post('branch_id'),
				'allow_alt_description'=>$this->input->post('allow_alt_description'),
				'is_serialized'=>$this->input->post('is_serialized'),
				'location_id'=>$this->input->post('location_id')=='' ? null:$this->input->post('location_id'),
				'make_id'=>$this->input->post('make_id')=='' ? null:$this->input->post('make_id'),
				'category'=>$this->input->post('category'),
				'barcode'=>$barcode,
				'grad_id'=>$grad_id,
				'khmername_id'=>$this->input->post('khmername_id')=='' ? null:$this->input->post('khmername_id')
				);
			$this->Item->updateBarcodeAfterSave($item_id,$barcode_item_data);
			
			/*-----------------------------------------------------------------------------*/

			
			$this->Item_taxes->save($items_taxes_data, $item_id);
			
			
			$this->addImage($item_id);
			echo '300 OK';
			//redirect('items/comment');	
			/*if($this->input->post('generate_barcode')){
			redirect('items/generate_barcodes/'.$item_id.'/'.$this->input->post("quantity").'');
			}else{
			redirect('items/');	
			}*/
		}
		else//failure
		{
			//echo json_encode(array('success'=>false,'message'=>$this->lang->line('items_error_adding_updating').' '.
			//$item_data['name'],'item_id'=>-1));
		}
	}
	
	
	
	
?>


<?php 
//function save copy from model item
	/*
	Inserts or updates a item
	*/
	function save(&$item_data,$item_id=false)
	{
		if (!$item_id or !$this->exists($item_id))
		{
			if($this->db->insert('items',$item_data))
			{
				$item_data['item_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	}
?>